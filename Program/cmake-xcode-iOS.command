#! /bin/bash
cd `dirname $0`

if [ ! -d "Build" ]; then
	mkdir Build
fi
cd Build
if [ -d "xcode-iOS" ]; then
	rm -Rf xcode-iOS
fi
mkdir xcode-iOS
cd xcode-iOS
cmake -DCMAKE_TOOLCHAIN_FILE="CMake/iOS.toolchain.cmake" -DBUILD_RUNTIME=1 -D_USE_EASTL=1 -D_SUPPORT_METAL=1 -D_SUPPORT_VULKAN=1 -G "Xcode" ../..
