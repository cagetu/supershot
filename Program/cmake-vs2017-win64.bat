@echo off
@echo:
@echo Generating Visual Studio Solution (64 bit)
@echo:
pushd %~dp0
:: remove any generated cmake build files
if not exist "Build" md Build
cd "Build"
if exist "msvc2017" rd "msvc2017" /s /q
md msvc2017
cd msvc2017
::cmake -G "Visual Studio 15 2017 Win64" -D_SUPPORT_D3D11=1 -D_SUPPORT_D3D12=1 -D_SUPPORT_OPENGL_ES=1 -D_SUPPORT_VULKAN=1 ..\..
cmake -G "Visual Studio 15 2017 Win64" -D_USE_EASTL=1 -D_SUPPORT_OPENGL_ES=1 -D_SUPPORT_VULKAN=1 ..\..
@echo:
@echo Solution created in %~dp0Build\msvc2017
@echo:
popd
if "%1" == "nopause" goto end
pause
:end