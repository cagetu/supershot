# Locate DX11
#
# This module defines:
# DX11_INCLUDE_DIR = where to find the headers
# DX11_LIBRARY = where to DX11 library exist
# DX11_FOUND = YES or NO
#

if (WIN32)
    set(DX11_LIBDIR "")
elseif (APPLE)
    set(DX11_LIBDIR "macOS")
elseif (LINUX)
    set(DX11_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(DX11_LIBDIR ${DX11_LIBDIR}/x64)
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(DX11_LIBDIR ${DX11_LIBDIR}/x64)
else ()
    set(DX11_LIBDIR ${DX11_LIBDIR}/x86)
endif ()

set(DX11_LIBNAME "d3d11" "d3dx11")
set(DX11_HEADER "d3d11.h" "d3dx11.h")

find_path(DX11_INCLUDE_DIR ${DX11_HEADER}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/DirectX/Include"
    NO_DEFAULT_PATH)

find_library(DX11_LIBRARY ${DX11_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/DirectX/Lib/${DX11_LIBDIR}"
    NO_DEFAULT_PATH)

if (DX11_LIBRARY AND DX11_INCLUDE_DIR)
    set(DX11_FOUND "YES")
    message(STATUS "Found DX11: ${DX11_LIBRARY}")
else ()
    set(DX11_FOUND "NO")
    message(STATUS "Not found DX11 in ${DX11_SEARCH_PATHS}")
endif ()
