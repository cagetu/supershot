# Locate Spir-V Reflect
#
# This module defines:
# SPIRVREFLECT_INCLUDE_DIR = where to find the headers
# SPIRVREFLECT_LIBRARY = where to Spir-V Reflect library exist
# SPIRVREFLECT_FOUND = YES or NO
#

if (WIN32)
    set(SPIRVREFLECT_LIBDIR "")
elseif (APPLE)
    set(SPIRVREFLECT_LIBDIR "macOS")
elseif (LINUX)
    set(SPIRVREFLECT_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(SPIRVREFLECT_LIBDIR ${SPIRVREFLECT_LIBDIR}/x64)
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(SPIRVREFLECT_LIBDIR ${SPIRVREFLECT_LIBDIR}/x64)
else ()
    set(SPIRVREFLECT_LIBDIR ${SPIRVREFLECT_LIBDIR}/x86)
endif ()

set(SPIRVREFLECT_LIBNAME "spirv-reflect")

find_path(SPIRVREFLECT_INCLUDE_DIR "spirv_reflect.h"
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/spirv-reflect"
    NO_DEFAULT_PATH)

find_library(SPIRVREFLECT_LIBRARY ${SPIRVREFLECT_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/spirv-reflect/lib/${SPIRVREFLECT_LIBDIR}"
    NO_DEFAULT_PATH)

if (SPIRVREFLECT_LIBRARY AND SPIRVREFLECT_INCLUDE_DIR)
    set(SPIRVREFLECT_FOUND "YES")
    message(STATUS "Found SpirvReflect: ${SPIRVREFLECT_LIBRARY}")
else ()
    set(SPIRVREFLECT_FOUND "NO")
    message(STATUS "Not found SpirvReflect in ${SPIRVREFLECT_SEARCH_PATHS}")
endif ()
