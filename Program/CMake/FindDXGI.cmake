# Locate DXGI
#
# This module defines:
# DXGI_INCLUDE_DIR = where to find the headers
# DXGI_LIBRARY = where to DXGI library exist
# DXGI_FOUND = YES or NO
#

if (WIN32)
    set(DXGI_LIBDIR "")
elseif (APPLE)
    set(DXGI_LIBDIR "macOS")
elseif (LINUX)
    set(DXGI_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(DXGI_LIBDIR ${DXGI_LIBDIR}/x64)
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(DXGI_LIBDIR ${DXGI_LIBDIR}/x64)
else ()
    set(DXGI_LIBDIR ${DXGI_LIBDIR}/x86)
endif ()

set(DXGI_LIBNAME "dxgi")

find_path(DXGI_INCLUDE_DIR "DXGI.h"
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/DirectX/Include"
    NO_DEFAULT_PATH)

find_library(DXGI_LIBRARY ${DXGI_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/DirectX/Lib/${DXGI_LIBDIR}"
    NO_DEFAULT_PATH)

if (DXGI_LIBRARY AND DXGI_INCLUDE_DIR)
    set(DXGI_FOUND "YES")
    message(STATUS "Found DXGI: ${DXGI_LIBRARY}")
else ()
    set(DXGI_FOUND "NO")
    message(STATUS "Not found DXGI in ${DXGI_SEARCH_PATHS}")
endif ()
