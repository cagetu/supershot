# Locate SHADERC
#
# This module defines:
# SHADERC_INCLUDE_DIR = where to find the headers
# SHADERC_LIBRARY = where to SHADERC library exist
# SHADERC_FOUND = YES or NO
#

if (WIN32)
    set(SHADERC_LIBDIR "")
elseif (APPLE)
    set(SHADERC_LIBDIR "macOS")
elseif (LINUX)
    set(SHADERC_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(SHADERC_LIBDIR ${SHADERC_LIBDIR}/x64)
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(SHADERC_LIBDIR ${SHADERC_LIBDIR}/x64)
else ()
    set(SHADERC_LIBDIR ${SHADERC_LIBDIR}/x86)
endif ()

# shaderc
set(SHADERC_LIBNAME "shaderc_combined")

find_path(SHADERC_INCLUDE_DIR "shaderc.h"
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/shaderc/libshaderc/include"
    NO_DEFAULT_PATH)

find_library(SHADERC_LIBRARY ${SHADERC_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/shaderc/libshaderc/lib/${SHADERC_LIBDIR}"
    NO_DEFAULT_PATH)

if (SHADERC_LIBRARY AND SHADERC_INCLUDE_DIR)
    set(SHADERC_FOUND "YES")
    message(STATUS "Found SHADERC: ${SHADERC_LIBRARY}")
else ()
    set(SHADERC_FOUND "NO")
    message(STATUS "Not found SHADERC in ${SHADERC_SEARCH_PATHS}")
endif ()

# shaderc_util
set(SHADERC_UTIL_LIBNAME "shaderc_util")

find_path(SHADERC_UTIL_INCLUDE_DIR "spirv_tools_wrapper.h"
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/shaderc/libshaderc_util/include"
    NO_DEFAULT_PATH)

find_library(SHADERC_UTIL_LIBRARY ${SHADERC_UTIL_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/shaderc/libshaderc_util/lib/${SHADERC_LIBDIR}"
    NO_DEFAULT_PATH)

if (SHADERC_UTIL_LIBRARY AND SHADERC_UTIL_INCLUDE_DIR)
    set(SHADERC_UTIL_FOUND "YES")
    message(STATUS "Found SHADERC_UTIL: ${SHADERC_UTIL_LIBRARY}")
else ()
    set(SHADERC_UTIL_FOUND "NO")
    message(STATUS "Not found SHADERC_UTIL in ${SHADERC_UTIL_SEARCH_PATHS}")
endif ()
