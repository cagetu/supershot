# Locate VULKAN
#
# This module defines:
# VULKAN_INCLUDE_DIR = where to find the headers
# VULKAN_LIBRARY = where to OpenGL ES3 library exist
# VULKAN_FOUND = YES or NO
#

if (WIN32)

  if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(VULKAN_LIBDIR "")
  elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
    set(VULKAN_LIBDIR "32")
  endif ()
  
elseif (APPLE)
    set(VULKAN_LIBDIR "macOS")
elseif (LINUX)
    set(VULKAN_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(VULKAN_LIBDIR ${VULKAN_LIBDIR})
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(VULKAN_LIBDIR ${VULKAN_LIBDIR})
else ()
    set(VULKAN_LIBDIR ${VULKAN_LIBDIR})
endif ()

set(VULKAN_LIBNAME "vulkan-1")
set(SHADERC_LIBNAME "shaderc_combined")
set(VKLAYER_LIBNAME "VkLayer_utils")

find_path(VULKAN_INCLUDE_DIR 
		"vulkan/vulkan.h"
		PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/Vulkan/Include"
		NO_DEFAULT_PATH)

find_library(VULKAN_LIBRARY 
		${VULKAN_LIBNAME}
		PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/Vulkan/Lib${VULKAN_LIBDIR}"
		NO_DEFAULT_PATH)
find_library(SHADERC_LIBRARY 
		${SHADERC_LIBNAME}
		PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/Vulkan/Lib${VULKAN_LIBDIR}"
		NO_DEFAULT_PATH)
find_library(VKLAYER_LIBRARY 
		${VKLAYER_LIBNAME}
		PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/Vulkan/Lib${VULKAN_LIBDIR}"
		NO_DEFAULT_PATH)

if (VULKAN_LIBRARY AND VULKAN_INCLUDE_DIR)
    set(VULKAN_FOUND "YES")
    message(STATUS "Found VULKAN: ${VULKAN_LIBRARY}")
else ()
    set(VULKAN_FOUND "NO")
    message(STATUS "Not found VULKAN in ${VULKAN_SEARCH_PATHS}")
endif ()
