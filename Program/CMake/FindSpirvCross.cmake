# Locate Spir-V Cross
#
# This module defines:
# SPIRVCROSS_INCLUDE_DIR = where to find the headers
# SPIRVCROSS_LIBRARY = where to Spir-V Cross library exist
# SPIRVCROSS_FOUND = YES or NO
#

if (WIN32)
    set(SPIRVCROSS_LIBDIR "")
elseif (APPLE)
    set(SPIRVCROSS_LIBDIR "macOS")
elseif (LINUX)
    set(SPIRVCROSS_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(SPIRVCROSS_LIBDIR ${SPIRVCROSS_LIBDIR}/x64)
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(SPIRVCROSS_LIBDIR ${SPIRVCROSS_LIBDIR}/x64)
else ()
    set(SPIRVCROSS_LIBDIR ${SPIRVCROSS_LIBDIR}/x86)
endif ()

set(SPIRVCROSS_LIBNAME "spirv-cross")

find_path(SPIRVCROSS_INCLUDE_DIR "spirv_cross.hpp"
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/spirv-cross"
    NO_DEFAULT_PATH)

find_library(SPIRVCROSS_LIBRARY ${SPIRVCROSS_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/spirv-cross/lib/${SPIRVCROSS_LIBDIR}"
    NO_DEFAULT_PATH)

if (SPIRVCROSS_LIBRARY AND SPIRVCROSS_INCLUDE_DIR)
    set(SPIRVCROSS_FOUND "YES")
    message(STATUS "Found SpirvCross: ${SPIRVCROSS_LIBRARY}")
else ()
    set(SPIRVCROSS_FOUND "NO")
    message(STATUS "Not found SpirvCross in ${SPIRVCROSS_SEARCH_PATHS}")
endif ()
