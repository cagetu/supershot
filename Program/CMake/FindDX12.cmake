# Locate DX12
#
# This module defines:
# DX12_INCLUDE_DIR = where to find the headers
# DX12_LIBRARY = where to DX12 library exist
# DX12_FOUND = YES or NO
#

if (WIN32)
    set(DX12_LIBDIR "")
elseif (APPLE)
    set(DX12_LIBDIR "macOS")
elseif (LINUX)
    set(DX12_LIBDIR "Linux")
endif ()

if (APPLE)
    # do nothing
elseif (CMAKE_CL_64)
    set(DX12_LIBDIR ${DX12_LIBDIR}/x64)
elseif (CMAKE_COMPILER_IS_GNUCXX AND CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(DX12_LIBDIR ${DX12_LIBDIR}/x64)
else ()
    set(DX12_LIBDIR ${DX12_LIBDIR}/x86)
endif ()

set(DX12_LIBNAME "d3d12" "d3dx12")
set(DX12_HEADER "d3d12.h" "d3dx12.h")

find_path(DX12_INCLUDE_DIR ${DX12_HEADER}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/DirectX/Include"
    NO_DEFAULT_PATH)

find_library(DX12_LIBRARY ${DX12_LIBNAME}
    PATHS "${ENGINE_SOURCE_DIR}/ThirdParty/DirectX/Lib/${DX12_LIBDIR}"
    NO_DEFAULT_PATH)

if (DX12_LIBRARY AND DX12_INCLUDE_DIR)
    set(DX12_FOUND "YES")
    message(STATUS "Found DX12: ${DX12_LIBRARY}")
else ()
    set(DX12_FOUND "NO")
    message(STATUS "Not found DX12 in ${DX12_SEARCH_PATHS}")
endif ()
