struct VSInput
{
    float4 Position : POSITION0;
    float4 Normal : NORMAL0;
    float4 TexCoord : TEXCOORD0;
    float4 Color : COLOR0;
};
 
struct VSOutput
{
    float4 Position : POSITION0;
    float4 Normal : NORMAL0;
    float4 TexCoord : TEXCOORD0;
    float4 Color : COLOR0;
};
 
VSOutput mainVS( VSInput In )
{
    VSOutput Out = ( VSOutput )0;
    Out.Position = In.Position;
    Out.Normal = In.Normal;
    Out.Color = In.Color;
	
	#if _USE_NORMALMAP
		Out.Color += float4(1, 0, 0 , 0);
	#endif
	
    return Out;
}
