#include "foundation.h"
#include "assignRegistry.h"
#include "Container\map.h"

#if _HAS_THREADS
#include <mutex>
static std::mutex lockMutex;
#define SCOPED_LOCK std::lock_guard<std::mutex> lock(lockMutex)
#else
#define SCOPED_LOCK
#endif

static Map<String, String> GAssigns;

void AssignRegistry::_Startup()
{
}

void AssignRegistry::_Shutdown()
{
	GAssigns.Clear();
}

void AssignRegistry::Register(const String& assign, const String& path)
{
	SCOPED_LOCK
	if (GAssigns.Contains(assign))
	{
		GAssigns[assign] = path;
	}
	else
	{
		GAssigns.Insert(assign, path);
	}
}

void AssignRegistry::Unregister(const String& assign)
{
	SCOPED_LOCK
	GAssigns.Remove(assign);
}

bool AssignRegistry::Has(const String& assign)
{
	SCOPED_LOCK
	return GAssigns.Contains(assign);
}

String AssignRegistry::Find(const String& assign)
{
	SCOPED_LOCK
	IndexT index = GAssigns.FindIndex(assign);
	if (index == INVALID_INDEX)
		return String();

	return GAssigns.ValueAt(index);
}

String AssignRegistry::Resolve(const String& uri)
{
	SCOPED_LOCK
	String result = uri;

	IndexT index = -1;
	while ((index = result.findFirst(TEXT(':'))) > 0)
	{
		if (index > 1)
		{
			String assign = result.substr(0, index);

			if (Has(assign))
			{
				String postStr = result.substr(index + 1, result.size() - (index + 1));
				String replace = Find(assign);

				if (!replace.empty())
				{
					if (replace[replace.size() - 1] != TEXT(':')
						&& (replace[replace.size() - 1] != TEXT('/')
							|| replace[replace.size() - 2] != TEXT(':')))
					{
						replace.append(TEXT("/"));
					}
					replace.append(postStr);
				}
				result = replace;
			}
			else
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
	if (index < 0)
	{
		String replace = Find(result);

		if (!replace.empty())
		{
			if (replace[replace.size() - 1] != TEXT(':')
				&& (replace[replace.size() - 1] != TEXT('/')
					|| replace[replace.size() - 2] != TEXT(':')))
			{
				replace.append(TEXT("/"));
			}
		}
		result = replace;
	}

	result.replace(TEXT('\\'), TEXT('/'));
	result.trim(TEXT("/"));

	return result;
}
