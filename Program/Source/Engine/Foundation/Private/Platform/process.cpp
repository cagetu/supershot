#include "foundation.h"

/*	외부 Process (.exe, .bat) 파일을 호출하기 위한 Process 처리기
	- Process 
*/
ProcHandle CommonPlatformProcess::CreateProc(const TCHAR* URL, const TCHAR* Parms, bool bLaunchDetached, bool bLaunchHidden, bool bLaunchReallyHidden, uint32* OutProcessID, int32 PriorityModifier, const TCHAR* OptionalWorkingDirectory, void* PipeWriteChild, void * PipeReadChild)
{
	return ProcHandle();
}

ProcHandle CommonPlatformProcess::OpenProcess(uint32 ProcessID)
{
	return ProcHandle();
}

bool CommonPlatformProcess::IsProcRunning(ProcHandle & ProcessHandle)
{
	return false;
}

void CommonPlatformProcess::WaitForProc(ProcHandle & ProcessHandle)
{
}

void CommonPlatformProcess::CloseProc(ProcHandle & ProcessHandle)
{
}

void CommonPlatformProcess::TerminateProc(ProcHandle & ProcessHandle, bool KillTree)
{
}

