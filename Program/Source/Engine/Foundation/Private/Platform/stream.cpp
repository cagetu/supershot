#include "foundation.h"

FileStream::FileStream()
	: m_FileHandle(NULL)
	, m_Mode(ReadOnly)
	, m_Length(0)
	, m_CurPos(-1)
{
}
FileStream::FileStream(const TCHAR* filename, Mode mode)
{
	Open(filename, mode);
}

FileStream::FileStream(FILE* fp, int32 off, int32 len, Mode mode)
{
	m_Mode = mode;
	m_FileHandle = fp;
	m_CurPos = off;
	if (len == 0)
	{
		PlatformUtil::fseek(fp, 0, SEEK_END);
		m_Length = ftell(fp) - m_CurPos;
	}
	else
	{
		m_Length = len;
	}
	PlatformUtil::fseek(fp, off, SEEK_SET);
}

FileStream::~FileStream()
{
	if (m_FileHandle)
	{
		PlatformUtil::fclose(m_FileHandle);
	}
	m_FileHandle = NULL;
}

bool FileStream::Open(const TCHAR* filename, Mode mode)
{
	TCHAR* m;
	if (mode == FileIO::ReadOnly) m = TEXT("rb");
	else if (mode == FileIO::WriteOnly)	m = TEXT("wb");
	else
	{
		assert(0);
		return nullptr;
	}

	FILE* fp = PlatformUtil::fopen(filename, m);
	if (fp)
	{
		m_FileHandle = fp;
		return true;
	}

	return false;
}

void FileStream::Close()
{
	if (m_FileHandle)
	{
		PlatformUtil::fclose(m_FileHandle);
	}
}

int32 FileStream::ReadBytes(uint8* output, int32 size)
{
	return (int32)PlatformUtil::fread(m_FileHandle, output, sizeof(uint8), size);
}

int32 FileStream::WriteBytes(const uint8* input, int32 size)
{
	return (int32)PlatformUtil::fwrite(m_FileHandle, (void*)input, sizeof(TCHAR), size);
}

int32 FileStream::Seek(int32 offset, _SEEK pos)
{
	switch (pos)
	{
	case _SET:
		PlatformUtil::fseek(m_FileHandle, m_CurPos + offset, SEEK_SET);
		break;
	case _CUR:
		PlatformUtil::fseek(m_FileHandle, offset, SEEK_CUR);
		break;
	case _END:
		PlatformUtil::fseek(m_FileHandle, m_CurPos + m_Length + offset, SEEK_SET);
		break;
	}
	return PlatformUtil::ftell(m_FileHandle) - m_CurPos;
}

int32 FileStream::Tell()
{
	return PlatformUtil::ftell(m_FileHandle);
}

void FileStream::Flush()
{
	PlatformUtil::fflush(m_FileHandle);
}

MemoryStream::MemoryStream()
	: m_Data(NULL)
    , m_Length(0)
	, m_CurPos(0)
	, m_bAutoDestroy(false)
{
}

MemoryStream::MemoryStream(uint8* buffer, int32 length, bool bAutoDestroy)
	: m_Data(buffer)
	, m_CurPos(0)
    , m_Length(0)
	, m_bAutoDestroy(bAutoDestroy)
{
}

MemoryStream::~MemoryStream()
{
	if (m_bAutoDestroy)
    {
        DeletePOD(m_Data);
    }
	m_Data = NULL;
	m_CurPos = 0;
    m_Length = 0;
}

int32 MemoryStream::ReadBytes(uint8* output, int32 size)
{
	if (output == NULL)
		return 0;

	Memory::MemCpy(output, CurrentData(), size);
	m_CurPos += size;

	return size;
}

int32 MemoryStream::WriteBytes(const uint8* input, int32 size)
{
	if (input == NULL)
		return 0;

	Memory::MemCpy(CurrentData(), input, size);
	m_CurPos += size;

	return size;
}

int32 MemoryStream::Seek(int32 offset, _SEEK mode)
{
    switch (mode)
    {
    case _SET:
        m_CurPos = offset;
        break;
    case _CUR:
        m_CurPos += offset;
        break;
    case _END:
        m_CurPos = m_Length + offset;
        break;
    }
    return m_CurPos;
}

uint8* MemoryStream::CurrentData()
{
	return m_Data + m_CurPos;
}
