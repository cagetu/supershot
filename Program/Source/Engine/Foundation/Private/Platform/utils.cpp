#include "foundation.h"
#include <io.h>

const TCHAR* CommonPlatformUtil::GetSystemErrorMessage(TCHAR* OutBuffer, int32 BufferCount, int32 Error)
{
	const TCHAR* Message = TEXT("No system errors available on this platform.");
	ASSERT(OutBuffer && BufferCount > 80);
	Error = 0;
	Strcpy(OutBuffer, BufferCount - 1, Message);
	return OutBuffer;
}

void CommonPlatformUtil::MemMove(void* dest, const void* src, size_t size)
{
	::memmove(dest, src, size);
}

void CommonPlatformUtil::MemCpy(void* dest, const void* src, size_t size)
{
	::memcpy(dest, src, size);
}

void CommonPlatformUtil::MemSet(void* dest, int32 val, size_t size)
{
	::memset(dest, val, size);
}

int32 CommonPlatformUtil::MemCmp(const void* buf1, const void* buf2, size_t size)
{
	return ::memcmp(buf1, buf2, size);
}

void CommonPlatformUtil::MemZero(void* dest, size_t size)
{
	::memset(dest, 0, size);
}

template <typename T>
static FORCEINLINE void Valswap(T& A, T& B)
{
	// Usually such an implementation would use move semantics, but
	// we're only ever going to call it on fundamental types and MoveTemp
	// is not necessarily in scope here anyway, so we don't want to
	// #include it if we don't need to.
	T Tmp = A;
	A = B;
	B = Tmp;
}

FILE * CommonPlatformUtil::fopen(const TCHAR* fileName, const TCHAR* mode)
{
	return NULL;
}

void CommonPlatformUtil::fclose(FILE * fp)
{
	CHECK(fp, TEXT("fp == NULL"));
	::fclose(fp);
}

int32 CommonPlatformUtil::remove(const TCHAR* fileName)
{
	return -1;
}

int32 CommonPlatformUtil::rename(const TCHAR* oldFileName, const TCHAR* newFileName)
{
	return -1;
}

int32 CommonPlatformUtil::access(const TCHAR* fileName, int32 accmode)
{
	return -1;
}

int32 CommonPlatformUtil::fwrite(FILE* fp, void* buffer, int32 elementSize, int32 elementCount)
{
	return (int32)::fwrite(buffer, elementSize, elementCount, fp);
}

int32 CommonPlatformUtil::fread(FILE* fp, void* buffer, int32 elementSize, int32 elementCount)
{
	return (int32)::fread(buffer, elementSize, elementCount, fp);
}

int32 CommonPlatformUtil::fseek(FILE* fp, int32 offset, int32 mode)
{
	return ::fseek(fp, offset, mode);
}

int32 CommonPlatformUtil::ftell(FILE* fp)
{
	return ::ftell(fp);
}

void CommonPlatformUtil::fflush(FILE* fp)
{
	::fflush(fp);
}

void CommonPlatformUtil::Sleep(float sec)
{
	int32 milliSecs = int32(sec * 1000.0);
	::Sleep(milliSecs);
}

void CommonPlatformUtil::Exit(int32 exitCode)
{
	::exit(exitCode);
}

unsigned long CommonPlatformUtil::GetTick()
{
#/*
ifdef WIN32
	static unsigned long pivottick = timeGetTime();
	return timeGetTime() - pivottick;
#elif defined( __APPLE__ )
	static double pivottick = CFAbsoluteTimeGetCurrent();
	return (unsigned long)((CFAbsoluteTimeGetCurrent() - pivottick) * 1000);
#else
	struct timespec ctimespec;
	clock_gettime(CLOCK_REALTIME, &ctimespec);
	static int pivottick = ctimespec.tv_sec * 1000;

	int ns = (int)((double)ctimespec.tv_nsec / (double)1000000);

	return (ctimespec.tv_sec * 1000 + ns) - pivottick;
#endif
*/

	return 0;
}