#include "foundation.h"

AndroidCriticalSection::AndroidCriticalSection()
{
	pthread_mutex_init(&m_Mutex, NULL);
}

AndroidCriticalSection::~AndroidCriticalSection()
{
	pthread_mutex_destroy(&m_Mutex);
}

void AndroidCriticalSection::Lock()
{
	pthread_mutex_lock(&m_Mutex);
}

void AndroidCriticalSection::Unlock()
{
	pthread_mutex_unlock(&m_Mutex);
}