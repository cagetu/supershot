#include "foundation.h"

void AndroidSys::DebugOut(const wchar* msg)
{
	char message[256];
	unicode::convert(message, sizeof(message), msg);

	// Builds for distribution should not have logging in them:
	// http://developer.android.com/tools/publishing/preparing.html#publishing-configure
	__android_log_print(ANDROID_LOG_DEBUG, "VISUALSHOCK", "%s", message);
}

void AndroidSys::DebugOut(const char* msg)
{
	// Builds for distribution should not have logging in them:
	// http://developer.android.com/tools/publishing/preparing.html#publishing-configure
	__android_log_print(ANDROID_LOG_DEBUG, "VISUALSHOCK", "%s", msg);
}

void AndroidSys::DebugOutFormat(const wchar* format, ...)
{
	wchar logtxt[1024] = { 0, };
	va_list ap;
	va_start(ap, format);
	unicode::_sprintf(logtxt, _countof(logtxt), format, ap);
	va_end(ap);

	// Builds for distribution should not have logging in them:
	// http://developer.android.com/tools/publishing/preparing.html#publishing-configure
	__android_log_print(ANDROID_LOG_DEBUG, "VISUALSHOCK", "%s", logtxt);
}

void AndroidSys::DebugOutFormat(const char* format, ...)
{
	char logtxt[1024] = { 0, };
	va_list ap;
	va_start(ap, format);
	vsprintf(logtxt, format, ap);
	va_end(ap);

	// Builds for distribution should not have logging in them:
	// http://developer.android.com/tools/publishing/preparing.html#publishing-configure
	__android_log_print(ANDROID_LOG_DEBUG, "VISUALSHOCK", "%s", logtxt);
}

FILE * AndroidSys::fopen(const wchar * fname, const wchar * m, bool jet)
{
	// android rule : 1) read ext path --> 2) read assets (apk)
	FILE *fp = NULL;

#if 1
	char _extfullpath[256] = { 0, };
	unicode::convert(_extfullpath, sizeof(_extfullpath), fname);
	char _mode[16] = { 0, };
	unicode::convert(_mode, sizeof(_mode), m);
	// 1) Try to read from Ext sdcard
	fp = ::fopen(_extfullpath, _mode);
	if (fp)
	{
#ifdef _DEBUG
		__android_log_print(ANDROID_LOG_INFO, "ENGINE-LOG", "--> Open from SDCARD %s (%s) OK!", _extfullpath, _mode);
#endif
		return fp;
	}

	return fp;
#else
	string extfullpath;

	if (jet)
		extfullpath = Platform::m_cachePath + L"/" + fname + L".jet";
	else
		extfullpath = Platform::m_cachePath + L"/" + fname;

	extfullpath.make_lower();

	char _extfullpath[256] = { 0, };
	unicode::convert(_extfullpath, sizeof(_extfullpath), extfullpath.c_str());
	char _mode[16] = { 0, };
	unicode::convert(_mode, sizeof(_mode), m);

	// 1) Try to read from Ext sdcard
	fp = ::fopen(_extfullpath, _mode);
	if (fp)
	{
#ifdef _DEBUG
		__android_log_print(ANDROID_LOG_INFO, "ENGINE-LOG", "--> Open from SDCARD %s (%s) OK!", _extfullpath, _mode);
#endif
		return fp;
	}

	// assets read only!
	//assert( m[0] == 'r' );

	// 2) Try to read from APK (Assets)
	char _assetbuffer[256] = { 0, };

	// .jet 과 같은 이름을 추가
	string fullassetname = fullassetname = string(fname);

	fullassetname.make_lower();
	unicode::utf16toutf8(_assetbuffer, sizeof(_assetbuffer), fullassetname.c_str());

	AAsset* asset = AAssetManager_open(Platform::m_assets, _assetbuffer, AASSET_MODE_UNKNOWN);
	if (asset == NULL)
	{
#ifdef _DEBUG
		__android_log_print(ANDROID_LOG_ERROR, "ENGINE-LOG", "--> Not found %s from Android Assets..(fopen)", _assetbuffer);
#endif
		return NULL;
	}

	off_t outStart, outLength;
	int fd = AAsset_openFileDescriptor(asset, &outStart, &outLength);
	if (fd >= 0)
	{
		fp = fdopen(fd, "r");
#ifdef _DEBUG
		__android_log_print(ANDROID_LOG_DEBUG, "ENGINE-LOG", "--> Open from Assets %s OK!", _assetbuffer);
#endif
	}
	else
	{
#ifdef _DEBUG		
		__android_log_print(ANDROID_LOG_ERROR, "ENGINE-LOG", "--> Open Android Assets Error: %s", _assetbuffer);
#endif
	}

	AAsset_close(asset);
	fseek(fp, outStart, SEEK_SET);
	return fp;
}

void AndroidSys::fclose(FILE * fp)
{
	::fclose(fp);
}

int AndroidSys::remove(const char* fname)
{
	return ::remove(fname);
}

int AndroidSys::remove(const wchar* fname)
{
	char f[256];
	unicode::convert(f, _countof(f), fname);
	return ::remove(f);
}

int AndroidSys::rename(const char* foldname, const char* fnewname)
{
	return ::rename(foldname, fnewname);
}

int AndroidSys::rename(const wchar* foldname, const wchar* fnewname)
{
	char fo[256];
	char fn[256];
	unicode::convert(fo, _countof(fo), foldname);
	unicode::convert(fn, _countof(fn), fnewname);
	return ::rename(fo, fn);
}

int AndroidSys::access(const char* fname, int accmode)
{
	return ::_access(fname, accmode);
}

int AndroidSys::access(const wchar* fname, int accmode)
{
	char f[256];
	unicode::convert(f, _countof(f), fname);
	return ::access(f, accmode);
}
