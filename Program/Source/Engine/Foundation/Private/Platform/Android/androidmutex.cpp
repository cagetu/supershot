#include "foundation.h"

AndroidMutex::AndroidMutex()
{
	pthread_mutex_init(&m_Mutex, NULL);
}

AndroidMutex::~AndroidMutex()
{
	pthread_mutex_destroy(&m_Mutex);
}

void AndroidMutex::Lock()
{
	pthread_mutex_lock(&m_Mutex);
}

void AndroidMutex::Unlock()
{
	pthread_mutex_unlock(&m_Mutex);
}