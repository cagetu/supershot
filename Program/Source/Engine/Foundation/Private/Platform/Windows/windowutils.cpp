#include "foundation.h"
#include <io.h>

const TCHAR* WindowPlatformUtil::GetSystemErrorMessage(TCHAR* OutBuffer, int32 BufferCount, int32 Error)
{
	ASSERT(OutBuffer && BufferCount);
	*OutBuffer = TEXT('\0');
	if (Error == 0)
	{
		Error = GetLastError();
	}
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, Error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), OutBuffer, BufferCount, NULL);
	TCHAR* Found = Strchr(OutBuffer, TEXT('\r'));
	if (Found)
	{
		*Found = TEXT('\0');
	}
	Found = Strchr(OutBuffer, TEXT('\n'));
	if (Found)
	{
		*Found = TEXT('\0');
	}
	return OutBuffer;
}

void WindowPlatformUtil::DebugOut(const TCHAR* format, ...)
{
	TCHAR temp[1024];
	va_list ap;
	int r;
	va_start(ap, format);
#if _UNICODE
	r = vswprintf(temp, _countof(temp), format, ap);
#else
	r = vsprintf(temp, _countof(temp), format, ap);
#endif
	va_end(ap);

	OutputDebugString(temp);
}

int WindowPlatformUtil::Dialog(const TCHAR* text, const TCHAR* caption, int type)
{
#if _UNICODE
	return ::MessageBoxW(NULL, text, caption, type);
#else
	return ::MessageBoxA(NULL, text, caption, type);
#endif
}

void WindowPlatformUtil::Prefetch(void* p, int32 offset)
{
	_mm_prefetch((const char*)((char*)p + offset), _MM_HINT_T0);
}

FILE * WindowPlatformUtil::fopen(const TCHAR* fileName, const TCHAR* mode)
{
	FILE *fp = NULL;
#if _UNICODE
	::_wfopen_s(&fp, fileName, mode);
#else
	::fopen_s(&fp, fileName, mode);
#endif
	return fp;
}

void WindowPlatformUtil::fclose(FILE * fp)
{
	::fclose(fp);
}

int32 WindowPlatformUtil::remove(const TCHAR* fileName)
{
#if _UNICODE
	return ::_wremove(fileName);
#else
	return ::remove(fileName);
#endif
}

int32 WindowPlatformUtil::rename(const TCHAR* oldFileName, const TCHAR* newFileName)
{
#if _UNICODE
	return ::_wrename(oldFileName, newFileName);
#else
	return ::rename(oldFileName, newFileName);
#endif
}

int32 WindowPlatformUtil::access(const TCHAR* fileName, int32 accmode)
{
#if _UNICODE
	return ::_waccess(fileName, accmode);
#else
	return ::access(fileName, accmode);
#endif
}

void WindowPlatformUtil::Sleep(float sec)
{
	DWORD milliSecs = DWORD(sec * 1000.0);
	::Sleep(milliSecs);
}

bool WindowPlatformUtil::IsExistFile(const TCHAR* path)
{
	WIN32_FIND_DATA fd;
	HANDLE h = FindFirstFile(path, &fd);

	return (h != INVALID_HANDLE_VALUE) ? true : false;
}

void WindowPlatformUtil::FindFiles(Array<String>& list, const TCHAR* path, const TCHAR* filter, bool recur)
{
	if (path == NULL)
	{
		FindFiles(list, TEXT(""), filter, recur);
		return;
	}

	WIN32_FIND_DATA fd;
	HANDLE h = FindFirstFile(path[0] == '\0' ? filter : unicode::format(TEXT("%s\\%s"), path, filter).c_str(), &fd);

	if (h != INVALID_HANDLE_VALUE)
	{
		do {
			if (!(fd.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
			{
				list.Add(path[0] == '\0' ? fd.cFileName : unicode::format(L"%s\\%s", path, fd.cFileName));
			}
			else if (recur == true && unicode::strcmp(fd.cFileName, L".") != 0 && unicode::strcmp(fd.cFileName, L"..") != 0)
			{
				FindFiles(list, path[0] == '\0' ? fd.cFileName : unicode::format(TEXT("%s\\%s"), path, fd.cFileName).c_str(), filter, recur);
			}
		} while (FindNextFile(h, &fd) == TRUE);
		FindClose(h);
	}
}

unsigned long WindowPlatformUtil::GetTick()
{
	static unsigned long pivottick = timeGetTime();
	return timeGetTime() - pivottick;
}