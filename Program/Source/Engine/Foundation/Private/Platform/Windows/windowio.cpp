#include "foundation.h"

////////////////////////////////////////////////////////////////////////////


MemoryMappedIO::MemoryMappedIO(HANDLE hFile, HANDLE hMapFile, int32 len)
	: MemoryIO(NULL, len, false)
{
	m_hFile = hFile;
	m_hMapFile = hMapFile;
	m_pAddress = NULL;
}

MemoryMappedIO::~MemoryMappedIO()
{
	Unload();
	if (m_hMapFile != NULL)
		CloseHandle(m_hMapFile);
	if (m_hFile != NULL)
		CloseHandle(m_hFile);
}

MemoryMappedIO* MemoryMappedIO::Open(const wchar* fname)
{
	HANDLE hFile = CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hFile == nullptr)
		return nullptr;

	LARGE_INTEGER fileSize;
	if (GetFileSizeEx(hFile, &fileSize) == FALSE)
	{
		CloseHandle(hFile);
		return nullptr;
	}

	HANDLE hMapFile = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
	if (hMapFile == nullptr)
	{
		CloseHandle(hFile);
		return nullptr;
	}
	return new MemoryMappedIO(hFile, hMapFile, fileSize.LowPart);
}

MemoryMappedIO* MemoryMappedIO::Close(MemoryMappedIO* file)
{
	delete file;
	return nullptr;
}

int32 MemoryMappedIO::Read(uint8 * output, int32 len)
{
	if (m_pAddress == NULL)
		Load(0, m_Length);

	int32 readBytes = MemoryIO::Read(output, len);
	if (m_Offset == m_Length)
		Unload();

	return readBytes;
}

bool MemoryMappedIO::Load(uint32 offset, uint32 length)
{
	unsigned __int64 alignedoff = offset & (~0xFFFFLL);

	m_pAddress = MapViewOfFile(m_hMapFile, FILE_MAP_READ, (uint32)(alignedoff >> 32), (uint32)alignedoff, (unsigned int)(offset + length - alignedoff));
	m_Data = (const uint8*)m_pAddress + (offset - alignedoff);
	return m_pAddress != NULL ? true : false;
}

void MemoryMappedIO::Unload()
{
	if (m_pAddress != NULL)
		UnmapViewOfFile(m_pAddress);
	m_pAddress = NULL;
}

////////////////////////////////////////////////////////////////////////////


SyncedIO::SyncedIO(IArchive *f, int32 off, int32 len, CriticalSection* cs)
{
	m_fp = f;
	m_Pivot = off;
	m_Length = len;
	m_Offset = 0;
	m_CS = cs;
}

int32 SyncedIO::Read(uint8 * output, int32 len)
{
	if (m_Offset + len > m_Length)
		return Read(output, m_Length - m_Offset);

	if (len > 0)
	{
		CSScopeLock lock(*m_CS);

		m_fp->Seek(m_Pivot + m_Offset, IArchive::_SET);
		m_fp->Read(output, len);
		m_Offset += len;
	}
	return len;
}

int32 SyncedIO::Seek(int32 offset, _SEEK pos)
{
	CSScopeLock lock(*m_CS);

	switch (pos)
	{
	case _SET:
		m_Offset = offset;
		break;
	case _CUR:
		m_Offset += offset;
		break;
	case _END:
		m_Offset = m_Length + offset;
		break;
	}
	return m_Offset;
}

////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////

/*
CAsyncStdIO::CAsyncStdIO(void* handle, int32 len)
{
	m_Offset = 0;
	m_Length = len;
	m_Handle = handle;
}

CAsyncStdIO::~CAsyncStdIO()
{
	if (m_Handle != NULL)
		Loader().Release(m_Handle);
}

CAsyncStdIO* CAsyncStdIO::Open(const wchar * fname)
{
#ifdef WIN32
	IArchive* f = MemoryMappedIO::Open(fname);
#else
	IArchive* f = CStdIO::Open(fname);
#endif
	if (f)
	{
		void* handle = Loader().Insert(f, f->GetSize());
		return new CAsyncStdIO(handle, f->GetSize());
	}
	return NULL;
}

int32 CAsyncStdIO::Read(void * ptr, int32 len)
{
	int32 l = Loader().Read(ptr, m_Offset, len, m_Handle);
	m_Offset += l;
	return l;
}

int32 CAsyncStdIO::Seek(int32 offset, _SEEK pos)
{
	switch (pos)
	{
	case _SET: m_Offset = offset; break;
	case _CUR: m_Offset += offset; break;
	case _END: m_Offset = m_Length + offset; break;
	}
	return m_Offset;
}
*/