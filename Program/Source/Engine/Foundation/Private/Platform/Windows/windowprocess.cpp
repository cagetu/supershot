#include "foundation.h"
#include <shellapi.h>
#include <ShlObj.h>
#include <LM.h>
#include <Psapi.h>
#include <TlHelp32.h>

#pragma comment(lib, "psapi.lib")

ProcHandle WindowPlatformProcess::CreateProc(const TCHAR* URL, const TCHAR* Parms, bool bLaunchDetached, bool bLaunchHidden, bool bLaunchReallyHidden, uint32* OutProcessID, int32 PriorityModifier, const TCHAR* OptionalWorkingDirectory, void* PipeWriteChild, void * PipeReadChild)
{
	// initialize process attributes
	SECURITY_ATTRIBUTES Attr;
	Attr.nLength = sizeof(SECURITY_ATTRIBUTES);
	Attr.lpSecurityDescriptor = NULL;
	Attr.bInheritHandle = true;

	// initialize process creation flags
	uint32 CreateFlags = NORMAL_PRIORITY_CLASS;
	if (PriorityModifier < 0)
	{
		CreateFlags = (PriorityModifier == -1) ? BELOW_NORMAL_PRIORITY_CLASS : IDLE_PRIORITY_CLASS;
	}
	else if (PriorityModifier > 0)
	{
		CreateFlags = (PriorityModifier == 1) ? ABOVE_NORMAL_PRIORITY_CLASS : HIGH_PRIORITY_CLASS;
	}

	if (bLaunchDetached)
	{
		CreateFlags |= DETACHED_PROCESS;
	}

	// initialize window flags
	uint32 dwFlags = 0;
	uint16 ShowWindowFlags = SW_HIDE;
	if (bLaunchReallyHidden)
	{
		dwFlags = STARTF_USESHOWWINDOW;
	}
	else if (bLaunchHidden)
	{
		dwFlags = STARTF_USESHOWWINDOW;
		ShowWindowFlags = SW_SHOWMINNOACTIVE;
	}

	if (PipeWriteChild != nullptr || PipeReadChild != nullptr)
	{
		dwFlags |= STARTF_USESTDHANDLES;
	}

	// initialize startup info
	STARTUPINFO StartupInfo = {
		sizeof(STARTUPINFO),
		NULL, NULL, NULL,
		(::DWORD)CW_USEDEFAULT,
		(::DWORD)CW_USEDEFAULT,
		(::DWORD)CW_USEDEFAULT,
		(::DWORD)CW_USEDEFAULT,
		(::DWORD)0, (::DWORD)0, (::DWORD)0,
		(::DWORD)dwFlags,
		ShowWindowFlags,
		0, NULL,
		HANDLE(PipeReadChild),
		HANDLE(PipeWriteChild),
		HANDLE(PipeWriteChild)
	};

	// create the child process
	String CommandLine = Str::Printf(TEXT("\"%s\" %s"), URL, Parms);
	PROCESS_INFORMATION ProcInfo;

	if (!CreateProcess(NULL, (TCHAR*)CommandLine.c_str(), &Attr, &Attr, true, (::DWORD)CreateFlags, NULL, OptionalWorkingDirectory, &StartupInfo, &ProcInfo))
	{
		DWORD ErrorCode = GetLastError();

		TCHAR ErrorMessage[512];
		WindowPlatformUtil::GetSystemErrorMessage(ErrorMessage, 512, ErrorCode);

		ULOG(Log::_Warn, "CreateProc failed: %s (0x%08x)", ErrorMessage, ErrorCode);

		if (ErrorCode == ERROR_NOT_ENOUGH_MEMORY || ErrorCode == ERROR_OUTOFMEMORY)
		{
			// These errors are common enough that we want some available memory information
			//FPlatformMemoryStats Stats = FPlatformMemory::GetStats();
			//ULOG(Log::_Debug, "Mem used: %.2f MB, OS Free %.2f MB", Stats.UsedPhysical / 1048576.0f, Stats.AvailablePhysical / 1048576.0f);
		}
		ULOG(Log::_Warn, "URL: %s %s", URL, Parms);

		if (OutProcessID != nullptr)
		{
			*OutProcessID = 0;
		}

		return ProcHandle();
	}

	if (OutProcessID != nullptr)
	{
		*OutProcessID = ProcInfo.dwProcessId;
	}

	::CloseHandle(ProcInfo.hThread);

	return ProcHandle(ProcInfo.hProcess);
}

ProcHandle WindowPlatformProcess::OpenProcess(uint32 ProcessID)
{
	return ProcHandle(::OpenProcess(PROCESS_ALL_ACCESS, 0, ProcessID));
}

bool WindowPlatformProcess::IsProcRunning(ProcHandle & ProcessHandle)
{
	bool bApplicationRunning = true;
	uint32 WaitResult = ::WaitForSingleObject(ProcessHandle.Get(), 0);
	if (WaitResult != WAIT_TIMEOUT)
	{
		bApplicationRunning = false;
	}
	return bApplicationRunning;
}

void WindowPlatformProcess::WaitForProc(ProcHandle & ProcessHandle)
{
	::WaitForSingleObject(ProcessHandle.Get(), INFINITE);
}

void WindowPlatformProcess::CloseProc(ProcHandle & ProcessHandle)
{
	if (ProcessHandle.IsValid())
	{
		::CloseHandle(ProcessHandle.Get());
		ProcessHandle.Reset();
	}
}

void WindowPlatformProcess::TerminateProc(ProcHandle & ProcessHandle, bool KillTree)
{
	if (KillTree)
	{
		HANDLE SnapShot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

		if (SnapShot != INVALID_HANDLE_VALUE)
		{
			::DWORD ProcessId = ::GetProcessId(ProcessHandle.Get());

			PROCESSENTRY32 Entry;
			Entry.dwSize = sizeof(PROCESSENTRY32);

			if (::Process32First(SnapShot, &Entry))
			{
				do
				{
					if (Entry.th32ParentProcessID == ProcessId)
					{
						HANDLE ChildProcHandle = ::OpenProcess(PROCESS_ALL_ACCESS, 0, Entry.th32ProcessID);

						if (ChildProcHandle)
						{
							ProcHandle ChildHandle(ChildProcHandle);
							TerminateProc(ChildHandle, KillTree);
							//::TerminateProcess(ChildProcHandle, 1);
						}
					}
				} while (::Process32Next(SnapShot, &Entry));
			}
		}
	}

	TerminateProcess(ProcessHandle.Get(), 0);
}

void WindowPlatformProcess::Sleep(float Seconds)
{
	SleepNoStats(Seconds);
}

void WindowPlatformProcess::SleepNoStats(float Seconds)
{
	uint32 Milliseconds = (uint32)(Seconds * 1000.0);
	if (Milliseconds == 0)
	{
		::SwitchToThread();
	}
	::Sleep(Milliseconds);
}

void WindowPlatformProcess::SleepInfinite()
{
	//ASSERT(FPlatformProcess::SupportsMultithreading());
	::Sleep(INFINITE);
}

