#include "foundation.h"

WindowOutputConsole::WindowOutputConsole()
	: m_Handle(NULL)
{
}

WindowOutputConsole::~WindowOutputConsole()
{
	Close();
}

void WindowOutputConsole::Open()
{
	if (m_Handle == NULL)
	{
		if (::AllocConsole())
		{
			m_Handle = ::GetStdHandle(STD_OUTPUT_HANDLE);
			if (m_Handle != INVALID_HANDLE_VALUE)
			{
				//SetConsoleScreenBufferSize(m_Handle, );

				::SetWindowPos( GetConsoleWindow(), HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOSENDCHANGING | SWP_NOZORDER );

				//SetConsoleCtrlHandler()
			}
		}
	}
}

void WindowOutputConsole::Close()
{
	if (m_Handle != NULL)
	{
		::FreeConsole();
		m_Handle = NULL;
	}
}

bool WindowOutputConsole::IsShow() const
{
	return m_Handle != NULL ? true : false;
}

void WindowOutputConsole::Clear()
{
	if (m_Handle != NULL)
	{
		CONSOLE_SCREEN_BUFFER_INFO scInfo;
		if (::GetConsoleScreenBufferInfo(m_Handle, &scInfo))
		{
			COORD pos = {0, 0};
			DWORD written;
			::FillConsoleOutputCharacter(m_Handle, ' ', scInfo.dwSize.X * scInfo.dwSize.Y, pos, &written);
		}
	}
}


void WindowOutputConsole::Print(const char* msg)
{
	if (m_Handle != NULL)
	{
		DWORD writesize;
		::WriteConsoleA(m_Handle, msg, (DWORD)strlen(msg), &writesize, NULL);
	}
}

void WindowOutputConsole::Print(const wchar* msg)
{
	if (m_Handle != NULL)
	{
		unicode::string temp(msg);

		DWORD writesize;
		::WriteConsoleW(m_Handle, temp.c_str(), temp.size(), &writesize, NULL);
	}
}

void WindowOutputConsole::SetColor(RGBA color, bool bIntensity)
{
	unsigned short attribute = 0;
	{
		attribute |= (TO_R(color) > 0.0f) ? FOREGROUND_RED : 0;
		attribute |= (TO_G(color) > 0.0f) ? FOREGROUND_GREEN : 0;
		attribute |= (TO_B(color) > 0.0f) ? FOREGROUND_BLUE : 0;
		attribute |= bIntensity ? FOREGROUND_INTENSITY : 0;
	}
	__SetColor(attribute);
}

//----------------------------------------------------------------
/**
	@brief 색상 변경
	@param Attributes
						폰트 색상					<BR>
							FOREGROUND_BLUE			<BR>
							FOREGROUND_GREEN		<BR>
							FOREGROUND_RED			<BR>
							FOREGROUND_INTENSITY	<BR>
						배경색						<BR>
							BACKGROUND_BLUE			<BR>
							BACKGROUND_GREEN		<BR>
							BACKGROUND_RED			<BR>
							BACKGROUND_INTENSITY 	<BR>
*/
//----------------------------------------------------------------
void WindowOutputConsole::__SetColor(unsigned short attribute)
{
	if (m_Handle != NULL)
	{
		::SetConsoleTextAttribute( m_Handle, attribute );
	}
}
