#include "foundation.h"
#include "blowfish.h"
#include "ini.h"

const TCHAR tag[2] = { '[', ']' };
const unsigned long encode_sig = 0xc00ec0de;

Ini::Ini()
{
	m_Buffer = NULL;
	m_Length = 0;
	m_CurPos = 0;
	memset(m_EncryptKey, 0, sizeof(m_EncryptKey));
}

Ini::~Ini()
{
	if (m_Buffer)
		delete [] m_Buffer;
}

bool Ini::Parse(char* buffer, int len)
{
	String key;
	String value;

	TCHAR *str = (TCHAR *)buffer;
	enum
	{
		_idle,
		_readkey,
		_readvalue,
	} state = _idle;

	for (int i=0; i< len/2; i++)
	{
 		if( str[i] == 0xfeff )
		{
			_ASSERT(state == _idle);
 			continue;
		}

		if( str[i] == tag[0] && state == _idle)
		{
			state = _readkey;
			continue;
		}

		if( str[i] == tag[1] && str[i+1] == 0x0d && str[i+2] == 0x0a && state == _readkey)
		{
			state = _readvalue;
			i+=3-1;
			continue;
		}

		if( state == _readvalue && str[i] == 0x0d && str[i+1] == 0x0a )
		{
			m_List.Insert(key, value);
			state = _idle;
			key.clear();
			value.clear();

			i+=2-1;
			continue;
		}

		if( state == _readkey )
		{
			key += str[i];
		}

		if( state == _readvalue )
		{
			value += str[i];
		}
	}

	if( !key.empty() && !value.empty() )
	{
		_ASSERT(state == _readvalue);
		m_List.Insert(key, value);
	}

	return true;
}

bool Ini::LoadINI(const TCHAR* fname)
{
	if (m_EncryptKey[0] != '\0')
	{
		bool ret = false;
		FILE* fp = PlatformUtil::fopen(fname, TEXT("rb"));
		if (fp != NULL)
		{
			char *buffer = NULL;
			fseek(fp, 0, SEEK_END);
			int len  = ftell(fp);
			fseek(fp, 0, SEEK_SET);

			buffer = new char [len];
			fread(buffer, 1, len, fp);
			fclose(fp);

			unsigned char sig[4];
			memcpy(sig, buffer, 4);
			if( 0xde == sig[0] && 0xc0 == sig[1] && 0x0e == sig[2] && 0xc0 == sig[3] )
			{
				memcpy(&len, buffer+4, sizeof(int));

				CBlowFish blowfish;
				blowfish.Initialize((unsigned char*) m_EncryptKey, utf8::strlen(m_EncryptKey));

				char *out = new char [len];
				blowfish.Decode((unsigned char*)buffer+8, (unsigned char*)out, len);

				ret = Parse(out, len);

				delete [] out;
			}
			else
			{
				ret = Parse(buffer, len);
			}

			delete [] buffer;
		}

		return ret;
	}
	else
	{
		FILE* fp = PlatformUtil::fopen(fname, TEXT("rb"));
		if (fp != NULL)
		{
			char *buffer = NULL;
			fseek(fp, 0, SEEK_END);
			int len  = ftell(fp);
			buffer = new char [len];

			fseek(fp, 0, SEEK_SET);
			fread(buffer, 1 ,len, fp);
			fclose(fp);

			// parse
			bool ret = Parse(buffer, len);
			delete [] buffer;

			return ret;
		}
		return false;
	}
}

void Ini::SetKey(const TCHAR* key)
{
	ConvertToUTF8(m_EncryptKey, sizeof(m_EncryptKey), key);
}

#define ALLOCSIZE	32

void Ini::MemWrite(void *data, int len)
{
	int dpos = 0;
	while( len > 0 )
	{
		int dlen = len;
		if( len > ALLOCSIZE )
			dlen = ALLOCSIZE; 
		len -= ALLOCSIZE;

		if( m_Length - m_CurPos < dlen )
		{
			if( m_Buffer )
			{
				// realloc
				char *temp = new char[ m_Length + ALLOCSIZE ];
				memcpy(temp, m_Buffer, m_CurPos );
				delete [] m_Buffer;

				m_Buffer = temp;
				m_Length += ALLOCSIZE;
			}
			else
			{
				m_Buffer = new char[ ALLOCSIZE ];
				m_Length = ALLOCSIZE;
			}
		}

		memcpy( m_Buffer+m_CurPos, (char*)data+dpos, dlen );
		dpos += dlen;
		m_CurPos += dlen;
	}
}

bool Ini::SaveINI(const TCHAR* fname)
{
	unsigned short sig = 0xfeff;
	unsigned short end[2] = { 0x0d, 0x0a };

	MemWrite(&sig, sizeof(sig));

	Pair<String, String>* it = m_List.begin();
	for (; it != m_List.end(); it++	)
	{
		MemWrite((void*)&tag[0], sizeof(tag[0]));
		MemWrite((void*)it->key.c_str(), it->key.size() * 2);
		MemWrite((void*)&tag[1], sizeof(tag[1]));

		MemWrite(&end[0], sizeof(end[0]));
		MemWrite(&end[1], sizeof(end[1]));


		MemWrite((void*)it->value.c_str(), it->value.size() * 2);
		MemWrite(&end[0], sizeof(end[0]));
		MemWrite(&end[1], sizeof(end[1]));
	}

	if (m_EncryptKey[0])
	{
		CBlowFish blowfish;
		blowfish.Initialize((unsigned char*) m_EncryptKey, utf8::strlen(m_EncryptKey));
		int outlen = blowfish.GetOutputLength(m_CurPos);

		unsigned char *out = new unsigned char[outlen];
		blowfish.Encode((unsigned char*)m_Buffer, out, m_CurPos);

		FILE* fp = PlatformUtil::fopen(fname, TEXT("wb"));
		if (fp == NULL)
			return false;

		fwrite(&encode_sig, 1, sizeof(encode_sig), fp);
		fwrite(&outlen, 1, sizeof(outlen), fp);
		fwrite(out, 1, outlen, fp);

		fclose(fp);

		delete [] m_Buffer;
		m_Buffer = NULL;
		m_Length = m_CurPos = 0;
	}
	else
	{
		FILE* fp = PlatformUtil::fopen(fname, TEXT("wb"));
		if (fp == NULL)
			return false;

		fwrite(m_Buffer, 1, m_CurPos, fp);
		fclose(fp);

		if( m_Buffer )
		{
			delete [] m_Buffer;
			m_Buffer = NULL;
			m_CurPos = 0;
			m_Length = 0;
		}
	}
	return true;
}

bool Ini::GetBool(const TCHAR* name, bool def)
{
	const TCHAR *s = Find(name);
	if( s )
	{
		return !Strcmp(s, TEXT("true")) ? true : false;
	}
	
	return def;
}

int Ini::GetInt(const TCHAR* name, int def)
{
	const TCHAR *s = Find(name);
	if( s )
	{
		return StrToInt(s);
	}

	return def;
}

float Ini::GetFloat(const TCHAR* name, float def)
{
	const TCHAR *s = Find(name);
	if( s )
	{
		return StrToFloat(s);
	}

	return def;
}

const TCHAR *Ini::GetString(const TCHAR* name, const TCHAR* def)
{
	const TCHAR *s = Find(name);
	if( s )
	{
		return s;
	}

	return def;
}

void Ini::Set(const TCHAR* name, bool b)
{
	const String value(b == true ? TEXT("true") : TEXT("false"));
	m_List.FindOrAdd(name) = value;
}

void Ini::Set(const TCHAR* name, int n)
{
	const String value = SFormat(TEXT("%d"), n);
	m_List.FindOrAdd(name) = value;
}

void Ini::Set(const TCHAR* name, float f)
{
	String value = SFormat(TEXT("%f"), f);
	m_List.FindOrAdd(name) = value;
}

void Ini::Set(const TCHAR* name, const TCHAR *str)
{
	String value(str);
	m_List.FindOrAdd(name) = value;
}

const TCHAR* Ini::Find(const TCHAR *k)
{
	IndexT index = m_List.FindIndex(k);
	if (index > INVALID_INDEX)
	{
		return m_List.ValueAt(index).c_str();
	}
	return NULL;
}