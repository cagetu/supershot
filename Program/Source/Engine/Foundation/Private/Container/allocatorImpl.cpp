﻿#include "foundation.h"

//------------------------------------------------------------------==
//	Linear Allocator
//------------------------------------------------------------------==
LinearAllocator::LinearAllocator(void* ptr, unsigned int capacity)
{
	assert(ptr != NULL);
	assert(capacity > 0);

	m_Ptr = (char*)ptr;
	m_End = (char*)ptr + capacity;
	m_Offset = (char*)ptr;
}

void* LinearAllocator::Allocate(uint32 size, uint32 align)
{
	size = Memory::Align(size, align);

	// Out of Memory
	if ((m_Offset + size) > m_End)
		return NULL;

	char* offset = m_Offset;
	m_Offset = m_Offset + size;
	return offset;
}

void LinearAllocator::Deallocate(void* ptr)
{
	m_Offset = (char*)ptr;
}

int32 LinearAllocator::Size() const
{
	return int32(m_End - m_Ptr);
}

void LinearAllocator::Reset()
{
	m_Offset = m_Ptr;
}

//------------------------------------------------------------------==
//	Stack Allocator
//------------------------------------------------------------------==
StackAllocator::StackAllocator(void* ptr, unsigned int size)
{
	assert(ptr != NULL);
	assert(size > 0);

	m_Ptr = (char*)ptr;
	m_End = (char*)ptr + size;
	m_Offset = (char*)ptr;
	m_Count = 0;
}

void* StackAllocator::Allocate(uint32 size, uint32 alignement)
{
	size += Memory::ALLOCATION_OFFSET + Memory::COUNT_OFFSET;
	size = Memory::Align(size, alignement);

	// out of memory 
	if ((m_Offset + size) > m_End)
	{
		return NULL;
	}

	const int allocation_offset = (int)(m_Offset - m_Ptr);

	union
	{
		void* voidPtr;
		char* charPtr;
		int* intPtr;
	};

	voidPtr = m_Offset;

	// 처음 4바이트에 allocation offset을 저장한다.
	*intPtr = allocation_offset;
	charPtr += Memory::ALLOCATION_OFFSET;

	// 다음 4바이트에는 카운트를 저장한다.
	*intPtr = ++m_Count;
	charPtr += Memory::COUNT_OFFSET;

	char* offset = (char*)voidPtr;
	m_Offset += size;
	return offset;
}

void StackAllocator::Deallocate(void* ptr)
{
	assert(ptr != NULL);

	union
	{
		void* voidPtr;
		char* charPtr;
		int* intPtr;
	};

	voidPtr = ptr;

	// 두번째 4바이트는 count
	charPtr -= Memory::COUNT_OFFSET;
	assert(m_Count == *intPtr);

	// 처음 4바이트는 allocation offset
	charPtr -= Memory::ALLOCATION_OFFSET;
	const int allocation_offset = *intPtr;

	m_Offset = m_Ptr + allocation_offset;
}

int32 StackAllocator::Size() const
{
	return int32(m_End - m_Ptr);
}

void StackAllocator::Reset()
{
	m_Offset = m_Ptr;
}

//------------------------------------------------------------------==
//	Pool Allocator
//------------------------------------------------------------------==
PoolAllocator::PoolAllocator(void* ptr, uint32 capacity, uint32 elementSize, int32 alignment)
{
	assert(ptr != NULL);
	assert(capacity > 0);
	assert(elementSize > 0);
	assert(elementSize > sizeof(int32));

	m_Ptr = (char*)ptr;
	m_Size = capacity;
	m_bInitialized = false;
}

void PoolAllocator::Initialize(uint32 size, uint32 align)
{
	union
	{
		void* ptr_void;
		char* ptr_char;
	};

	ptr_void = m_Ptr;

	size = Memory::Align(size, align);

	const int elementCount = (m_Size / size) - 1;
	for (int i = 0; i < elementCount; i++)
	{
		char* element = ptr_char + (i * size);
		*(char**)element = element + size;	// 다음 청크의 위치를 저장한다.
	}

	*((char**)&ptr_char[elementCount * size]) = NULL;
	m_Next = ptr_char;
}

void* PoolAllocator::Allocate(uint32 size, uint32 align)
{
	if (!m_bInitialized)
	{
		Initialize(size, align);
		m_bInitialized = true;
	}

	if (m_Next == NULL)
		return NULL;

	char* current = m_Next;
	m_Next = *((char**)m_Next);
	return current;
}

void PoolAllocator::Deallocate(void* ptr)
{
	if (ptr == NULL)
		return;

	*((char**)ptr) = m_Next;
	m_Next = (char*)ptr;
}

int32 PoolAllocator::Size() const
{
	return m_Size;
}

void PoolAllocator::Reset()
{
}

//------------------------------------------------------------------==
//	Scope Stack Allocator
//------------------------------------------------------------------==
ScopeStackAllocator::ScopeStackAllocator(LinearAllocator& allocator, int32 alignment)
{
	m_Allocator = &allocator;
	m_Offset = allocator.Offset();
	m_Alignment = alignment;
	m_FinalizerChain = NULL;
}

ScopeStackAllocator::~ScopeStackAllocator()
{
	for (Memory::Finalizer* f = m_FinalizerChain; f; f = f->next)
	{
		(*f->fn)((char*)f + Memory::Align(sizeof(Memory::Finalizer), m_Alignment));
	}
	m_FinalizerChain = NULL;

	m_Allocator->Deallocate(m_Offset);
}

//------------------------------------------------------------------==
//	Global Memory
//------------------------------------------------------------------==
namespace Memory
{
	struct GlobalMemory
	{
		static const int32 ALLOCATOR_MEMORY = sizeof(Allocator);
		char allocatorBuffer[ALLOCATOR_MEMORY];

		// Array가 사용하는 기본 Allocator
		Allocator defaultAllocator;
		// One FrameBuffer Allocator 등등을 여기에 등록해주자!!!

		GlobalMemory()
		{
		}
	};
	GlobalMemory _GlobalMemory;

	void Initliaize()
	{
		char* ptr = _GlobalMemory.allocatorBuffer;

		//_GlobalMemory.defaultAllocator = new (ptr) Allocator();
		//_GlobalMemory.defaultAllocator = Memory::Construct<Allocator>(ptr);
		//ptr += sizeof(Allocator);

	}

	void Shutdown()
	{
		//Memory::Destruct<Allocator>(_GlobalMemory.defaultAllocator);
	}

	Allocator& DefaultAllocator()
	{
		return _GlobalMemory.defaultAllocator;
	}
}
