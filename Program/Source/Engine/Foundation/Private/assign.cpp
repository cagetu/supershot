// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "assign.h"

Assign::Assign(const String & name, const String & path)
	: m_Name(name), m_Path(path)
{
	CHECK(!name.empty() && !path.empty(), TEXT("empty")); 
}
