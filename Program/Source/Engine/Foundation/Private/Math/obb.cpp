#include "foundation.h"
#include "Math\obb.h"
#include "Math\matrix.h"

//////////////////////////////////////////////////////////////////////
// OBB박스 만들기 
//////////////////////////////////////////////////////////////////////
namespace OBBUtil
{
    //--------------------------------------------------------------
    void Rotate( float a[3][3], float s, float tau, int i, int j, int k, int l )
    {
        float h, g;
        g = a[i][j];
        h = a[k][l];
        a[i][j] = g - s * ( h + g *tau );
        a[k][l] = h + s * ( g - h *tau );
    }

    //--------------------------------------------------------------
    bool Jacobi( float a[3][3], float v[3][3], float d[3] )
    {
        int n = 3;
        int i, j, iq, ip;
        float tresh, theta, tau, t, sm, s, h, g, c, b[3], z[3];

        for( ip = 0; ip < n; ip++ ){
            for( iq = 0; iq < n; iq++ ) v[ip][iq] = 0.0f;
            v[ip][ip] = 1.0f;
        }
        for( ip = 0; ip < n; ip++ ){
            b[ip] = d[ip] = a[ip][ip];
            z[ip] = 0.0f;
        }
        for( i = 0; i < 50; i++ ){
            sm = 0.0f;
            for( ip = 0; ip < n - 1; ip++ ){
                for( iq = ip + 1; iq < n; iq++ ) sm += Math::Abs(a[ip][iq]);
            }

            if( sm == 0.0f ) return true;
            if( i < 3 ) tresh = 0.2f * sm / ( n * n );
            else tresh = 0.0f;
            for( ip = 0; ip < n - 1; ip++ ){
                for( iq = ip + 1; iq < n; iq++ ){
                    g = 100.0f * Math::Abs( a[ip][iq] );
                    if( i > 3 && ( fabs( d[ip] ) + g ) == fabs( d[ip] )
                        && ( fabs( d[iq] ) + g ) == fabs( d[iq] ) ) a[ip][iq] = 0.0f;
                    else if( fabs( a[ip][iq] ) > tresh ){
                        h = d[iq] - d[ip];
                        if( ( fabs( h ) + g ) == fabs( h ) ) t = a[ip][iq] / h;
                        else{
                            theta = 0.5f * h / a[ip][iq];
                            t = 1.0f / ( Math::Abs( theta ) + (float)sqrt( 1.0f + theta * theta ) );
                            if( theta < 0.0f ) t = -t;
                        }
                        c = 1.0f / (float)sqrt( 1 + t * t );
                        s = t * c;
                        tau = s / ( 1.0f + c );
                        h = t * a[ip][iq];
                        z[ip] -= h;
                        z[iq] += h;
                        d[ip] -= h;
                        d[iq] += h;
                        a[ip][iq] = 0.0f;

                        for( j = 0; j < ip; j++ ) Rotate( a, s, tau, j, ip, j, iq );
                        for( j = ip + 1; j < iq; j++ ) Rotate( a, s, tau, ip, j, j, iq );
                        for( j = iq + 1; j < n; j++ ) Rotate( a, s, tau, ip, j, iq, j );
                        for( j = 0; j < n; j++ ) Rotate( v, s, tau, j, ip, j, iq );
                    }
                }
            }
            for( ip = 0; ip < n; ip++ ){
                b[ip] += z[ip];
                d[ip] = b[ip];
                z[ip] = 0.0f;
            }
        }

        return false;
    }
}

OBB OBB::MakeFrom(const Vec3 * vertices, unsigned int count)
{
	OBB obb;

	// compute mean of points
	Vec3 m(0, 0, 0);
	unsigned int i = 0;
	for (i = 0; i < count; ++i )
	{
		m += vertices[i];
	}

	m /= (float)count;

	// compute covariances of points
	float C11 = 0, C22 = 0, C33 = 0, C12 = 0, C13 = 0,  C23 = 0;
	for (i = 0; i < count; ++i )
	{
		C11 += ( vertices[i].x - m.x ) * ( vertices[i].x - m.x );
		C22 += ( vertices[i].y - m.y ) * ( vertices[i].y - m.y );
		C33 += ( vertices[i].z - m.z ) * ( vertices[i].z - m.z );
		C12 += ( vertices[i].x - m.x ) * ( vertices[i].y - m.y );
		C13 += ( vertices[i].x - m.x ) * ( vertices[i].z - m.z );
		C23 += ( vertices[i].y - m.y ) * ( vertices[i].z - m.z );
	}

	C11 /= count;
	C22 /= count;
	C33 /= count;
	C12 /= count;
	C13 /= count;
	C23 /= count;

	// compute eigenvectors for covariance matrix
	float Matrix[3][3] = 
	{
		{ C11, C12, C13 },
		{ C12, C22, C23 },
		{ C13, C23, C33 },
	};

	float EigenVectors[3][3];
	float EigenValue[3];
	OBBUtil::Jacobi( Matrix, EigenVectors, EigenValue );

	struct SORT
	{
		int ID;
		float Value;
	} Sort[3] = { { 0, EigenValue[0] }, { 1, EigenValue[1] }, { 2, EigenValue[2] } };

	// 축 구하기 
	for (i = 0; i < 3; ++i)
	{
		obb.Axis[i].x = EigenVectors[0][Sort[i].ID];
		obb.Axis[i].y = EigenVectors[1][Sort[i].ID];
		obb.Axis[i].z = EigenVectors[2][Sort[i].ID];
	}

	float tmp_min[3] = {  FLT_MAX,  FLT_MAX,  FLT_MAX };
	float tmp_max[3] = { -FLT_MAX, -FLT_MAX, -FLT_MAX };

	// OBB 방향 벡터에 최소 볼록 집합들을 투영한 후 그 방향으로 최대, 최소값을 구한다.
	for (int j = 0; j < 3; ++j)
	{
		for (i = 0; i < count; ++i)
		{
			float a = Vec3::DotProduct( vertices[i], obb.Axis[j] );
			if( tmp_min[j] > a ) 
				tmp_min[j] = a;
			if( tmp_max[j] < a ) 
				tmp_max[j] = a;
		}
	}

	// 중심 구하기 
	obb.Center = (obb.Axis[0] * (( tmp_min[0] + tmp_max[0] ) * 0.5f)) +
				 (obb.Axis[1] * (( tmp_min[1] + tmp_max[1] ) * 0.5f)) +
				 (obb.Axis[2] * (( tmp_min[2] + tmp_max[2] ) * 0.5f));

	// 길이 구하기
	for (i = 0; i < 3; ++i) 
		obb.Extent[i] = 0.5f * ( tmp_max[i] - tmp_min[i] );

	return obb;
}

OBB::OBB() 
{
	Memory::MemZero(Extent);
}

OBB::OBB(const Vec3 & center, const Vec3 * axis, const float * extent)
{
	Center = center;
	Memory::MemCpy(Axis, axis, sizeof(Vec3)*3);
	Memory::MemCpy(Extent, extent, sizeof(float)*3);
}

OBB::OBB(OBB&& obb) noexcept
{
    Center = obb.Center;
    Memory::MemCpy(Axis, obb.Axis, sizeof(Vec3)*3);
    Memory::MemCpy(Extent, obb.Extent, sizeof(float)*3);
    
    obb.Center = Vec3::ZERO;
    Memory::MemZero(obb.Axis, sizeof(Vec3)*3);
    Memory::MemZero(obb.Extent, sizeof(float)*3);
}

OBB::OBB(const OBB& obb)
{
	Center = obb.Center;
	Memory::MemCpy(Axis, obb.Axis, sizeof(Vec3) * 3);
	Memory::MemCpy(Extent, obb.Extent, sizeof(float) * 3);
}

OBB& OBB::operator = (OBB&& obb) noexcept
{
	Center = obb.Center;
	Memory::MemCpy(Axis, obb.Axis, sizeof(Vec3) * 3);
	Memory::MemCpy(Extent, obb.Extent, sizeof(float) * 3);

	obb.Center = Vec3::ZERO;
	Memory::MemZero(obb.Axis, sizeof(Vec3) * 3);
	Memory::MemZero(obb.Extent, sizeof(float) * 3);

	return *this;
}

OBB& OBB::operator = (const OBB& obb)
{
	Center = obb.Center;
	Memory::MemCpy(Axis, obb.Axis, sizeof(Vec3) * 3);
	Memory::MemCpy(Extent, obb.Extent, sizeof(float) * 3);

	return *this;
}

bool OBB::operator == (const OBB& rhs) const
{
	if (Center != rhs.Center)
		return false;

	for (int32 i = 0; i < 3; i++)
	{
		if (Axis[i] != rhs.Axis[i])
			return false;
		if (Extent[i] != rhs.Extent[i])
			return false;
	}
	return true;
}

void OBB::Expand(const OBB& obb)
{
	Vec3 akVertex[8], akVertex0[8];

	GetVertices(akVertex);
	obb.GetVertices(akVertex0);

	Center = 0.5f * (Center + obb.Center);

	Mat4 M, M0;
	M.SetAxises(Axis);
	M0.SetAxises(obb.Axis);

	Quat Q, Q0;
	Q = M.GetRotation();
	Q0 = M0.GetRotation();

	float QDot = Quat::DotProduct(Q, Q0);
	if (QDot < 0.0f)
		Q0 = -Q0;

	Quat qResult = Q + Q0;
	float Q0Dot = Quat::DotProduct(qResult, qResult);

	float InvLength = Math::InvSqrt(Q0Dot);
	qResult = qResult * InvLength;
	Mat4 mResult;
	mResult.SetRotation(qResult);
	mResult.GetAxises(Axis);

	int i, j;
	float fDot;
	float kMin[3], kMax[3];
	Vec3 kDiff;

	for (i = 0; i < 3; i++)
	{
		kMin[i] = 0.0f;
		kMax[i] = 0.0f;
	}

	for (i = 0; i < 8; i++)
	{
		kDiff = akVertex[i] - Center;
		for (j = 0; j < 3; j++)
		{
			fDot = Vec3::DotProduct(kDiff, Axis[j]);
			if (fDot > kMax[j])
				kMax[j] = fDot;
			else if (fDot < kMin[j])
				kMin[j] = fDot;
		}
	}

	for (i = 0; i < 8; i++)
	{
		kDiff = akVertex0[i] - Center;
		for (j = 0; j < 3; j++)
		{
			fDot = Vec3::DotProduct(kDiff, Axis[j]);
			if (fDot > kMax[j])
				kMax[j] = fDot;
			else if (fDot < kMin[j])
				kMin[j] = fDot;
		}
	}

	for (j = 0; j < 3; j++)
	{
		Center += (0.5f * (kMax[j] + kMin[j])) * Axis[j];
		Extent[j] = (0.5f * (kMax[j] - kMin[j]));
	}
}

void OBB::GetVertices(Vec3 * vertices) const
{
	Vec3 aEAxis[3] = {
		Axis[0] * Extent[0],
		Axis[1] * Extent[1],
		Axis[2] * Extent[2]
	};

	vertices[0] = Center - aEAxis[0] - aEAxis[1] - aEAxis[2];
	vertices[1] = Center + aEAxis[0] - aEAxis[1] - aEAxis[2];
	vertices[2] = Center + aEAxis[0] + aEAxis[1] - aEAxis[2];
	vertices[3] = Center - aEAxis[0] + aEAxis[1] - aEAxis[2];
	vertices[4] = Center - aEAxis[0] - aEAxis[1] + aEAxis[2];
	vertices[5] = Center + aEAxis[0] - aEAxis[1] + aEAxis[2];
	vertices[6] = Center + aEAxis[0] + aEAxis[1] + aEAxis[2];
	vertices[7] = Center - aEAxis[0] + aEAxis[1] + aEAxis[2];
}

void OBB::Translate(const Vec3& v)
{
	Center += v;
}

OBB OBB::Transform(const Mat4& tm) const
{
	Mat4 rot;
	tm.GetRotation(rot);

	Vec3 newcenter = rot.Transform(Center);
	newcenter += tm.GetPosition();

	Vec3 newaxis[3];
	float newextent[3];

	for (int i = 0; i < 3; i++)
	{
		newaxis[i] = rot.Transform(Axis[i]);
		newextent[i] = Extent[i] * Vec3::Size(newaxis[i]);
		newaxis[i].Normalize();
	}

	return OBB(newcenter, newaxis, newextent);
}

// obb 와 Ray의 충돌
bool OBB::IntersectTest(const Vec3& origin, const Vec3& dir, float& outDistance)
{
	float t_min = -99999.0f;
	float t_max = 99999.0f;

	outDistance = 0.0f;
	Vec3 p = Center - origin;

	float e, f, h, t1, t2;
	for (int i = 0; i < 3; ++i)
	{
		e = Vec3::DotProduct(Axis[i], p);
		f = Vec3::DotProduct(Axis[i], dir);

		// h = Extent[i]*0.5f;
		h = Extent[i];

		if (Math::Abs<float>(f) > __EPSILON)
		{
			t1 = (e + h) / f;
			t2 = (e - h) / f;

			if (t1 > t2)	Math::Swap(t1, t2);
			if (t1 > t_min)	t_min = t1;
			if (t2 < t_max)	t_max = t2;

			if (t_min > t_max)
			{
				return false;
			}

			if (t_max < 0.0f)
			{
				return false;
			}
		}
		else if ((-e - h > 0) || (-e + h < 0))
		{
			return false;
		}
	}

	if (t_min > 0.0f)
	{
		outDistance = t_min;
		return true;
	}
	else
	{
		outDistance = t_max;
		return true;
	}

	outDistance = 0.0f;
	return false;
}

// OBB와 Ray 충돌
bool OBB::IntersectTest(const Vec3& origin, const Vec3& dir)
{
	float fWdU[3], fAWdU[3], fDdU[3], fADdU[3], fAWxDdU[3], fRhs;

	Vec3 kDiff = origin - Center;

	for (int i = 0; i < 3; ++i)
	{
		fWdU[i] = Vec3::DotProduct(dir, Axis[i]);
		fDdU[i] = Vec3::DotProduct(kDiff, Axis[i]);
		fAWdU[i] = Math::Abs(fWdU[i]);
		fADdU[i] = Math::Abs(fDdU[i]);

		if (fADdU[i] > Extent[i] && fDdU[i] * fWdU[i] >= 0.0f)
			return false;
	}

	//
	Vec3 kWxD = Vec3::CrossProduct(dir, kDiff);

	fAWxDdU[0] = Math::Abs(Vec3::DotProduct(kWxD, Axis[0]));
	fRhs = Extent[1] * fAWdU[2] + Extent[2] * fAWdU[1];
	if (fAWxDdU[0] > fRhs)
		return false;

	fAWxDdU[1] = Math::Abs(Vec3::DotProduct(kWxD, Axis[1]));
	fRhs = Extent[0] * fAWdU[2] + Extent[2] * fAWdU[0];
	if (fAWxDdU[1] > fRhs)
		return false;

	fAWxDdU[2] = Math::Abs(Vec3::DotProduct(kWxD, Axis[2]));
	fRhs = Extent[0] * fAWdU[1] + Extent[1] * fAWdU[0];
	if (fAWxDdU[2] > fRhs)
		return false;

	return true;
}

// Obb와 Obb 충돌
bool OBB::IntersectTest(const OBB& Obb)
{
	const Vec3* akA = GetAxises();
	const Vec3* akB = Obb.GetAxises();

	const float* afEA = GetExtents();
	const float* afEB = Obb.GetExtents();

	// compute difference of box centers, D = C1-C0
	Vec3 kD = GetCenter() - Obb.GetCenter();

	float aafC[3][3];     // matrix C = A^T B, c_{ij} = Dot(A_i,B_j)
	float aafAbsC[3][3];  // |c_{ij}|
	float afAD[3];        // Dot(A_i,D)
	float fOBB, fR1, fR;   // interval radii and distance between centers
	float fOBB1;           // = OBB + R1

	// axis C0+t*A0	
	aafC[0][0] = Vec3::DotProduct(akA[0], akB[0]);
	aafC[0][1] = Vec3::DotProduct(akA[0], akB[1]);
	aafC[0][2] = Vec3::DotProduct(akA[0], akB[2]);

	afAD[0] = Vec3::DotProduct(akA[0], kD);

	aafAbsC[0][0] = Math::Abs(aafC[0][0]);
	aafAbsC[0][1] = Math::Abs(aafC[0][1]);
	aafAbsC[0][2] = Math::Abs(aafC[0][2]);
	fR = Math::Abs(afAD[0]);
	fR1 = afEB[0] * aafAbsC[0][0] + afEB[1] * aafAbsC[0][1] + afEB[2] * aafAbsC[0][2];
	fOBB1 = afEA[0] + fR1;
	if (fR > fOBB1)
		return false;


	// axis C0+t*A1
	aafC[1][0] = Vec3::DotProduct(akA[1], akB[0]);
	aafC[1][1] = Vec3::DotProduct(akA[1], akB[1]);
	aafC[1][2] = Vec3::DotProduct(akA[1], akB[2]);
	afAD[1] = Vec3::DotProduct(akA[1], kD);

	aafAbsC[1][0] = Math::Abs(aafC[1][0]);
	aafAbsC[1][1] = Math::Abs(aafC[1][1]);
	aafAbsC[1][2] = Math::Abs(aafC[1][2]);
	fR = Math::Abs(afAD[1]);

	fR1 = afEB[0] * aafAbsC[1][0] + afEB[1] * aafAbsC[1][1] + afEB[2] * aafAbsC[1][2];
	fOBB1 = afEA[1] + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A2
	aafC[2][0] = Vec3::DotProduct(akA[2], akB[0]);
	aafC[2][1] = Vec3::DotProduct(akA[2], akB[1]);
	aafC[2][2] = Vec3::DotProduct(akA[2], akB[2]);
	afAD[2] = Vec3::DotProduct(akA[2], kD);

	aafAbsC[2][0] = Math::Abs(aafC[2][0]);
	aafAbsC[2][1] = Math::Abs(aafC[2][1]);
	aafAbsC[2][2] = Math::Abs(aafC[2][2]);
	fR = Math::Abs(afAD[2]);

	fR1 = afEB[0] * aafAbsC[2][0] + afEB[1] * aafAbsC[2][1] + afEB[2] * aafAbsC[2][2];
	fOBB1 = afEA[2] + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*B0
	fR = Math::Abs(Vec3::DotProduct(akB[0], kD));
	fOBB = afEA[0] * aafAbsC[0][0] + afEA[1] * aafAbsC[1][0] + afEA[2] * aafAbsC[2][0];
	fOBB1 = fOBB + afEB[0];
	if (fR > fOBB1)
		return false;

	// axis C0+t*B1
	fR = Math::Abs(Vec3::DotProduct(akB[1], kD));
	fOBB = afEA[0] * aafAbsC[0][1] + afEA[1] * aafAbsC[1][1] + afEA[2] * aafAbsC[2][1];
	fOBB1 = fOBB + afEB[1];
	if (fR > fOBB1)
		return false;

	// axis C0+t*B2
	fR = Math::Abs(Vec3::DotProduct(akB[2], kD));
	fOBB = afEA[0] * aafAbsC[0][2] + afEA[1] * aafAbsC[1][2] + afEA[2] * aafAbsC[2][2];
	fOBB1 = fOBB + afEB[2];
	if (fR > fOBB1)
		return false;

	// axis C0+t*A0xB0
	fR = Math::Abs(afAD[2] * aafC[1][0] - afAD[1] * aafC[2][0]);
	fOBB = afEA[1] * aafAbsC[2][0] + afEA[2] * aafAbsC[1][0];
	fR1 = afEB[1] * aafAbsC[0][2] + afEB[2] * aafAbsC[0][1];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A0xB1
	fR = Math::Abs(afAD[2] * aafC[1][1] - afAD[1] * aafC[2][1]);
	fOBB = afEA[1] * aafAbsC[2][1] + afEA[2] * aafAbsC[1][1];
	fR1 = afEB[0] * aafAbsC[0][2] + afEB[2] * aafAbsC[0][0];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A0xB2
	fR = Math::Abs(afAD[2] * aafC[1][2] - afAD[1] * aafC[2][2]);
	fOBB = afEA[1] * aafAbsC[2][2] + afEA[2] * aafAbsC[1][2];
	fR1 = afEB[0] * aafAbsC[0][1] + afEB[1] * aafAbsC[0][0];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A1xB0
	fR = Math::Abs(afAD[0] * aafC[2][0] - afAD[2] * aafC[0][0]);
	fOBB = afEA[0] * aafAbsC[2][0] + afEA[2] * aafAbsC[0][0];
	fR1 = afEB[1] * aafAbsC[1][2] + afEB[2] * aafAbsC[1][1];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A1xB1
	fR = Math::Abs(afAD[0] * aafC[2][1] - afAD[2] * aafC[0][1]);
	fOBB = afEA[0] * aafAbsC[2][1] + afEA[2] * aafAbsC[0][1];
	fR1 = afEB[0] * aafAbsC[1][2] + afEB[2] * aafAbsC[1][0];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A1xB2
	fR = Math::Abs(afAD[0] * aafC[2][2] - afAD[2] * aafC[0][2]);
	fOBB = afEA[0] * aafAbsC[2][2] + afEA[2] * aafAbsC[0][2];
	fR1 = afEB[0] * aafAbsC[1][1] + afEB[1] * aafAbsC[1][0];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A2xB0
	fR = Math::Abs(afAD[1] * aafC[0][0] - afAD[0] * aafC[1][0]);
	fOBB = afEA[0] * aafAbsC[1][0] + afEA[1] * aafAbsC[0][0];
	fR1 = afEB[1] * aafAbsC[2][2] + afEB[2] * aafAbsC[2][1];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A2xB1
	fR = Math::Abs(afAD[1] * aafC[0][1] - afAD[0] * aafC[1][1]);
	fOBB = afEA[0] * aafAbsC[1][1] + afEA[1] * aafAbsC[0][1];
	fR1 = afEB[0] * aafAbsC[2][2] + afEB[2] * aafAbsC[2][0];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	// axis C0+t*A2xB2
	fR = Math::Abs(afAD[1] * aafC[0][2] - afAD[0] * aafC[1][2]);
	fOBB = afEA[0] * aafAbsC[1][2] + afEA[1] * aafAbsC[0][2];
	fR1 = afEB[0] * aafAbsC[2][1] + afEB[1] * aafAbsC[2][0];
	fOBB1 = fOBB + fR1;
	if (fR > fOBB1)
		return false;

	return true;
}
