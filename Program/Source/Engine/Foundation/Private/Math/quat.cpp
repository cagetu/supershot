#include "foundation.h"
#include "Math\quat.h"
#include "Math\rotator.h"

const Quat Quat::IDENTITY(0.0f, 0.0f, 0.0f, 1.0f);

void Quat::Normalize(float Tolerance)
{
	const float SquareSum = x * x + y * y + z * z + w * w;

	if (SquareSum >= Tolerance)
	{
		const float Scale = Math::InvSqrt(SquareSum);

		x *= Scale;
		y *= Scale;
		z *= Scale;
		w *= Scale;
	}
	else
	{
		*this = Quat::IDENTITY;
	}
}

bool Quat::IsNormalized() const
{
	return (Math::Abs(1.0f - SizeSquared()) < THRESH_QUAT_NORMALIZED);
}

Quat Quat::GetNormalized(float Tolerance) const
{
	Quat Result(*this);
	Result.Normalize(Tolerance);
	return Result;
}

bool Quat::ContainsNaN() const
{
	return (!Math::IsFinite(x) ||
			!Math::IsFinite(y) ||
			!Math::IsFinite(z) ||
			!Math::IsFinite(w));
}

Quat Quat::Log() const
{
	Quat Result;
	Result.w = 0.f;

	if (Math::Abs(w) < 1.f)
	{
		const float Angle = Math::ACos(w);
		const float SinAngle = Math::Sin(Angle);

		if (Math::Abs(SinAngle) >= SMALL_NUMBER)
		{
			const float Scale = Angle / SinAngle;
			Result.x = Scale * x;
			Result.y = Scale * y;
			Result.z = Scale * z;

			return Result;
		}
	}

	Result.x = x;
	Result.y = y;
	Result.z = z;

	return Result;
}

Quat Quat::Exp() const
{
	const float Angle = Math::Sqrt(x * x + y * y + z * z);
	const float SinAngle = Math::Sin(Angle);

	Quat Result;
	Result.w = Math::Cos(Angle);

	if (Math::Abs(SinAngle) >= SMALL_NUMBER)
	{
		const float Scale = SinAngle / Angle;
		Result.x = Scale * x;
		Result.y = Scale * y;
		Result.z = Scale * z;
	}
	else
	{
		Result.x = x;
		Result.y = y;
		Result.z = z;
	}

	return Result;
}

Vec3 Quat::Euler() const
{
	Rotator EulerAngles(*this);
	return EulerAngles.Euler();
}

//
// Based on:
// http://lolengine.net/blog/2014/02/24/quaternion-from-two-vectors-final
// http://www.euclideanspace.com/maths/algebra/vectors/angleBetween/index.htm
//
Quat FindBetween_Helper(const Vec3& A, const Vec3& B, float NormAB)
{
	float W = NormAB + Vec3::DotProduct(A, B);
	Quat Result;

	if (W >= 1e-6f * NormAB)
	{
		//Axis = Vec3::CrossProduct(A, B);
		Result = Quat(A.y * B.z - A.z * B.y,
					A.z * B.x - A.x * B.z,
					A.x * B.y - A.y * B.x,
					W);
	}
	else
	{
		// A and B point in opposite directions
		W = 0.f;
		Result = Math::Abs(A.x) > Math::Abs(A.y)
			? Quat(-A.z, 0.f, A.x, W)
			: Quat(0.f, -A.z, A.y, W);
	}

	Result.Normalize();
	return Result;
}

Quat Quat::FindBetweenNormals(const Vec3& Normal1, const Vec3& Normal2)
{
	const float NormAB = 1.f;
	return FindBetween_Helper(Normal1, Normal2, NormAB);
}

Quat Quat::FindBetweenVectors(const Vec3& Vector1, const Vec3& Vector2)
{
	const float NormAB = Math::Sqrt(Vector1.SizeSquared() * Vector2.SizeSquared());
	return FindBetween_Helper(Vector1, Vector2, NormAB);
}

Vec3 Quat::GetRotationAxis() const
{
	const float SquareSum = x * x + y * y + z * z;
	if (SquareSum < SMALL_NUMBER)
	{
		return Vec3::XAXIS;
	}
	const float Scale = Math::InvSqrt(SquareSum);
	return Vec3(x * Scale, y * Scale, z * Scale);
}

Radian Quat::GetAngle() const
{
	// W = cos(Theta*0.5f)
	return 2.0f * Math::ACos(w);
}

Radian Quat::AngularDistance(const Quat& Q) const
{
	float InnerProd = x * Q.x + y * Q.y + z * Q.z + w * Q.w;
	return Math::ACos((2 * InnerProd * InnerProd) - 1.f);
}

Vec3 Quat::RotateVector(Vec3 V) const
{
	// http://people.csail.mit.edu/bkph/articles/Quaternions.pdf
	// V' = V + 2w(Q x V) + (2Q x (Q x V))
	// refactor:
	// V' = V + w(2(Q x V)) + (Q x (2(Q x V)))
	// T = 2(Q x V);
	// V' = V + w*(T) + (Q x T)

	const Vec3 Q(x, y, z);
	const Vec3 T = 2.f * Vec3::CrossProduct(Q, V);
	const Vec3 Result = V + (w * T) + Vec3::CrossProduct(Q, T);
	return Result;
}

Vec3 Quat::UnrotateVector(Vec3 V) const
{
	const Vec3 Q(-x, -y, -z); // Inverse
	const Vec3 T = 2.f * Vec3::CrossProduct(Q, V);
	const Vec3 Result = V + (w * T) + Vec3::CrossProduct(Q, T);
	return Result;
}

Quat Quat::Multiply(const Quat& q1, const Quat& q2)
{
	// q1 * q2 = (w1v2 + w2v1 + cross(v1, v2), w1w2 - dot(v1,v2))

	Quat q;
	q.x = q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y;
	q.y = q1.w*q2.y + q1.y*q2.w + q1.z*q2.x - q1.x*q2.z;
	q.z = q1.w*q2.z + q1.z*q2.w + q1.x*q2.y - q1.y*q2.x;
	q.w = q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z;
	return q;
}

Quat Quat::Slerp(const Quat &q1, const Quat &q2, float w)
{
	float cr, sr, rad, t1, t2;
	Quat qt, q;

	cr = Quat::DotProduct(q1, q2);

	if (cr < 0.0f)
	{
		qt.x = -q1.x;
		qt.y = -q1.y;
		qt.z = -q1.z;
		qt.w = -q1.w;
		cr = -cr;
	}
	else
	{
		qt = q1;
	}

	if (1.0f - 0.05f < cr)
	{
		q.x = qt.x + (q2.x - qt.x) * w;
		q.y = qt.y + (q2.y - qt.y) * w;
		q.z = qt.z + (q2.z - qt.z) * w;
		q.w = qt.w + (q2.w - qt.w) * w;
		return q;
	}

	rad	= Math::ACos(cr);
	sr = Math::Sin(rad);

	t1 = Math::Sin(rad * (1.0f - w)) / sr;
	t2 = Math::Sin(rad * w) / sr;

	q.x	= qt.x * t1 + q2.x * t2;
	q.y	= qt.y * t1 + q2.y * t2;
	q.z	= qt.z * t1 + q2.z * t2;
	q.w	= qt.w * t1 + q2.w * t2;
	return q;
}

Quat Quat::MakeFrom(const Vec3& v1, const Vec3& v2)
{
	Vec3 vn = Vec3::Normalize(v1+v2);
	Vec3 q_xyz = Vec3::CrossProduct(v2, vn);
	float q_w = Vec3::DotProduct(vn, v2);

	Quat q;
	q.x = q_xyz.x;
	q.y = q_xyz.y;
	q.z = q_xyz.z;
	q.w = q_w;
	return q;
}

Quat Quat::MakeFromAxisAngle(const Vec3& axis, Radian rad)
{
	float cs = Math::Cos(rad*0.5f);
	float sn = Math::Sin(rad*0.5f);

	Quat q;
	q.x = axis.x*sn;
	q.y = axis.y*sn;
	q.z = axis.z*sn;
	q.w = cs;
	return q;
}

void Quat::Decompose(Vec3& axis, Radian& angle) const
{
	axis = GetRotationAxis();
	angle = GetAngle();
}

void Quat::Decompose(Radian& pitch, Radian& yaw, Radian& roll)
{
    float sqw = w * w;
    float sqx = x * x;
    float sqy = y * y;
    float sqz = z * z;

    /// rotation about x-axis
    pitch = (float)Math::ASin(-2.0f * ( x * z - y * w ));
    /// rotation about y-axis
    yaw = (float)Math::ATan2(2.0f * ( y * z + x * w ) , ( -sqx - sqy + sqz + sqw ));
    /// rotation about z-axis
    roll = (float)Math::ATan2(2.0f * ( x * y + z * w ) , (  sqx - sqy - sqz + sqw ));
}

// Quaternion To Euler
void Quat::MakeToEuler(const Quat &q, Radian *r)
{
	float sqw = q.w * q.w;
	float sqx = q.x * q.x;
	float sqy = q.y * q.y;
	float sqz = q.z * q.z;

	// yaw
	r[0] = (float)Math::ATan2(2.0f * ( q.y * q.z + q.x * q.w ) , ( -sqx - sqy + sqz + sqw ));
	// pitch
	r[1] = (float)Math::ASin(-2.0f * ( q.x * q.z - q.y * q.w ));
	// roll
	r[2] = (float)Math::ATan2(2.0f * ( q.x * q.y + q.z * q.w ) , (  sqx - sqy - sqz + sqw ));
}

// Quaternion From Euler
Quat Quat::MakeFromEuler(Radian rx, Radian ry, Radian rz)
{
	float cr = Math::Cos(rx/2);
	float cp = Math::Cos(ry/2);
	float cy = Math::Cos(rz/2);
	float sr = Math::Sin(rx/2);
	float sp = Math::Sin(ry/2);
	float sy = Math::Sin(rz/2);

	float cpcy = cp * cy;
	float spsy = sp * sy;

	return Quat(sr * cpcy - cr * spsy, cr * sp * cy + sr * cp * sy, cr * cp * sy - sr * sp * cy, cr * cpcy + sr * spsy);
}

