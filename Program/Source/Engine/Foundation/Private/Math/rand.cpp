#include "foundation.h"
#include "Math\rand.h"
#include "mtrand.h"

#define _USE_MTRAND	1

#if _USE_MTRAND
static MTRand mtRandPrivate;
#endif

void RandomNumber::Startup(int32 seed)
{
#if _USE_MTRAND
	mtRandPrivate.seed(seed);
#else
	::srand(seed);
#endif
}

void RandomNumber::Shutdown()
{
}

int32 RandomNumber::Rand()
{
	return ::rand();
}

int32 RandomNumber::RandInt()
{
#if _USE_MTRAND
	return mtRandPrivate.randInt();
#else
	return ::rand();
#endif
}

int32 RandomNumber::RandInt(int32 minValue, int32 maxValue)
{
	return minValue + (maxValue - minValue) * RandomNumber::RandInt();
}

float RandomNumber::RandFloat()
{
#if _USE_MTRAND
	return mtRandPrivate.rand();
#else
	return ::rand() / (float)(RAND_MAX + 1.0f);
#endif
}

float RandomNumber::RandFloat(float minValue, float maxValue)
{
	return minValue + (maxValue - minValue) * RandomNumber::RandFloat();
}
