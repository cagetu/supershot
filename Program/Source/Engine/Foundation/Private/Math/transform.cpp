#include "foundation.h"
#include "Math\transform.h"
#include "Math\quat.h"

Mat4 Transform::RotationX(Radian rad)
{
	float cs = Math::Cos(rad);
	float sn = Math::Sin(rad);
	Mat4 m = {
		1, 0, 0, 0,
		0, +cs, +sn, 0,
		0, -sn, +cs, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::RotationY(Radian rad)
{
	float cs = Math::Cos(rad);
	float sn = Math::Sin(rad);
	Mat4 m = {
		+cs, 0, -sn, 0,
		0, 1, 0, 0,
		+sn, 0, +cs, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::RotationZ(Radian rad)
{
	float cs = Math::Cos(rad);
	float sn = Math::Sin(rad);
	Mat4 m = {
		+cs, +sn, 0, 0,
		-sn, +cs, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::Rotation(const Quat& q)
{
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;
	Mat4 m;

	xx = 2.0f * q.x * q.x;
	yy = 2.0f * q.y * q.y;
	zz = 2.0f * q.z * q.z;
	xy = 2.0f * q.x * q.y;
	xz = 2.0f * q.x * q.z;
	yz = 2.0f * q.y * q.z;
	wx = 2.0f * q.w * q.x;
	wy = 2.0f * q.w * q.y;
	wz = 2.0f * q.w * q.z;

	m._11 = 1 - (yy + zz);
	m._12 = (xy - wz);
	m._13 = (xz + wy);
	m._14 = 0;

	m._21 = (xy + wz);
	m._22 = 1 - (xx + zz);
	m._23 = (yz - wx);
	m._24 = 0;

	m._31 = (xz - wy);
	m._32 = (yz + wx);
	m._33 = 1 - (xx + yy);
	m._34 = 0;

	m._41 = 0;
	m._42 = 0;
	m._43 = 0;
	m._44 = 1;

	return m;
}

Mat4 Transform::Multiply(const Mat4& a, const Mat4& b)
{
	Mat4 m;

	m._11 = a._11 * b._11 + a._12 * b._21 + a._13 * b._31 + a._14 * b._41;
	m._12 = a._11 * b._12 + a._12 * b._22 + a._13 * b._32 + a._14 * b._42;
	m._13 = a._11 * b._13 + a._12 * b._23 + a._13 * b._33 + a._14 * b._43;
	m._14 = a._11 * b._14 + a._12 * b._24 + a._13 * b._34 + a._14 * b._44;

	m._21 = a._21 * b._11 + a._22 * b._21 + a._23 * b._31 + a._24 * b._41;
	m._22 = a._21 * b._12 + a._22 * b._22 + a._23 * b._32 + a._24 * b._42;
	m._23 = a._21 * b._13 + a._22 * b._23 + a._23 * b._33 + a._24 * b._43;
	m._24 = a._21 * b._14 + a._22 * b._24 + a._23 * b._34 + a._24 * b._44;

	m._31 = a._31 * b._11 + a._32 * b._21 + a._33 * b._31 + a._34 * b._41;
	m._32 = a._31 * b._12 + a._32 * b._22 + a._33 * b._32 + a._34 * b._42;
	m._33 = a._31 * b._13 + a._32 * b._23 + a._33 * b._33 + a._34 * b._43;
	m._34 = a._31 * b._14 + a._32 * b._24 + a._33 * b._34 + a._34 * b._44;

	m._41 = a._41 * b._11 + a._42 * b._21 + a._43 * b._31 + a._44 * b._41;
	m._42 = a._41 * b._12 + a._42 * b._22 + a._43 * b._32 + a._44 * b._42;
	m._43 = a._41 * b._13 + a._42 * b._23 + a._43 * b._33 + a._44 * b._43;
	m._44 = a._41 * b._14 + a._42 * b._24 + a._43 * b._34 + a._44 * b._44;
	return m;
}

Vec4 Transform::Multiply(const Mat4& m, const Vec4& p)
{
	return TransformVector(m, p);
}

Mat4 Transform::Scaling(const Vec3& s)
{
	Mat4 m = 
	{
		s.x, 0, 0, 0,
		0, s.y, 0, 0,
		0, 0, s.z, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::Scaling(float s)
{
	return Scaling(Vec3(s, s, s));
}

Mat4 Transform::Translate(const Vec3& p)
{
	Mat4 m = 
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		p.x, p.y, p.z, 1
	};
	return m;
}

Mat4 Transform::Translate(float x, float y, float z)
{
	Mat4 m = 
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		x, y, z, 1
	};
	return m;
}

Vec4 Transform::TransformVector(const Mat4& m, const Vec4& p)
{
	return Vec4(p.x*m._11 + p.y*m._21 + p.z*m._31 + p.w*m._41,
				p.x*m._12 + p.y*m._22 + p.z*m._32 + p.w*m._42,
				p.x*m._13 + p.y*m._23 + p.z*m._33 + p.w*m._43,
				p.x*m._14 + p.y*m._24 + p.z*m._34 + p.w*m._44);
}

Vec3 Transform::TransformPoint(const Mat4& m, const Point3& p)
{
	return Vec3(p.x*m._11 + p.y*m._21 + p.z*m._31 + m._41,
				p.x*m._12 + p.y*m._22 + p.z*m._32 + m._42,
				p.x*m._13 + p.y*m._23 + p.z*m._33 + m._43);
}

Vec3 Transform::TransformNormal(const Mat4& m, const Vec3& p)
{
	return Vec3(p.x*m._11 + p.y*m._21 + p.z*m._31,
				p.x*m._12 + p.y*m._22 + p.z*m._32,
				p.x*m._13 + p.y*m._23 + p.z*m._33);
}

Vec3 Transform::TransformHomogen(const Mat4& m, const Vec3& p)
{
	Vec4 out(p.x*m._11 + p.y*m._21 + p.z*m._31 + m._41,
			p.x*m._12 + p.y*m._22 + p.z*m._32 + m._42,
			p.x*m._13 + p.y*m._23 + p.z*m._33 + m._43,
			p.x*m._14 + p.y*m._24 + p.z*m._34 + m._44);
	return Vec3(out.x / out.w, out.y / out.w, out.z / out.w);
}