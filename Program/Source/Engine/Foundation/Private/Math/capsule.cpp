#include "foundation.h"
#include "Math\capsule.h"
#include "Math\sphere.h"
#include "Math\plane.h"

Capsule::Capsule()
{
	radius = 0.0f;
}

Capsule::Capsule(const Point3 & startPoint, const Point3 & endPoint, float r)
{
    v1 = startPoint;
    v2 = endPoint;
    radius = r;
}

Capsule::Capsule(Capsule&& capsule) noexcept
{
	v1 = capsule.v1;
	v2 = capsule.v2;
	radius = capsule.radius;

	v1 = v2 = Point3::ZERO;
	radius = 0.0f;
}

Capsule::Capsule(const Capsule& capsule)
{
	v1 = capsule.v1;
	v2 = capsule.v2;
	radius = capsule.radius;
}

Capsule& Capsule::operator = (Capsule&& capsule) noexcept
{
	v1 = capsule.v1;
	v2 = capsule.v2;
	radius = capsule.radius;

	v1 = v2 = Point3::ZERO;
	radius = 0.0f;

	return *this;
}

Capsule& Capsule::operator = (const Capsule& capsule)
{
	v1 = capsule.v1;
	v2 = capsule.v2;
	radius = capsule.radius;

	return *this;
}

bool Capsule::operator == (const Capsule& capsule) const
{
	return (v1 == capsule.v1) && (v2 == capsule.v2) && (radius == capsule.radius);
}

float Capsule::SweepTest(const Sphere & sphere, const Vec3 & dir, Plane * out) const
{
//	if (Math::IsNearlyZero(Vec3::Size(v1-v2)) == true)
//		return SweepTest(sphere, dir, Sphere(v1, radius), out);

	float r = (sphere.radius + radius);
	float rr = r * r;

	Vec3 va = (sphere.Center() - v1) - (v2 - v1) * (Vec3::DotProduct(sphere.Center() - v1, v2 - v1) / Vec3::DotProduct(v2 - v1, v2 - v1));
	Vec3 vb = (sphere.Center() + dir - v1) - (v2 - v1) * (Vec3::DotProduct(sphere.Center() + dir - v1, v2 - v1) / Vec3::DotProduct(v2 - v1, v2 - v1));

	float dp_v2_v1 = Vec3::DotProduct(vb - va, vb - va);

	if (Math::Abs<float>(dp_v2_v1) < __EPSILON)
	{	//	v1 == v2 (capsule 에 평행한 경우 직선인 경우)
		if (Vec3::DotProduct(va, va) > rr)
			return 1.0f;
	}
	else
	{
		float tt = Vec3::DotProduct(-va, vb - va) / dp_v2_v1;
		float hh = Vec3::Size(va + (vb - va) * tt);

		if (hh > r)
			return 1.0f;

		tt -= Math::Sqrt(rr - hh*hh) / Vec3::Size(vb - va);

 		if (tt >= 0.0f && tt < 1.0f)
		{
			Vec3 p2 = sphere.Center() + dir * tt;
			Vec3 p1 = p2 - (va + (vb - va) * tt);

			float d1 = Vec3::DotProduct(p1 - v1, v2 - v1);

			if (d1 <= 0)
			{
				Sphere s1(v1, radius);
				return s1.SweepTest(sphere, dir, out);
			}
			if (d1 >= Vec3::DotProduct(v2 - v1, v2 - v1))
			{
				Sphere s2(v2, radius);
				return s2.SweepTest(sphere, dir, out);
			}

			Vec3 norm = Vec3::Normalize(p2 - p1);

			if (out != NULL)
				*out = Plane(norm, radius == 0 ? p1 : p1 + norm*radius);
			return tt;
		}

		if (tt > 1.0f)
			return 1.0f;
	}

    if (Vec3::DotProduct(dir, v2 - v1) < 0.0f)
	{
		Sphere s1(v1, radius);
		return s1.SweepTest(sphere, dir, out);
	}
	else
	{
		Sphere s2(v2, radius);
		return s2.SweepTest(sphere, dir, out);
	}
}

bool Capsule::ContactTest(const Sphere & sphere) const
{
	float r = (sphere.Radius() + radius);
	float rr = r * r;

	float t = Vec3::DotProduct(sphere.Center() - v1, v2 - v1) / Vec3::DotProduct(v2 - v1, v2 - v1);
    if (t < 0.0f)
		t = 0.0f;
	else if (t > 1.0f)
		t = 1.0f;

	return Vec3::Size((sphere.Center() - v1) - (v2 - v1) * t) <= sphere.Radius() ? true : false;
}
