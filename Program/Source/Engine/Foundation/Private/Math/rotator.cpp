#include "foundation.h"
#include "Math\rotator.h"

Rotator::Rotator(const Quat& Q)
{
	const float SingularityTest = Q.z * Q.x - Q.w * Q.y;
	const float YawY = 2.f * (Q.w * Q.z + Q.x * Q.y);
	const float YawX = (1.f - 2.f * (Math::Square(Q.y) + Math::Square(Q.z)));

	// reference 
	// http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/

	// this value was found from experience, the above websites recommend different values
	// but that isn't the case for us, so I went through different testing, and finally found the case 
	// where both of world lives happily. 
	const float SINGULARITY_THRESHOLD = 0.4999995f;
	const float RAD_TO_DEG = (180.f) / _PI;

	float Pitch, Yaw, Roll;

	if (SingularityTest < -SINGULARITY_THRESHOLD)
	{
		Pitch = -90.f;
		Yaw = Math::ATan2(YawY, YawX) * RAD_TO_DEG;
		Roll = Rotator::NormalizeAxis(-Yaw - (2.f * Math::ATan2(Q.x, Q.w) * RAD_TO_DEG));
	}
	else if (SingularityTest > SINGULARITY_THRESHOLD)
	{
		Pitch = 90.f;
		Yaw = Math::ATan2(YawY, YawX) * RAD_TO_DEG;
		Roll = Rotator::NormalizeAxis(Yaw - (2.f * Math::ATan2(Q.x, Q.w) * RAD_TO_DEG));
	}
	else
	{
		Pitch = Math::FastASin(2.f * (SingularityTest)) * RAD_TO_DEG;
		Yaw = Math::ATan2(YawY, YawX) * RAD_TO_DEG;
		Roll = Math::ATan2(-2.f * (Q.w * Q.x + Q.y * Q.z), (1.f - 2.f * (Math::Square(Q.x) + Math::Square(Q.y)))) * RAD_TO_DEG;
	}
}

Vec3 Rotator::Euler() const
{
	return Vec3(Pitch, Yaw, Roll);
}

Rotator Rotator::MakeFromEuler(float pitch, float yaw, float roll)
{
	return Rotator(pitch, yaw, roll);
}

Quat Rotator::Quaternion() const
{
	const float DEG_TO_RAD = _PI / (180.f);
	const float RADS_DIVIDED_BY_2 = DEG_TO_RAD / 2.f;
	float SP, SY, SR;
	float CP, CY, CR;

	const float PitchNoWinding = Math::FMod(Pitch, 360.0f);
	const float YawNoWinding = Math::FMod(Yaw, 360.0f);
	const float RollNoWinding = Math::FMod(Roll, 360.0f);

	Math::SinCos(&SP, &CP, PitchNoWinding * RADS_DIVIDED_BY_2);
	Math::SinCos(&SY, &CY, YawNoWinding * RADS_DIVIDED_BY_2);
	Math::SinCos(&SR, &CR, RollNoWinding * RADS_DIVIDED_BY_2);

	Quat RotationQuat;
	RotationQuat.x = CR * SP * SY - SR * CP * CY;
	RotationQuat.y = -CR * SP * CY - SR * CP * SY;
	RotationQuat.z = CR * CP * SY - SR * SP * CY;
	RotationQuat.w = CR * CP * CY + SR * SP * SY;

	return RotationQuat;
}
