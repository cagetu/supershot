#include "foundation.h"
#include "Math\aabb.h"
#include "Math\matrix.h"
#include "Math\sphere.h"
#include "Math\plane.h"

AABB::AABB()
{
    minX = minY = minZ = 99999.0f;
    maxX = maxY = maxZ = -99999.0f;
}

AABB::AABB(const AABB& aabb)
{
	minX = aabb.minX; minY = aabb.minY; minZ = aabb.minZ;
	maxX = aabb.maxX; maxY = aabb.maxY; maxZ = aabb.maxZ;
}

AABB::AABB(AABB&& aabb) noexcept
{
    minX = aabb.minX; minY = aabb.minY; minZ = aabb.minZ;
    maxX = aabb.maxX; maxY = aabb.maxY; maxZ = aabb.maxZ;
    
    aabb.Empty();
}

AABB::AABB(const Vec3 & min, const Vec3 & max)
{
    minX = min.x, minY = min.y, minZ = min.z;
    maxX = max.x, maxY = max.y, maxZ = max.z;
}

AABB::AABB(const Vec3 & p)
{
    minX = maxX = p.x, minY = maxY = p.y, minZ = maxZ = p.z;
}

AABB::AABB(const Vec3* points, int num)
{
	AABB aabb = points[0];
	for(int i=1; i<num; i++)
		aabb.Expand(points[i]);
	*this = aabb;
}

AABB& AABB::operator = (AABB&& aabb) noexcept
{
	minX = aabb.minX; minY = aabb.minY;	minZ = aabb.minZ;
	maxX = aabb.maxX; maxY = aabb.maxY; maxZ = aabb.maxZ;

	aabb.Empty();
	return *this;
}

AABB& AABB::operator = (const AABB& aabb)
{
	minX = aabb.minX; minY = aabb.minY;	minZ = aabb.minZ;
	maxX = aabb.maxX; maxY = aabb.maxY; maxZ = aabb.maxZ;
	return *this;
}

bool AABB::operator == (const AABB& aabb) const
{
	return (minX == aabb.minX && minY == aabb.minY && minZ == aabb.minZ &&
			maxX == aabb.maxX && maxY == aabb.maxY && maxZ == aabb.maxZ);
}

bool AABB::operator != (const AABB& aabb) const
{
	return !(operator == (aabb));
}

void AABB::Expand(const Vec3 & p)
{
	if (p.x < minX) minX = p.x;
	if (p.x > maxX) maxX = p.x;
	if (p.y < minY) minY = p.y;
	if (p.y > maxY) maxY = p.y;
	if (p.z < minZ) minZ = p.z;
	if (p.z > maxZ) maxZ = p.z;
}

void AABB::Expand(const AABB & bound)
{
	if (bound.minX < minX) minX = bound.minX;
	if (bound.maxX > maxX) maxX = bound.maxX;
	if (bound.minY < minY) minY = bound.minY;
	if (bound.maxY > maxY) maxY = bound.maxY;
	if (bound.minZ < minZ) minZ = bound.minZ;
	if (bound.maxZ > maxZ) maxZ = bound.maxZ;
}

void AABB::Expand(float size)
{
	minX -= size;
	minY -= size;
	minZ -= size;
	maxX += size;
	maxY += size;
	maxZ += size;
}

void AABB::Intersection(const AABB & bound)
{
	if (bound.minX > minX) minX = bound.minX;
	if (bound.maxX < maxX) maxX = bound.maxX;
	if (bound.minY > minY) minY = bound.minY;
	if (bound.maxY < maxY) maxY = bound.maxY;
	if (bound.minZ > minZ) minZ = bound.minZ;
	if (bound.maxZ < maxZ) maxZ = bound.maxZ;
}

bool AABB::IntersectTest(const AABB & bound) const
{
	if (bound.maxX < minX) return false;
	if (bound.minX > maxX) return false;
	if (bound.maxY < minY) return false;
	if (bound.minY > maxY) return false;
	if (bound.maxZ < minZ) return false;
	if (bound.minZ > maxZ) return false;
	return true;
}

bool AABB::IntersectTest(const Sphere& s) const
{
	const Point3& center = s.Center();

	if(center.x < minX && minX - center.x > s.radius ) return false;
	if(center.x > maxX && center.x - maxX > s.radius ) return false;
	if(center.y < minY && minY - center.y > s.radius ) return false;
	if(center.y > maxY && center.y - maxY > s.radius ) return false;
	if(center.z < minZ && minZ - center.z > s.radius ) return false;
	if(center.z > maxZ && center.z - maxZ > s.radius ) return false;
	return true;
}

void AABB::Translate(const Vec3& v)
{
	minX += v.x;
	minY += v.y;
	minZ += v.z;
	maxX += v.x;
	maxY += v.y;
	maxZ += v.z;
}

AABB AABB::Transform(const Mat4 & tm) const
{
	Vec3 p = Min() * tm;
	AABB hull(p, p);
	for(int i=1; i<8; i++)
	{
		p = Vec3((i&1) ? minX : maxX, (i&2) ? minY : maxY, (i&4) ? minZ : maxZ) * tm;
		if (p.x < hull.minX) hull.minX = p.x;
		if (p.x > hull.maxX) hull.maxX = p.x;
		if (p.y < hull.minY) hull.minY = p.y;
		if (p.y > hull.maxY) hull.maxY = p.y;
		if (p.z < hull.minZ) hull.minZ = p.z;
		if (p.z > hull.maxZ) hull.maxZ = p.z;
	}
	return hull;
}

float AABB::RayIntersect(const Point3 & p, const Vec3 & dir, Plane* out) const
{
	if (p.x < minX)
	{
		if (p.x + dir.x < minX)
			return 1.0f;

		float t = (minX - p.x) / dir.x;

		float y = p.y + dir.y * t;
		float z = p.z + dir.z * t;

		if (y >= minY && y <= maxY && z >= minZ && z <= maxZ)
		{
			if (out)
				*out = Plane(-1, 0, 0, minX);
			return t;
		}
	}
	if (p.x > maxX)
	{
		if (p.x + dir.x > maxX)
			return 1.0f;

		float t = (maxX - p.x) / dir.x;

		float y = p.y + dir.y * t;
		float z = p.z + dir.z * t;

		if (y >= minY && y <= maxY && z >= minZ && z <= maxZ)
		{
			if (out)
				*out = Plane(1, 0, 0, maxX);
			return t;
		}
	}


	if (p.y < minY)
	{
		if (p.y + dir.y < minY)
			return 1.0f;

		float t = (minY - p.y) / dir.y;

		float x = p.x + dir.x * t;
		float z = p.z + dir.z * t;

		if (x >= minX && x <= maxX && z >= minZ && z <= maxZ)
		{
			if (out)
				*out = Plane(0, -1, 0, minY);
			return t;
		}
	}
	if (p.y > maxY)
	{
		if (p.y + dir.y > maxY)
			return 1.0f;

		float t = (maxY - p.y) / dir.y;

		float x = p.x + dir.x * t;
		float z = p.z + dir.z * t;

		if (x >= minX && x <= maxX && z >= minZ && z <= maxZ)
		{
			if (out)
				*out = Plane(0, 1, 0, maxY);
			return t;
		}
	}


	if (p.z < minZ)
	{
		if (p.z + dir.z < minZ)
			return 1.0f;

		float t = (minZ - p.z) / dir.z;

		float x = p.x + dir.x * t;
		float y = p.y + dir.y * t;

		if (x >= minX && x <= maxX && y >= minY && y <= maxY)
		{
			if (out)
				*out = Plane(0, 0, -1, minZ);
			return t;
		}
	}
	if (p.z > maxZ)
	{
		if (p.z + dir.z > maxZ)
			return 1.0f;

		float t = (maxZ - p.z) / dir.z;

		float x = p.x + dir.x * t;
		float y = p.y + dir.y * t;

		if (x >= minX && x <= maxX && y >= minY && y <= maxY)
		{
			if (out)
				*out = Plane(0, 0, 1, maxZ);
			return t;
		}
	}

	return 1.0f;
}

void AABB::GetPlanes(Plane* planes) const
{
	// cw 같은데.. ?!?!
	static const int _face[6][4] =
	{
		0, 2, 3, 1, // -z
		1, 3, 7, 5, // +x
		3, 2, 6, 7, // +y
		2, 0, 4, 6, // -x
		0, 1, 5, 4, // -y
		7, 6, 4, 5, // +z
	} ;

	static Vec3 v[8];
	GetPoints(v);

	for(int i=0; i<6; i++)
	{
		planes[i] = Plane(v[_face[i][0]], v[_face[i][1]], v[_face[i][2]]);
		planes[i].Normalize();
	}
}

void AABB::GetPoints(Point3* points) const
{
	/*
			6---------7
		   /|        /|  
	      / |       / |
	     2---------3  |
		 |	4- - - | -5
		 | /       | / 
		 |/        |/
		 0---------1
	*/
	for(int i=0; i<8; i++)
		points[i] = Vec3((i&1) ? maxX : minX, (i&2) ? maxY : minY, (i&4) ? maxZ :minZ);
}

float AABB::Distance(Point3& point) const
{
	float dist = 0.f;
	// x
	{
		if( point.x < minX ) dist += (minX - point.x) * (minX - point.x);
		if( point.x > maxX ) dist += (point.x - maxX) * (point.x - maxX);
	}
	// y
	{
		if( point.y < minY ) dist += (minY - point.y) * (minY - point.y);
		if( point.y > maxY ) dist += (point.y - maxY) * (point.y - maxY);
	}
	// z
	{
		if( point.z < minZ ) dist += (minZ - point.z) * (minZ - point.z);
		if( point.z > maxZ ) dist += (point.z - maxZ) * (point.z - maxZ);
	}
	return dist;
}

bool AABB::BoundTest(const Point3& start, const Point3& direct, float radius) const
{
	float t1=0.0f, t2=1.0f;
	Vec3 dv;
	int i;

	const float* v = &direct.x;
	const float* min = &minX;
	const float* max = &maxX;
	const float* startv = &start.x;

	for(i=0; i<3; i++)
	{
		if (v[i] > 0)
		{
			t1 = Math::Max((min[i] - radius - startv[i]) / v[i], t1);
			t2 = Math::Min((max[i] + radius - startv[i]) / v[i], t2);
		}	else
		if (v[i] < 0)
		{
			t1 = Math::Max((max[i] + radius - startv[i]) / v[i], t1);
			t2 = Math::Min((min[i] - radius - startv[i]) / v[i], t2);
		}	else
		{
			if (min[i] > startv[i] + radius || max[i] < startv[i] - radius)
				return false;
			continue;
		}

		if (t1 > t2)
			return false;
	}

	return true;
}

static int _box_idx[][4] =
{
	0, 2, 6, 4,
	5, 7, 3, 1,
	5, 1, 0, 4,
	3, 7, 6, 2,
	1, 3, 2, 0,
	4, 6, 7, 5,	
} ;

float AABB::SweepTest(const Sphere& pos, const Vec3& vec, Plane* out) const
{
	if (BoundTest(pos.Center(), vec, pos.Radius()) == true)
	{
		Vec3 v[8];
		float t, mint=1.0f;
		Plane pl;
		int i, j;

		for(i=0; i<8; i++)
		{
			v[i] = Vec3((i&1) ? minX : maxX, (i&2) ? minY : maxY, (i&4) ? minZ : maxZ);
		}

		const float* dv = &vec.x;
		for(i=0; i<3; i++)
		{
			int off = dv[i] < 0 ? i*2 : i*2+1;
			Vec3 p[4];
			for(j=0; j<4; j++)
				p[j] = v[_box_idx[off][j]];
			t = Sphere::SweepTest(pos, vec, p, 4, &pl);
			if (t < mint)
			{
				mint = t;
				if (out != NULL)
					*out = pl;
			}
		}

		return mint;
	}
	return 1.0f;
}
