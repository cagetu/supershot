#include "foundation.h"
#include "Math\ray.h"

Ray::Ray(const Point3& orig, const Vec3& dir)
{
    origin = orig;
    direction = dir;
}

/// Ray 상의 한 점을 구한다.
Point3 Ray::GetPoint(float t) const
{
    return origin + t * direction;
}

Ray Ray::MakeFrom(const Point3& startPoint, const Point3& endPoint)
{
    Ray Ray;
    Ray.origin = startPoint;
    Ray.direction = (endPoint - startPoint).Normalize();
    return Ray;
}
