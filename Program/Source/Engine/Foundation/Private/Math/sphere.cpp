#include "foundation.h"
#include "Math\sphere.h"
#include "Math\plane.h"
#include "Math\capsule.h"

float Sphere::SweepTest(const Sphere& sphere, const Vec3& dir, Plane* out) const
{
	Vec3	vd = Center() - sphere.Center();

	float o = Vec3::DotProduct(vd, dir); // oblique

	if (o >= 0.0f)
	{
		float dd = Vec3::DotProduct(dir, dir);
		float t	= o / dd;
		float hh = Vec3::DotProduct(vd, vd) - t * o;
		float rr = (sphere.radius + radius) * (sphere.radius + radius);
        
		if (hh <= rr)
		{
//			if (hh < rr)
				t -= Math::Sqrt((rr - hh) / dd);

			if (t >= 0.0f && t < 1.0f)
			{
				if (out != NULL)
				{
					Vec3 vb = sphere.Center() + dir * t;
					Vec3 vc = vb - Center();
					float vcl = Vec3::Size(vc);
					Vec3 vn = vc * (1.0f / vcl);
					*out = Plane(vn, vb - vc * (sphere.radius / vcl));
				}
				return t;
			}
		}
	}

	return 1.0f;
}

/*
    - https://raytracing.github.io/books/RayTracingInOneWeekend.html#overview
    - https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection
 */
float Sphere::RayIntersect(const Point3& origin, const Vec3& dir, Plane* out) const
{
	Vec3 vd = Center() - origin;

	float o = Vec3::DotProduct(vd, dir); // oblique
	if (o >= 0.0f)
	{
		float dd = Vec3::DotProduct(dir, dir);
		float t	= o / dd;
		float hh = Vec3::DotProduct(vd, vd) - t * o;
		float rr = radius * radius;

		if (hh <= rr)
		{
//			if (hh < rr)
				t -= Math::Sqrt((rr - hh) / dd);

			if (t >= 0.0f && t < 1.0f)
			{
				if (out != NULL)
				{
					Vec3 vb = origin + dir * t;
					Vec3 vc = vb - Center();
					float vcl = Vec3::Size(vc);
					Vec3 vn = vc * (1.0f / vcl);
					*out = Plane(vn, vb);
				}
				return t;
			}
		}
	}

	return 1.0f;
}

bool Sphere::ContactTest(const Sphere& sphere) const
{
	return Vec3::Size(Center() - sphere.Center()) <= sphere.radius+radius ? true : false;
}

bool Sphere::IsInside(const Point3& point) const
{
    return Vec3::Size(Center() - point) <= radius ? true : false;
}

float Sphere::SweepTest(const Sphere & sphere, const Vec3 & dir, const Vec3 * poly, int vtxnum, Plane * out)
{
//	평면에 대해서 절대 충돌 불가능한 경우만 추려낸다
	Vec3	up = Vec3::Normalize(Vec3::CrossProduct(poly[1] - poly[0], poly[2] - poly[0])), p;
	Plane	pl(up, poly[0]);
	float	dd, h, t;
	int		i1, i2;
#ifdef _DEBUG
	int		test = 0;
#endif

	dd = Vec3::DotProduct(up, dir);
	h = pl.Distance(sphere.Center());

	if (h >= sphere.radius)
	{
#ifdef _DEBUG
		if (h + dd > sphere.radius)						//	윗면에 닿을 수가 없음
		{
			test = 3;
			return 1.0f;
		}
#else
		if (h + dd > sphere.radius)						//	윗면에 닿을 수가 없음
			return 1.0f;
#endif
	}	else
	if (h < -sphere.radius)
	{
#ifdef _DEBUG
		if (h + dd < -sphere.radius)						//	아랫면에도 닿을 수가 없음
		{
			test = 2;
//			return 1.0f;
		}
//		if (h + dd < 0)
//		{
//			test = 1;
//		}
#else
		if (h + dd < -sphere.radius)
			return 1.0f;
#endif
	}

//	다각형 안에서 충돌하는 지 검사.
//	완전히 안에 있으면 다각형 평면과 검사, 아닌 경우 각 엣지 캡슐과 검사
	if (dd != 0.0f &&
		h >= sphere.radius && h + dd <= sphere.radius)
	{
//		_ASSERT(dd != 0.0f);

		t	= (h - sphere.radius) / (-dd);
		p	= sphere.Center() + dir * t;

#ifdef _DEBUG
		float ddd = pl.Distance(p) - sphere.radius;
		_ASSERT(Math::Abs(ddd) < 0.01f);
#endif

		for(i1=0, i2=vtxnum-1; i1<vtxnum; i2=i1++)
		{
			if (Vec3::DotProduct(Vec3::CrossProduct(poly[i1] - poly[i2], up), p - poly[i2]) > 0)
				break;
		}

		if (i1 == vtxnum)
		{
			*out = Plane(pl.a, pl.b, pl.c, pl.d);
#ifdef _DEBUG
			_ASSERT(t == 1.0f || test == false);
#endif
			return t;
		}
	}

	if (sphere.radius > 0)
	{
//	각 엣지와 검사
		float mint = 1.0f;
		for(i1=0, i2=vtxnum-1; i1<vtxnum; i2=i1++)
		{
			t = SweepTest(sphere, dir, Capsule(poly[i1], poly[i2], 0.0f), &pl);

			if (t < mint)
			{
#ifdef _DEBUG
				float tttt = pl.Distance(sphere.Center() + dir * t);
				_ASSERT(Math::Abs(tttt - sphere.radius) < 0.01f);
#endif
				mint = t;
				*out = pl;
			}
		}
#ifdef _DEBUG
		_ASSERT(mint == 1.0f || test == 0);
#endif
		return mint;
	}
	return 1.0f;
}

float Sphere::SweepTest(const Sphere & sphere, const Vec3 & dir, const Capsule & capsule, Plane * out)
{
#if 0
//	if (Math::IsEqual(capsule.v1, capsule.v2) == true)
//	if (Math::IsZero(Vec3::Size(capsule.v1 -capsule.v2)) == true)
//		return SweepTest(sphere, dir, Sphere(capsule.v1, capsule.radius), out);

//	if (sphere.radius + capsule.radius == 0)
//		return 1.0f;
	float r = (sphere.radius + capsule.radius);
	float rr = r * r;

	Vec3 v1 = (sphere.Center() - capsule.v1) - (capsule.v2 - capsule.v1) * Vec3::DotProduct(sphere.Center() - capsule.v1, capsule.v2 - capsule.v1) * (1.0f / Vec3::DotProduct(capsule.v2 - capsule.v1, capsule.v2 - capsule.v1));
	Vec3 v2 = (sphere.Center() + dir - capsule.v1) - (capsule.v2-capsule.v1) * Vec3::DotProduct(sphere.Center() + dir-capsule.v1, capsule.v2 - capsule.v1) * (1.0f / Vec3::DotProduct(capsule.v2 - capsule.v1, capsule.v2 - capsule.v1));

	//_ASSERT(util::abs(Vec3::DotProduct(v1, capsule.v2-capsule.v1) / Vec3::Size(v1) / Vec3::Size(capsule.v2-capsule.v1)) < 0.01f);
	//_ASSERT(util::abs(Vec3::DotProduct(v2, capsule.v2-capsule.v1) / Vec3::Size(v2) / Vec3::Size(capsule.v2-capsule.v1)) < 0.01f);

#ifdef _DEBUG
	int _debug = 0;
#endif
	float dp_v2_v1 = Vec3::DotProduct(v2 - v1, v2 - v1);

	if (util::abs(dp_v2_v1) < 0.00001f)
	{	//	v1 == v2
		//	capsule 에 평행한 경우 직선인 경우
		float hh = Vec3::DotProduct(v1, v1);//Size(v1);
		if (hh > rr)
		{
#ifdef _DEBUG
			_debug = 1;
#else
			return 1.0f;
#endif
		}
	}
	else
	{
		float tt = Vec3::DotProduct(-v1, v2 - v1) / dp_v2_v1;
		float hh = Vec3::Size(v1 + (v2 - v1) * tt);

		if (hh > r)
		{
#ifdef _DEBUG
		//	_ASSERT(SweepTest(sphere, direct, Sphere(capsule.v1, capsule.radius), out) == 1.0f);
		//	_ASSERT(SweepTest(sphere, direct, Sphere(capsule.v2, capsule.radius), out) == 1.0f);
#endif
			return 1.0f;
		}

		tt -= sqrt(rr - hh*hh) / Vec3::Size(v2 - v1);

 		if (tt >= 0.0f && tt < 1.0f)
		{
			Vec3 p2 = sphere.Center() + dir * tt;
			Vec3 p1 = p2 - (v1 + (v2 - v1) * tt);

			float d1 = Vec3::DotProduct(p1 - capsule.v1, capsule.v2 - capsule.v1);
			if (d1 <= 0)
			{
				return SweepTest(sphere, dir, Sphere(capsule.v1, capsule.radius), out);
			}
			if (d1 >= Vec3::DotProduct(capsule.v2 - capsule.v1, capsule.v2 - capsule.v1))
			{
				return SweepTest(sphere, dir, Sphere(capsule.v2, capsule.radius), out);
			}

			Vec3 norm = Vec3::Normalize(p2 - p1);

			*out = Plane(norm, capsule.radius == 0 ? p1 : p1 + norm * capsule.radius);
			return tt;
		}

		if (tt > 1.0f)
		{
#ifdef _DEBUG
			_debug = 2;
#else
			return 1.0f;
#endif
		}
	}

	float t;

	if (Vec3::DotProduct(dir, capsule.v2 - capsule.v1) < 0)
	{
		t = SweepTest(sphere, dir, Sphere(capsule.v1, capsule.radius), out);
	}
	else
	{
		t = SweepTest(sphere, dir, Sphere(capsule.v2, capsule.radius), out);
	}
#ifdef _DEBUG
	if (_debug != 0)
	{
//		_ASSERT(t == 1.0f);
	}
#endif
	return t;
#else
	return capsule.SweepTest(sphere, dir, out);
#endif
}
