#include "foundation.h"
#include "Math\plane.h"
#include "Math\matrix.h"

Plane::Plane(Plane&& plane) noexcept
{
	a = plane.a;
	b = plane.b;
	c = plane.c;
	d = plane.d;

	plane.a = plane.b = plane.c = plane.d = 0.0f;
}

Plane::Plane(const Plane& plane)
{
	a = plane.a;
	b = plane.b;
	c = plane.c;
	d = plane.d;
}

Plane::Plane(const Point3 &v1, const Point3 &v2, const Point3 &v3)
{
	Vec3 norm = Vec3::Normalize(Vec3::CrossProduct(v2-v1, v3-v1));

	a = norm.x;
	b = norm.y;
	c = norm.z;
	d = -Vec3::DotProduct(norm, v1);
}

Plane& Plane::operator = (Plane&& plane) noexcept
{
	a = plane.a;
	b = plane.b;
	c = plane.c;
	d = plane.d;

	plane.a = plane.b = plane.c = plane.d = 0.0f;

	return *this;
}

Plane& Plane::operator = (const Plane& plane)
{
	a = plane.a;
	b = plane.b;
	c = plane.c;
	d = plane.d;

	return *this;
}

Plane Plane::Transform(const Mat4 & tm) const
{
	Vec3 va(-d * a, -d * b, -d*c);
	Vec3 vb = va * tm;
	Vec3 vv = tm.TransformNormal(Normal());
	return Plane(vv, -Vec3::DotProduct(vv, vb));
}

void Plane::Normalize()
{
	float invSqrt = 1.0f / Math::Sqrt(a*a + b*b + c*c);
	a *= invSqrt;
	b *= invSqrt;
	c *= invSqrt;
	d *= invSqrt;
}