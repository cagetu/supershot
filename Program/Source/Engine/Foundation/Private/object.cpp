// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "foundation.h"
#include "object.h"
#include "Container\map.h"

//------------------------------------------------------------------
__ImplementRootRtti( IObject );
//------------------------------------------------------------------

static ObjectID ms_ulNextID = 0;				//!< 다음 객체에 할당할 ID
static Map<ObjectID, IObject*> ms_InUsed;		//!< 생성된 객체 리스트 (원하는 시점에서 객체의 존재 여부를 알아온다.)

//------------------------------------------------------------------
//Const/Dest
IObject::IObject()
{
	__ConstructReference;
	m_ulID = ms_ulNextID++;
	ms_InUsed.Insert(m_ulID, this);
}
//------------------------------------------------------------------
IObject::~IObject()
{
	__DestructReference;
	IndexT index = ms_InUsed.FindIndex(m_ulID);
	CHECK(index > INVALID_INDEX, TEXT("IObject::~IObject"));
	ms_InUsed.RemoveAt(index);
}
