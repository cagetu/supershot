#include "foundation.h"
#include "variant.h"
#include "Math\vector.h"
#include "Math\matrix.h"

Variant Variant::Null;

Variant::Variant()
	: m_Type(EType::Void)
{
}

Variant::Variant(Variant&& rhs) noexcept
{
	m_Type = rhs.m_Type;
	rhs.m_Type = EType::Void;

	switch (m_Type)
	{
	case EType::Void:
		break;
	case EType::Bool:
		m_Value.Bool = rhs.m_Value.Bool;
		break;
	case EType::Int:
		{
			m_Value.Int = rhs.m_Value.Int;
			rhs.m_Value.Int = 0;
		}
		break;
	case EType::Float:
		{
			m_Value.Float[0] = rhs.m_Value.Float[0];
			rhs.m_Value.Float[0] = 0.0f;
		}
		break;
	case EType::Vector2:
	case EType::Vector3:
	case EType::Vector4:
		{
			Memory::MemCpy(m_Value.Float, rhs.m_Value.Float, sizeof(float)*4);
			Memory::MemZero(rhs.m_Value.Float);
		}
		break;
	case EType::Matrix3:
	case EType::Matrix4:
		{
			Memory::MemCpy(m_Value.Matrix, rhs.m_Value.Matrix, sizeof(Mat4));
			Memory::MemCpy(rhs.m_Value.Matrix, &Mat4::IDENTITY._11, sizeof(Mat4));
		}
		break;
	case EType::Texture:
		//m_Value.Texture = rhs.m_Value.Texture;
		break;
	case EType::FloatArray:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Floats = rhs.m_Value.Array.Floats;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Floats = nullptr;
		}
		break;
	case EType::Vector2Array:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Vector2 = rhs.m_Value.Array.Vector2;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Vector2 = nullptr;
		}
		break;
	case EType::Vector3Array:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Vector3 = rhs.m_Value.Array.Vector3;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Vector3 = nullptr;
		}
		break;
	case EType::Vector4Array:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Vector4 = rhs.m_Value.Array.Vector4;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Vector4 = nullptr;
		}
		break;
	case EType::MatrixArray:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Matrixes = rhs.m_Value.Array.Matrixes;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Matrixes = nullptr;
		}
		break;
	default:
		WARNNING(TEXT("Variant ="));
		break;
	}
}

Variant::Variant(const Variant& rhs)
{
	m_Type = rhs.m_Type;

	switch (m_Type)
	{
	case EType::Void:
		break;
	case EType::Bool:
		m_Value.Bool = rhs.m_Value.Bool;
		break;
	case EType::Int:
		m_Value.Int = rhs.m_Value.Int;
		break;
	case EType::Float:
		m_Value.Float[0] = rhs.m_Value.Float[0];
		break;
	case EType::Vector2:
	case EType::Vector3:
	case EType::Vector4:
		m_Value.Float[0] = rhs.m_Value.Float[0];
		m_Value.Float[1] = rhs.m_Value.Float[1];
		m_Value.Float[2] = rhs.m_Value.Float[2];
		m_Value.Float[3] = rhs.m_Value.Float[3];
		break;
	case EType::Matrix3:
	case EType::Matrix4:
		Memory::MemCpy(m_Value.Matrix, rhs.m_Value.Matrix, sizeof(Mat4));
		break;
	case EType::Texture:
		//m_Value.Texture = rhs.m_Value.Texture;
		break;
	case EType::FloatArray:
		Duplicate(rhs.m_Value.Array.Floats, sizeof(float), rhs.m_Value.Array.Count);
		break;
	case EType::Vector2Array:
		Duplicate(rhs.m_Value.Array.Vector2, sizeof(Vec2), rhs.m_Value.Array.Count);
		break;
	case EType::Vector3Array:
		Duplicate(rhs.m_Value.Array.Vector3, sizeof(Vec3), rhs.m_Value.Array.Count);
		break;
	case EType::Vector4Array:
		Duplicate(rhs.m_Value.Array.Vector4, sizeof(Vec4), rhs.m_Value.Array.Count);
		break;
	case EType::MatrixArray:
		Duplicate(rhs.m_Value.Array.Matrixes, sizeof(Mat4), rhs.m_Value.Array.Count);
		break;
	default:
		WARNNING(TEXT("Variant ="));
		break;
	}
}

Variant::Variant(const Mat3& val) : m_Type(EType::Matrix3)
{
	m_Value.Matrix[0] = val._11;  m_Value.Matrix[1] = val._12;  m_Value.Matrix[2] = val._13;	m_Value.Matrix[3] = 0.0f;
	m_Value.Matrix[4] = val._21;  m_Value.Matrix[5] = val._22;  m_Value.Matrix[6] = val._23;	m_Value.Matrix[7] = 0.0f;
	m_Value.Matrix[8] = val._31;  m_Value.Matrix[9] = val._32;  m_Value.Matrix[10] = val._33;	m_Value.Matrix[11] = 0.0f;
	m_Value.Matrix[12] = 0.0f;	  m_Value.Matrix[13] = 0.0f;	m_Value.Matrix[14] = 0.0f;		m_Value.Matrix[15] = 1.0f;
}

Variant::Variant(const Mat4& val) : m_Type(EType::Matrix4)
{
	Memory::MemCpy(m_Value.Matrix, &val._11, sizeof(Mat4));
}

Variant& Variant::operator = (Variant&& rhs) noexcept
{
	Delete();

	m_Type = rhs.m_Type;
	rhs.m_Type = EType::Void;

	switch (m_Type)
	{
	case EType::Void:
		break;
	case EType::Bool:
		m_Value.Bool = rhs.m_Value.Bool;
		break;
	case EType::Int:
		{
			m_Value.Int = rhs.m_Value.Int;
			rhs.m_Value.Int = 0;
		}
		break;
	case EType::Float:
		{
			m_Value.Float[0] = rhs.m_Value.Float[0];
			rhs.m_Value.Float[0] = 0.0f;
		}
		break;
	case EType::Vector2:
	case EType::Vector3:
	case EType::Vector4:
		{
			Memory::MemCpy(m_Value.Float, rhs.m_Value.Float, sizeof(float) * 4);
			Memory::MemZero(rhs.m_Value.Float);
		}
		break;
	case EType::Matrix3:
	case EType::Matrix4:
		{
			Memory::MemCpy(m_Value.Matrix, rhs.m_Value.Matrix, sizeof(Mat4));
			Memory::MemCpy(rhs.m_Value.Matrix, &Mat4::IDENTITY._11, sizeof(Mat4));
		}
		break;
	case EType::Texture:
		//m_Value.Texture = rhs.m_Value.Texture;
		break;
	case EType::FloatArray:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Floats = rhs.m_Value.Array.Floats;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Floats = nullptr;
		}
		break;
	case EType::Vector2Array:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Vector2 = rhs.m_Value.Array.Vector2;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Vector2 = nullptr;
		}
		break;
	case EType::Vector3Array:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Vector3 = rhs.m_Value.Array.Vector3;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Vector3 = nullptr;
		}
		break;
	case EType::Vector4Array:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Vector4 = rhs.m_Value.Array.Vector4;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Vector4 = nullptr;
		}
		break;
	case EType::MatrixArray:
		{
			m_Value.Array.Count = rhs.m_Value.Array.Count;
			m_Value.Array.Matrixes = rhs.m_Value.Array.Matrixes;
			rhs.m_Value.Array.Count = 0;
			rhs.m_Value.Array.Matrixes = nullptr;
		}
		break;
	default:
		WARNNING(TEXT("Variant ="));
		break;
	}

	return *this;
}

Variant & Variant::operator = ( const Variant& rhs )
{
	Delete();

	m_Type = rhs.m_Type;

	switch (m_Type)
	{
		case EType::Void:
			break;
		case EType::Bool:
			m_Value.Bool = rhs.m_Value.Bool;
			break;
		case EType::Int:
			m_Value.Int = rhs.m_Value.Int;
			break;
		case EType::Float:
			m_Value.Float[0] = rhs.m_Value.Float[0];
			break;
		case EType::Vector2:
		case EType::Vector3:
		case EType::Vector4:
			m_Value.Float[0] = rhs.m_Value.Float[0];
			m_Value.Float[1] = rhs.m_Value.Float[1];
			m_Value.Float[2] = rhs.m_Value.Float[2];
			m_Value.Float[3] = rhs.m_Value.Float[3];
			break;
		case EType::Matrix3:
		case EType::Matrix4:
			Memory::MemCpy(m_Value.Matrix, rhs.m_Value.Matrix, sizeof(Mat4));
			break;
		case EType::Texture:
			//m_Value.Texture = rhs.m_Value.Texture;
			break;
		case EType::FloatArray:
			Duplicate(rhs.m_Value.Array.Floats, sizeof(float), rhs.m_Value.Array.Count);
			break;
		case EType::Vector2Array:
			Duplicate(rhs.m_Value.Array.Vector2, sizeof(Vec2), rhs.m_Value.Array.Count);
			break;
		case EType::Vector3Array:
			Duplicate(rhs.m_Value.Array.Vector3, sizeof(Vec3), rhs.m_Value.Array.Count);
			break;
		case EType::Vector4Array:
			Duplicate(rhs.m_Value.Array.Vector4, sizeof(Vec4), rhs.m_Value.Array.Count);
			break;
		case EType::MatrixArray:
			Duplicate(rhs.m_Value.Array.Matrixes, sizeof(Mat4), rhs.m_Value.Array.Count);
			break;
		default:
			WARNNING(TEXT("Variant ="));
			break;
	}
	return *this;
}

bool Variant::operator == ( const Variant& rhs ) const
{
	if (rhs.m_Type == m_Type)
	{
		switch (rhs.m_Type)
		{
		case EType::Void:
			return true;
		case EType::Bool:
			return (m_Value.Bool == rhs.m_Value.Bool);
		case EType::Int:
			return (m_Value.Int == rhs.m_Value.Int);
		case EType::Float:
			return (m_Value.Float[0] == rhs.m_Value.Float[0]);
		case EType::Vector2:
		case EType::Vector3:
		case EType::Vector4:
			return !memcmp(m_Value.Float, rhs.m_Value.Float, sizeof(float)*4);
		case EType::Texture:
			//return (m_Value.Texture == rhs.m_Value.Texture);
		case EType::Matrix3 :
		case EType::Matrix4:
			return !memcmp(&m_Value.Matrix, &rhs.m_Value.Matrix, sizeof(Mat4));
		case EType::FloatArray :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Floats, rhs.m_Value.Array.Floats, sizeof(float)*Count()) ? true : false;
		case EType::Vector2Array:
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Vector2, rhs.m_Value.Array.Vector2, sizeof(Vec2)*Count()) ? true : false;
		case EType::Vector3Array :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Vector3, rhs.m_Value.Array.Vector3, sizeof(Vec3)*Count()) ? true : false;
		case EType::Vector4Array :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Vector4, rhs.m_Value.Array.Vector4, sizeof(Vec4)*Count()) ? true : false;
		case EType::MatrixArray :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Matrixes, rhs.m_Value.Array.Matrixes, sizeof(Mat4)*Count()) ? true : false;
		default:
			WARNNING(TEXT("Variant =="));
			return false;
		}
	}
	return false;
}

Variant::operator const Mat3() const
{
	ASSERT(m_Type == EType::Matrix3);

	return Mat3(m_Value.Matrix[0], m_Value.Matrix[1], m_Value.Matrix[2],
				m_Value.Matrix[4], m_Value.Matrix[5], m_Value.Matrix[6],
				m_Value.Matrix[8], m_Value.Matrix[9], m_Value.Matrix[10]);
}

void Variant::Delete()
{
    if (EType::FloatArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
			delete[] (m_Value.Array.Floats);
    }
    else if (EType::Vector2Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec2) > minsize)
			delete[] (m_Value.Array.Vector2);
    }
    else if (EType::Vector3Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec3) > minsize)
			delete[] (m_Value.Array.Vector3);
    }
    else if (EType::Vector4Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec4) > minsize)
			delete[] (m_Value.Array.Vector4);
    }
    else if (EType::MatrixArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
			delete[] (m_Value.Array.Matrixes);
    }
	m_Type = EType::Void;
}

void Variant::Reset()
{
    if (EType::FloatArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Floats[i] = 0.0f;
		}
    }
    else if (EType::Vector2Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec2) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector2[i].x = m_Value.Array.Vector2[i].y = 0.0f;
		}
    }
    else if (EType::Vector3Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec3) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector3[i].x = m_Value.Array.Vector3[i].y = m_Value.Array.Vector3[i].z = 0.0f;
		}
    }
    else if (EType::Vector4Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec4) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector4[i].x = m_Value.Array.Vector4[i].y = m_Value.Array.Vector4[i].z = m_Value.Array.Vector4[i].w = 0.0f;
		}
    }
    else if (EType::MatrixArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Matrixes[i] = Mat4::IDENTITY;
		}
    }
}

void Variant::Duplicate(const void * ptr, int32 pitch, int32 num)
{
	const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
	uint32 size = pitch * num;

	m_Value.Array.Count = num;
	m_Value.Array.ptr = size <= minsize ? &m_Value.Array._empty : (void*)(new char [size]);
	PlatformUtil::MemCpy(m_Value.Array.ptr, ptr, size);
}

//float _ttt23232[11];
//Variant _ttt2312(_ttt23232, 11);