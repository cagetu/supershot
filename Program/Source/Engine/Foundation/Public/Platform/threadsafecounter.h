#pragma once

class ThreadSafeCounter
{
public:
	ThreadSafeCounter()
	{
		m_Count = 0;
	}
	ThreadSafeCounter(const ThreadSafeCounter& copy)
	{
		m_Count = copy.GetCount();
	}
	ThreadSafeCounter(int32 value)
	{
		m_Count = value;
	}

	// 카운트를 증가하고 새로운 값을 반환한다.
	int32 Increment()
	{
		return Atomics::InterlockedIncrement(&m_Count);
	}

	// 카운트를 감소하고 새로운 값을 반환한다.
	int32 Decrement()
	{
		return Atomics::InterlockedDecrement(&m_Count);
	}

	// amount 만큼 증가하고 이전 값을 반환한다.
	int32 Add(int32 amount)
	{
		return Atomics::InterlockedAdd(&m_Count, amount);
	}

	// amount 만큼 감소시키고 이전 값을 반환한다.
	int32 Subtract(int32 amount)
	{
		return Atomics::InterlockedAdd(&m_Count, -amount);
	}

	// 특정 값으로 값을 설정하고 이전 값을 반환한다.
	int32 Set(int32 value)
	{
		return Atomics::InterlockedExchange(&m_Count, value);
	}

	// 카운터를 0으로 리셋
	int32 Reset()
	{
		return Atomics::InterlockedExchange(&m_Count, 0);
	}

	// 현재 값을 얻어온다.
	int32 GetCount() const
	{
		return Atomics::InterlockedCompareExchange(&const_cast<ThreadSafeCounter*>(this)->m_Count, 0, 0);
	}

private:
	/** Hidden on purpose as usage wouldn't be thread safe. */
	void operator=(const ThreadSafeCounter& Other) {}

	volatile int32 m_Count;
};