#pragma once

class IOSMutex : public IMutex
{
public:
	IOSMutex();
	virtual ~IOSMutex();

	void Lock() override;
	void Unlock() override;

private:
	mutable pthread_mutex_t m_Mutex;
};