#pragma once

struct IOSTypes : public TypeDefines
{
	typedef size_t				SIZE_T;
	typedef decltype(NULL)		TYPE_NULL;
	typedef char16_t			CHAR16;
	typedef char16_t			widechar;
	typedef widechar			TCHAR;
};

typedef IOSTypes PlatformTypes;

#ifndef BYTE
typedef unsigned char       BYTE;
#endif

