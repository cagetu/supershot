#pramga once

struct IOSSys : public BaseSys
{
	static void DebugOut(const wchar* msg);
	static void DebugOutFormat(const wchar* format, ...);

	static FILE * fopen(const wchar* fname, const wchar* mode);
	static FILE * fopen(const char* fname, const char* mode);
	void fclose(FILE * fp);

	static int remove(const wchar* fname);
	static int remove(const char* fname);

	static int rename(const wchar* foldname, const wchar* fnewname);
	static int rename(const char* foldname, const char* fnewname);

	static int access(const wchar* fname, int accmode);
	static int access(const char* fname, int accmode);
};

typedef IOSSys PlatformSys;
