// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

namespace Memory
{
	template <typename DestinationElementType, typename SourceElementType>
	struct TCanBitwiseRelocate
	{
		enum
		{
			Value =
				TOr<
					TAreTypesEqual<DestinationElementType, SourceElementType>,
					TAnd<
						TIsBitwiseConstructible<DestinationElementType, SourceElementType>,
						TIsTriviallyDestructible<SourceElementType>
					>
				>::Value
		};
	};

	template <class ElementType>
	static FORCEINLINE void MemZero(ElementType& src)
	{
		PlatformUtil::MemZero(&src, sizeof(src));
	}

	template <class ElementType>
	static FORCEINLINE void MemSet(ElementType& src, int32 val)
	{
		PlatformUtil::MemSet(src, val, sizeof(src));
	}

	static FORCEINLINE void MemZero(void* dest, size_t size)
	{
		PlatformUtil::MemZero(dest, size);
	}

	static FORCEINLINE void MemCpy(void* dest, const void* src, size_t size)
	{
		PlatformUtil::MemCpy(dest, src, size);
	}

	static FORCEINLINE int32 MemCmp(const void* dest, const void* src, size_t size)
	{
		return PlatformUtil::MemCmp(dest, src, size);
	}

	static FORCEINLINE void MemMove(void* dest, const void* src, size_t size)
	{
		PlatformUtil::MemMove(dest, src, size);
	}

	template <typename ElementType>
	static FORCEINLINE const ElementType Align(const ElementType Ptr, uint64 Alignment)
	{
		static_assert(TIsIntegral<ElementType>::Value || TIsPointer<ElementType>::Value, "IsAligned expects an integer or pointer type");

		return (ElementType)(((PTRINT)Ptr + Alignment - 1) & ~(Alignment - 1));
	}

	template <typename ElementType>
	static FORCEINLINE constexpr bool IsAligned(ElementType Val, uint64 Alignment)
	{
		static_assert(TIsIntegral<ElementType>::Value || TIsPointer<ElementType>::Value, "IsAligned expects an integer or pointer type");

		return !((uint64)Val & (Alignment - 1));
	}

	template <class ElementType>
	static FORCEINLINE ElementType* Construct(void* ptr)
	{
		return new (ptr) ElementType();
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<!TIsTriviallyDestructible<ElementType>::Value>::Type Destruct(ElementType* ptr)
	{
		// We need a typedef here because VC won't compile the destructor call below if ElementType itself has a member called ElementType
		typedef ElementType DestructItemsElementTypeTypedef;

		ptr->DestructItemsElementTypeTypedef::~DestructItemsElementTypeTypedef();
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<TIsTriviallyDestructible<ElementType>::Value>::Type Destruct(ElementType* ptr)
	{
	}

	/**
	 * Default constructs a range of items in memory.
	 *
	 * @param	Elements	The address of the first memory location to construct at.
	 * @param	Count		The number of elements to destruct.
	 */
	template <typename ElementType, typename SizeType>
	FORCEINLINE typename TEnableIf<!TIsZeroConstructType<ElementType>::Value>::Type DefaultConstructItems(void* Address, SizeType Count)
	{
		ElementType* Element = (ElementType*)Address;
		while (Count)
		{
			new (Element) ElementType;
			++Element;
			--Count;
		}
	}


	template <typename ElementType, typename SizeType>
	FORCEINLINE typename TEnableIf<TIsZeroConstructType<ElementType>::Value>::Type DefaultConstructItems(void* Elements, SizeType Count)
	{
		PlatformUtil::MemSet(Elements, 0, sizeof(ElementType) * Count);
	}

	template <class ElementType>
	static FORCEINLINE void ConstructItems(void* address, SizeT count)
	{
		ElementType* element = (ElementType*)address;
		while (count)
		{
			new (element) ElementType;
			++element;
			--count;
		}
	}

	template <class ElementType>
	static FORCEINLINE void ConstructItems(void* dest, const ElementType* src, SizeT count)
	{
		while (count)
		{
			new (dest) ElementType(*src);
			++(ElementType*&)dest;
			++src;
			--count;
		}
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<!TIsTriviallyDestructible<ElementType>::Value>::Type DestructItems(ElementType* elements, SizeT count)
	{
		while (count)
		{
			// We need a typedef here because VC won't compile the destructor call below if ElementType itself has a member called ElementType
			typedef ElementType DestructItemsElementTypeTypedef;

			elements->DestructItemsElementTypeTypedef::~DestructItemsElementTypeTypedef();
			++elements;
			--count;
		}
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<TIsTriviallyDestructible<ElementType>::Value>::Type DestructItems(ElementType* elements, SizeT count)
	{
	}

	/**
	 * Relocates a range of items to a new memory location as a new type. This is a so-called 'destructive move' for which
	 * there is no single operation in C++ but which can be implemented very efficiently in general.
	 *
	 * @param	Dest		The memory location to relocate to.
	 * @param	Source		A pointer to the first item to relocate.
	 * @param	Count		The number of elements to relocate.
	 */
	template <class ElementType>
	static FORCEINLINE typename TEnableIf<!TCanBitwiseRelocate<ElementType, ElementType>::Value>::Type RelocateConstructItems(void* dest, const ElementType* src, SizeT count)
	{
		while (count)
		{
			// We need a typedef here because VC won't compile the destructor call below if SourceElementType itself has a member called SourceElementType
			typedef ElementType RelocateConstructItemsElementTypeTypedef;

			new (dest) ElementType(*src);
			++(ElementType*&)dest;
			(src++)->RelocateConstructItemsElementTypeTypedef::~RelocateConstructItemsElementTypeTypedef();
			--count;
		}
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<TCanBitwiseRelocate<ElementType, ElementType>::Value>::Type RelocateConstructItems(void* dest, const ElementType* src, SizeT count)
	{
		/* All existing UE containers seem to assume trivial relocatability (i.e. memcpy'able) of their members,
		 * so we're going to assume that this is safe here.  However, it's not generally possible to assume this
		 * in general as objects which contain pointers/references to themselves are not safe to be trivially
		 * relocated.
		 *
		 * However, it is not yet possible to automatically infer this at compile time, so we can't enable
		 * different (i.e. safer) implementations anyway. */

		Memory::MemMove(dest, src, sizeof(ElementType) * count);
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<!TIsTriviallyCopyConstructible<ElementType>::Value>::Type MoveConstructItems(void* dest, const ElementType* src, SizeT count)
	{
		while (count)
		{
			new (dest) ElementType((ElementType&&)*src);
			++(ElementType*&)dest;
			++src;
			--count;
		}
	}

	template <class ElementType>
	static FORCEINLINE typename TEnableIf<TIsTriviallyCopyConstructible<ElementType>::Value>::Type MoveConstructItems(void* dest, const ElementType* src, SizeT count)
	{
		Memory::MemMove(dest, src, sizeof(ElementType) * count);
	}

	/**
	 * Move assigns a range of items.
	 *
	 * @param	Dest		The memory location to start move assigning to.
	 * @param	Source		A pointer to the first item to move assign.
	 * @param	Count		The number of elements to move assign.
	 */
	template <typename ElementType>
	FORCEINLINE typename TEnableIf<!TIsTriviallyCopyAssignable<ElementType>::Value>::Type MoveAssignItems(ElementType* Dest, const ElementType* Source, SizeT Count)
	{
		while (Count)
		{
			*Dest = (ElementType&&)*Source;
			++Dest;
			++Source;
			--Count;
		}
	}

	template <typename ElementType>
	FORCEINLINE typename TEnableIf<TIsTriviallyCopyAssignable<ElementType>::Value>::Type MoveAssignItems(ElementType* Dest, const ElementType* Source, SizeT Count)
	{
		FMemory::Memmove(Dest, Source, sizeof(ElementType) * Count);
	}

	template <typename ElementType>
	FORCEINLINE typename TEnableIf<TTypeTraits<ElementType>::IsBytewiseComparable, bool>::Type CompareItems(const ElementType* A, const ElementType* B, int32 Count)
	{
		return !Memory::MemCmp(A, B, sizeof(ElementType) * Count);
	}

	template <typename ElementType>
	FORCEINLINE typename TEnableIf<!TTypeTraits<ElementType>::IsBytewiseComparable, bool>::Type CompareItems(const ElementType* A, const ElementType* B, int32 Count)
	{
		while (Count)
		{
			if (!(*A == *B))
			{
				return false;
			}

			++A;
			++B;
			--Count;
		}
		return true;
	}

}
