// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

/*	출력 메시지

	1. 콘솔 메시지
	2. 로그 메시지 (디버그 메시지)
	3. 에러 메시지
*/

class	IOutputConsole
{
public :
	IOutputConsole() {}
	virtual ~IOutputConsole() {}

	virtual void Open() {}
	virtual void Close() {}

	virtual bool IsShow() const { return false; }

	virtual void Clear() {}
	virtual void SetColor(RGBA color, bool bIntensity = false) {}
	virtual void Print(const char* msg) {}
	virtual void Print(const wchar* msg) {}
} ;

