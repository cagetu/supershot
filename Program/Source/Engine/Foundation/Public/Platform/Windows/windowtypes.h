#pragma once

#include <windows.h>

struct WindowsTypes : public TypeDefines
{
#ifdef _WIN64
	typedef unsigned __int64	SIZE_T;
#else
	typedef unsigned long		SIZE_T;
#endif
};

typedef WindowsTypes PlatformTypes;

#pragma warning(disable : 4481) // nonstandard extension used: override specifier 'override'
#define OVERRIDE override
#if defined(__clang__) || _MSC_VER >= 1900
#define CONSTEXPR constexpr
#else
#define CONSTEXPR
#endif
#define FINAL sealed
#define ABSTRACT abstract

// Function type macros.
#define VARARGS     __cdecl					/* Functions with variable arguments */
//#define CDECL	    __cdecl					/* Standard C function */
#define STDCALL		__stdcall				/* Standard calling convention */
#define FORCEINLINE __forceinline			/* Force code to be inline */
#define FORCENOINLINE __declspec(noinline)	/* Force code to NOT be inline */

// disable this now as it is annoying for generic platform implementations
#pragma warning(disable : 4100) // unreferenced formal parameter

#define PLATFORM_64BITS	0
