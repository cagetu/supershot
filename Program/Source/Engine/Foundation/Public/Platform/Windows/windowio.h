#pragma once

class	MemoryMappedIO : public MemoryIO
{
public:
	~MemoryMappedIO();

	int32 Read(uint8 * output, int32 len);

	static MemoryMappedIO* Open(const wchar* fname);
	static MemoryMappedIO* Close(MemoryMappedIO* file);

protected:
	MemoryMappedIO(HANDLE hFile, HANDLE hMapFile, int32 len);
	bool Load(uint32 offset, uint32 length);
	void Unload();

private:
	void * m_pAddress;
	HANDLE m_hMapFile, m_hFile;
};

class	SyncedIO : public IArchive
{
public:
	SyncedIO(IArchive *f, int32 off, int32 len, CriticalSection* cs);

	int32 Read(uint8 * output, int32 len);
	int32 Seek(int32 offset, _SEEK pos);
	uint32 GetSize() const { return m_Length; }

protected:
	uint32 m_Length;
	uint32 m_Pivot;
	uint32 m_Offset;

	IArchive* m_fp;
	CriticalSection* m_CS;
};

/*
class	CAsyncStdIO : public IArchive
{
public:
	~CAsyncStdIO();

	int32 Read(uint8 * output, int32 len);
	int32 Seek(int32 offset, _SEEK pos);
	int32 GetSize() const { return m_Length; }

	static CAsyncStdIO* Open(const TCHAR * fname);

private:
	CAsyncStdIO(void* handle, int32 len);

	void * m_Handle;
	int32 m_Offset;
	int32 m_Length;

	static CConcurrentLoader & Loader() { static CConcurrentLoader _Loader; return _Loader; }
};
*/