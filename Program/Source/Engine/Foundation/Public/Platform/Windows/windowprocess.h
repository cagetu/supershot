#pragma once

/** Windows implementation of the process handle. */
struct ProcHandle : public TProcHandle<HANDLE, nullptr>
{
public:
	/** Default constructor. */
	FORCEINLINE ProcHandle()
		: TProcHandle()
	{}

	/** Initialization constructor. */
	FORCEINLINE explicit ProcHandle(HandleType Other)
		: TProcHandle(Other)
	{}
};

struct WindowPlatformProcess : public CommonPlatformProcess
{
	/**
	 * Creates a new process and its primary thread. The new process runs the
	 * specified executable file in the security context of the calling process.
	 * @param URL					executable name
	 * @param Parms					command line arguments
	 * @param bLaunchDetached		if true, the new process will have its own window
	 * @param bLaunchHidded			if true, the new process will be minimized in the task bar
	 * @param bLaunchReallyHidden	if true, the new process will not have a window or be in the task bar
	 * @param OutProcessId			if non-NULL, this will be filled in with the ProcessId
	 * @param PriorityModifier		-2 idle, -1 low, 0 normal, 1 high, 2 higher
	 * @param OptionalWorkingDirectory		Directory to start in when running the program, or NULL to use the current working directory
	 * @param PipeWrite				Optional HANDLE to pipe for redirecting output
	 * @return	The process handle for use in other process functions
	 */
	static ProcHandle CreateProc(const TCHAR* URL, const TCHAR* Parms, bool bLaunchDetached, bool bLaunchHidden, bool bLaunchReallyHidden, uint32* OutProcessID, int32 PriorityModifier, const TCHAR* OptionalWorkingDirectory, void* PipeWriteChild, void * PipeReadChild = nullptr);

	/**
	 * Opens an existing process.
	 *
	 * @param ProcessID				The process id of the process for which we want to obtain a handle.
	 * @return The process handle for use in other process functions
	 */
	static ProcHandle OpenProcess(uint32 ProcessID);
	static bool IsProcRunning(ProcHandle & ProcessHandle);
	static void WaitForProc(ProcHandle & ProcessHandle);
	static void CloseProc(ProcHandle & ProcessHandle);
	static void TerminateProc(ProcHandle & ProcessHandle, bool KillTree = false);
	static void Sleep(float Seconds);
	static void SleepNoStats(float Seconds);
	static void SleepInfinite();
};

typedef WindowPlatformProcess PlatformProcess;
