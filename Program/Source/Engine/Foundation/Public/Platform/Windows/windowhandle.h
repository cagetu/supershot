// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//
class WindowPlatformHandle : public TWindowHandle<HWND, HINSTANCE>
{
public:
	FORCEINLINE WindowPlatformHandle()
		: TWindowHandle() 
	{
		m_Width = 0;
		m_Height = 0;
		m_hWnd = NULL;
		m_hInstance = NULL;
		m_bFullScreen = false;
	}

	FORCEINLINE WindowPlatformHandle(HWND hwnd, HINSTANCE hInstance, uint32 _width, uint32 _height, bool _fullscreen = false)
		: TWindowHandle(hwnd, hInstance, _width, _height, _fullscreen) 
	{
		m_hWnd = hwnd;
		m_hInstance = hInstance;
		m_Width = _width;
		m_Height = _height;
		m_bFullScreen = _fullscreen;
	}
};

typedef WindowPlatformHandle WindowHandle;
