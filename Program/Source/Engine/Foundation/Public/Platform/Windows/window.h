// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include <process.h>

//
#define SAFE_RELEASE(p) { if(p) { p->Release(); p=NULL; } }

#include "Platform/Windows/windowutils.h"
#include "Platform/Windows/windowconsole.h"
#include "Platform/Windows/windowinterlocked.h"
#include "Platform/Windows/windowcriticalsection.h"
#include "Platform/Windows/windowmutex.h"
#include "Platform/Windows/windowio.h"
#include "Platform/Windows/windowhandle.h"
#include "Platform/Windows/windowprocess.h"

