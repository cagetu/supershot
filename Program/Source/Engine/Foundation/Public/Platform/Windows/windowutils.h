// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

struct WindowPlatformUtil : public CommonPlatformUtil
{
	static void Sleep(float sec);

	static void DebugOut(const TCHAR *format, ...);
	static int Dialog(const TCHAR* text, const TCHAR* caption, int type = 0);

	static void Prefetch(void* p, int32 offset);

	static const TCHAR* GetSystemErrorMessage(TCHAR* OutBuffer, int32 BufferCount, int32 Error);

	// File IO
	static FILE* fopen(const TCHAR* fileName, const TCHAR* mode);
	static void fclose(FILE * fp);

	static int32 remove(const TCHAR* fileName);
	static int32 rename(const TCHAR* oldFileName, const TCHAR* newFileName);
	static int32 access(const TCHAR* fileName, int32 accmode);

	static void FindFiles(Array<String>& list, const TCHAR* path = NULL, const TCHAR* filter = TEXT("*.*"), bool recur = true);
	static bool IsExistFile(const TCHAR* path);

	static unsigned long GetTick();
};

typedef WindowPlatformUtil PlatformUtil;

#define DebugMsg PlatformUtil::DebugOut
#define DialogMsg PlatformUtil::Dialog
