// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class	WindowOutputConsole : public IOutputConsole
{
public :
	WindowOutputConsole();
	virtual ~WindowOutputConsole();

	void Open() override;
	void Close() override;

	bool IsShow() const override;

	void Clear() override;
	void SetColor(RGBA color, bool bIntensity = false) override;
	void Print(const char* msg) override;
	void Print(const wchar* msg) override;

protected :
	void __SetColor(unsigned short attributes);

	HANDLE	m_Handle;
} ;

typedef WindowOutputConsole PlatformConsole;