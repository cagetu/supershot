// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

// Header Files
#ifdef TARGET_OS_ANDROID
#include <jni.h>
#include <android\log.h>
#include <cctype>	// tolower
#include <unistd.h>
#endif // TARGET_OS_ANDROID

#include <math.h>
#include <time.h>
#include <assert.h>
#include <wchar.h>
#include <stdio.h>
#include <stdarg.h>
#include <float.h>
#include <algorithm>

//#include <string>
//#include <vector>
//#include <map>
//#include <deque>
//#include <set>
//#include <unordered_map>

//----------------------------------------------------------
// FIXMEs / TODOs / NOTE macros
//----------------------------------------------------------
#define _QUOTE(x)		# x 
#define QUOTE(x)		_QUOTE(x)
#define __FILE__LINE__	__FILE__ "(" QUOTE(__LINE__) ") : "

#define FILE_LINE	message( __FILE__LINE__ )

#define NOTE( x )	message( __FILE__LINE__" NOTE :   " #x "\n" ) 
#define TODO( x )	message( __FILE__LINE__" TODO :   " #x "\n" ) 
#define FIXME( x )  message( __FILE__LINE__" FIXME:   " #x "\n" ) 

//----------------------------------------------------------
// Includes
//----------------------------------------------------------
#include "Platform\types.h"

// Container
#include "Container\template.h"
#include "Container\typetraits.h"
#include "Container\assertion.h"
#include "Container\allocator.h"
#include "Container\allocatorimpl.h"
#include "Container\algorithm.h"
#include "Container\pair.h"
//#include "Container\array.h"
//#include "Container\set.h"
//#include "Container\map.h"
//#include "Container\queue.h"
//#include "Container\stack.h"
#include "Container\unicode.h"
#include "Container\string.h"

// Platform
#include "Platform\utils.h"
#include "Platform\console.h"
#include "Platform\interlocked.h"
#include "Platform\criticalsection.h"
#include "Platform\mutex.h"
#include "Platform\stream.h"
#include "Platform\io.h"
#include "Platform\hwnd.h"
#include "Platform\timer.h"
#include "Platform\process.h"

#if defined(TARGET_OS_WINDOWS)
	#include "Windows\window.h"
#elif defined(TARGET_OS_ANDROID)
	#include "Android\android.h"
#elif defined(TARGET_OS_IPHONE)
	#include "iOS\ios.h"
#endif

#include "Platform\threadsafecounter.h"
#include "Platform\memoryutil.h"