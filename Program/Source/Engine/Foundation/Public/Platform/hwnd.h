// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

template <typename H, typename I>
class TWindowHandle
{
	typedef H HandleType;
	typedef I InstanceType;

public:
	FORCEINLINE TWindowHandle()
		: m_hWnd(0)
		, m_hInstance(0)
		, m_Width(0)
		, m_Height(0)
		, m_bFullScreen(false)
	{
	}
	FORCEINLINE TWindowHandle(HandleType hwnd, InstanceType hInstance, uint32 _width, uint32 _height, bool _fullscreen = false)
		: m_hWnd(hwnd)
		, m_hInstance(hInstance)
		, m_Width(_width)
		, m_Height(_height)
		, m_bFullScreen(_fullscreen)
	{
	}

	void SetSize(uint32 width, uint32 height)
	{
		m_Width = width;
		m_Height = height;
	}

	HandleType Handle() const { return m_hWnd; }
	InstanceType Instance() const { return m_hInstance; }

	uint32 Width() const { return m_Width; }
	uint32 Height() const { return m_Height; }
	bool IsFullScreen() const { return m_bFullScreen; }

protected:
	H m_hWnd;
	I m_hInstance;

	uint32 m_Width;
	uint32 m_Height;
	bool m_bFullScreen;
};