// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

/** Generic implementation for the process handle. */
template< typename T, T InvalidHandleValue >
struct TProcHandle
{
	typedef T HandleType;
public:

	/** Default constructor. */
	FORCEINLINE TProcHandle()
		: Handle(InvalidHandleValue)
	{ }

	/** Initialization constructor. */
	FORCEINLINE explicit TProcHandle(T Other)
		: Handle(Other)
	{ }

	/** Assignment operator. */
	FORCEINLINE TProcHandle& operator=(const TProcHandle& Other)
	{
		if (this != &Other)
		{
			Handle = Other.Handle;
		}
		return *this;
	}

	/** Accessors. */
	FORCEINLINE T Get() const
	{
		return Handle;
	}

	FORCEINLINE void Reset()
	{
		Handle = InvalidHandleValue;
	}

	FORCEINLINE bool IsValid() const
	{
		return Handle != InvalidHandleValue;
	}

protected:
	/** Platform specific handle. */
	T Handle;
};

struct ProcHandle;

struct CommonPlatformProcess
{
	/**
	 * Creates a new process and its primary thread. The new process runs the
	 * specified executable file in the security context of the calling process.
	 * @param URL					executable name
	 * @param Parms					command line arguments
	 * @param bLaunchDetached		if true, the new process will have its own window
	 * @param bLaunchHidded			if true, the new process will be minimized in the task bar
	 * @param bLaunchReallyHidden	if true, the new process will not have a window or be in the task bar
	 * @param OutProcessId			if non-NULL, this will be filled in with the ProcessId
	 * @param PriorityModifier		-2 idle, -1 low, 0 normal, 1 high, 2 higher
	 * @param OptionalWorkingDirectory		Directory to start in when running the program, or NULL to use the current working directory
	 * @param PipeWrite				Optional HANDLE to pipe for redirecting output
	 * @return	The process handle for use in other process functions
	 */
	static ProcHandle CreateProc(const TCHAR* URL, const TCHAR* Parms, bool bLaunchDetached, bool bLaunchHidden, bool bLaunchReallyHidden, uint32* OutProcessID, int32 PriorityModifier, const TCHAR* OptionalWorkingDirectory, void* PipeWriteChild, void * PipeReadChild = nullptr);

	/**
	 * Opens an existing process.
	 *
	 * @param ProcessID				The process id of the process for which we want to obtain a handle.
	 * @return The process handle for use in other process functions
	 */
	static ProcHandle OpenProcess(uint32 ProcessID);

	/**
	 * Returns true if the specified process is running
	 *
	 * @param ProcessHandle handle returned from FPlatformProcess::CreateProc
	 * @return true if the process is still running
	 */
	static bool IsProcRunning(ProcHandle & ProcessHandle);

	/**
	 * Waits for a process to stop
	 *
	 * @param ProcessHandle handle returned from FPlatformProcess::CreateProc
	 */
	static void WaitForProc(ProcHandle & ProcessHandle);

	/**
	 * Cleans up ProcHandle after we're done with it.
	 *
	 * @param ProcessHandle handle returned from FPlatformProcess::CreateProc.
	 */
	static void CloseProc(ProcHandle & ProcessHandle);

	/** Terminates a process
	 *
	 * @param ProcessHandle handle returned from FPlatformProcess::CreateProc
	 * @param KillTree Whether the entire process tree should be terminated.
	 */
	static void TerminateProc(ProcHandle & ProcessHandle, bool KillTree = false);
};