// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class ICriticalSection
{
public:
	virtual void Lock() = 0;
	virtual void Unlock() = 0;
};

class CSScopeLock
{
public:
	CSScopeLock(ICriticalSection & cs) { m_cs = &cs; m_cs->Lock(); }
	~CSScopeLock() { Leave(); }

	void Leave() { if (m_cs) { m_cs->Unlock(); m_cs = NULL; } }

private:
	ICriticalSection* m_cs;
};
