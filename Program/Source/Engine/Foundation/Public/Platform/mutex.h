// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class IMutex
{
public:
	virtual void Lock() = 0;
	virtual void Unlock() = 0;
};

class MTScopeLock
{
public:
	MTScopeLock(IMutex& mutex)
	{
		m_Mutex = &mutex;
		m_Mutex->Lock();
	}
	MTScopeLock()
	{
		Leave();
	}

	void Leave()
	{
		if (m_Mutex)
		{
			m_Mutex->Unlock();
		}
	}

private:
	IMutex* m_Mutex;
};
