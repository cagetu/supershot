// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class IBaseStream
{
public:
    enum _SEEK
    {
        _SET = 0,
        _CUR,
        _END
    };

public:
	virtual int32 WriteBytes(const uint8* input, int32 size) abstract;
	virtual int32 ReadBytes(uint8* output, int32 size) abstract;
    virtual int32 Seek(int32 offset, _SEEK mode) abstract;
};

class FileStream : public IBaseStream
{
public:
	enum Mode
	{
		ReadOnly = 0,
		WriteOnly,
	};
	FileStream();
	FileStream(const TCHAR* filename, Mode mode = ReadOnly);
	FileStream(FILE* fp, int32 offset, int32 length, Mode mode);
	~FileStream();

	bool Open(const TCHAR* filename, Mode mode = ReadOnly);
	void Close();

	int32 ReadBytes(uint8* output, int32 size) override;
	int32 WriteBytes(const uint8* input, int32 size) override;
    int32 Seek(int32 offset, _SEEK mode) override;
    int32 Tell();
	void Flush();

	bool IsOpened() const { return m_FileHandle ? true : false; }
	uint32 Size() const { return m_Length; }

protected:
	FILE* m_FileHandle;
	uint32 m_Length;
	uint32 m_CurPos;
	Mode m_Mode;
};

class MemoryStream : public IBaseStream
{
public:
	MemoryStream();
	MemoryStream(uint8* buffer, int32 length, bool bAutoDestroy = false);
	~MemoryStream();

	int32 ReadBytes(uint8* output, int32 size) override;
	int32 WriteBytes(const uint8* input, int32 size) override;
    int32 Seek(int32 offset, _SEEK mode) override;
    
    uint32 Size() const { return m_Length; }
    
	uint8* CurrentData();

private:
	uint8* m_Data;
    uint32 m_Length;
	uint32 m_CurPos;
	bool m_bAutoDestroy;
};

// Stream Utility

template<class T>
int32 StreamReadFlat(IBaseStream* stream, T& out_value)
{
	return stream->ReadBytes(reinterpret_cast<uint8_t*>(&out_value), sizeof(T));
}

template <class T>
int32 StreamWriteFlat(IBaseStream* stream, const T& value)
{
	return stream->WriteBytes(reinterpret_cast<uint8_t*>(&value), sizeof(T));
}
