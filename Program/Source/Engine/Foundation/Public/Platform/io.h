// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class IArchive /*: public RefCount*/
{
public :
	enum _SEEK
    {
        _SET = 0,
        _CUR,
        _END
    } ;
    
    virtual int32 Write(void * ptr, int32 len) = 0;
	virtual int32 Read(uint8 * output, int32 len) = 0;
	virtual int32 Seek(int32 offset, _SEEK pos) = 0;
	virtual uint32 GetSize() const { return 0; }

	uint8 GetChar() { uint8 ch; return Read(&ch, 1) == 1 ? ch : -1; }
};

//------------------------------------------------------------------
/** File IO
	@author cagetu
	@remarks
		Read Only
*/
//------------------------------------------------------------------
class FileIO : public IArchive
{
public :
	enum Mode
	{
		ReadOnly = 0,
		WriteOnly,
	};
	FileIO();
	virtual ~FileIO();

	bool Open(const TCHAR * fname, Mode mode = ReadOnly);
	void Close();

	int32 Write(void * ptr, int32 len);
	int32 Read(uint8* output, int32 len);
	int32 Seek(int32 offset, _SEEK pos);
	int32 Tell();
	void Flush();

	uint32 GetSize() const { return m_Length; }

	static FileIO* Create(const TCHAR * fname, Mode mode = ReadOnly);
	static FileIO* Create(FILE* fp, int off=0, int len=0, Mode mode = ReadOnly);
	static FileIO* Destroy(FileIO* file);

protected :
	FileIO(FILE *fp, int off, int len, Mode mode);

	FILE* m_FileHandle;
	uint32 m_Length;
	uint32 m_CurPos;
	Mode m_Mode;
} ;

//------------------------------------------------------------------
/**
*/
//------------------------------------------------------------------
class MemoryIO : public IArchive
{
public:
	MemoryIO(const uint8* buffer, int32 len, bool bAutoDestroy);
	virtual ~MemoryIO();
    
    int32 Write(void * ptr, int32 len);
	int32 Read(uint8* output, int32 len);
	int32 Seek(int32 offset, _SEEK pos);

	uint32 GetSize() const { return m_Length; }
	bool IsAutoDestroy() const { return m_bAutoDestroy; }

protected:
	const uint8* m_Data;
	uint32 m_Length;
	uint32 m_Offset;
	bool m_bAutoDestroy;
};
