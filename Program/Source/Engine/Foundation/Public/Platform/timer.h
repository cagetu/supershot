// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class Timer
{
public:
	Timer();
	~Timer();

	void Reset();

	void Start();
	void Stop();

	void Tick();

	float TotalTime() const;
	float DeltaTime() const;

	bool IsStopped() const;

protected:
	double m_SecondsPerCount;
	double m_DeltaTime;

	__int64 m_BaseTime;
	__int64 m_PausedTime;
	__int64 m_StopTime;
	__int64 m_PrevTime;
	__int64 m_CurrTime;

	bool m_Stopped;
};