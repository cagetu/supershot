// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

struct CommonPlatformUtil
{
	static void Sleep(float sec);
	static void Exit(int32 exitCode = -1);

	static void DebugOut(const TCHAR* format, ...) {}
	static int32 Dialog(const TCHAR* text, const TCHAR* caption, int32 type = 0) { return 0; }

	static const TCHAR* GetSystemErrorMessage(TCHAR* OutBuffer, int32 BufferCount, int32 Error);

	//// 메모리... 나중에 옮기자...
	static void MemMove(void* dest, const void* src, size_t size);
	static void MemCpy(void* dest, const void* src, size_t size);
	static void MemSet(void* dest, int32 val, size_t size);
	static void MemZero(void* dest, size_t size);
	static int32 MemCmp(const void* buf1, const void* buf2, size_t size);

	static void Prefetch(void* p, int32 offset) {}

	//// File IO
	static FILE* fopen(const TCHAR* fileName, const TCHAR* mode);
	static void fclose(FILE * fp);
	static int32 fwrite(FILE* fp, void* buffer, int32 elementSize, int32 elementCount);
	static int32 fread(FILE* fp, void* buffer, int32 elementSize, int32 elementCount);
	static int32 fseek(FILE* fp, int32 offset, int32 mode);
	static int32 ftell(FILE* fp);
	static void fflush(FILE* fp);

	static int32 remove(const TCHAR* fileName);
	static int32 rename(const TCHAR* oldFileName, const TCHAR* newFileName);
	static int32 access(const TCHAR* fileName, int32 accmode);

	static void FindFiles(Array<String>& list, const TCHAR* path = NULL, const TCHAR* filter = TEXT("*.*"), bool recur = true) {}
	static bool IsExistFile(const TCHAR* path) { return false; }

	static unsigned long GetTick();
};
