// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

// Platform
#include "Platform\platform.h"

// Math
#include "Math\mathutil.h"
//#include "Math\vector.h"
//#include "Math\ray.h"
//#include "Math\quat.h"
//#include "Math\matrix.h"
//#include "Math\aabb.h"
//#include "Math\obb.h"
//#include "Math\capsule.h"
//#include "Math\plane.h"
//#include "Math\sphere.h"
//#include "Math\spline.h"
//#include "Math\rect2d.h"
//#include "Math\transform.h"

// Core
#include "refcount.h"
#include "ptr.h"
#include "rtti.h"
//#include "serialize.h"
//#include "object.h"

// Utilities
#include "log.h"
//#include "crc.h"
//#include "name.h"
//#include "args.h"
//#include "color.h"
//#include "minixml.h"
//#include "ini.h"
//#include "bitflags.h"
//#include "variant.h"
//#include "assign.h"
//#include "assignregistry.h"



