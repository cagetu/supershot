// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/** AssignRegistry
	@author cagetu
	@desc
		- https://floooh.github.io/2007/02/17/nebula3-io-subsystem-uris-and-assigns.html
		- URI를 기반으로 "content:texture.png" 사용할 수 있도록 "content:"를 등록한다.
*/
//------------------------------------------------------------------
class AssignRegistry
{
public:
	/// 최초 시작
	static void _Startup();
	/// 최종 마무리
	static void _Shutdown();

	/// Assign 추가
	static void Register(const String& name, const String& path);
	/// Assign 제거
	static void Unregister(const String& name);
	/// Assign 등록 여부 확인
	static bool Has(const String& name);
	/// Assign 찾기
	static String Find(const String& name);
	/// Assign -> FullPath로 변환
	static String Resolve(const String& uri);
};