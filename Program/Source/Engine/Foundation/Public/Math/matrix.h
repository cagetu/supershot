// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"
#include "Math\quat.h"

struct Quat;

//------------------------------------------------------------------
/** @class Matrix4x4
    @desc   4x4 Matrix의 변환에 대한 처리
*/
//------------------------------------------------------------------
struct	Mat4
{
	float _11, _12, _13, _14;
	float _21, _22, _23, _24;
	float _31, _32, _33, _34;
	float _41, _42, _43, _44;

public:
	Mat4();
	Mat4(Mat4&& m) noexcept;
	Mat4(const Mat4& m);
	Mat4(float m11, float m12, float m13, float m14,
		 float m21, float m22, float m23, float m24,
		 float m31, float m32, float m33, float m34,
		 float m41, float m42, float m43, float m44);

	Mat4& operator = (Mat4&& rhs) noexcept;
	Mat4& operator = (const Mat4& rhs);

	bool operator == (const Mat4& rhs) const;
	bool operator != (const Mat4& rhs) const { return !(*this == rhs); }

    void Identify();
    
	Vec3 Transform(const Vec3 & p) const;
	Vec4 Transform(const Vec4 & p) const;
	Vec3 TransformNormal(const Vec3 & p) const;
	Vec3 TransformHomogen(const Vec3 & p) const;

	Mat4 Transpose() const;
	Mat4 Inverse() const;

	void SetPosition(const Vec3& p);
	Vec3& GetPosition() { return *(Vec3*)&_41; }
	const Vec3& GetPosition() const { return *(Vec3*)&_41; }

	void SetRotation(const Quat& q);
	void GetRotation(Quat& q) const;
	void GetRotation(Mat4& rot) const;
	Quat GetRotation() const { Quat q; GetRotation(q); return q; }

	void SetScale(float scale);
	void SetScale(const Vec3& scale);
	Vec3 GetScale() const { return Vec3(_11, _22, _33); }

	void Translate(const Vec3 & p);
	void Rotate(const Quat& q);
	void Scale(float v);

	float Determinant() const;

	void SetAxises(const Vec3* axises);
	void GetAxises(Vec3* outAxises);
	const Vec3& GetRow0() const { return *(Vec3*)&_11; }
	const Vec3& GetRow1() const { return *(Vec3*)&_21; }
	const Vec3& GetRow2() const { return *(Vec3*)&_31; }

	void Decompose(Vec3& translate, Quat& rotate, Vec3& scale) const;
	void Decompose(Vec3& translate, Vec3& rotate, Vec3& scale) const;

	static Mat4 Make(const Quat& rot, const Vec3& tanslate);
	static Mat4 Make(const Vec3& xaxis, const Vec3& yaxis, const Vec3& zxaxis, const Vec3& tanslate);
	static Mat4 Make(const Vec3& axis, float rad);

	static Mat4 Multiply(const Mat4 & a, const Mat4 & b);
	static Mat4 Inverse(const Mat4 & m) { return m.Inverse(); }

	static Mat4 Mirror(const Vec3& normal, const Point3& point);

	static Mat4 IDENTITY;

#ifdef _PHYSX3
	operator physx::pubfnd3::PxTransform () const { return physx::pubfnd3::PxTransform(Position(), GetRotation()); }
#endif
} ;

inline Vec3 operator * (const Vec3 & p, const Mat4 & tm) { return tm.Transform(p); }
inline Vec4 operator * (const Vec4 & p, const Mat4 & tm) { return tm.Transform(p); }
inline Mat4 operator * (const Mat4 & m1, const Mat4 & m2) { return Mat4::Multiply(m1, m2); }

///////////////////////////////////////////////////////////////////////////////////////////////////

struct	Mat4rigid : public Mat4
{
	Mat4rigid Inverse() const { return Transpose(); }
	Mat4rigid Transpose() const;

	//static Mat4rigid Multiply(const Mat4rigid & a, const Mat4rigid & b);
	//static Mat4rigid LookAt(const Vec3 & pivot, const Vec3 & target, const Vec3 & upvec);
	//static Mat4rigid LookAt(const Vec3 & pivot, const Vec3 & target);
	//static Mat4rigid RotationX(float rad);
	//static Mat4rigid RotationY(float rad);
	//static Mat4rigid RotationZ(float rad);
} ;

///////////////////////////////////////////////////////////////////////////////////////////////////

struct	Mat3
{
	float	_11, _12, _13;
	float	_21, _22, _23;
	float	_31, _32, _33;

	Mat3();
	//Mat3(Mat3&& m);
	Mat3(const Mat3& m);
	Mat3(float m11, float m12, float m13,
		 float m21, float m22, float m23,
		 float m31, float m32, float m33);

	void Identify();

	float Determinant();

	void SetPosition(const Vec2& p);
	Vec2& GetPosition() { return *(Vec2*)&_31; }

	void SetRotation(float rad);
	float GetRotation() const;

	void SetScale(float scale);
	void SetScale(const Vec2& scale);
	Vec2 GetScale() const;

	void FromMat4(const Mat4& m);
	Mat4 ToMat4() const;

	static Mat3 Rotation(float rad);
	static Mat3 Scaling(float s) { return Scaling(s, s); }
	static Mat3 Scaling(float w, float h);
	static Mat3 Translate(const Vec2 & p);

	static Mat3 Make(const Vec2& pos, float rotate);

	static Mat3 Multiply(const Mat3& m1, const Mat3& m2);
	static Vec2 Transform(const Vec2& v, const Mat3& m);

	static Mat3 IDENTITY;
};

inline Vec2 operator * (const Vec2 & p, const Mat3 & tm) { return Mat3::Transform(p, tm); }
inline Mat3 operator * (const Mat3 & m1, const Mat3 & m2) { return Mat3::Multiply(m1, m2); }
