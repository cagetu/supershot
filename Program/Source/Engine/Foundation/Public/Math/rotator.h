// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"
#include "Math\quat.h"

//------------------------------------------------------------------
/** @class	Rotator
	@desc   Euler 각도

			(UE4에 있는 Rotator를 보고 만들면서, 공식을 이해하기 위함이다)

			TODO:
				- Rotator <-> Quaternion 만다는 공식 및 코드 작성하기
				- Vector -> Rotator 만드는 코드 공식 및 코드 작성하기
				- Rotator -> Matrix 만드는 코드 만들기
*/
//------------------------------------------------------------------
struct Rotator
{
	// right 축을 중심으로 회전. 위 / 아래로 바라본다.
	float Pitch;
	// up 축을 중심으로 회전
	float Yaw;
	// forward 축을 중심으로 회전.
	float Roll;

public:
	Rotator() : Pitch(0.0f), Yaw(0.0f), Roll(0.0f) {}
	Rotator(Rotator&& q) noexcept { Pitch = q.Pitch; Yaw = q.Yaw; Roll = q.Roll; q.Pitch = q.Yaw = q.Roll = 0.0f; }
	Rotator(const Rotator& Q) { Pitch = Q.Pitch; Yaw = Q.Yaw; Roll = Q.Roll; }
	Rotator(float pitch, float yaw, float roll) { Pitch = pitch; Yaw = yaw; Roll = roll; }
	Rotator(const Quat& Q);

	Rotator& operator = (Rotator&& q) noexcept { Pitch = q.Pitch; Yaw = q.Yaw; Roll = q.Roll;  q.Pitch = q.Yaw = q.Roll = 0.0f; return (*this); }
	Rotator& operator = (const Rotator& Q) { Pitch = Q.Pitch; Yaw = Q.Yaw; Roll = Q.Roll; return (*this); }

	// Euler 각도로 변환
	Vec3 Euler() const;
	// Quaternion으로 변환
	Quat Quaternion() const;

	// [0, 360도]로 변환
	Rotator Clamp() const;
	// [-180, 180도]으로 정규화
	void Normalize();

	// [-180, 180] 으로 변환된 Rotator 얻기
	Rotator GetNormalized() const;
	// [0, 360] 으로 변환된 Rotator 얻기
	Rotator GetDenormalized() const;

	bool ContainsNaN() const;

	// statics

	static float ClampAxis(float angle);
	static float NormalizeAxis(float angle);

	static Rotator MakeFromEuler(float pitch, float yaw, float roll);
};

//------------------------------------------------------------------
//	
//------------------------------------------------------------------
FORCEINLINE Rotator Rotator::Clamp() const
{
	return Rotator(ClampAxis(Pitch), ClampAxis(Yaw), ClampAxis(Roll));
}

FORCEINLINE float Rotator::ClampAxis(float Angle)
{
	// returns Angle in the range (-360,360)
	Angle = Math::FMod(Angle, 360.f);

	if (Angle < 0.f)
	{
		// shift to [0,360) range
		Angle += 360.f;
	}

	return Angle;
}


FORCEINLINE float Rotator::NormalizeAxis(float Angle)
{
	// returns Angle in the range [0,360)
	Angle = ClampAxis(Angle);

	if (Angle > 180.f)
	{
		// shift to (-180,180]
		Angle -= 360.f;
	}

	return Angle;
}

FORCEINLINE void Rotator::Normalize()
{
	Pitch = NormalizeAxis(Pitch);
	Yaw = NormalizeAxis(Yaw);
	Roll = NormalizeAxis(Roll);
}

FORCEINLINE bool Rotator::ContainsNaN() const
{
	return (!Math::IsFinite(Pitch) ||
			!Math::IsFinite(Yaw) ||
			!Math::IsFinite(Roll));
}

FORCEINLINE Rotator Rotator::GetNormalized() const
{
	Rotator Rot = *this;
	Rot.Normalize();
	return Rot;
}

FORCEINLINE Rotator Rotator::GetDenormalized() const
{
	Rotator Rot = *this;
	Rot.Pitch = ClampAxis(Rot.Pitch);
	Rot.Yaw = ClampAxis(Rot.Yaw);
	Rot.Roll = ClampAxis(Rot.Roll);
	return Rot;
}
