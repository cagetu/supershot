// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"

class Sphere;
struct Plane;

//------------------------------------------------------------------
/** @class	Capsule
	@desc   ĸ��...
*/
//------------------------------------------------------------------
struct Capsule
{
	Point3 v1, v2;
	float radius;

public:
	Capsule();
	Capsule(Capsule&& capsule) noexcept;
	Capsule(const Capsule& capsule);
    Capsule(const Point3 & startPoint, const Point3 & endPoint, float r);

	Capsule& operator = (Capsule&& capsule) noexcept;
	Capsule& operator = (const Capsule& capsule);

	bool operator == (const Capsule& capsule) const;
	bool operator != (const Capsule& capsule) const { return !(*this == capsule); }

	float SweepTest(const Sphere & sphere, const Vec3 & dir, Plane * out) const;
	bool ContactTest(const Sphere & sphere) const;
} ;
