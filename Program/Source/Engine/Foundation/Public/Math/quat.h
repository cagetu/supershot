// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"

//------------------------------------------------------------------
/** @class	Quaternion
	@desc   쿼터니온 클래스

			* 단위 길이의 사원수는 3차원 회전 변환을 나타낸다.
				* 게임에서는 단위 벡터일 때의 회전 변환을 나타내는 기능을 활용한다.
			* 회전 축 (벡터)와 회전 각도 (Theta)
			* 회전각이 Theta일 때, v * sin(Theta/2), cos(Theta/2) 로 표현된다.
*/
//------------------------------------------------------------------
struct Quat
{
	float x;
	float y;
	float z;
	float w;

public:
	Quat() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
	Quat(Quat&& q) noexcept { x = q.x; y = q.y; z = q.z; w = q.w; q.x = q.y = q.z = q.w = 0.0f; }
	Quat(const Quat& Q) { x = Q.x; y = Q.y; z = Q.z; w = Q.w; }
	Quat(float qx, float qy, float qz, float qw) { x = qx; y = qy; z = qz; w = qw; }

	FORCEINLINE Quat operator - () { return Quat(-x, -y, -z, -w); }

	/*	사원수의 덧셈과 뺄셈
	*	단위 길이 사원수 두 개를 합하면 두 결과는 3D 회전을 나타내지 않는다.
	*	그 이유는 더 이상 단위 길이가 아니기 때문이다.
	*	특별히 단위 길이 성질을 보존할 만한 작업을 하지 않는 한 게임에서 사원수의 합은 거의 사용하지 않는다.
	*/
	FORCEINLINE Quat operator + (const Quat& Q) { return Quat(x + Q.x, y + Q.y, z + Q.z, w + Q.w); }
	FORCEINLINE Quat operator - (const Quat& Q) { return Quat(x - Q.x, y - Q.y, z - Q.z, w - Q.w); }
	FORCEINLINE Quat& operator += (const Quat& Q) { x += Q.x; y += Q.y; z += Q.z; w += Q.w; return *this; }
	FORCEINLINE Quat& operator -= (const Quat& Q) { x -= Q.x; y -= Q.y; z -= Q.z; w -= Q.w; return *this; }

	FORCEINLINE Quat operator * (const Quat& Q) { return Quat::Multiply((*this), Q); }
	FORCEINLINE Quat operator * (float scale) { return Quat(x * scale, y * scale, z * scale, w * scale); }
	FORCEINLINE Quat& operator *= (const Quat& Q) { *this = Quat::Multiply(*this, Q); return *this; }
	FORCEINLINE Quat& operator *= (float scale) { x *= scale; y *= scale; z *= scale; w *= scale; return *this; }

	FORCEINLINE Quat& operator = (Quat&& Q) noexcept { x = Q.x; y = Q.y; z = Q.z; w = Q.w; Q.x = Q.y = Q.z = Q.w = 0.0f; return (*this); }
	FORCEINLINE Quat& operator = (const Quat& Q) { x = Q.x; y = Q.y; z = Q.z; w = Q.w; return (*this); }

	FORCEINLINE bool operator == (const Quat& rhs) const { return (x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w); }
	FORCEINLINE bool operator != (const Quat& rhs) const { return !(*this == rhs); }

	FORCEINLINE float Size() const { return Math::Sqrt(x * x + y * y + z * z + w * w); }
	FORCEINLINE float SizeSquared() const { return (y * x + y * y + z * z + w * w); }

	/*	사원수의 역
		- 원래의 값과 곱하면 스칼라 값이 1이 되는 사원수를 뜻한다.
		- 역: 켤례 [-q, s] 를 길이로 나눈다.
		- 정규화 되었다고 가정하면, 길이로 나누지 않아도 된다.
	*/
	FORCEINLINE Quat Inverse() const { ASSERT(IsNormalized());  return Quat(-x, -y, -z, w); }

	// 정규화 (Unit Vector 크기로 만든다)
	void Normalize(float Tolerance = SMALL_NUMBER);
	Quat GetNormalized(float Tolerance = SMALL_NUMBER) const;
	bool IsNormalized() const;

	bool ContainsNaN() const;

	/**
	 * @return quaternion with W=0 and V=theta*v.
	 */
	Quat Log() const;

	/**
	 * @note Exp should really only be used after Log.
	 * Assumes a quaternion with W=0 and V=theta*v (where |v| = 1).
	 * Exp(q) = (sin(theta)*v, cos(theta))
	 */
	Quat Exp() const;

	/** Convert a Quaternion into floating-point Euler angles (in degrees). */
	Vec3 Euler() const;

	// 축이 되는 Unit Vector를 만든다.
	Vec3 GetRotationAxis() const;

	// 각도를 구한다.
	Radian GetAngle() const;

	// 두 쿼터니언 사이의 각 거리 (angular distance)를 찾는다.
	Radian AngularDistance(const Quat& Q) const;

	Vec3 RotateVector(Vec3 V) const;
	Vec3 UnrotateVector(Vec3 V) const;

	void Decompose(Vec3& axis, Radian& angle) const;
	void Decompose(Radian& pitch, Radian& yaw, Radian& roll);

	/// Statics

	static Quat FindBetween(const Vec3& Vector1, const Vec3& Vector2) {	return FindBetweenVectors(Vector1, Vector2); }

	static Quat FindBetweenNormals(const Vec3& Normal1, const Vec3& Normal2);
	static Quat FindBetweenVectors(const Vec3& Vector1, const Vec3& Vector2);

	static Quat MakeFrom(const Vec3& va, const Vec3& vb);
	static Quat MakeFromAxisAngle(const Vec3& axis, Radian rad);
	static Quat Multiply(const Quat& qa, const Quat& qb);
	static Quat Slerp(const Quat &qa, const Quat &qb, float w);
	static float DotProduct(const Quat &qa, const Quat &qb) { return qa.x*qb.x + qa.y*qb.y + qa.z*qb.z + qa.w*qb.w; }

	static Quat MakeFromEuler(Radian rx, Radian ry, Radian rz);
	static void	MakeToEuler(const Quat &q, Radian *r);

	static const Quat IDENTITY;
} ;

FORCEINLINE Quat operator * (float scale, const Quat& Q) { return Quat(Q.x * scale, Q.y * scale, Q.z * scale, Q.w * scale); }
