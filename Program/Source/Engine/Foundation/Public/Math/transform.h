// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Math\vector.h"
#include "Math\matrix.h"

struct Quat;

//------------------------------------------------------------------
/** @class Transform
	@desc Vector 및 Matrix의 변환에 대한 처리
*/
//------------------------------------------------------------------
class Transform
{
public:
	static Vec4 TransformVector(const Mat4& m, const Vec4& p);
	static Vec3 TransformPoint(const Mat4& m, const Point3& p);
	static Vec3 TransformNormal(const Mat4& m, const Vec3& p);
	static Vec3 TransformHomogen(const Mat4& m, const Vec3& p);

	static Vec4 Multiply(const Mat4& m, const Vec4& p);
	static Mat4 Multiply(const Mat4& a, const Mat4& b);

	static Mat4 RotationX(Radian rad);
	static Mat4 RotationY(Radian rad);
	static Mat4 RotationZ(Radian rad);
	static Mat4 Rotation(const Quat& q);
	static Mat4 Scaling(const Vec3& s);
	static Mat4 Scaling(float s);
	static Mat4 Translate(const Vec3& p);
	static Mat4 Translate(float x, float y, float z);
};
