// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"

struct Mat4;

//------------------------------------------------------------------
/** @class	Plane
	@desc   평면 방정식
*/
//------------------------------------------------------------------
struct Plane
{
	float a, b, c, d;

public:
	Plane() { a = b = c = d = 0.0f; }
	Plane(Plane&& plane) noexcept;
	Plane(const Plane& plane);
	Plane(float a, float b, float c, float d) { this->a = a, this->b = b, this->c = c, this->d = d; }
	Plane(const Vec3 &norm, float d) { a = norm.x, b = norm.y, c = norm.z, this->d = d; }
	Plane(const Vec3 &norm, const Point3 & onpoint) { a = norm.x, b = norm.y, c = norm.z, d = -Vec3::DotProduct(norm, onpoint); }
	Plane(const Point3 &v1, const Point3 &v2, const Point3 &v3);

	Plane& operator = (Plane&& plane) noexcept;
	Plane& operator = (const Plane& plane);

	inline bool operator == (const Plane& rhs) const { return (a == rhs.a && b == rhs.b && c == rhs.c && d == rhs.d); }
	inline bool operator != (const Plane& rhs) const { return !(*this == rhs); }

	Vec3 & Normal() { return *(Vec3*)&a; }
	const Vec3 & Normal() const { return *(Vec3*)&a; }
	Plane Flip() const { return Plane(-a, -b, -c, -d); }
	Plane Transform(const Mat4 & m) const;
	void Normalize();

	float DotProduct(const Vec4& p) { return a*p.x+b*p.y+c*p.z+d*p.w; }
	float Distance(const Vec3 &p) const { return a*p.x+b*p.y+c*p.z+d; }

	int GetSide(const Vec3& p) const
	{
		float d = Distance(p);
		if (d < -1.0e-30f)
			return -1;	// NEGATIVE_SIDE;
		if (d > 1.0e-30f)
			return 1;	// POSITIVE_SIDE;
		return 0;		// NO_SIDE;
	}

	float SweepTest(const Vec3& p, float radius, const Vec3& v)
	{
		float d1 = Distance(p) - radius;
		float d2 = v.x*a + v.y*b + v.z*c;
		if (d1 >= 0 && d1 - (-d2) <= 0)
			return -d1 / d2;
		return 1.0f;
	}

	// 벡터 - 평면 교차
	bool Intersect(const Point3& p, const Vec3& dir, float& t) const 
	{
		const Vec3& normal = Normal();
		float cos = Vec3::DotProduct(normal, dir);
		if (Math::Abs<float>(cos) < __EPSILON)	// EPSILON
			return false;

//		t = (d - Vec3::DotProduct(normal, p)) / cos;

		float dist = Distance(p);
		t = -dist/cos;
		return true;
	}

	// Ray - 평면 교차
	float RayIntersect(const Point3& p, const Vec3& dir) const
	{
		float d1 = Distance(p);
		float d2 = Vec3::DotProduct(Normal(), dir);
		if (d1 >= 0 && d2 < 0)
			return d1 >= -d2 ? 1.0f : -d1/d2;
		return 1.0f;
	}

	// Segment - 평면 교차
	bool IntersectWithEdge(Vec3& output, const Vec3& a, const Vec3& b) const 
	{
		float t;
		if (Intersect(a, (b-a), t))
		{
			output = a + (b-a) * t;
			return true;
		}
		return false;
	}

	/// Statics

	static Plane MakeFrom(const Point3& v1, const Point3& v2, const Point3& v3) { return Plane(v1, v2, v3); }
} ;
