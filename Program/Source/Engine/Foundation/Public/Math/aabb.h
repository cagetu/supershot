// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"

struct Mat4;
struct Plane;
class Sphere;

//------------------------------------------------------------------
/** @class	Axis Aligned Bounding Box
    @desc   축 정렬된 바운딩 박스
*/
//------------------------------------------------------------------
struct AABB
{
public:
	float minX, minY, minZ;	// 
	float maxX, maxY, maxZ;

public:
    AABB();
	AABB(AABB&& aabb) noexcept;
	AABB(const AABB& aabb);
    AABB(const Vec3 & min, const Vec3 & max);
    AABB(const Vec3 & p);
	AABB(const Vec3* points, int num);

	AABB& operator = (AABB&& aabb) noexcept;
	AABB& operator = (const AABB& aabb);

	bool operator == (const AABB& aabb) const;
	bool operator != (const AABB& aabb) const;

	void Empty() { minX = minY = minZ = 99999.0f; maxX = maxY = maxZ = -99999.0f; }

	const Vec3& Min() const { return *(Vec3*)&minX; }
	const Vec3& Max() const { return *(Vec3*)&maxX; }

	Vec3 Center() const { return Vec3((minX+maxX)*0.5f, (minY+maxY)*0.5f, (minZ+maxZ)*0.5f); }
	Vec3 Extent() const { return Max() - Min(); }

	AABB Transform(const Mat4& tm) const;
	void Translate(const Vec3& v);

	float Distance(Point3& point) const;

	/// 

	void GetPlanes(Plane* planes) const;
	void GetPoints(Point3* points) const;

	///

	void Expand(const Vec3 & p);
	void Expand(const AABB & bound);
	void Expand(float size);
	void Union(const AABB & bound) { Expand(bound); }
	void Intersection(const AABB & bound);	// 교집합

    bool IsInside(const Point3 & p) const { return p.x >= minX && p.x <= maxX && p.y >= minY && p.y <= maxY && p.z >= minZ && p.z <= maxZ; }
    
	bool IntersectTest(const AABB & bound) const;
	bool IntersectTest(const Sphere& s) const;

	float RayIntersect(const Point3 & p, const Vec3 & dir, Plane* out=NULL) const;
	float SweepTest(const Sphere& pos, const Vec3& vec, Plane* out=NULL) const;

	bool BoundTest(const Point3 &start, const Point3 &direct, float radius) const;
};