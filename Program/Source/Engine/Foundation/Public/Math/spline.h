// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Math\vector.h"

//------------------------------------------------------------------
/** @class	Catmull-Rom Spine
	@desc   Catmull-Rom Spine
*/
//------------------------------------------------------------------
struct Spline
{
	static Vec2 CatmullRom(const Vec2& p0, const Vec2& p1, const Vec2& p2, const Vec2& p3, float t);
	static Vec3 CatmullRom(const Vec3& p0, const Vec3& p1, const Vec3& p2, const Vec3& p3, float t);
	static Vec4 CatmullRom(const Vec4& p0, const Vec4& p1, const Vec4& p2, const Vec4& p3, float t);
};
