// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/*	@class Vector2
*/
//------------------------------------------------------------------
struct	Vec2
{
public:
    float x, y;

public:
    // constructor
    explicit Vec2() { x = y = 0.0f; }
    Vec2(Vec2&& v) noexcept { x = v.x; y = v.y; v.x = v.y = 0.0f; }
    explicit Vec2(const Vec2& v) { x = v.x; y = v.y; }
    explicit Vec2(float x, float y) { this->x = x; this->y = y; }
    explicit Vec2(const float* f) { this->x = f[0]; this->y = f[1]; }

    //	스위즐링 2차원
    FORCEINLINE Vec2 xy() const { return Vec2(x, y); }
    FORCEINLINE Vec2 yx() const { return Vec2(y, x); }

    //FORCEINLINE Vec2& operator -() { x *= -1.0f; y *= -1.0f; return (*this); }
    FORCEINLINE Vec2 operator - () const { return Vec2(-x, -y); }
    FORCEINLINE float& operator[] (uint8 i) { assert(i < 2); return (&x)[i]; }
    FORCEINLINE float operator[] (uint8 i) const { assert(i < 2); return (&x)[i]; }

    FORCEINLINE bool operator < (const Vec2& v) const { return (x < v.x&& y < v.y) ? true : false; }
    FORCEINLINE bool operator > (const Vec2& v) const { return (x > v.x && y > v.y) ? true : false; }
    FORCEINLINE bool operator <= (const Vec2& v) const { return (x <= v.x && y <= v.y) ? true : false; }
    FORCEINLINE bool operator >= (const Vec2& v) const { return (x >= v.x && y >= v.y) ? true : false; }
    FORCEINLINE bool operator == (const Vec2& v) const { return (x == v.x && y == v.y) ? true : false; }
    FORCEINLINE bool operator != (const Vec2& v) const { return !(operator == (v)); }

    FORCEINLINE Vec2& operator = (Vec2&& v) noexcept { x = v.x; y = v.y; v.x = v.y = 0.0f; return *this; }
    FORCEINLINE Vec2& operator = (const Vec2& v) { x = v.x; y = v.y; return *this; }
    FORCEINLINE Vec2& operator += (const Vec2& v) { this->x += v.x; this->y += v.y; return *this; }
    FORCEINLINE Vec2& operator -= (const Vec2& v) { this->x -= v.x; this->y -= v.y; return *this; }
    FORCEINLINE Vec2& operator *= (float f) { this->x *= f; this->y *= f; return *this; }
    FORCEINLINE Vec2& operator /= (float f) { this->x /= f; this->y /= f; return *this; }

    FORCEINLINE Vec2 operator + (const Vec2& v) const { return Vec2(x + v.x, y + v.y); }
    FORCEINLINE Vec2 operator - (const Vec2& v) const { return Vec2(x - v.x, y - v.y); }
    FORCEINLINE Vec2 operator * (const Vec2& v) const { return Vec2(v.x * x, v.y * y); }
    FORCEINLINE Vec2 operator * (float s) const { return Vec2(x * s, y * s); }
    FORCEINLINE Vec2 operator / (const Vec2& v) const { return Vec2(x / v.x, y / v.y); }
    FORCEINLINE Vec2 operator / (float s) const { assert(s > 0.0f); return Vec2(x / s, y / s); }

    FORCEINLINE bool IsNearlyZero() const { return Math::IsNearlyZero(x) && Math::IsNearlyZero(y); }

    FORCEINLINE float Size() const { return Math::Sqrt(SizeSquared()); }
    FORCEINLINE float SizeSquared() const { return (x * x + y * y); }

    FORCEINLINE Vec2& Normalize() { float s = 1.0f / Size(); *this *= s; return *this; }

    String ToString() const;

    static float Size(const Vec2& v) { return Math::Sqrt(v.x * v.x + v.y * v.y); }
    static Vec2 Multiply(const Vec2& a, const Vec2& b) { return Vec2(a.x * b.x, a.y * b.y); }
    static Vec2 Normalize(const Vec2& v) { float s = 1.0f / Math::Sqrt(v.x * v.x + v.y * v.y); return Vec2(v.x * s, v.y * s); }
    //static Vec2 Lerp(const Vec2& a, const Vec2& b, float w) { return Vec2(a.x+(b.x-a.x)*w, a.y+(b.y-a.y)*w); }

    static float DotProduct(const Vec2& v1, const Vec2& v2) { return v1.x * v2.x + v1.y * v2.y; }
    static float CrossProduct(const Vec2& v1, const Vec2& v2) { return v1.x * v2.y - v1.y * v2.x; }

    static Vec2 GetRandom();
    static Vec2 GetRandom(float minValue, float maxValue);

    static bool IsNearlyEqual(const Vec2& v1, const Vec2& v2) { return Math::IsNearlyEqual(v1.x, v2.x) && Math::IsNearlyEqual(v1.y, v2.y); }

    static Vec2 ZERO;
    static Vec2 ONE;
};

#define Point2 Vec2

/// Scale * Vector
FORCEINLINE Vec2 operator * (float scale, const Vec2& v) { return Vec2(v.x * scale, v.y * scale); }

extern bool IntersectSegments(const Vec2& seg1Start, const Vec2& seg1End, const Vec2& seg2Start, const Vec2& seg2End, Point2& outContactPoint);

//------------------------------------------------------------------
/*	@class Vector3
*/
//------------------------------------------------------------------
struct	Vec3
{
public:
    float x, y, z;

public:
    // constructor
    Vec3() { x = y = z = 0.0f; }
    Vec3(Vec3&& v) noexcept { x = v.x; y = v.y; z = v.z; v.x = v.y = v.z = 0.0f; }
    Vec3(const Vec3& v) { x = v.x; y = v.y; z = v.z; }
    Vec3(float x, float y, float z) { this->x = x, this->y = y, this->z = z; }
    Vec3(const float* f) { this->x = f[0]; this->y = f[1]; this->z = f[2]; }

    //	스위즐링 2차원
    FORCEINLINE Vec2 xy() const { return Vec2(x, y); }
    FORCEINLINE Vec2 xz() const { return Vec2(x, z); }
    FORCEINLINE Vec2 yx() const { return Vec2(y, x); }
    FORCEINLINE Vec2 yz() const { return Vec2(y, z); }
    FORCEINLINE Vec2 zx() const { return Vec2(z, x); }
    FORCEINLINE Vec2 zy() const { return Vec2(z, y); }

    //	스위즐링 3차원
    FORCEINLINE Vec3 xyz() const { return Vec3(x, y, z); }
    FORCEINLINE Vec3 xzy() const { return Vec3(x, z, y); }
    FORCEINLINE Vec3 yxz() const { return Vec3(y, x, z); }
    FORCEINLINE Vec3 yzx() const { return Vec3(y, z, x); }
    FORCEINLINE Vec3 zxy() const { return Vec3(z, x, y); }
    FORCEINLINE Vec3 zyx() const { return Vec3(z, y, x); }

    // FORCEINLINE Vec3& operator -() { x *= -1.0f; y *= -1.0f; z *= -1.0f; return (*this); }
    FORCEINLINE Vec3 operator - () const { return Vec3(-x, -y, -z); }
    FORCEINLINE float& operator[] (uint8 i) { assert(i < 3); return (&x)[i]; }
    FORCEINLINE float operator[] (uint8 i) const { assert(i < 3); return (&x)[i]; }

    FORCEINLINE Vec3& operator = (Vec3&& v) noexcept { x = v.x; y = v.y; z = v.z; v.x = v.y = v.z = 0.0f; return *this; }
    FORCEINLINE Vec3& operator = (const Vec3& v) { x = v.x; y = v.y; z = v.z; return *this; }

    FORCEINLINE bool operator == (const Vec3& v) const { return (x == v.x && y == v.y && z == v.z) ? true : false; }
    FORCEINLINE bool operator != (const Vec3& v) const { return !(operator == (v)); }

    FORCEINLINE Vec3 operator + (const Vec3& v) const { return Vec3(x + v.x, y + v.y, z + v.z); }
    FORCEINLINE Vec3 operator - (const Vec3& v) const { return Vec3(x - v.x, y - v.y, z - v.z); }
    FORCEINLINE Vec3 operator * (const Vec3& v) const { return Vec3(v.x * x, v.y * y, v.z * z); }
    FORCEINLINE Vec3 operator * (float s) const { return Vec3(x * s, y * s, z * s); }
    FORCEINLINE Vec3 operator / (const Vec3& v) const { return Vec3(x / v.x, y / v.y, z / v.z); }
    FORCEINLINE Vec3 operator / (float s) const { assert(s > 0.0f); return Vec3(x / s, y / s, z / s); }

    FORCEINLINE Vec3& operator += (const Vec3& v) { x += v.x; y += v.y; z += v.z; return *this; }
    FORCEINLINE Vec3& operator -= (const Vec3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
    FORCEINLINE Vec3& operator *= (float f) { x *= f; y *= f; z *= f; return *this; }
    FORCEINLINE Vec3& operator /= (float f) { x /= f; y /= f; z /= f; return *this; }

    FORCEINLINE bool operator < (const Vec3& v) const { return (x < v.x&& y < v.y&& z < v.z) ? true : false; }
    FORCEINLINE bool operator > (const Vec3& v) const { return (x > v.x && y > v.y && z > v.z) ? true : false; }
    FORCEINLINE bool operator <= (const Vec3& v) const { return (x <= v.x && y <= v.y && z <= v.z) ? true : false; }
    FORCEINLINE bool operator >= (const Vec3& v) const { return (x >= v.x && y >= v.y && z >= v.z) ? true : false; }

    FORCEINLINE bool IsNearlyZero() const { return Math::IsNearlyZero(x) && Math::IsNearlyZero(y) && Math::IsNearlyZero(z); }

    FORCEINLINE float Size() const { return Math::Sqrt(SizeSquared()); }
    FORCEINLINE float SizeSquared() const { return (x * x + y * y + z * z); }

    FORCEINLINE Vec3& Normalize() { float s = 1.0f / Size(); *this *= s; return *this; }

    String ToString() const;

    static float Size(const Vec3& v) { return Math::Sqrt(v.x * v.x + v.y * v.y + v.z * v.z); }
    static Vec3 Multiply(const Vec3& a, const Vec3& b) { return Vec3(a.x * b.x, a.y * b.y, a.z * b.z); }
    static Vec3 Normalize(const Vec3& v) { float s = 1.0f / Math::Sqrt(v.x * v.x + v.y * v.y + v.z * v.z); return Vec3(v.x * s, v.y * s, v.z * s); }
    //static Vec3 Lerp(const Vec3& a, const Vec3& b, float w) { return (a + w * (b - a)); }
    static Vec3 CrossProduct(const Vec3& a, const Vec3& b) { return Vec3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x); }
    static float DotProduct(const Vec3& a, const Vec3& b) { return a.x * b.x + a.y * b.y + a.z * b.z; }

    static Vec3 GetRandom();
    static Vec3 GetRandom(float minValue, float maxValue);

    static bool IsNearlyEqual(const Vec3& v1, const Vec3& v2) { return Math::IsNearlyEqual(v1.x, v2.x) && Math::IsNearlyEqual(v1.y, v2.y) && Math::IsNearlyEqual(v1.z, v2.z); }

    static Vec3 ZERO;
    static Vec3 ONE;
    static Vec3 XAXIS, YAXIS, ZAXIS;
};

#define Point3 Vec3

// FORCEINLINE Fuction

/// Scale * Vector
FORCEINLINE Vec3 operator * (float scale, const Vec3& v) { return Vec3(v.x * scale, v.y * scale, v.z * scale); }

//------------------------------------------------------------------
/*	@class Vector4
*/
//------------------------------------------------------------------
struct	Vec4
{
public:
    float x, y, z, w;

public:
    // constructor (no initialize)
    Vec4() { x = y = z = w = 0.0f; }
    Vec4(Vec4&& v) noexcept { x = v.x; y = v.y; z = v.z; w = v.w; v.x = v.y = v.z = v.w = 0.0f; }
    Vec4(const Vec4& v) { x = v.x; y = v.y; z = v.z; w = v.w;; }
    Vec4(float x, float y, float z, float w) { this->x = x, this->y = y, this->z = z; this->w = w; }
    Vec4(const float* f) { this->x = f[0]; this->y = f[1]; this->z = f[2]; this->w = f[3]; }
    Vec4(const Vec3& v, float w) { this->x = v.x; this->y = v.y; this->z = v.z; this->w = w; }

    //	스위즐링 2차원
    FORCEINLINE Vec2 xy() const { return Vec2(x, y); }
    FORCEINLINE Vec2 xz() const { return Vec2(x, z); }
    FORCEINLINE Vec2 yx() const { return Vec2(y, x); }
    FORCEINLINE Vec2 yz() const { return Vec2(y, z); }
    FORCEINLINE Vec2 zx() const { return Vec2(z, x); }
    FORCEINLINE Vec2 zy() const { return Vec2(z, y); }

    //	스위즐링 3차원
    FORCEINLINE Vec3 xyz() const { return Vec3(x, y, z); }
    FORCEINLINE Vec3 xzy() const { return Vec3(x, z, y); }
    FORCEINLINE Vec3 yxz() const { return Vec3(y, x, z); }
    FORCEINLINE Vec3 yzx() const { return Vec3(y, z, x); }
    FORCEINLINE Vec3 zxy() const { return Vec3(z, x, y); }
    FORCEINLINE Vec3 zyx() const { return Vec3(z, y, x); }

    // FORCEINLINE Vec4& operator -() { x *= -1.0f; y *= -1.0f; z *= -1.0f; w *= -1.0f; return (*this); }
    FORCEINLINE Vec4 operator-() const { return Vec4(-x, -y, -z, -w); }
    FORCEINLINE float& operator[] (uint8 i) { assert(i < 4); return (&x)[i]; }
    FORCEINLINE float operator[] (uint8 i) const { assert(i < 4); return (&x)[i]; }

    FORCEINLINE Vec4 operator + (const Vec4& v) const { return Vec4(x + v.x, y + v.y, z + v.z, w + v.w); }
    FORCEINLINE Vec4 operator - (const Vec4& v) const { return Vec4(x - v.x, y - v.y, z - v.z, w - v.w); }
    FORCEINLINE Vec4 operator * (const Vec4& v) const { return Vec4(v.x * x, v.y * x, v.z * x, v.w * x); }
    FORCEINLINE Vec4 operator * (float s) const { return Vec4(x * s, y * s, z * s, w * s); }
    FORCEINLINE Vec4 operator / (const Vec4& v) const { return Vec4(x / v.x, y / v.y, z / v.z, w / v.w); }
    FORCEINLINE Vec4 operator / (float s) const { return Vec4(x / s, y / s, z / s, w / s); }

    FORCEINLINE Vec4& operator = (Vec4&& v) noexcept { x = v.x; y = v.y; z = v.z; w = v.w; v.x = v.y = v.z = v.w = 0.0f; return *this; }
    FORCEINLINE Vec4& operator = (const Vec4& v) { x = v.x; y = v.y; z = v.z; w = v.w; return *this; }

    FORCEINLINE bool operator == (const Vec4& v) const { return (x == v.x && y == v.y && z == v.z && w == v.w) ? true : false; }
    FORCEINLINE bool operator != (const Vec4& v) const { return !(operator == (v)); }

    FORCEINLINE Vec4& operator += (const Vec4& v) { this->x += v.x; this->y += v.y; this->z += v.z; this->w += v.w; return *this; }
    FORCEINLINE Vec4& operator -= (const Vec4& v) { this->x -= v.x; this->y -= v.y; this->z -= v.z; this->w -= v.w; return *this; }
    FORCEINLINE Vec4& operator *= (float f) { this->x *= f; this->y *= f; this->z *= f; this->w *= f; return *this; }
    FORCEINLINE Vec4& operator /= (float f) { this->x /= f; this->y /= f; this->z /= f; this->w /= f; return *this; }

    FORCEINLINE bool operator < (const Vec4& v) const { return (x < v.x&& y < v.y&& z < v.z&& w < v.w) ? true : false; }
    FORCEINLINE bool operator > (const Vec4& v) const { return (x > v.x && y > v.y && z > v.z && w > v.w) ? true : false; }
    FORCEINLINE bool operator <= (const Vec4& v) const { return (x <= v.x && y <= v.y && z <= v.z && w <= v.w) ? true : false; }
    FORCEINLINE bool operator >= (const Vec4& v) const { return (x >= v.x && y >= v.y && z >= v.z && w >= v.w) ? true : false; }

    FORCEINLINE bool IsNearlyZero() const { return Math::IsNearlyZero(x) && Math::IsNearlyZero(y) && Math::IsNearlyZero(z) && Math::IsNearlyZero(w); }

    FORCEINLINE float Size() const { return Math::Sqrt(SizeSquared()); }
    FORCEINLINE float SizeSquared() const { return (x * x + y * y + z * z + w * w); }

    FORCEINLINE Vec4& Normalize() { float s = 1.0f / Size(); *this *= s; return *this; }

    String ToString() const;

    static float Size(const Vec4& v) { return Math::Sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w); }
    static Vec4 Multiply(const Vec4& a, const Vec4& b) { return Vec4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w); }
    static Vec4 Normalize(const Vec4& v) { float s = 1.0f / Math::Sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w); return Vec4(v.x * s, v.y * s, v.z * s, v.w * s); }
    //static Vec4 Lerp(const Vec4& a, const Vec4& b, float ww) { return (a + ww * (b - a)); }
    static Vec4 GetRandom();
    static Vec4 GetRandom(float minValue, float maxValue);

    static bool IsNearlyEqual(const Vec4& v1, const Vec4& v2) { return Math::IsNearlyEqual(v1.x, v2.x) && Math::IsNearlyEqual(v1.y, v2.y) && Math::IsNearlyEqual(v1.z, v2.z) && Math::IsNearlyEqual(v1.w, v2.w); }

    static Vec4 ZERO;
    static Vec4 ONE;
};

// FORCEINLINE Fuction

/// Scale * Vector
FORCEINLINE Vec4 operator * (float scale, const Vec4& v) { return Vec4(v.x * scale, v.y * scale, v.z * scale, v.w * scale); }
