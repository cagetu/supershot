// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"

struct Plane;
struct Capsule;

//------------------------------------------------------------------
/** @class	Shpere
    @desc   ��
*/
//------------------------------------------------------------------
class Sphere
{
public:
    Sphere() { radius = 0.0f; }
    Sphere(Sphere&& s) noexcept { origin = s.origin; radius = s.radius; s.origin = Point3::ZERO; s.radius = 0.0f; }
    Sphere(const Sphere&& s) { origin = s.origin; radius = s.radius; }
    Sphere(const Point3& c, float r) { this->origin = c; this->radius = r; }

    inline Sphere& operator = (Sphere&& s) noexcept { origin = s.origin; radius = s.radius; s.origin = Point3::ZERO; s.radius = 0.0f; return *this; }
    inline Sphere& operator = (const Sphere&& s) { origin = s.origin; radius = s.radius; return *this; }

    inline bool operator == (const Sphere& s) const { return (origin == s.origin) && (radius == s.radius); }
    inline bool operator != (const Sphere& s) const { return !(operator == (s)); }

	Point3& Center() { return origin; }
	const Point3& Center() const { return origin; }

    float& Radius() { return radius; }
    const float Radius() const { return radius; }

    bool ContactTest(const Sphere& sphere) const;
    bool IsInside(const Point3& point) const;
    
	float SweepTest(const Sphere& sphere, const Vec3& dir, Plane* out=NULL) const;
    float RayIntersect(const Point3& origin, const Vec3& dir, Plane* out=NULL) const;

	static float SweepTest(const Sphere & sphere, const Vec3 & dir, const Sphere & body, Plane * out) { return body.SweepTest(sphere, dir, out); }
	static float SweepTest(const Sphere & sphere, const Vec3 & dir, const Vec3 * poly, int vtxnum, Plane * out);
	static float SweepTest(const Sphere & sphere, const Vec3 & dir, const Capsule & capsule, Plane * out);

public:
    Point3 origin;
    float radius;
} ;
