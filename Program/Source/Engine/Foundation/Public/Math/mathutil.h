// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

//	기본 수학 함수들

#define _PI 			(3.1415926535897932f)
// Aux constants.
#define INV_PI			(0.31830988618f)
#define HALF_PI			(1.57079632679f)

// Copied from float.h
#define MAX_FLT 3.402823466e+38F
#define __EPSILON (1.0e-30f)
#define SMALL_NUMBER	(1.e-8f)

#define DEG2RAD(d)	((d)*(_PI/180.0f))
#define RAD2DEG(d)	((d)*(180.0f/_PI))

#define THRESH_VECTOR_NORMALIZED		(0.01f)		/** Allowed error for a normalized vector (against squared magnitude) */
#define THRESH_QUAT_NORMALIZED			(0.01f)		/** Allowed error for a normalized quaternion (against squared magnitude) */

#ifndef MAX
#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

typedef float Degree;
typedef float Radian;

struct Math
{
	template<class T>	static CONSTEXPR FORCEINLINE T Min(const T a, const T b) { return (a<b) ? a : b; }
	template<class T>	static CONSTEXPR FORCEINLINE T Max(const T a, const T b) { return (a>b) ? a : b; }
	template<class T>	static CONSTEXPR FORCEINLINE T Abs(const T A) { return (A >= (T)0) ? A : -A; }
	template<class T>	static CONSTEXPR FORCEINLINE T Sign(const T A) { return (A > (T)0) ? (T)1 : ((A < (T)0) ? (T)-1 : (T)0); }
	template<class T>	static CONSTEXPR FORCEINLINE void Swap(T &a, T &b) { T tmp = a; a = b; b = tmp; }

	static FORCEINLINE int32 Trunc(float value) { return (int32)value; }
	static FORCEINLINE int32 Round(float value) { return ((value - ::floor(value) > 0.5) ? (int32)::ceil(value) : (int32)::floor(value)); }
	static FORCEINLINE float Ceil(float value) { return ::ceilf(value); }
	static FORCEINLINE float Floor(float value) { return (float)::floor(value); }
	static FORCEINLINE float Fractional(float Value) { return Value - (float)Trunc(Value); }
	static FORCEINLINE float Frac(float Value) { return Value - Floor(Value); }

	static FORCEINLINE float Saturate(float value) { return MAX(0.0f, MIN(1.0f, value)); }

	static FORCEINLINE float Clamp(float target, float minValue, float maxValue) { return MAX(minValue, MIN(maxValue, target)); }
	static FORCEINLINE int32 Clamp(int32 target, int32 minValue, int32 maxValue) { return MAX(minValue, MIN(maxValue, target)); }
	static FORCEINLINE uint32 Clamp(uint32 target, uint32 minValue, uint32 maxValue) { return MAX(minValue, MIN(maxValue, target)); }
	//template <class _T> static CONSTEXPR FORCEINLINE const _T Clamp(const _T& value, const _T& low, const _T& high) { return MAX(low, MIN(high, value));	}

	/** Multiples value by itself */
	template< class T >
	static FORCEINLINE T Square(const T A)
	{
		return A * A;
	}

	template <class T> 
	static FORCEINLINE T Lerp(T x, T y, float l) 
	{ 
		return x + l * (y - x); 
	}

	static FORCEINLINE float Lerp(float x, float y, float l)
	{
		return x + l * (y - x);
	}

	static FORCEINLINE float Smoothstep(float a, float b, float x)
	{
		float t = Saturate((x - a) / (b - a));
		return t*t*(3.0f - (2.0f - t));
	}

	/** Performs a 2D linear interpolation between four values values, FracX, FracY ranges from 0-1 */
	template< class T, class U >
	static FORCEINLINE T BiLerp(const T& P00, const T& P10, const T& P01, const T& P11, const U& FracX, const U& FracY)
	{
		return Lerp(
			Lerp(P00, P10, FracX),
			Lerp(P01, P11, FracX),
			FracY
		);
	}

	/**
	 * Performs a cubic interpolation
	 *
	 * @param  P - end points
	 * @param  T - tangent directions at end points
	 * @param  Alpha - distance along spline
	 *
	 * @return  Interpolated value
	 */
	template< class T, class U >
	static FORCEINLINE T CubicInterp(const T& P0, const T& T0, const T& P1, const T& T1, const U& A)
	{
		const float A2 = A * A;
		const float A3 = A2 * A;

		return (T)(((2 * A3) - (3 * A2) + 1) * P0) + ((A3 - (2 * A2) + A) * T0) + ((A3 - A2) * T1) + (((-2 * A3) + (3 * A2)) * P1);
	}

	/**
	 * Performs a first derivative cubic interpolation
	 *
	 * @param  P - end points
	 * @param  T - tangent directions at end points
	 * @param  Alpha - distance along spline
	 *
	 * @return  Interpolated value
	 */
	template< class T, class U >
	static FORCEINLINE T CubicInterpDerivative(const T& P0, const T& T0, const T& P1, const T& T1, const U& A)
	{
		T a = 6.f * P0 + 3.f * T0 + 3.f * T1 - 6.f * P1;
		T b = -6.f * P0 - 4.f * T0 - 2.f * T1 + 6.f * P1;
		T c = T0;

		const float A2 = A * A;

		return (a * A2) + (b * A) + c;
	}

	/**
	 * Performs a second derivative cubic interpolation
	 *
	 * @param  P - end points
	 * @param  T - tangent directions at end points
	 * @param  Alpha - distance along spline
	 *
	 * @return  Interpolated value
	 */
	template< class T, class U >
	static FORCEINLINE T CubicInterpSecondDerivative(const T& P0, const T& T0, const T& P1, const T& T1, const U& A)
	{
		T a = 12.f * P0 + 6.f * T0 + 6.f * T1 - 12.f * P1;
		T b = -6.f * P0 - 4.f * T0 - 2.f * T1 + 6.f * P1;

		return (a * A) + b;
	}

	/** Interpolate between A and B, applying an ease in function.  Exp controls the degree of the curve. */
	template< class T >
	static FORCEINLINE T InterpEaseIn(const T& A, const T& B, float Alpha, float Exp)
	{
		float const ModifiedAlpha = Pow(Alpha, Exp);
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolate between A and B, applying an ease out function.  Exp controls the degree of the curve. */
	template< class T >
	static FORCEINLINE T InterpEaseOut(const T& A, const T& B, float Alpha, float Exp)
	{
		float const ModifiedAlpha = 1.f - Pow(1.f - Alpha, Exp);
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolate between A and B, applying an ease in/out function.  Exp controls the degree of the curve. */
	template< class T >
	static FORCEINLINE T InterpEaseInOut(const T& A, const T& B, float Alpha, float Exp)
	{
		return Lerp<T>(A, B, (Alpha < 0.5f) ?
			InterpEaseIn(0.f, 1.f, Alpha * 2.f, Exp) * 0.5f :
			InterpEaseOut(0.f, 1.f, Alpha * 2.f - 1.f, Exp) * 0.5f + 0.5f);
	}

	/** Interpolation between A and B, applying a step function. */
	template< class T >
	static FORCEINLINE T InterpStep(const T& A, const T& B, float Alpha, int32 Steps)
	{
		if (Steps <= 1 || Alpha <= 0)
		{
			return A;
		}
		else if (Alpha >= 1)
		{
			return B;
		}

		const float StepsAsFloat = static_cast<float>(Steps);
		const float NumIntervals = StepsAsFloat - 1.f;
		float const ModifiedAlpha = FloorToFloat(Alpha * StepsAsFloat) / NumIntervals;
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying a sinusoidal in function. */
	template< class T >
	static FORCEINLINE T InterpSinIn(const T& A, const T& B, float Alpha)
	{
		float const ModifiedAlpha = -1.f * Cos(Alpha * HALF_PI) + 1.f;
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying a sinusoidal out function. */
	template< class T >
	static FORCEINLINE T InterpSinOut(const T& A, const T& B, float Alpha)
	{
		float const ModifiedAlpha = Sin(Alpha * HALF_PI);
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying a sinusoidal in/out function. */
	template< class T >
	static FORCEINLINE T InterpSinInOut(const T& A, const T& B, float Alpha)
	{
		return Lerp<T>(A, B, (Alpha < 0.5f) ?
			InterpSinIn(0.f, 1.f, Alpha * 2.f) * 0.5f :
			InterpSinOut(0.f, 1.f, Alpha * 2.f - 1.f) * 0.5f + 0.5f);
	}

	/** Interpolation between A and B, applying an exponential in function. */
	template< class T >
	static FORCEINLINE T InterpExpoIn(const T& A, const T& B, float Alpha)
	{
		float const ModifiedAlpha = (Alpha == 0.f) ? 0.f : Pow(2.f, 10.f * (Alpha - 1.f));
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying an exponential out function. */
	template< class T >
	static FORCEINLINE T InterpExpoOut(const T& A, const T& B, float Alpha)
	{
		float const ModifiedAlpha = (Alpha == 1.f) ? 1.f : -Pow(2.f, -10.f * Alpha) + 1.f;
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying an exponential in/out function. */
	template< class T >
	static FORCEINLINE T InterpExpoInOut(const T& A, const T& B, float Alpha)
	{
		return Lerp<T>(A, B, (Alpha < 0.5f) ?
			InterpExpoIn(0.f, 1.f, Alpha * 2.f) * 0.5f :
			InterpExpoOut(0.f, 1.f, Alpha * 2.f - 1.f) * 0.5f + 0.5f);
	}

	/** Interpolation between A and B, applying a circular in function. */
	template< class T >
	static FORCEINLINE T InterpCircularIn(const T& A, const T& B, float Alpha)
	{
		float const ModifiedAlpha = -1.f * (Sqrt(1.f - Alpha * Alpha) - 1.f);
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying a circular out function. */
	template< class T >
	static FORCEINLINE T InterpCircularOut(const T& A, const T& B, float Alpha)
	{
		Alpha -= 1.f;
		float const ModifiedAlpha = Sqrt(1.f - Alpha * Alpha);
		return Lerp<T>(A, B, ModifiedAlpha);
	}

	/** Interpolation between A and B, applying a circular in/out function. */
	template< class T >
	static FORCEINLINE T InterpCircularInOut(const T& A, const T& B, float Alpha)
	{
		return Lerp<T>(A, B, (Alpha < 0.5f) ?
			InterpCircularIn(0.f, 1.f, Alpha * 2.f) * 0.5f :
			InterpCircularOut(0.f, 1.f, Alpha * 2.f - 1.f) * 0.5f + 0.5f);
	}

	static FORCEINLINE float Exp(float Value) { return expf(Value); }
	static FORCEINLINE float LogE(float Value) { return logf(Value); }
	static FORCEINLINE float LogX(float Base, float Value) { return LogE(Value) / LogE(Base); }
	static FORCEINLINE float Log2(float Value) { return LogE(Value) * 1.4426950f /* 1.0 / Loge(2) = 1.4426950f */; }

	static FORCEINLINE float FMod(float X, float Y) { return fmodf(X, Y); }
	static FORCEINLINE float Pow(float A, float B) { return powf(A, B); }
	static FORCEINLINE float Sqrt(float value) { return ::sqrtf(value); }
	static FORCEINLINE float InvSqrt(float value) { return 1.0f / ::sqrtf(value); }

	static FORCEINLINE float Cos(Radian rad) { return ::cosf(rad); }
	static FORCEINLINE float Sin(Radian rad) { return ::sinf(rad); }
	static FORCEINLINE float Tan(Radian rad) { return ::tanf(rad); }

	static FORCEINLINE float ACos(Radian rad)
	{
		if (-1.0f < rad)
			return rad < 1.0f ? (Radian)::acos(rad) : 0.0f;
		return _PI;
	}

	static FORCEINLINE float ASin(Radian rad)
	{
		if (-1.0f < rad)
			return rad < 1.0f ? (Radian)::asin(rad) : HALF_PI;	// Half _PI
		return -HALF_PI;
	}

	// Computes the sine and cosine of a scalar value.
	static FORCEINLINE void SinCos(float* ScalarSin, float* ScalarCos, float  Value)
	{
		// Map Value to y in [-pi,pi], x = 2*pi*quotient + remainder.
		float quotient = (INV_PI * 0.5f) * Value;
		if (Value >= 0.0f)
		{
			quotient = (float)((int)(quotient + 0.5f));
		}
		else
		{
			quotient = (float)((int)(quotient - 0.5f));
		}
		float y = Value - (2.0f * _PI) * quotient;

		// Map y to [-pi/2,pi/2] with sin(y) = sin(Value).
		float sign;
		if (y > HALF_PI)
		{
			y = _PI - y;
			sign = -1.0f;
		}
		else if (y < -HALF_PI)
		{
			y = -_PI - y;
			sign = -1.0f;
		}
		else
		{
			sign = +1.0f;
		}

		float y2 = y * y;

		// 11-degree minimax approximation
		*ScalarSin = (((((-2.3889859e-08f * y2 + 2.7525562e-06f) * y2 - 0.00019840874f) * y2 + 0.0083333310f) * y2 - 0.16666667f) * y2 + 1.0f) * y;

		// 10-degree minimax approximation
		float p = ((((-2.6051615e-07f * y2 + 2.4760495e-05f) * y2 - 0.0013888378f) * y2 + 0.041666638f) * y2 - 0.5f) * y2 + 1.0f;
		*ScalarCos = sign * p;
	}

	// Note:  We use FASTASIN_HALF_PI instead of HALF_PI inside of FastASin(), since it was the value that accompanied the minimax coefficients below.
	// It is important to use exactly the same value in all places inside this function to ensure that FastASin(0.0f) == 0.0f.
	// For comparison:
	//		HALF_PI				== 1.57079632679f == 0x3fC90FDB
	//		FASTASIN_HALF_PI	== 1.5707963050f  == 0x3fC90FDA
#define FASTASIN_HALF_PI (1.5707963050f)
	/**
	* Computes the ASin of a scalar value.
	*
	* @param Value  input angle
	* @return ASin of Value
	*/
	static FORCEINLINE float FastASin(float Value)
	{
		// Clamp input to [-1,1].
		bool nonnegative = (Value >= 0.0f);
		float x = Math::Abs(Value);
		float omx = 1.0f - x;
		if (omx < 0.0f)
		{
			omx = 0.0f;
		}
		float root = Math::Sqrt(omx);
		// 7-degree minimax approximation
		float result = ((((((-0.0012624911f * x + 0.0066700901f) * x - 0.0170881256f) * x + 0.0308918810f) * x - 0.0501743046f) * x + 0.0889789874f) * x - 0.2145988016f) * x + FASTASIN_HALF_PI;
		result *= root;  // acos(|x|)
		// acos(x) = pi - acos(-x) when x < 0, asin(x) = pi/2 - acos(x)
		return (nonnegative ? FASTASIN_HALF_PI - result : result - FASTASIN_HALF_PI);
	}
#undef FASTASIN_HALF_PI

	static FORCEINLINE float ATan(Radian rad) { return ::atanf(rad); }
	static FORCEINLINE float ATan2(float height, float width) { return ::atan2f(height, width); }	// 높이, 너비로 직각삼각형 각도 계산

	static FORCEINLINE float Tan2Sin(float t) { return t / ::sqrtf(t*t + 1.0f); }
	static FORCEINLINE float Tan2Cos(float t) { return 1.0f / ::sqrtf(t*t + 1.0f); }

	static FORCEINLINE bool IsPower2(int32 x) { return (!((x)& ((x)-1))); }

	static FORCEINLINE bool IsNaN(float A) { return ((*(uint*)&A) & 0x7FFFFFFF) > 0x7F800000; }
	static FORCEINLINE bool IsFinite(float A) {	return ((*(uint*)&A) & 0x7F800000) != 0x7F800000; }

	static FORCEINLINE bool IsNearlyEqual(float A, float B, float ErrorTolerance = __EPSILON) {	return Abs<float>(A - B) <= ErrorTolerance;	}
	static FORCEINLINE bool IsNearlyEqual(double A, double B, double ErrorTolerance = __EPSILON) { return Abs<double>(A - B) <= ErrorTolerance; }
	static FORCEINLINE bool IsNearlyZero(float Value, float ErrorTolerance = __EPSILON) { return Abs<float>(Value) <= ErrorTolerance; }
	static FORCEINLINE bool IsNearlyZero(double Value, double ErrorTolerance = __EPSILON) {	return Abs<double>(Value) <= ErrorTolerance; }
};
