// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

#include "Math\vector.h"

struct Mat4;

//------------------------------------------------------------------
/** @class	Object Oriented Bounding Box
    @desc   축이 정렬되지 않은 바운딩 박스 처리
*/
//------------------------------------------------------------------
struct	OBB
{
	Vec3 Center;
	Vec3 Axis[3];
	float Extent[3];

public:
	OBB();
    OBB(OBB&& obb) noexcept;
	OBB(const OBB& obb);
    OBB(const Vec3 & center, const Vec3 * axis, const float * extent);

	OBB& operator = (OBB&& obb) noexcept;
	OBB& operator = (const OBB& obb);

	bool operator == (const OBB& rhs) const;
	bool operator != (const OBB& rhs) const { return !(*this == rhs); }

	const Vec3& GetCenter() const { return Center; }
	const Vec3* GetAxises() const { return Axis; }
	const float* GetExtents() const { return Extent; }

	void Expand(const OBB& obb);

	OBB Transform(const Mat4& tm) const;
	void Translate(const Vec3& v);

	void GetVertices(Vec3 * vertices) const;

	// Intersect
	bool IntersectTest(const Vec3& origin, const Vec3& dir, float& outDistance);
	bool IntersectTest(const Vec3& origin, const Vec3& dir);
	bool IntersectTest(const OBB& Obb);
    
	//
	// Statics
	//
    static OBB MakeFrom(const Vec3* vertices, unsigned int count);
};
