// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/** @class	RandomNumber
	@desc   난수 생성기
*/
//------------------------------------------------------------------
struct RandomNumber
{
	static void Startup(int32 seed);
	static void Shutdown();

	static int32 Rand();

	static int32 RandInt();
	static int32 RandInt(int32 minValue, int32 maxValue);
	static float RandFloat();
	static float RandFloat(float minValue, float maxValue);
};
