// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Math\vector.h"

//------------------------------------------------------------------
/*	@class 2D Rect
	@desc 좌상단이 (0, 0)을 기준으로 한다.
*/
//------------------------------------------------------------------
struct Rect2D
{
	float X;
	float Y;
	float Width;
	float Height;

	Rect2D(float x, float y, float width, float height);
	Rect2D(const Vec2& leftTop, const Vec2& rightBottom);

	Vec2 LeftTop() const { return Vec2(X, Y); }
	Vec2 RightTop() const { return Vec2(X + Width, Y); }
	Vec2 RightBottom() const { return Vec2(X + Width, Y + Height); }
	Vec2 LeftBottom() const { return Vec2(X, Y + Height); }

	Vec2 Center() const { return Vec2(X + Width * 0.5f, Y + Height * 0.5f); }
	Vec2 Size() const { return Vec2(Width, Height); }

	// Segment - Rect 교차
	bool IntersectTest(const Vec2& segmentStart, const Vec2& segmentEnd);
};

//------------------------------------------------------------------
/*	@class Box2D
	@desc 2D 상의 Box를 지정
*/
//------------------------------------------------------------------
struct Box2D
{
	Vec2 Min;
	Vec2 Max;

	Box2D(const Vec2& min, const Vec2& max);

	bool Intersect(const Box2D& other) const;
};