// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Math\vector.h"

//------------------------------------------------------------------
/*	@class  Ray
	@desc   3차원 Ray를 표현한다.
            - 3D 상의 한 점 P
            - Origin (시작점)
            - Direction (방향)

            P(t) = Origin + t * Direction;
*/
//------------------------------------------------------------------
struct Ray
{
public:
    Ray() : origin(Vec3::ZERO), direction(Vec3::ZERO) {}
    Ray(Ray&& r) noexcept { origin = r.origin; direction = r.direction; r.origin = Point3::ZERO; r.direction = Vec3::ZERO; }
    Ray(const Ray& r) { origin = r.origin; direction = r.direction; }
    Ray(const Point3& orig, const Vec3& dir);

    FORCEINLINE Ray& operator = (Ray& r) noexcept { origin = r.origin; direction = r.direction; r.origin = Point3::ZERO; r.direction = Vec3::ZERO; return *this; }
    FORCEINLINE Ray& operator = (const Ray& r) { origin = r.origin; direction = r.direction; return *this; }

    FORCEINLINE bool operator == (const Ray& r) const { return (origin == r.origin) && (direction == r.direction); }
    FORCEINLINE bool operator != (const Ray& r) const { return !(*this == r); }

    /// 시작점
    Point3& Origin() { return origin; }
    const Point3& Origin() const { return origin; }

    /// 진행 방향
    Vec3& Direction() { return direction; }
    const Vec3& Direction() const { return direction; }
    
    /// Ray 상의 한 점을 구한다.
    Point3 GetPoint(float t) const;

    //
    //  Statics
    //
    static Ray MakeFrom(const Point3& startPoint, const Point3& endPoint);

private:
    Point3 origin;
    Vec3 direction;

    /*  Ray Interval
    *   t-interval 오차 범위를 고려해서 교차될 수 있는 t의 범위를 제한한다. [Tmin, Tmax]
    *   예를 들어서) self-intersection을 피하기 위해서 epsilon 크기 만큼을 제외 시킨다. [Tmin = epsilon, Tmax = 1-epsion]
    *   DXR에서 RayDesc에 정의된다.
    */
    float Tmin = 0.0f;
    float Tmax = MAX_FLT;
};


//------------------------------------------------------------------
/*  @class  Ray Tracer
    @desc   Ray Trace를 위해서 Ray가 Hit 되었는지를 순회하기 위한 객체
*/
//------------------------------------------------------------------
class IRayHittable
{
public:
    struct HitResult
    {
        Point3 point;
        Vec3 normal;
    };
    
public:
    virtual bool RayTest(const Ray& Ray, float minT, float maxT, HitResult& output) = 0;
};

