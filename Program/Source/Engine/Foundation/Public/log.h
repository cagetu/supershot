// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "logstream.h"

//------------------------------------------------------------------
/**	Log
	@author cagetu
	@desc 로그 클래스
*/
//------------------------------------------------------------------
class Log
{
public:
	/// log levels
	enum Level
	{
		None = 0,
		_Error,
		_Warn,
		_Info,
		_Debug,

		Max,
		InvalidLevel
	};

	/// 최초 시작
	static void _Startup();
	/// 최종 마무리
	static void _Shutdown();

	static void Register(const Ptr<ILogStream>& stream);
	static void Unregister(const Ptr<ILogStream>& stream);

	static void SetLevel(Level level);

	static void Debug(Level level, const char* fileName, const char* funcName, int32 lineNumber, const TCHAR* format, ...);
	static void DebugUtf8(Level level, const char* fileName, const char* funcName, int32 lineNumber, const char* format, ...);

private:
	static void Text(const TCHAR* text);
	static void Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);
	static void Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);
	static void Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);

	static Array<Ptr<ILogStream> > LogStreams;
	static Level LogLevel;
};

#define ULOG(level, format, ...) \
( \
	Log::Debug(level, __FILE__, __FUNCTION__, __LINE__, TEXT(format), ##__VA_ARGS__) \
)

#define ULOG_UTF8(level, format, ...) \
( \
	Log::DebugUtf8(level, __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__) \
)
