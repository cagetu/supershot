// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

struct Vec4;

//------------------------------------------------------------------
/**	@class	LinearColor
	@desc	0 ~ 1 로 표현되는 칼라
*/
//------------------------------------------------------------------
struct LinearColor
{
	union
	{
		struct 
		{
			float r, g, b, a;	/* 0.0f ~ 1.0f */
		};
		float channel[4]; // {r, g, b, a}
	};

	LinearColor() : r(1.0f), g(1.0f), b(1.0f), a(1.0f) {}
	LinearColor(LinearColor&& rhs) noexcept { r = rhs.r; g = rhs.g; b = rhs.b; a = rhs.a; rhs.g = rhs.b = rhs.a = 0.0f; }
	LinearColor(const LinearColor& rhs) : r(rhs.r), g(rhs.g), b(rhs.b), a(rhs.a) {}
	LinearColor(float red, float green, float blue, float alpha = 1.0): r(red), g(green), b(blue), a(alpha) {}
	LinearColor(const int8 red, const int8 green, const int8 blue, const int8 alpha);
	LinearColor(RGBA rgba);

	FORCEINLINE operator RGBA () const { return TO_RGBA((unsigned int)((r)*255.f),(unsigned int)((g)*255.f),(unsigned int)((b)*255.f),(unsigned int)((a)*255.f)); }
	FORCEINLINE operator Vec4& () const { return *(Vec4*)&r; }

	FORCEINLINE bool operator == (const LinearColor& rhs) const { return (r == rhs.r && g == rhs.g && b == rhs.b && a == rhs.a); }
	FORCEINLINE bool operator != (const LinearColor& rhs) const { return !(*this == rhs); }

	FORCEINLINE LinearColor& operator = (RGBA c) { (*this) = LinearColor(c); return *this; }
	FORCEINLINE LinearColor& operator = (const LinearColor& rhs) { r = rhs.r; g = rhs.g; b = rhs.b; a = rhs.a;  return *this; }
	FORCEINLINE LinearColor& operator = (LinearColor&& rhs) noexcept { r = rhs.r; g = rhs.g; b = rhs.b; a = rhs.a; rhs.g = rhs.b = rhs.a = 0.0f; return *this; }

	static LinearColor WHITE;
	static LinearColor BLACK;
	static LinearColor RED;
	static LinearColor GREEN;
	static LinearColor BLUE;
	static LinearColor YELLOW;
};

inline LinearColor operator * (const LinearColor& c, float s) { return LinearColor(c.r*s, c.g*s, c.b*s, c.a*s); }
inline LinearColor operator + (const LinearColor& a, const LinearColor& b) { return LinearColor(a.r+b.r, a.g+b.g, a.b+b.b, a.a+b.a); }
inline LinearColor operator - (const LinearColor& a, const LinearColor& b) { return LinearColor(a.r-b.r, a.g-b.g, a.b-b.b, a.a-b.a); }
