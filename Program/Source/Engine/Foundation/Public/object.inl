// Copyright (c) 2006~. cagetu
//
//******************************************************************

//------------------------------------------------------------------
inline
ObjectID IObject::GetUID() const
{
	return m_ulID;
}

//------------------------------------------------------------------
template <class T>
T* StaticCast( IObject* pObject )
{
	return (T*)pObject;
}
template <class T>
const T* StaticCast( const IObject* pObject )
{
	return (const T*)pObject;
}

//------------------------------------------------------------------
template <class T>
T* DynamicCast( IObject* pObject )
{
	return pObject && pObject->IsDerivedFrom(&T::RTTI) ? (T*)pObject : 0;
}
template <class T>
const T* DynamicCast( const IObject* pObject )
{
	return pObject && pObject->IsDerivedFrom(&T::RTTI) ? (const T*)pObject : 0;
}
