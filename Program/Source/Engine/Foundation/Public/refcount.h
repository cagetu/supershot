// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
//	@class	ThreadSafe reference ī����
//------------------------------------------------------------------
class RefCount
{
public:
	RefCount() = default;
	virtual ~RefCount() = default;

	RefCount(const RefCount& Rhs) = delete;
	RefCount& operator=(const RefCount& Rhs) = delete;

	uint32 RefCount::AddRef()
	{
		//++m_RefCount;
		return uint32(Atomics::InterlockedIncrement(&m_RefCount));
	}

	uint32 RefCount::Release()
	{
		//--m_RefCount;
		const int32 count = Atomics::InterlockedDecrement(&m_RefCount);
		if (count <= 0)
			delete this;

		return uint32(count);
	}

	uint32 GetRefCount() const
	{
		return uint32(m_RefCount);
	}

protected:
	volatile int32 m_RefCount = 0;
};

//------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------
#define __DeclareReference(classname) \
public : \
	uint32 AddRef() { return uint32(Atomics::InterlockedIncrement( &m_RefCount )); } \
	uint32 Release() { const int32 count = Atomics::InterlockedDecrement(&m_RefCount); if (count <= 0) { delete this; } return uint32(count); } \
	uint32 GetRefCount() const { return uint32(m_RefCount); } \
private : \
	volatile int32 m_RefCount; \
private :

#define __ConstructReference \
	m_RefCount = 0; 

#define __DestructReference \
	CHECK(m_RefCount == 0, TEXT("RefCount == 0"));
