// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

enum SerialMode
{
	PropertySave = 1 << 0,
	PropertyTransient = 1 << 1,
};

template <typename TYPE, int32 MODE>
class SerialProperty
{
public:
	SerialProperty(const TYPE& type)
		: m_Type(type)
		, m_Mode(MODE)
	{
	}
	operator TYPE()
	{
		return m_Type;
	}

private:
	TYPE m_Type;
	int32 m_Mode;
};
