// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	Log
	@author cagetu
	@desc 로그 스트림
*/
//------------------------------------------------------------------
class ILogStream : public RefCount
{
public:
	virtual ~ILogStream() {}

	virtual void Text(const TCHAR* text) = 0;
	virtual void Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber) = 0;
	virtual void Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber) = 0;
	virtual void Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber) = 0;
};

//------------------------------------------------------------------
/**	LogFile
	@author cagetu
	@desc
		- 로그를 파일에 기록한다
*/
//------------------------------------------------------------------
class LogFile : public ILogStream
{
public:
	LogFile(const TCHAR* filePath, bool bConsoleOut = false, bool bDebugOut = true);
	virtual ~LogFile();

	bool Open();
	void Close();

	virtual void Text(const TCHAR* text);
	virtual void Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);
	virtual void Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);
	virtual void Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);

protected:
	void Begin();
	void End();

	void PutTime();
	void PutDate();
	void PutLine(uint32 nNumLines = 1);
	void PutTab(uint32 nNumTabs = 1);
	void PutString(const TCHAR* format, ...);

	virtual void PutHeader();
	virtual void PutFooter();

	String m_FilePath;
	uint32 m_bDebugOut : 1;
	uint32 m_bConsoleOut : 1;

	FileIO* m_File;
	PlatformConsole m_Console;

	int32 m_Indentation;

private:
	CriticalSection m_CS;
};