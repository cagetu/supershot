#pragma once

//--------------------------------------------------------------
//	스마트 포인터 객체
//
//--------------------------------------------------------------
template <class T>
class Ptr
{
public :
	Ptr();
	Ptr(T* o, bool bAddRef = true);
	Ptr(const Ptr& copy);
	Ptr(Ptr&& move) noexcept;
	~Ptr();

	void Empty();
	bool IsValid() const;
	bool IsValidRef(const Ptr& o) const;

	T* Get() const;
	T** GetInitRef();

	operator T() const;
	operator T*() const;
	operator const T*() const;
	T& operator* () const;
	T* operator-> () const;

	Ptr& operator =( T* o );
	Ptr& operator =( const Ptr& o );
	Ptr& operator =( Ptr&& copy ) noexcept;

	bool operator ==( T* o ) const;
	bool operator !=( T* o ) const;
	bool operator ==( const Ptr& o ) const;
	bool operator !=( const Ptr& o ) const;

	template <class OtherT> Ptr<OtherT> Cast() const;
	template <class OtherT> Ptr<OtherT> DynamicCast() const;

private:
	T*	object;			//!< 공유 객체

	template<class T> friend class WeakPtr;
};

//--------------------------------------------------------------
//	WeakPointer
//--------------------------------------------------------------
template <class T>
class WeakPtr
{
public:
	WeakPtr();
	WeakPtr(const Ptr<T>& ptr);
	WeakPtr(const WeakPtr<T>& ptr);
	~WeakPtr();

	void operator=(const Ptr<T>& rhs);
	void operator=(const WeakPtr<T>& rhs);
	void operator=(T* rhs);
	T* operator->() const;
	T& operator*() const;

	operator T*() const;
	T* Get() const;

	bool IsValid() const;

private:
	Ptr<T> m_Ptr;
};

//--------------------------------------------------------------
//	Pointer Wapper
//	- default null value로 생성자를 가진다.
//--------------------------------------------------------------
template<class T>
class RawPtr
{
public:
	RawPtr(T* ptr = nullptr);

	T* ptr() const;
	T*& ref();

	T& operator*() const;
	T* operator->() const;

private:
	T* m_Ptr;
};

#include "ptr.inl"