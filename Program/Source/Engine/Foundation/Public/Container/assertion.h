// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

namespace Assertion
{
	/*	ScopeDescriptor
		- Assertion의 현재 Scope를 알기 위한 함수 범위 Stack
		- 너무 빈빈하게 설정하지 않으며, 큰 단위의 함수에만 적용한다.
	*/
	struct Scope
	{
		Scope(const TCHAR* func)
		{
			name = func;
			next = current;
			current = this;
		}
		~Scope()
		{
			current = next;
		}
		Scope* next;
		const TCHAR* name;
		static Scope* current;
	} ;

	void HandleSkippableAssert(const TCHAR* fileName, const TCHAR* functionName, int lineNumber, const char* expression);
	void HandleSkippableAssert(const TCHAR* fileName, const TCHAR* functionName, int lineNumber, const char* expression, const TCHAR* format, ...);
};

// boolean을 반환한다. expression이 false 일 때 발생... release일 때에는 true로 반환시킨다.
#define CHECK(expression, format, ...) \
( \
	( (expression) ? true : \
		(Assertion::HandleSkippableAssert(__FILEW__, __FUNCTIONW__, __LINE__, #expression, format, ##__VA_ARGS__), false) \
	) \
)

#define WARNNING(format, ...) \
( \
	(Assertion::HandleSkippableAssert(__FILEW__, __FUNCTIONW__, __LINE__, "", format), false) \
)

#define ASSERT(expression) \
( \
	( (expression) ? true : \
		(Assertion::HandleSkippableAssert(__FILEW__, __FUNCTIONW__, __LINE__, #expression), false) \
	) \
)
