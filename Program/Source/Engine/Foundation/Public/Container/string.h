#pragma once
// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************

/** String
	@author cagetu
	@desc
		- 현재 character Set에 대응하는 String 객체...
*/
typedef __string<TCHAR> String;

namespace Str
{
	int32 Length(const TCHAR* str);
	bool IsEqual(const TCHAR* dest, const TCHAR* src);

	int32 Scanf(const TCHAR* buffer, const TCHAR* format, ...);
	String Printf(const TCHAR* format, ...);
	String VPrintf(const TCHAR* format, va_list ap);
	String Format(const TCHAR* fmt, ...);
	String VFormat(const TCHAR* fmt, va_list ap);
}

int32 Strlen(const TCHAR* str);
TCHAR* Strchr(const TCHAR* str, TCHAR ch);
TCHAR* Strrchr(const TCHAR* str, TCHAR ch);
TCHAR* Strstr(const TCHAR* str1, const TCHAR* str2);
int32 Strcmp(const TCHAR* str1, const TCHAR* str2);
int32 Stricmp(const TCHAR* str1, const TCHAR* str2);
void Strcpy(TCHAR* dest, int32 destsize, const TCHAR* src);
void StrcpyPart(TCHAR* dest, TCHAR* src, size_t size);
TCHAR* Strncpy(TCHAR* dest, int32 destsize, TCHAR* source, int32 count);
TCHAR* Strcat(TCHAR* dest, const TCHAR* src);
TCHAR* Strlwr(TCHAR* str, int32 sizeinwords);
String Strlwr(const String& str);

void StrTime(TCHAR* str, int32 sizeinwords);
void StrDate(TCHAR* str, int32 sizeinwords);

//	number
bool StrToBool(const TCHAR* str);
float StrToFloat(const TCHAR* str);
long StrToLong(const TCHAR* str);
void StrToVec2(const TCHAR* str, const Vec2* vec);
void StrToVec3(const TCHAR* str, const Vec3* vec);
void StrToVec4(const TCHAR* str, const Vec4* vec);
int32 StrToInt(const TCHAR* str);
void StrToInt2(const TCHAR* str, int* int2);
void StrToInt3(const TCHAR* str, int* int3);
int32 StrToHex(const TCHAR* buf, int32 len);
TCHAR* IntToStr(int32 val, TCHAR* buf, int32 radix = 10);
TCHAR* IntToStr(int32 value, int32 width, int32 trim);
TCHAR* FloatToStr(float val, TCHAR* buf, int32 dp = 5);
void HexToStr(uint32 val, TCHAR* buf, unsigned radix, int32 negative);

//	format
// %d, %i, %l , %f, %s only!!!
int32 SScanf(const TCHAR* buffer, const TCHAR* format, ...);
TCHAR* SPrintf(TCHAR* dest, int32 destlen, const TCHAR* format, ...);
TCHAR* VSPrintf(TCHAR* dest, int32 destlen, const TCHAR* format, va_list ap);
String SFormat(const TCHAR* fmt, ...);
String VFormat(const TCHAR* fmt, va_list ap);

//	FilePath
String SplitFilePath(const TCHAR* path);
String SplitFileName(const TCHAR* path);
String SplitFileExt(const TCHAR* path);
String SplitFileNameExt(const TCHAR* path);
void SplitPath(TCHAR* path, TCHAR* drive, TCHAR* dir, TCHAR* fname, TCHAR* ext);

String& Trim(String& str, const String whitespace = TEXT(" \t\n\r"));
SizeT Tokenize(Array<String>& output, const String& input, const String& whitespace = TEXT(" \t\n\r"));

int32 ConvertToUTF8(char* dest, int32 destsize, const TCHAR* src);
int32 ConvertToWide(wchar* dest, int32 destsize, const TCHAR* src);

String ConvertToString(const char* str);