// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

namespace Template
{
	template<typename T> struct TRemoveReference { typedef T Type; };
	template<typename T> struct TRemoveReference<T& > { typedef T Type; };
	template<typename T> struct TRemoveReference<T&&> { typedef T Type; };

	template<typename T> struct TRValueToLValueReference { typedef T  Type; };
	template<typename T> struct TRValueToLValueReference<T&&> { typedef T& Type; };

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	/*	Move: R-Value로 캐스팅 하는 것
	 * 
	 *	TYPE&&: l-value와 r-value 를 모두 받을 수 있다. (forward reference)
	 *			인자로 lvalue 전달 :	TYPE => Test& 로 들어온다. TYPE&& : Test& && => Test& (reference collapse && 와 && 일 때에만 && 된다)
	 *			인자로 rvalue 전달 : TYPE => Test 로 들어온다. TYPE&& : Test && => Test&& 
	 */
	template <typename TYPE>
	FORCEINLINE typename TRemoveReference<TYPE>::Type&& Move(TYPE&& value)
	{
		// rvalue로 캐스팅 하는 것이 목표 -> reference& 일 경우를 없애기 위해서, reference 제거 후 캐스팅 
		// (제거하지 않으면, Type& && -> Type& 형태로 캐스팅 될 수 있다.)
		typedef typename TRemoveReference<TYPE>::Type CastType;

		return (CastType&&)value;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	/*	Perfect Forwarding : 완벽한 전달. rvalue와 lvalue가 모두 전달이 잘 되어서 처리될 수 있도록 처리가 필요
	*						 (reference collapse를 이용해서, rvalue만 rvalue가 되도록 캐스팅 한다)
	*	template <typename F, type T>
	*	void chronometry(F f, T&& arg)
	*	{
	*		f (std::forward<T>(arg));	// fowrard()가 내부적으로 캐스팅을 수행
	*	}
	*/
	template <typename TYPE>
	FORCEINLINE TYPE&& Forward(typename TRemoveReference<TYPE>::Type& value)
	{
		return (TYPE&&)value;
	}

	template <typename TYPE>
	FORCEINLINE TYPE&& Forward(typename TRemoveReference<TYPE>::Type&& value)
	{
		return (TYPE&&)value;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename TYPE>
	FORCEINLINE TYPE Copy(TYPE& Val)
	{
		return const_cast<const TYPE&>(Val);
	}

	template <typename TYPE>
	FORCEINLINE TYPE Copy(const TYPE& Val)
	{
		return Val;
	}

	template <typename TYPE>
	FORCEINLINE TYPE&& Copy(TYPE&& Val)
	{
		// If we already have an rvalue, just return it unchanged, rather than needlessly creating yet another rvalue from it.
		return Move(Val);
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename TYPE>
	FORCEINLINE SIZE_T Distance(TYPE* first, TYPE* last)
	{
		return last - first;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
/**
 * Does a boolean AND of the ::Value static members of each type, but short-circuits if any Type::Value == false.
 */
template <typename... Types>
struct TAnd;

template <bool LHSValue, typename... RHS>
struct TAndValue
{
	enum { Value = TAnd<RHS...>::Value };
};

template <typename... RHS>
struct TAndValue<false, RHS...>
{
	enum { Value = false };
};

template <typename LHS, typename... RHS>
struct TAnd<LHS, RHS...> : TAndValue<LHS::Value, RHS...>
{
};

template <>
struct TAnd<>
{
	enum { Value = true };
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
/**
 * Does a boolean OR of the ::Value static members of each type, but short-circuits if any Type::Value == true.
 */
template <typename... Types>
struct TOr;

template <bool LHSValue, typename... RHS>
struct TOrValue
{
	enum { Value = TOr<RHS...>::Value };
};

template <typename... RHS>
struct TOrValue<true, RHS...>
{
	enum { Value = true };
};

template <typename LHS, typename... RHS>
struct TOr<LHS, RHS...> : TOrValue<LHS::Value, RHS...>
{
};

template <>
struct TOr<>
{
	enum { Value = false };
};

/**
 * Does a boolean NOT of the ::Value static members of the type.
 */
template <typename Type>
struct TNot
{
	enum { Value = !Type::Value };
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
/**
* Binary predicate class for sorting elements in reverse order.  Assumes < operator is defined for the template type.
*
* See: http://en.cppreference.com/w/cpp/utility/functional/greater
*/
template <typename T = void>
struct TGreater
{
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return B < A;
	}
};

template <>
struct TGreater<void>
{
	template <typename T>
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return B < A;
	}
};

/**
* Binary predicate class for sorting elements in order.  Assumes < operator is defined for the template type.
* Forward declaration exists in ContainersFwd.h
*
* See: http://en.cppreference.com/w/cpp/utility/functional/less
*/
template <typename T /*= void */>
struct TLess
{
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return A < B;
	}
};

template <>
struct TLess<void>
{
	template <typename T>
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return A < B;
	}
};


/**
 * Includes a function in an overload set if the predicate is true.  It should be used similarly to this:
 *
 * // This function will only be instantiated if SomeTrait<T>::Value is true for a particular T
 * template <typename T>
 * typename TEnableIf<SomeTrait<T>::Value, ReturnType>::Type Function(const T& Obj)
 * {
 *     ...
 * }
 *
 * ReturnType is the real return type of the function.
 */
template <bool Predicate, typename Result = void>
class TEnableIf;

template <typename Result>
class TEnableIf<true, Result>
{
public:
	typedef Result Type;
};

template <typename Result>
class TEnableIf<false, Result>
{ };


/** Tests whether two typenames refer to the same type. */
template<typename A, typename B>
struct TAreTypesEqual;

template<typename, typename>
struct TAreTypesEqual
{
	enum { Value = false };
};

template<typename A>
struct TAreTypesEqual<A, A>
{
	enum { Value = true };
};

#define ARE_TYPES_EQUAL(A,B) TAreTypesEqual<A,B>::Value


/**
 * Reverses the order of the bits of a value.
 * This is an TEnableIf'd template to ensure that no undesirable conversions occur.  Overloads for other types can be added in the same way.
 *
 * @param Bits - The value to bit-swap.
 * @return The bit-swapped value.
 */
template <typename T>
FORCEINLINE typename TEnableIf<TAreTypesEqual<T, uint32>::Value, T>::Type ReverseBits(T Bits)
{
	Bits = (Bits << 16) | (Bits >> 16);
	Bits = ((Bits & 0x00ff00ff) << 8) | ((Bits & 0xff00ff00) >> 8);
	Bits = ((Bits & 0x0f0f0f0f) << 4) | ((Bits & 0xf0f0f0f0) >> 4);
	Bits = ((Bits & 0x33333333) << 2) | ((Bits & 0xcccccccc) >> 2);
	Bits = ((Bits & 0x55555555) << 1) | ((Bits & 0xaaaaaaaa) >> 1);
	return Bits;
}

/**
 * utility template for a class that should not be copyable.
 * Derive from this class to make your class non-copyable
 */
class Noncopyable
{
protected:
	// ensure the class cannot be constructed directly
	Noncopyable() {}
	// the class should not be used polymorphically
	~Noncopyable() {}
private:
	Noncopyable(const Noncopyable&);
	Noncopyable& operator=(const Noncopyable&);
};
