#pragma once

//------------------------------------------------------------------==========================
/*	Dynamic Map Container Class
	- Red-Black Tree를 사용하는 Map이 아니라, Pair<Key, Value>를 가지는 Array 컨테이너이다.
*/
//------------------------------------------------------------------==========================
template <typename KEY, typename VALUE, typename ALLOCATOR = Memory::Allocator>
class Map
{
public:
	Map();
	Map(const Map& other);
	Map(Map&& other);
	~Map();

	/// type defines
	typedef Pair<KEY, VALUE>& reference;
	typedef const Pair<KEY, VALUE>& const_reference;
	typedef Pair<KEY, VALUE>* iterator;
	typedef const Pair<KEY, VALUE>* const_iterator;

	void operator=(const Map& other);
	void operator=(Map&& other);

	// Key 값에 값을 넣어준다. 단, 같은 Key가 들어올 때에는 Overwrite하지 않는다.
	IndexT Insert(const KEY& key, const VALUE& value);
	// Key 값에 값을 넣어준다. 단, 같은 Key가 들어올 때에는 Overwrite하지 않는다.
	IndexT Insert(const Pair<KEY, VALUE>& item);
	// Key 값에 값을 넣어준다. 단, 같은 Key가 들어올 때에는 Overwrite하지 않는다.
	IndexT Insert(Pair<KEY, VALUE>&& item);

	void Remove(const KEY& key);
	void RemoveAll(const KEY& key);
	void RemoveAt(IndexT index, bool bShrink = false);

	VALUE& operator[](const KEY& key);
	const VALUE& operator[](const KEY& key) const;

	const KEY& KeyAt(IndexT index) const;
	VALUE& ValueAt(IndexT index);
	const VALUE& ValueAt(IndexT index) const;

	/// 사용 중인 Element 개수
	SizeT Num() const;
	/// 할당된 최대 Element 개수
	SizeT Capacity() const;
	/// 비어 있는지 확인
	bool IsEmpty() const;

	/// Size만큼 버퍼 크기를 확보한다.
	void Reserve(SizeT size);
	/// 사용하는 크기에 맞게 최대 크기를 맞춘다.
	void Shrink();
	/// Size를 0으로 한다.
	void Clear();

	/// 포함 여부 확인
	bool Contains(const KEY& key) const;
	/// Index 찾기
	IndexT FindIndex(const KEY& key) const;
	/// 찾거나 추가
	VALUE& FindOrAdd(const KEY& key);

	/// C++ conform begin, MAY RETURN nullptr!
	Pair<KEY, VALUE>* begin();
	/// C++ conform begin, MAY RETURN nullptr!
	const Pair<KEY, VALUE>* begin() const;
	/// C++ conform end,  MAY RETURN nullptr!
	Pair<KEY, VALUE>* end();
	/// C++ conform end, MAY RETURN nullptr!
	const Pair<KEY, VALUE>* end() const;

private:
	Array<Pair<KEY, VALUE>, ALLOCATOR> m_Elements;
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::Map()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::Map(const Map& other)
	: m_Elements(other.m_Elements)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::Map(Map&& other)
	//: m_Elements((Array<Pair<KEY, VALUE>>&&)other.m_Elements)	
	//: m_Elements(std::move(other.m_Elements))
	: m_Elements(Template::Move(other.m_Elements))
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::~Map()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::operator=(const Map& other)
{
	m_Elements = other.m_Elements;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::operator=(Map&& other)
{
	//m_Elements = std::move(other.m_Elements);
	m_Elements = Template::Move(other.m_Elements);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
SizeT Map<KEY, VALUE, ALLOCATOR>::Num() const
{
	return m_Elements.Num();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
SizeT Map<KEY, VALUE, ALLOCATOR>::Capacity() const
{
	return m_Elements.Capacity();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
bool Map<KEY, VALUE, ALLOCATOR>::IsEmpty() const
{
	return m_Elements.IsEmpty();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Reserve(SizeT size)
{
	m_Elements.Reserve(size);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Shrink()
{
	m_Elements.Shrink();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Clear()
{
	m_Elements.Clear();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
VALUE& Map<KEY, VALUE, ALLOCATOR>::operator[](const KEY& key)
{
	_ASSERT(m_Elements.Data());
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	_ASSERT(it != m_Elements.end() && (key == it->key));
	return it->Value();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const VALUE& Map<KEY, VALUE, ALLOCATOR>::operator[](const KEY& key) const
{
	_ASSERT(m_Elements.Data());
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	_ASSERT(it != m_Elements.end() && (key == it->key));
	return it->Value();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const KEY& Map<KEY, VALUE, ALLOCATOR>::KeyAt(IndexT index) const
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	_ASSERT(index < Size());

	return m_Elements[index].key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const VALUE& Map<KEY, VALUE, ALLOCATOR>::ValueAt(IndexT index) const
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	//_ASSERT(index < Size());

	return m_Elements[index].value;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
VALUE& Map<KEY, VALUE, ALLOCATOR>::ValueAt(IndexT index)
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	//_ASSERT(index < Size());

	return m_Elements[index].value;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
bool Map<KEY, VALUE, ALLOCATOR>::Contains(const KEY& key) const
{
	return m_Elements.Contains(key);
	//return std::binary_search(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key));
	//return (m_Elements.BinarySearchIndex(key) > 0) ? true : false;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::FindIndex(const KEY& key) const
{
	//auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key));
	//if ((it != m_Elements.end()) && (key == it->Key()))
	//	return IndexT(it - m_Elements.begin());
	//return INVALID_INDEX;
	return m_Elements.FindIndex(key);
}

////-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
VALUE& Map<KEY, VALUE, ALLOCATOR>::FindOrAdd(const KEY& key)
{
	IndexT index = FindIndex(key);
	if (index == INVALID_INDEX)
	{
		index = Insert(key, VALUE());
	}
	return ValueAt(index);
}

////-----------------------------------------------------------------------------------------------------------------------------------------------------
//template <typename KEY, typename VALUE, typename ALLOCATOR>
//IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const KEY& key, const VALUE& value)
//{
//	return Insert(Pair<KEY, VALUE>(key, value));
//}
//
////-----------------------------------------------------------------------------------------------------------------------------------------------------
//template <typename KEY, typename VALUE, typename ALLOCATOR>
//IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const Pair<KEY, VALUE>& item)
//{
//	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
//	IndexT index = IndexT(it - m_Elements.begin());
//	return m_Elements.Insert(index, item);
//}
//
////-----------------------------------------------------------------------------------------------------------------------------------------------------
//template <typename KEY, typename VALUE, typename ALLOCATOR>
//IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(Pair<KEY, VALUE>&& item)
//{
//	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
//	IndexT index = IndexT(it - m_Elements.begin());
//	return m_Elements.Insert(index, item);
//}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const KEY& key, const VALUE& value)
{
	return Insert(Pair<KEY, VALUE>(key, value));
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const Pair<KEY, VALUE>& item)
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
	if ((it != m_Elements.end()) && (item.key == it->key))
	{
		//return IndexT(it - m_Elements.begin());
		return INVALID_INDEX; // 같은 값이 있으면 실패라고 해버리자!!!
	}

	IndexT index = IndexT(it - m_Elements.begin());	// Size() - 1;
	return m_Elements.Insert(index, item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(Pair<KEY, VALUE>&& item)
{
	//auto test = std::binary_search(m_Elements.begin(), m_Elements.end(), item);

	// key 값보다 큰 가장 작은 정수의 값을 찾는다.
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
	if ((it != m_Elements.end()) && (item.key == it->key))
	{
		//return IndexT(it - m_Elements.begin());
		return INVALID_INDEX; // 같은 값이 있으면 실패라고 해버리자!!!
	}

	IndexT index = IndexT(it - m_Elements.begin());	// Size() - 1;
	return m_Elements.Insert(index, item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Remove(const KEY& key)
{
	IndexT index = FindIndex(key);
	if (index > INVALID_INDEX)
	{
		m_Elements.RemoveAt(index);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::RemoveAll(const KEY& key)
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	if (it != m_Elements.end())
	{
		const IndexT index = IndexT(it - m_Elements.begin());
		while (index < m_Elements.Num() && m_Elements[index].Key() == key)
		{
			m_Elements.RemoveAt(index);
		}
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::RemoveAt(IndexT index, bool bShrink)
{
	m_Elements.RemoveAt(index, bShrink);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::begin()
{
	return m_Elements.begin();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::begin() const
{
	return m_Elements.begin();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::end()
{
	return m_Elements.end();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::end() const
{
	return m_Elements.end();
}

