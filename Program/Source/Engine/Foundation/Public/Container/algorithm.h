#pragma once

namespace AlgoBase
{
	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename RangeValueType, typename PredicateValueType, typename SortPredicateType>
	FORCEINLINE SIZE_T __LowerBound(RangeValueType* first, RangeValueType* last, const PredicateValueType& value, SortPredicateType SortPredicate)
	{
		SIZE_T pos = 0;
		SIZE_T size = Template::Distance(first, last);

		while (size > 0)
		{
			const SIZE_T left = size % 2;
			size = size / 2;

			const SIZE_T index = pos + size;
			const SIZE_T posIfLess = index + left;

			pos = SortPredicate(first[index], value) ? posIfLess : pos;
		}
		return pos;
	}

	template <typename RangeValueType, typename PredicateValueType, typename SortPredicateType>
	FORCEINLINE SIZE_T ___UpperBound(RangeValueType* first, RangeValueType* last, const PredicateValueType& Value, SortPredicateType SortPredicate)
	{
		SIZE_T pos = 0;
		SIZE_T size = Template::Distance(first, last);

		while (size > 0)
		{
			const SIZE_T left = size % 2;
			size = size / 2;

			const SIZE_T index = pos + size;
			const SIZE_T posIfLess = index + left;

			pos = !SortPredicate(ptr[index], value) ? posIfLess : pos;
		}
		return pos;
	}
}

namespace Algorithm
{
	// BinarySearch
	// LowerBound
	// UpperBound

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename RangeType, typename ValueType, typename SortPredicateType>
	FORCEINLINE int32 LowerBound(RangeType& Range, const ValueType& Value, SortPredicateType SortPredicate)
	{
		return (int32)AlgoBase::__LowerBound(Range.begin(), Range.end(), Value, SortPredicate);
	}
	template <typename RangeType, typename ValueType>
	FORCEINLINE int32 LowerBound(RangeType& Range, const ValueType& Value)
	{
		return (int32)AlgoBase::__LowerBound(Range.begin(), Range.end(), Value, TLess<ValueType>());
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename RangeType, typename ValueType, typename SortPredicateType>
	FORCEINLINE int32 UpperBound(RangeType& Range, const ValueType& Value, SortPredicateType SortPredicate)
	{
		return (int32)AlgoBase::__UpperBound(Range.begin(), Range.end(), Value, SortPredicate);
	}
	template <typename RangeType, typename ValueType>
	FORCEINLINE int32 UpperBound(RangeType& Range, const ValueType& Value)
	{
		return (int32)AlgoBase::__UpperBound(Range.begin(), Range.end(), Value, Template::Less<ValueType>());
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename RangeType, typename ValueType, typename SortPredicateType>
	FORCEINLINE int32 BinarySearch(RangeType& Range, const ValueType& Value, SortPredicateType SortPredicate)
	{
		SIZE_T CheckIndex = LowerBound(Range, Value, SortPredicate);
		SIZE_T Size = Template::Distance(Range.begin(), Range.end());
		if (CheckIndex < Size)
		{
			auto&& CheckValue = Range.begin()[CheckIndex];
			// Since we returned lower bound we already know Value <= CheckValue. So if Value is not < CheckValue, they must be equal
			if (!SortPredicate(Value, CheckValue))
			{
				return (int32)CheckIndex;
			}
		}
		return INVALID_INDEX;
	}
	template <typename RangeType, typename ValueType>
	FORCEINLINE int32 BinarySearch(RangeType& Range, const ValueType& Value)
	{
		return BinarySearch(Range, Value, Template::Less<ValueType>());
	}

}