#ifndef __UNICODE_H__
#define __UNICODE_H__

#include "Container\array.h"

//! Very simple string class with some useful features.
/** string<char> and string<wchar_t> both accept Unicode AND ASCII/Latin-1,
	so you can assign Unicode to string<char> and ASCII/Latin-1 to string<wchar_t>
	(and the other way round) if you want to.
	However, note that the conversation between both is not done using any encoding.
	This means that char strings are treated as ASCII/Latin-1, not UTF-8, and
	are simply expanded to the equivalent wchar_t, while Unicode/wchar_t
	characters are truncated to 8-bit ASCII/Latin-1 characters, discarding all
	other information in the wchar_t.
	Helper functions for converting between UTF-8 and wchar_t are provided
	outside the string class for explicit use.
*/

// unicode::utf8, unicode::string(wide)

uint32 locale_lower(uint32 x);
uint32 locale_upper(uint32 x);

template <typename T, typename TAlloc = Memory::Allocator>
class __string
{
public:
	__string()
		: array(0), allocated(0), used(1)
	{
		//array = allocator.AllocPODs<T>(1); // new T[1];
		//array[0] = 0x0;
		//allocated = used = 1;
		local[0] = 0x00;
	}

	__string(const __string<T, TAlloc>& other)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;
		*this = other;
	}

	__string(__string<T, TAlloc>&& other)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;
		//if (array)
		//{
		//	allocator.Deallocate(array); // delete [] array;
		//	array = NULL;
		//}

		used = other.used;
		if (used > _NUM_SMALL)
		{
			allocated = other.allocated;
			array = other.array;
		}
		else
		{
			allocated = 0;

			const T* p = other.c_str();
			for (int32 i = 0; i < used; ++i, ++p)
				local[i] = *p;
		}

		//*this = other;
		other.local[0] = 0x00;
		other.array = 0;
		other.allocated = 0;
		other.used = 1;
	}

	template <class B, class A>
	__string(const __string<B, A>& other)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;
		*this = other;
	}

	//! Constructs a string from a float
	explicit __string(const double number)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;
		char tmpbuf[255];
		::snprintf(tmpbuf, 255, "%0.6f", number);
		*this = tmpbuf;
	}

	//! Constructs a string from an int32 
	explicit __string(int32 number)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;

		// store if negative and make positive

		bool negative = false;
		if (number < 0)
		{
			number *= -1;
			negative = true;
		}

		// temporary buffer for 16 numbers

		char tmpbuf[16] = { 0 };
		uint32 idx = 15;

		// special case '0'

		if (!number)
		{
			tmpbuf[14] = '0';
			*this = &tmpbuf[14];
			return;
		}

		// add numbers

		while (number && idx)
		{
			--idx;
			tmpbuf[idx] = (char)('0' + (number % 10));
			number /= 10;
		}

		// add sign

		if (negative)
		{
			--idx;
			tmpbuf[idx] = '-';
		}

		*this = &tmpbuf[idx];
	}


	//! Constructs a string from an uint32 
	explicit __string(uint32 number)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;

		// temporary buffer for 16 numbers

		char tmpbuf[16] = { 0 };
		uint32 idx = 15;

		// special case '0'

		if (!number)
		{
			tmpbuf[14] = '0';
			*this = &tmpbuf[14];
			return;
		}

		// add numbers

		while (number && idx)
		{
			--idx;
			tmpbuf[idx] = (char)('0' + (number % 10));
			number /= 10;
		}

		*this = &tmpbuf[idx];
	}

	//! Constructs a string from a long
	explicit __string(long number)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;

		// store if negative and make positive

		bool negative = false;
		if (number < 0)
		{
			number *= -1;
			negative = true;
		}

		// temporary buffer for 16 numbers

		char tmpbuf[16] = { 0 };
		uint32 idx = 15;

		// special case '0'

		if (!number)
		{
			tmpbuf[14] = '0';
			*this = &tmpbuf[14];
			return;
		}

		// add numbers

		while (number && idx)
		{
			--idx;
			tmpbuf[idx] = (char)('0' + (number % 10));
			number /= 10;
		}

		// add sign

		if (negative)
		{
			--idx;
			tmpbuf[idx] = '-';
		}

		*this = &tmpbuf[idx];
	}

	//! Constructs a string from an unsigned long
	explicit __string(unsigned long number)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;

		// temporary buffer for 16 numbers

		char tmpbuf[16] = { 0 };
		uint32 idx = 15;

		// special case '0'

		if (!number)
		{
			tmpbuf[14] = '0';
			*this = &tmpbuf[14];
			return;
		}

		// add numbers

		while (number && idx)
		{
			--idx;
			tmpbuf[idx] = (char)('0' + (number % 10));
			number /= 10;
		}

		*this = &tmpbuf[idx];
	}

	template <class B>
	__string(const B* const c, uint32 length)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;

		if (!c)
		{
			*this = "";
			return;
		}

		if (length >= _NUM_SMALL)
		{
			allocated = used = length + 1;
			array = allocator.AllocPODs<T>(used); // new T[used];

			for (uint32 l = 0; l < length; ++l)
				array[l] = (T)c[l];
			array[length] = 0;
		}
		else
		{
			used = length + 1;
			for (uint32 l = 0; l < length; ++l)
				local[l] = (T)c[l];
			local[length] = 0;
		}
	}

	template <class B>
	__string(const B* const c)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;
		*this = c;
	}

	__string(const T* begin, const T* end)
	{
		const uint32 n = (uint32)(end - begin);

		// not yet!!!
	}

	__string(const __string<T, TAlloc>& other, uint32 length)
		: array(0), allocated(0), used(1)
	{
		local[0] = 0x00;

		*this = __string(other.c_str(), length);
	}

	~__string()
	{
		if (array)
			allocator.Deallocate(array); // delete [] array;
		array = NULL;
		used = 1;
		allocated = 0;
		local[0] = 0x00;
	}

	__string<T, TAlloc>& operator=(const __string<T, TAlloc>& other)
	{
		if (this == &other)
			return *this;

		used = (int32)other.size() + 1;
		if (used >= _NUM_SMALL)
		{
			if (used > allocated)
			{
				allocator.DeallocPOD(array); // delete [] array;
				allocated = used;
				array = allocator.AllocPODs<T>(used); //new T[used];
			}

			const T* p = other.c_str();
			for (int32 i = 0; i < used; ++i, ++p)
				array[i] = *p;
		}
		else
		{
			if (array)
			{
				allocator.Deallocate(array); // delete [] array;
				array = NULL;
			}
			allocated = 0;

			const T* p = other.c_str();
			for (int32 i = 0; i < used; ++i, ++p)
				local[i] = *p;
		}
		return *this;
	}

	__string<T, TAlloc>& operator=(__string<T, TAlloc>&& other)
	{
		if (other.array != array)
		{
			allocator.Deallocate(array); // delete [] array;
			array = NULL;
		}

		used = other.used;
		if (used >= _NUM_SMALL)
		{
			allocated = other.allocated;
			array = other.array;
		}
		else
		{
			allocated = 0;

			const T* p = other.c_str();
			for (int32 i = 0; i < used; ++i, ++p)
				local[i] = *p;

			other.local[0] = 0x00;
		}

		other.used = 1;
		other.allocated = 0;
		other.array = NULL;
		return *this;
	}

	template <class B, class A>
	__string<T, TAlloc>& operator=(const __string<B, A>& other)
	{
		*this = other.c_str();
		return *this;
	}

	template <class B>
	__string<T, TAlloc>& operator=(const B* const c)
	{
		if (!c)
		{
			//if (!array)
			//{
			//	array = allocator.AllocPODs<T>(1); //new T[1];
			//	allocated = 1;
			//}
			//array[0] = 0x0;
			local[0] = 0x00;
			if (array)
				array[0] = 0x00;
			used = 1;
			return *this;
		}

		if ((void*)c == (void*)array)
			return *this;

		uint32 len = 0;
		const B* p = c;
		do
		{
			++len;
		} while (*p++);

		T* oldArray = array;

		used = len;
		if (used >= _NUM_SMALL)
		{
			if (used > allocated)
			{
				allocated = used;
				array = allocator.AllocPODs<T>(used); //new T[used];
			}

			for (uint32 l = 0; l < len; ++l)
				array[l] = (T)c[l];
		}
		else
		{
			for (uint32 l = 0; l < len; ++l)
				local[l] = (T)c[l];
		}

		if (oldArray && oldArray != array)
		{
			allocator.Deallocate(oldArray); // delete [] oldArray;
		}

		return *this;
	}

	__string<T, TAlloc> operator+(const __string<T, TAlloc>& other) const
	{
		__string<T, TAlloc> str(*this);
		str.append(other);

		return str;
	}

	template <class B>
	__string<T, TAlloc> operator+(const B* const c) const
	{
		__string<T, TAlloc> str(*this);
		str.append(c);

		return str;
	}

	const T* operator * () const
	{
		return c_str();
	}

	T& operator [](const uint32 index)
	{
		CHECK(isValidIndex(index), TEXT("operator[]"));
		T* str = const_cast<T*>(c_str());
		return str[index];
	}

	const T& operator [](const uint32 index) const
	{
		CHECK(isValidIndex(index), TEXT("operator[]"));
		T* str = const_cast<T*>(c_str());
		return str[index];
	}

	bool operator ==(const T* const other) const
	{
		if (!other)
			return false;

		T* str = const_cast<T*>(c_str());

		uint32 i;
		for (i = 0; str[i] && other[i]; ++i)
		{
			if (str[i] != other[i])
				return false;
		}
		return !str[i] && !other[i];
	}

	bool operator ==(const __string<T, TAlloc>& other) const
	{
		T* str = const_cast<T*>(c_str());

		for (uint32 i = 0; str[i] && other[i]; ++i)
		{
			if (str[i] != other[i])
				return false;
		}
		return used == other.used;
	}

	bool operator <(const __string<T, TAlloc>& other) const
	{
		T* str = const_cast<T*>(c_str());

		if (used == other.used)
		{
			for (uint32 i = 0; str[i] && other[i]; ++i)
			{
				int32 diff = str[i] - other[i];
				if (diff)
					return diff < 0;
			}
		}
		return used < other.used;
	}

	bool operator >(const __string<T, TAlloc>& other) const
	{
		T* str = const_cast<T*>(c_str());

		if (used == other.used)
		{
			for (uint32 i = 0; str[i] && other[i]; ++i)
			{
				int32 diff = str[i] - other[i];
				if (diff)
					return diff > 0;
			}
		}
		return used > other.used;
	}

	bool operator !=(const T* const str) const
	{
		return !(*this == str);
	}

	bool operator !=(const __string<T, TAlloc>& other) const
	{
		return !(*this == other);
	}

	void shrink()
	{
		if (used >= _NUM_SMALL)
		{
			if (used < allocated)
			{
				T* newArray = allocator.AllocPODs<T>(used);
				if (array)
				{
					for (uint32 l = 0; l < used; ++l)
						newArray[l] = (T)array[l];

					allocator.Deallocate(array);
				}

				array = newArray;
				allocated = used;
			}
		}
		else
		{
			if (array)
			{
				for (uint32 l = 0; l < used; ++l)
					local[l] = (T)array[l];

				allocator.Deallocate(array);
				array = NULL;
			}
			allocated = 0;
		}
	}

	// memory size is not changed!!
	void reset()
	{
		local[0] = 0x00;
		if (array)
			array[0] = 0x0;
		used = 1;
	}

	// 
	void clear(int32 slack = 0)
	{
		local[0] = 0x00;
		if (array)
			array[0] = 0x0;
		used = 1;

		if (allocated < slack)
		{
			reallocate(slack);
		}
	}

	bool empty() const
	{
		return size() > 0 ? false : true;
	}

	uint32 length() const
	{
		return (array != NULL) ? allocated : _NUM_SMALL;
	}

	uint32 size() const
	{
		return used - 1;
	}

	const T* c_str() const
	{
		return (array != NULL) ? array : &local[0];
	}

	void to_lower()
	{
		T* str = const_cast<T*>(c_str());
		for (int32 i = 0; i < used; ++i)
			str[i] = locale_lower(str[i]);
	}

	void to_upper()
	{
		T* str = const_cast<T*>(c_str());
		for (int32 i = 0; i < used; ++i)
			str[i] = locale_upper(str[i]);
	}

	//! Compares the strings ignoring case.
	/** \param other: Other string to compare.
	\return True if the strings are equal ignoring case. */
	bool equals_ignore_case(const __string<T, TAlloc>& other) const
	{
		for (uint32 i = 0; c_str()[i] && other.c_str()[i]; ++i)
		{
			if (locale_lower(c_str()[i]) != locale_lower(other.c_str()[i]))
				return false;
		}

		return used == other.used;
	}

	//! Compares the strings ignoring case.
	/** \param other: Other string to compare.
		\param sourcePos: where to start to compare in the string
	\return True if the strings are equal ignoring case. */
	bool equals_substring_ignore_case(const __string<T, TAlloc>&other, const int32 sourcePos = 0) const
	{
		if ((uint32)sourcePos >= used)
			return false;

		uint32 i;
		for (i = 0; c_str()[sourcePos + i] && other[i]; ++i)
		{
			if (locale_lower(c_str()[sourcePos + i]) != locale_lower(other.c_str()[i]))
				return false;
		}

		return c_str()[sourcePos + i] == 0 && other.c_str()[i] == 0;
	}

	//! Compares the strings ignoring case.
	/** \param other: Other string to compare.
	\return True if this string is smaller ignoring case. */
	bool lower_ignore_case(const __string<T, TAlloc>& other) const
	{
		for (uint32 i = 0; c_str()[i] && other.c_str()[i]; ++i)
		{
			int32 diff = (int32)locale_lower(c_str()[i]) - (int32)locale_lower(other.c_str()[i]);
			if (diff)
				return diff < 0;
		}

		return used < other.used;
	}


	//! compares the first n characters of the strings
	/** \param other Other string to compare.
	\param n Number of characters to compare
	\return True if the n first characters of both strings are equal. */
	bool equalsn(const __string<T, TAlloc>& other, uint32 n) const
	{
		uint32 i;
		for (i = 0; i < n && c_str()[i] && other.c_str()[i]; ++i)
		{
			if (c_str()[i] != other.c_str()[i])
				return false;
		}

		// if one (or both) of the strings was smaller then they
		// are only equal if they have the same length
		return (i == n) || (used == other.used);
	}


	//! compares the first n characters of the strings
	/** \param str Other string to compare.
	\param n Number of characters to compare
	\return True if the n first characters of both strings are equal. */
	bool equalsn(const T* const str, uint32 n) const
	{
		if (!str)
			return false;

		uint32 i;
		for (i = 0; i < n && c_str()[i] && str[i]; ++i)
		{
			if (c_str()[i] != str[i])
				return false;
		}

		// if one (or both) of the strings was smaller then they
		// are only equal if they have the same length
		return (i == n) || (c_str()[i] == 0 && str[i] == 0);
	}

	void insert(int32 pos, const T* const other)
	{
		uint32 len = 0;
		const T* p = other;
		while (*p)
		{
			++len;
			++p;
		}

		__string temp;
		temp.append(array, pos);
		temp.append(other, len);
		temp.append(array + pos, used - pos);

		uint32 tsize = (uint32)temp.size();
		clear(tsize);

		T* str = const_cast<T*>(c_str());
		for (uint32 l = 0; l < tsize; ++l)
			str[l] = temp[l];

		str[tsize] = 0;
		used = tsize;
	}

	__string<T, TAlloc>& append(T character)
	{
		const int32 newsize = used + 1;
		if (newsize >= _NUM_SMALL)
		{
			if (newsize > allocated)
				reallocate(newsize);
		}
		++used;

		T* str = const_cast<T*>(c_str());
		str[used - 2] = character;
		str[used - 1] = 0;

		return *this;
	}

	__string<T, TAlloc>& append(const __string<T, TAlloc>& other)
	{
		--used;

		uint32 len = (uint32)other.size() + 1;

		const int32 newsize = used + len;
		if (newsize >= _NUM_SMALL)
		{
			if (newsize > allocated)
				reallocate(newsize);
		}
		T* str = const_cast<T*>(c_str());
		for (uint32 l = 0; l < len; ++l)
			str[used + l] = other[l];

		used += len;

		return *this;
	}

	//! Appends a char string to this string
	/** \param other: Char string to append. */
	/** \param length: The length of the string to append. */
	__string<T, TAlloc>& append(const T* const other, uint32 length = 0xffffffff)
	{
		if (!other)
			return *this;

		uint32 len = 0;
		const T* p = other;
		while (*p)
		{
			++len;
			++p;
		}
		if (len > length)
			len = length;

		const int32 newsize = used + len;
		if (newsize >= _NUM_SMALL)
		{
			if (newsize > allocated)
				reallocate(newsize);
		}

		--used;
		++len;

		T* str = const_cast<T*>(c_str());
		for (uint32 l = 0; l < len; ++l)
			str[l + used] = *(other + l);

		used += len;

		return *this;
	}

	__string<T, TAlloc>& append(const __string<T, TAlloc>& other, uint32 length)
	{
		if ((uint32)other.size() < length)
		{
			append(other);
			return *this;
		}

		const int32 newsize = used + length;
		if (newsize >= _NUM_SMALL)
		{
			if (newsize > allocated)
				reallocate(newsize);
		}

		--used;

		T* str = const_cast<T*>(c_str());
		for (uint32 l = 0; l < length; ++l)
			str[l + used] = other[l];

		used += length;

		str[used] = 0;
		++used;

		return *this;
	}

	void reserve(int32 count)
	{
		if (count < allocated)
			return;

		reallocate(count);
	}

	int32 findFirst(T c) const
	{
		for (int32 i = 0; i < used; ++i)
		{
			if (c_str()[i] == c)
				return i;
		}

		return -1;
	}

	int32 findFirstChar(const T* const c, uint32 count) const
	{
		if (!c)
			return -1;

		for (int32 i = 0; i < used; ++i)
		{
			for (uint32 j = 0; j < count; ++j)
			{
				if (c_str()[i] == c[j])
					return i;
			}
		}

		return -1;
	}

	template <class B>
	int32 findFirstCharNotInList(const B* const c, uint32 count) const
	{
		for (int32 i = 0; i < used - 1; ++i)
		{
			uint32 j;
			for (j = 0; j < count; ++j)
			{
				if (c_str()[i] == c[j])
					break;
			}
			if (j == count)
				return i;
		}

		return -1;
	}

	template <class B>
	int32 findLastCharNotInList(const B* const c, uint32 count) const
	{
		for (int32 i = (int32)(used - 2); i >= 0; --i)
		{
			uint32 j;
			for (j = 0; j < count; ++j)
			{
				if (c_str()[i] == c[j])
					break;
			}
			if (j == count)
				return i;
		}

		return -1;
	}

	int32 findNext(T c, uint32 startPos) const
	{
		for (uint32 i = startPos; i < used; ++i)
		{
			if (c_str()[i] == c)
				return i;
		}

		return -1;
	}

	int32 findLast(T c, int32 start = -1) const
	{
		start = Math::Clamp(start < 0 ? (int32)(used)-1 : start, 0, (int32)(used)-1);
		for (int32 i = start; i >= 0; --i)
		{
			if (c_str()[i] == c)
				return i;
		}

		return -1;
	}

	int32 findLastChar(const T* const c, uint32 count) const
	{
		if (!c)
			return -1;

		for (int32 i = used - 1; i >= 0; --i)
		{
			for (uint32 j = 0; j < count; ++j)
			{
				if (c_str()[i] == c[j])
					return i;
			}
		}

		return -1;
	}

	template <class B>
	int32 find(const B* const str, const uint32 start = 0) const
	{
		if (str && *str)
		{
			uint32 len = 0;

			while (str[len])
				++len;

			if (len > size())
				return -1;

			for (uint32 i = start; i < used - len; ++i)
			{
				uint32 j = 0;

				while (str[j] && c_str()[i + j] == str[j])
					++j;

				if (!str[j])
					return i;
			}
		}

		return -1;
	}

	__string substr(uint32 pos)
	{
		uint32 size = this->size();
		if (pos > size)
			return __string<T>();
		return __string(c_str() + pos);
	}

	//! Returns a substring
	/** \param begin Start of substring.
	\param length Length of substring.
	\param to_lower copy only lower case */
	__string<T> substr(uint32 begin, int32 length, bool to_lower = false) const
	{
		// if start after string
		// or no proper substring length
		if ((length <= 0) || (begin >= size()))
			return __string<T>();

		// clamp length to maximal value
		if ((length + begin) > size())
			length = size() - begin;

		__string<T> temp;
		temp.reserve(length + 1);

		if (to_lower)
		{
			T* str = const_cast<T*>(c_str());
			for (int32 i = 0; i < length; ++i)
				temp[i] = locale_lower(str[i + begin]);
		}
		else
		{
			T* str = const_cast<T*>(c_str());
			for (int32 i = 0; i < length; ++i)
				temp[i] = str[i + begin];
		}

		temp[length] = 0;
		temp.used = length+1;
		return temp;
	}

	//! Trims the string.
	/** Removes the specified characters (by default, Latin-1 whitespace)
	from the begining and the end of the string. */
	__string<T, TAlloc>& trim(const __string<T, TAlloc> & whitespace /*= " \t\n\r"*/)
	{
		// find start and end of the substring without the specified characters
		const int32 begin = findFirstCharNotInList(whitespace.c_str(), whitespace.used);
		if (begin == -1)
			return (*this = "");

		const int32 end = findLastCharNotInList(whitespace.c_str(), whitespace.used);

		return (*this = substr(begin, (end + 1) - begin));
	}

	__string<T, TAlloc>& operator += (T c)
	{
		append(c);
		return *this;
	}

	__string<T, TAlloc>& operator += (const T* const c)
	{
		append(c);
		return *this;
	}

	__string<T, TAlloc>& operator += (const __string<T, TAlloc>& other)
	{
		append(other);
		return *this;
	}

	__string<T, TAlloc>& replace(T toReplace, T replaceWith)
	{
		T* str = const_cast<T*>(c_str());
		for (int32 i = 0; i < used; ++i)
		{
			if (str[i] == toReplace)
				str[i] = replaceWith;
		}

		return *this;
	}

	// repalce : change str from n1 to (n1+n2)... but str size can be bigger than n2.
	template <class B>
	__string<T, TAlloc>& replace(int32 n1, int32 n2, const B* const str)
	{
		int32 osize = size();
		if (osize < n1 || osize < n1 + n2) 
			return *this; // string is too small.

		int32 newstrlen = 0;
		const T* p = str;
		while (*p)
		{
			++newstrlen;
			++p;
		}

		// 1. allocate!!!
		int32 arraypos = 0;
		int32 totalsize = osize - n2 + newstrlen + 1;

		T * target = NULL;
		T * new_array = NULL;

		int32 new_allocated = 0;
		if (totalsize >= _NUM_SMALL)
		{
			new_array = allocator.AllocPODs<T>(totalsize);
			new_allocated = totalsize;
			target = new_array;
		}
		else
		{
			new_allocated = 0;
			target = &local[0];
		}

		// copy first
		const T* src = const_cast<T*>(c_str());
		if (src)
		{
			for (int32 first = 0; first < n1; first++, arraypos++)
			{
				target[arraypos] = src[first];
			}
		}

		// second
		for (int32 second = 0; second < newstrlen; second++, arraypos++)
		{
			target[arraypos] = (T)str[second];
		}

		// third
		if (src)
		{
			int32 final = osize - (n1 + n2);
			for (int32 third = 0; third < final; third++, arraypos++)
			{
				target[arraypos] = src[(n1 + n2) + third];
			}
		}

		target[arraypos] = 0;
		used = totalsize;

		// deallocated!!
		T * oldarray = array;
		{
			array = new_array;
			allocated = new_allocated;
		}
		if (oldarray)
			allocator.Deallocate(oldarray);

		return *this;
	}

	__string<T, TAlloc>& remove(T c)
	{
		T* str = const_cast<T*>(c_str());
		CHECK(str, TEXT("remove"));

		uint32 pos = 0;
		uint32 found = 0;
		for (uint32 i = 0; i < used; ++i)
		{
			if (str[i] == c)
			{
				++found;
				continue;
			}

			str[pos++] = str[i];
		}
		used -= found;
		str[used] = 0;

		return *this;
	}

	__string<T, TAlloc>& remove(const __string<T, TAlloc>& toRemove)
	{
		uint32 size = (uint32)toRemove.size();
		if (size == 0)
			return *this;

		T* str = const_cast<T*>(c_str());
		CHECK(str, TEXT("remove"));

		uint32 pos = 0;
		uint32 found = 0;
		for (uint32 i = 0; i < used; ++i)
		{
			uint32 j = 0;
			while (j < size)
			{
				if (str[i + j] != toRemove[j])
					break;
				++j;
			}
			if (j == size)
			{
				found += size;
				i += size - 1;
				continue;
			}

			str[pos++] = str[i];
		}
		used -= found;
		str[used] = 0;

		return *this;
	}

	__string<T, TAlloc>& removeChars(const __string<T, TAlloc> & characters)
	{
		T* str = const_cast<T*>(c_str());
		CHECK(str, TEXT("removeChars"));

		uint32 pos = 0;
		uint32 found = 0;
		for (uint32 i = 0; i < used; ++i)
		{
			bool docontinue = false;
			uint32 size = (uint32)characters.size();
			for (uint32 j = 0; j < size; ++j)
			{
				if (characters[j] == str[i])
				{
					++found;
					docontinue = true;
					break;
				}
			}
			if (docontinue)
				continue;

			str[pos++] = str[i];
		}
		used -= found;
		str[used] = 0;

		return *this;
	}

	__string<T, TAlloc>& erase(uint32 index)
	{
		T* str = const_cast<T*>(c_str());
		CHECK(str, TEXT("erase"));

		for (uint32 i = index + 1; i < used; ++i)
			str[i - 1] = str[i];
		--used;

		return *this;
	}

	__string<T, TAlloc>& erase(uint32 index, int32 n)
	{
		T* str = const_cast<T*>(c_str());
		CHECK(str, TEXT("erase"));

		int32 p = index;
		for (uint32 i = index + n; i < used; ++i)
			str[p++] = str[i];
		used -= n;

		return *this;
	}

	__string<T, TAlloc>& validate()
	{
		T* str = const_cast<T*>(c_str());
		CHECK(str, TEXT("validate"));

		uint32 len = length();
		for (uint32 i = 0; i < len; ++i)
		{
			if (str[i] == 0)
			{
				used = i + 1;
				return;
			}
		}

		if (len > 0)
		{
			used = len - 1;
			str[used] = 0;
		}
		else
		{
			used = 0;
		}

		return *this;
	}

	bool isValidIndex(uint32 index) const
	{
		return index >= 0 && index < length();
	}

	T lastChar() const
	{
		return used > 1 ? c_str()[used - 2] : 0;
	}

	template<class container>
	uint32 split(container& ret, const T* const c, uint32 count = 1, bool ignoreEmptyTokens = true, bool keepSeparators = false) const
	{
		if (!c)
			return 0;

		const uint32 oldSize = (uint32)ret.size();
		uint32 lastpos = 0;
		bool lastWasSeparator = false;
		for (uint32 i = 0; i < used; ++i)
		{
			bool foundSeparator = false;
			for (uint32 j = 0; j < count; ++j)
			{
				if (c_str()[i] == c[j])
				{
					if ((!ignoreEmptyTokens || i - lastpos != 0) &&
						!lastWasSeparator)
					{
						ret.push_back(__string<T, TAlloc>(&c_str()[lastpos], i - lastpos));
					}
					foundSeparator = true;
					lastpos = (keepSeparators ? i : i + 1);
					break;
				}
			}
			lastWasSeparator = foundSeparator;
		}
		if ((used - 1) > lastpos)
			ret.push_back(__string<T, TAlloc>(&c_str()[lastpos], (used - 1) - lastpos));
		return (uint32)ret.size() - oldSize;
	}

private:
	void reallocate(int32 new_size)
	{
		if (new_size >= _NUM_SMALL)
		{
			T* old_array = array;

			array = allocator.AllocPODs<T>(new_size); //new T[new_size];
			allocated = new_size;

			if (allocated < used)
				used = allocated;

			if (old_array)
			{
				int32 amount = used < new_size ? used : new_size;
				for (int32 i = 0; i < amount; ++i)
					array[i] = old_array[i];

				allocator.Deallocate(old_array); // delete [] old_array;
			}
			else
			{
				int32 amount = used < new_size ? used : new_size;
				for (int32 i = 0; i < amount; ++i)
					array[i] = local[i];

				local[0] = 0x00;
				array[amount] = 0x00;
			}
		}
		else
		{
			if (array)
			{
				int32 amount = used < new_size ? used : new_size;
				for (int32 i = 0; i < amount; ++i)
					local[i] = array[i];

				allocator.Deallocate(array); // delete [] old_array;
				array = NULL;
			}

			allocated = 0;
		}
	}

protected:
	int32 used;

	// SmallBuffer
	enum { _NUM_SMALL = 8 };
	T local[_NUM_SMALL];
	// LargeBuffer
	T* array;
	int32 allocated;
	TAlloc allocator;
};

#define MAX_STRING 1024

struct Vec2;
struct Vec3;
struct Vec4;

namespace utf8
{
	typedef __string<char> string;

	//--------------------------------------------------------------------------------------------------------
	//	utf8
	//--------------------------------------------------------------------------------------------------------
	inline int32 strcmp(const char * str1, const char* str2) { return ::strcmp(str1, str2); }
	inline int32 stricmp(const char* str1, const char* str2) { return ::_stricmp(str1, str2); }
	inline int32 strlen(const char* str) { return (int32)::strlen(str); }
	inline char* strchr(const char* str, char ch) { return (char*)::strchr(str, ch); }
	inline char* strrchr(const char* str, char ch) { return (char*)::strrchr(str, ch); }
	inline char* strstr(const char* str1, const char* str2) { return (char*)::strstr(str1, str2); }
	inline void strcpy(char* dest, int32 destsize, const char* src) { ::strcpy_s(dest, destsize, src); }
	inline char* strncpy(char* dest, int32 destsize, char* source, int32 count) { ::strncpy_s(dest, destsize, source, (size_t)count); return dest; }
	inline char* strcat(char* dst, int32 destsize, const char* src) { ::strcat_s(dst, destsize, src); return dst; }
	inline char* strlwr(char* str, int32 sizeinwords) { ::_strlwr_s(str, sizeinwords); return str; }
	inline char* sprintf(char* dest, int32 destlen, const char* format, ...) { va_list ap; va_start(ap, format); ::_vsnprintf_s(dest, destlen, MAX_STRING, format, ap); va_end(ap); return dest; }
	inline char* vsprintf(char* dest, int32 destlen, const char* format, va_list ap) { ::_vsnprintf_s(dest, destlen, MAX_STRING, format, ap); return dest; }

	inline void strtime(char* str, int32 sizeinwords) { ::_strtime_s(str, sizeinwords); }
	inline void strdate(char* str, int32 sizeinwords) { ::_strdate_s(str, sizeinwords); }

	int32 sscanf(const char* buffer, const char* format, ...);
	int32 vsscanf(const char* buffer, const char* format, va_list ap);
	void strcpypart(char* dest, char* src, size_t size);
	string lowercase(const string& str);

	inline int32 atoi(const char* str) { return ::atoi(str); }
	inline float atof(const char* str) { return (float)::atof(str); }
	inline long atol(const char* str) { return ::atol(str); }
	bool atob(const char* str);
	void atov(const char* str, const Vec2* vec);
	void atov(const char* str, const Vec3* vec);
	void atov(const char* str, const Vec4* vec);
	void atoi2(const char* str, int32 * int2);
	void atoi3(const char* str, int32 * int3);
	int32 gethex(const char* buf, int32 len);
	void xtoa(uint32 val, char* buf, unsigned radix, int32 negative);
	inline char* itoa(int32 val, char* buf, int32 radix = 10) { CHECK(0, TEXT("Deprecated!!! please use _itoa_s")); return NULL; /*return ::_itoa(val, buf, radix);*/ }
	inline char* itoa_s(int32 val, char* buf, int32 bufsize, int32 radix = 10) { ::_itoa_s(val, buf, bufsize, radix); return buf; }
	char* int_str(int32 value, int32 width, int32 trim);
	char* ftoa(float val, char* buf, int32 dp = 5);

	string format(const char* fmt, ...);
	string vformat(const char* fmt, va_list ap);

	string splitFilePath(const char* path);
	string splitFileName(const char* path);
	string splitFileExt(const char* path);
	string splitFileNameExt(const char* path);
	void splitPath(char* path, char* drive, char* dir, char* fname, char* ext);

	SizeT tokenize(Array<string>& output, const string& input, const string& whitespace);
}

namespace unicode
{
	typedef __string<wchar> string;

	//--------------------------------------------------------------------------------------------------------
	//	widechar
	//--------------------------------------------------------------------------------------------------------
	int32 strlen(const wchar* str);
	wchar* strchr(const wchar* str, wchar ch);
	wchar* strrchr(const wchar* str, wchar ch);
	wchar* strstr(const wchar* str1, const wchar* str2);
	int32 strcmp(const wchar* str1, const wchar* str2);
	int32 stricmp(const wchar* str1, const wchar* str2);
	void strcpy(wchar* dest, const wchar* src);
	void strcpy_s(wchar* dest, int32 destsize, const wchar* src);
	wchar* strncpy(wchar* dest, wchar* source, int32 count);
	wchar* strncpy_s(wchar* dest, int32 destsize, wchar* source, int32 count);
	wchar* strcat(wchar* dst, const wchar* src);
	wchar* strcat_s(wchar* dst, int32 destsize, const wchar*src);
	wchar* strlwr(wchar* str, int32 sizeinwords);

	inline void strtime(wchar* str, int32 sizeinwords) { ::_wstrtime_s(str, sizeinwords); }
	inline void strdate(wchar* str, int32 sizeinwords) { ::_wstrdate_s(str, sizeinwords); }

	void strcpypart(wchar* dest, wchar* src, size_t size);
	string lowercase(const string& str);

	//	number
	int32 atoi(const wchar* str);
	float atof(const wchar* str);
	long atol(const wchar* str);
	bool atob(const wchar* str);
	void atov(const wchar* str, const Vec2* vec);
	void atov(const wchar* str, const Vec3* vec);
	void atov(const wchar* str, const Vec4* vec);
	void atoi2(const wchar* str, int32 * int2);
	void atoi3(const wchar* str, int32 * int3);
	wchar* itoa(int32 val, wchar* buf, int32 radix = 10);
	wchar* itoa_s(int32 val, wchar* buf, int32 bufsize, int32 radix = 10);
	int32 gethex(const wchar*buf, int32 len);
	void xtoa(uint32 val, wchar* buf, unsigned radix, int32 negative);
	wchar* ftoa(float val, wchar*buf, int32 dp = 5);
	wchar* int_str(int32 value, int32 width, int32 trim);

	//	format
	// %d, %i, %l , %f, %s only!!!
	int32 sscanf(const wchar* buffer, const wchar* format, ...);
	int32 vsscanf(const wchar* buffer, const wchar* format, va_list ap);
	wchar* sprintf(wchar* dest, int32 destlen, const wchar* format, ...);
	wchar* vsprintf(wchar* dest, int32 destlen, const wchar* format, va_list ap);;

	string format(const wchar* fmt, ...);
	string vformat(const wchar* fmt, va_list ap);

	//	FilePath
	string splitFilePath(const wchar* path);
	string splitFileName(const wchar* path);
	string splitFileExt(const wchar* path);
	string splitFileNameExt(const wchar* path);
	void splitPath(wchar* path, wchar* drive, wchar* dir, wchar* fname, wchar* ext);

	SizeT tokenize(Array<string>& output, const string& input, const string& whitespace);

	//--------------------------------------------------------------------------------------------------------
	//	UTF Convert
	//--------------------------------------------------------------------------------------------------------
	// utf-8 --> unicode (utf-16/utf-32)
	int32 convert(wchar* dest, int32 destsize, const char *src, bool utf8 = false);
	string convert(const char * str);
	string convert(const utf8::string& str);

	// unicode (utf-16/utf-32) --> utf-8
	int32 convert(char * dest, int32 destsize, const wchar*src, bool utf8 = false);
	utf8::string convert(const wchar * str);
	utf8::string convert(const string& str);

	//********************************************************************************************************************************

#define MAX_ARGS	10	

#define ARGS_1  p[0]
#define ARGS_2  p[0], p[1]
#define ARGS_3  p[0], p[1], p[2]
#define ARGS_4  p[0], p[1], p[2], p[3]
#define ARGS_5  p[0], p[1], p[2], p[3], p[4]
#define ARGS_6  p[0], p[1], p[2], p[3], p[4], p[5]
#define ARGS_7  p[0], p[1], p[2], p[3], p[4], p[5], p[6]
#define ARGS_8  p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]
#define ARGS_9  p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]
#define ARGS_10 p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9]

#ifdef TARGET_OS_ANDROID
	int32 	loadfile(const wchar* fname, char **ptr);
#endif
};


#endif	// __UNICODE_H__
