// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
//	Linear Allocator
//	- 초기에 할당받은 메모리에서 순차적으로 할당해줄 수 있는 할당자
//------------------------------------------------------------------
class LinearAllocator : public Memory::Allocator
{
public:
	LinearAllocator(void* ptr, unsigned int size);

	void* Allocate(uint32 size, uint32 align = Memory::DEFAULT_ALIGN) override;
	void Deallocate(void* ptr) override;
	int32 Size() const override;
	void Reset() override;

private:
	LinearAllocator(const LinearAllocator&);
	LinearAllocator& operator=(const LinearAllocator&);

	void* Offset() const { return m_Offset; }

	char* m_Ptr;
	char* m_End;
	char* m_Offset;

	friend class ScopeStackAllocator;
};


//------------------------------------------------------------------
//	Stack Allocator
//------------------------------------------------------------------
class StackAllocator : public Memory::Allocator
{
public:
	StackAllocator(void* ptr, unsigned int size);

	void* Allocate(uint32 size, uint32 align = Memory::DEFAULT_ALIGN) override;
	void Deallocate(void* ptr) override;
	int32 Size() const override;
	void Reset() override;

	//void* GetOffset() const { return m_Offset; }

private:
	StackAllocator(const StackAllocator&);
	StackAllocator& operator=(const StackAllocator&);

	char* m_Ptr;
	char* m_End;
	char* m_Offset;
	int m_Count;
};


//------------------------------------------------------------------
//	Pool Allocator
//------------------------------------------------------------------
class PoolAllocator : public Memory::Allocator
{
public:
	PoolAllocator(void* ptr, uint32 capacity, uint32 elementsize, int32 alignment = Memory::DEFAULT_ALIGN);

	virtual void* Allocate(uint32 size, uint32 align = Memory::DEFAULT_ALIGN) override;
	virtual void  Deallocate(void* ptr) override;
	virtual int32 Size() const override;
	virtual void  Reset() override;

private:
	PoolAllocator(const PoolAllocator&);
	PoolAllocator& operator=(const PoolAllocator&);

	void Initialize(uint32 size, uint32 align);

	char* m_Ptr;
	char* m_Next;
	int32 m_Size;

	uint32 m_bInitialized;
};


//------------------------------------------------------------------
//	Scope Stack Allocator
//------------------------------------------------------------------
class ScopeStackAllocator
{
public:
	ScopeStackAllocator(LinearAllocator& allocator, int32 alignment = Memory::DEFAULT_ALIGN);
	~ScopeStackAllocator();

	// 소멸자를 가진 Object를 할당
	template <typename T>
	T* AllocObject()
	{
		int32 size = Memory::align(sizeof(Memory::Finalizer), m_Alignment);
		Memory::Finalizer* finalizer = (Memory::Finalizer*)m_Allocator->Allocate(size);

		finalizer->fn = &Memory::Destructor<T>;
		finalizer->next = m_FinalizerChain;
		m_FinalizerChain = finalizer;

		return new (m_Allocator->Allocate(Memory::align(sizeof(T), m_Alignment))) T;
	}

	// 소멸자를 가지지 않은 Object를 할당
	template <typename T>
	T* AllocPOD()
	{
		return new (m_Allocator->Allocate(Memory::align(sizeof(T), m_Alignment))) T;
	}

	// Raw 메모리 할당
	void* Allocate(uint32 size)
	{
		return m_Allocator->Allocate(size, m_Alignment);
	}

private:
	LinearAllocator* m_Allocator;
	int m_Alignment;

	Memory::Finalizer* m_FinalizerChain;
	void* m_Offset;
};

//------------------------------------------------------------------
//	Inline Allocator
//------------------------------------------------------------------
template <typename TYPE, uint32 COUNT, typename ALLOCATOR = Memory::Allocator>
class InlineAllocator : public Allocator
{
public:
	InlineAllocator() {}
	~InlineAllocator() {}

	void* Allocate(uint32 size, uint32 align = Memory::DEFAULT_ALIGN) override
	{
		int32 count = size / sizeof(TYPE);

		if (count <= COUNT)
		{
			return m_InlineData;
		}

		return NULL;
	}
	void Deallocate(void* ptr) override
	{
		//
	}
	int32 Size() const override
	{
		return COUNT * sizeof(TYPE);
	}
	void Reset() override
	{
		// Array<int32> TEST;
		// InlineArray<int32> TEST;
		// FixedArray<int32> TEST;
	}

private:
	InlineAllocator(const InlineAllocator&);
	InlineAllocator& operator=(const InlineAllocator&);

	TYPE m_InlineData[COUNT];
	ALLOCATOR m_SecondAllocator;
};
