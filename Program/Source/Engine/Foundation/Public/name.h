// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class NameTable;

/** Name index. */
typedef int32 NAME_INDEX;

//------------------------------------------------------------------
/** @class	Name
	@author cagetu
	@desc	Hashed String Id. 
			스트링을 Id 형태로 변환하여, 생성에 비용이 들지만 빠른 비교등에 사용한다.
			zho: https://github.com/zho7611/dg.git 를 인용
			article: http://cowboyprogramming.com/2007/01/04/practical-hash-ids/
*/
//------------------------------------------------------------------
class Name
{
public:
	Name();
	Name(const TCHAR* str);
	Name(const String& str);
	Name(const Name& str);
	~Name() = default;

	void Set(const TCHAR* str);
	void Set(const String& str);
	void Set(const Name& str);

	void Append(const TCHAR* str);
	void Append(const String& str);

	const TCHAR* Get() const;

	bool IsEmpty() const;
	bool IsEqual(const Name& other) const;
	bool IsEqual(const TCHAR* str) const;

	Name& operator=(const Name& other);
	bool operator==(const Name& other) const;

private:
	NAME_INDEX m_Index;

public:
	static void _Startup();
	static void _Shutdown();

private:
	static NameTable* s_NameTable;
};

//------------------------------------------------------------------
inline
bool Name::IsEmpty() const
{
	return m_Index == INVALID_INDEX ? true : false;
}

//------------------------------------------------------------------
inline
bool Name::IsEqual(const Name& other) const
{
	return m_Index == other.m_Index;
}
