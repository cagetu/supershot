
//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr()
{
	object = nullptr;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr(T* o, bool bAddRef)
{
	object = o;

	if (object && bAddRef)
		object->AddRef();
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr(const Ptr& copy)
{
	object = copy.object;

	if (object)
		object->AddRef();
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::Ptr(Ptr&& move) noexcept
{
	object = move.object;
	move.object = nullptr;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::~Ptr()
{
	if (object)
		object->Release();
}

//--------------------------------------------------------------
template <class T>
void Ptr<T>::Empty()
{
	if (object)
	{
		object->Release();
		object = nullptr;
	}
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::IsValid() const
{
	return (nullptr != object);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::IsValidRef(const Ptr& o) const
{
	return (nullptr != o.object);
}

//--------------------------------------------------------------
template <class T>
T* Ptr<T>::Get() const
{
	CHECK(object != nullptr, TEXT("object == nullptr"));
	return object;
}

//--------------------------------------------------------------
template <class T>
T** Ptr<T>::GetInitRef()
{
	*this = NULL;
	return &object;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::operator T() const
{
	return object;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::operator T*() const
{
	CHECK(object != nullptr, TEXT("object == nullptr"));
	return object;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>::operator const T*() const
{
	CHECK(object != nullptr, TEXT("object == nullptr"));
	return object;
}

//--------------------------------------------------------------
template <class T>
T& Ptr<T>::operator* () const
{
	CHECK(object != nullptr, TEXT("object == nullptr"));
	return *object;
}

//--------------------------------------------------------------
template <class T>
T* Ptr<T>::operator-> () const
{
	CHECK(object != nullptr, TEXT("object == nullptr"));
	return object;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>& Ptr<T>::operator= (T* o)
{
	//if (object != o)
	{
		// Call AddRef before Release, in case the new reference is the same as the old reference.
		T* oldRef = object;
#if 1
		object = o;
		if (object)
		{
			object->AddRef();
		}
		if (oldRef)
		{
			oldRef->Release();
		}
#else
		if (object)
			object->Release();

		if (o)
			o->AddRef();

		object = o;
#endif
	}
	return *this;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>& Ptr<T>::operator= (const Ptr& o)
{
	return *this = o.object;
}

//--------------------------------------------------------------
template <class T>
Ptr<T>& Ptr<T>::operator= (Ptr&& copy) noexcept
{
	if (object != o.object)
	{
		T* oldRef = object;
		object = copy.object;
		copy.object = nullptr;
		if (oldRef)
		{
			oldRef->Release();
		}
	}
	return *this;
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator== (T* o) const
{
	return (object == o);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator!= (T* o) const
{
	return (object != o);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator== (const Ptr& o) const
{
	return (object == o.object);
}

//--------------------------------------------------------------
template <class T>
bool Ptr<T>::operator!= (const Ptr& o) const
{
	return (object != o.object);
}

//--------------------------------------------------------------
/** @brief	다른 smart pointer로 cast (UnSafe!!!!)
*/
//--------------------------------------------------------------
template <class T>
template <class OtherT>
Ptr<OtherT> Ptr<T>::Cast() const
{
	//return *(Ptr<OtherT>*)this;
	return (OtherT*)this->Object();
}

//--------------------------------------------------------------
/** @brief	다른 smart pointer로 cast (UnSafe!!!!)
*/
//--------------------------------------------------------------
template <class T>
template <class OtherT>
Ptr<OtherT> Ptr<T>::DynamicCast() const
{
	return DynamicCast<OtherT>(this->Object());
}

/***************************************************************/
//	WeakPointer
/***************************************************************/
template <class T>
WeakPtr<T>::WeakPtr()
	: m_Ptr(nullptr)
{
}

template <class T>
WeakPtr<T>::WeakPtr(const Ptr<T>& ptr)
	: m_Ptr(nullptr)
{
	m_Ptr.object = ptr.Get();
}

template <class T>
WeakPtr<T>::WeakPtr(const WeakPtr<T>& ptr)
	: m_Ptr(nullptr)
{
	m_Ptr.object = ptr.Get();
}

template <class T>
WeakPtr<T>::~WeakPtr()
{
	m_Ptr.object = nullptr;
}

template <class T>
void WeakPtr<T>::operator=(const Ptr<T>& rhs)
{
	m_Ptr.object = rhs.Get();
}

template <class T>
void WeakPtr<T>::operator=(const WeakPtr<T>& rhs)
{
	m_Ptr.object = rhs.Get();
}

template <class T>
void WeakPtr<T>::operator=(T* rhs)
{
	m_Ptr.object = rhs;
}

template <class T>
T* WeakPtr<T>::operator->() const
{
	CHECK(m_Ptr, "opeartor->");
	return m_Ptr;
}

template <class T>
T& WeakPtr<T>::operator*() const
{
	CHECK(m_Ptr, "opeartor*");
	return *m_Ptr;
}

template <class T>
WeakPtr<T>::operator T*() const
{
	CHECK(m_Ptr, "opeartor T*");
	return m_Ptr;
}

template <class T>
bool WeakPtr<T>::IsValid() const
{
	return m_Ptr.IsValid();
}

template <class T>
T* WeakPtr<T>::Get() const
{
	return m_Ptr.Get();
}

/***************************************************************/
//	Raw Pointer
/***************************************************************/
template <class T>
RawPtr<T>::RawPtr(T* ptr)
	: m_Ptr(ptr)
{
}

template <class T>
T* RawPtr<T>::ptr() const
{
	return m_Ptr;
}

template <class T>
T*& RawPtr<T>::ref()
{
	return m_Ptr;
}

template <class T>
T& RawPtr<T>::operator*() const
{
	CHECK(m_Ptr, "operatr*");
	return *m_Ptr;
}

template <class T>
T* RawPtr<T>::operator->() const
{
	CHECK(m_Ptr, "operatr*");
	return m_Ptr;
}
