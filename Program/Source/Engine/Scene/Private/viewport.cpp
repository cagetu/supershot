#include "foundation.h"
#include "viewport.h"
#include "camera.h"

Viewport::Viewport()
	: m_Left(0.0f)
	, m_Top(0.0f)
	, m_Width(1.0f)
	, m_Height(1.0f)
	, m_MinDepth(0.0f)
	, m_MaxDepth(1.0f)
{
}
Viewport::~Viewport()
{
}

void Viewport::SetViewport(float x, float y, float width, float height, float minDepth, float maxDepth)
{
	m_Left = x;
	m_Top = y;
	m_Width = width;
	m_Height = height;
	m_MinDepth = minDepth;
	m_MaxDepth = maxDepth;
}
