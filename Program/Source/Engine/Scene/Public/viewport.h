// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class Camera;

//------------------------------------------------------------------
/**	@class	Viewport
	@desc
*/
//------------------------------------------------------------------
class Viewport
{
public:
	Viewport();
	~Viewport();

	void SetViewport(float x, float y, float width, float height, float minDepth = 0.0f, float maxDepth = 1.0f);

	float Top() const { return m_Top; }
	float Left() const { return m_Left; }
	float Width() const { return m_Width; }
	float Height() const { return m_Height; }
	float MinDepth() const { return m_MinDepth; }
	float MaxDepth() const { return m_MaxDepth; }

private:
	float m_Top;
	float m_Left;
	float m_Width;
	float m_Height;
	float m_MinDepth;
	float m_MaxDepth;

	Camera* m_Camera;
};