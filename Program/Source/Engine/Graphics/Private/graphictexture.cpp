#include "foundation.h"
#include "graphictexture.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, IGraphicTexture, IGraphicResource);

IGraphicTexture::IGraphicTexture()
	: m_Width(0)
	, m_Height(0)
	, m_MipLevels(1)
	, m_Format(FMT_NONE)
{
}
IGraphicTexture::~IGraphicTexture()
{
}