#include "foundation.h"
#include "Vulkan/vulkanheader.h"
#include "Vulkan/vulkandriver.h"
#include "Vulkan/vulkanrenderwindow.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkancommandlist.h"
#include "Vulkan/vulkanshadercompiler.h"
#include "Vulkan/vulkanadapter.h"
#include "color.h"

VulkanDevice* GVulkanDevice = nullptr;

__ImplementRtti(Gfx, VulkanDriver, IGraphicDriver);

VulkanDriver::VulkanDriver()
	: m_InstanceHandle(VK_NULL_HANDLE)
	, m_SurfaceHandle(VK_NULL_HANDLE)
	, m_Device(nullptr)
	, m_ShaderCompiler(nullptr)
{
}
VulkanDriver::~VulkanDriver()
{
	Destroy();
}

bool VulkanDriver::Create(const WindowHandle& windowHandle)
{
	if (!CreateInstance())
		return false;

	if (!CreateSurface(windowHandle))
		return false;

	if (!CreateDevice())
		return false;

	WindowHandle hWnd = windowHandle;
	VkSurfaceCapabilitiesKHR& surfaceCaps = m_Device->PhysicalDevice()->SurfaceCapabilities;

	// 윈도우 크기는 surface.minImageExtent 와 surfaceCaps.maxImageExtent 범위에 있어야 한다.
	int32 width = Math::Clamp(hWnd.Width(), surfaceCaps.minImageExtent.width, surfaceCaps.maxImageExtent.width);
	int32 height = Math::Clamp(hWnd.Height(), surfaceCaps.minImageExtent.height, surfaceCaps.maxImageExtent.height);

	hWnd.SetSize(width, height);

	if (!CreateRenderWindow(hWnd))
		return false;

#pragma message("RenderWindow 이미지와 CommandBuffer를 조립할 수 있어야 한다")

	m_ShaderCompiler = NewObject(VulkanShaderCompiler);

	return true;
}

void VulkanDriver::Destroy()
{
	DestroyRenderWindow();
	DestroyDevice();
	DestroySurface();
	DestroyInstance();

	DeleteObject(m_ShaderCompiler);
}

bool VulkanDriver::CreateInstance()
{
	Array<const char*> instanceLayers;
	Array<const char*> instanceExtensions;
	VulkanLayer::GetInstanceLayersAndExtensions(instanceExtensions, instanceLayers);

	// 어플리케이션 정보 설정
	VkApplicationInfo app;
	{
		Memory::MemZero(&app, sizeof(app));
		app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		app.pApplicationName = "Supershot";
		app.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
		app.engineVersion = VK_MAKE_VERSION(0, 0, 1);
		app.apiVersion = VULKAN_API_VERSION;
		app.pEngineName = "GrindHouse";
		app.pNext = NULL;
	}

	// 벌칸 인스턴스 생성 정보
	VkInstanceCreateInfo inst;
	{
		Memory::MemZero(&inst, sizeof(inst));
		inst.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		inst.pNext = nullptr;
		inst.pApplicationInfo = &app;
		inst.enabledExtensionCount = instanceExtensions.Num();
		inst.ppEnabledExtensionNames = inst.enabledExtensionCount > 0 ? &instanceExtensions[0] : nullptr;
		inst.flags = 0;

#if VULKAN_HAS_DEBUGGING_ENABLED
		inst.enabledLayerCount = instanceLayers.Num();
		inst.ppEnabledLayerNames = inst.enabledLayerCount > 0 ? &instanceLayers[0] : nullptr;
#endif
	}

	// 인스턴스 생성
	VkResult result = ::vkCreateInstance(&inst, nullptr, &m_InstanceHandle);
	if (result != VK_SUCCESS)
	{
		if (result == VK_ERROR_LAYER_NOT_PRESENT)
			ULOG(Log::_Debug, "vkCreateInstance is failed!! [VK_ERROR_LAYER_NOT_PRESENT] Specified layer is not found.");
		else if (result == VK_ERROR_EXTENSION_NOT_PRESENT)
			ULOG(Log::_Debug, "vkCreateInstance is failed!! [VK_ERROR_EXTENSION_NOT_PRESENT] Specified extensions is not found.");
		return false;
	}

	return true;
}

void VulkanDriver::DestroyInstance()
{
	if (m_InstanceHandle)
	{
		::vkDestroyInstance(m_InstanceHandle, nullptr);
		m_InstanceHandle = VK_NULL_HANDLE;
	}
}

bool VulkanDriver::CreateSurface(const WindowHandle& windowHandle)
{
	//
	// 1. Surface 생성
	//
#if TARGET_OS_WINDOWS
	VkWin32SurfaceCreateInfoKHR surfaceCreateInfo;
	Memory::MemZero(surfaceCreateInfo);
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.flags = 0;
	surfaceCreateInfo.pNext = nullptr;
	surfaceCreateInfo.hinstance = (HINSTANCE)windowHandle.Instance();
	surfaceCreateInfo.hwnd = (HWND)windowHandle.Handle();
	VK_RESULT_ASSERT(::vkCreateWin32SurfaceKHR(m_InstanceHandle, &surfaceCreateInfo, nullptr, &m_SurfaceHandle));
#elif TARGET_OS_ANDROID
	VkAndroidSurfaceCreateInfoKHR surfaceCreateInfo;
	Memory::Memzero(surfaceCreateInfo);
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.window = (ANativeWindow*)WindowHandle;

	VK_RESULT_ASSERT(vkCreateAndroidSurfaceKHR(Instance, &surfaceCreateInfo, nullptr, &m_SurfaceHandle));
#endif

	return true;
}

void VulkanDriver::DestroySurface()
{
	if (m_SurfaceHandle != VK_NULL_HANDLE)
	{
		vkDestroySurfaceKHR(m_InstanceHandle, m_SurfaceHandle, NULL);
		m_SurfaceHandle = VK_NULL_HANDLE;
	}
}

bool VulkanDriver::CreateDevice()
{
	//
	// 1. 장착된 모든 그래픽 카드 핸들 확보
	//
	uint32 gpuCount = 0;
	VK_RESULT(::vkEnumeratePhysicalDevices(m_InstanceHandle, &gpuCount, nullptr));
	if (gpuCount <= 0)
	{
		CHECK(gpuCount > 0, TEXT("gpuCount == 0"));
		return false;
	}

	Array<VkPhysicalDevice> physicalDevices(gpuCount);
	VK_RESULT(::vkEnumeratePhysicalDevices(m_InstanceHandle, &gpuCount, physicalDevices.Data()));

	//
	// 2. 그래픽 카드 수집
	//
	m_PhysicalDevices.Clear(gpuCount);

	Array<uint32> candidateGPU;
	for (SizeT index = 0; index < gpuCount; index++)
	{
		m_PhysicalDevices[index].Create(physicalDevices[index], m_SurfaceHandle);

		bool isSupportGPU = m_PhysicalDevices[index].IsSupportGPU(VK_QUEUE_GRAPHICS_BIT);
		if (isSupportGPU)
		{
			candidateGPU.AddUnique(index);
		}
	}

	//
	// 3. 그래픽 카드 선택 및 생성
	//
	for (uint32 index = 0; index < candidateGPU.Num(); index++)
	{
		VulkanPhysicalDevice& physicalDevice = m_PhysicalDevices[candidateGPU[index]];

		bool isDiscrete = physicalDevice.IsDiscreteGPU();
		bool isLastGPU = (index == gpuCount - 1) ? true : false;

		if (isDiscrete || isLastGPU)
		{
			// 논리적 Device 생성
			VulkanDevice* tempDevice = NewObject(VulkanDevice);
			if (tempDevice->Create(&physicalDevice))
			{
				m_Device = tempDevice;
				break;
			}
			else
			{
				DeleteObject(tempDevice);
			}
		}
	}
	
	GVulkanDevice = m_Device;
	return m_Device != nullptr;
}

void VulkanDriver::DestroyDevice()
{
	DeleteObject(m_Device);

	for (uint32 i = 0; i < m_PhysicalDevices.Num(); i++)
	{
		m_PhysicalDevices[i].Destroy();
	}

	GVulkanDevice = nullptr;
}

bool VulkanDriver::CreateRenderWindow(const WindowHandle& windowHandle)
{
	m_RenderWindow = NewObject(VulkanRenderWindow);
	if (!m_RenderWindow->Create(windowHandle, FMT_B8G8R8A8, true))
	{
		return false;
	}

	return true;
}

void VulkanDriver::DestroyRenderWindow()
{
	DeleteObject(m_RenderWindow);
}

void VulkanDriver::BeginFrame(Viewport* viewport)
{
	m_RenderWindow->BeginFrame(CLEAR_COLOR | CLEAR_DEPTH | CLEAR_STENCIL, LinearColor::WHITE, 1.0f, 1);
}

void VulkanDriver::EndFrame()
{
	m_RenderWindow->EndFrame();
}

void VulkanDriver::Present()
{
	m_RenderWindow->Present();
}

void VulkanDriver::Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil)
{
}

void VulkanDriver::SetViewport(const Viewport* viewport)
{
}

IGraphicShaderCompiler* VulkanDriver::GetShaderCompiler()
{
	return m_ShaderCompiler;
}

IGraphicShaderModule* VulkanDriver::CompileShader(const ShaderDescription& desc)
{
	return m_ShaderCompiler->Compile(desc);
}