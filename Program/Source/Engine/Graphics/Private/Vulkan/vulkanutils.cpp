#include "foundation.h"
#include "Vulkan/vulkan.h"
#include "Vulkan/vulkanutils.h"

namespace VULKAN
{
	const char* VkErrorString(VkResult result)
	{
		switch (result)
		{
#define STR(r) case VK_ ##r: return #r
			STR(NOT_READY);
			STR(TIMEOUT);
			STR(EVENT_SET);
			STR(EVENT_RESET);
			STR(INCOMPLETE);
			STR(ERROR_OUT_OF_HOST_MEMORY);
			STR(ERROR_OUT_OF_DEVICE_MEMORY);
			STR(ERROR_INITIALIZATION_FAILED);
			STR(ERROR_DEVICE_LOST);
			STR(ERROR_MEMORY_MAP_FAILED);
			STR(ERROR_LAYER_NOT_PRESENT);
			STR(ERROR_EXTENSION_NOT_PRESENT);
			STR(ERROR_FEATURE_NOT_PRESENT);
			STR(ERROR_INCOMPATIBLE_DRIVER);
			STR(ERROR_TOO_MANY_OBJECTS);
			STR(ERROR_FORMAT_NOT_SUPPORTED);
			STR(ERROR_SURFACE_LOST_KHR);
			STR(ERROR_NATIVE_WINDOW_IN_USE_KHR);
			STR(SUBOPTIMAL_KHR);
			STR(ERROR_OUT_OF_DATE_KHR);
			STR(ERROR_INCOMPATIBLE_DISPLAY_KHR);
			STR(ERROR_VALIDATION_FAILED_EXT);
			STR(ERROR_INVALID_SHADER_NV);
#undef STR
		default:
			return "UNKNOWN_ERROR";
		}
	}

	void CheckResult(VkResult result, const char* func, const char* file, const int line, int level)
	{
		ULOG_UTF8(Log::_Debug, "**********************************************************");
		ULOG_UTF8(Log::_Debug, "[ERROR] VkResult is Failed");
		ULOG_UTF8(Log::_Debug, "\t*ErrorCode: %s ", VkErrorString(result), file, line);
		ULOG_UTF8(Log::_Debug, "\t*Func: %s ", func);
		ULOG_UTF8(Log::_Debug, "\t*File: %s : %d", file, line);
		ULOG_UTF8(Log::_Debug, "**********************************************************");

		if (level == 1)
		{
			_ASSERT(result == VK_SUCCESS);
		}
	}

	VkFormat ConvertFormat(uint32 format)
	{
		static struct _FORMAT
		{
			uint32 type;
			VkFormat format;
		} _TypeFormat[] =
		{
			FMT_R8G8B8A8, VK_FORMAT_B8G8R8A8_UNORM,
			FMT_B8G8R8A8, VK_FORMAT_B8G8R8A8_UNORM,
		};
		for (int32 i = 0; i < _countof(_TypeFormat); i++)
		{
			if (_TypeFormat[i].type == format)
			{
				return _TypeFormat[i].format;
			}
		}

		return VK_FORMAT_UNDEFINED;
	}
} // namespace VULKAN
