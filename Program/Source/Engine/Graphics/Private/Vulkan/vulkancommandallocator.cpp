#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkancommandlist.h"

//------------------------------------------------------------------------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanCommandAllocator, IGraphicCommandAllocator);

VulkanCommandAllocator::VulkanCommandAllocator(COMMANDLIST_TYPE commandQueueType, uint32 queueFamilyIndex, Vk::CommandPoolCreateFlag::Type createFlags)
	: IGraphicCommandAllocator(commandQueueType)
{
	/*
		* VK_COMMAND_POOL_CREATE_TRANSIENT_BIT
			- Pool로 부터 가져온 command buffer가 짧게 사용되고 사용 이후 Pool로 반환될 때
			- Pool내에 메모리 할당 행위를 컨트롤하는데 사용될 수 있다.
		* VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
			- vkResetCommandBuffer가 호출될 때, 명시적으로 Reset된다.
			- vkBeginCommandBuffer가 호출될 때, 암묵적으로 Reset된다.
		- Reset이 설정되지 않았을 때, 이것이 설정되지 않았을 때에는 vkResetCommandPool이 호출될 때에만 리셋될 수 있다.
	*/
	VkCommandPoolCreateFlags poolCreateFlags = 0;
	if (createFlags & Vk::CommandPoolCreateFlag::_TRANSIENT_BIT)
	{
		poolCreateFlags |= VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
	}
	if (createFlags & Vk::CommandPoolCreateFlag::_RESET_COMMAND_BUFFER_BIT)
	{
		poolCreateFlags |= VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	}

	VkCommandPoolCreateInfo createInfo;
	Memory::MemZero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = poolCreateFlags;  // VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;	// or VK_COMMAND_POOL_CREATE_TRANSIENT_BIT
	createInfo.queueFamilyIndex = queueFamilyIndex;

	m_VkCommandPool = VK_NULL_HANDLE;
	VK_RESULT_ASSERT(::vkCreateCommandPool(GVulkanDevice->Handle(), &createInfo, nullptr, &m_VkCommandPool));

	m_CreateFlags = createFlags;
	m_QueueFamilyIndex = queueFamilyIndex;
}

VulkanCommandAllocator::~VulkanCommandAllocator()
{
	for (auto it = m_CommandLists.begin(); it != m_CommandLists.end(); it++)
		delete (*it);
	m_CommandLists.Clear();

	if (m_VkCommandPool != VK_NULL_HANDLE)
	{
		::vkDestroyCommandPool(GVulkanDevice->Handle(), m_VkCommandPool, nullptr);
		m_VkCommandPool = VK_NULL_HANDLE;
	}
}

void VulkanCommandAllocator::Reset()
{
	if (m_VkCommandPool != VK_NULL_HANDLE)
	{
		VK_RESULT(vkResetCommandPool(GVulkanDevice->Handle(), m_VkCommandPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT));
	}
}

VulkanCommandList* VulkanCommandAllocator::CreateCommandList(bool bSecondary)
{
	VulkanCommandList* newBuffer = NewObject(VulkanCommandList, this);
	m_CommandLists.Add(newBuffer);

	return newBuffer;
}

void VulkanCommandAllocator::DestroyCommandList(VulkanCommandList* buffer)
{
	if (buffer)
	{
		m_CommandLists.Remove(buffer);
		DeleteObject(buffer);
	}
}

void VulkanCommandAllocator::DestroyAllCommandLists()
{
	for (auto it = m_CommandLists.begin(); it != m_CommandLists.end(); it++)
		DeleteObject(*it);
	m_CommandLists.Clear();
}

void VulkanCommandAllocator::RefreshFenceStatus()
{
	for (SizeT i = 0; i < m_CommandLists.Num(); i++)
	{
		VulkanCommandList* CmdBuffer = m_CommandLists[i];
		CmdBuffer->RefreshFenceStatus();
	}
}
