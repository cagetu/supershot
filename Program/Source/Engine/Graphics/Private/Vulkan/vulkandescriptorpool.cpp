// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanadapter.h"
#include "Vulkan/vulkandescriptorlayout.h"
#include "Vulkan/vulkandescriptorpool.h"

//---------------------------------------------------------------------------------------------------------------------------------
//
//	DescriptorPool
//
//---------------------------------------------------------------------------------------------------------------------------------
VulkanDescriptorPool::VulkanDescriptorPool(VulkanDevice* device)
	: m_PoolHandle(VK_NULL_HANDLE)
	, m_NumAllocatedDescriptorSets(0)
	, m_PeakAllocatedDescriptorSets(0)
{
	const VkPhysicalDeviceLimits& Limits = device->PhysicalDevice()->DeviceProps.limits;

	// Increased from 8192 to prevent Protostar crashing on Mali
	m_MaxDescriptorSets = 16384;
	Memory::MemZero(m_MaxAllocatedPerType);
	Memory::MemZero(m_NumAllocatedPerType);
	Memory::MemZero(m_PeakAllocatedPerType);

	//#todo-rco: Get some initial values
	uint32 LimitMaxUniformBuffers = 2048;
	uint32 LimitMaxSamplers = 1024;
	uint32 LimitMaxCombinedImageSamplers = 4096;
	uint32 LimitMaxUniformTexelBuffers = 512;

	Array<VkDescriptorPoolSize> poolTypes;
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		type.descriptorCount = LimitMaxUniformBuffers;
		poolTypes.Add(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		type.descriptorCount = LimitMaxUniformBuffers;
		poolTypes.Add(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_SAMPLER;
		type.descriptorCount = LimitMaxSamplers;
		poolTypes.Add(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		type.descriptorCount = LimitMaxCombinedImageSamplers;
		poolTypes.Add(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
		type.descriptorCount = LimitMaxUniformTexelBuffers;
		poolTypes.Add(type);
	}
	for (const VkDescriptorPoolSize& poolType : poolTypes)
	{
		m_MaxAllocatedPerType[poolType.type] = poolType.descriptorCount;
	}

	VkDescriptorPoolCreateInfo PoolInfo;
	Memory::MemZero(PoolInfo);
	PoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	PoolInfo.poolSizeCount = poolTypes.Num();
	PoolInfo.pPoolSizes = poolTypes.Data();
	PoolInfo.maxSets = m_MaxDescriptorSets;;

	VK_RESULT(vkCreateDescriptorPool(device->Handle(), &PoolInfo, nullptr, &m_PoolHandle));

	static IndexT TicketID = 0;
	TicketID = (TicketID+1) >= INT_MAX ? 1 : TicketID++;
	m_PoolID = TicketID;
}

VulkanDescriptorPool::~VulkanDescriptorPool()
{
	// Pool에 반영된 모든 DescriptorSet들은 어떻게 하지?? DescriptorPool을 날리면 DescriptorSets들도 모두 같이 날아가나???
	for (SizeT index = 0; index < m_DescriptorSets.Num(); index++)
	{
		VulkanDescriptorSets* descriptorSets = m_DescriptorSets[index];

		VK_RESULT_EXPAND(vkFreeDescriptorSets(GVulkanDevice->Handle(), m_PoolHandle, descriptorSets->GetHandles().Num(), descriptorSets->GetHandles().Data()));
		TrackRemoveUsage(*descriptorSets->m_Layout);
		DeleteObject(descriptorSets);
	}
	m_DescriptorSets.Clear();

	if (m_PoolHandle != VK_NULL_HANDLE)
	{
		vkDestroyDescriptorPool(GVulkanDevice->Handle(), m_PoolHandle, nullptr);
		m_PoolHandle = VK_NULL_HANDLE;
	}
}

VulkanDescriptorSets* VulkanDescriptorPool::CreateDescriptorSet(VulkanDescriptorSetsLayout* layout)
{
	if (CanAllocate(*layout))
	{
		VkDescriptorSetAllocateInfo descriptorAllocateInfo = {};
		descriptorAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		descriptorAllocateInfo.descriptorSetCount = layout->NumDescriptorSets();
		descriptorAllocateInfo.pSetLayouts = layout->GetHandles().Data();
		descriptorAllocateInfo.descriptorPool = m_PoolHandle;

		Array<VkDescriptorSet> descriptorSets;
		descriptorSets.AddUnInitialized(layout->NumDescriptorSets());
		VkResult result = vkAllocateDescriptorSets(GVulkanDevice->Handle(), &descriptorAllocateInfo, descriptorSets.Data());
		if (result == VK_SUCCESS)
		{
			VulkanDescriptorSets* newDescriptorSets = NewObject(VulkanDescriptorSets, m_PoolID, layout);
			newDescriptorSets->m_DescriptorSetHandles = Template::Move(descriptorSets);

			m_DescriptorSets.Add(newDescriptorSets);

			TrackAddUsage(*layout);

			return newDescriptorSets;
		}
	}
	return nullptr;
}

void VulkanDescriptorPool::DestroyDescriptorSet(VulkanDescriptorSets* descriptorSets)
{
	if (descriptorSets)
	{
		VK_RESULT_EXPAND(vkFreeDescriptorSets(GVulkanDevice->Handle(), m_PoolHandle, descriptorSets->GetHandles().Num(), descriptorSets->GetHandles().Data()));

		TrackRemoveUsage(*descriptorSets->m_Layout);

		m_DescriptorSets.Remove(descriptorSets);
		DeleteObject(descriptorSets);
	}
}

void VulkanDescriptorPool::Reset()
{
	if (m_PoolHandle != VK_NULL_HANDLE)
	{
		VK_RESULT(vkResetDescriptorPool(GVulkanDevice->Handle(), m_PoolHandle, 0));
	}
	m_PoolHandle = VK_NULL_HANDLE;

	for (SizeT index = 0; index < m_DescriptorSets.Num(); index++)
	{
		DeleteObject(m_DescriptorSets[index]);
	}
	m_DescriptorSets.Clear();

	m_NumAllocatedDescriptorSets = 0;
}

bool VulkanDescriptorPool::CanAllocate(const VulkanDescriptorSetsLayout& Layout) const
{
	for (uint32 TypeIndex = VK_DESCRIPTOR_TYPE_BEGIN_RANGE; TypeIndex < VK_DESCRIPTOR_TYPE_END_RANGE; ++TypeIndex)
	{
		if (m_NumAllocatedPerType[TypeIndex] + (int32)Layout.NumUsedType((VkDescriptorType)TypeIndex) > m_MaxAllocatedPerType[TypeIndex])
		{
			return false;
		}
	}
	return true;
}

void VulkanDescriptorPool::TrackAddUsage(const VulkanDescriptorSetsLayout& Layout)
{
	for (uint32 TypeIndex = VK_DESCRIPTOR_TYPE_BEGIN_RANGE; TypeIndex < VK_DESCRIPTOR_TYPE_END_RANGE; ++TypeIndex)
	{
		m_NumAllocatedPerType[TypeIndex] += (int32)Layout.NumUsedType((VkDescriptorType)TypeIndex);
		m_PeakAllocatedPerType[TypeIndex] = Math::Max(m_PeakAllocatedPerType[TypeIndex], m_NumAllocatedPerType[TypeIndex]);
	}

	m_NumAllocatedDescriptorSets += Layout.NumDescriptorSets();
	m_PeakAllocatedDescriptorSets = Math::Max(m_PeakAllocatedDescriptorSets, m_NumAllocatedDescriptorSets);
}

void VulkanDescriptorPool::TrackRemoveUsage(const VulkanDescriptorSetsLayout& Layout)
{
	for (uint32 TypeIndex = VK_DESCRIPTOR_TYPE_BEGIN_RANGE; TypeIndex < VK_DESCRIPTOR_TYPE_END_RANGE; ++TypeIndex)
	{
		m_NumAllocatedPerType[TypeIndex] -= (int32)Layout.NumUsedType((VkDescriptorType)TypeIndex);
		_ASSERT(m_NumAllocatedPerType[TypeIndex] >= 0);
	}

	m_NumAllocatedDescriptorSets -= Layout.NumDescriptorSets();
}