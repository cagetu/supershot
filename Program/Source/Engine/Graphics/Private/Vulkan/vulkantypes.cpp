#include "foundation.h"
#include "Vulkan/vulkanheader.h"
#include "Vulkan/vulkantypes.h"

//------------------------------------------------------------------
//------------------------------------------------------------------

VkAttachmentLoadOp LoadAction(RT_LOAD_TYPE loadAction)
{
	/*	https://developer.samsung.com/game/usage
		VK_ATTACHMENT_LOAD_OP_DONT_CARE
			: This is the most efficient option. It should be used when the attachment doesn't need to be initialized, e.g. in renders where every pixel will be colored by a sky box or draw

		VK_ATTACHMENT_LOAD_OP_CLEAR
			: This operation is very efficient on tilers. As discussed in the Clearing framebuffer attachments section, it is the most efficient way to clear an attachment at the start of a render pass

		VK_ATTACHMENT_LOAD_OP_LOAD
			: This is the most costly operation. On TBRs, on-tile memory is initialized by loading the attachment's preserved data from system memory
	*/
	VkAttachmentLoadOp loadOp = VK_ATTACHMENT_LOAD_OP_MAX_ENUM;

	switch (loadAction)
	{
	case RT_LOAD_TYPE::Load:	loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;		break;
	case RT_LOAD_TYPE::Clear:	loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;		break;
	case RT_LOAD_TYPE::None:	loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;	break;
	default:																break;
	}

	_ASSERT(loadOp != VK_ATTACHMENT_LOAD_OP_MAX_ENUM);
	return loadOp;
}

VkAttachmentStoreOp StoreAction(RT_STORE_TYPE storeAction)
{
	/*	https://developer.samsung.com/game/usage
		VK_ATTACHMENT_STORE_OP_DONT_CARE
			: This is the most efficient option. It should be used when the attachment doesn't need to be preserved, e.g. depth and stencil attachment data only used to render this pass

		VK_ATTACHMENT_STORE_OP_STORE
			: This is the most costly operation. On TBRs, on-tile memory is preserved by storing to an attachment image in system memory
	*/
	VkAttachmentStoreOp storeOp = VK_ATTACHMENT_STORE_OP_MAX_ENUM;

	switch (storeAction)
	{
	case RT_STORE_TYPE::Store:	storeOp = VK_ATTACHMENT_STORE_OP_STORE;		break;
	case RT_STORE_TYPE::None:	storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;	break;
	default:																break;
	}

	_ASSERT(storeOp != VK_ATTACHMENT_STORE_OP_MAX_ENUM);
	return storeOp;
}

//------------------------------------------------------------------
//------------------------------------------------------------------
