#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "vulkan/vulkanbuffer.h"
#include "vulkan/vulkanvertexdeclaration.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanVertexDeclaration, IGraphicVertexDeclaration);

VulkanVertexDeclaration::VulkanVertexDeclaration()
{
}
VulkanVertexDeclaration::~VulkanVertexDeclaration()
{
}
