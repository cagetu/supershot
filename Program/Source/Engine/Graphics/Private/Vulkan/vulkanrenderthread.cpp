#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanrenderthread.h"
#include "Vulkan/vulkanrenderframe.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkancommandlist.h"
#include "Vulkan/vulkancommandqueue.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanRenderThread, IGraphicRenderThread);

VulkanRenderThread::VulkanRenderThread()
{
}

VulkanRenderThread::~VulkanRenderThread()
{
}

void VulkanRenderThread::AddFrame(VulkanRenderFrame* renderFrame)
{
}

void VulkanRenderThread::RemoveFrame(VulkanRenderFrame* renderFrame)
{
}
