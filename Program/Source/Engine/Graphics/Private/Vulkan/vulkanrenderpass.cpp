#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanrenderpass.h"
#include "Vulkan/vulkanadapter.h"
//#include <EASTL/array.h>

//------------------------------------------------------------------------------------------------------------------------------------
//	VulkanRenderPass
//------------------------------------------------------------------------------------------------------------------------------------
VulkanRenderPass::VulkanRenderPass()
	: m_RenderPassHandle(VK_NULL_HANDLE)
	, m_Width(0)
	, m_Height(0)
{
	__ConstructReference;
}

VulkanRenderPass::~VulkanRenderPass()
{
	Destroy();

	__DestructReference;
}

bool VulkanRenderPass::Create(const Vk::RenderPassLayout& renderpassLayout)
{
	m_Width = renderpassLayout.Width;
	m_Height = renderpassLayout.Height;

	/*
		1. 모든 subpassLayout에 추가된 attachment 정보를 가지고 전체 attachment/attachmentView 목록을 만든다.
		2. 각 SubPass의 ColorAttachment / DepthAttachment에는 index를 부여한다.
	*/
	SizeT numSubPasses = renderpassLayout.SubPassLayouts.Num();

	m_SubPassAttachmentReferences.Clear(numSubPasses);
	m_SubPassAttachmentReferences.AddDefaulted(numSubPasses);

	m_SubPassDescs.Clear(numSubPasses);
	m_SubPassDescs.AddDefaulted(numSubPasses);

	for (SizeT subPassIndex = 0; subPassIndex < numSubPasses; subPassIndex++)
	{
		// 현재 정의된 subPass Layout
		const Vk::SubPassLayout& subpassLayout = renderpassLayout.SubPassLayouts[subPassIndex];

		// 현재 서브 패스 Attachment 목록
		Vk::SubPassAttachmentReference& subpassAttachmentRef = m_SubPassAttachmentReferences[subPassIndex];
		subpassAttachmentRef.Reset();

		for (int32 index = 0; index < subpassLayout.NumColorAttachments; index++)
		{
			const Vk::SubPassLayout::ColorAttachmentInfo& attachment = subpassLayout.ColorAttachments[index];

			// AttachmentDescription
			VkAttachmentDescription desc = {};
			{
				desc.flags = 0;
				/// attachment Format
				desc.format = GVulkanDevice->PhysicalDevice()->GetFormat(attachment.format);
				/// 이미지의 sample 수 (멀티샘플링 지원 여부 체크)
				desc.samples = GVulkanDevice->PhysicalDevice()->GetSampleCount(attachment.multiSamples);
				/// 렌더패스가 시작할 때 이미지의 내용을 어떻게 처리할 것인지 결정 (Clear, Preserve, don't care).
				desc.loadOp = LoadAction(attachment.colorOp.load);
				/// 렌더패스 이후에 이미지의 내용을 어떻게 할 것인지 드라이버에게 정보를 준다.
				desc.storeOp = StoreAction(attachment.colorOp.store);
				/// depth/stencil 이미지들의 stencil 파트에 대해서 LoadAction 설정. color attachment들에 대해서는 무시된다.
				desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
				/// depth/stencil 이미지들의 stencil 파트에 대해서 StoreAction 설정. color attachment들에 대해서는 무시된다.
				desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
				/// 렌더패스가 시작할 때, attachment가 가질 layout
				desc.initialLayout = attachment.layout.initLayout;
				/// 렌더패스가 끝났을 때 이미지를 자동으로 Layout으로 전환한다.
				desc.finalLayout = attachment.layout.finalLayout;
			}
			IndexT attachmentIndex = m_AttachmentDecs.Add(desc);

			// AttachmentView
			m_AttachmentViews.Add(attachment.imageView);

			// ClearValue
			VkClearValue clearValue;
			{
				clearValue.color.float32[0] = attachment.clearColor.r;
				clearValue.color.float32[1] = attachment.clearColor.g;
				clearValue.color.float32[2] = attachment.clearColor.b;
				clearValue.color.float32[3] = attachment.clearColor.a;
			}
			m_ClearValues.Add(clearValue);

			// AttachmentReferenece
			VkAttachmentReference& colorRef = subpassAttachmentRef.ColorAttachments[index];
			colorRef.attachment = attachmentIndex;
			colorRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			subpassAttachmentRef.NumColorAttachments++;
		}

		// Depth
		if (subpassLayout.HasDepthStencilAttachment)
		{
			const Vk::SubPassLayout::DepthStencilAttachmentInfo& attachment = subpassLayout.DepthStencilAttachment;

			// AttachmentDescription
			VkAttachmentDescription desc;
			{
				desc.flags = 0;
				/// attachment Format
				desc.format = GVulkanDevice->PhysicalDevice()->GetFormat(attachment.format);
				/// 이미지의 sample 수 (멀티샘플링 지원 여부 체크)
				desc.samples = GVulkanDevice->PhysicalDevice()->GetSampleCount(attachment.multiSamples);
				/// 렌더패스가 시작할 때 이미지의 내용을 어떻게 처리할 것인지 결정 (Clear, Preserve, don't care).
				desc.loadOp = LoadAction(attachment.depthOp.load);
				/// 렌더패스 이후에 이미지의 내용을 어떻게 할 것인지 드라이버에게 정보를 준다.
				desc.storeOp = StoreAction(attachment.depthOp.store);
				/// depth/stencil 이미지들의 stencil 파트에 대해서 LoadAction 설정. color attachment들에 대해서는 무시된다.
				desc.stencilLoadOp = LoadAction(attachment.stencilOp.load);
				/// depth/stencil 이미지들의 stencil 파트에 대해서 StoreAction 설정. color attachment들에 대해서는 무시된다.
				desc.stencilStoreOp = StoreAction(attachment.stencilOp.store);
				/// 렌더패스가 시작할 때, attachment가 가질 layout
				desc.initialLayout = attachment.layout.initLayout;
				/// 렌더패스가 끝났을 때 이미지를 자동으로 Layout으로 전환한다.
				desc.finalLayout = attachment.layout.finalLayout;
			}
			IndexT attachmentIndex = m_AttachmentDecs.Add(desc);

			// AttachmentView
			m_AttachmentViews.Add(attachment.imageView);

			// ClearValue
			VkClearValue clearValue;
			{
				clearValue.depthStencil.depth = attachment.clearDepth;
				clearValue.depthStencil.stencil = attachment.clearStencil;
			}
			m_ClearValues.Add(clearValue);

			// AttachmentReferenece
			VkAttachmentReference& depthRef = subpassAttachmentRef.DepthStencilAttachment;
			depthRef.attachment = attachmentIndex;
			depthRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

			subpassAttachmentRef.HasDepthStencilAttachment = true;
		}

		// Resolve
		if (subpassLayout.HasResolveAttachment)
		{
			const Vk::SubPassLayout::ResolveAttachmentInfo& attachment = subpassLayout.ResolveAttachment;

			// AttachmentDescription
			VkAttachmentDescription desc = {};
			{
				desc.flags = 0;
				/// attachment Format
				desc.format = GVulkanDevice->PhysicalDevice()->GetFormat(attachment.format);
				/// 이미지의 sample 수 (멀티샘플링 지원 여부 체크)
				desc.samples = GVulkanDevice->PhysicalDevice()->GetSampleCount(attachment.multiSamples);
				/// 렌더패스가 시작할 때 이미지의 내용을 어떻게 처리할 것인지 결정 (Clear, Preserve, don't care).
				desc.loadOp = LoadAction(attachment.resolveOp.load);
				/// 렌더패스 이후에 이미지의 내용을 어떻게 할 것인지 드라이버에게 정보를 준다.
				desc.storeOp = StoreAction(attachment.resolveOp.store);
				/// 렌더패스가 시작할 때, attachment가 가질 layout
				desc.initialLayout = attachment.layout.initLayout;
				/// 렌더패스가 끝났을 때 이미지를 자동으로 Layout으로 전환한다.
				desc.finalLayout = attachment.layout.finalLayout;
			}
			IndexT attachmentIndex = m_AttachmentDecs.Add(desc);

			// AttachmentView
			m_AttachmentViews.Add(attachment.imageView);

			// AttachmentReferenece
			VkAttachmentReference& resolveRef = subpassAttachmentRef.ResolveAttachment;
			resolveRef.attachment = attachmentIndex;
			resolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			subpassAttachmentRef.HasResolveAttachment = true;
		}

		// subpass 설명: attachment 정보 설정
		VkSubpassDescription& subPassDesc = m_SubPassDescs[subPassIndex];
		subPassDesc.flags = 0;
		subPassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subPassDesc.colorAttachmentCount = subpassAttachmentRef.NumColorAttachments;
		subPassDesc.pColorAttachments = subpassAttachmentRef.ColorAttachments;
		subPassDesc.pDepthStencilAttachment = &subpassAttachmentRef.DepthStencilAttachment;
		subPassDesc.pResolveAttachments = subpassAttachmentRef.HasResolveAttachment ? nullptr : &subpassAttachmentRef.ResolveAttachment;
		subPassDesc.inputAttachmentCount = subpassAttachmentRef.NumInputAttachments;
		subPassDesc.pInputAttachments = subpassAttachmentRef.NumInputAttachments > 0 ? nullptr : subpassAttachmentRef.InputAttachments;
		subPassDesc.preserveAttachmentCount = subpassAttachmentRef.NumPreserveAttachments;
		subPassDesc.pPreserveAttachments = subpassAttachmentRef.NumPreserveAttachments > 0 ? nullptr : subpassAttachmentRef.PreserveAttachments;
	}

	// Setup subpass dependencies
	// These will add the implicit ttachment layout transitionss specified by the attachment descriptions
	// The actual usage layout is preserved through the layout specified in the attachment reference
	// Each subpass dependency will introduce a memory and execution dependency between the source and dest subpass described by
	// srcStageMask, dstStageMask, srcAccessMask, dstAccessMask (and dependencyFlags is set)
	// Note: VK_SUBPASS_EXTERNAL is a special constant that refers to all commands executed outside of the actual renderpass)
	Array<VkSubpassDependency> dependencies(2);

	// First dependency at the start of the renderpass
	// Does the transition from final to initial layout
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;								// Producer of the dependency
	dependencies[0].dstSubpass = 0;													// Consumer is our single subpass that will wait for the execution depdendency
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// Second dependency at the end the renderpass
	// Does the transition from the initial to the final layout
	dependencies[1].srcSubpass = 0;													// Producer of the dependency is our single subpass
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;								// Consumer are all commands outside of the renderpass
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// rendepass 생성 구조체
	VkRenderPassCreateInfo createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	createInfo.flags = 0;
	createInfo.pNext = nullptr;
	/// subpass 목록
	createInfo.subpassCount = m_SubPassDescs.Num();
	createInfo.pSubpasses = m_SubPassDescs.Data();
	/// 사용될 모든 attachment 목록
	createInfo.attachmentCount = m_AttachmentDecs.Num();
	createInfo.pAttachments = m_AttachmentDecs.Data();
	/// dependency
	createInfo.dependencyCount = dependencies.Num();
	createInfo.pDependencies = dependencies.Data();

	VK_RESULT(vkCreateRenderPass(GVulkanDevice->Handle(), &createInfo, nullptr, &m_RenderPassHandle));

	return true;
}

void VulkanRenderPass::Destroy()
{
	if (m_RenderPassHandle)
	{
		vkDestroyRenderPass(GVulkanDevice->Handle(), m_RenderPassHandle, nullptr);
	}
	m_RenderPassHandle = VK_NULL_HANDLE;
}
