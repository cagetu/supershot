// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanshader.h"
#include "Vulkan/vulkanshadermodule.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanShader, IGraphicShader);

VulkanShader::VulkanShader()
{
}

VulkanShader::~VulkanShader()
{
}
