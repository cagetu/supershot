#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanfence.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkanrenderpass.h"
#include "Vulkan/vulkanframebuffer.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkancommandlist.h"

#define _TEST_RESET 1

//------------------------------------------------------------------------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanCommandList, IGraphicCommandList);

VulkanCommandList::VulkanCommandList(VulkanCommandAllocator* commandPool, bool bSecondary)
{
	VkCommandBufferAllocateInfo allocateInfo = {};
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.pNext = nullptr;
	/// Drawing List와 유사한 전통적인 Command Buffer. Queue에 Submit된다. Secondary는 Primary CommandBuffer에 참조될수만 있다. (Submit 불가능)
	allocateInfo.level = bSecondary ? VK_COMMAND_BUFFER_LEVEL_SECONDARY : VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	/// 생성할 CommandBuffer 수
	allocateInfo.commandBufferCount = 1;
	/// CommandPool 핸들
	allocateInfo.commandPool = commandPool->Handle();
	VK_RESULT_ASSERT(::vkAllocateCommandBuffers(GVulkanDevice->Handle(), &allocateInfo, &m_BufferHandle));

	m_CommandPool = commandPool;
	m_bSecondary = bSecondary;
	m_Recoreded = false;
	m_State = _STATE::READYTOBEGIN;

	/// 커멘드 버퍼에 사용할 팬스 생성
	m_Fence = GVulkanDevice->FenceManager()->AllocateFence(false);
	m_FenceSignaledCounter = 0;
}

VulkanCommandList::~VulkanCommandList()
{
	if (m_BufferHandle != VK_NULL_HANDLE)
	{
		// CommandBuffer의 사용이 완료를 기다린다.
		if (m_State == _STATE::SUBMITTED)
		{
			uint32 timeout = 60 * 1000 * 1000LL;
			GVulkanDevice->FenceManager()->WaitAndReleaseFence(m_Fence, timeout);
		}
		else
		{
			GVulkanDevice->FenceManager()->ReleaseFence(m_Fence);
		}

		::vkFreeCommandBuffers(GVulkanDevice->Handle(), m_CommandPool->Handle(), 1, &m_BufferHandle);
		m_BufferHandle = VK_NULL_HANDLE;
	}
}

void VulkanCommandList::BeginRecord(UsageFlags::Type usageFlags)
{
	CHECK(m_State == _STATE::READYTOBEGIN, TEXT("m_State != _STATE::READYTOBEGIN"));

	/*
		VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT : Secondary Command Buffer에 사용한다.
		VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT : 한번 Recording 된 Primary Command Buffer 를 resubmit 가능하다.
		VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT : 한번만 SUBMIT 된 후, CommandBuffer가 Reset 된다.
	*/
	m_UsageFlags = usageFlags;
	VkCommandBufferUsageFlags bufferUsageFlags = 0;
	if (usageFlags&UsageFlags::_RENDER_PASS_CONTINUE)
	{
		bufferUsageFlags |= VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
	}
	if (usageFlags&UsageFlags::_SIMULTANEOUS_USE_BIT)
	{
		bufferUsageFlags |= VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	}
	if (usageFlags&UsageFlags::_ONETIME_SUBMIT)
	{
		_ASSERT(m_CommandPool->CanBeReset());
		bufferUsageFlags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	}

	VkCommandBufferBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.flags = bufferUsageFlags;
	info.pNext = nullptr;
	info.pInheritanceInfo = nullptr;
	VK_RESULT(::vkBeginCommandBuffer(m_BufferHandle, &info));

	m_State = _STATE::BEGIN_RECORD;
}

void VulkanCommandList::EndRecord()
{
	_ASSERT(m_State == _STATE::BEGIN_RECORD);

	VK_RESULT(::vkEndCommandBuffer(m_BufferHandle));
	
	m_Recoreded = true;

	m_State = _STATE::END_RECORD;
}

void VulkanCommandList::BeginRenderPass(VulkanFrameBuffer* frameBuffer)
{
	_ASSERT(m_State == _STATE::BEGIN_RECORD);

	VulkanRenderPass* renderPass = frameBuffer->RenderPass();

	VkRenderPassBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	info.pNext = nullptr;
	info.renderArea.offset.x = 0;
	info.renderArea.offset.y = 0;
	info.framebuffer = frameBuffer->Handle();
	info.renderPass = renderPass->Handle();
	info.renderArea.extent.width = renderPass->Width();
	info.renderArea.extent.height = renderPass->Height();
	info.clearValueCount = renderPass->ClearValues().Num();
	info.pClearValues = renderPass->ClearValues().Data();

	vkCmdBeginRenderPass(m_BufferHandle, &info, VK_SUBPASS_CONTENTS_INLINE/*VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS*/);

	m_State = _STATE::RENDERPASS;
}

void VulkanCommandList::EndRenderPass()
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdEndRenderPass(m_BufferHandle);

	m_State = _STATE::BEGIN_RECORD;
}

void VulkanCommandList::Submit(VulkanCommandQueue* gfxQueue,
                                 VulkanSemaphore* signalSemaphore,
                                 VulkanSemaphore* waitSemaphore,
                                 VkPipelineStageFlags waitStageMask,
                                 bool bWaitIdle)
{
	_ASSERT(m_State == _STATE::END_RECORD);

	// 서밋 전에는 반드시 Unsignaled 여야 한다
	_ASSERT(!GVulkanDevice->FenceManager()->IsSignaled(m_Fence));

	gfxQueue->Submit(this, m_Fence, signalSemaphore, waitSemaphore, waitStageMask, bWaitIdle);

	m_State = _STATE::SUBMITTED;

	//m_CommandPool->RefreshFenceStatus();
	RefreshFenceStatus();
}

void VulkanCommandList::RefreshFenceStatus()
{
	if (m_State == _STATE::SUBMITTED)
	{
#if _TEST_RESET
		// 서밋했는데, 현재 상태가 완료 
		if (GVulkanDevice->FenceManager()->IsSignaled(m_Fence))
		{
			Reset();

			GVulkanDevice->FenceManager()->ResetFence(m_Fence);

			m_FenceSignaledCounter++;
		}
#else
		// 다시 사용할 수 있게 되기 전에 command buffer의 실행이 완전히 끝날 때까지 대기한다.
		WaitForIdle();
		m_State = _STATE::READYTOBEGIN;
#endif
	}
	else
	{
		_ASSERT(m_Fence->IsSignaled() == false);
	}
}

//------------------------------------------------------------------
/*
 *	FreeCommandBuffer -> AllocateCommandBuffer 하는 것보다 훨씬 효율적이다.
*/
//------------------------------------------------------------------
void VulkanCommandList::Reset()
{
	if (m_BufferHandle != VK_NULL_HANDLE)
	{
		// _RESET_COMMAND_BUFFER_BIT이 설정되어 있을 때에만, Reset이 가능하다.
		_ASSERT(m_CommandPool->CanBeReset());
		VK_RESULT(vkResetCommandBuffer(m_BufferHandle, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT));

		m_State = _STATE::READYTOBEGIN;
	}
}

void VulkanCommandList::WaitForIdle()
{
#if _TEST_RESET
	if (m_State == _STATE::SUBMITTED)
	{
		m_Fence->WaitForSignal(UINT64_MAX);
		m_Fence->Reset();

		m_State = _STATE::READYTOBEGIN;
	}
#else
	if (m_Recoreded == false)
		return;

	m_Fence->WaitForSignal(UINT64_MAX);
	m_Fence->Reset();

	m_Recoreded = false;
#endif
}

void VulkanCommandList::ExcuteCommands(uint32 count, Array<VulkanCommandList*>& secondaryBuffers)
{
	CHECK(IsSecondary() == false, TEXT("ExcuteCommands is enabled only primary command buffer!"));

	Array<VkCommandBuffer> bufferHandles;
	for (uint32 index = 0; index < secondaryBuffers.Num(); index++)
	{
		ASSERT(secondaryBuffers[index]->IsSecondary());
		bufferHandles.Add(secondaryBuffers[index]->Handle());
	}

	vkCmdExecuteCommands(m_BufferHandle, count, bufferHandles.Data());
}

void VulkanCommandList::SetImageBarrier(VkImage image, 
										  VkImageLayout oldLayout, 
										  VkImageLayout newLayout, 
										  VkAccessFlags srcAccessMask, 
										  VkAccessFlags dstAccessMask,
										  VkPipelineStageFlags srcStageMask, 
										  VkPipelineStageFlags dstStageMask, 
										  const VkImageSubresourceRange& subresourceRange)
{
	//_ASSERT(CmdBuffer->GetState() == VulkanCmdBuffer::_STATE::BEGIN);
	_ASSERT(Handle() != VK_NULL_HANDLE);

	VkImageMemoryBarrier imageBarrier;
	Memory::MemZero(imageBarrier);
	imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageBarrier.pNext = nullptr;
	imageBarrier.image = image;
	// QueueFamily의 소유를 변경하고 싶을 때 사용한다.
	imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	// Layout 변경을 위해 설정
	imageBarrier.oldLayout = oldLayout;
	imageBarrier.newLayout = newLayout;
	// 장벽 구현 전에 수행이 완료되어야 하는 파이프라인 스테이지
	imageBarrier.srcAccessMask = srcAccessMask;
	// 장벽 이전의 명령이 모두 수행되기 전까지 시작하면 안되는 파이프라인 스테이지
	imageBarrier.dstAccessMask = dstAccessMask;
	// 영향을 받는 이미지의 일부 부분 설정
	imageBarrier.subresourceRange = subresourceRange;

	::vkCmdPipelineBarrier(m_BufferHandle, srcStageMask, dstStageMask, 0, 0, nullptr, 0, nullptr, 1, &imageBarrier);
}

const VkImageSubresourceRange& DefaultSubresourceRange()
{
	static VkImageSubresourceRange SubresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
	return SubresourceRange;
}

void VulkanCommandList::SetImageBarrier(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout)
{
	SetImageBarrier(image, oldLayout, newLayout, DefaultSubresourceRange());
}

void VulkanCommandList::SetImageBarrier(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, const VkImageSubresourceRange& subresourceRange)
{
	//_ASSERT(CmdBuffer->GetState() == VulkanCmdBuffer::_STATE::BEGIN);
	_ASSERT(Handle() != VK_NULL_HANDLE);

	//
	// Barrier는 우리가 기대했던 결과를 얻는다는 것을 확인하는 특수한 순서에 발생하는 우리의 operation이 GPU에서 완료되었다는 것을 확인한다.
	// 하나의 Barrier는 큐에서 두 가지 수행으로 나뉜다: Barrier 이전과 Barrier 이후
	// 
	VkImageMemoryBarrier imageBarrier;
	Memory::MemZero(imageBarrier);
	imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageBarrier.pNext = nullptr;
	imageBarrier.oldLayout = oldLayout;
	imageBarrier.newLayout = newLayout;
	imageBarrier.image = image;
	imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.subresourceRange = subresourceRange;

	switch (newLayout)
	{
	case VK_IMAGE_LAYOUT_GENERAL:
		if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		break;
	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		if (oldLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		}
		imageBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;
	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		imageBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;
	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL:
		_ASSERT(0);
		break;
	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		imageBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
		break;
	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		_ASSERT(0);
		break;
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		imageBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;
	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		_ASSERT(0);
		break;
	case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
		if (oldLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
		imageBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		break;
	default:
		_ASSERT(0);
		break;
	}

	/*	Pipeline Stage Flags
		: logical pipeline 이벤트들이 시그널 될 수 있는 곳이 어딘지 명시. 실행 의존성의 source와 destination을 명시

		VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT
		- Command들이 처음에 Queue에 의해 받아들여지는 파이프라인의 Stage

		VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT
		- Draw/DispatchIndirect 데이터 구조체가 소비되는 파이프라인 Stage

		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
		- 버텍스와 인덱스 버퍼가 소비되는 파이프라인 Stage

		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT
		- Vertex Shader Stage

		VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT
		- Tessellation Control Shader Stage

		VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT
		- Tessellation Evaulation Shader Stage

		VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT
		- Geometry Shader Stage

		VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT
		- Fragment Shader Stage

		VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
		- 미리(early) fragment tests(fragment shading 호출 전에 depth나 stencil test)를 수행하는 파이프라인 스테이지

		VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT
		- 늦은(late) fragment tests(fragment shading 호출 후 depth,stencil test)를 수행하는 파이프라인 스테이지

		VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
		- 최종 칼라값이 pipeline으로부터 출력되고 Blending 이후 파이프라인 스테이지
		- 이 stage는 subpass의 마지막에 발생하는 resolve 오퍼레이션이 포함된다.
		- 값들이 메모리에 Commit 되어지는 불필요한 지시를 하지 말아야 한다.

		VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT
		- Compute Shader의 실행

		VK_PIPELINE_STAGE_TRANSFER_BIT
		- Copy Command의 실행
		- 이것은 모든 transfer commands로 부터 수행 결과를 포함한다.
		- transfer command 의 세트는 vkCmdCopyBuffer, vkCmdCopyImage, vkCmdBlitImage, VkCmdCopyBufferToImage, vkCmdCopyImageToBuffer,
		vkCmdUpdateBuffer, vkCmdFillBuffer, vkCmdClearColorImage, vkCmdClearDepthStencilImage, vkCmdResolveImage, vkCmdCopyQueryPoolResults로 구성된다.

		VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
		- Command가 실행이 완료되는 파이프라인에서 Final Stage

		VK_PIPELINE_STAGE_HOST_BIT
		- 디바이스 메모리의 읽기/쓰기 호스트에 실행을 지시하는 pseudo-stage

		VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT
		- 모든 graphics 파이프라인 Stage의 실행

		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT
		- Queue에 지원되는 모든 Stage의 실행
	*/
	VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

	if (oldLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
	{
		srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	}
	else if (newLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
	{
		srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	}

	/*	Pipeline Barrier
		- Command buffer내 초기(eariler) command의 세트와 command buffer내 이후(later) command의 세트 사이의 메모리 의존성과 실행 의존성을 추가한다.
		- dstStageMask에 오직 VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT와 실행 의존성은 다음 순서의 Commands를 지연시키지 않을 것이다.
		- VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT는 다음 access가 다른 queue에서나 presentation engine에 의해 완료되었을 memory barriers와 layout transition을 완수하는데 유용하다.
		- 이 경우에 같은 Queue에서 다음 순서의 command들은 대기할 필요가 없지만, barrier나 transition은 반드시 batch 시그널과 관련된 이전의 세마포어들이 완료되어야만 한다.
		- 만약 구현이 파이프라인의 어떤 명시된 스테이지에 이벤트 스테이트를 업데이트를 할 수 없다면, 대신에 논리적으로 마지막 Stage에 업데이트한다.
	*/
	::vkCmdPipelineBarrier(m_BufferHandle,
							srcStageMask,	//  
							dstStageMask,
							0,				// VkDependencyFlags 
							0, nullptr,		// MemoryBarrier
							0, nullptr,		// BufferMemoryBarrier
							1, &imageBarrier);	// ImageMemoryBarrier
}

void VulkanCommandList::ClearColorImage(VkImage imageHandle, VkImageLayout imageLayout, const VkClearColorValue& clearValue)
{
	vkCmdClearColorImage(m_BufferHandle, imageHandle, imageLayout, &clearValue, 1, &DefaultSubresourceRange());
}

void VulkanCommandList::ClearDepthStencilImage(VkImage imageHandle, VkImageLayout imageLayout, const VkClearDepthStencilValue& clearValue)
{
	vkCmdClearDepthStencilImage(m_BufferHandle, imageHandle, imageLayout, &clearValue, 1, &DefaultSubresourceRange());
}

void VulkanCommandList::ClearResolveImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageResolve* pRegions)
{
	vkCmdResolveImage(m_BufferHandle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void VulkanCommandList::ClearAttachments(uint32 attachmentCount, const VkClearAttachment* pAttachments, uint32 rectCount, const VkClearRect* pRects)
{
	vkCmdClearAttachments(m_BufferHandle, attachmentCount, pAttachments, rectCount, pRects);
}

void VulkanCommandList::SetViewport(float width, float height, float posX, float posY, float minDepth, float maxDepth)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	VkViewport vp;
	vp.x = posX;
	vp.y = posY;
	vp.width = width;
	vp.height = height;
	vp.minDepth = minDepth;
	vp.maxDepth = maxDepth;

	vkCmdSetViewport(m_BufferHandle, 0, 1, &vp);
}

void VulkanCommandList::SetScissorRect(uint32 width, uint32 height, int32 posX, int32 posY)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	VkRect2D rect;
	rect.offset.x = posX;
	rect.offset.y = posY;
	rect.extent.width = width;
	rect.extent.height = height;

	vkCmdSetScissor(m_BufferHandle, 0, 1, &rect);
}

void VulkanCommandList::SetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetDepthBias(m_BufferHandle, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
}

void VulkanCommandList::SetDepthBounds(float minDepthBounds, float maxDepthBounds)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetDepthBounds(m_BufferHandle, minDepthBounds, maxDepthBounds);
}

void VulkanCommandList::SetBlendConstants(float blendConstants[4])
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetBlendConstants(m_BufferHandle, blendConstants);
}

void VulkanCommandList::SetStencilCompareMask(uint32 faceMask, uint32 compareMask)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetStencilCompareMask(m_BufferHandle, faceMask, compareMask);
}

void VulkanCommandList::SetStencilWriteMask(uint32 faceMask, uint32 writeMask)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetStencilWriteMask(m_BufferHandle, faceMask, writeMask);
}

void VulkanCommandList::SetStencilReference(uint32 faceMask, uint32 reference)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetStencilReference(m_BufferHandle, faceMask, reference);
}
