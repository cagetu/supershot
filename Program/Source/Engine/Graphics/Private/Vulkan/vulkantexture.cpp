#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkantexture.h"
#include "Vulkan/vulkanadapter.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanTexture, IGraphicTexture);

VulkanTexture::VulkanTexture()
	: m_Image(nullptr)
	, m_ImageView(nullptr)
{
}

VulkanTexture::~VulkanTexture()
{
	ASSERT(m_Image == nullptr);
	ASSERT(m_ImageView == nullptr);
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanRenderTexture, VulkanTexture);

VulkanRenderTexture::VulkanRenderTexture()
	: m_Multisamples(1)
	, m_bCreated(false)
{
}

VulkanRenderTexture::~VulkanRenderTexture()
{
	Destroy();
}

bool VulkanRenderTexture::Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	m_Image = image;
	m_ImageView = imageView;

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = false;
	return true;
}

void VulkanRenderTexture::Destroy()
{
	if (m_bCreated)
	{
		DeleteObject(m_ImageView);
		DeleteObject(m_Image);
	}
	m_ImageView = nullptr;
	m_Image = nullptr;
}


//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanColorRenderTexture, VulkanRenderTexture);

VulkanColorRenderTexture::VulkanColorRenderTexture()
	: m_ClearColor(LinearColor::WHITE)
{
}

VulkanColorRenderTexture::~VulkanColorRenderTexture()
{
}

bool VulkanColorRenderTexture::Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	m_Image = NewObject(VulkanImage);
	if (m_Image->Create(width, height, 1, format, TEX_RenderTarget, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_USAGE_TRANSFER_DST_BIT, mipMaps, multiSamples) == false)
	{
		DeleteObject(m_Image);
		return false;
	}

	m_ImageView = NewObject(VulkanImageView);
	if (m_ImageView->Create(m_Image->Handle(), format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D, mipMaps) == false)
	{
		DeleteObject(m_ImageView);
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = true;
	return true;
}

bool VulkanColorRenderTexture::Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	m_Image = NewObject(VulkanImage);
	if (m_Image->Create(imageHandle) == false)
	{
		DeleteObject(m_Image);
		return false;
	}

	m_ImageView = NewObject(VulkanImageView);
	if (m_ImageView->Create(m_Image->Handle(), format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D, mipMaps) == false)
	{
		DeleteObject(m_ImageView);
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = true;
	return true;
}

bool VulkanColorRenderTexture::Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	return VulkanRenderTexture::Create(image, imageView, width, height, format, mipMaps, multiSamples);
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanDepthStencilRenderTexture, VulkanRenderTexture);

VulkanDepthStencilRenderTexture::VulkanDepthStencilRenderTexture()
	: m_ClearDepth(1.0f)
	, m_ClearStencil(1)
{
}

VulkanDepthStencilRenderTexture::~VulkanDepthStencilRenderTexture()
{
}

bool VulkanDepthStencilRenderTexture::Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	m_Image = NewObject(VulkanImage);
	if (!m_Image->Create(width, height, 1, format, TEX_DepthStencilTarget, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_USAGE_TRANSFER_DST_BIT, mipMaps, multiSamples))
	{
		DeleteObject(m_Image);
		return false;
	}

	m_ImageView = NewObject(VulkanImageView);
	if (!m_ImageView->Create(m_Image->Handle(), format, VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, VK_IMAGE_VIEW_TYPE_2D, mipMaps))
	{
		DeleteObject(m_ImageView);
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = true;
	return true;
}

bool VulkanDepthStencilRenderTexture::Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	m_Image = NewObject(VulkanImage);
	if (!m_Image->Create(imageHandle))
	{
		DeleteObject(m_Image);
		return false;
	}

	m_ImageView = NewObject(VulkanImageView);
	if (!m_ImageView->Create(m_Image->Handle(), format, VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, VK_IMAGE_VIEW_TYPE_2D, mipMaps))
	{
		DeleteObject(m_ImageView);
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = true;
	return true;
}

bool VulkanDepthStencilRenderTexture::Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	return VulkanRenderTexture::Create(image, imageView, width, height, format, mipMaps, multiSamples);
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanResolveRenderTexture, VulkanRenderTexture);

VulkanResolveRenderTexture::VulkanResolveRenderTexture()
{
}

VulkanResolveRenderTexture::~VulkanResolveRenderTexture()
{
}

bool VulkanResolveRenderTexture::Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	uint32 usageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
	//createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	m_Image = NewObject(VulkanImage);
	if (m_Image->Create(width, height, 1, format, TEX_ResolveRenderTarget, VK_IMAGE_VIEW_TYPE_2D, usageFlags, mipMaps, multiSamples) == false)
	{
		DeleteObject(m_Image);
		return false;
	}

	m_ImageView = NewObject(VulkanImageView);
	if (m_ImageView->Create(m_Image->Handle(), format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D, mipMaps) == false)
	{
		DeleteObject(m_ImageView);
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = true;
	return true;
}

bool VulkanResolveRenderTexture::Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	m_Image = NewObject(VulkanImage);
	if (m_Image->Create(imageHandle) == false)
	{
		DeleteObject(m_Image);
		return false;
	}

	m_ImageView = NewObject(VulkanImageView);
	if (m_ImageView->Create(m_Image->Handle(), format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D, mipMaps) == false)
	{
		DeleteObject(m_ImageView);
		return false;
	}

	m_Width = width;
	m_Height = height;
	m_Format = format;
	m_MipLevels = mipMaps;
	m_Multisamples = multiSamples;
	m_bCreated = true;
	return true;
}

bool VulkanResolveRenderTexture::Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps, int32 multiSamples)
{
	return VulkanRenderTexture::Create(image, imageView, width, height, format, mipMaps, multiSamples);
}
