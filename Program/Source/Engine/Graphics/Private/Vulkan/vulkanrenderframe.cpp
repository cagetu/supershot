#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkancommandlist.h"
#include "Vulkan/vulkanrenderpass.h"
#include "Vulkan/vulkanframebuffer.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkanrenderframe.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanRenderFrame, IGraphicRenderFrame);

VulkanRenderFrame::VulkanRenderFrame(VulkanCommandAllocator* commandPool, VulkanCommandQueue* gfxQueue)
	: m_CommandPool(commandPool)
	, m_GfxQueue(gfxQueue)
	, m_FrameBuffer(nullptr)
{
}

VulkanRenderFrame::~VulkanRenderFrame()
{
	Destroy();
}

void VulkanRenderFrame::Reset()
{
	if (m_CommandPool.IsValid())
	{
		m_CommandPool->Reset();
	}
}

bool VulkanRenderFrame::Create(VulkanFrameBuffer* framebuffer)
{
	m_FrameBuffer = framebuffer;
	return true;
}

void VulkanRenderFrame::Destroy()
{
	m_FrameBuffer = nullptr;
	m_CommandPool = nullptr;
}

VulkanCommandList* VulkanRenderFrame::CreateCommandList(bool bSecondary)
{
	return m_CommandPool->CreateCommandList(bSecondary);
}

void VulkanRenderFrame::DestroyCommandList(VulkanCommandList* commandBuffer)
{
	m_CommandPool->DestroyCommandList(commandBuffer);
}

