// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkandescriptor.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanDescriptor, IGraphicDescriptor);

VulkanDescriptor::VulkanDescriptor()
{
}

VulkanDescriptor::~VulkanDescriptor()
{
}


