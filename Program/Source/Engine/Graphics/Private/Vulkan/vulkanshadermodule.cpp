// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanshadermodule.h"
#include "SpirvReflect/spirv_reflect.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanShaderModule, IGraphicShaderModule);

VulkanShaderModule::VulkanShaderModule()
	: m_ShaderModule(VK_NULL_HANDLE)
{
}
VulkanShaderModule::~VulkanShaderModule()
{
	//Destroy();
}

bool VulkanShaderModule::Create(ShaderType::TYPE type, uint32 elementSize, const void* elementData)
{
	IGraphicShaderModule::Create(type, elementSize, elementData);

	SpvReflectResult result = SpvReflectResult::SPV_REFLECT_RESULT_SUCCESS;

	SpvReflectShaderModule shaderModule;
	result = spvReflectCreateShaderModule(m_ElementSize, m_ElementData, &shaderModule);
	if (result == SpvReflectResult::SPV_REFLECT_RESULT_SUCCESS)
	{
		// entryPoint
		for (uint32 index = 0; index < shaderModule.entry_point_count; index++)
		{
			SpvReflectEntryPoint& entryPoint = shaderModule.entry_points[index];
			m_EntryPoint = entryPoint.name;
		}

		// input
		// Sementic을 얻어올 수 없을까???? (https://lifeisforu.tistory.com/512)
		m_Outputs.Clear();
		m_Inputs.AddDefaulted(shaderModule.input_variable_count);
		for (uint32 index = 0; index < shaderModule.input_variable_count; index++)
		{
			SpvReflectInterfaceVariable& inputValue = shaderModule.input_variables[index];

			m_Inputs[index].name = inputValue.name;
			m_Inputs[index].location = inputValue.location;
			m_Inputs[index].sementic = inputValue.semantic;
		}

		// output
		m_Outputs.Clear();
		m_Outputs.AddDefaulted(shaderModule.output_variable_count);
		for (uint32 index = 0; index < shaderModule.output_variable_count; index++)
		{
			SpvReflectInterfaceVariable& outputValue = shaderModule.output_variables[index];

			m_Outputs[index].name = outputValue.name;
			m_Outputs[index].location = outputValue.location;
			m_Outputs[index].sementic = outputValue.semantic;
		}

		m_NumDescriptors = shaderModule.descriptor_binding_count;

		// descriptor binding
		//for (uint32 index = 0; index < shaderModule.descriptor_binding_count; index++)
		//{
		//	SpvReflectDescriptorBinding& descriptorBinding = shaderModule.descriptor_bindings[index];
		//	switch (descriptorBinding.descriptor_type)
		//	{
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLER: // = VK_DESCRIPTOR_TYPE_SAMPLER
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER: // = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLED_IMAGE: // = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_IMAGE: // = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER: // = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER: // = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER: // = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER: // = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC: // = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC: // = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC
		//		break;
		//	case SPV_REFLECT_DESCRIPTOR_TYPE_INPUT_ATTACHMENT: // = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT
		//		break;
		//	default:
		//		ASSERT(0);
		//		break;
		//	}
		//}

		// descriptor set
		for (uint32 index = 0; index < shaderModule.descriptor_set_count; index++)
		{
			SpvReflectDescriptorSet& descriptorSet = shaderModule.descriptor_sets[index];
			
			uint32 SetIndex = descriptorSet.set;

			for (SizeT descId = 0; descId < descriptorSet.binding_count; descId++)
			{
				SpvReflectDescriptorBinding* descriptorBinding = descriptorSet.bindings[descId];
				switch (descriptorBinding->descriptor_type)
				{
				case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLER: // =descriptorBindingVK_DESCRIPTOR_TYPE_SAMPLER
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER: // = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLED_IMAGE: // = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_IMAGE: // = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER: // = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER: // = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER: // = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER: // = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC: // = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC: // = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC
					break;
				case SPV_REFLECT_DESCRIPTOR_TYPE_INPUT_ATTACHMENT: // = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT
					break;
				default:
					ASSERT(0);
					break;
				}
				descriptorBinding->name;
			}
		}

		// push constant
		for (uint32 index = 0; index < shaderModule.push_constant_block_count; index++)
		{
			SpvReflectBlockVariable& pushConstant = shaderModule.push_constant_blocks[index];
			int a = 1;
		}

		//여기서 다시.....그냥 spirvReflect 객체를 가지고 있게 하는게 맞을 것 같은데;;;;;

		spvReflectDestroyShaderModule(&shaderModule);
	}

	// Create Shader Module
	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	createInfo.pCode = (uint32*)&m_ElementData[0];
	createInfo.codeSize = m_ElementSize;
	VK_RESULT(vkCreateShaderModule(GVulkanDevice->Handle(), &createInfo, nullptr, &m_ShaderModule));

	return true;
}

void VulkanShaderModule::Destroy()
{
	IGraphicShaderModule::Destroy();

	m_Inputs.Clear();
	m_Outputs.Clear();
	m_Images.Clear();
	m_Samplers.Clear();
	m_SampledImages.Clear();
	m_UniformBuffers.Clear();
	m_PushConstants.Clear();
	m_SubpassInputs.Clear();

	if (m_ShaderModule != VK_NULL_HANDLE)
	{
		vkDestroyShaderModule(GVulkanDevice->Handle(), m_ShaderModule, NULL);
	}
	m_ShaderModule = VK_NULL_HANDLE;
}

