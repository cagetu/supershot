#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanlayers.h"

#if VULKAN_HAS_DEBUGGING_ENABLED

#if VULKAN_ENABLE_DRAW_MARKERS
#define RENDERDOC_LAYER_NAME	"VK_LAYER_RENDERDOC_Capture"
#endif

#define VULKAN_ENABLE_STANDARD_VALIDATION	1

// intance에 대해서 활성화 되기를 원하는 유효한 layer의 리스트
static const char* GRequiredLayersInstance[] =
{
	"VK_LAYER_LUNARG_swapchain",
};

// instance에 대해서 활성화 되기를 원하는 유효한 layer의 리스트
static const char* GValidationLayersInstance[] =
{
#if VULKAN_ENABLE_API_DUMP
	"VK_LAYER_LUNARG_api_dump",
#endif

#if VULKAN_ENABLE_STANDARD_VALIDATION
	"VK_LAYER_LUNARG_standard_validation",
#else
	"VK_LAYER_GOOGLE_threading",
	"VK_LAYER_LUNARG_parameter_validation",
	"VK_LAYER_LUNARG_object_tracker",
	"VK_LAYER_LUNARG_image",
	"VK_LAYER_LUNARG_core_validation",
	"VK_LAYER_LUNARG_swapchain",
	"VK_LAYER_GOOGLE_unique_objects",
#endif

	"VK_LAYER_LUNARG_device_limits",
	//"VK_LAYER_LUNARG_screenshot",
	//"VK_LAYER_NV_optimus",
	//"VK_LAYER_LUNARG_vktrace",		// Useful for future
};

// Instance Extensions to enable
static const char* GInstanceExtensions[] =
{
	VK_KHR_SURFACE_EXTENSION_NAME,
#if TARGET_OS_ANDROID
	VK_KHR_ANDROID_SURFACE_EXTENSION_NAME,
#else
	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#endif
#if !VULKAN_DISABLE_DEBUG_CALLBACK
	VK_EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
};

struct LayerExtension
{
	VkLayerProperties LayerProps;
	VkExtensionProperties ExtensionProps[256];
	int32 NumExtensionProps;
};

// Instance 레벨 확장 지원여부
// LayerName이 null이면 모든 layer 대해서 
void GetInstanceLayerExtensions(const char* LayerName, LayerExtension& OutLayer)
{
	VkResult Result;
	do
	{
		// 3번째 인자를 null로 해서 total number 획득
		uint32 Count = 0;
		Result = ::vkEnumerateInstanceExtensionProperties(LayerName, &Count, nullptr);
		_ASSERT(Result >= VK_SUCCESS);

		// 3번째 인자를 저장할 포인트를 넘겨서 다시 호출
		OutLayer.NumExtensionProps = Count;
		Memory::MemZero(OutLayer.ExtensionProps);
		if (Count > 0)
		{
			Result = ::vkEnumerateInstanceExtensionProperties(LayerName, &Count, &OutLayer.ExtensionProps[0]);
			_ASSERT(Result >= VK_SUCCESS);
		}
	} while (Result == VK_INCOMPLETE);
}

void VulkanLayer::GetInstanceLayersAndExtensions(Array<const char*>& outInstanceExtensions, Array<const char*>& OutInstanceLayers)
{
	LayerExtension globalLayerExtensions;
	GetInstanceLayerExtensions(nullptr, globalLayerExtensions);

	VkResult Result;
	
	// Now per layer
	Array<LayerExtension> GlobalLayers;
	
	int32 TotalLayerCount = 0;
	VkLayerProperties GlobalLayerProperties[128];
	Memory::MemZero(GlobalLayerProperties, sizeof(GlobalLayerProperties));

	do
	{
		uint32 InstanceLayerCount = 0;
		Result = vkEnumerateInstanceLayerProperties(&InstanceLayerCount, nullptr);
		_ASSERT(Result >= VK_SUCCESS);
	
		if (InstanceLayerCount > 0)
		{
			Result = vkEnumerateInstanceLayerProperties(&InstanceLayerCount, &GlobalLayerProperties[TotalLayerCount]);
			_ASSERT(Result >= VK_SUCCESS);
		}
	
		TotalLayerCount += InstanceLayerCount;
	
	} while (Result == VK_INCOMPLETE);

	GlobalLayers.Clear(TotalLayerCount);
	for (int32 i = 0; i < TotalLayerCount; i++)
	{
		LayerExtension layer;
		layer.LayerProps = GlobalLayerProperties[i];
		GetInstanceLayerExtensions(GlobalLayerProperties[i].layerName, layer);
		GlobalLayers.Add(layer);
	}

#if VULKAN_HAS_DEBUGGING_ENABLED
	// Verify that all required instance layers are available
	for (uint32 LayerIndex = 0; LayerIndex < _countof(GRequiredLayersInstance); ++LayerIndex)
	{
		bool bValidationFound = false;
		const char* CurrValidationLayer = GRequiredLayersInstance[LayerIndex];
		for (uint32 Index = 0; Index < GlobalLayers.Num(); ++Index)
		{
			if (!::strcmp(GlobalLayers[Index].LayerProps.layerName, CurrValidationLayer))
			{
				bValidationFound = true;
				OutInstanceLayers.Add(CurrValidationLayer);
				break;
			}
		}

		if (!bValidationFound)
		{
			ULOG_UTF8(Log::_Debug, "Unable to find Vulkan Instance Required Layer: %s", CurrValidationLayer);
		}
	}

	// Verify that all requested debugging device-layers are available
	for (uint32 LayerIndex = 0; LayerIndex < _countof(GValidationLayersInstance); ++LayerIndex)
	{
		bool bValidationFound = false;
		const char* CurrValidationLayer = GValidationLayersInstance[LayerIndex];
		for (uint32 Index = 0; Index < GlobalLayers.Num(); ++Index)
		{
			if (!::strcmp(GlobalLayers[Index].LayerProps.layerName, CurrValidationLayer))
			{
				bValidationFound = true;
				OutInstanceLayers.Add(CurrValidationLayer);
				break;
			}
		}

		if (!bValidationFound)
		{
			ULOG_UTF8(Log::_Debug, "Unable to find Vulkan Instance validation Layer: %s", CurrValidationLayer);
		}
	}
#endif	// VULKAN_HAS_DEBUGGING_ENABLED

	for (int32 i = 0; i < globalLayerExtensions.NumExtensionProps; i++)
	{
		for (int32 j = 0; j < _countof(GInstanceExtensions); j++)
		{
			if (!::strcmp(globalLayerExtensions.ExtensionProps[i].extensionName, GInstanceExtensions[j]))
			{
				outInstanceExtensions.Add(GInstanceExtensions[j]);
				break;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////

	if (outInstanceExtensions.Num() > 0)
	{
		ULOG(Log::_Debug, "Using Instance Extenstions.");
		for (const char* Extension : outInstanceExtensions)
		{
			ULOG_UTF8(Log::_Debug, "* %s", Extension);
		}
	}

	if (OutInstanceLayers.Num() > 0)
	{
		ULOG(Log::_Debug, "Using Instance layers.");
		for (const char* Layer : OutInstanceLayers)
		{
			ULOG_UTF8(Log::_Debug, "* %s", Layer);
		}
	}

	outInstanceExtensions.Shrink();
	OutInstanceLayers.Shrink();
}

//*********************************************************************************************************
//
//*********************************************************************************************************

// List of validation layers which we want to activate for the device
static const char* GRequiredLayersDevice[] =
{
	"VK_LAYER_LUNARG_swapchain",
};

// List of validation layers which we want to activate for the device
static const char* GValidationLayersDevice[] =
{
#if VULKAN_ENABLE_API_DUMP
	"VK_LAYER_LUNARG_api_dump",
#endif

#if VULKAN_ENABLE_STANDARD_VALIDATION
	"VK_LAYER_LUNARG_standard_validation",
#else
	"VK_LAYER_GOOGLE_threading",
	"VK_LAYER_LUNARG_parameter_validation",
	"VK_LAYER_LUNARG_object_tracker",
	"VK_LAYER_LUNARG_image",
	"VK_LAYER_LUNARG_core_validation",
	"VK_LAYER_LUNARG_swapchain",
	"VK_LAYER_GOOGLE_unique_objects",
#endif

	"VK_LAYER_LUNARG_device_limits",
	//"VK_LAYER_LUNARG_screenshot",
	//"VK_LAYER_NV_optimus",
	//"VK_LAYER_LUNARG_vktrace",		// Useful for future
};
#endif // VULKAN_HAS_DEBUGGING_ENABLED


// Device Extensions to enable
static const char* GDeviceExtensions[] =
{
	//	VK_KHR_SURFACE_EXTENSION_NAME,			// Not supported, even if it's reported as a valid extension... (SDK/driver bug?)
#if TARGET_OS_ANDROID
	VK_KHR_SURFACE_EXTENSION_NAME,
	VK_KHR_ANDROID_SURFACE_EXTENSION_NAME,
#else
	//	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,	// Not supported, even if it's reported as a valid extension... (SDK/driver bug?)
#endif
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};

// Device 레벨 확장 지원여부
// LayerName이 null이면 모든 layer 대해서 
static inline void GetDeviceLayerExtensions(VkPhysicalDevice Device, const char* LayerName, LayerExtension& OutLayer)
{
	VkResult Result;
	//do
	//{
		// 3번째 인자를 null로 해서 total number 획득
		uint32 Count = 0;
		Result = vkEnumerateDeviceExtensionProperties(Device, LayerName, &Count, nullptr);
		_ASSERT(Result >= VK_SUCCESS);
		_ASSERT(Count > 0);

		// 3번째 인자를 저장할 포인트를 넘겨서 다시 호출
		Memory::MemZero(OutLayer.ExtensionProps);
		if (Count > 0)
		{
			Result = vkEnumerateDeviceExtensionProperties(Device, LayerName, &Count, &OutLayer.ExtensionProps[0]);
			_ASSERT(Result >= VK_SUCCESS);
		}

		OutLayer.NumExtensionProps = Count;
	//} while (Result == VK_INCOMPLETE);
}

void VulkanLayer::GetDeviceLayerAndExtensions(VkPhysicalDevice InDevice, Array<const char*>& outDeviceExtensions, Array<const char*>& outDeviceLayers, bool& outDebugMarkers)
{
	// Setup device layer properties
	uint32 Count = 0;
	::vkEnumerateDeviceLayerProperties(InDevice, &Count, nullptr);

	Array<VkLayerProperties> LayerProperties(Count);
	::vkEnumerateDeviceLayerProperties(InDevice, &Count, LayerProperties.Data());

#if VULKAN_HAS_DEBUGGING_ENABLED

	bool bRenderDocFound = false;
#if VULKAN_ENABLE_DRAW_MARKERS
	bool bDebugExtMarkerFound = false;
	for (uint32 Index = 0; Index < LayerProperties.Num(); ++Index)
	{
		if (!::strcmp(LayerProperties[Index].layerName, RENDERDOC_LAYER_NAME))
		{
			bRenderDocFound = true;
			break;
		}
		else if (!::strcmp(LayerProperties[Index].layerName, VK_EXT_DEBUG_MARKER_EXTENSION_NAME))
		{
			bDebugExtMarkerFound = true;
			break;
		}
	}
#endif


	// Verify that all required device layers are available
	for (uint32 LayerIndex = 0; LayerIndex < _countof(GRequiredLayersDevice); ++LayerIndex)
	{
		bool bValidationFound = false;
		const char* CurrValidationLayer = GRequiredLayersDevice[LayerIndex];
		for (uint32 Index = 0; Index < LayerProperties.Num(); ++Index)
		{
			if (!::strcmp(LayerProperties[Index].layerName, CurrValidationLayer))
			{
				bValidationFound = true;
				outDeviceLayers.Add(CurrValidationLayer);
				break;
			}
		}
		if (!bValidationFound)
		{
			ULOG_UTF8(Log::_Debug, "Unable to find Vulkan device Layer: %s", CurrValidationLayer);
		}
	}

	// Verify that all requested debugging device-layers are available. Skip validation layers under RenderDoc
	if (!bRenderDocFound)
	{
		for (uint32 LayerIndex = 0; LayerIndex < _countof(GValidationLayersDevice); ++LayerIndex)
		{
			bool bValidationFound = false;
			const char* CurrValidationLayer = GValidationLayersDevice[LayerIndex];
			for (uint32 Index = 0; Index < LayerProperties.Num(); ++Index)
			{
				if (!::strcmp(LayerProperties[Index].layerName, CurrValidationLayer))
				{
					bValidationFound = true;
					outDeviceLayers.Add(CurrValidationLayer);
					break;
				}
			}

			if (!bValidationFound)
			{
				ULOG_UTF8(Log::_Debug, "Unable to find Vulkan validation device Layer: %s", CurrValidationLayer);
			}
		}
	}
#endif

	LayerExtension Extensions;
	//Memory::MemZero(Extensions);
	GetDeviceLayerExtensions(InDevice, nullptr, Extensions);

	for (uint32 Index = 0; Index < _countof(GDeviceExtensions); ++Index)
	{
		for (int32 i = 0; i < Extensions.NumExtensionProps; i++)
		{
			if (!::strcmp(GDeviceExtensions[Index], Extensions.ExtensionProps[i].extensionName))
			{
				outDeviceExtensions.Add(GDeviceExtensions[Index]);
				break;
			}
		}
	}

#if VULKAN_ENABLE_DRAW_MARKERS
	if (bRenderDocFound)
	{
		for (int32 i = 0; i < Extensions.NumExtensionProps; i++)
		{
			if (!::strcmp(Extensions.ExtensionProps[i].extensionName, VK_EXT_DEBUG_MARKER_EXTENSION_NAME))
			{
				outDeviceExtensions.Add(VK_EXT_DEBUG_MARKER_EXTENSION_NAME);
				outDebugMarkers = true;
				break;
			}
		}
	}
#endif

	outDeviceExtensions.Shrink();
	outDeviceLayers.Shrink();

	//////////////////////////////////////////////////////////////////////

	if (outDeviceExtensions.Num() > 0)
	{
		ULOG(Log::_Debug, "Using Device Extenstions.");
		for (const char* Extension : outDeviceExtensions)
		{
			ULOG_UTF8(Log::_Debug, "* %s", Extension);
		}
	}

	if (outDeviceLayers.Num() > 0)
	{
		ULOG(Log::_Debug, "Using Device layers.");
		for (const char* Layer : outDeviceLayers)
		{
			ULOG_UTF8(Log::_Debug, "* %s", Layer);
		}
	}
}
