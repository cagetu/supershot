#include "foundation.h"
#include "graphic.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkandriver.h"
#include "Vulkan/vulkanswapchain.h"
#include "Vulkan/vulkansemaphore.h"
#include "Vulkan/vulkantexture.h"
#include "Vulkan/vulkanrenderpass.h"
#include "Vulkan/vulkanframebuffer.h"
#include "Vulkan/vulkanrendertarget.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkancommandlist.h"
#include "Vulkan/vulkanrenderwindow.h"
#include "graphicsettings.h"

#define _TEST_FRAMEBUFFER 1

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanRenderWindow, IGraphicRenderWindow);

VulkanRenderWindow::VulkanRenderWindow()
	: m_SwapChain(nullptr)
	, m_DepthStencilSurface(nullptr)
	, m_ResolveSurface(nullptr)
	, m_GfxCommandQueue(nullptr)
	, m_CommandPool(nullptr)
	, m_FrameBuffers(nullptr)
	, m_AcquiredSwapIndex(0)
	, m_PresentCompleteSemaphore(nullptr)
{
	for (int32 index = 0; index < _NUM_SWAP_BUFFERS; index++)
	{
		m_CommandLists[index] = nullptr;
		m_RenderCompleteSemaphores[index] = nullptr;
	}
}

VulkanRenderWindow::~VulkanRenderWindow()
{
	Destroy();
}

bool VulkanRenderWindow::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, bool bUseDepthSurface)
{
	if (!IGraphicRenderWindow::Create(windowHandle, format, bUseDepthSurface))
		return false;

	if (!CreateCommandQueue())
		return false;

	VulkanSwapchain* swapchain = new VulkanSwapchain;
	if (!swapchain->Create(windowHandle, format, m_GfxCommandQueue, _NUM_SWAP_BUFFERS))
		return false;

	m_SwapChain = swapchain;

	CreateCommandLists(_NUM_SWAP_BUFFERS);

#pragma message("TODO: DepthSurface create!!!!!")
	if (bUseDepthSurface)
	{
		CreateDepthSurface(windowHandle.Width(), windowHandle.Height(), FMT_D24S8);
	}

	CreateResolveSurface(windowHandle.Width(), windowHandle.Height(), format);

	CreateFrameBuffers(_NUM_SWAP_BUFFERS);
	return true;
}

void VulkanRenderWindow::Destroy()
{
	DestroyResolveSurface();
	DestroyDepthSurface();
	DestroyCommandLists();
	DestroyCommandQueue();

	DeleteObjectArray(m_FrameBuffers, _NUM_SWAP_BUFFERS);

	//DeleteObject(m_SwapChain);
	m_SwapChain = nullptr;
}

bool VulkanRenderWindow::CreateDepthSurface(uint32 width, uint32 height, FORMAT_TYPE format)
{
	m_DepthStencilSurface = NewObject(VulkanDepthStencilRenderTexture);
	if (m_DepthStencilSurface->Create(width, height, format, 1, GraphicSettings::Get().MultiSampleType) == false)
	{
		DeleteObject(m_DepthStencilSurface);
		return false;
	}

	m_DepthStencilSurface->ImageLayout().Reset();
	return true;
}

void VulkanRenderWindow::DestroyDepthSurface()
{
	DeleteObject(m_DepthStencilSurface);
}

bool VulkanRenderWindow::CreateResolveSurface(uint32 width, uint32 height, FORMAT_TYPE format)
{
	m_ResolveSurface = new VulkanResolveRenderTexture();
	if (m_ResolveSurface->Create(width, height, format, 1, GraphicSettings::Get().MultiSampleType) == false)
	{
		DeleteObject(m_ResolveSurface);
		return false;
	}

	return true;
}

void VulkanRenderWindow::DestroyResolveSurface()
{
	DeleteObject(m_ResolveSurface);
}

void VulkanRenderWindow::Resize(const WindowHandle& windowHandle)
{
	_ASSERT(windowHandle.Width() > 0);
	_ASSERT(windowHandle.Height() > 0);

	if (Width() != windowHandle.Width() || Height() != windowHandle.Height())
	{
		VulkanDriver* driver = Gfx::Driver()->DynamicCast<VulkanDriver>();
		ASSERT(driver != nullptr);

		driver->Device()->WaitForIdle();

		// 스왑체인 재생성
		m_WindowHandle = windowHandle;
		m_SwapChain->Resize(windowHandle);

		// 깊이 버퍼 재성성

		if (HasDepthSurface())
		{
			CreateDepthSurface(windowHandle.Width(), windowHandle.Height());
		}

		// 프레임버퍼 재생성
	}
}

bool VulkanRenderWindow::CreateCommandQueue()
{
	m_GfxCommandQueue = GVulkanDevice->CreateCommandQueue(COMMANDLIST_TYPE::DIRECT, true);
	if (m_GfxCommandQueue == nullptr)
		return false;

	return true;
}

void VulkanRenderWindow::DestroyCommandQueue()
{
	if (m_GfxCommandQueue && GVulkanDevice)
	{
		m_GfxCommandQueue->WaitForIdle();
		GVulkanDevice->DestroyCommandQueue(m_GfxCommandQueue);
		m_GfxCommandQueue = nullptr;
	}
}

bool VulkanRenderWindow::CreateCommandLists(int32 requireBufferCount)
{
	m_CommandPool = GVulkanDevice->CreateCommandPool(m_GfxCommandQueue->GetCommandListType(), m_GfxCommandQueue->FamilyIndex(), Vk::CommandPoolCreateFlag::_RESET_COMMAND_BUFFER_BIT);

	for (int32 index = 0; index < requireBufferCount; index++)
	{
		m_CommandLists[index] = m_CommandPool->CreateCommandList();
		m_RenderCompleteSemaphores[index] = new VulkanSemaphore();
	}
	return true;
}

void VulkanRenderWindow::DestroyCommandLists()
{
	for (int32 index = 0; index < _NUM_SWAP_BUFFERS; index++)
	{
		DeleteObject(m_RenderCompleteSemaphores[index]);
	}

	if (m_CommandPool)
	{
		for (int32 index = 0; index < _NUM_SWAP_BUFFERS; index++)
		{
			m_CommandPool->DestroyCommandList(m_CommandLists[index]);
			m_CommandLists[index] = nullptr;
		}

		GVulkanDevice->DestroyCommandPool(m_CommandPool);
		m_CommandPool = nullptr;
	}
}

bool VulkanRenderWindow::CreateFrameBuffers(int32 requireBufferCount)
{
	m_FrameBuffers = NewObjectArray(VulkanFrameBuffer, requireBufferCount);
	return true;
}

void VulkanRenderWindow::CreateRenderPass(VulkanFrameBuffer& currentFrameBuffer, int32 clearFlags, const LinearColor& clearColor, float clearDepth, ushort clearStencil)
{
	Vk::RenderPassLayout renderpassLayout;
	renderpassLayout.Width = m_WindowHandle.Width();
	renderpassLayout.Height = m_WindowHandle.Height();

	Vk::SubPassLayout subpassLayout;
	{
		// Color Attachment
		VulkanColorRenderTexture* colorSurface = m_SwapChain->GetBackBuffer(m_AcquiredSwapIndex);
		if (colorSurface && colorSurface->ImageView())
		{
			if (colorSurface->ImageView()->IsValid())
			{
				//colorSurface.ColorLoadStore().load = (clearFlags&CLEAR_COLOR) ? RT_LOAD_TYPE::Clear : RT_LOAD_TYPE::None;
				//colorSurface.ColorLoadStore().store = RT_STORE_TYPE::Store;
				//colorSurface.ClearColor() = clearColor;
				//colorSurface.ImageLayout().initLayout = VK_IMAGE_LAYOUT_UNDEFINED;
				//colorSurface.ImageLayout().finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;

				Vk::AttachmentOp colorOp((clearFlags & CLEAR_COLOR) ? RT_LOAD_TYPE::Clear : RT_LOAD_TYPE::None, RT_STORE_TYPE::Store);
				Vk::ImageLayoutOp colorLayout(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

				subpassLayout.AddColorAttachment(colorSurface->ImageView()->Handle(),
					colorSurface->Format(),
					colorSurface->Multisamples(),
					colorLayout, clearColor, colorOp);
			}
		}

		// Depth Attachment
		if (m_DepthStencilSurface)
		{
			//m_DepthStencilSurface->DepthLoadStore().load = (clearFlags&CLEAR_DEPTH) ? RT_LOAD_TYPE::Clear : RT_LOAD_TYPE::None;
			//m_DepthStencilSurface->DepthLoadStore().store = RT_STORE_TYPE::Store;
			//m_DepthStencilSurface->StencilLoadStore().load = (clearFlags&CLEAR_STENCIL) ? RT_LOAD_TYPE::Clear : RT_LOAD_TYPE::None;;
			//m_DepthStencilSurface->StencilLoadStore().store = RT_STORE_TYPE::Store;
			//m_DepthStencilSurface->ClearDepth() = clearDepth;
			//m_DepthStencilSurface->ClearStencil() = clearStencil;

			Vk::AttachmentOp depthOp((clearFlags&CLEAR_DEPTH) ? RT_LOAD_TYPE::Clear : RT_LOAD_TYPE::None, RT_STORE_TYPE::Store);
			Vk::AttachmentOp stencilOp((clearFlags&CLEAR_STENCIL) ? RT_LOAD_TYPE::Clear : RT_LOAD_TYPE::None, RT_STORE_TYPE::Store);
			Vk::ImageLayoutOp depthLayout(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

			subpassLayout.AddDepthStencilAttachment(m_DepthStencilSurface->ImageView()->Handle(),
				m_DepthStencilSurface->Format(),
				m_DepthStencilSurface->Multisamples(),
				depthLayout, depthOp, clearDepth, stencilOp, clearStencil);
		}

		// Resolve Attachment
		if (m_ResolveSurface)
		{
			Vk::AttachmentOp resolveOp(RT_LOAD_TYPE::None, RT_STORE_TYPE::Store);
			Vk::ImageLayoutOp resolveLayout(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);

			subpassLayout.AddResolveAttachment(m_ResolveSurface->ImageView()->Handle(),
				m_ResolveSurface->Format(),
				m_ResolveSurface->Multisamples(),
				resolveLayout, resolveOp);
		}
	}
	renderpassLayout.AddSubPass(subpassLayout);

	// renderpass
	VulkanRenderPass* renderPass = NewObject(VulkanRenderPass);
	renderPass->Create(renderpassLayout);

	// frameBuffer
	currentFrameBuffer.Create(renderPass);

}

void VulkanRenderWindow::BlockingSwapBuffers()
{
	m_CommandLists[m_PresentCompleteSemaphoreIndex]->GetState();
}

void VulkanRenderWindow::BeginFrame(int32 clearFlags, const LinearColor& clearColor, float clearDepth, ushort clearStencil)
{
	/*	[TODO]
		AttachmentManager.Register("BackBuffer", surface)
		AttachmentManager.Register("DepthStencil", depth);

		subPassLayout.AttachColor("BackBuffer")
		subPassLayout.AttachDepth("Depth")

		1. 매 프레임마다 Framebuffer를 계속 생성해주는가?
		2. FrameBuffer를 계속 캐싱해서 사용하는가???
	*/

	// 새로운 Swapchaing 이미지를 얻는다.
	VULKAN::_RESULT hr = m_SwapChain->GetNextBackBufferIndex(m_AcquiredSwapIndex, &m_PresentCompleteSemaphore, m_PresentCompleteSemaphoreIndex);
	_ASSERT(hr == VULKAN::_RESULT::_SUCCESS);

#if _TEST_FRAMEBUFFER
	// Color Attachment
	VulkanFrameBuffer& currentFrameBuffer = m_FrameBuffers[m_AcquiredSwapIndex];
	if (currentFrameBuffer.Handle() == VK_NULL_HANDLE)
	{
		CreateRenderPass(currentFrameBuffer, clearFlags, clearColor, clearDepth, clearStencil);
	}

	// Begin Record CommandBuffer
	VulkanCommandList* commandBuffer = m_CommandLists[m_AcquiredSwapIndex];
	if (commandBuffer)
	{
		// 다시 사용할 수 있게 되기 전에 command buffer의 실행이 완전히 끝날 때까지 대기한다.
		commandBuffer->WaitForIdle();

		commandBuffer->BeginRecord();
		commandBuffer->BeginRenderPass(&currentFrameBuffer);
		/*
			m_CommandBuffer->SetViewport(width, height);
			m_CommandBuffer->SetScissor(width, height);
		*/
	}
#endif
}

void VulkanRenderWindow::EndFrame()
{
	// End Record CommandBuffer

#if _TEST_FRAMEBUFFER
	VulkanCommandList* commandBuffer = m_CommandLists[m_AcquiredSwapIndex];
	if (commandBuffer)
	{
		commandBuffer->EndRenderPass();

#pragma message("Please Research [Barrier]")

		// 이 이미지를 Color Atttachment Layout -> Presnet Layout으로 변경요청한다.
		//VulkanColorRenderTexture& colorSurface = m_SwapChain->GetBackBuffer(m_AcquiredSwapIndex);
		//commandBuffer->SetImageBarrier(colorSurface.Image()->Handle(), VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
#if 0
		{
			// Transition our swap image to present.
			// Do this instead of having the renderpass do the transition
			// so we can take advantage of the general layout to avoid 
			// additional image barriers.
			VkImageMemoryBarrier barrier = {};
			barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.image = m_swapchainImages[m_currentSwapIndex];
			barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			barrier.subresourceRange.baseMipLevel = 0;
			barrier.subresourceRange.levelCount = 1;
			barrier.subresourceRange.baseArrayLayer = 0;
			barrier.subresourceRange.layerCount = 1;
			barrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			barrier.dstAccessMask = 0;

			vkCmdPipelineBarrier(
				commandBuffer,
				VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
				0, 0, nullptr, 0, nullptr, 1, &barrier);
		}
#endif

		commandBuffer->EndRecord();
	}

#endif
	//commandBuffer->Submit()
}

void VulkanRenderWindow::Present()
{
#if _TEST_FRAMEBUFFER
	// CommandBuffer 제출 (이건 EndFrame에서 해야 하는걸까??)
	if (VulkanCommandList* commandBuffer = m_CommandLists[m_AcquiredSwapIndex])
	{
		VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		// 완료되면 보내지는 시그널
		VulkanSemaphore* renderCompleteSemaphore = m_RenderCompleteSemaphores[m_AcquiredSwapIndex];
		commandBuffer->Submit(m_GfxCommandQueue, renderCompleteSemaphore, m_PresentCompleteSemaphore, dstStageMask, false);
	}
#endif

	// SwapBuffer
	m_SwapChain->Present(m_GfxCommandQueue, m_RenderCompleteSemaphores[m_AcquiredSwapIndex]);
}
