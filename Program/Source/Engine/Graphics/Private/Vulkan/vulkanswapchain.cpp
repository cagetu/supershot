#include "foundation.h"
#include "graphic.h"
#include "Vulkan/vulkandriver.h"
#include "Vulkan/vulkansemaphore.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkantexture.h"
#include "Vulkan/vulkanadapter.h"
#include "Vulkan/vulkanswapchain.h"

#define _MAX_SURFACES 3

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanSwapchain, IGraphicSwapchain);

VulkanSwapchain::VulkanSwapchain()
	: m_SwapChainHandle(VK_NULL_HANDLE)
	, m_CurrentBackBufferIndex(-1)
	, m_CurrentSemaphoreIndex(-1)
	, m_ImageAcquiredWaitSemaphores(nullptr)
{
}
VulkanSwapchain::~VulkanSwapchain()
{
	Destroy();
}


VkSurfaceFormatKHR GetSurfaceFormat(uint32 NumFormats, VkSurfaceFormatKHR* Formats, VkFormat DesireFormat)
{
	static struct _SURFACEFORMAT
	{
		VkFormat format;
		const char* name;
	} GSurfaceFormats[] =
	{
		VK_FORMAT_UNDEFINED, "VK_FORMAT_UNDEFINED",
		VK_FORMAT_R8G8B8A8_UNORM, "VK_FORMAT_R8G8B8A8_UNORM",
		VK_FORMAT_R8G8B8A8_SRGB, "VK_FORMAT_R8G8B8A8_SRGB",
		VK_FORMAT_B8G8R8A8_UNORM, "VK_FORMAT_B8G8R8A8_UNORM",
		VK_FORMAT_B8G8R8A8_SRGB, "VK_FORMAT_B8G8R8A8_SRGB",
	};

	for (uint32 index = 0; index < NumFormats; index++)
	{
		for (int32 j = 0; j < _countof(GSurfaceFormats); j++)
		{
			if (Formats[index].format == GSurfaceFormats[j].format)
			{
				ULOG_UTF8(Log::_Debug, "%d: %s", index, GSurfaceFormats[j].name);
				break;
			}
		}
	}

	if (NumFormats == 1 && Formats[0].format == VK_FORMAT_UNDEFINED)
	{
		return{ VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for (uint32 index = 0; index < NumFormats; index++)
	{
		if (Formats[index].format == DesireFormat)
			return Formats[index];
	}

	return Formats[0];
}

VkPresentModeKHR GetSurfacePresentMode(uint32 NumSurfacePresentModes, VkPresentModeKHR modes[], VkPresentModeKHR DesireMode)
{
	for (uint32 Index = 0; Index < NumSurfacePresentModes; Index++)
	{
		if (modes[Index] == DesireMode)
			return DesireMode;
	}

	ULOG(Log::_Debug, "%d PresentMode is not Support!! Select %d Mode\n", DesireMode, modes[0]);
	return modes[0];
}

VkSurfaceTransformFlagBitsKHR GetSurfaceTransform(VkSurfaceCapabilitiesKHR& surfaceCaps)
{
	if (surfaceCaps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
	{
		return VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	}
	else
	{
		return surfaceCaps.currentTransform;
	}
}

// 이미지를 생성할 때 이미지의 처리 목적을 지정해야 한다.
VkImageUsageFlags GetSurfaceImageUsage()
{
	/*
		VK_IMAGE_USAGE_TRANSFER_SRC_BIT
			- 이미지가 Transfer Command의 Source로 사용될 수 있다.
		VK_IMAGE_USAGE_TRANSFER_DST_BIT
			- 이미지가 Transfer Command의 Destination로 사용될 수 있다.
		VK_IMAGE_USAGE_SAMPLED_BIT
			- 이미지가 VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE 혹은 VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER로 VkDescriptorSet slot을 사용하기 위해 적합한 VkImageView 생성에 사용되고, 셰이더에 의해 샘플 된다.
		VK_IMAGE_USAGE_STORAGE_BIT
			- 이미지가 VK_DESCRIPTOR_TYPE_STORAGE_IMAGE VkDescriptorSet slot 타입을 사용하기에 VkImageView를 생성하는데 사용된다.
		VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
			- 이미지가 Color 로서 사용하기 위해 VkImageView를 생성하거나 VkFrameBuffer에 attachment를 resolve할 수 있다.
		VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
			- 이미지가 VkFrameBuffer에 Depth/Stencil attachment로서 사용하기에 적합한 VkImageView를 생성하는데 사용된다.
		VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT
			- VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT 를 가지고 할당한 이미지에 메모리를 바운드 한다.
			- (color, resolve, depth/stencil, input attachment)로 사용하기에 적합한 VkImageView 생성되는 이미지에 대해서 설정될 수 있다.
		VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT
			- 이미지가 VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT VkDescriptorSet slot 타입을 사용하기에 적합한 VkImageView를 생성하는데 사용된다.
			- input attachment로 셰이더로부터 읽어진다.
			- framebuffer에서 input attachment로서 사용된다.
	*/
	return VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
}

bool VulkanSwapchain::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, IGraphicCommandQueue* commandQueue, uint32 surfaceCount)
{
	if (false == CreateSwapChain(windowHandle, commandQueue, format, surfaceCount))
		return false;

	if (false == CreateBackBuffers(windowHandle.Width(), windowHandle.Height(), format))
		return false;

	m_BackBufferFormat = format;
	return true;
}

void VulkanSwapchain::Destroy()
{
	for (SizeT i = 0; i < m_BackBuffers.Num(); i++)
	{
		delete m_BackBuffers[i];
	}
	m_BackBuffers.Clear();

	if (m_ImageAcquiredWaitSemaphores)
	{
		delete[] m_ImageAcquiredWaitSemaphores;
	}
	m_ImageAcquiredWaitSemaphores = nullptr;
	m_CurrentBackBufferIndex = -1;
	m_CurrentSemaphoreIndex = -1;

	if (m_SwapChainHandle != VK_NULL_HANDLE)
	{
		vkDestroySwapchainKHR(GVulkanDevice->Handle(), m_SwapChainHandle, NULL);
		m_SwapChainHandle = VK_NULL_HANDLE;
	}
}

bool VulkanSwapchain::Resize(const WindowHandle& windowHandle)
{
	// RE-CREATE SWAPCHAIN!!!
	return true;
}

VulkanColorRenderTexture* VulkanSwapchain::GetBackBuffer(uint32 index) const
{
	if (m_BackBuffers.IsValidIndex(index))
	{
		return m_BackBuffers[index];
	}
	return nullptr;
}

bool VulkanSwapchain::CreateSwapChain(const WindowHandle& windowHandle, IGraphicCommandQueue* commandQueue, FORMAT_TYPE format, uint32 surfaceCount)
{
	VulkanDriver* driver = Gfx::Driver()->DynamicCast<VulkanDriver>();
	VulkanDevice* device = driver->Device();
	VulkanPhysicalDevice* physicalDevice = device->PhysicalDevice();

	// 1. Swapchain Format 설정
	VkSurfaceFormatKHR surfaceFormat = { VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	surfaceFormat = GetSurfaceFormat(physicalDevice->NumSurfaceFormats, physicalDevice->SurfaceFormats, VULKAN::ConvertFormat(format));

	// 2. Present Mode 설정
	VkPresentModeKHR PresentMode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
	PresentMode = GetSurfacePresentMode(physicalDevice->NumSurfacePresentModes, physicalDevice->SurfacePresentModes, PresentMode);

	// 3. Swapchaing Capacity 체크
	VkSurfaceCapabilitiesKHR& surfaceCaps = physicalDevice->SurfaceCapabilities;

	// 4. Swapchain 생성
	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	createInfo.surface = driver->Surface();
	createInfo.imageUsage = GetSurfaceImageUsage();		//! 이미지를 생성할 때 이미지의 처리 목적을 지정해야 한다.
	createInfo.minImageCount = Math::Clamp(surfaceCount, surfaceCaps.minImageCount, surfaceCaps.maxImageCount);
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageArrayLayers = 1;
	createInfo.imageExtent.width = windowHandle.Width(); // (surfaceCaps.currentExtent.width == -1 ? windowHandle.Width() : surfaceCaps.currentExtent.width);
	createInfo.imageExtent.height = windowHandle.Height(); // (surfaceCaps.currentExtent.height == -1 ? windowHandle.Height() : surfaceCaps.currentExtent.height);
	createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;	// 오직 하나의 Queue Family가 한번에 하나의 Image를 사용할 때, "exclusive" sharing mode를 사용한다.
	//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
	//createInfo.queueFamilyIndexCount = 0;
	//createInfo.pQueueFamilyIndices = nullptr;
	createInfo.preTransform = GetSurfaceTransform(surfaceCaps);
	createInfo.presentMode = PresentMode;
	createInfo.oldSwapchain = VK_NULL_HANDLE;
	createInfo.clipped = VK_TRUE;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	VkResult res = ::vkCreateSwapchainKHR(device->Handle(), &createInfo, nullptr, &m_SwapChainHandle);

	return (res == VK_SUCCESS) ? true : false;;
}

void VulkanSwapchain::DestroySwapChain()
{
}

bool VulkanSwapchain::CreateBackBuffers(int32 width, int32 height, FORMAT_TYPE format)
{
	VulkanDriver* driver = Gfx::Driver()->DynamicCast<VulkanDriver>();
	VulkanDevice* device = driver->Device();

	// 5. Swapchain Image를 획득한다. (이건 이미 메모리 할당까지 되어 있는 이미지 객체이다.)
	VK_RESULT(::vkGetSwapchainImagesKHR(device->Handle(), m_SwapChainHandle, &m_BackBufferCount, nullptr));
	CHECK(m_BackBufferCount <= _MAX_SURFACES, TEXT("m_BackBufferCount <= _MAX_SURFACES"));

	// 6. Image Surface 생성
	VkImage imageHandles[_MAX_SURFACES];
	VK_RESULT(::vkGetSwapchainImagesKHR(device->Handle(), m_SwapChainHandle, &m_BackBufferCount, &imageHandles[0]));

	for (uint32 index = 0; index < m_BackBufferCount; index++)
	{
		VulkanColorRenderTexture* backBuffer = NewObject(VulkanColorRenderTexture);
		if (backBuffer->Create(imageHandles[index], width, height, format))
		{
			m_BackBuffers.Add(backBuffer);
		}
		else
		{
			DeleteObject(backBuffer);
		}
	}

	// 7. Image Semaphore 생성
	m_ImageAcquiredWaitSemaphores = new VulkanSemaphore[m_BackBufferCount];
	return true;
}

void VulkanSwapchain::DestroyBackBuffers()
{
}

//--------------------------------------------------------------------------------------------------------
/*	- 우리가 그릴려고 하는 다음 Swapchain Image의 Index를 얻는다.
	- "infinite" timeout으로 설정하면, Image가 준비될 때까지 Block될 것이다.
	-  Semaphore는 Image가 준비될 때 Signal을 얻어올 것이다.
*/
//--------------------------------------------------------------------------------------------------------
VULKAN::_RESULT VulkanSwapchain::GetNextBackBufferIndex(int32& acquiredImageIndex, VulkanSemaphore** acquiredSemaphore, int32& acquiredSemaphoreIndex)
{
	uint32 imageIndex = 0;

	const int32 prevSemaphoreIndex = m_CurrentSemaphoreIndex;
	m_CurrentSemaphoreIndex = (m_CurrentSemaphoreIndex + 1) % m_BackBufferCount;

	VkResult result = ::vkAcquireNextImageKHR(GVulkanDevice->Handle(),
											  m_SwapChainHandle,
											  UINT64_MAX,														// TIMEOUT = 무한대!!!
											  m_ImageAcquiredWaitSemaphores[m_CurrentSemaphoreIndex].Handle(),	// Presention Engine이 이미지 사용을 끝냈을 때 Signal된다. Image를 획득할 수 있을 때까지 대기
											  VK_NULL_HANDLE,													// Fence
											  &imageIndex);
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		m_CurrentSemaphoreIndex = prevSemaphoreIndex;
		return VULKAN::_RESULT::_OUT_OF_DATE;
	}
	if (result == VK_ERROR_SURFACE_LOST_KHR)
	{
		m_CurrentSemaphoreIndex = prevSemaphoreIndex;
		return VULKAN::_RESULT::_SURFACE_LOST;
	}

	m_CurrentBackBufferIndex = (int32)imageIndex;

	acquiredImageIndex = m_CurrentBackBufferIndex;
	acquiredSemaphoreIndex = m_CurrentSemaphoreIndex;
	*acquiredSemaphore = &m_ImageAcquiredWaitSemaphores[m_CurrentSemaphoreIndex];

	return VULKAN::_RESULT::_SUCCESS;
}

VULKAN::_RESULT VulkanSwapchain::Present(VulkanCommandQueue* presentQueue, VulkanSemaphore* renderCompleteSignal, bool bWaitForIdle)
{
	if (m_CurrentBackBufferIndex == -1)
	{
		return VULKAN::_RESULT::_SUCCESS;
	}

	VkPresentInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	// Present가 발생할 수 있을 때까지 기다린다.
	if (renderCompleteSignal)
	{
		info.waitSemaphoreCount = 1;								// 이미지를 Present 하기 전에 우리가 원하는 대기하는 Queue Semaphore의 수
		info.pWaitSemaphores = renderCompleteSignal->HandlePtr();	// Queue가 기다려야 하는 Semaphore 핸들 Array
	}
	info.swapchainCount = 1;
	info.pSwapchains = &m_SwapChainHandle;							// Image들을 Present하기를 원하는 모든 Swapchain의 핸들 Array.
	info.pImageIndices = (uint32*)&m_CurrentBackBufferIndex;		// Present하기를 원하는 Image들의 인덱스를 포함한 Array. Index는 pSwapChain의 Array에 대응되는 인덱스이다.

	VkResult result = ::vkQueuePresentKHR(presentQueue->Handle(), &info);
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		return VULKAN::_RESULT::_OUT_OF_DATE;
	}
	if (result == VK_ERROR_SURFACE_LOST_KHR)
	{
		return VULKAN::_RESULT::_SURFACE_LOST;
	}
	if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
	{
		VK_RESULT(result);
	}

	if (bWaitForIdle)
	{
		VK_RESULT(::vkQueueWaitIdle(presentQueue->Handle()));
	}

	return VULKAN::_RESULT::_SUCCESS;
}
