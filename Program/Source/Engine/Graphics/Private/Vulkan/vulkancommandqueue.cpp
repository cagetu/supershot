#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkancommandlist.h"
#include "Vulkan/vulkanswapchain.h"
#include "Vulkan/vulkansemaphore.h"
#include "Vulkan/vulkanfence.h"

//------------------------------------------------------------------------------------------------------------------------------------
// VulkanCommandQueue
//------------------------------------------------------------------------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanCommandQueue, IGraphicCommandQueue);

//------------------------------------------------------------------------------------------------------------------------------------
VulkanCommandQueue::VulkanCommandQueue(VulkanDevice* device, uint32 familyIndex, uint32 queueIndex, COMMANDLIST_TYPE queueType)
	: IGraphicCommandQueue(queueType)
	, m_FamilyIndex(familyIndex)
	, m_QueueIndex(queueIndex)
{
	::vkGetDeviceQueue(device->Handle(), familyIndex, queueIndex, &m_QueueHandle);
	_ASSERT(m_QueueHandle != VK_NULL_HANDLE);
}
VulkanCommandQueue::~VulkanCommandQueue()
{
}

void VulkanCommandQueue::Submit(VulkanCommandList* commandBuffer, 
						 VulkanFence* fence,
						 VulkanSemaphore* signalSemaphore, 
						 VulkanSemaphore* waitSemaphore,
                         VkPipelineStageFlags waitStageMask,
                         bool bWaitIdle)
{
	VkSubmitInfo submitInfo;
	Memory::MemZero(submitInfo);
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	// 실행을 위해 Submit할 CommandBuffer. 
	// Submit 하면 Command를 실행 (ex, acquired swapchain image를 Color Attachment로 바인드 하는 Command Buffer를 Submit 한다.)
	if (commandBuffer)
	{
		VkCommandBuffer commandBufferHandle = commandBuffer->Handle();
		submitInfo.pCommandBuffers = &commandBufferHandle;
		submitInfo.commandBufferCount = 1;
	}
	// 실행을 시작하기 전에 signal이 올 때까지 기다린다. 
	if (waitSemaphore)
	{
		VkSemaphore semaphore = waitSemaphore->Handle();
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &semaphore;				// 이 Batch에 대한 Command Buffer가 Execution을 시작하기 전에 대기하는 Semaphore의 Array
		submitInfo.pWaitDstStageMask = &waitStageMask;			// Semaphore Wait가 발생할 각 대응하는 파이프라인 스테이지의 Array
	}
	// CommandBuffer의 실행이 완료되었을 때, Signal을 보내는 Semaphore
	if (signalSemaphore)
	{
		VkSemaphore semaphore = signalSemaphore->Handle();
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &semaphore;
	}

	//	Fence: 커맨드 버퍼 실행이 완료되었는지를 알려주는 신호 메커니즘을 사용한다.
	//	- (fence가 NULL이 아니고, submitCount가 0이 아니면), pCommandBuffers에 정의된 모든 커맨드 실행이 끝날 때 fence는 종료를 알리는 신호를 받게 된다.
	//	- (fence가 NULL이 아니고, submitCount가 0이면), fence 신호는 이전의 큐에 제출한 모든 작업이 완료되었음을 의미한다.
	//
	VK_RESULT(::vkQueueSubmit(m_QueueHandle, 1, &submitInfo, fence->Handle()));

	// Queue가 Submmit된 모든 작업을 완료하기를 기다린다.
	if (bWaitIdle)
	{
		VK_RESULT(::vkQueueWaitIdle(m_QueueHandle));
	}
}

//------------------------------------------------------------------
/*	Queue가 Submmit된 모든 작업을 완료하기를 기다린다.

	- Device->WaitForIdle 은 디바이스의 모든 Queue에 대해서 WaitForIdle 처리

	- 하지만 성능 상의 이유로 사용을 추천하지는 않는다. Queue에 모든 작업들을 완전히 Flush하며 매우 무거운 연산들이다.
	  따라서, 어플리케이션을 종료할 때나, 쓰레드 관리, 메모리 관리와 같은 어플리케이션 서브시스템을 다시 초기화할 때, Pause 등에 적합하다.

	- 동기화가 필요할 때에는 Fence를 이용하자!
*/
void VulkanCommandQueue::WaitForIdle()
{
	if (m_QueueHandle == VK_NULL_HANDLE)
		return;

	VK_RESULT(vkQueueWaitIdle(m_QueueHandle));
}

//------------------------------------------------------------------------------------------------------------------------------------
// VulkanQueueFamily
//------------------------------------------------------------------------------------------------------------------------------------
VulkanQueueFamily::VulkanQueueFamily(uint32 familyIndex, uint32 queueCount, COMMANDLIST_TYPE queueType)
	: m_FamilyIndex(familyIndex)
	, m_QueueCount(queueCount)
	, m_QueueType(queueType)
{
	m_Usages.AddZeroed(queueCount);
}
VulkanQueueFamily::~VulkanQueueFamily()
{
	m_Usages.Clear();
	ClearCommandQueues();
}

VulkanCommandQueue* VulkanQueueFamily::CreateCommandQueue(VulkanDevice* device)
{
	uint32 EmptyIndex = UINT_MAX;
	for (uint32 Index = 0; Index < m_Usages.Num(); Index++)
	{
		if (m_Usages[Index] == 0)
		{
			EmptyIndex = Index;
			m_Usages[Index] = 1;
			break;
		}
	}
	CHECK(EmptyIndex < m_QueueCount, TEXT("EmptyIndex(%d) < m_QueueCount(%d)"), EmptyIndex, m_QueueCount);

	VulkanCommandQueue* NewQueue = NewObject(VulkanCommandQueue, device, m_FamilyIndex, EmptyIndex, m_QueueType);
	m_CommandQueues.Add(NewQueue);

	return NewQueue;
}

void VulkanQueueFamily::DestroyCommandQueue(VulkanCommandQueue* queue)
{
	if (queue)
	{
		m_Usages[queue->QueueIndex()] = 0;
		m_CommandQueues.Remove(queue);

		DeleteObject(queue);
	}
}

void VulkanQueueFamily::ClearCommandQueues()
{
	for (int32 index = 0; index < m_CommandQueues.Num(); index++)
	{
		DeleteObject(m_CommandQueues[index]);
	}
	m_CommandQueues.Clear();
}