#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanrenderpass.h"
#include "Vulkan/vulkanrendertarget.h"
#include "Vulkan/vulkanframebuffer.h"

VulkanFrameBuffer::VulkanFrameBuffer()
	: m_FrameBufferHandle(VK_NULL_HANDLE)
{
	__ConstructReference;
}

VulkanFrameBuffer::~VulkanFrameBuffer()
{
	Destroy();

	__DestructReference;
}

bool VulkanFrameBuffer::Create(VulkanRenderPass* renderPass)
{
	/*	framebuffer는 어떤 이미지들이 이 attachment들로 사용될 것인지를 명시한다.

		* framebuffer는 render pass가 작동하는 특정 image들을 기술한다.
			: render pass에서 사용되는 모든 텍스쳐들을 기술한다.
			: (color, depth/stencil) 처럼 우리가 렌더링 하는 이미지들 뿐만 아니라, (input attachment) 처럼 source data로 사용되는 이미지들
		* render pass와 frame buffer를 분리함으로서, 다른 framebuffer에서 render pass를 사용할수도 있고, 다른 renderpass에서 framebuffer를 사용할수도 있다.
			: 유사한 타입과 용도의 이미지에 비슷한 형태로 수행되는 것을 의미한다.

		1. framebuffer와 renderpass attachment에 사용되는 각 이미지에 대한 ImageView를 만들어야 한다!
		2. Vulkan에서는 이미지를 직접 접근되지 않는다. 그래서 Image View가 사용된다. ImageView는 Image를 감싸고 추가적인 정보를 제공한다.
	*/
	//m_Attachments = attachments;

	VkFramebufferCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	createInfo.pNext = nullptr;
	// 사용할 렌더패스
	createInfo.renderPass = renderPass->Handle();
	// 렌더패스 attachment에 사용될 imageview 목록
	createInfo.attachmentCount = renderPass->AttachmentViews().Num();
	createInfo.pAttachments = renderPass->AttachmentViews().Data();
	// Size 설정
	createInfo.width = renderPass->Width();
	createInfo.height = renderPass->Height();
	createInfo.layers = 1;		// ???
	VK_RESULT(vkCreateFramebuffer(GVulkanDevice->Handle(), &createInfo, nullptr, &m_FrameBufferHandle));

	m_RenderPass = renderPass;
	return true;
}

bool VulkanFrameBuffer::Create(const Vk::RenderPassLayout& renderPassLayout)
{
	VulkanRenderPass* renderPass = NewObject(VulkanRenderPass);
	if (renderPass->Create(renderPassLayout))
	{
		return Create(renderPass);
	}

	DeleteObject(renderPass);
	return false;
}

void VulkanFrameBuffer::Destroy()
{
	if (m_FrameBufferHandle)
	{
		vkDestroyFramebuffer(GVulkanDevice->Handle(), m_FrameBufferHandle, nullptr);
	}
	m_FrameBufferHandle = NULL;

	m_RenderPass = NULL;
}
