#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanrenderpasslayout.h"

namespace Vk
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------------------------------------------------------------
//	RenderPassLayout
//------------------------------------------------------------------------------------------------------------------------------------
RenderPassLayout::RenderPassLayout()
	: Width(0), Height(0)
{
}

int32 RenderPassLayout::AddSubPass(const SubPassLayout& subPassLayout)
{
	return SubPassLayouts.Add(subPassLayout);
}

void RenderPassLayout::RemoveSubPass(int32 index)
{
	SubPassLayouts.RemoveAt(index);
}

void RenderPassLayout::ClearSubPasses()
{
	SubPassLayouts.Clear();
}

//------------------------------------------------------------------------------------------------------------------------------------
//	SubPassLayout
//------------------------------------------------------------------------------------------------------------------------------------
SubPassLayout::SubPassLayout()
{
	Memory::MemZero(ColorAttachments);
	Memory::MemZero(DepthStencilAttachment);
	Memory::MemZero(ResolveAttachment);

	NumColorAttachments = 0;
	HasDepthStencilAttachment = false;
	HasResolveAttachment = false;
}

void SubPassLayout::AddColorAttachment(VkImageView imageView, FORMAT_TYPE format, int32 multiSamples,
	const Vk::ImageLayoutOp& layout, const LinearColor& clearColor, const Vk::AttachmentOp& colorOp)
{
	const uint32 colorIndex = NumColorAttachments;

	ColorAttachments[colorIndex].imageView = imageView;
	ColorAttachments[colorIndex].format = format;
	ColorAttachments[colorIndex].multiSamples = multiSamples;
	ColorAttachments[colorIndex].clearColor = clearColor;
	ColorAttachments[colorIndex].colorOp = colorOp;
	ColorAttachments[colorIndex].layout = layout;

	NumColorAttachments++;
}

void SubPassLayout::AddDepthStencilAttachment(VkImageView imageView, FORMAT_TYPE format, int32 multiSamples,
											  const Vk::ImageLayoutOp& layout, const Vk::AttachmentOp& depthOp, float clearDepth, 
											  const Vk::AttachmentOp& stencilOp, ushort clearStencil)
{
	DepthStencilAttachment.imageView = imageView;
	DepthStencilAttachment.format = format;
	DepthStencilAttachment.multiSamples = multiSamples;
	DepthStencilAttachment.clearDepth = clearDepth;
	DepthStencilAttachment.clearStencil = clearStencil;
	DepthStencilAttachment.depthOp = depthOp;
	DepthStencilAttachment.stencilOp = stencilOp;
	DepthStencilAttachment.layout = layout;

	HasDepthStencilAttachment = true;
}

void SubPassLayout::AddResolveAttachment(VkImageView imageView, FORMAT_TYPE format, int32 multiSamples, const Vk::ImageLayoutOp& layout, const Vk::AttachmentOp& resolveOp)
{
	ResolveAttachment.imageView = imageView;
	ResolveAttachment.format = format;
	ResolveAttachment.multiSamples = multiSamples;
	ResolveAttachment.resolveOp = resolveOp;
	ResolveAttachment.layout = layout;

	HasResolveAttachment = true;
}

void SubPassLayout::Clear()
{
	Memory::MemZero(ColorAttachments);
	Memory::MemZero(DepthStencilAttachment);
	Memory::MemZero(ResolveAttachment);

	NumColorAttachments = 0;
	HasDepthStencilAttachment = false;
	HasResolveAttachment = false;
}

bool SubPassLayout::Compare(const SubPassLayout& rhs) const
{
	if (NumColorAttachments != rhs.NumColorAttachments ||
		HasDepthStencilAttachment != rhs.HasDepthStencilAttachment)
		return false;

	if (NumColorAttachments == rhs.NumColorAttachments)
	{
		if (memcmp(ColorAttachments, rhs.ColorAttachments, sizeof(ColorAttachmentInfo) * NumColorAttachments) != 0)
			return false;
	}
	if (HasDepthStencilAttachment == rhs.HasDepthStencilAttachment)
	{
		if (memcmp(&DepthStencilAttachment, &rhs.DepthStencilAttachment, sizeof(DepthStencilAttachmentInfo)) != 0)
			return false;
	}
	if (HasResolveAttachment == rhs.HasResolveAttachment)
	{
		if (memcmp(&ResolveAttachment, &rhs.ResolveAttachment, sizeof(ResolveAttachmentInfo)) != 0)
			return false;
	}
	return true;
}

bool SubPassLayout::operator==(const SubPassLayout& rhs)
{
	return Compare(rhs);
}

bool SubPassLayout::operator!=(const SubPassLayout& rhs)
{
	return (*this) == rhs ? false : true;
}

bool SubPassLayout::operator==(const SubPassLayout& rhs) const
{
	return Compare(rhs);
}

bool SubPassLayout::operator!=(const SubPassLayout& rhs) const
{
	return (*this) == rhs ? false : true;
}

//------------------------------------------------------------------------------------------------------------------------------------
//	SubPassLayout
//------------------------------------------------------------------------------------------------------------------------------------
void SubPassAttachmentReference::Reset()
{
	Memory::MemZero(ColorAttachments);
	Memory::MemZero(InputAttachments);
	Memory::MemZero(DepthStencilAttachment);
	Memory::MemZero(ResolveAttachment);
	Memory::MemZero(PreserveAttachments);

	NumColorAttachments = 0;
	NumInputAttachments = 0;
	NumPreserveAttachments = 0;
	HasDepthStencilAttachment = false;
	HasResolveAttachment = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // end of namespace
