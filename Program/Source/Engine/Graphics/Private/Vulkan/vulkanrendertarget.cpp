#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkantexture.h"
#include "Vulkan/vulkanrenderpass.h"
#include "Vulkan/vulkanrendertarget.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanRenderTarget, IGraphicRenderTarget);

VulkanRenderTarget::VulkanRenderTarget()
{
	m_DepthStencilSurface = nullptr;
}

VulkanRenderTarget::~VulkanRenderTarget()
{
}

bool VulkanRenderTarget::Create(int width, int height, int32 multisample)
{
	m_Width = width;
	m_Height = height;
	m_Multisample = multisample;
	return true;
}

void VulkanRenderTarget::Destroy()
{
}

void VulkanRenderTarget::SetRenderTexture(VulkanColorRenderTexture* surface)
{
	if (!m_ColorSurfaces.IsEmpty())
	{
		if (m_ColorSurfaces[0] == surface)
			return;
	}

	ClearRenderTextures();
	AddRenderTexture(surface);
}

void VulkanRenderTarget::AddRenderTexture(VulkanColorRenderTexture* surface)
{
	m_ColorSurfaces.Push(surface);
}

//VulkanRenderTexture* VulkanRenderTarget::AddRenderTexture(FORMAT_TYPE format, RT_LOAD_TYPE loadAction, RT_STORE_TYPE storeAction)
//{
//	VulkanColorRenderTexture* surface = new VulkanColorRenderTexture();
//	if (surface->Create(m_Width, m_Height, format) == false)
//	{
//		delete surface;
//		return NULL;
//	}
//
//	surface->ColorLoadStore().load = loadAction;
//	surface->ColorLoadStore().store = storeAction;
//
//	m_ColorSurfaces.Push(surface);
//
//	m_DescriptionDirty = true;
//
//	return surface;
//}

void VulkanRenderTarget::ClearRenderTextures()
{
	m_ColorSurfaces.Clear();
}

VulkanRenderTexture* VulkanRenderTarget::GetRenderTexture(int32 channel) const
{
	return m_ColorSurfaces.IsEmpty() == false && channel < (int32)m_ColorSurfaces.Num() ? m_ColorSurfaces[channel] : NULL;
}

VulkanRenderTexture* VulkanRenderTarget::GetRenderTexture(const String& id) const
{
	for (auto const it : m_ColorSurfaces)
	{
		if (it->GetName() == id)
			return it;
	}
	return NULL;
}

int32 VulkanRenderTarget::GetNumRenderTextures() const
{
	return m_ColorSurfaces.Num();
}

void VulkanRenderTarget::CreateDepthTexture(FORMAT_TYPE format, RT_LOAD_TYPE loadAction, RT_STORE_TYPE storeAction, RT_LOAD_TYPE stencilLoadAction, RT_STORE_TYPE stencilStoreAction)
{
	VulkanDepthStencilRenderTexture* depthStencil = NewObject(VulkanDepthStencilRenderTexture);
	if (depthStencil->Create(m_Width, m_Height, format, m_Multisample) == false)
	{
		DeleteObject(depthStencil);
		return;
	}

	m_DepthStencilSurface = depthStencil;
	m_DepthStencilSurface->DepthLoadStore().load = loadAction;
	m_DepthStencilSurface->DepthLoadStore().store = storeAction;
	m_DepthStencilSurface->StencilLoadStore().load = stencilLoadAction;
	m_DepthStencilSurface->StencilLoadStore().store = stencilStoreAction;

	assert(0);
}

void VulkanRenderTarget::SetDepthTexture(VulkanDepthStencilRenderTexture* surface)
{
	if (m_DepthStencilSurface == surface)
		return;

	m_DepthStencilSurface = surface;
}

VulkanDepthStencilRenderTexture* VulkanRenderTarget::GetDepthTexture() const
{
	return m_DepthStencilSurface;
}

//VkSubPassLayout layout;
//for (uint32 index = 0; index < m_ColorSurfaces.Num(); index++)
//{
//	VulkanColorRenderTexture* surface = m_ColorSurfaces[index];
//	layout.AddColorAttachment(surface->ImageView()->Handle(), surface->Format(), surface->Multisamples(), surface->ColorLoadStore(), surface->ClearColor());
//}
//{
//	VulkanDepthStencilRenderTexture* surface = m_DepthStencilSurface;
//	layout.AddDepthStencilAttachment(surface->ImageView()->Handle(), surface->Format(), surface->Multisamples(), 
//		surface->DepthLoadStore(), surface->StencilLoadStore(), surface->ClearDepth(), surface->ClearStencil());
//}
