#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkansemaphore.h"

VulkanSemaphore::VulkanSemaphore()
	: m_SemaphoreHandle(VK_NULL_HANDLE)
{
	VkSemaphoreCreateInfo createInfo;
	Memory::MemZero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkResult result = ::vkCreateSemaphore(GVulkanDevice->Handle(), &createInfo, nullptr, &m_SemaphoreHandle);
	_ASSERT(result == VK_SUCCESS);
}

VulkanSemaphore::~VulkanSemaphore()
{
	if (m_SemaphoreHandle)
	{
		vkDestroySemaphore(GVulkanDevice->Handle(), m_SemaphoreHandle, NULL);
	}

	m_SemaphoreHandle = VK_NULL_HANDLE;
}
