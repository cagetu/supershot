#include "foundation.h"
//#include "graphic.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanfence.h"

//------------------------------------------------------------------
/**	@class	VulkanFence
*/
//------------------------------------------------------------------
VulkanFence::VulkanFence(bool bIsSignaled)
	: m_FenceHandle(VK_NULL_HANDLE)
{
	VkFenceCreateInfo createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = bIsSignaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;
	VK_RESULT(::vkCreateFence(GVulkanDevice->Handle(), &createInfo, nullptr, &m_FenceHandle));

	m_IsSignaled = bIsSignaled;
}

VulkanFence::~VulkanFence()
{
	vkDestroyFence(GVulkanDevice->Handle(), m_FenceHandle, NULL);
	m_FenceHandle = VK_NULL_HANDLE;
}

void VulkanFence::Reset()
{
	if (m_IsSignaled == true)
	{
		// 하나 이상의 fence의 상태를 reset 한다. 이미 unsignaled 상태라면, 아무 효과가 발생하지 않는다.
		vkResetFences(GVulkanDevice->Handle(), 1, &m_FenceHandle);
		m_IsSignaled = false;
	}
}

bool VulkanFence::GetStatus()
{
	// Fence의 상태를 확인한다.
	VkResult result = vkGetFenceStatus(GVulkanDevice->Handle(), m_FenceHandle);
	if (result == VK_SUCCESS)
	{
		m_IsSignaled = true;
		return true;
	}
	else if (result == VK_NOT_READY)
	{
		m_IsSignaled = false;
		return false;
	}

	VK_RESULT(result);
	return false;
}

bool VulkanFence::WaitForSignal(uint64 timeout)
{
	// Fence의 모든 그룹이 Signal 될 때까지 기다린다.
	VkResult result = vkWaitForFences(GVulkanDevice->Handle(), 1, &m_FenceHandle, true, timeout);
	if (result == VK_SUCCESS)
	{
		m_IsSignaled = true;
		return true;
	}
	else if (result == VK_TIMEOUT)
	{
		m_IsSignaled = false;
		return false;
	}

	VK_RESULT(result);
	return false;
}

//------------------------------------------------------------------
/**	@class	VulkanFenceManager
*/
//------------------------------------------------------------------
VulkanFenceManager::VulkanFenceManager()
{
}
VulkanFenceManager::~VulkanFenceManager()
{
	Destroy();
}

void VulkanFenceManager::Create()
{
}

void VulkanFenceManager::Destroy()
{
	/// Need Threading Lock
	CHECK(m_UsedFences.IsEmpty(), TEXT("UsedFences must be empty!!!"));

	for (auto it = m_FreeFences.begin(); it != m_FreeFences.end(); it++)
		DeleteObject(*it);
	m_FreeFences.Clear();
}

VulkanFence* VulkanFenceManager::AllocateFence(bool bCreateSignaled)
{
	if (!m_FreeFences.IsEmpty())
	{
		VulkanFence* fence = m_FreeFences.Pop();
		m_UsedFences.Add(fence);

		if (bCreateSignaled)
		{
			fence->m_IsSignaled = true;
		}
		return fence;
	}

	VulkanFence* fence = NewObject(VulkanFence, bCreateSignaled);
	m_UsedFences.Add(fence);

	return fence;
}

void VulkanFenceManager::ReleaseFence(VulkanFence*& fence)
{
	if (fence)
	{
		fence->Reset();

		m_UsedFences.Remove(fence);
		m_FreeFences.Add(fence);
		fence = nullptr;
	}
}

void VulkanFenceManager::ResetFence(VulkanFence* fence)
{
	if (fence)
	{
		fence->Reset();
	}
}

bool VulkanFenceManager::CheckFenceState(VulkanFence* fence)
{
	if (fence)
	{
		return fence->GetStatus();
	}

	return false;
}

bool VulkanFenceManager::WaitForFence(VulkanFence* fence, uint64 timeout)
{
	if (fence)
	{
		return fence->WaitForSignal(timeout);
	}

	return false;
}

void VulkanFenceManager::WaitAndReleaseFence(VulkanFence*& fence, uint64 timeout)
{
	if (fence->IsSignaled() == false)
	{
		WaitForFence(fence, timeout);
	}

	ReleaseFence(fence);
}

bool VulkanFenceManager::IsSignaled(VulkanFence* fence)
{
	if (fence->IsSignaled())
	{
		return true;
	}

	return CheckFenceState(fence);
}
