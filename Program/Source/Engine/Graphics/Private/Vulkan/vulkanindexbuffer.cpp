#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "vulkan/vulkanbuffer.h"
#include "Vulkan/vulkanindexbuffer.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanIndexBuffer, IGraphicIndexBuffer);

VulkanIndexBuffer::VulkanIndexBuffer()
	: m_Buffer(nullptr)
{
}

VulkanIndexBuffer::~VulkanIndexBuffer()
{
}

bool VulkanIndexBuffer::Create(int32 bufferSize)
{
	return false;
}

void VulkanIndexBuffer::Destroy()
{
}
