#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "vulkan/vulkanbuffer.h"
#include "Vulkan/vulkanvertexbuffer.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanVertexBuffer, IGraphicVertexBuffer);

VulkanVertexBuffer::VulkanVertexBuffer()
	: m_Buffer(nullptr)
{
}

VulkanVertexBuffer::~VulkanVertexBuffer()
{
}

bool VulkanVertexBuffer::Create(int32 bufferSize)
{
	return false;
}

void VulkanVertexBuffer::Destroy()
{
}
