#include "foundation.h"
//#include "graphic.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanimage.h"
#include "Vulkan/vulkanadapter.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanImage, VulkanResource);

VulkanImage::VulkanImage()
	: m_ImageHandle(VK_NULL_HANDLE)
	, m_Allocation(VK_NULL_HANDLE)
{
}

VulkanImage::~VulkanImage()
{
	Destroy();
}

static const VkImageTiling GVulkanViewTypeTilingMode[VK_IMAGE_VIEW_TYPE_RANGE_SIZE] =
{
	VK_IMAGE_TILING_LINEAR,		// VK_IMAGE_VIEW_TYPE_1D
	VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_2D
	VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_3D
	VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_CUBE
	VK_IMAGE_TILING_LINEAR,		// VK_IMAGE_VIEW_TYPE_1D_ARRAY
	VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_2D_ARRAY
	VK_IMAGE_TILING_LINEAR,		// VK_IMAGE_VIEW_TYPE_CUBE_ARRAY
};

bool VulkanImage::Create(uint32 width, uint32 height, uint32 depth, uint32 format, uint32 textureFlags, 
						 VkImageViewType viewType, VkImageUsageFlags defaultUsageFlags,
						 uint32 mipMaps, uint32 multiSamples, bool isArray, bool isTilingLinear)
{
	VulkanPhysicalDevice* physicDevice = GVulkanDevice->PhysicalDevice();
	CHECK(physicDevice, TEXT("physicial Device is null"));

	const VkPhysicalDeviceProperties& deviceProperties = physicDevice->DeviceProps;

	VkImageCreateInfo createInfo;
	Memory::MemZero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	switch (viewType)
	{
	case VK_IMAGE_VIEW_TYPE_1D:
	case VK_IMAGE_VIEW_TYPE_1D_ARRAY:
		createInfo.imageType = VK_IMAGE_TYPE_1D;
		createInfo.extent.width = width;
		createInfo.extent.height = height;
		createInfo.extent.depth = 1;
		_ASSERT(width <= deviceProperties.limits.maxImageDimension1D);
		break;

	case VK_IMAGE_VIEW_TYPE_2D:
	case VK_IMAGE_VIEW_TYPE_2D_ARRAY:
		createInfo.imageType = VK_IMAGE_TYPE_2D;
		createInfo.extent.width = width;
		createInfo.extent.height = height;
		createInfo.extent.depth = 1;
		_ASSERT(width <= deviceProperties.limits.maxImageDimension2D);
		_ASSERT(height <= deviceProperties.limits.maxImageDimension2D);
		break;

	case VK_IMAGE_VIEW_TYPE_3D:
		createInfo.imageType = VK_IMAGE_TYPE_3D;
		createInfo.extent.width = width;
		createInfo.extent.height = height;
		createInfo.extent.depth = depth;
		_ASSERT(width <= deviceProperties.limits.maxImageDimension3D);
		_ASSERT(height <= deviceProperties.limits.maxImageDimension3D);
		_ASSERT(depth <= deviceProperties.limits.maxImageDimension3D);
		break;

	case VK_IMAGE_VIEW_TYPE_CUBE:
	case VK_IMAGE_VIEW_TYPE_CUBE_ARRAY:
		createInfo.imageType = VK_IMAGE_TYPE_2D;
		createInfo.extent.width = width;
		createInfo.extent.height = height;
		createInfo.extent.depth = 1;
		_ASSERT(width <= deviceProperties.limits.maxImageDimensionCube);
		_ASSERT(height <= deviceProperties.limits.maxImageDimensionCube);
		_ASSERT(width == height);
		break;
	}

	uint32 layerCount = (viewType == VK_IMAGE_VIEW_TYPE_CUBE) ? 6 : 1;
	createInfo.arrayLayers = (isArray ? height : 1) * layerCount;
	createInfo.mipLevels = mipMaps;

	createInfo.format = physicDevice->GetFormat(format);
	_ASSERT(createInfo.format != VK_FORMAT_UNDEFINED);

	createInfo.tiling = isTilingLinear ? VK_IMAGE_TILING_LINEAR : GVulkanViewTypeTilingMode[viewType];

	createInfo.usage = defaultUsageFlags;	// VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	//createInfo.usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	//createInfo.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	//createInfo.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;

	if (textureFlags & TextureCreateFlags::TEX_Present)
	{
		createInfo.usage |= VK_IMAGE_USAGE_STORAGE_BIT;
	}
	else if (textureFlags & TextureCreateFlags::TEX_RenderTarget)
	{
		createInfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	}
	else if (textureFlags & TextureCreateFlags::TEX_DepthStencilTarget)
	{
		createInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	}
	else if (textureFlags & TextureCreateFlags::TEX_ResolveRenderTarget)
	{
		createInfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	}

	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;	// 오직 하나의 Queue Family가 한번에 하나의 Image를 사용할 때, "exclusive" sharing mode를 사용한다.
	//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
	createInfo.queueFamilyIndexCount = 0;
	createInfo.pQueueFamilyIndices = nullptr;

	if (createInfo.tiling == VK_IMAGE_TILING_LINEAR)
	{
		multiSamples = 1;
		_ASSERT(0);
	}

	createInfo.samples = physicDevice->GetSampleCount(multiSamples);

	VmaAllocationInfo allocInfo;
	VmaAllocationCreateInfo allocCreateInfo = {};
	allocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	if (!GVulkanDevice->Allocator()->CreateImage(&m_ImageHandle, &createInfo, &allocCreateInfo, &m_Allocation, &allocInfo))
		return false;

	return true;
}

bool VulkanImage::Create(VkImage imageHandle)
{
	m_ImageHandle = imageHandle;
	return true;
}

void VulkanImage::Destroy()
{
	if (m_ImageHandle)
	{
		if (m_Allocation)
		{
			GVulkanDevice->Allocator()->DestroyImage(m_ImageHandle, m_Allocation);
		}
		//else
		//{
		//	vkDestroyImage(GVulkanDevice->Handle(), m_ImageHandle, NULL);
		//}
	}
	m_ImageHandle = VK_NULL_HANDLE;
}

//------------------------------------------------------------------

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanImageView, VulkanDescriptor);

VulkanImageView::VulkanImageView()
	: m_ImageViewHandle(VK_NULL_HANDLE)
	, m_NumMipmaps(0)
	, m_NumArrays(0)
{
}

VulkanImageView::~VulkanImageView()
{
	Destroy();
}

bool VulkanImageView::Create(VkImage image, int32 format, VkImageAspectFlags aspectMask, VkImageViewType viewType, uint32 mipMaps, uint32 arraySize)
{
	_ASSERT(m_ImageViewHandle == VK_NULL_HANDLE);

	VkImageViewCreateInfo createInfo;
	Memory::MemZero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.image = image;
	createInfo.viewType = viewType;
	createInfo.format = GVulkanDevice->PhysicalDevice()->GetFormat(format);
	createInfo.components = GVulkanDevice->PhysicalDevice()->GetFormatCommpoentMapping(format);
	////SubresourceRange
	createInfo.subresourceRange.aspectMask = aspectMask;
	createInfo.subresourceRange.baseMipLevel = 0;
	createInfo.subresourceRange.levelCount = mipMaps;
	createInfo.subresourceRange.baseArrayLayer = 0;
	createInfo.subresourceRange.layerCount = arraySize * (viewType == VK_IMAGE_VIEW_TYPE_CUBE ? 6 : 1);

	VK_RESULT(vkCreateImageView(GVulkanDevice->Handle(), &createInfo, nullptr, &m_ImageViewHandle));

	m_NumMipmaps = mipMaps;
	m_NumArrays = arraySize;
	return true;
}

void VulkanImageView::Destroy()
{
	if (m_ImageViewHandle != VK_NULL_HANDLE)
	{
		vkDestroyImageView(GVulkanDevice->Handle(), m_ImageViewHandle, NULL);
	}
	m_ImageViewHandle = VK_NULL_HANDLE;
}
