// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanshadercompiler.h"
#include "Vulkan/vulkanshadermodule.h"
#ifdef _USE_SPIRVRCROSS
#include <spirv_cross.hpp>
#include <spirv_reflect.hpp>
#endif
#ifdef _USE_SHADERC
#include <shaderc/shaderc.hpp>
#include <EASTL/string.h>
#endif
#include "assignRegistry.h"
#include "crc.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanShaderCompiler, IGraphicShaderCompiler);

VulkanShaderCompiler::VulkanShaderCompiler()
{
}

VulkanShaderCompiler::~VulkanShaderCompiler()
{
}

//------------------------------------------------------------------
/*
	1. hlsl 파일을 기반으로 spirv 코드를 생성한다.
	2. (hlsl + defines 정보)를 기반으로 spirv 파일은 캐시된다.
	3. spirv 파일을 기반으로 shadermodule 객체를 생성한다.
*/
IGraphicShaderModule* VulkanShaderCompiler::Compile(const ShaderDescription& desc)
{
	// DXC.exe를 이용하여, HLSL -> SPIR-V 파일을 만들어준다.
	String dxcPath = TEXT("C:\\supershot\\Program\\Source\\ThirdParty\\DXC\\dxc.exe");

	// .\dxc.exe -spirv -T vs_5_0 -E mainVS -D _USE_NORMALMAP .\test.hlsl -Fo test.vertex -Fc test_dis.vertex
	unicode::string cmd = TEXT("-spirv -fspv-reflect -O3");

	if (desc.type == ShaderType::VERTEXSHADER)
	{
		cmd += TEXT(" -T vs_6_0");
	}
	else if (desc.type == ShaderType::FRAGMENTSHADER)
	{
		cmd += TEXT(" -T ps_6_0");
	}
	else
	{
		ASSERT(0);
	}

	// defines
	String defines;
	for (SizeT index = 0; index < desc.defines.Num(); index++)
	{
		const ShaderDefine& shaderDefine = desc.defines[index];

		if (!shaderDefine.value.empty())
		{
			defines += Str::Printf(TEXT(" -D %s=%s"), shaderDefine.name.c_str(), shaderDefine.value.c_str());
		}
		else
		{
			defines += Str::Printf(TEXT(" -D %s"), shaderDefine.name.c_str());
		}
	}

	String shaderPath = AssignRegistry::Resolve(TEXT("shader"));
	String inputFile = Str::Printf(TEXT("%s/%s"), *shaderPath, desc.filename.c_str());

	uint32 hash = Crc::StrCrc32<TCHAR>(defines.c_str());
	String outputfile = Str::Printf(TEXT("%s/%s_%d.spv"), *shaderPath, desc.filename.c_str(), hash);

	// input
	cmd += Str::Printf(TEXT(" %s"), inputFile.c_str());

	// output
	cmd += Str::Printf(TEXT(" -Fo %s"), outputfile.c_str());

	//cmd += Str::Printf(TEXT(" -Fc %s.txt"), desc.filename.c_str());

	// entryPoint
	cmd += Str::Printf(TEXT(" -E %s"), desc.entryPoint.c_str());

	// defines
	cmd += defines;

	ULOG(Log::_Debug, "cmd: %s", cmd.c_str());

	ProcHandle Handle = PlatformProcess::CreateProc(dxcPath.c_str(), cmd.c_str(), true, true, true, NULL, 0, *shaderPath, NULL);
	if (!Handle.IsValid())
	{
		return NULL;
	}

	PlatformProcess::WaitForProc(Handle);
	PlatformProcess::CloseProc(Handle);

	String cacheFile = Str::Format(TEXT("%s"), outputfile.c_str());

	// 변환된 spirv 코드
	FileIO fileIO;
	if (!fileIO.Open(cacheFile.c_str()))
	{
		return NULL;
	}

	SizeT fileSize = fileIO.GetSize();
	uint8* cache = new uint8[fileSize];
	fileIO.Read(cache, fileSize);
	fileIO.Close();

	// 셰이더 모듈 생성
	VulkanShaderModule* shaderModule = NewObject(VulkanShaderModule);
	if (shaderModule->Create(desc.type, fileSize, (void*)cache))
	{
		return shaderModule;
	}
	DeleteObject(shaderModule);

#ifdef _USE_SHADERC
	// ShaderC를 이용해서, SPIR-V로 변환
	shaderc::Compiler compiler;

	shaderc::CompileOptions options;
	for (SizeT index = 0; index < desc.defines.Num(); index++)
	{
		const ShaderDefine& define = desc.defines[index];
		if (define.value.empty())
		{
			options.AddMacroDefinition(define.name.c_str());
		}
		else
		{
			options.AddMacroDefinition(define.value.c_str());
		}
	}
#ifdef _DEBUG
	options.SetGenerateDebugInfo();
	options.SetSuppressWarnings();
	options.SetWarningsAsErrors();
#endif
	//options.SetOptimizationLevel(shaderc_optimization_level_size);

	shaderc_shader_kind shaderType;
	if (desc.type == ShaderType::VERTEXSHADER)			shaderType = shaderc_shader_kind::shaderc_glsl_vertex_shader;
	else if (desc.type == ShaderType::FRAGMENTSHADER)	shaderType = shaderc_shader_kind::shaderc_glsl_fragment_shader;
	else if (desc.type == ShaderType::COMPUTESHADER)	shaderType = shaderc_shader_kind::shaderc_glsl_compute_shader;
	else if (desc.type == ShaderType::GEOMETRYSHADER)	shaderType = shaderc_shader_kind::shaderc_glsl_geometry_shader;
	else _ASSERT(0);

	// Precompile : GLSL 을 Define을 적용된 셰이더코드를 만들어준다.
	shaderc::PreprocessedSourceCompilationResult precompiledResult = compiler.PreprocessGlsl(code, size, shaderType, desc.filename.c_str(), options);
	if (precompiledResult.GetCompilationStatus() != shaderc_compilation_status::shaderc_compilation_status_success)
	{
		OutputDebugStringA(precompiledResult.GetErrorMessage().c_str());
		return NULL;
	}

	// GLSL -> SPIRV
	eastl::string compiledCode(precompiledResult.cbegin(), precompiledResult.cend());
	shaderc::SpvCompilationResult result = compiler.CompileGlslToSpv(compiledCode.data(), compiledCode.size(), shaderType, desc.filename.c_str(), desc.entryPoint.c_str(), options);
	if (result.GetCompilationStatus() != shaderc_compilation_status::shaderc_compilation_status_success)
	{
		OutputDebugStringA(result.GetErrorMessage().c_str());
		return NULL;
	}

	VulkanShaderModule* CompiledShader = NewObject(VulkanShaderModule);
	if (CompiledShader->Create(desc.type, (SizeT)(result.cend() - result.cbegin()), sizeof(shaderc::SpvCompilationResult::element_type), (void*)result.cbegin()))
	{
		return CompiledShader;
	}

	DeleteObject(CompiledShader);
#endif
	return NULL;
}
