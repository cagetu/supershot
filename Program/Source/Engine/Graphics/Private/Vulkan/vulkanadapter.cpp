#include "foundation.h"
#include "Vulkan/vulkanadapter.h"

//------------------------------------------------------------------
// 물리적 장치에 대한 정보들을 체크한다.
//------------------------------------------------------------------
void VulkanPhysicalDevice::Create(VkPhysicalDevice InDeviceHandle, VkSurfaceKHR InSurfaceHandle)
{
	DeviceHandle = InDeviceHandle;

	::vkGetPhysicalDeviceProperties(DeviceHandle, &DeviceProps);
	::vkGetPhysicalDeviceFeatures(DeviceHandle, &DeviceFeatures);

	// CommandBuffer는 Queue를 통해서 실행을 위해 하드웨어에 전달된다.
	// 하지만, 버퍼는 Graphics Command, Compute Command와 같이 다른 타입의 Operation을 포함할 것이다. 이게 Queue가 타른 타입으로 나뉘어져있는 이유이다.
	// 각 Queue Family는 다른 타입의 Operation을 지원한다.
	// PhysicalDevice가 우리가 수행하기를 원하는 Operation의 타입을 지원하는지 체크해야만 한다.

	// QueueFamilyProps
	Memory::MemZero(QueueFamilyProps);

	NumQueueFamily = 0;
	::vkGetPhysicalDeviceQueueFamilyProperties(DeviceHandle, &NumQueueFamily, nullptr);

	Memory::MemZero(QueueFamilyProps);
	::vkGetPhysicalDeviceQueueFamilyProperties(DeviceHandle, &NumQueueFamily, &QueueFamilyProps[0]);

	// Device Type
	utf8::string deviceType;
	switch (DeviceProps.deviceType)
	{
	case VK_PHYSICAL_DEVICE_TYPE_OTHER:				deviceType = "Other";			break;
	case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:	deviceType = "Intergrated GPU";	break;
	case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:		deviceType = "Discrete GPU";	break;
	case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:		deviceType = "Virtual GPU";		break;
	case VK_PHYSICAL_DEVICE_TYPE_CPU:				deviceType = "CPU";				break;
	default:										deviceType = "Unknown";			break;
	}

	ULOG_UTF8(Log::_Debug, "* DeviceID: %d, DeviceName: %s, DeviceType: %s", DeviceProps.deviceID, DeviceProps.deviceName, deviceType.c_str());
	ULOG_UTF8(Log::_Debug, "* API 0x%x Driver 0x%x VendorId 0x%x", DeviceProps.apiVersion, DeviceProps.driverVersion, DeviceProps.vendorID);


	for (uint32 familyIndex = 0; familyIndex < NumQueueFamily; familyIndex++)
	{
		const VkQueueFamilyProperties& prop = QueueFamilyProps[familyIndex];

		utf8::string desc;
		if ((prop.queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
		{
			QueueFlags |= VK_QUEUE_GRAPHICS_BIT;
			desc += ("Gfx");
		}
		if ((prop.queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
		{
			QueueFlags |= VK_QUEUE_COMPUTE_BIT;
			utf8::string component = (desc.empty() ? "Compute" : "+ Compute");
			desc = desc + component;
		}
		if ((prop.queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
		{
			QueueFlags |= VK_QUEUE_TRANSFER_BIT;
			desc += (desc.empty() ? utf8::string("") : utf8::string("+")) + utf8::string(" Transfer");
		}
		if ((prop.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT)
		{
			QueueFlags |= VK_QUEUE_SPARSE_BINDING_BIT;
			desc += (desc.empty() ? utf8::string("") : utf8::string("+")) + utf8::string(" Sparse");
		}

		ULOG_UTF8(Log::_Debug, "* QueueFamilyIndex: %d, queueCount: %d : %s", familyIndex, prop.queueCount, desc.c_str());
	}

	// PixelFormat 설정

	for (uint32 Index = 0; Index < VK_FORMAT_RANGE_SIZE; ++Index)
	{
		const VkFormat Format = (VkFormat)Index;
		Memory::MemZero(FormatProperties[Index]);
		vkGetPhysicalDeviceFormatProperties(DeviceHandle, Format, &FormatProperties[Index]);
	}

	SetupFormats();

	EnumulateSurfaceCapacities(InSurfaceHandle);
}

void VulkanPhysicalDevice::Destroy()
{
	// TODO
}

void VulkanPhysicalDevice::EnumulateSurfaceCapacities(VkSurfaceKHR InSurfaceHandle)
{
	Memory::MemZero(bQueueSupportPresents);

	for (uint32 familyIndex = 0; familyIndex < NumQueueFamily; familyIndex++)
	{
		const VkQueueFamilyProperties& prop = QueueFamilyProps[familyIndex];

		// 1-1. Surface 지원 여부 체크 (GraphicsQueue의 FamilyIndex에 대한 Surface가 Present를 지원하는지 체크)
		VK_RESULT(vkGetPhysicalDeviceSurfaceSupportKHR(DeviceHandle, familyIndex, InSurfaceHandle, &bQueueSupportPresents[familyIndex]));
	}

	//
	// surfaceFormat 설정 
	VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(DeviceHandle, InSurfaceHandle, &NumSurfaceFormats, nullptr));
	_ASSERT(NumSurfaceFormats > 0);

	VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(DeviceHandle, InSurfaceHandle, &NumSurfaceFormats, &SurfaceFormats[0]));
	_ASSERT(SurfaceFormats[0].format != VK_FORMAT_UNDEFINED);

	//
	// SurfaceCapabilies
	VK_RESULT(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(DeviceHandle, InSurfaceHandle, &SurfaceCapabilities));

	//
	// SurfacePresentModes
	VK_RESULT(vkGetPhysicalDeviceSurfacePresentModesKHR(DeviceHandle, InSurfaceHandle, &NumSurfacePresentModes, nullptr));
	_ASSERT(NumSurfacePresentModes > 0);

	VK_RESULT(vkGetPhysicalDeviceSurfacePresentModesKHR(DeviceHandle, InSurfaceHandle, &NumSurfacePresentModes, &SurfacePresentModes[0]));
}

bool VulkanPhysicalDevice::IsSupportGPU(VkQueueFlagBits operationType) const
{
	return (QueueFlags&operationType) ? true : false;
}

bool VulkanPhysicalDevice::IsSupportPresent(uint32 familyIndex) const
{
	return (bQueueSupportPresents[familyIndex] == VK_TRUE) ? true : false;
}

bool VulkanPhysicalDevice::IsDiscreteGPU() const
{
	return DeviceProps.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

bool VulkanPhysicalDevice::IsFormatSupported(VkFormat Format) const
{
	auto ArePropertiesSupported = [](const VkFormatProperties& Prop) -> bool
	{
		return (Prop.bufferFeatures != 0) || (Prop.linearTilingFeatures != 0) || (Prop.optimalTilingFeatures != 0);
	};

	if (Format >= 0 && Format < VK_FORMAT_RANGE_SIZE)
	{
		const VkFormatProperties& Prop = FormatProperties[Format];
		return ArePropertiesSupported(Prop);
	}

	// Check for extension formats
	//auto it = m_ExtensionFormatProperties.find(Format);
	//if (it == m_ExtensionFormatProperties.end())
	//{
	//	return ArePropertiesSupported(it->second);
	//}

	VkFormatProperties NewProperties;
	Memory::MemZero(NewProperties);
	vkGetPhysicalDeviceFormatProperties(DeviceHandle, Format, &NewProperties);

	// Add it for faster caching next time
	//m_ExtensionFormatProperties.insert(std::make_pair(Format, NewProperties));

	return ArePropertiesSupported(NewProperties);
}


//////////////////////////////////////////////////////////////////////////////////////

static const VkComponentMapping ComponentIdentity = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };
static const VkComponentMapping ComponentR = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO };
static const VkComponentMapping ComponentGR = { VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO };
static const VkComponentMapping ComponentRGBA = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
static const VkComponentMapping ComponentBGRA = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };

void VulkanPhysicalDevice::SetFormat(uint32 format, VkFormat vkFormat, const VkComponentMapping& vkComponent)
{
	PixelFormats[format].format = vkFormat;
	PixelFormats[format].mapping = vkComponent;
	PixelFormats[format].isSupported = IsFormatSupported(vkFormat);
}

void VulkanPhysicalDevice::SetupFormats()
{
	for (int i = 0; i < FMT_MAX; i++)
	{
		PixelFormats[i].format = VK_FORMAT_UNDEFINED;
		PixelFormats[i].mapping = ComponentRGBA;
		PixelFormats[i].bpp = 0;
	}

	SetFormat(FMT_B8G8R8A8, VK_FORMAT_B8G8R8A8_UNORM, ComponentBGRA);
	SetFormat(FMT_R8G8B8A8, VK_FORMAT_R8G8B8A8_UNORM, ComponentRGBA);
	SetFormat(FMT_D24S8, VK_FORMAT_D24_UNORM_S8_UINT, ComponentIdentity);	// VK_FORMAT_D32_SFLOAT_S8_UINT
	SetFormat(FMT_D16, VK_FORMAT_D16_UNORM, ComponentR);
	SetFormat(FMT_R16F, VK_FORMAT_R16_SFLOAT, ComponentR);
	SetFormat(FMT_G16R16F, VK_FORMAT_R16G16_SFLOAT, ComponentGR);
	SetFormat(FMT_A16B16G16R16F, VK_FORMAT_R16G16B16A16_SFLOAT, ComponentRGBA);
	SetFormat(FMT_R32F, VK_FORMAT_R32_SFLOAT, ComponentR);
	SetFormat(FMT_G32R32F, VK_FORMAT_R32G32_SFLOAT, ComponentGR);
	SetFormat(FMT_A32B32G32R32F, VK_FORMAT_R32G32B32A32_SFLOAT, ComponentRGBA);
}

VkFormat VulkanPhysicalDevice::GetFormat(uint32 format) const
{
	return PixelFormats[format].format;
}

const VkComponentMapping& VulkanPhysicalDevice::GetFormatCommpoentMapping(uint32 format) const
{
	return PixelFormats[format].mapping;
}

uint32 VulkanPhysicalDevice::GetFormatBlockByteSize(uint32 format) const
{
	uint32 bpp = 0;
	switch (format)
	{
	case FMT_R5G6B5:
	case FMT_R5G5B5A1:
	case FMT_R4G4B4A4:
		bpp = 16;
		break;
	case FMT_R8G8B8:
		bpp = 24;
		break;
	case FMT_R8G8B8A8:
	case FMT_B8G8R8A8:
		bpp = 32;
		break;
	default:
		_ASSERT(0);
	};
	return bpp / 8;
}

VkSampleCountFlagBits VulkanPhysicalDevice::GetSampleCount(uint32 sampleCount) const
{
	VkSampleCountFlagBits sampleBit;

	switch (sampleCount)
	{
	case 1:		sampleBit = VK_SAMPLE_COUNT_1_BIT;	break;
	case 2:		sampleBit = VK_SAMPLE_COUNT_2_BIT;	break;
	case 4:		sampleBit = VK_SAMPLE_COUNT_4_BIT;	break;
	case 8:		sampleBit = VK_SAMPLE_COUNT_8_BIT;	break;
	case 16:	sampleBit = VK_SAMPLE_COUNT_16_BIT;	break;
	case 32:	sampleBit = VK_SAMPLE_COUNT_32_BIT;	break;
	case 64:	sampleBit = VK_SAMPLE_COUNT_64_BIT;	break;
	default:	_ASSERT(0);							break;
	}

	return sampleBit;
}