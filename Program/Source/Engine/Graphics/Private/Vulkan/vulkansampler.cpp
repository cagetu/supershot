#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkansampler.h"
#include "Vulkan/vulkanadapter.h"

//------------------------------------------------------------------

VulkanSampler::VulkanSampler()
	: m_SamplerHandle(VK_NULL_HANDLE)
{
}

VulkanSampler::VulkanSampler(
	VkFilter minFilter, VkFilter magFilter,
	VkSamplerAddressMode addressU, VkSamplerAddressMode addressV, VkSamplerAddressMode addressW,
	int32 maxLodLevel, int32 minLodLevel,
	float mipLodBias, VkSamplerMipmapMode mipmapMode,
	VkBorderColor borderColor,
	bool anisotropyEnable,
	bool compareEnable, VkCompareOp compareOp)
{
	Create(minFilter, magFilter, addressU, addressV, addressW, maxLodLevel, minLodLevel, mipLodBias, mipmapMode, borderColor, anisotropyEnable, compareEnable, compareOp);
}

VulkanSampler::~VulkanSampler()
{
	Destroy();
}

// Create a texture sampler
// In Vulkan textures are accessed by samplers
// This separates all the sampling information from the texture data. This means you could have multiple sampler objects for the same texture with different settings
// Note: Similar to the samplers available with OpenGL 3.3

bool VulkanSampler::Create(
	VkFilter minFilter, VkFilter magFilter, 
	VkSamplerAddressMode addressU, VkSamplerAddressMode addressV, VkSamplerAddressMode addressW,
	int32 maxLodLevel, int32 minLodLevel, 
	float mipLodBias, VkSamplerMipmapMode mipmapMode,
	VkBorderColor borderColor, 
	bool anisotropyEnable, 
	bool compareEnable, VkCompareOp compareOp)
{
	_ASSERT(m_SamplerHandle == VK_NULL_HANDLE);

	VkSamplerCreateInfo createInfo;
	Memory::MemZero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	createInfo.maxAnisotropy = 1.0f;
	createInfo.magFilter = magFilter;
	createInfo.minFilter = minFilter;
	createInfo.addressModeU = addressU;
	createInfo.addressModeV = addressV;
	createInfo.addressModeW = addressW;
	createInfo.mipmapMode = mipmapMode;
	createInfo.mipLodBias = mipLodBias;
	createInfo.minLod = (float)minLodLevel;
	// Set max level-of-detail to mip level count of the texture
	createInfo.maxLod = (float)maxLodLevel;

	// Enable anisotropic filtering
	// This feature is optional, so we must check if it's supported on the device
	if (GVulkanDevice->PhysicalDevice()->DeviceFeatures.samplerAnisotropy)
	{
		// Use max. level of anisotropy for this example
		createInfo.maxAnisotropy = GVulkanDevice->PhysicalDevice()->DeviceProps.limits.maxSamplerAnisotropy;
		createInfo.anisotropyEnable = anisotropyEnable;
	}
	else
	{
		// The device does not support anisotropic filtering
		createInfo.maxAnisotropy = 1.0;
		createInfo.anisotropyEnable = VK_FALSE;
	}

	createInfo.compareEnable = compareEnable;
	createInfo.compareOp = compareOp;
	createInfo.borderColor = borderColor;

	VK_RESULT(vkCreateSampler(GVulkanDevice->Handle(), &createInfo, nullptr, &m_SamplerHandle));

	return true;
}

void VulkanSampler::Destroy()
{
	if (m_SamplerHandle)
	{
		vkDestroySampler(GVulkanDevice->Handle(), m_SamplerHandle, NULL);
	}
	m_SamplerHandle = VK_NULL_HANDLE;
}