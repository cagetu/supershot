#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanbuffer.h"
#include "Vulkan/vulkanadapter.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanBuffer, VulkanResource);

VulkanBuffer::VulkanBuffer()
	: m_BufferHandle(VK_NULL_HANDLE)
{
}

VulkanBuffer::~VulkanBuffer()
{
	Destroy();
}

bool VulkanBuffer::Create(uint32 size, BufferUsageFlags usageFlag)
{
	VkBufferCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	// 오직 하나의 Queue Family가 한번에 하나의 Buffer를 사용할 때, "exclusive" sharing mode를 사용한다.
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
	createInfo.queueFamilyIndexCount = 0;
	createInfo.pQueueFamilyIndices = nullptr;
	//Size
	createInfo.size = size;
	//Usage
	createInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	if (usageFlag == BufferUsageFlags::BUF_Static)
	{
		createInfo.usage |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	}

	VmaAllocationCreateInfo allocCreateInfo = {};
	if (usageFlag == BufferUsageFlags::BUF_Static)
	{
		allocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	}
	else if (usageFlag == BufferUsageFlags::BUF_Dynamic)
	{
		allocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
		allocCreateInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
	}

	VmaAllocationInfo allocInfo;
	if (!GVulkanDevice->Allocator()->CreateBuffer(&m_BufferHandle, &createInfo, &allocCreateInfo, &m_Allocation, &allocInfo))
		return false;

	return true;
}

void VulkanBuffer::Destroy()
{
	if (m_BufferHandle)
	{
		GVulkanDevice->Allocator()->DestroyBuffer(m_BufferHandle, m_Allocation);
		m_BufferHandle = VK_NULL_HANDLE;
	}
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanBufferView, VulkanDescriptor);

VulkanBufferView::VulkanBufferView()
	: m_BufferViewHandle(VK_NULL_HANDLE)
{
}

VulkanBufferView::~VulkanBufferView()
{
	Destroy();
}

bool VulkanBufferView::Create(VkBuffer buffer, int32 format)
{
	_ASSERT(m_BufferViewHandle == VK_NULL_HANDLE);

	VkBufferViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.buffer = buffer;
	createInfo.format = GVulkanDevice->PhysicalDevice()->GetFormat(format);
	createInfo.flags;
	createInfo.offset;
	createInfo.range;
	return false;
}

void VulkanBufferView::Destroy()
{
	if (m_BufferViewHandle)
	{
		vkDestroyBufferView(GVulkanDevice->Handle(), m_BufferViewHandle, nullptr);
	}
	m_BufferViewHandle = VK_NULL_HANDLE;
}



