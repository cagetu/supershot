#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanadapter.h"
#include "Vulkan/vulkancommandqueue.h"
#include "Vulkan/vulkanfence.h"
#include "Vulkan/vulkancommandallocator.h"
#include "Vulkan/vulkanmemory.h"
#include "Vulkan/vulkandescriptorlayout.h"
#include "Vulkan/vulkandescriptorpool.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanDevice, IGraphicDevice);

VulkanDevice::VulkanDevice()
	: m_DeviceHandle(VK_NULL_HANDLE)
	, m_PhysicalDevice(nullptr)
	, m_Allocator(nullptr)
	, m_FenceManager(nullptr)
{
}

VulkanDevice::~VulkanDevice()
{
	Destroy();
}

bool VulkanDevice::Create(VulkanPhysicalDevice* InPhysicalDevice)
{
	MTScopeLock lock(m_QueueSync);

	uint32 totalQueueCount = 0;

	const SizeT NumQueueFamilyProps = InPhysicalDevice->NumQueueFamily;

	Array<VkDeviceQueueCreateInfo> queueCreateInfos(NumQueueFamilyProps);
	for (uint32 familyIndex = 0; familyIndex < NumQueueFamilyProps; familyIndex++)
	{
		const VkQueueFamilyProperties& prop = InPhysicalDevice->QueueFamilyProps[familyIndex];

		VkDeviceQueueCreateInfo& info = queueCreateInfos[familyIndex];
		info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		info.pNext = nullptr;
		info.flags = 0;
		info.queueFamilyIndex = familyIndex;
		info.queueCount = prop.queueCount;

		totalQueueCount += prop.queueCount;

		if (prop.queueFlags & VK_QUEUE_GRAPHICS_BIT)
		{
			VulkanQueueFamily* NewQueueFamily = NewObject(VulkanQueueFamily, familyIndex, prop.queueCount, COMMANDLIST_TYPE::DIRECT);
			m_QueueFamilies.Add(NewQueueFamily);
		}
		else if (prop.queueFlags & VK_QUEUE_COMPUTE_BIT)
		{
			VulkanQueueFamily* NewQueueFamily = NewObject(VulkanQueueFamily, familyIndex, prop.queueCount, COMMANDLIST_TYPE::COMPUTE);
			m_QueueFamilies.Add(NewQueueFamily);
		}
		else
		{
			ASSERT(0);
		}
	}

	Array<float> queueProperties(totalQueueCount);
	float* CurrentPriority = &queueProperties[0];
	*CurrentPriority = 0.0f;
	for (uint32 index = 0; index < NumQueueFamilyProps; ++index)
	{
		const VkQueueFamilyProperties& CurrProps = InPhysicalDevice->QueueFamilyProps[index];

		VkDeviceQueueCreateInfo& info = queueCreateInfos[index];
		info.pQueuePriorities = CurrentPriority;
		for (int32 QueueIndex = 0; QueueIndex < (int32)CurrProps.queueCount; ++QueueIndex)
		{
			*CurrentPriority++ = 1.0f;
		}
	}

	bool debugMarkers;
	Array<const char*> deviceExtensions;
	Array<const char*> deviceLayers;
	VulkanLayer::GetDeviceLayerAndExtensions(InPhysicalDevice->DeviceHandle, deviceExtensions, deviceLayers, debugMarkers);

	VkDeviceCreateInfo deviceInfo;
	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceInfo.pNext = nullptr;
	deviceInfo.flags = 0;
	deviceInfo.enabledExtensionCount = deviceExtensions.Num();
	deviceInfo.ppEnabledExtensionNames = deviceExtensions.IsEmpty() ? nullptr : &deviceExtensions[0];
	deviceInfo.enabledLayerCount = deviceLayers.Num();
	deviceInfo.ppEnabledLayerNames = deviceLayers.IsEmpty() ? nullptr : &deviceLayers[0];
	deviceInfo.queueCreateInfoCount = NumQueueFamilyProps;
	deviceInfo.pQueueCreateInfos = NumQueueFamilyProps > 0 ? &queueCreateInfos[0] : nullptr;
	deviceInfo.pEnabledFeatures = &InPhysicalDevice->DeviceFeatures;

	VkResult result = ::vkCreateDevice(InPhysicalDevice->DeviceHandle, &deviceInfo, nullptr, &m_DeviceHandle);
	if (result != VK_SUCCESS)
		return false;

	m_PhysicalDevice = InPhysicalDevice;

	m_Allocator = NewObject(VulkanMemoryAllocator, this);
	m_FenceManager = NewObject(VulkanFenceManager);

	m_DescriptorPools.Add(NewObject(VulkanDescriptorPool, this));
	return true;
}

void VulkanDevice::Destroy()
{
	MTScopeLock lock(m_QueueSync);

	for (SizeT i = 0; i < m_QueueFamilies.Num(); i++)
		DeleteObject(m_QueueFamilies[i]);
	m_QueueFamilies.Clear();

	for (auto it = m_CommandPools.begin(); it != m_CommandPools.end(); it++)
		DeleteObject(*it);
	m_CommandPools.Clear();

	for (auto it = m_DescriptorPools.begin(); it != m_DescriptorPools.end(); it++)
		DeleteObject(*it);
	m_DescriptorPools.Clear();

	DeleteObject(m_FenceManager);
	DeleteObject(m_Allocator);

	if (m_DeviceHandle)
	{
		::vkDestroyDevice(m_DeviceHandle, NULL);
		m_DeviceHandle = VK_NULL_HANDLE;
	}

	m_PhysicalDevice = nullptr;
}

bool VulkanDevice::WaitForIdle()
{
	VkResult hr = vkDeviceWaitIdle(m_DeviceHandle);
	if (hr == VK_SUCCESS)
		return true;

	return false;
}

VulkanCommandQueue* VulkanDevice::CreateCommandQueue(COMMANDLIST_TYPE commandQueueType, bool bSupportPresent)
{
	MTScopeLock lock(m_QueueSync);

	for (uint32 Index = 0; Index < m_QueueFamilies.Num(); Index++)
	{
		// Present 지원여부 체크
		if (bSupportPresent)
		{
			if (!m_PhysicalDevice->IsSupportPresent(Index))
				continue;
		}

		if (m_QueueFamilies[Index]->QueueType() == commandQueueType)
		{ 
			return m_QueueFamilies[Index]->CreateCommandQueue(this);
		}
	}
	return nullptr;
}

void VulkanDevice::DestroyCommandQueue(VulkanCommandQueue* queue)
{
	if (queue)
	{
		MTScopeLock lock(m_QueueSync);

		m_QueueFamilies[queue->FamilyIndex()]->DestroyCommandQueue(queue);
	}
}

VulkanCommandAllocator* VulkanDevice::CreateCommandPool(COMMANDLIST_TYPE commandQueueType, uint32 queueFamilyIndex, Vk::CommandPoolCreateFlag::Type createFlags)
{
	MTScopeLock lock(m_CommandPoolSync);

	VulkanCommandAllocator* NewCommandPool = NewObject(VulkanCommandAllocator, commandQueueType, queueFamilyIndex, createFlags);
	m_CommandPools.Add(NewCommandPool);

	return NewCommandPool;
}

void VulkanDevice::DestroyCommandPool(VulkanCommandAllocator* commandPool)
{
	if (commandPool)
	{
		MTScopeLock lock(m_CommandPoolSync);

		m_CommandPools.Remove(commandPool);
		DeleteObject(commandPool);
	}
}

VulkanDescriptorSets* VulkanDevice::CreateDescriptorSet(VulkanDescriptorSetsLayout* layout)
{
	VulkanDescriptorPool* usableDescriptorPool = m_DescriptorPools.Back();
	if (!usableDescriptorPool->CanAllocate(*layout))
	{
		usableDescriptorPool = NewObject(VulkanDescriptorPool, this);
		m_DescriptorPools.Add(usableDescriptorPool);
	}

	VulkanDescriptorSets* descriptorSets = usableDescriptorPool->CreateDescriptorSet(layout);
	ASSERT(descriptorSets);
	return descriptorSets;
}

void VulkanDevice::DestroyDescriptorSet(VulkanDescriptorSets* descriptorSets)
{
	// 현재 DescriptorSets을 할당한 DescriptorPool을 찾아서 제거해달라고 해야 한다.
	IndexT poolID = descriptorSets->GetPoolID();
	for (SizeT index = 0; index < m_DescriptorPools.Num(); index++)
	{
		if (m_DescriptorPools[index]->ID() == poolID)
		{
			m_DescriptorPools[index]->DestroyDescriptorSet(descriptorSets);
			break;
		}
	}
}
