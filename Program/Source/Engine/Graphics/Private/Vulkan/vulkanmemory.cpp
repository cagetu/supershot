#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanloader.h"
#include "Vulkan/vulkanadapter.h"

#define VMA_IMPLEMENTATION
#define VMA_MIN(a,b)	(((a) < (b)) ? (a) : (b))
#define VMA_MAX(a,b)	(((a) > (b)) ? (a) : (b))
#include "Vulkan/vulkanmemory.h"

//https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/

VulkanMemoryAllocator::VulkanMemoryAllocator(VulkanDevice* device)
{
	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.physicalDevice = device->PhysicalDevice()->DeviceHandle;
	allocatorInfo.device = device->Handle();

	VK_RESULT(vmaCreateAllocator(&allocatorInfo, &m_Allocator));
}

VulkanMemoryAllocator::~VulkanMemoryAllocator()
{
	vmaDestroyAllocator(m_Allocator);
}

bool VulkanMemoryAllocator::CreateImage(VkImage* imageHandle, const VkImageCreateInfo* pCreateInfo, const VmaAllocationCreateInfo* allocCreateInfo, VmaAllocation* pAllocation, VmaAllocationInfo* pAllocationInfo)
{
	VkResult result = vmaCreateImage(m_Allocator, pCreateInfo, allocCreateInfo, imageHandle, pAllocation, pAllocationInfo);
	if (result != VK_SUCCESS)
		return false;

	return true;
}

void VulkanMemoryAllocator::DestroyImage(VkImage imageHandle, VmaAllocation pAllocation)
{
	vmaDestroyImage(m_Allocator, imageHandle, pAllocation);
}

bool VulkanMemoryAllocator::CreateBuffer(VkBuffer* bufferHandle, const VkBufferCreateInfo* pCreateInfo, const VmaAllocationCreateInfo* allocCreateInfo, VmaAllocation* pAllocation, VmaAllocationInfo* pAllocationInfo)
{
	VkResult result = vmaCreateBuffer(m_Allocator, pCreateInfo, allocCreateInfo, bufferHandle, pAllocation, pAllocationInfo);
	if (result != VK_SUCCESS)
		return false;

	return true;
}

void VulkanMemoryAllocator::DestroyBuffer(VkBuffer bufferHandle, VmaAllocation pAllocation)
{
	vmaDestroyBuffer(m_Allocator, bufferHandle, pAllocation);
}
