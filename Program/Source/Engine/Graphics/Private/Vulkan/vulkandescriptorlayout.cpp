// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkandescriptorlayout.h"
#include "Vulkan/vulkanadapter.h"
#include "crc.h"

void VulkanDescriptorSetsLayout::DescriptorSetLayout::GenerateHash()
{
	hash = Crc::MemCrc32(layoutBings.Data(), sizeof(VkDescriptorSetLayoutBinding) * layoutBings.Num());
}

//---------------------------------------------------------------------------------------------------------------------------------
//
//	DescriptorSetLayouts
//
//---------------------------------------------------------------------------------------------------------------------------------
VulkanDescriptorSetsLayout::VulkanDescriptorSetsLayout()
{
	__ConstructReference;
	Memory::MemZero(m_NumDescriptorsPerType);
}

VulkanDescriptorSetsLayout::~VulkanDescriptorSetsLayout()
{
	// :::::TODO:::::
	//	Fence 처리를 해서, Pending 처리를 해야 한다....

	for (VkDescriptorSetLayout& handle : m_DescriptorSetLayoutHandles)
	{
		vkDestroyDescriptorSetLayout(GVulkanDevice->Handle(), handle, nullptr);
	}
	m_DescriptorSetLayoutHandles.Clear();

	__DestructReference;
}

void VulkanDescriptorSetsLayout::Clear()
{
	Memory::MemZero(m_NumDescriptorsPerType);
	m_SetLayouts.Clear();
	m_DescriptorSetLayoutHandles.Clear();
}

void VulkanDescriptorSetsLayout::AddDescriptor(uint32 layoutSetID, const VkDescriptorSetLayoutBinding& descriptor)
{
	if (m_SetLayouts.Num() <= layoutSetID)
	{
		m_SetLayouts.SetNum(layoutSetID + 1, false);
	}
	DescriptorSetLayout& setLayout = m_SetLayouts[layoutSetID];
	setLayout.layoutBings.Add(descriptor);

	m_NumDescriptorsPerType[descriptor.descriptorType]++;
}

void VulkanDescriptorSetsLayout::Submit()
{
	const VkPhysicalDeviceLimits& Limits = GVulkanDevice->PhysicalDevice()->DeviceProps.limits;
	{
		// Check for maxDescriptorSetSamplers
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_SAMPLER]
			+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER]
			< Limits.maxDescriptorSetSamplers);

		// Check for maxDescriptorSetUniformBuffers
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER]
			+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC]
			< Limits.maxDescriptorSetUniformBuffers);

		// Check for maxDescriptorSetUniformBuffersDynamic
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC]
			< Limits.maxDescriptorSetUniformBuffersDynamic);

		// Check for maxDescriptorSetStorageBuffers
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER]
			+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC]
			< Limits.maxDescriptorSetStorageBuffers);

		// Check for maxDescriptorSetStorageBuffersDynamic
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC]
			< Limits.maxDescriptorSetStorageBuffersDynamic);

		// Check for maxDescriptorSetSampledImages
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER]
			+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE]
			+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER]
			< Limits.maxDescriptorSetSampledImages);

		// Check for maxDescriptorSetStorageImages
		_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_IMAGE]
			+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER]
			< Limits.maxDescriptorSetStorageImages);
	}

	m_DescriptorSetLayoutHandles.Reserve(m_SetLayouts.Num());

	for (DescriptorSetLayout& descriptorSetLayout : m_SetLayouts)
	{
		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.pNext = nullptr;
		layoutInfo.bindingCount = descriptorSetLayout.layoutBings.Num();
		layoutInfo.pBindings = descriptorSetLayout.layoutBings.Data();

		VkDescriptorSetLayout handle = VK_NULL_HANDLE;
		VK_RESULT(vkCreateDescriptorSetLayout(GVulkanDevice->Handle(), &layoutInfo, nullptr, &handle));

		m_DescriptorSetLayoutHandles.Add(handle);
	}
}

//---------------------------------------------------------------------------------------------------------------------------------
//
//	VulkanDescriptorSets
//
//---------------------------------------------------------------------------------------------------------------------------------
VulkanDescriptorSets::VulkanDescriptorSets(IndexT poolID, VulkanDescriptorSetsLayout* layout)
	: m_Layout(layout)
	, m_PoolID(poolID)
{
	// DescriptorSet 생성
	//m_Pool = GVulkanDevice->AllocateDescriptorSets(*layout, m_Sets.data());
	//m_Pool->TrackAddUsage(*layout);
}

VulkanDescriptorSets::~VulkanDescriptorSets()
{
	if (m_Layout)
	{
		delete m_Layout;
	}
	m_Layout = nullptr;
}
