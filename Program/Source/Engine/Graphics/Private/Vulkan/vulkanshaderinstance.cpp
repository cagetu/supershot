// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "Vulkan/vulkandevice.h"
#include "Vulkan/vulkanshaderinstance.h"
#include "Vulkan/vulkanshadermodule.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanShaderInstance, IGraphicShaderInstance);

VulkanShaderInstance::VulkanShaderInstance()
{
}

VulkanShaderInstance::~VulkanShaderInstance()
{
}

bool VulkanShaderInstance::BindShader(IGraphicShaderModule* shaderModule)
{
	if (!shaderModule)
		return false;

	VulkanShaderModule* gfxShaderModule = shaderModule->DynamicCast<VulkanShaderModule>();
	if (gfxShaderModule != nullptr)
		return false;

	// Shader 모듈 추가해서 Descriptor 추가한다.

	const Array<VulkanShaderModule::Attribute>& inputs = gfxShaderModule->GetInputs();
	for (SizeT index = 0; index < inputs.Num(); index++)
	{
		inputs[index].sementic;
		inputs[index].location;
		inputs[index].name;

		int32 a = 1;
	}

	const Array<VulkanShaderModule::Attribute>& outputs = gfxShaderModule->GetOuputs();
	for (SizeT index = 0; index < outputs.Num(); index++)
	{
		outputs[index].sementic;
		outputs[index].location;
		outputs[index].name;
	}

	uint32 numDescriptors = gfxShaderModule->NumDescriptors();
	for (uint32 index = 0; index < numDescriptors; index++)
	{
		//m_DescriptorSetsLayout.AddDescriptor()
	}

	// Shader 파라미터 옮기기...

	return true;
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, VulkanShaderModuleInstance, IGraphicShaderModuleInstance);

VulkanShaderModuleInstance::VulkanShaderModuleInstance()
{
	m_ShaderModule = nullptr;
}

VulkanShaderModuleInstance::~VulkanShaderModuleInstance()
{
}

bool VulkanShaderModuleInstance::Create(IGraphicShaderModule* shaderModule)
{
	if (!shaderModule)
		return false;

	m_ShaderModule = shaderModule->DynamicCast<VulkanShaderModule>();
	if (m_ShaderModule != nullptr)
		return false;

	return true;
}

void VulkanShaderModuleInstance::Destroy()
{
}

void VulkanShaderModuleInstance::SetValue(uint32 index, const Variant& value)
{
}

void VulkanShaderModuleInstance::ClearValues()
{
}
