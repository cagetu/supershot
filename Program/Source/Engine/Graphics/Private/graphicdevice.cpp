#include "foundation.h"
#include "graphicdevice.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicDevice);

IGraphicDevice::IGraphicDevice()
{
	__ConstructReference;
}
IGraphicDevice::~IGraphicDevice()
{
	__DestructReference;
}
