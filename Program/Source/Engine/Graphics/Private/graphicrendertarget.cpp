#include "foundation.h"
#include "graphicrendertarget.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicRenderTarget);

IGraphicRenderTarget::IGraphicRenderTarget()
	: m_Width(0), m_Height(0), m_Multisample(1)
	, m_MipLevel(0), m_NumMips(0), m_ClearFlags(0)
	, m_ClearDepth(1.0f), m_ClearColor(LinearColor::WHITE), m_ClearStencil(1)
{
}

IGraphicRenderTarget::~IGraphicRenderTarget()
{
}

