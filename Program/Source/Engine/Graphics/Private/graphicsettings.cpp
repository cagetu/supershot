#include "foundation.h"
#include "graphicsettings.h"

GraphicSettings& GraphicSettings::Get()
{
	static GraphicSettings __instance;
	return __instance;
}
