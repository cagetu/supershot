// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "graphicshadermodule.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicShaderModule);

IGraphicShaderModule::IGraphicShaderModule()
{
	__ConstructReference;
	m_Type = ShaderType::_INVALID;
	m_ElementSize = 0;
	m_ElementData = nullptr;
}
IGraphicShaderModule::~IGraphicShaderModule()
{
	__DestructReference;
	Destroy();
}

bool IGraphicShaderModule::Create(ShaderType::TYPE type, uint32 elementSize, const void* elementData)
{
	m_Type = type;
	m_ElementSize = elementSize;

	m_ElementData = NewPOD(uint8, GetByteSize());
	Memory::MemCpy(m_ElementData, elementData, GetByteSize());
	return true;
}

void IGraphicShaderModule::Destroy()
{
	DeletePOD(m_ElementData);
	m_ElementSize = 0;
}
