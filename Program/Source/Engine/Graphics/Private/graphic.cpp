#include "graphicheader.h"
#include "graphic.h"
#include "color.h"

Gfx::TYPE g_GfxType = Gfx::_NONE;
IGraphicDriver* g_GfxDriver = nullptr;

IGraphicDriver* Gfx::Driver()
{
	CHECK(g_GfxDriver, TEXT("GfxDriver is Invalid"));
	return g_GfxDriver;
}

Gfx::TYPE Gfx::DriverType()
{
	return g_GfxType;
}

void Gfx::_Startup(TYPE type)
{
	switch (type)
	{
#if _SUPPORT_D3D11
	case _D3D11:
		g_GfxDriver = new D3D11Driver();
		Coordinate::_Startup(Coordinate::_D3D);
		break;
#endif

#if _SUPPORT_D3D12
	case _D3D12:
		g_GfxDriver = new D3D12Driver();
		Coordinate::_Startup(Coordinate::_D3D);
		break;
#endif

#if _SUPPORT_OPENGL_ES
	case _OPENGLES:
		g_GfxDriver = new OpenGLESDriver();
		Coordinate::_Startup(Coordinate::_OPENGL);
		break;
#endif

#if _SUPPORT_VULKAN
	case _VULKAN:
		g_GfxDriver = new VulkanDriver();
		Coordinate::_Startup(Coordinate::_VULKAN);
		break;
#endif
	default:
		break;
	};

	g_GfxType = type;
}

void Gfx::_Shutdown()
{
	if (g_GfxDriver)
		delete g_GfxDriver;
	g_GfxDriver = nullptr;

	Coordinate::_Shutdown();
}

bool Gfx::Create(const WindowHandle& displayHandle)
{
	return g_GfxDriver->Create(displayHandle);
}

void Gfx::Destroy()
{
	if (g_GfxDriver)
		delete g_GfxDriver;
	g_GfxDriver = nullptr;
}

void Gfx::BeginFrame(Viewport* viewport)
{
	if (g_GfxDriver)
	{
		g_GfxDriver->BeginFrame(viewport);
	}
}

void Gfx::EndFrame()
{
	if (g_GfxDriver)
	{
		g_GfxDriver->EndFrame();
	}
}

void Gfx::Present()
{
	if (g_GfxDriver)
	{
		g_GfxDriver->Present();
	}
}

void Gfx::Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil)
{
	if (g_GfxDriver)
	{
		g_GfxDriver->Clear(color, depth, clearFlags, stencil);
	}
}

IGraphicShaderModule* Gfx::CompileShader(const ShaderDescription& desc)
{
	if (g_GfxDriver)
	{
		return g_GfxDriver->CompileShader(desc);
	}
	return NULL;
}