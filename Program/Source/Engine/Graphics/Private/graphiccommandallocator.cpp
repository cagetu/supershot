#include "foundation.h"
#include "graphiccommandallocator.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicCommandAllocator);

IGraphicCommandAllocator::IGraphicCommandAllocator(COMMANDLIST_TYPE commandQueueType)
	: m_CommandQueueType(commandQueueType)
{
}

IGraphicCommandAllocator::~IGraphicCommandAllocator()
{
}
