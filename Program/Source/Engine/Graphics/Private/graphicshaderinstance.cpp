// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "graphicshaderinstance.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicShaderInstance);

IGraphicShaderInstance::IGraphicShaderInstance()
{
}

IGraphicShaderInstance::~IGraphicShaderInstance()
{
}

const ShaderFeature::MASK& IGraphicShaderInstance::GetFeatureFlags() const
{
	return m_FeatureFlags;
}

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicShaderModuleInstance);

IGraphicShaderModuleInstance::IGraphicShaderModuleInstance()
	: m_ShaderTypes(ShaderType::_INVALID)
{
}

IGraphicShaderModuleInstance::~IGraphicShaderModuleInstance()
{
}
