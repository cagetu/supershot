#include "foundation.h"
#include "graphicrenderwindow.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicRenderWindow);

IGraphicRenderWindow::IGraphicRenderWindow()
	: m_Format(FMT_NONE)
	, m_bUseDepthSurface(false)
{
	__ConstructReference;
}

IGraphicRenderWindow::~IGraphicRenderWindow()
{
	__DestructReference;
}

bool IGraphicRenderWindow::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, bool bUseDepthSurface)
{
	m_WindowHandle = windowHandle;
	m_bUseDepthSurface = bUseDepthSurface;
	m_Format = format;
	return true;
}


