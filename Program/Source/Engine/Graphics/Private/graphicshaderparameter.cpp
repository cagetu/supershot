#include "foundation.h"
#include "graphicshaderparameter.h"

// Initialize
Map<String, int32> ShaderParameter::m_ParamRegistry;
Variant ShaderParameter::m_CommonValue[];

void ShaderParameter::Initialize()
{
	static const struct
	{
		const int32 pid;
		const TCHAR* sementic;
	} _list[] =
	{
		WORLD, TEXT("WORLD"),
		VIEW, TEXT("VIEW"),
		PROJECTION, TEXT("PROJECTION"),
		CAMERA, TEXT("CAMERA"),
		CAMERAPOSITION, TEXT("CAMERAPOSITION"),
		CAMERADIRECTION, TEXT("CAMERADIRECTION"),
		CAMERADISTANCE, TEXT("CAMERADISTANCE"),
		CAMERAFOV, TEXT("CAMERAFOV"),
		FRUSTUMCORNERS, TEXT("FRUSTUMCORNERS"),
		GLOBALLIGHTDIRECTION, TEXT("GLOBALLIGHTDIRECTION"),
		GLOBALLIGHTPOSITION, TEXT("GLOBALLIGHTPOSITION"),
		GLOBALLIGHTCOLOR, TEXT("GLOBALLIGHTCOLOR"),
		GLOBALAMBIENTCOLOR, TEXT("GLOBALAMBIENTCOLOR"),
		HEMISPHERECOLOR, TEXT("HEMISPHERECOLOR"),
		HEMISPHEREUPPER, TEXT("HEMISPHEREUPPER"),
		HEMISPHERELOWER, TEXT("HEMISPHERELOWER"),
		RIMLIGHTDIRECTION, TEXT("RIMLIGHTDIRECTION"),
		RIMLIGHTCOLOR, TEXT("RIMLIGHTCOLOR"),
		RIMLIGHTPARAM, TEXT("RIMLIGHTPARAM"),
		SCREENSIZE, TEXT("SCREENSIZE"),
		DIFFUSEMAP, TEXT("DIFFUSEMAP"),

		TMARRAY, TEXT("TMARRAY"),

		///////////////////////////////////////
		WORLDVIEWPROJECTION, TEXT("WORLDVIEWPROJECTION"),
		WORLDVIEW, TEXT("WORLDVIEW"),
		VIEWPROJECTION, TEXT("VIEWPROJECTION"),

		SCREENPROJSCALE, TEXT("SCREENPROJSCALE"),
		SCREENPROJBIAS, TEXT("SCREENPROJBIAS"),
		//QUICKGAUSSIANWEIGHTS, TEXT("QUICKGAUSSIANWEIGHTS"),
		//QUICKGAUSSIANOFFSETS, TEXT("QUICKGAUSSIANOFFSETS"),
	};

	// 복사 금지, 변경 금지
	for (auto const& element : _list)
	{
		Register(element.sementic, element.pid);
	}
	//for (int32 i = 0; i < _countof(_list); i++)
	//	Register(_list[i].sementic, _list[i].pid);
}

void ShaderParameter::Shutdown()
{
}

void ShaderParameter::Reset()
{
}

int32 ShaderParameter::Register(const String& sementic)
{
	IndexT index = m_ParamRegistry.FindIndex(sementic);
	if (index != INVALID_INDEX)
	{
		return m_ParamRegistry.ValueAt(index);
	}

	//
	static PID autoIncrement = _MAX_DEFINED_PARAM;

	PID uid = autoIncrement++;
	m_ParamRegistry.Insert(sementic, uid);
	return uid;
}

void ShaderParameter::Register(const String& sementic, PID uid)
{
	m_ParamRegistry.Insert(sementic, uid);
}

int32 ShaderParameter::Find(const String& sementic)
{
	IndexT index = m_ParamRegistry.FindIndex(sementic);
	if (index != INVALID_INDEX)
	{
		return m_ParamRegistry.ValueAt(index);
	}
	return -1;
}

void ShaderParameter::SetCommonValue(PID pid, const Variant& value)
{
	m_CommonValue[pid] = value;
}

const Variant& ShaderParameter::GetCommonValue(PID pid)
{
	return m_CommonValue[pid];
}