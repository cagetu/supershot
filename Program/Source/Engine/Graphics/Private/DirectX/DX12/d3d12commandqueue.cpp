// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX\DX12\d3d12commandqueue.h"
#include "DirectX\DX12\d3d12commandlist.h"
#include "DirectX\DX12\d3d12device.h"
#include "DirectX\DX12\d3d12fence.h"
#include "DirectX\DX12\d3d12utils.h"

//------------------------------------------------------------------------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12CommandQueue, IGraphicCommandQueue);

D3D12CommandQueue::D3D12CommandQueue(D3D12Device* device, COMMANDLIST_TYPE commandQueueType, D3D12_COMMAND_QUEUE_FLAGS queueFlags, D3D12_COMMAND_QUEUE_PRIORITY queuePriority, uint32 nodeMask)
	: IGraphicCommandQueue(commandQueueType)
{
	/*
		enum D3D12_COMMAND_LIST_TYPE
		{
			// GPU가 직접 실행할 수 있는 명령 리스트
			D3D12_COMMAND_LIST_TYPE_DIRECT	= 0,
			// GPU가 직접 실행할 수 없음 (Direct 명령 리스트가 필요하다)
			D3D12_COMMAND_LIST_TYPE_BUNDLE	= 1,
			// Compute Shader 르 위한 명령 버퍼
			D3D12_COMMAND_LIST_TYPE_COMPUTE	= 2,
			// Copy를 위한 명령 버퍼
			D3D12_COMMAND_LIST_TYPE_COPY	= 3
		} 	D3D12_COMMAND_LIST_TYPE;

		enum D3D12_COMMAND_QUEUE_FLAGS
		{
			// 기본 명령 큐
			D3D12_COMMAND_QUEUE_FLAG_NONE	= 0,
			// 명령 큐에 대하여 GPU 타임아웃을 비활성화
			D3D12_COMMAND_QUEUE_FLAG_DISABLE_GPU_TIMEOUT	= 0x1
		} 	D3D12_COMMAND_QUEUE_FLAGS;

		enum D3D12_COMMAND_QUEUE_PRIORITY
		{
			// 보통 우선 순위
			D3D12_COMMAND_QUEUE_PRIORITY_NORMAL	= 0,
			// 높은 우선 순위
			D3D12_COMMAND_QUEUE_PRIORITY_HIGH	= 100,
			D3D12_COMMAND_QUEUE_PRIORITY_GLOBAL_REALTIME	= 10000
		} 	D3D12_COMMAND_QUEUE_PRIORITY;
	*/

	D3D12_COMMAND_LIST_TYPE d3dCommandQueueType = D3D12::D3DCommandListType(commandQueueType);

	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = d3dCommandQueueType;
	queueDesc.Flags = queueFlags;
	queueDesc.Priority = queuePriority;
	queueDesc.NodeMask = nodeMask;		// 단일 GPU: 0

	ID3D12CommandQueue* pCommandQueue = nullptr;
	if (SUCCEEDED(device->GetDevice()->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&pCommandQueue))))
	{
		m_D3DQueue = pCommandQueue;
	}
}

D3D12CommandQueue::~D3D12CommandQueue()
{
	m_D3DQueue = nullptr;
}

void D3D12CommandQueue::Submit(D3D12CommandList* commandList)
{
	ASSERT(GetCommandListType() == commandList->GetCommandListType());

	ID3D12CommandList* list = commandList->GetHandle();
	m_D3DQueue->ExecuteCommandLists(1, &list);
}

void D3D12CommandQueue::Submit(const Array<D3D12CommandList*>& commandLists)
{
	if (commandLists.Num() > 0)
	{
		Array<ID3D12CommandList*> commandlistHandles;
		for (SizeT index = 0; index < commandLists.Num(); index++)
		{
			ASSERT(GetCommandListType() == commandLists[index]->GetCommandListType());

			ID3D12CommandList* listHandle = commandLists[index]->GetHandle();
			commandlistHandles.Add(listHandle);
		}

		m_D3DQueue->ExecuteCommandLists(commandLists.Num(), commandlistHandles.Data());
	}
}

// 새 펜스 지점을 설정하는 명령을 명령 대기열에 추가한다.
// Fence 값이 FenceValue가 되면 통지를 받겠다고 설정한다.
bool D3D12CommandQueue::Signal(D3D12Fence* fence, uint64 fenceValue)
{
	ASSERT(m_D3DQueue.IsValid());

	if (SUCCEEDED(m_D3DQueue->Signal(fence->GetHandle(), fenceValue)))
	{
		return true;
	}
	return false;
}

bool D3D12CommandQueue::Wait(D3D12Fence* fence, uint64 fenceValue)
{
	ASSERT(m_D3DQueue.IsValid());

	if (false == m_D3DQueue.IsValid())
		return false;

	if (SUCCEEDED(m_D3DQueue->Wait(fence->GetHandle(), fenceValue)))
	{
		return true;
	}
	return false;
}
