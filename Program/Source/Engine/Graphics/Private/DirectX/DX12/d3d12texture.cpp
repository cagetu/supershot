// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12texture.h"
#include "DirectX/DX12/d3d12device.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12Texture, IGraphicTexture);

D3D12Texture::D3D12Texture(D3D12Device* device)
	: m_Device(device)
	, m_Resource(nullptr)
{
}

D3D12Texture::~D3D12Texture()
{
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12RenderTargetTexture, D3D12Texture);

D3D12RenderTargetTexture::D3D12RenderTargetTexture(D3D12Device* device)
	: D3D12Texture(device)
{
}
D3D12RenderTargetTexture::D3D12RenderTargetTexture(D3D12Device* device, ID3D12Resource* textureHandle, const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView)
	: D3D12RenderTargetTexture(device)
{
	m_Resource = textureHandle;
	m_RenderTargetView = renderTaretView;
}
D3D12RenderTargetTexture::~D3D12RenderTargetTexture()
{
	SAFE_RELEASE(m_Resource);
}

void D3D12RenderTargetTexture::SetRenderTargetView(const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView)
{
	m_RenderTargetView = renderTaretView;
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12ColorBuffer, D3D12RenderTargetTexture);

D3D12ColorBuffer::D3D12ColorBuffer(D3D12Device* device)
	: D3D12RenderTargetTexture(device)
{
}
D3D12ColorBuffer::D3D12ColorBuffer(D3D12Device* device, ID3D12Resource* textureHandle, const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView)
	: D3D12RenderTargetTexture(device, textureHandle, renderTaretView)
{
}
D3D12ColorBuffer::~D3D12ColorBuffer()
{
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12DepthStencilBuffer, D3D12RenderTargetTexture);

D3D12DepthStencilBuffer::D3D12DepthStencilBuffer(D3D12Device* device)
	: D3D12RenderTargetTexture(device)
{
}
D3D12DepthStencilBuffer::D3D12DepthStencilBuffer(D3D12Device* device, ID3D12Resource* textureHandle, const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView)
	: D3D12RenderTargetTexture(device, textureHandle, renderTaretView)
{
}
D3D12DepthStencilBuffer::~D3D12DepthStencilBuffer()
{
}
