// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12header.h"
#include "d3d12DXGIFactory.h"

//static 초기화
IDXGIFactory2* DXGIFactory::m_DxGIFactory = nullptr;

bool DXGIFactory::Create(bool bIsDebug)
{
	UINT dxgiFactoryFlags = 0;

	// D3D12의 디버그층을 활성화한다.
	// Enable the debug layer (requires the Graphics Tools "optional feature").
	// NOTE: Enabling the debug layer after device creation will invalidate the active device.
	if (bIsDebug)
	{
		ID3D12Debug* debugController = nullptr;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();

			// Enable additional debug layers.
			dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
		}
		else
		{
			// 설정 > 앱 > 추가 기능 > Graphics Tools 설치 필요
		}
	}

	IDXGIFactory2* dxgiFactory = nullptr;

	if (SUCCEEDED(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&dxgiFactory))))
	{
		m_DxGIFactory = dxgiFactory;
		return true;
	}

	return false;
}

void DXGIFactory::Destroy()
{
	SAFE_RELEASE(m_DxGIFactory);
}

IDXGIFactory2* DXGIFactory::Get()
{
	return m_DxGIFactory;
}

