// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12utils.h"

#define D3DERR(x) case x: ErrorCodeText = TEXT(#x); break;
#define LOCTEXT_NAMESPACE "Developer.MessageLog"

#ifndef _FACD3D 
#define _FACD3D  0x876
#endif	//_FACD3D 
#ifndef MAKE_D3DHRESULT
#define _FACD3D  0x876
#define MAKE_D3DHRESULT( code )  MAKE_HRESULT( 1, _FACD3D, code )
#endif	//MAKE_D3DHRESULT

#if WITH_D3DX_LIBS
#ifndef D3DERR_INVALIDCALL
#define D3DERR_INVALIDCALL MAKE_D3DHRESULT(2156)
#endif//D3DERR_INVALIDCALL
#ifndef D3DERR_WASSTILLDRAWING
#define D3DERR_WASSTILLDRAWING MAKE_D3DHRESULT(540)
#endif//D3DERR_WASSTILLDRAWING
#endif

static String GetD3D12DeviceHungErrorString(HRESULT ErrorCode)
{
	String ErrorCodeText;

	switch (ErrorCode)
	{
		D3DERR(DXGI_ERROR_DEVICE_HUNG)
			D3DERR(DXGI_ERROR_DEVICE_REMOVED)
			D3DERR(DXGI_ERROR_DEVICE_RESET)
			D3DERR(DXGI_ERROR_DRIVER_INTERNAL_ERROR)
			D3DERR(DXGI_ERROR_INVALID_CALL)
	default:
		ErrorCodeText = SFormat(TEXT("%08X"), (int32)ErrorCode);
	}

	return ErrorCodeText;
}

static String GetD3D12ErrorString(HRESULT ErrorCode, ID3D12Device * Device)
{
	String ErrorCodeText;

	switch (ErrorCode)
	{
		D3DERR(S_OK);
		D3DERR(D3D11_ERROR_FILE_NOT_FOUND)
			D3DERR(D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS)
#if WITH_D3DX_LIBS
			D3DERR(D3DERR_INVALIDCALL)
			D3DERR(D3DERR_WASSTILLDRAWING)
#endif	//WITH_D3DX_LIBS
			D3DERR(E_FAIL)
			D3DERR(E_INVALIDARG)
			D3DERR(E_OUTOFMEMORY)
			D3DERR(DXGI_ERROR_INVALID_CALL)
			D3DERR(E_NOINTERFACE)
			D3DERR(DXGI_ERROR_DEVICE_REMOVED)
#if PLATFORM_WINDOWS
			EMBED_DXGI_ERROR_LIST(D3DERR, )
#endif
	default:
		ErrorCodeText = SFormat(TEXT("%08X"), (int32)ErrorCode);
	}

	if (ErrorCode == DXGI_ERROR_DEVICE_REMOVED && Device)
	{
		HRESULT hResDeviceRemoved = Device->GetDeviceRemovedReason();
		ErrorCodeText += String(TEXT(" with Reason: ")) + GetD3D12DeviceHungErrorString(hResDeviceRemoved);
	}

	return ErrorCodeText;
}

namespace D3D12
{

	void VerifyD3D12Result(HRESULT D3DResult, const char* Code, const char* Filename, uint32 Line, ID3D12Device* Device, String Message)
	{
		ASSERT(FAILED(D3DResult));

		const String& ErrorString = GetD3D12ErrorString(D3DResult, Device);
		utf8::string erroMsg = unicode::convert(ErrorString);
		utf8::string msg = unicode::convert(Message);

		//ULOG_UTF8(Log::Error, "%s failed \n at %s:%u \n with error %s\n%s", Code, Filename, Line, erroMsg.c_str(), msg.c_str());

		// Terminate with device removed but we don't have any GPU crash dump information
		//if (D3DResult == DXGI_ERROR_DEVICE_REMOVED)
		//{
		//	TerminateOnGPUCrash(Device, nullptr, 0);
		//}
		//else if (D3DResult == E_OUTOFMEMORY)
		//{
		//	TerminateOnOutOfMemory(D3DResult, false);
		//}

		//UE_LOG(LogD3D12RHI, Fatal, TEXT("%s failed \n at %s:%u \n with error %s\n%s"), ANSI_TO_TCHAR(Code), ANSI_TO_TCHAR(Filename), Line, *ErrorString, *Message);
	}

	DXGI_FORMAT D3DFormat(FORMAT_TYPE format)
	{
		static struct _SURFACEFORMAT
		{
			FORMAT_TYPE format;
			DXGI_FORMAT d3dformat;
		} __Formats[] =
		{
			FMT_NONE, DXGI_FORMAT_UNKNOWN,
			FMT_R8G8B8A8, DXGI_FORMAT_R8G8B8A8_UNORM,
			FMT_B8G8R8A8, DXGI_FORMAT_B8G8R8A8_UNORM,
			FMT_D32, DXGI_FORMAT_D32_FLOAT,
			FMT_D24S8, DXGI_FORMAT_D24_UNORM_S8_UINT,
			FMT_D16, DXGI_FORMAT_D16_UNORM,
			// 부동 소수점 포멧
			FMT_R16F, DXGI_FORMAT_R16_FLOAT,
		};
		for (int32 index = 0; index < _countof(__Formats); index++)
		{
			if (__Formats[index].format == format)
			{
				return __Formats[index].d3dformat;
			}
		}

		return DXGI_FORMAT_UNKNOWN;
	}

	D3D12_COMMAND_LIST_TYPE D3DCommandListType(COMMANDLIST_TYPE commandType)
	{
		D3D12_COMMAND_LIST_TYPE d3dCommandQueueType = D3D12_COMMAND_LIST_TYPE_DIRECT;

		if (commandType == COMMANDLIST_TYPE::BUNDLE)
			d3dCommandQueueType = D3D12_COMMAND_LIST_TYPE_BUNDLE;
		else if (commandType == COMMANDLIST_TYPE::COMPUTE)
			d3dCommandQueueType = D3D12_COMMAND_LIST_TYPE_COMPUTE;
		else if (commandType == COMMANDLIST_TYPE::COPY)
			d3dCommandQueueType = D3D12_COMMAND_LIST_TYPE_COPY;

		return d3dCommandQueueType;
	}

} // end of namespace D3D12
