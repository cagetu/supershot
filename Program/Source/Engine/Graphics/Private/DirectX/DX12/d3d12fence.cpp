#include "foundation.h"
#include "DirectX/DX12/d3d12fence.h"
#include "DirectX/DX12/d3d12device.h"

//------------------------------------------------------------------
//------------------------------------------------------------------

// Fence: 초기값 설정
D3D12Fence::D3D12Fence(D3D12Device* device, uint64 initialValue, D3D12_FENCE_FLAGS fenceFlags)
{
	m_D3DFence = nullptr;
	m_FenceFlags = D3D12_FENCE_FLAG_NONE;

	ID3D12Fence* pFence = nullptr;
	if (SUCCEEDED(device->GetDevice()->CreateFence(initialValue, fenceFlags, IID_PPV_ARGS(&pFence))))
	{
		m_D3DFence = pFence;
		m_FenceFlags = fenceFlags;
	}
}

D3D12Fence::~D3D12Fence()
{
	m_D3DFence.Empty();
}


// GPU가 현재 Fence 지점에 도달했으면, 이벤트를 발동한다.
//	-  첫번째 인자로 들어오는 값으로 Fence 오브젝트 값일 바뀌면, 두번째 인자로 들어오는 Handle 이벤트를 True로 만들어준다.
bool D3D12Fence::SetEventOnCompletion(uint64 value, HANDLE eventHandle)
{
	return SUCCEEDED(m_D3DFence->SetEventOnCompletion(value, eventHandle));
}

bool D3D12Fence::Signal(uint64 value)
{
	return SUCCEEDED(m_D3DFence->Signal(value));
}

uint64 D3D12Fence::GetCompletedValue() const
{
	return m_D3DFence->GetCompletedValue();
}

//------------------------------------------------------------------
//------------------------------------------------------------------
D3D12FencePool::D3D12FencePool(D3D12Device* device)
{
	m_Device = device;
}
D3D12FencePool::~D3D12FencePool()
{
	CHECK(m_UsedFences.IsEmpty(), TEXT("UsedFences must be empty!!!"));
	CHECK(m_FreeFences.IsEmpty(), TEXT("FreeFences must be empty!!!"));
}

D3D12Fence* D3D12FencePool::AllocateFence()
{
	CSScopeLock Lock(CS);

	if (!m_FreeFences.IsEmpty())
	{
		D3D12Fence* fence = m_FreeFences.Pop();
		m_UsedFences.Add(fence);

		return fence;
	}

	D3D12Fence* fence = NewObject(D3D12Fence, m_Device);
	m_UsedFences.Add(fence);
	return fence;
}

void D3D12FencePool::ReleaseFence(D3D12Fence* fence, uint64 currentFenceValue)
{
	if (nullptr == fence)
		return;

	CSScopeLock Lock(CS);

	m_UsedFences.Remove(fence);
	m_FreeFences.Add(fence);
	fence = nullptr;
}

void D3D12FencePool::Destroy()
{
	CSScopeLock Lock(CS);

	/// Need Threading Lock
	CHECK(m_UsedFences.IsEmpty(), TEXT("UsedFences must be empty!!!"));

	for (auto it = m_FreeFences.begin(); it != m_FreeFences.end(); it++)
		DeleteObject(*it);
	m_FreeFences.Clear();
}
