// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12renderwindow.h"
#include "DirectX/DX12/d3d12swapchain.h"
#include "DirectX/DX12/d3d12device.h"
#include "DirectX/DX12/d3d12commandqueue.h"
#include "DirectX/DX12/d3d12commandallocator.h"
#include "DirectX/DX12/d3d12commandlist.h"
#include "DirectX/DX12/d3d12utils.h"
#include "DirectX/DX12/d3d12descriptorheap.h"
#include "DirectX/DX12/d3d12texture.h"
#include "graphicsettings.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12RenderWindow, IGraphicRenderWindow);

D3D12RenderWindow::D3D12RenderWindow(D3D12Device* device)
{
	m_Device = device;
	m_SwapChain = nullptr;
	m_DepthStencilDescriptorHeap = nullptr;
	m_DepthStencilBuffer = nullptr;

	m_CommandQueue = nullptr;
	m_CommandAllocator = nullptr;

	for (int32 index = 0; index < _NUM_SWAP_BUFFERS; index++)
	{
		m_CommandLists[index] = nullptr;
	}
}

D3D12RenderWindow::~D3D12RenderWindow()
{
	Destroy();

	m_Device = nullptr;
}

bool D3D12RenderWindow::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, bool bUseDepthSurface)
{
	if (!IGraphicRenderWindow::Create(windowHandle, format, bUseDepthSurface))
		return false;
	
	m_CommandQueue = m_Device->CreateCommandQueue(COMMANDLIST_TYPE::DIRECT, D3D12_COMMAND_QUEUE_FLAG_NONE);
	if (!m_CommandQueue)
		return false;

	m_CommandAllocator = m_Device->CreateCommandAllocator(COMMANDLIST_TYPE::DIRECT);
	if (!m_CommandAllocator)
		return false;

	D3D12Swapchain* swapchain = NewObject(D3D12Swapchain, m_Device);
	if (false == swapchain->Create(windowHandle, format, m_CommandQueue, _NUM_SWAP_BUFFERS))
	{
		DeleteObject(swapchain);
		return false;
	}

	m_SwapChain = swapchain;

	CreateCommandLists(_NUM_SWAP_BUFFERS);

	if (bUseDepthSurface)
	{
		// CreateDepthBuffer
	}

	return true;
}

void D3D12RenderWindow::Destroy()
{
	if (m_CommandQueue)
	{
		m_Device->DestroyCommandQueue(m_CommandQueue);
		m_CommandQueue = nullptr;
	}
	if (m_CommandAllocator)
	{
		m_Device->DestroyCommandAllocator(m_CommandAllocator);
		m_CommandAllocator = nullptr;
	}

	DestroyCommandLists();

	DestroyDepthBuffer();
	m_SwapChain = nullptr;
}

void D3D12RenderWindow::Resize(const WindowHandle& windowHandle)
{
}

void D3D12RenderWindow::Present()
{
}

void D3D12RenderWindow::Begin()
{
}

void D3D12RenderWindow::End()
{
}

bool D3D12RenderWindow::CreateDepthBuffer(uint32 width, uint32 height, FORMAT_TYPE format)
{
	D3D12DescriptorHeap* dsvHeap = new D3D12DescriptorHeap();
	if (!dsvHeap->Create(m_Device, 1, D3D12_DESCRIPTOR_HEAP_TYPE_DSV, D3D12_DESCRIPTOR_HEAP_FLAG_NONE))
	{
		delete dsvHeap;
		return false;
	}
	m_DepthStencilDescriptorHeap = dsvHeap;

	D3D12_RESOURCE_DESC depthStencilDesc = {};
	depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	depthStencilDesc.Alignment = 0;
	// dpethStencil의 속성을 서술한다.
	depthStencilDesc.Width = width;
	depthStencilDesc.Height = height;
	depthStencilDesc.Format = D3D12::D3DFormat(format);
	depthStencilDesc.DepthOrArraySize = 1;
	depthStencilDesc.MipLevels = 1;
	// Multi Sample에 대해 서술 
	depthStencilDesc.SampleDesc.Count = GraphicSettings::Get().MultiSampleType;
	depthStencilDesc.SampleDesc.Quality = GraphicSettings::Get().MultiSampleQuality;
	// 텍스쳐 레이아웃: 여기서는 지정 안 함
	depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	ID3D12Resource* depthStencilResource = nullptr;

	D3D12_CLEAR_VALUE clearValue;
	clearValue.Format = D3D12::D3DFormat(format);
	clearValue.DepthStencil.Depth = 1.0f;
	clearValue.DepthStencil.Stencil = 0.0f;
	if (FAILED(m_Device->GetDevice()->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthStencilDesc,
		D3D12_RESOURCE_STATE_COMMON, // 자원에서의 초기 상태 지정 (현재 어떤 용도를 나타내는가)
		&clearValue,
		IID_PPV_ARGS(&depthStencilResource))))
	{
		return false;
	}

	// 생성된 렌더 대상 뷰가 저장될 서술자의 핸들
	D3D12_CPU_DESCRIPTOR_HANDLE descriptorHandleCPU = m_DepthStencilDescriptorHeap->GetDescriptorHandleCPU(0);

	// 매개변수가 null일 경우, 자원을 생성할 때 지정한 자료 형색을 적용해서 그 자원의 첫 번째 밉맵 수준에 대한 뷰를 생성한다.
	const D3D12_DEPTH_STENCIL_VIEW_DESC* depthStencilViewDesc = nullptr;
	m_Device->GetDevice()->CreateDepthStencilView(depthStencilResource, depthStencilViewDesc, descriptorHandleCPU);

	// 생성한 BackBuffer 정보들
	m_DepthStencilBuffer = new D3D12DepthStencilBuffer(m_Device, depthStencilResource, descriptorHandleCPU);

	// 자원 전이!!!!

	return true;
}

void D3D12RenderWindow::DestroyDepthBuffer()
{
}

bool D3D12RenderWindow::CreateCommandLists(int32 requireBufferCount)
{
	//D3D12CommandList* commandList = new D3D12CommandList();
	//if (!commandList->Create(m_Device, 0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_CommandAllocator, nullptr))
	//{
	//	return false;
	//}

	return true;
}

void D3D12RenderWindow::DestroyCommandLists()
{

}

