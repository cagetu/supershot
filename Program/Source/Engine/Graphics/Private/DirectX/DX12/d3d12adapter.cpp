// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12adapter.h"

//------------------------------------------------------------------
D3D12Adapter::D3D12Adapter(const D3D12AdapterDesc& desc)
	: m_AdapterDesc(desc)
	, m_AdapterHandle(nullptr)
{
	__ConstructReference;
}

D3D12Adapter::~D3D12Adapter()
{
	Destroy();

	__DestructReference;
}

void D3D12Adapter::Create(IDXGIAdapter* inAdapter)
{
	m_AdapterHandle = inAdapter;
}

void D3D12Adapter::Destroy()
{
	SAFE_RELEASE(m_AdapterHandle);
}

