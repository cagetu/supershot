// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include <dxgi1_3.h>
#include <dxgi1_4.h>
#include <dxgi1_6.h>
#include <delayimp.h>

//------------------------------------------------------------------
/**	@class	DirectX Graphic Interface Factory 클래스
*	@desc	DirectX Graphic Interface Factory 객체 클래스
*/
//------------------------------------------------------------------
class DXGIFactory
{
public:
	static bool Create(bool bIsDebug = false);
	static void Destroy();

	static IDXGIFactory2* Get();

public:
	static IDXGIFactory2* m_DxGIFactory;
};
