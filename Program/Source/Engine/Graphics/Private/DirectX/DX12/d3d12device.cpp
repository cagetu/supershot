#include "foundation.h"
#include "DirectX/DX12/d3d12device.h"
#include "DirectX/DX12/d3d12adapter.h"
#include "DirectX/DX12/d3d12commandqueue.h"
#include "DirectX/DX12/d3d12commandallocator.h"
#include "DirectX/DX12/d3d12descriptorpool.h"
#include "DirectX/DX12/d3d12utils.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12Device, IGraphicDevice);

D3D12Device::D3D12Device(const Ptr<D3D12Adapter>& adapter)
{
	ID3D12Device* pDevice = nullptr;
	VERIFYD3D12RESULT(D3D12CreateDevice(adapter->GetHandle(), adapter->GetDesc().MaxSupportedFeatureLevel, IID_PPV_ARGS(&pDevice)));

	m_Device = pDevice;
	m_Adapter = adapter;
}
D3D12Device::~D3D12Device()
{
	m_Adapter.Empty();
	m_Device.Empty();
}

D3D12CommandAllocator* D3D12Device::CreateCommandAllocator(COMMANDLIST_TYPE commandListType)
{
	D3D12CommandAllocator* commandAllocator = new D3D12CommandAllocator(this, commandListType);
	return commandAllocator;
}

void D3D12Device::DestroyCommandAllocator(D3D12CommandAllocator* commandAllocator)
{
	if (commandAllocator)
		delete commandAllocator;
	commandAllocator = nullptr;
}

D3D12CommandQueue* D3D12Device::CreateCommandQueue(COMMANDLIST_TYPE commandListType, D3D12_COMMAND_QUEUE_FLAGS commandQueueFlags)
{
	D3D12CommandQueue* commandQueue = new D3D12CommandQueue(this, commandListType, commandQueueFlags);
	return commandQueue;
}

void D3D12Device::DestroyCommandQueue(D3D12CommandQueue* commandQueue)
{
	if (commandQueue)
	{
		delete commandQueue;
	}
	commandQueue = nullptr;
}

int32 D3D12Device::CheckSupportMSAA(FORMAT_TYPE format)
{
	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msaaLevels;
	msaaLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	msaaLevels.Format = D3D12::D3DFormat(format);
	msaaLevels.NumQualityLevels = 0;
	msaaLevels.SampleCount = 4;

	int32 msaaQualityLevel = 0;
	GetDevice()->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS, &msaaQualityLevel, sizeof(D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS));

	return msaaQualityLevel;
}

//void D3D12Device::SetViewport(int32 x, int32 y, int32 width, int32 height, float minDepth, float maxDepth)
//{
//	D3D12_VIEWPORT vp;
//	vp.TopLeftX = (float)x;
//	vp.TopLeftY = (float)y;
//	vp.Width = (float)width;
//	vp.Height = (float)height;
//	vp.MinDepth = minDepth;
//	vp.MaxDepth = maxDepth;
//}
