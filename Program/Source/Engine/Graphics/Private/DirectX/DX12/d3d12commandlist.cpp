// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12commandlist.h"
#include "DirectX/DX12/d3d12commandallocator.h"
#include "DirectX/DX12/d3d12pipelinestate.h"
#include "DirectX/DX12/d3d12device.h"
#include "DirectX/DX12/d3d12utils.h"

//------------------------------------------------------------------------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12CommandList, IGraphicCommandList);

D3D12CommandList::D3D12CommandList()
{
	m_D3DCommandList = nullptr;
	m_CommandAllocator = nullptr;
	m_bOpended = false;
}

D3D12CommandList::~D3D12CommandList()
{
	Destroy();
}

bool D3D12CommandList::Create(D3D12Device* device, uint32 nodeMask, D3D12CommandAllocator* commandAllocator, D3D12PipelineState* initState)
{
	D3D12_COMMAND_LIST_TYPE d3dCommandListType = D3D12::D3DCommandListType(commandAllocator->GetCommandQueueType());

	ID3D12GraphicsCommandList* pCommandList = nullptr;
	if (FAILED(device->GetDevice()->CreateCommandList(nodeMask, d3dCommandListType,
		commandAllocator->GetHandle(), initState ? initState->GetHandle() : nullptr,
		IID_PPV_ARGS(&pCommandList))))
	{
		return false;
	}

	m_CommandAllocator = commandAllocator;
	m_D3DCommandList = pCommandList;

	Close();

	return true;
}

void D3D12CommandList::Destroy()
{
	m_D3DCommandList.Empty();
	m_CommandAllocator = nullptr;
}

bool D3D12CommandList::Close()
{
	if (SUCCEEDED(m_D3DCommandList->Close()))
	{
		m_bOpended = false;
		return true;
	}
	return false;
}

COMMANDLIST_TYPE D3D12CommandList::GetCommandListType() const
{
	return m_CommandAllocator->GetCommandQueueType();
}

//------------------------------------------------------------------
// @comment: Reset을 호출 하려면 명령 목록이 닫혀 있어야 한다.
//------------------------------------------------------------------
bool D3D12CommandList::Reset(D3D12CommandAllocator* commandAllocator, D3D12PipelineState* initState)
{
	CHECK(false == m_bOpended, TEXT("[D3D12CommandList::Reset] m_bClosed == false"));

	return SUCCEEDED(m_D3DCommandList->Reset(commandAllocator->GetHandle(), initState ? initState->GetHandle() : nullptr));
}

void D3D12CommandList::ClearState(D3D12PipelineState* pipelineState)
{
	m_bOpended = true;

	m_D3DCommandList->ClearState(pipelineState->GetHandle());
}

