// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12device.h"
#include "DirectX/DX12/d3d12Swapchain.h"
#include "DirectX/DX12/d3d12commandqueue.h"
#include "DirectX/DX12/d3d12utils.h"
#include "DirectX/DX12/d3d12descriptorheap.h"
#include "DirectX/DX12/d3d12texture.h"
#include "d3d12DXGIFactory.h"
#include "graphicsettings.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12Swapchain, IGraphicSwapchain);

D3D12Swapchain::D3D12Swapchain(D3D12Device* device)
{
	m_SwapChainHandle = nullptr;
	m_Device = device;
	m_BackBufferDescriptorHeap = nullptr;
}
D3D12Swapchain::~D3D12Swapchain()
{
	Destroy();

	m_Device = nullptr;
}

bool D3D12Swapchain::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, IGraphicCommandQueue* commandQueue, uint32 surfaceCount)
{
	if (!CreateSwapChain(windowHandle, commandQueue, format, surfaceCount))
		return false;

	if (!CreateBackBuffers(surfaceCount))
		return false;

	return true;
}

void D3D12Swapchain::Destroy()
{
	DestroyBackBuffers();
	DestroySwapChain();
}

bool D3D12Swapchain::Resize(const WindowHandle& windowHandle)
{
	return false;
}

void D3D12Swapchain::Present()
{
}

/*	스왑체인 생성
*	
*/
bool D3D12Swapchain::CreateSwapChain(const WindowHandle& windowHandle, IGraphicCommandQueue* commandQueue, FORMAT_TYPE backBufferFormat, uint32 backBufferCount)
{
	D3D12CommandQueue* d3d12commandQueue = Cast<D3D12CommandQueue>(commandQueue);
	if (!d3d12commandQueue)
		return false;

	DXGI_SWAP_CHAIN_DESC swapchainDesc;
	// BackBuffer를 렌더 대상으로 사용할 것이다.
	swapchainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	// Double Buffer or Triple Buffer
	swapchainDesc.BufferCount = backBufferCount;
	// backbuffer의 속성을 서술한다.
	swapchainDesc.BufferDesc.Width = windowHandle.Width();
	swapchainDesc.BufferDesc.Height = windowHandle.Height();
	swapchainDesc.BufferDesc.Format = D3D12::D3DFormat(backBufferFormat);
	if (GraphicSettings::Get().bVSyncEnable)
	{
		swapchainDesc.BufferDesc.RefreshRate.Numerator = GraphicSettings::Get().RefreshRateNumerator;
		swapchainDesc.BufferDesc.RefreshRate.Denominator = GraphicSettings::Get().RefreshRateDenominator;
	}
	else
	{
		swapchainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapchainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}
	swapchainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapchainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	// Multi Sample에 대해 서술 
	swapchainDesc.SampleDesc.Count = GraphicSettings::Get().MultiSampleType;
	swapchainDesc.SampleDesc.Quality = GraphicSettings::Get().MultiSampleQuality;
	// 렌더링 결과가 표시될 윈도우 핸들
	swapchainDesc.OutputWindow = windowHandle.Handle();
	swapchainDesc.Windowed = windowHandle.IsFullScreen();
	// swapchain 옵션??? TODO: 공부할 필요 있음
	swapchainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	//	DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH
	//	- 응용 프로그램이 전체화면으로 전환될 때, 응용 프로그램의 현재 크기에 가장 잘 맞는 디스플레이 모드를 선택한다.
	//	- 이 플래그를 지정하지 않으면 전체화면으로 전환될 때, 현재 데스크톱 디스플레이 모드를 선택한다. 
	swapchainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	IDXGISwapChain* swapChain = nullptr;
	if (FAILED(DXGIFactory::Get()->CreateSwapChain(d3d12commandQueue->GetHandle(), &swapchainDesc, &swapChain)))
	{
		return false;
	}

	// Next upgrade the IDXGISwapChain to a IDXGISwapChain3 interface and store it in a private member variable named m_swapChain.
	// This will allow us to use the newer functionality such as getting the current back buffer index.
	IDXGISwapChain3* swapChain3 = nullptr;
	if (FAILED(swapChain->QueryInterface(__uuidof(IDXGISwapChain3), (void**)&m_SwapChainHandle)))
	{
		return false;
	}

	return true;
}

void D3D12Swapchain::DestroySwapChain()
{
	SAFE_RELEASE(m_SwapChainHandle);
}

bool D3D12Swapchain::CreateBackBuffers(uint32 surfaceCount)
{
	D3D12DescriptorHeap* backbufferDescriptorHeap = new D3D12DescriptorHeap();
	if (!backbufferDescriptorHeap->Create(m_Device, surfaceCount, D3D12_DESCRIPTOR_HEAP_TYPE_RTV, D3D12_DESCRIPTOR_HEAP_FLAG_NONE))
	{
		delete backbufferDescriptorHeap;
		return false;
	}

	m_BackBufferDescriptorHeap = backbufferDescriptorHeap;

	// BackBuffer Surface를 생성.
	for (uint32 bufferIndex = 0; bufferIndex < surfaceCount; bufferIndex++)
	{
		ID3D12Resource* d3dbackBufferResource = nullptr;
		if (SUCCEEDED(m_SwapChainHandle->GetBuffer(bufferIndex, __uuidof(ID3D12Resource), (void**)&d3dbackBufferResource)))
		{
			// 생성된 렌더 대상 뷰가 저장될 서술자의 핸들
			D3D12_CPU_DESCRIPTOR_HANDLE descriptorHandleCPU = m_BackBufferDescriptorHeap->GetDescriptorHandleCPU(bufferIndex);

			// 매개변수가 null일 경우, 자원을 생성할 때 지정한 자료 형색을 적용해서 그 자원의 첫 번째 밉맵 수준에 대한 뷰를 생성한다.
			const D3D12_RENDER_TARGET_VIEW_DESC* d3dRTVDesc = nullptr;
			m_Device->GetDevice()->CreateRenderTargetView(d3dbackBufferResource, d3dRTVDesc, descriptorHandleCPU);

			// 생성한 BackBuffer 정보들
			m_BackBuffers.Add(new D3D12ColorBuffer(m_Device, d3dbackBufferResource, descriptorHandleCPU));
		}
	}

	return true;
}

void D3D12Swapchain::DestroyBackBuffers()
{
	for (int32 index = 0; index < m_BackBuffers.Num(); index++)
	{
		delete m_BackBuffers[index];
	}
	m_BackBuffers.Clear();

	if (m_BackBufferDescriptorHeap)
	{
		delete m_BackBufferDescriptorHeap;
	}
	m_BackBufferDescriptorHeap = nullptr;
}

uint32 D3D12Swapchain::GetCurrentBackbufferIndex() const
{
	return m_SwapChainHandle->GetCurrentBackBufferIndex();
}

D3D12ColorBuffer* D3D12Swapchain::GetBackBuffer(uint32 backBufferIndex) const
{
	if (m_BackBuffers.IsValidIndex(backBufferIndex))
	{
		return m_BackBuffers[backBufferIndex];
	}
	return nullptr;
}
