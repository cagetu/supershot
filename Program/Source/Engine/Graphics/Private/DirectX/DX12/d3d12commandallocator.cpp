// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX\DX12\d3d12commandallocator.h"
#include "DirectX\DX12\d3d12commandlist.h"
#include "DirectX\DX12\d3d12device.h"
#include "DirectX\DX12\d3d12utils.h"

//------------------------------------------------------------------------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12CommandAllocator, IGraphicCommandAllocator);

D3D12CommandAllocator::D3D12CommandAllocator(D3D12Device* device, COMMANDLIST_TYPE commandQueueType)
	: IGraphicCommandAllocator(commandQueueType)
{
	D3D12_COMMAND_LIST_TYPE D3DCommandQueueType = D3D12::D3DCommandListType(commandQueueType);

	ID3D12CommandAllocator* pCommandAllocator = nullptr;
	if (SUCCEEDED(device->GetDevice()->CreateCommandAllocator(D3DCommandQueueType, IID_PPV_ARGS(&pCommandAllocator))))
	{
		m_D3DAllocator = pCommandAllocator;
	}
}

D3D12CommandAllocator::~D3D12CommandAllocator()
{
	m_D3DAllocator.Empty();
}

void D3D12CommandAllocator::Reset()
{
	if (m_D3DAllocator.IsValid())
	{
		VERIFYD3D12RESULT(m_D3DAllocator->Reset());
	}
}
