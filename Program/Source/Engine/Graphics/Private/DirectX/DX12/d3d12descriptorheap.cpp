// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12descriptorheap.h"
#include "DirectX/DX12/d3d12device.h"

inline D3D12_DESCRIPTOR_HEAP_TYPE Translate(DESCRIPTOR_HEAP_TYPE InHeapType)
{
	switch (InHeapType)
	{
	case DESCRIPTOR_HEAP_TYPE::STANDARD:     return D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	case DESCRIPTOR_HEAP_TYPE::RENDERTARGET: return D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	case DESCRIPTOR_HEAP_TYPE::DEPTHSTENCIL: return D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	case DESCRIPTOR_HEAP_TYPE::SAMPLER:      return D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
	default: ASSERT(0);
	}
}

D3D12DescriptorHeap::D3D12DescriptorHeap()
	: m_HeapHandle(nullptr)
	, m_HeapType(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
	, m_HeapFlags(D3D12_DESCRIPTOR_HEAP_FLAG_NONE)
	, m_DescriptorSize(0)
{
}

D3D12DescriptorHeap::~D3D12DescriptorHeap()
{
	Destroy();
}

bool D3D12DescriptorHeap::Create(D3D12Device* device, uint32 numDescriptors, D3D12_DESCRIPTOR_HEAP_TYPE descriptorType, D3D12_DESCRIPTOR_HEAP_FLAGS descriptorFlags)
{
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
	// 서술자 타입
	heapDesc.Type = descriptorType;
	// 생성하고자 하는 서술자 개수
	heapDesc.NumDescriptors = numDescriptors;
	heapDesc.Flags = descriptorFlags;
	heapDesc.NodeMask = 0;

	ID3D12DescriptorHeap* desriptorHeap = nullptr;
	if (FAILED(device->GetDevice()->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&desriptorHeap))))
	{
		return false;
	}

	m_HeapHandle = desriptorHeap;
	m_HeapType = descriptorType;
	m_HeapFlags = descriptorFlags;
	m_NumDescriptors = numDescriptors;

	// GPU 하드웨어에 따라서 Descriptor의 크기가 달라진다. Descriptor에 대한 메모리 사이즈를 얻어온다.
	m_DescriptorSize = device->GetDevice()->GetDescriptorHandleIncrementSize(descriptorType);
	return true;
}

void D3D12DescriptorHeap::Destroy()
{
	SAFE_RELEASE(m_HeapHandle);
}

D3D12_CPU_DESCRIPTOR_HANDLE D3D12DescriptorHeap::GetDescriptorHandleCPU(uint32 offsetInDescriptor)
{
	return offsetInDescriptor > 0 ? 
		CD3DX12_CPU_DESCRIPTOR_HANDLE(m_HeapHandle->GetCPUDescriptorHandleForHeapStart(), offsetInDescriptor, m_DescriptorSize) : 
		m_HeapHandle->GetCPUDescriptorHandleForHeapStart();
}

D3D12_GPU_DESCRIPTOR_HANDLE D3D12DescriptorHeap::GetDescriptorHandleGPU(uint32 offsetInDescriptor)
{
	const CD3DX12_GPU_DESCRIPTOR_HANDLE baseHandle((m_HeapFlags & D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE) ? m_HeapHandle->GetGPUDescriptorHandleForHeapStart() : CD3DX12_GPU_DESCRIPTOR_HANDLE{});
	return offsetInDescriptor > 0 ? CD3DX12_GPU_DESCRIPTOR_HANDLE(baseHandle, offsetInDescriptor, m_DescriptorSize) : baseHandle;
}
