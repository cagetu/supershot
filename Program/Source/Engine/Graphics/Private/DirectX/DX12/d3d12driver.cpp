// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX12/d3d12header.h"
#include "DirectX/DX12/d3d12driver.h"
#include "DirectX/DX12/d3d12device.h"
#include "DirectX/DX12/d3d12adapter.h"
#include "DirectX/DX12/d3d12renderwindow.h"
#include "DirectX/DX12/d3d12shadercompiler.h"
#include "d3d12DXGIFactory.h"

inline void ThrowIfFailed(HRESULT hr)
{
	ASSERT(SUCCEEDED(hr));
}

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D12Driver, IGraphicDriver);

D3D12Driver::D3D12Driver()
	: m_Device(nullptr)
	, m_RenderWindow(nullptr)
	, m_ShaderCompiler(nullptr)
{
}
D3D12Driver::~D3D12Driver()
{
	Destroy();

	DXGIFactory::Destroy();
}

static inline int D3D12_PreferAdapterVendor()
{
	//if (FParse::Param(FCommandLine::Get(), TEXT("preferAMD")))
	//{
	//	return 0x1002;
	//}

	//if (FParse::Param(FCommandLine::Get(), TEXT("preferIntel")))
	//{
	//	return 0x8086;
	//}

	//if (FParse::Param(FCommandLine::Get(), TEXT("preferNvidia")))
	//{
	//	return 0x10DE;
	//}

	return -1;
}

static bool bIsQuadBufferStereoEnabled = false;

/** This function is used as a SEH filter to catch only delay load exceptions. */
static bool IsDelayLoadException(PEXCEPTION_POINTERS ExceptionPointers)
{
#if WINVER > 0x502	// Windows SDK 7.1 doesn't define VcppException
	switch (ExceptionPointers->ExceptionRecord->ExceptionCode)
	{
	case VcppException(ERROR_SEVERITY_ERROR, ERROR_MOD_NOT_FOUND):
	case VcppException(ERROR_SEVERITY_ERROR, ERROR_PROC_NOT_FOUND):
		return EXCEPTION_EXECUTE_HANDLER;
	default:
		return EXCEPTION_CONTINUE_SEARCH;
	}
#else
	return EXCEPTION_EXECUTE_HANDLER;
#endif
}

#define CA_SUPPRESS( WarningNumber )

/**
 * Returns the minimum D3D feature level required to create based on
 * command line parameters.
 */
static D3D_FEATURE_LEVEL GetRequiredD3DFeatureLevel()
{
	return D3D_FEATURE_LEVEL_11_0;
}

/**
 * Attempts to create a D3D12 device for the adapter using at minimum MinFeatureLevel.
 * If creation is successful, true is returned and the max supported feature level is set in OutMaxFeatureLevel.
 */
static bool SafeTestD3D12CreateDevice(IDXGIAdapter* Adapter, D3D_FEATURE_LEVEL MinFeatureLevel, D3D_FEATURE_LEVEL& OutMaxFeatureLevel, uint32& OutNumDeviceNodes)
{
	const D3D_FEATURE_LEVEL FeatureLevels[] =
	{
		// Add new feature levels that the app supports here.
		//D3D_FEATURE_LEVEL_12_2,
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0
	};

	__try
	{
		ID3D12Device* pDevice = nullptr;
		if (SUCCEEDED(D3D12CreateDevice(Adapter, MinFeatureLevel, IID_PPV_ARGS(&pDevice))))
		{
			// Determine the max feature level supported by the driver and hardware.
			D3D_FEATURE_LEVEL MaxFeatureLevel = MinFeatureLevel;
			D3D12_FEATURE_DATA_FEATURE_LEVELS FeatureLevelCaps = {};
			FeatureLevelCaps.pFeatureLevelsRequested = FeatureLevels;
			FeatureLevelCaps.NumFeatureLevels = _countof(FeatureLevels);
			if (SUCCEEDED(pDevice->CheckFeatureSupport(D3D12_FEATURE_FEATURE_LEVELS, &FeatureLevelCaps, sizeof(FeatureLevelCaps))))
			{
				MaxFeatureLevel = FeatureLevelCaps.MaxSupportedFeatureLevel;
			}

			OutMaxFeatureLevel = MaxFeatureLevel;
			OutNumDeviceNodes = pDevice->GetNodeCount();

			pDevice->Release();
			return true;
		}
	}
	__except (IsDelayLoadException(GetExceptionInformation()))
	{
		// We suppress warning C6322: Empty _except block. Appropriate checks are made upon returning. 
		CA_SUPPRESS(6322);
	}

	return false;
}

bool D3D12Driver::Create(const WindowHandle& windowHandle)
{
	if (!CreateDevice())
		return false;

	WindowHandle windowInfo = windowHandle;

	if (!CreateRenderWindow(windowInfo))
		return false;

	m_ShaderCompiler = new D3D12ShaderCompiler();

	return true;
}

void D3D12Driver::Destroy()
{
	if (m_RenderWindow)
		delete m_RenderWindow;
	m_RenderWindow = nullptr;

	if (m_Device)
		delete m_Device;
	m_Device = nullptr;

	if (m_ShaderCompiler)
		delete m_ShaderCompiler;
	m_ShaderCompiler = nullptr;
}

bool D3D12Driver::CreateDevice()
{
	if (!DXGIFactory::Create())
		return false;

	Ptr<IDXGIAdapter1> tempAdapter;
	const D3D_FEATURE_LEVEL MinRequiredFeatureLevel = GetRequiredD3DFeatureLevel();

	D3D12AdapterDesc FirstWithoutIntegratedAdapter;
	D3D12AdapterDesc FirstAdapter;

	bool bIsAnyAMD = false;
	bool bIsAnyIntel = false;
	bool bIsAnyNVIDIA = false;

	bool bRequestedWARP = false;
	bool bAllowPerfHUD = false;

	int PreferredVendor = D3D12_PreferAdapterVendor();

	for (UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != DXGIFactory::Get()->EnumAdapters1(adapterIndex, tempAdapter.GetInitRef()); ++adapterIndex)
	{
		if (tempAdapter.IsValid())
		{
			D3D_FEATURE_LEVEL MaxSupportedFeatureLevel = static_cast<D3D_FEATURE_LEVEL>(0);
			uint32 NumNodes = 0;

			if (SafeTestD3D12CreateDevice(tempAdapter, MinRequiredFeatureLevel, MaxSupportedFeatureLevel, NumNodes))
			{
				ASSERT(NumNodes > 0);

				DXGI_ADAPTER_DESC adapterDesc;
				HRESULT hr = tempAdapter->GetDesc(&adapterDesc);
				ASSERT(SUCCEEDED(hr));

				bool bFindOutput = false;

				// 출력 대상 찾기
				Ptr<IDXGIOutput> tempOutput;
				for (UINT outputIndex = 0; DXGI_ERROR_NOT_FOUND != tempAdapter->EnumOutputs(outputIndex, tempOutput.GetInitRef()); ++outputIndex)
				{
					DXGI_OUTPUT_DESC outputDesc;
					hr = tempOutput->GetDesc(&outputDesc);
					ASSERT(SUCCEEDED(hr));

					ULOG(Log::_Debug, "* Output: DeviceName: %s", outputDesc.DeviceName);

					bFindOutput = true;
				}

				ULOG(Log::_Debug, "* DeviceID: %d, Device Desc: %s", adapterDesc.DeviceId, adapterDesc.Description);
				ULOG_UTF8(Log::_Debug, "* Revision 0x%x SubSystem 0x%x VendorId 0x%x", adapterDesc.Revision, adapterDesc.SubSysId, adapterDesc.VendorId);
				ULOG_UTF8(Log::_Debug, "* VRam: %d, SysMem: %d, SharedMem: %ul",
					adapterDesc.DedicatedVideoMemory / (1024 * 1024), adapterDesc.DedicatedSystemMemory / (1024 * 1024), adapterDesc.SharedSystemMemory / (1024 * 1024));

				//bool bIsSoftware = (adapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) ? true : false;
				bool bIsAMD = adapterDesc.VendorId == 0x1002;
				bool bIsIntel = adapterDesc.VendorId == 0x8086;
				bool bIsNVIDIA = adapterDesc.VendorId == 0x10DE;
				bool bIsWARP = adapterDesc.VendorId == 0x1414;

				if (bIsAMD) bIsAnyAMD = true;
				if (bIsIntel) bIsAnyIntel = true;
				if (bIsNVIDIA) bIsAnyNVIDIA = true;

				// Simple heuristic but without profiling it's hard to do better
				const bool bIsIntegrated = bIsIntel;
				// PerfHUD is for performance profiling
				const bool bIsPerfHUD = !Stricmp(adapterDesc.Description, TEXT("NVIDIA PerfHUD"));

				D3D12AdapterDesc CurrentAdapter(adapterDesc, adapterIndex, MaxSupportedFeatureLevel, NumNodes);

				// Requested WARP, reject all other adapters.
				const bool bSkipRequestedWARP = bRequestedWARP && !bIsWARP;

				// we don't allow the PerfHUD adapter
				const bool bSkipPerfHUDAdapter = bIsPerfHUD && !bAllowPerfHUD;

				const bool bSkipAdapter = bSkipRequestedWARP || bSkipPerfHUDAdapter;

				if (!bSkipAdapter)
				{
					if (!bIsIntegrated && !FirstWithoutIntegratedAdapter.IsValid())
					{
						FirstWithoutIntegratedAdapter = CurrentAdapter;
					}
					else if (PreferredVendor == adapterDesc.VendorId && FirstWithoutIntegratedAdapter.IsValid())
					{
						FirstWithoutIntegratedAdapter = CurrentAdapter;
					}

					if (!FirstAdapter.IsValid())
					{
						FirstAdapter = CurrentAdapter;
					}
					else if (PreferredVendor == adapterDesc.VendorId && FirstAdapter.IsValid())
					{
						FirstAdapter = CurrentAdapter;
					}
				}
			}
		}
	}

	Ptr<D3D12Adapter> NewAdapter;
	if ((bIsAnyAMD || bIsAnyNVIDIA))
	{
		// We assume Intel is integrated graphics (slower than discrete) than NVIDIA or AMD cards and rather take a different one
		if (FirstWithoutIntegratedAdapter.IsValid())
		{
			NewAdapter = new D3D12Adapter(FirstWithoutIntegratedAdapter);
			//m_Adapters.Add(NewAdapter);
		}
		else
		{
			NewAdapter = new D3D12Adapter(FirstAdapter);
			//m_Adapters.Add(NewAdapter);
		}
	}
	else
	{
		NewAdapter = new D3D12Adapter(FirstAdapter);
		//m_Adapters.Add(NewAdapter);
	}

	if (!NewAdapter.IsValid())
	{
		return false;
	}

	m_Device = new D3D12Device(NewAdapter);
	return true;
}

void D3D12Driver::DestroyDevice()
{
	if (m_Device)
		delete m_Device;
	m_Device = nullptr;
}

bool D3D12Driver::CreateRenderWindow(const WindowHandle& windowHandle)
{
	m_RenderWindow = NewObject(D3D12RenderWindow, m_Device);
	if (false == m_RenderWindow->Create(windowHandle, FMT_B8G8R8A8, true))
	{
		DestroyRenderWindow();
		return false;
	}

	return true;
}

void D3D12Driver::DestroyRenderWindow()
{
	DeleteObject(m_RenderWindow);
}

IGraphicShaderCompiler* D3D12Driver::GetShaderCompiler() 
{
	return (IGraphicShaderCompiler*)m_ShaderCompiler;
}

IGraphicShaderModule* D3D12Driver::CompileShader(const ShaderDescription& desc)
{
	if (m_ShaderCompiler)
	{
		return m_ShaderCompiler->Compile(desc);
	}

	return nullptr;
}

void D3D12Driver::Present()
{
}

void D3D12Driver::BeginFrame(Viewport* viewport)
{
	m_RenderWindow->Begin();

	SetViewport(viewport);
}

void D3D12Driver::EndFrame()
{
	m_RenderWindow->End();
}

void D3D12Driver::Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil)
{
}

void D3D12Driver::SetViewport(const Viewport* viewport)
{
}