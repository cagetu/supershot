// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX11/d3d11device.h"
#include "viewport.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D11Device, IGraphicDevice);

D3D11Device::D3D11Device(ID3D11Device* InDevice, ID3D11DeviceContext* InDeviceContext, IDXGIFactory* dxgiFactory)
	: m_Device(InDevice)
	, m_DeviceContext(InDeviceContext)
	, m_DxgiFactory(dxgiFactory)
{
}
D3D11Device::~D3D11Device()
{
	SAFE_RELEASE(m_Device);
	SAFE_RELEASE(m_DeviceContext);
	SAFE_RELEASE(m_DxgiFactory);
}

void D3D11Device::SetViewport(int32 x, int32 y, int32 width, int32 height, float minDepth, float maxDepth)
{
	/*	[Setup the viewport]

		- Direct3D9에서는 application이 viewport를 설정하지 않으면, default viewport가 rendertarget 사이즈와 같은 크기로 설정되었으나,
		- Direct3D11에서는 default가 no viewport이기 때문에, 설정해주어야 한다. 일반적으로 렌더타겟의 사이즈와 동일하게...
		- 멀티 뷰포트는 일단 무시!!!!
		- x,y = [-1,1] z = [0,1]
	*/
	D3D11_VIEWPORT vp;
	vp.TopLeftX = (float)x;
	vp.TopLeftY = (float)y;
	vp.Width = (float)width;
	vp.Height = (float)height;
	vp.MinDepth = minDepth;
	vp.MaxDepth = maxDepth;
	m_DeviceContext->RSSetViewports(1, &vp);
}

bool D3D11Device::CreateRenderTargetView(ID3D11Resource* resources, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc, ID3D11RenderTargetView **ppRTView)
{
	HRESULT hr = m_Device->CreateRenderTargetView(resources, pDesc, ppRTView);
	return SUCCEEDED(hr) ? true : false;
}

bool D3D11Device::CreateDepthStencilView(ID3D11Resource* resources, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc, ID3D11DepthStencilView **ppDepthStencilVieww)
{
	HRESULT hr = m_Device->CreateDepthStencilView(resources, pDesc, ppDepthStencilVieww);
	return SUCCEEDED(hr) ? true : false;
}

bool D3D11Device::CreateBuffer(const D3D11_BUFFER_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Buffer **ppBuffer)
{
	HRESULT hr = m_Device->CreateBuffer(pDesc, pInitialData, ppBuffer);
	return SUCCEEDED(hr) ? true : false;
}

bool D3D11Device::CreateTexture2D(const D3D11_TEXTURE2D_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Texture2D **ppTexture2D)
{
	HRESULT hr = m_Device->CreateTexture2D(pDesc, pInitialData, ppTexture2D);
	return SUCCEEDED(hr) ? true : false;
}

void D3D11Device::SetRenderTarget(ID3D11RenderTargetView* renderTargetView, ID3D11DepthStencilView* depthStencilView)
{
	m_CurrentRTV.Clear();
	m_CurrentRTV.Add(renderTargetView);
	m_CurrentDSV = depthStencilView;

	m_DeviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
}

void D3D11Device::SetRenderTargets(const Array<ID3D11RenderTargetView*>& renderTargetViews, ID3D11DepthStencilView* depthStencilView)
{
	m_CurrentRTV = renderTargetViews;
	m_CurrentDSV = depthStencilView;

	m_DeviceContext->OMSetRenderTargets(renderTargetViews.Num(), renderTargetViews.Data(), depthStencilView);
}

void D3D11Device::ClearRenderTargetView(const LinearColor& clearColor)
{
	for (SizeT index = 0; index < m_CurrentRTV.Num(); index++)
	{
		m_DeviceContext->ClearRenderTargetView(m_CurrentRTV[index], &clearColor.r);
	}
}

void D3D11Device::ClearDepthStencilView(unsigned long clearFlags, float depth, unsigned char stencil)
{
	if (m_CurrentDSV)
	{
		if ((clearFlags&CLEAR_DEPTH) || (clearFlags&CLEAR_STENCIL))
		{
			unsigned int flags = (clearFlags&CLEAR_DEPTH) ? D3D11_CLEAR_DEPTH : 0;
			flags |= (clearFlags&CLEAR_STENCIL) ? D3D11_CLEAR_STENCIL : 0;

			m_DeviceContext->ClearDepthStencilView(m_CurrentDSV, flags, depth, stencil);
		}
	}
}
