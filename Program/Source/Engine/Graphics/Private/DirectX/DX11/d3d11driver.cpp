// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX11/d3d11driver.h"
#include "DirectX/DX11/d3d11renderwindow.h"
#include "DirectX/DX11/d3d11shadercompiler.h"
#include "viewport.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D11Driver, IGraphicDriver);

D3D11Driver::D3D11Driver()
	: m_Device(nullptr)
	, m_RenderWindow(nullptr)
{
}
D3D11Driver::~D3D11Driver()
{
	Destroy();
}

bool IsSupportMultiThreadSafe()
{
	return true;
}
bool IsSupportPerfHUD()
{
	return true;
}

bool D3D11Driver::Create(const WindowHandle& windowHandle)
{
	if (!CreateDevice())
		return false;

	m_RenderWindow = new D3D11RenderWindow(m_Device);
	if (!m_RenderWindow->Create(windowHandle, FMT_R8G8B8A8))
	{
		delete m_RenderWindow;
		m_RenderWindow = nullptr;
		return false;
	}

	m_ShaderCompiler = new D3D11ShaderCompiler();
	return true;
}

void D3D11Driver::Destroy()
{
	if (m_RenderWindow)
		delete m_RenderWindow;
	m_RenderWindow = nullptr;

	if (m_Device)
		delete m_Device;
	m_Device = nullptr;
}

bool D3D11Driver::CreateDevice()
{
	/*	DirectX Graphics Infrastructure
		- 윈도우 사이즈 갱신
		- 디스플레이 정보 얻기
		- SwapChain
		- 화면 모드 전환
	*/
	IDXGIFactory* dxgiFactory;
	HRESULT hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&dxgiFactory);
	if (FAILED(hr))
		return false;

	IDXGIAdapter* AdapterPtr;
	D3D_DRIVER_TYPE DriverType = D3D_DRIVER_TYPE_UNKNOWN;

	// 비동기 리소스 생성이 된다면 : 0, 아니라면 싱글 쓰레드 
	unsigned int DeviceFlags = IsSupportMultiThreadSafe() ? 0 : D3D11_CREATE_DEVICE_SINGLETHREADED;
#if _DEBUG
	DeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	bool bAllowPerfHUD = IsSupportPerfHUD() ? true : false;

	bool bEnablePerfHUD = false;

	IDXGIAdapter* EnumAdapter;
	unsigned int CurrentAdapter = 0;
	while (dxgiFactory->EnumAdapters(CurrentAdapter, &EnumAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		if (EnumAdapter != NULL)
		{
			DXGI_ADAPTER_DESC AdapterDesc;
			if (SUCCEEDED(EnumAdapter->GetDesc(&AdapterDesc)))
			{
				ULOG(Log::_Debug, "==========================================================================================================");
				ULOG(Log::_Debug, "Adaptor(#%d): %s", CurrentAdapter, AdapterDesc.Description);
				ULOG(Log::_Debug, "==========================================================================================================");

				const bool bIsPerfHUD = !unicode::strcmp(AdapterDesc.Description, TEXT("NVIDIA PerfHUD"));

				// Select the first adapter in normal circumstances or the PerfHUD one if it exists and we are allowed to use it.
				const bool bUseAdapter = CurrentAdapter == 0 || (bAllowPerfHUD && bIsPerfHUD);
				if (bUseAdapter == true)
				{
					AdapterPtr = EnumAdapter;

					// Adapter 정보를 기록하자...
				}
				if (bIsPerfHUD)
				{
					DriverType = D3D_DRIVER_TYPE_REFERENCE;

					bEnablePerfHUD = true;
				}
			}
			++CurrentAdapter;
		}
	}
	if (!AdapterPtr)
		return false;

	// 피쳐 레벨
	static D3D_FEATURE_LEVEL FeatureLevel[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1,
	};
	unsigned int featureLevelCount = _countof(FeatureLevel);

	D3D_FEATURE_LEVEL ActualFeatureLevel = D3D_FEATURE_LEVEL_11_1;

	ID3D11Device* Device = NULL;
	ID3D11DeviceContext* ImmediateDeviceContext = NULL;

	if (bEnablePerfHUD == true)
	{
		// Creating the Direct3D device.
		hr = D3D11CreateDevice(AdapterPtr,
			DriverType,
			NULL,
			DeviceFlags,
			FeatureLevel,
			featureLevelCount,
			D3D11_SDK_VERSION,
			&Device,
			&ActualFeatureLevel,
			&ImmediateDeviceContext);
		if (FAILED(hr))
			return false;
	}
	else
	{
		// 드라이버 타입
		static struct _DRIVERTYPE
		{
			D3D_DRIVER_TYPE type;
			const wchar* desc;
		} _drivertypes[] =
		{
			D3D_DRIVER_TYPE_HARDWARE, L"DRIVER TYPE HARDWARE",	// 하드웨어 드라이버 (HAL 모드)
			D3D_DRIVER_TYPE_WARP, L"DRIVER TYPE WARP",			// Warp 모드
			D3D_DRIVER_TYPE_REFERENCE, L"DRIVER TYPE REFERENCE",// 래퍼런스 레스터라이져 (REF 모드)
		};
		unsigned int driverTypeCount = _countof(_drivertypes);
		for (unsigned int type = 0; type < driverTypeCount; type++)
		{
			// Creating the Direct3D device.
			hr = D3D11CreateDevice(AdapterPtr,
				DriverType,
				NULL,
				DeviceFlags,
				FeatureLevel,
				featureLevelCount,
				D3D11_SDK_VERSION,
				&Device,
				&ActualFeatureLevel,
				&ImmediateDeviceContext);
			if (SUCCEEDED(hr))
			{
				struct featuretype
				{
					D3D_FEATURE_LEVEL lv;
					const TCHAR* desc;
				} _list[] =
				{
					D3D_FEATURE_LEVEL_11_1, TEXT("D3D_FEATURE_LEVEL_11_1"),
					D3D_FEATURE_LEVEL_11_0, TEXT("D3D_FEATURE_LEVEL_11_0"),
					D3D_FEATURE_LEVEL_10_1, TEXT("D3D_FEATURE_LEVEL_10_1"),
					D3D_FEATURE_LEVEL_10_0, TEXT("D3D_FEATURE_LEVEL_10_0"),
					D3D_FEATURE_LEVEL_9_3, TEXT("D3D_FEATURE_LEVEL_9_3"),
					D3D_FEATURE_LEVEL_9_2, TEXT("D3D_FEATURE_LEVEL_9_2"),
					D3D_FEATURE_LEVEL_9_1, TEXT("D3D_FEATURE_LEVEL_9_1"),
				};
				unicode::string feature;
				for (int i = 0; i < _countof(_list); i++)
				{
					if (_list[i].lv = ActualFeatureLevel)
					{
						feature = _list[i].desc;
						break;
					}
				}

				unicode::string msg = unicode::format(TEXT("- Driver Type : %s\n- Feature Type : %s\n"), _drivertypes[type].desc, feature);
				//unicode::string msg = unicode::format(L"- Feature Type : %s\n", feature);

				DebugMsg(TEXT("\n\n************************ [DEVICE INFO] *******************************\n"));
				DebugMsg(msg.c_str());
				DebugMsg(TEXT("**********************************************************************\n\n"));
				break;
			}
		}
	}

	if (!Device)
		return false;

	m_Device = new D3D11Device(Device, ImmediateDeviceContext, dxgiFactory);

	return true;
}

void D3D11Driver::SetViewport(const Viewport* viewport)
{
	int32 x = (int32)(viewport->Top() * m_RenderWindow->Width());
	int32 y = (int32)(viewport->Left() * m_RenderWindow->Height());
	int32 width = (int32)(viewport->Width() * m_RenderWindow->Width());
	int32 height = (int32)(viewport->Height() * m_RenderWindow->Height());

	m_Device->SetViewport(x, y, width, height, viewport->MinDepth(), viewport->MaxDepth());
}

void D3D11Driver::BeginFrame(Viewport* viewport)
{
	m_RenderWindow->Begin();

	SetViewport(viewport);
}

void D3D11Driver::EndFrame()
{
	m_RenderWindow->End();
}

void D3D11Driver::Present()
{
	// 스왑체인을 구현해야 한다... 스왑체인이 무엇인지 정의를 명확하게 하자!!!
	m_RenderWindow->Present();
}

void D3D11Driver::Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil)
{
	m_Device->ClearRenderTargetView(color);
	m_Device->ClearDepthStencilView(clearFlags, depth, stencil);
}

IGraphicShaderCompiler* D3D11Driver::GetShaderCompiler()
{
	return m_ShaderCompiler; 
}

IGraphicShaderModule* D3D11Driver::CompileShader(const ShaderDescription& desc)
{
	if (m_ShaderCompiler)
	{
		return m_ShaderCompiler->Compile(desc);
	}

	return nullptr;
}
