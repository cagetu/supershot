// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX11/d3d11swapchain.h"
#include "DirectX/DX11/d3d11device.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D11Swapchain, IGraphicSwapchain);

D3D11Swapchain::D3D11Swapchain(D3D11Device* device)
	: m_Device(device)
	, m_BackbufferRTV(NULL)
{
}
D3D11Swapchain::~D3D11Swapchain()
{
	Destroy();
}

bool D3D11Swapchain::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, IGraphicCommandQueue* commandQueue, uint32 surfaceCount)
{
	HWND hWnd = (HWND)windowHandle.Handle();

	DXGI_FORMAT outputFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
	//return DXGI_FORMAT_R10G10B10A2_UNORM;

	DXGI_MODE_DESC desc;
	desc.Width = windowHandle.Width();
	desc.Height = windowHandle.Height();
	desc.RefreshRate.Numerator = 0;
	desc.RefreshRate.Denominator = 0;
	desc.Format = outputFormat;
	desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;	// 이미지 생성을 위한 래스터 기법을 정의
	desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;					// 버퍼를 창의 클라이언트 영역에 적용하는 방식을 정의 (영역 크기에 맞게 비례 등)
	//sd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;			// 8비트 unsigned normal : 0~1 값
	//sd.BufferDesc.RefreshRate.Numerator = 60;						// 화면 주사율 : 60/1
	//sd.BufferDesc.RefreshRate.Denominator = 1;

	// 스왑 체인 설정
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory( &sd, sizeof( sd ) );
	// 1:single buffering, 2:double buffering, 3:triple buffering
	sd.BufferCount = 1;
	sd.BufferDesc = desc;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;	// 백버퍼 사용 용도
	sd.OutputWindow = hWnd;
	sd.SampleDesc.Count = 1;							// 멀티샘플링 수 / 퀄리티
	sd.SampleDesc.Quality = 0;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;			// 버퍼 내용을 표시한 이후 어떻게 처리할 것인가?
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;	// DXGI 버퍼 사용 방식에 대한 설정 (창 모드와 전체 모드 전환)
	sd.Windowed = windowHandle.IsFullScreen() ? FALSE : TRUE;	// 전체화면 모드

	HRESULT hr = m_Device->Factory()->CreateSwapChain(m_Device->Device(), &sd, &m_SwapChain);
	if (FAILED(hr))
		return false;

	// Set the DXGI message hook to not change the window behind our back.
	// 화면 모드 전환 설정하기
	hr = m_Device->Factory()->MakeWindowAssociation(hWnd, DXGI_MWA_NO_WINDOW_CHANGES);
	if (FAILED(hr))
		return false;

	// fullscreen 상태로 전환한다.
	bool fullscreen = windowHandle.IsFullScreen();
	hr = m_SwapChain->SetFullscreenState(fullscreen, NULL);
	if (FAILED(hr))
		return false;

	m_WindowHandle = windowHandle;

	if (CreateRenderBuffer() == false)
		return false;

	return true;
}

void D3D11Swapchain::Destroy()
{
	// If the swap chain was in fullscreen mode, switch back to windowed before releasing the swap chain.
	// DXGI throws an error otherwise.
	m_SwapChain->SetFullscreenState(false, NULL);

	SAFE_RELEASE(m_BackbufferRTV);
	SAFE_RELEASE(m_SwapChain);
}

bool D3D11Swapchain::CreateRenderBuffer()
{
	HRESULT hr;

	// 백버퍼 렌더타겟 생성
	Ptr<ID3D11Texture2D> BackBufferResource;
	hr = m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)BackBufferResource.GetInitRef());
	if (FAILED(hr))
		return false;

	if (!m_Device->CreateRenderTargetView(BackBufferResource.Get(), NULL, &m_BackbufferRTV))
		return false;

	return true;
}

bool D3D11Swapchain::Resize(const WindowHandle& windowHandle)
{
	_ASSERT(windowHandle.Width() > 0);
	_ASSERT(windowHandle.Height() > 0);

	if (Width() != windowHandle.Width() || Height() != windowHandle.Height())
	{
		// 버퍼 초기화
		SAFE_RELEASE(m_BackbufferRTV);

		/*	- 스왑 체인의 백버퍼를 렌더링 파이프라인에서 언바인딩
			- ResizeBuffers
				: 사이즈 변환 (백버퍼의 사이즈 갱신은 스왑체인을 표시하고 있는 윈도우의 사이즈나 해상도 등을 갱신하지 않는다.
			- ResizeTarget
				: 어플리케이션 측에서 필요한 경우에 윈도우 갱신처리를 할 경우가 있음.
				: WM_SIZE 메시지를 발생
			- 백버퍼의 렌더타겟 뷰를 다시 만들어야 한다.
		*/
		DXGI_FORMAT outputFormat = DXGI_FORMAT_B8G8R8A8_UNORM;	// DXGI_FORMAT_R10G10B10A2_UNORM
		HRESULT hr = m_SwapChain->ResizeBuffers(1, windowHandle.Width(), windowHandle.Height(), outputFormat, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
		_ASSERT(SUCCEEDED(hr));

		if (windowHandle.IsFullScreen() == true)
		{
			DXGI_MODE_DESC desc;
			desc.Width = windowHandle.Width();
			desc.Height = windowHandle.Height();
			desc.RefreshRate.Numerator = 0;
			desc.RefreshRate.Denominator = 0;
			desc.Format = outputFormat;
			desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;	// 이미지 생성을 위한 래스터 기법을 정의
			desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;					// 버퍼를 창의 클라이언트 영역에 적용하는 방식을 정의 (영역 크기에 맞게 비례 등)
			//sd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;	// 8비트 unsigned normal : 0~1 값
			//sd.BufferDesc.RefreshRate.Numerator = 60;			// 화면 주사율 : 60/1
			//sd.BufferDesc.RefreshRate.Denominator = 1;

			hr = m_SwapChain->ResizeTarget(&desc);
		}

		if (m_WindowHandle.IsFullScreen() != windowHandle.IsFullScreen())
		{
			//m_IsValid = false;

			const bool forceReset = true;

			HWND hWnd = (HWND)windowHandle.Handle();

			// Check if the viewport's window is focused before resetting the swap chain's fullscreen state.
			HWND FocusWindow = ::GetFocus();
			const bool bIsFocused = FocusWindow == windowHandle.Handle();
			const bool bIsIconic = !!::IsIconic(hWnd);
			if (forceReset || (bIsFocused && !bIsIconic))
			{
				//FlushRenderingCommands();

				// Store the current cursor clip rectangle as it can be lost when fullscreen is reset.
				RECT OriginalCursorRect;
				GetClipCursor(&OriginalCursorRect);

				bool isFullscreen = windowHandle.IsFullScreen();

				HRESULT Result = m_SwapChain->SetFullscreenState(isFullscreen, NULL);
				if (SUCCEEDED(Result))
				{
					ClipCursor(&OriginalCursorRect);
					//m_IsValid = true;
				}
				else
				{
					// Even though the docs say SetFullscreenState always returns S_OK, that doesn't always seem to be the case.
					//UE_LOG(LogD3D11RHI, Log, TEXT("IDXGISwapChain::SetFullscreenState returned %08x; waiting for the next frame to try again."),Result);
				}
			}
		}
		m_WindowHandle = windowHandle;

		CreateRenderBuffer();
	}
	return true;
}

void D3D11Swapchain::Present()
{
	unsigned long flags = 0; //D3DPRESENT_DONOTWAIT

	HRESULT hr = m_SwapChain->Present(0, flags);
	if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET || hr == DXGI_ERROR_DRIVER_INTERNAL_ERROR)
	{
		// 디바이스 초기화 처리해야 한다.

		//m_Driver->m_Device->Device()->NotifyDeviceRemoved();
		//CEngine::ResetDevice();
	}
	else if (hr == DXGI_STATUS_OCCLUDED)
	{
		// 대기 모드로 전환 시키자...

		/*
			if (대기모드)
			{
				hr = m_SwapChain->Presnet(0, DXGI_PRESENT_TEST);
				if (hr == DXGI_STATUS_OCCLUDED)
					return

				대기모드 해제
			}

			hr = m_SwapChain->Presnet(0, 0);
			if (hr == DXGI_STATUS_OCCLUDED)
				대기 모드 전환
		*/
	}
}