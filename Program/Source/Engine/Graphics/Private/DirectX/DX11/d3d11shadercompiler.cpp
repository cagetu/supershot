// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX11/D3D11ShaderCompiler.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D11ShaderCompiler, IGraphicShaderCompiler);

D3D11ShaderCompiler::D3D11ShaderCompiler()
{
}

D3D11ShaderCompiler::~D3D11ShaderCompiler()
{
}

//------------------------------------------------------------------
/*
	1. hlsl 파일을 기반으로 spirv 코드를 생성한다.
	2. (hlsl + defines 정보)를 기반으로 spirv 파일은 캐시된다.
	3. spirv 파일을 기반으로 shadermodule 객체를 생성한다.
*/
IGraphicShaderModule* D3D11ShaderCompiler::Compile(const ShaderDescription& desc)
{
	return nullptr;
}
