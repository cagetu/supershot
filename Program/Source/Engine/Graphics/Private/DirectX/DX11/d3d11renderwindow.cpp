// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "DirectX/DX11/d3d11renderwindow.h"
#include "DirectX/DX11/d3d11swapchain.h"
#include "DirectX/DX11/d3d11device.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, D3D11RenderWindow, IGraphicSwapchain);

D3D11RenderWindow::D3D11RenderWindow(D3D11Device* device)
	: m_Device(device)
	, m_SwapChain(NULL)
	, m_BackbufferDSV(NULL)
{
}
D3D11RenderWindow::~D3D11RenderWindow()
{
	Destroy();
}

bool D3D11RenderWindow::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, bool bUseDepthBuffer)
{
	m_SwapChain = new D3D11Swapchain(m_Device);
	if (!m_SwapChain->Create(windowHandle, format))
		return false;

	m_WindowHandle = windowHandle;
	m_Format = format;
	m_bUseDepthSurface = bUseDepthBuffer;

	if (HasDepthSurface())
	{
		if (CreateDepthBuffer() == false)
			return false;
	}

	return true;
}

void D3D11RenderWindow::Destroy()
{
	SAFE_RELEASE(m_BackbufferDSV);
	if (m_SwapChain)
		delete m_SwapChain;
	m_SwapChain = NULL;
}

bool D3D11RenderWindow::CreateDepthBuffer()
{
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = m_WindowHandle.Width();
	descDepth.Height = m_WindowHandle.Height();
	descDepth.MipLevels = 1;							// 밉맵 개수
	descDepth.ArraySize = 1;							// 배열 개수
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;	// format
	descDepth.SampleDesc.Count = 1;						// 샘플링
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;		// 깊이/스텐실 버퍼로부터 사용
	descDepth.CPUAccessFlags = 0;						// CPU에서 접근하지 않음.
	descDepth.MiscFlags = 0;							// 그 외 설정 없음

	Ptr<ID3D11Texture2D> depthStencilResource;
	if (!m_Device->CreateTexture2D(&descDepth, NULL, depthStencilResource.GetInitRef()))
		return false;

	// 생성한 텍스쳐는 텍스쳐 리소스의 한 종류이므로, 파이프라인에 설정하려면 뷰를 사용한다.
	// 깊이-스텐실 텍스쳐의 경우, DepthStencilView를 사용한다.

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	if (!m_Device->CreateDepthStencilView(depthStencilResource, &descDSV, &m_BackbufferDSV))
		return false;

	return true;
}

void D3D11RenderWindow::Resize(const WindowHandle& windowHandle)
{
	_ASSERT(windowHandle.Width() > 0);
	_ASSERT(windowHandle.Height() > 0);

	if (Width() != windowHandle.Width() || Height() != windowHandle.Height())
	{
		m_WindowHandle = windowHandle;

		m_SwapChain->Resize(windowHandle);

		if (HasDepthSurface())
		{
			CreateDepthBuffer();
		}
	}
}

void D3D11RenderWindow::Present()
{
	m_SwapChain->Present();
}

void D3D11RenderWindow::Begin()
{
	m_Device->SetRenderTarget(m_SwapChain->ColorBuffer(), m_BackbufferDSV);
}

void D3D11RenderWindow::End()
{
	if (m_SwapChain)
	{
		// TODO
	}
}