#include "foundation.h"
#include "graphicvertexelement.h"

//------------------------------------------------------------------
//----------------------------------------------------------------
VertexElement::VertexElement()
	: sementic(SEMENTIC::Invalid)
	, format(FORMAT::Float)
	, usageIndex(0)
{
}
//----------------------------------------------------------------
VertexElement::VertexElement(SEMENTIC _sementic, FORMAT _format, ushort _usageIndex)
	: sementic(_sementic)
	, format(_format)
	, usageIndex(_usageIndex)
{
}

//----------------------------------------------------------------
/** */
//----------------------------------------------------------------
VertexElement::SEMENTIC VertexElement::GetSementic() const
{
	return sementic;
}

//----------------------------------------------------------------
/** */
//----------------------------------------------------------------
VertexElement::FORMAT VertexElement::GetFormat() const
{
	return format;
}

//----------------------------------------------------------------
/** */
//----------------------------------------------------------------
ushort VertexElement::GetUsageIndex() const
{
	return usageIndex;
}

//----------------------------------------------------------------
//** @brief	component 사이즈 */
//----------------------------------------------------------------
ushort VertexElement::GetByteSize() const
{
	switch (format)
	{
	case FORMAT::Float:     return sizeof(float);
	case FORMAT::Float2:    return sizeof(float)*2;
	case FORMAT::Float3:    return sizeof(float)*3;
	case FORMAT::Float4:    return sizeof(float)*4;
	case FORMAT::Short2:    return sizeof(short)*2;
	case FORMAT::Short4:    return sizeof(short)*4;
	case FORMAT::Short2N:   return sizeof(short)*2;
	case FORMAT::Short4N:   return sizeof(short)*4;
	case FORMAT::UShort2:   return sizeof(unsigned short)*2;
	case FORMAT::UShort4:   return sizeof(unsigned short)*4;
	case FORMAT::UShort2N:  return sizeof(unsigned short)*2;
	case FORMAT::UShort4N:  return sizeof(unsigned short)*4;
	case FORMAT::Byte4:		return sizeof(char)*4;
	case FORMAT::Byte4N:    return sizeof(char)*4;
	case FORMAT::UByte4:	return sizeof(unsigned char)*4;
	case FORMAT::UByte4N:   return sizeof(unsigned char)*4;
	case FORMAT::Color:		return sizeof(RGBA);
	case FORMAT::Int:		return sizeof(int);
	case FORMAT::Int2:		return sizeof(int)*2;
	case FORMAT::Int3:		return sizeof(int)*3;
	case FORMAT::Int4:		return sizeof(int)*4;
	case FORMAT::UInt:		return sizeof(unsigned int);
	case FORMAT::UInt2:		return sizeof(unsigned int)*2;
	case FORMAT::UInt3:		return sizeof(unsigned int)*3;
	case FORMAT::UInt4:		return sizeof(unsigned int)*4;
	case FORMAT::Double:	return sizeof(double);
	case FORMAT::Double2:	return sizeof(double)*2;
	case FORMAT::Double3:	return sizeof(double)*3;
	case FORMAT::Half2:		return sizeof(float);
	case FORMAT::Half4:		return sizeof(float)*2;
	}
	return 0;
}

//----------------------------------------------------------------
/** */
//----------------------------------------------------------------
ushort VertexElement::GetTypeCount() const
{
	switch (format)
	{
	case FORMAT::Float:
	case FORMAT::Color:
	case FORMAT::Int:
	case FORMAT::UInt:
	case FORMAT::Double:
	case FORMAT::Half2:
		return 1;
	case FORMAT::Float2:
	case FORMAT::Short2:
	case FORMAT::Short2N:
	case FORMAT::UShort2:
	case FORMAT::UShort2N:
	case FORMAT::Int2:
	case FORMAT::UInt2:
	case FORMAT::Double2:
	case FORMAT::Half4:
		return 2;
	case FORMAT::Int3:
	case FORMAT::Float3:
	case FORMAT::Double3:
	case FORMAT::UInt3:		
		return 3;
	case FORMAT::Float4:
	case FORMAT::Short4:
	case FORMAT::Short4N:
	case FORMAT::UShort4:
	case FORMAT::UShort4N:
	case FORMAT::Byte4:
	case FORMAT::Byte4N:
	case FORMAT::UByte4:
	case FORMAT::UByte4N:
	case FORMAT::Int4:
	case FORMAT::UInt4:		
		return 4;
	}
	return 0;
}

//----------------------------------------------------------------
/** */
//----------------------------------------------------------------
bool VertexElement::IsNormalizedType() const
{
	switch (format)
	{
	case FORMAT::Short2N:
	case FORMAT::UShort2N:
	case FORMAT::Short4N:
	case FORMAT::UShort4N:
	case FORMAT::Byte4N:
	case FORMAT::UByte4N:
		return true;
	}
	return false;
}