#include "foundation.h"
#include "graphicshaderfeature.h"
#include "Container\map.h"

Array<String> ShaderFeature::m_BitToString;
Array<utf8::string> ShaderFeature::m_Masks;
Map<String, int32> ShaderFeature::m_StringToBit;
CBitFlags<512> ShaderFeature::m_FeatureBits;
int32 ShaderFeature::m_AllocBit = 0;

void ShaderFeature::Initialize()
{
	static struct
	{
		int32 bit;
		const TCHAR* define;
	} _list[] =
	{
		_SKINNING, TEXT("SKINNING"),
		_INSTANCING, TEXT("_INSTANCING"),
		_VERTEXCOMPRESS, TEXT("_VERTEXCOMPRESS"),
		_USE_NORMAL, TEXT("_USE_NORMAL"),
		_USE_VERTEXCOLOR, TEXT("_USE_VERTEXCOLOR"),
		_USE_ALPHA, TEXT("_USE_ALPHA"),
	};
	for (int32 i = 0; i < _countof(_list); i++)
	{
		ReserveBit(_list[i].bit, _list[i].define);
	}
}

void ShaderFeature::Shutdown()
{
	Reset();
}

void ShaderFeature::Reset()
{
	m_StringToBit.Clear();
	m_BitToString.Clear();
	m_Masks.Clear();

	m_FeatureBits.Reset();
	m_AllocBit = _MAX_DEFINED_FEATURE;
}

ShaderFeature::MASK ShaderFeature::GetShaderMask(const Array<String>& defines)
{
	if (defines.IsEmpty())
		return MASK();

	MASK mask;
	for (Array<String>::const_iterator it = defines.begin(); it != defines.end(); it++)
	{
		int32 bitIndex = RegisterBit((*it));
		mask.SetFlags(bitIndex, true);
		//mask |= (1 << bitIndex);
	}
	return mask;
}

ShaderFeature::MASK ShaderFeature::GetShaderMask(const wchar* shadermask)
{
	if (shadermask == NULL)
		return MASK();

	MASK mask;

	int32 i, len;
	for (i = 0; shadermask[i] != '\0';)
	{
		if (shadermask[i] == ',')
		{
			i++;
			continue;
		}

		for (len = 0; shadermask[i + len] != '\0' && shadermask[i + len] != ','; len++);
		
		String def(&shadermask[i], len);
		i += len;

		int32 bitIndex = RegisterBit(def);
		mask.SetFlags(bitIndex, true);
		//mask |= (1 << bitIndex);
	}

	return mask;
}

void ShaderFeature::ReserveBit(int32 bitIndex, const String& def)
{
	m_StringToBit.Insert(def, bitIndex);
	m_BitToString.Add(def);

	utf8::string txt8 = unicode::convert(def);
	m_Masks.Add(txt8);

	m_FeatureBits.SetFlags(bitIndex, true);
}

int32 ShaderFeature::RegisterBit(const String& def)
{
	IndexT index = m_StringToBit.FindIndex(def);
	if (index != INVALID_INDEX)
	{
		return m_StringToBit.ValueAt(index);
	}

	int32 bitIndex = m_AllocBit++;
	if (bitIndex >= m_FeatureBits.Capacity())
	{
		//PlatformMisc::DebugOut("ShaderFeature Bit is Full 32\n");
		return -1;
	}
	m_StringToBit.Insert(def, bitIndex);
	m_BitToString.Add(def);

	utf8::string txt8 = unicode::convert(def);
	m_Masks.Add(txt8);

	m_FeatureBits.SetFlags(bitIndex, true);

	return bitIndex;
}

int32 ShaderFeature::FindBit(const String& def)
{	
	IndexT index = m_StringToBit.FindIndex(def);
	if (index != INVALID_INDEX)
	{
		return m_StringToBit.ValueAt(index);
	}
	return -1;
}

String ShaderFeature::GetMaskName(ShaderFeature::MASK mask)
{
	const int32 totalID = m_AllocBit;

	String output;
	for (int32 i = 0; i < totalID; i++)
	{
		//if (0 != (mask & (1 << i)))
		if (mask.GetFlags(i))
		{
			if (output.empty() == false)
				output += TEXT(",");

			output += m_BitToString[i];
		}
	}
	return output;
}

int32 ShaderFeature::GetMacro(MASK mask, utf8::string* macro, utf8::string* macroCode)
{
	int32 n = 0;

	for (int32 i = 0; i < m_AllocBit; i++)
	{
		//if (0 != (mask & (1 << i)))
		if (mask.GetFlags(i))
		{
			macro[n] = m_Masks[i];
			*macroCode += utf8::string("#define ") + macro[n] + utf8::string("\n");
			n++;
		}
	}
	macro[n].clear();
	return n;
}
