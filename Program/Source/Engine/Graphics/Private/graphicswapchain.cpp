#include "foundation.h"
#include "graphicswapchain.h"
#include "graphicdriver.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicSwapchain);

IGraphicSwapchain::IGraphicSwapchain()
	: m_BackBufferFormat(FMT_NONE)
	, m_BackBufferCount(0)
{
	__ConstructReference;
}
IGraphicSwapchain::~IGraphicSwapchain()
{
	__DestructReference;
}
