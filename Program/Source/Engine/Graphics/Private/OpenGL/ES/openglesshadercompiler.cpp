// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "graphic.h"
#include "OpenGL/ES/openglesshadercompiler.h"
#include "OpenGL/ES/openglesshadermodule.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESShaderCompiler, IGraphicShaderCompiler);

OpenGLESShaderCompiler::OpenGLESShaderCompiler()
{
}

OpenGLESShaderCompiler::~OpenGLESShaderCompiler()
{
}

IGraphicShaderModule* OpenGLESShaderCompiler::Compile(const ShaderDescription& desc)
{
	return NULL;
}