#include "graphic.h"
#include "OpenGL/ES/openglesvertexbuffer.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESVertexBuffer, IGraphicVertexBuffer);

OpenGLESVertexBuffer::OpenGLESVertexBuffer()
	: m_Handle(0)
	, m_BufferSize(0)
{
}

OpenGLESVertexBuffer::~OpenGLESVertexBuffer()
{
	Destroy();
}

bool OpenGLESVertexBuffer::Create(int32 bufferSize)
{
	glGenBuffers(1, &m_Handle);
	m_BufferSize = bufferSize;
	return true;
}

void OpenGLESVertexBuffer::Destroy()
{
	if (m_Handle != 0)
	{
		glDeleteBuffers(1, &m_Handle);
		m_Handle = 0;
	}
}