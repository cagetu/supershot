#include "graphic.h"
#include "OpenGL/ES/openglesrenderwindow.h"
#include "OpenGL/ES/openglesswapchain.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESRenderWindow, IGraphicRenderWindow);

OpenGLESRenderWindow::OpenGLESRenderWindow()
{
#ifdef WIN32
	m_eglDisplay = NULL;
	m_eglContext = NULL;
	m_eglSurface = NULL;
#endif

	m_DepthBufferId = -1;
	m_DirtyFlags = 0;

	m_SwapChain = NULL;
}

OpenGLESRenderWindow::~OpenGLESRenderWindow()
{
	Destroy();
}

#ifndef __APPLE__
///
// GetContextRenderableType()
//
//    Check whether EGL_KHR_create_context extension is supported.  If so,
//    return EGL_OPENGL_ES3_BIT_KHR instead of EGL_OPENGL_ES2_BIT
//
EGLint GetContextRenderableType(EGLDisplay eglDisplay)
{
#ifdef EGL_KHR_create_context
	const char *extensions = eglQueryString(eglDisplay, EGL_EXTENSIONS);

	// check whether EGL_KHR_create_context is in the extension string
	if (extensions != NULL && strstr(extensions, "EGL_KHR_create_context"))
	{
		// extension is supported
		return EGL_OPENGL_ES3_BIT_KHR;
	}
#endif
	// extension is not supported
	return EGL_OPENGL_ES2_BIT;
}
#endif

bool OpenGLESRenderWindow::Create(const WindowHandle& windowHandle, FORMAT_TYPE format, int32 multiSamples, bool bUseDepthBuffer)
{
	m_WindowHandle = windowHandle;
#ifdef WIN32
	HWND hWnd = (HWND)windowHandle.Handle();

	HDC hDC = GetDC(hWnd);
	if (!hDC)
		return false;

	m_eglDisplay = eglGetDisplay(hDC);
	if (m_eglDisplay == EGL_NO_DISPLAY)
	{
		m_eglDisplay = eglGetDisplay((EGLNativeDisplayType)EGL_DEFAULT_DISPLAY);
	}
	if (m_eglDisplay == EGL_NO_DISPLAY)
	{
		return false;
	}

	int majorVersion, minorVersion;
	if (!eglInitialize(m_eglDisplay, &majorVersion, &minorVersion))
		return false;

	eglBindAPI(EGL_OPENGL_ES_API);
	if (eglGetError() == 0)
		return false;

	const int pi32ConfigAttribs[] =
	{
		EGL_RED_SIZE,			8,
		EGL_GREEN_SIZE,			8,
		EGL_BLUE_SIZE,			8,
		//EGL_ALPHA_SIZE,			8,
		EGL_LEVEL,				0,
		EGL_BUFFER_SIZE,		0,
		EGL_SURFACE_TYPE,		EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	GetContextRenderableType(m_eglDisplay),
		EGL_NATIVE_RENDERABLE,	EGL_FALSE,
		EGL_DEPTH_SIZE,			16,
		EGL_NONE
		/*
				EGL_BUFFER_SIZE,		0,
				EGL_RED_SIZE,			5,
				EGL_GREEN_SIZE,			6,
				EGL_BLUE_SIZE,			5,
				EGL_ALPHA_SIZE,			0,
				EGL_LEVEL,				0,
				EGL_DEPTH_SIZE,			16,		// 깊이버퍼 Bit 크기
				EGL_SURFACE_TYPE,		EGL_WINDOW_BIT,
		#ifdef _GLES_1_1
				EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES_BIT,
		#else
				EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		#endif
				EGL_NATIVE_RENDERABLE,	EGL_DONT_CARE,
				EGL_NONE
		*/
	};

	EGLConfig eglConfig = 0;

	int32 iConfigs;
	if (!eglChooseConfig(m_eglDisplay, pi32ConfigAttribs, &eglConfig, 1, &iConfigs) || (iConfigs != 1))
	{
		int32 err = eglGetError(); // Clear error
		DebugMsg(TEXT("ERROR: %d\n"), err);
		return false;
	}

	EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE };
	m_eglContext = eglCreateContext(m_eglDisplay, eglConfig, NULL, contextAttribs);
	if (eglGetError() == 0)
		return false;

	m_eglSurface = eglCreateWindowSurface(m_eglDisplay, eglConfig, hWnd, NULL);
	if (m_eglSurface == EGL_NO_SURFACE)
	{
		eglGetError(); // Clear error
		m_eglSurface = eglCreateWindowSurface(m_eglDisplay, eglConfig, NULL, NULL);
	}
	if (eglGetError() == 0 || m_eglSurface == EGL_NO_SURFACE)
		return false;

	eglMakeCurrent(m_eglDisplay, m_eglSurface, m_eglSurface, m_eglContext);
	if (eglGetError() == 0)
		return false;

#endif //

	m_SwapChain = new OpenGLESSwapchain();
	if (!m_SwapChain->Create(windowHandle, format))
		return false;

	return true;
}

void OpenGLESRenderWindow::Destroy()
{
	if (m_SwapChain)
	{
		delete m_SwapChain;
	}
	m_SwapChain = NULL;

#ifdef WIN32
	eglSwapBuffers(m_eglDisplay, m_eglSurface);
	eglMakeCurrent(m_eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
	eglDestroyContext(m_eglDisplay, m_eglContext);
	eglDestroySurface(m_eglDisplay, m_eglSurface);
	eglTerminate(m_eglDisplay);
#elif defined(__APPLE__)
	// Tear down context
	if ([EAGLContext currentContext] == m_eglContext)
		[EAGLContext setCurrentContext : nil];
	[m_eglContext release];
	m_eglContext = nil;
#endif
}

void OpenGLESRenderWindow::Resize(const WindowHandle& windowHandle)
{
	m_WindowHandle = windowHandle;
}

void OpenGLESRenderWindow::Present()
{
#ifdef __APPLE__
	//[m_eglContext presentRenderbuffer:GL_RENDERBUFFER];	

#elif __ANDROID__

#else
	//unsigned long tick = timeGetTime();
	{
		eglSwapBuffers(m_eglDisplay, m_eglSurface);
	}
	//DebugMsg(TEXT("[COpenGLES::Present] %dms\n"), timeGetTime() - tick);
#endif
}

void OpenGLESRenderWindow::Begin()
{
	// 렌더 타겟 설정

	m_SwapChain->Begin();

	// 렌더 타겟 설정

	if (m_DirtyFlags&_DIRTY_DEPTHBUFFER)
	{
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_DepthBufferId);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			_ASSERT(0);
			return;
		}
		m_DirtyFlags &= ~_DIRTY_DEPTHBUFFER;
	}
}

void OpenGLESRenderWindow::End()
{
	m_SwapChain->End();
}

int32 OpenGLESRenderWindow::GetColorBuffer() const
{
	return m_SwapChain->GetColorBuffer();
}

int32 OpenGLESRenderWindow::GetDepthBuffer() const
{
	return m_DepthBufferId;
}

void OpenGLESRenderWindow::SetColorBuffer(int32 colorBufferHandle)
{
	m_SwapChain->SetColorBuffer(colorBufferHandle);
}

void OpenGLESRenderWindow::SetDepthBuffer(int32 depthBufferHandle)
{
	m_DepthBufferId = depthBufferHandle;
	m_DirtyFlags |= _DIRTY_DEPTHBUFFER;
}