#include "graphic.h"
#include "OpenGL/ES/openglesdriver.h"
#include "OpenGL/ES/openglesrenderwindow.h"
#include "OpenGL/ES/openglesshadercompiler.h"
#include "viewport.h"
#include "color.h"

__ImplementRtti(Gfx, OpenGLESDriver, IGraphicDriver);

OpenGLESDriver::OpenGLESDriver()
{
	m_RenderWindow = NULL;
	m_Device = NULL;
	m_ShaderCompiler = NULL;
}
OpenGLESDriver::~OpenGLESDriver()
{
	Destroy();
}

bool OpenGLESDriver::Create(const WindowHandle& windowHandle)
{
	m_RenderWindow = new OpenGLESRenderWindow();
	if (!m_RenderWindow->Create(windowHandle, FMT_R8G8B8A8, true))
	{
		Destroy();
		return false;
	}

	m_Device = new OpenGLESDevice();
	if (!m_Device->CheckCapacity())
	{
		Destroy();
		return false;
	}

	m_ShaderCompiler = new OpenGLESShaderCompiler();
	return true;
}

void OpenGLESDriver::Destroy()
{
	if (m_Device)
		delete m_Device;
	m_Device = NULL;

	if (m_RenderWindow)
		delete m_RenderWindow;
	m_RenderWindow = NULL;

	if (m_ShaderCompiler)
		delete m_ShaderCompiler;
	m_ShaderCompiler = NULL;

	glFinish();
}

void OpenGLESDriver::BeginFrame(Viewport* viewport)
{
	m_RenderWindow->Begin();

	// 뷰포트....
	SetViewport(viewport);
}

void OpenGLESDriver::EndFrame()
{
	m_RenderWindow->End();

	// 렌더 타겟 있을 경우 End...
}

void OpenGLESDriver::Present()
{
	m_RenderWindow->Present();
}

void OpenGLESDriver::Clear(const Color& color, float depth, unsigned long clearFlags, unsigned char stencil)
{
	if (clearFlags > 0)
	{
		GLbitfield clearMask = 0;
		if (clearFlags & CLEAR_COLOR)
		{
			glClearColor(color.r, color.g, color.b, color.a);
			clearMask |= GL_COLOR_BUFFER_BIT;
		}
		if (clearFlags & CLEAR_DEPTH)
		{
			glClearDepthf(depth);
			clearMask |= GL_DEPTH_BUFFER_BIT;
		}
		if (clearFlags & CLEAR_STENCIL)
		{
			glClearStencil(stencil);
			clearMask |= GL_STENCIL_BUFFER_BIT;
		}

		glClear(clearMask);
	}
}

void OpenGLESDriver::SetViewport(const Viewport* viewport)
{
	GLint x = (GLint)(viewport->Top() * m_RenderWindow->Width());
	GLint y = (GLint)(viewport->Left() * m_RenderWindow->Height());
	GLsizei width = (GLsizei)(viewport->Width() * m_RenderWindow->Width());
	GLsizei height = (GLsizei)(viewport->Height() * m_RenderWindow->Height());

	m_Device->SetViewport(x, y, width, height);
}

IGraphicShaderCompiler* OpenGLESDriver::GetShaderCompiler()
{
	return m_ShaderCompiler;
}

IGraphicShaderModule* OpenGLESDriver::CompileShader(const ShaderDescription& desc)
{
	return m_ShaderCompiler->Compile(desc);
}