#include "graphic.h"
#include "OpenGL/ES/openglesindexbuffer.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESIndexBuffer, IGraphicIndexBuffer);

OpenGLESIndexBuffer::OpenGLESIndexBuffer()
	: m_Handle(0)
	, m_BufferSize(0)
{
}

OpenGLESIndexBuffer::~OpenGLESIndexBuffer()
{
	Destroy();
}

bool OpenGLESIndexBuffer::Create(int32 bufferSize)
{
	glGenBuffers(1, &m_Handle);
	m_BufferSize = bufferSize;
	return true;
}

void OpenGLESIndexBuffer::Destroy()
{
	if (m_Handle != 0)
	{
		glDeleteBuffers(1, &m_Handle);
		m_Handle = 0;
	}
}