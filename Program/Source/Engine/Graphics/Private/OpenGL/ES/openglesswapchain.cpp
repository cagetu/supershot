#include "graphic.h"
#include "OpenGL/ES/openglesswapchain.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESSwapchain, IGraphicSwapchain);

OpenGLESSwapchain::OpenGLESSwapchain()
{
}
OpenGLESSwapchain::~OpenGLESSwapchain()
{
}

bool OpenGLESSwapchain::Create(const WindowHandle& windowHandle, FORMAT_TYPE format)
{
	m_WindowHandle = windowHandle;
	return true;
}

void OpenGLESSwapchain::Destroy()
{
}

bool OpenGLESSwapchain::Resize(const WindowHandle& windowHandle)
{
	m_WindowHandle = windowHandle;
	return true;
}

void OpenGLESSwapchain::Present()
{
}

void OpenGLESSwapchain::Begin()
{
	// 백버퍼 설정 (2013-05-30 : cagetu)
	// 플랫폼 별로 메인 프레임버퍼의 id가 다르기 때문에, 시작할 때 등록해놓는다.
	if (m_ColorBufferId == -1) {
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_ColorBufferId);
		DebugMsg(TEXT("[GL_FRAMEBUFFER_BINDING] %d"), m_ColorBufferId);
	}

	// 렌더 타겟 설정

	// backbuffer에 렌더링 하기 위해 다시 Backbuffer로 바인딩한다.
	//if (m_DirtyFlags&_DIRTY_COLORBUFFER)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, m_ColorBufferId);

		m_DirtyFlags &= ~_DIRTY_COLORBUFFER;
	}
}

void OpenGLESSwapchain::End()
{
}

void OpenGLESSwapchain::SetColorBuffer(int32 colorBufferHandle)
{
	m_ColorBufferId = colorBufferHandle;
	m_DirtyFlags |= _DIRTY_COLORBUFFER;
}
