// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "graphic.h"
#include "OpenGL/ES/openglesshadermodule.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESShaderModule, IGraphicShaderModule);

OpenGLESShaderModule::OpenGLESShaderModule()
{
}
OpenGLESShaderModule::~OpenGLESShaderModule()
{
	Destroy();
}

bool OpenGLESShaderModule::Create(ShaderType::TYPE type, uint32 elementSize, const void* elementData)
{
	IGraphicShaderModule::Create(type, elementSize, elementData);

	return true;
}

void OpenGLESShaderModule::Destroy()
{
	IGraphicShaderModule::Destroy();
}

