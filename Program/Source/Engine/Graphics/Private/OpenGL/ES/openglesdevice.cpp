#include "graphic.h"
#include "OpenGL/ES/openglesdevice.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, OpenGLESDevice, IGraphicDevice);

OpenGLESDevice::OpenGLESDevice()
{
	m_MaxVertexAttribs = 0;
}
OpenGLESDevice::~OpenGLESDevice()
{
}

void OpenGLESDevice::SetViewport(int32 x, int32 y, int32 width, int32 height, float minDepth, float maxDepth)
{
	glViewport(x, y, width, height);
}

bool OpenGLESDevice::CheckCapacity()
{
	ULOG(Log::_Debug, "***************************[Device Info]*********************************");

	const GLubyte* glslVer = glGetString(GL_SHADING_LANGUAGE_VERSION);
	ULOG(Log::_Debug, "[GLSL Version] %s", ConvertToString((const char*)glslVer).c_str());

	const GLubyte* esVer = glGetString(GL_VERSION);
	ULOG(Log::_Debug, "[OpenGL ES Version] %s", ConvertToString((const char*)esVer).c_str());

	GLboolean shadercompilerSupport;
	glGetBooleanv(GL_SHADER_COMPILER, &shadercompilerSupport);
	_ASSERT(shadercompilerSupport == GL_TRUE);

	// 바이너리 형식이 사용 가능한지 결정
	int numbinaryformat;
	glGetIntegerv(GL_NUM_SHADER_BINARY_FORMATS, &numbinaryformat);
	if (numbinaryformat > 0)
	{
		int* binaryformats = new int[numbinaryformat];

		// formats은 지원된 바이너리 형식을 얻어온다.
		glGetIntegerv(GL_SHADER_BINARY_FORMATS, binaryformats);

		ULOG(Log::_Debug, "glShaderBinary Support!");
		for (int i = 0; i < numbinaryformat; i++)
			ULOG(Log::_Debug, "\tformat : %d", binaryformats[i]);

		delete[] binaryformats;
	}

	//m_Flags |= (numbinaryformat > 0) && (COpenGLESExt::Instance().IsGLExtensionSupported("GL_OES_get_program_binary") == true) ? _SHADERBINARY : 0;

	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &m_MaxVertexAttribs);
	_ASSERT(m_MaxVertexAttribs <= 16);

	ULOG(Log::_Debug, "*************************************************************************");
	return true;
}