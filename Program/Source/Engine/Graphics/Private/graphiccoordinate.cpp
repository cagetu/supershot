#include "graphicheader.h"
#if _SUPPORT_D3D11 || _SUPPORT_D3D12
#include "DirectX/d3dcoordinate.h"
#endif
#if _SUPPORT_OPENGL_ES
#include "OpenGL/openglcoordinate.h"
#endif
#if _SUPPORT_VULKAN
#include "Vulkan/vulkancoordinate.h"
#endif

ICoordinate* g_pCoordinate = nullptr;

void Coordinate::_Startup(COORDINATE coordinate)
{
	// 플랫폼에 따른 Impl 작성해서 적용해주자!!!!
	switch (coordinate)
	{
	case _D3D:
#if _SUPPORT_D3D11 || _SUPPORT_D3D12
		g_pCoordinate = new D3DCoordinate();
#endif
		break;

	case _OPENGL:
#if _SUPPORT_OPENGL_ES
		g_pCoordinate = new OpenGLCoordinate();
#endif
		break;

	case _VULKAN:
#if _SUPPORT_VULKAN
		g_pCoordinate = new VulkanCoordinate();
#endif
		break;

	default:
		_ASSERT(0);
	}
}

void Coordinate::_Shutdown()
{
	if (g_pCoordinate)
	{
		delete g_pCoordinate;
		g_pCoordinate = nullptr;
	}
}

Mat4 Coordinate::LookAt(const Vec3& pivot, const Vec3& target, const Vec3& upvec)
{
	Vec3 zaxis = Vec3::Normalize(target - pivot);
	Vec3 xaxis = Vec3::Normalize(Vec3::CrossProduct(upvec, zaxis));
	//	Vec3 yaxis = Vec3::Normalize(Vec3::CrossProduct(zaxis, xaxis));
	Vec3 yaxis = Vec3::CrossProduct(zaxis, xaxis);

	Mat4 m;
	m._11 = xaxis.x; m._12 = xaxis.y; m._13 = xaxis.z; m._14 = 0.0f;
	m._21 = yaxis.x; m._22 = yaxis.y; m._23 = yaxis.z; m._24 = 0.0f;
	m._31 = zaxis.x; m._32 = zaxis.y; m._33 = zaxis.z; m._34 = 0.0f;
	m._41 = pivot.x; m._42 = pivot.y; m._43 = pivot.z; m._44 = 1.0f;
	return m;
}

Mat4 Coordinate::LookAt(const Vec3& pivot, const Vec3& target)
{
	Vec3 z = Vec3::Normalize(target - pivot);
	return LookAt(pivot, target, ::abs(z.y) > 0.999f ? Vec3::ZAXIS : Vec3::YAXIS);
}

Mat4 Coordinate::LookAtZAxis(const Vec3& pivot, const Vec3& zaxis)
{
	Vec3 upvec = ::abs(zaxis.y) > 0.999f ? Vec3::ZAXIS : Vec3::YAXIS;

	Vec3 xaxis = Vec3::Normalize(Vec3::CrossProduct(upvec, zaxis));
	Vec3 yaxis = Vec3::CrossProduct(zaxis, xaxis);

	Mat4 m;
	m._11 = xaxis.x; m._12 = xaxis.y; m._13 = xaxis.z; m._14 = 0.0f;
	m._21 = yaxis.x; m._22 = yaxis.y; m._23 = yaxis.z; m._24 = 0.0f;
	m._31 = zaxis.x; m._32 = zaxis.y; m._33 = zaxis.z; m._34 = 0.0f;
	m._41 = pivot.x; m._42 = pivot.y; m._43 = pivot.z; m._44 = 1.0f;
	return m;
}

Mat4 Coordinate::Projection(float nearWidth, float nearHeight, float nearClip, float farClip)
{
	return g_pCoordinate->Projection(nearWidth, nearHeight, nearClip, farClip);
}

//	@desc : z 구간으로 매핑되는 정규 시야 영역으로 변환
Mat4 Coordinate::ProjectionWithFov(float fov, float nearClip, float farClip, float aspectRatio)
{
	return g_pCoordinate->ProjectionWithFov(fov, nearClip, farClip, aspectRatio);
}

// @desc : z 구간으로 매핑되는 정규 시야 영역으로 변환
Mat4 Coordinate::ProjectionOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	return g_pCoordinate->ProjectionOffCenter(l, r, b, t, zn, zf);
}

//	@desc : z 구간으로 매핑되는 정규 시야 영역으로 변환
Mat4 Coordinate::InfiniteProjectionWithFov(float fov, float nearClip, float aspectRatio, float epsilon)
{
	return g_pCoordinate->InfiniteProjectionWithFov(fov, nearClip, aspectRatio, epsilon);
}

// @desc : 직교 투영
Mat4 Coordinate::OrthoOffCenter(float left, float right, float bottom, float top, float nearClip, float farClip)
{
	return g_pCoordinate->OrthoOffCenter(left, right, bottom, top, nearClip, farClip);
}

// @desc : 직교투영
Mat4 Coordinate::Ortho(float width, float height, float nearClip, float farClip)
{
	return g_pCoordinate->Ortho(width, height, nearClip, farClip);
}
