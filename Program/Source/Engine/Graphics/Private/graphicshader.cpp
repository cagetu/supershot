// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "graphicshader.h"

//------------------------------------------------------------------
__ImplementRtti(Gfx, IGraphicShader, IGraphicResource);

IGraphicShader::IGraphicShader()
{
}

IGraphicShader::~IGraphicShader()
{
}

const ShaderFeature::MASK& IGraphicShader::GetFeatureFlags() const
{
	return m_FeatureFlags;
}
