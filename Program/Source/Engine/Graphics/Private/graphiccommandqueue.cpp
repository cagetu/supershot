#include "foundation.h"
#include "graphiccommandqueue.h"

//------------------------------------------------------------------
__ImplementRootRtti(IGraphicCommandQueue);

IGraphicCommandQueue::IGraphicCommandQueue(COMMANDLIST_TYPE queueType)
	: m_CommandQueueType(queueType)
{
}

IGraphicCommandQueue::~IGraphicCommandQueue()
{
}
