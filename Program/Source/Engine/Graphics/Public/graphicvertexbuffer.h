// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"

//------------------------------------------------------------------
/**	@class	Base VertexBuffer
	@desc	그래픽 리소스로 사용되는 버텍스 버퍼
*/
//------------------------------------------------------------------
class IGraphicVertexBuffer : public IGraphicResource
{
	__DeclareRtti;
public:
	IGraphicVertexBuffer();
	virtual ~IGraphicVertexBuffer();

	virtual bool Create(int32 bufferSize) = 0;
	virtual void Destroy() = 0;
};
