// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

struct VertexElement
{
	enum class SEMENTIC
	{
		Invalid = -1,

		/// Position
		Position = 0,
		PositionT,
		/// Normal
		Normal,
		NormalUB4N,
		/// Texcoords
		Uv0,
		Uv0S2,
		Uv1,
		Uv1S2,
		Uv2,
		Uv2S2,
		Uv3,
		Uv3S2,
		/// Color
		Color,
		ColorUB4N,
		/// Tangent
		Tangent,
		TangentUB4N,
		/// Binormal
		Binormal,
		BinormalUB4N,
		/// BlendWeights
		BlendWeights,
		BlendWeightsUB4N,
		BlendIndices,
		BlendIndicesUB4,
		/// BlendWeights
		BlendWeights02,
		BlendWeightsUB4N02,
		BlendIndices02,
		BlendIndicesUB402,
	};

	enum class FORMAT
	{
		Float,		//> one-component float, expanded to (float, 0, 0, 1)
		Float2,		//> two-component float, expanded to (float, float, 0, 1)
		Float3,		//> three-component float, expanded to (float, float, float, 1)
		Float4,		//> four-component float
		Short2,		//> two-component signed short, expanded to (value, value, 0, 1)
		Short4,		//> four-component signed short
		Short2N,	//> two-component normalized signed short (value / 32767.0f)
		Short4N,	//> four-component normalized signed short (value / 32767.0f)
		UShort2,	//> two-component unsigned short, expanded to (value, value, 0, 1)
		UShort4,	//> four-component unsigned short
		UShort2N,	//> two-component normalized unsigned short (value / 32767.0f)
		UShort4N,	//> four-component normalized unsigned short (value / 32767.0f)
		Byte4,		//> four-component signed byte
		Byte4N,		//> four-component normalized signed byte (value / 255.0f)
		UByte4,		//> four-component unsigned byte
		UByte4N,	//> four-component normalized unsigned byte (value / 255.0f)
		Color,
		Int,
		Int2,
		Int3,
		Int4,
		UInt,
		UInt2,
		UInt3,
		UInt4,
		Double,
		Double2,
		Double3,
		Half2,
		Half4,
	};

	VertexElement();
	VertexElement(SEMENTIC eSementic, FORMAT eFormat, ushort usageIndex);

	SEMENTIC	GetSementic() const;
	FORMAT		GetFormat() const;
	ushort		GetTypeCount() const;
	ushort		GetByteSize() const;
	ushort		GetUsageIndex() const;
	bool		IsNormalizedType() const;

	SEMENTIC	sementic;
	FORMAT		format;
	ushort		usageIndex;
};
