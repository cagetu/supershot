// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandtypes.h"

//------------------------------------------------------------------
/**	@class	Base Command Queue
	@desc	Command Queue의 기본 객체
			Command 종류에 따라서 다른 커맨드 큐를 가진다.

			- VulkanCommandQueue
			- D3D12CommandQueue

			- CommandQueue
				- Submit(CommandList)
*/
//------------------------------------------------------------------
class IGraphicCommandQueue
{
	__DeclareRootRtti(IGraphicCommandQueue);
public:
	IGraphicCommandQueue(COMMANDLIST_TYPE queueType);
	virtual ~IGraphicCommandQueue();

	COMMANDLIST_TYPE GetCommandListType() const { return m_CommandQueueType; }

protected:
	COMMANDLIST_TYPE m_CommandQueueType;
};
