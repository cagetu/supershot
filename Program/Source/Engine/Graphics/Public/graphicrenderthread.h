// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	@class	Base Render Thread
	@desc
*/
//------------------------------------------------------------------
class IGraphicRenderThread
{
	__DeclareRootRtti(IGraphicRenderThread);
public:
	IGraphicRenderThread();
	virtual ~IGraphicRenderThread();
};
