// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Math\matrix.h"

class ICoordinate
{
public:
	virtual Mat4 Projection(float nearwidth, float nearheight, float nearclip, float farclip) = 0;
	virtual Mat4 ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio) = 0;
	virtual Mat4 ProjectionOffCenter(float l, float r, float b, float t, float nearclip, float farclip) = 0;
	virtual Mat4 InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon = 2.4f*10e-7f) = 0;
	virtual Mat4 Ortho(float w, float h, float nearclip, float farclip) = 0;
	virtual Mat4 OrthoOffCenter(float left, float right, float bottom, float top, float nearclip, float farclip) = 0;
};

//------------------------------------------------------------------
/** @class Coordinate
	@desc 그래픽 API에 기반한 좌표계에 맞는 변환 클래스 렌더링 정의할 때 같이 정의 되어야 할 듯
*/
//------------------------------------------------------------------
struct Coordinate
{
	enum COORDINATE
	{
		_D3D = 0,
		_OPENGL,
		_VULKAN,
	};
	static void _Startup(COORDINATE coordinate);
	static void _Shutdown();

public:
	static Mat4 LookAt(const Vec3& pivot, const Vec3& target, const Vec3& upvec);
	static Mat4 LookAt(const Vec3& pivot, const Vec3& target);
	static Mat4 LookAtZAxis(const Vec3& pivot, const Vec3& zaxis);

	static Mat4 Projection(float nearWidth, float nearHeight, float nearClip, float farClip);
	static Mat4 ProjectionWithFov(float fov, float nearClip, float farClip, float aspectRatio);
	static Mat4 ProjectionOffCenter(float left, float right, float bottom, float top, float nearClip, float farClip);
	static Mat4 InfiniteProjectionWithFov(float fov, float nearClip, float aspectRatio, float epsilon = 2.4f*10e-7f);
	static Mat4 Ortho(float w, float h, float nearClip, float farClip);
	static Mat4 OrthoOffCenter(float left, float right, float bottom, float top, float nearClip, float farClip);
};