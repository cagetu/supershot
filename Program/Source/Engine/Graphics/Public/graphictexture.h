// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"
#include "graphictypes.h"

//------------------------------------------------------------------
/**	@class	D3D12Texture
	@desc	D3D12 베이트 텍스쳐 클래스
*/
//------------------------------------------------------------------
class IGraphicTexture : public IGraphicResource
{
	__DeclareRtti;
public:
	IGraphicTexture();
	virtual ~IGraphicTexture();

	int32 Width() const { return m_Width; }
	int32 Height() const { return m_Height; }
	int32 MipLevels() const { return m_MipLevels; }
	FORMAT_TYPE Format() const { return m_Format; }

protected:
	int32 m_Width;
	int32 m_Height;
	int32 m_MipLevels;
	FORMAT_TYPE m_Format;
};
