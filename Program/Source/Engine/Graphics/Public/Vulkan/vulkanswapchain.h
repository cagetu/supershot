// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicswapchain.h"

class VulkanCommandQueue;
class VulkanSemaphore;
class VulkanImageView;
class VulkanColorRenderTexture;

//------------------------------------------------------------------
/**	@class	VulkanSwapchain
	@desc	Swapchain 클래스
			n개의 vkImage 를 돌려가면서 사용한다.

			1. surface 생성
			2. surface format
			3. present mode 설정
			4. swapchain 생성

			- present
			- image
				- swap chain은 여러개의 image로 구성되어 있다.
				- image 하나 마다 하나의 Queue로 바인딩 된다.
*/
//------------------------------------------------------------------
class VulkanSwapchain : public IGraphicSwapchain
{
	__DeclareRtti;
public:
	VulkanSwapchain();
	virtual ~VulkanSwapchain();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, IGraphicCommandQueue* commandQueue = nullptr, uint32 surfaceCount = 2) override;
	virtual void Destroy() override;

	virtual bool Resize(const WindowHandle& windowHandle) override;

public:
	VkSwapchainKHR Handle() const { return m_SwapChainHandle; }

	VulkanColorRenderTexture* GetBackBuffer(uint32 index) const;
	SizeT GetBackBufferCount() const { return m_BackBufferCount; }
	
	VULKAN::_RESULT GetNextBackBufferIndex(int32& acquiredImageIndex, VulkanSemaphore** acquiredSemaphore, int32& acquiredSemaphoreIndex);
	int32 GetCurrentBackBufferIndex() const { return m_CurrentBackBufferIndex; }

	VULKAN::_RESULT Present(VulkanCommandQueue* presentQueue, VulkanSemaphore* renderCompleteSignal, bool bWaitForIdle = true);

private:
	bool CreateSwapChain(const WindowHandle& windowHandle, IGraphicCommandQueue* commandQueue, FORMAT_TYPE format, uint32 surfaceCount);
	void DestroySwapChain();

	bool CreateBackBuffers(int32 width, int32 height, FORMAT_TYPE format);
	void DestroyBackBuffers();

	VkSwapchainKHR m_SwapChainHandle;

	Array<VulkanColorRenderTexture*> m_BackBuffers;

	VulkanSemaphore* m_ImageAcquiredWaitSemaphores;
	int32 m_CurrentSemaphoreIndex;
	int32 m_CurrentBackBufferIndex;
};