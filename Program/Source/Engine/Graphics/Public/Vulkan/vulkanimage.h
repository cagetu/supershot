// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Vulkan/vulkanresource.h"
#include "Vulkan/vulkandescriptor.h"

//------------------------------------------------------------------
/**	@class	VulkanImage
	@desc	Image 리소스 객체
			Image 생성 -> Image 메모리 할당 -> ImageLayout 설정 -> ImageView 생성
			
			텍스쳐와 렌더타겟의 소스가 된다.
*/
//------------------------------------------------------------------
class VulkanImage : public VulkanResource
{
	__DeclareRtti;
public:
	VulkanImage();
	virtual ~VulkanImage();

	bool Create(uint32 width, uint32 height, uint32 depth, uint32 format, uint32 textureFlags, 
				VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D,
				VkImageUsageFlags defaultUsageFlags = VK_IMAGE_USAGE_TRANSFER_DST_BIT,
				uint32 mipMaps = 1, uint32 multiSamples = 1, bool isArray = false, bool isTilingLinear = false);
	bool Create(VkImage imageHandle);
	void Destroy();

	VkImage Handle() const { return m_ImageHandle; }

private:
	VkImage m_ImageHandle;
	VmaAllocation m_Allocation;
};

//------------------------------------------------------------------
/**	@class	VulkanImageView
	@desc	Image 리소스 객체의 상호작용 객체
			Image 생성 -> Image 메모리 할당

			주로 셰이더 등에 입력이나 아우풋으로 잘 활용된다.
*/
//------------------------------------------------------------------
class VulkanImageView : public VulkanDescriptor
{
	__DeclareRtti;
public:
	VulkanImageView();
	virtual ~VulkanImageView();

	bool Create(VkImage image, int32 format = FMT_R8G8B8A8, VkImageAspectFlags aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D, uint32 mipMaps = 1, uint32 arraySize = 1);
	void Destroy();

	uint32 NumMipMaps() const { return m_NumMipmaps; }
	uint32 NumArrays() const { return m_NumArrays; }

	bool IsValid() const { return (m_ImageViewHandle != VK_NULL_HANDLE); }
	bool IsArray() const { return m_NumArrays > 1; }

	VkImageView Handle() const { return m_ImageViewHandle; }

private:
	VkImageView m_ImageViewHandle;

	uint32 m_NumMipmaps;
	uint32 m_NumArrays;
};
