﻿// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrenderthread.h"

class VulkanCommandAllocator;
class VulkanCommandQueue;
class VulkanRenderFrame;

//------------------------------------------------------------------
/**	@class	VulkanRenderThread
	@desc	Vulkan에서 렌더링 프로세싱을 하는 단위이다.

			RenderThread
				- RenderFrame (N)
				- RenderFrame (N+1)
				- RenderFrame (N+2)
*/
//------------------------------------------------------------------
class VulkanRenderThread : public IGraphicRenderThread
{
	__DeclareRtti;
public:
	VulkanRenderThread();
	virtual ~VulkanRenderThread();

	void AddFrame(VulkanRenderFrame* renderFrame);
	void RemoveFrame(VulkanRenderFrame* renderFrame);

private:
	Array<VulkanRenderFrame*> m_RenderFrame;
};
