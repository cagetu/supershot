// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrenderwindow.h"

class VulkanSemaphore;
class VulkanSwapchain;
class VulkanCommandQueue;
class VulkanCommandList;
class VulkanCommandAllocator;
class VulkanFrameBuffer;
class VulkanRenderPass;
class VulkanDepthStencilRenderTexture;
class VulkanResolveRenderTexture;

//------------------------------------------------------------------
/**	@class	VulkanRenderWindow
	@desc	화면에 그려지는 공간에 대한 객체이다. (Viewport로 하는 것이 더 맞을지도 모른다)
*/
//------------------------------------------------------------------
class VulkanRenderWindow : public IGraphicRenderWindow
{
	__DeclareRtti;
public:
	VulkanRenderWindow();
	virtual ~VulkanRenderWindow();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, bool bUseDepthSurface = true) override;
	virtual void Destroy() override;

	virtual void Resize(const WindowHandle& windowHandle) override;

	void BeginFrame(int32 clearFlags, const LinearColor& clearColor, float clearDepth, ushort clearStencil);
	void EndFrame();

	void Present();

	void BlockingSwapBuffers();

private:
	bool CreateDepthSurface(uint32 width, uint32 height, FORMAT_TYPE format = FMT_D24S8);
	void DestroyDepthSurface();

	bool CreateResolveSurface(uint32 width, uint32 height, FORMAT_TYPE format);
	void DestroyResolveSurface();

	bool CreateCommandQueue();
	void DestroyCommandQueue();

	bool CreateCommandLists(int32 requireBufferCount);
	void DestroyCommandLists();

	bool CreateFrameBuffers(int32 requireBufferCount);

	void CreateRenderPass(VulkanFrameBuffer& currentFrameBuffer, int32 clearFlags, const LinearColor& clearColor, float clearDepth, ushort clearStencil);

	Ptr<VulkanSwapchain> m_SwapChain;
	VulkanDepthStencilRenderTexture* m_DepthStencilSurface;
	VulkanResolveRenderTexture* m_ResolveSurface;

	VulkanCommandQueue* m_GfxCommandQueue;
	VulkanCommandAllocator* m_CommandPool;
	VulkanCommandList* m_CommandLists[_NUM_SWAP_BUFFERS];

	VulkanSemaphore* m_RenderCompleteSemaphores[_NUM_SWAP_BUFFERS];
	VulkanSemaphore* m_PresentCompleteSemaphore;
	int32 m_PresentCompleteSemaphoreIndex;
	int32 m_AcquiredSwapIndex;

	VulkanFrameBuffer* m_FrameBuffers;
};
