// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "color.h"

//------------------------------------------------------------------
/**	@class
	RenderPass
		1. RenderPass에서 사용하는 모든 Attachment 목록을 추가한다.
		2. SubPass에서는 RenderPass에 추가된 Attachment를 가지고 와서 사용한다.
		3. 하나의 RenderPass가 하나의 한 프레임을 담당한다.
		4. Dependency는 조금 더 봐야 할 듯....
		5. 하나의 RenderPass와 FrameBuffer가 한 쌍이 될 것 같은데...
		SubPass
			- RenderTarget(Color)
			- RenderTarget(Position)
			- RenderTarget(Normal)
			- RenderTarget(Depth)
		SubPass
			- RenderTarget(Color)
			- RenderTarget(Depth)
*/
//------------------------------------------------------------------
namespace Vk
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class SubPassLayout
{
public:
	struct Attachment
	{
		VkImageView imageView;
		FORMAT_TYPE format;
		int32 multiSamples;
		Vk::ImageLayoutOp layout;
	};

	struct ColorAttachmentInfo : public Attachment
	{
		LinearColor clearColor;
		Vk::AttachmentOp colorOp;
	};
	struct DepthStencilAttachmentInfo : public Attachment
	{
		float clearDepth;
		ushort clearStencil;
		Vk::AttachmentOp depthOp;
		Vk::AttachmentOp stencilOp;
	};
	struct ResolveAttachmentInfo : public Attachment
	{
		Vk::AttachmentOp resolveOp;
	};

	ColorAttachmentInfo ColorAttachments[_MAX_MRT];
	DepthStencilAttachmentInfo DepthStencilAttachment;
	ResolveAttachmentInfo ResolveAttachment;

	/// Color Image Attachment 개수
	int32 NumColorAttachments;
	/// Detph Image Attachment 사용 여부
	bool HasDepthStencilAttachment;
	/// Resolve Image Attachment 여부
	bool HasResolveAttachment;

public:
	SubPassLayout();

	void AddColorAttachment(VkImageView imageView, FORMAT_TYPE format, int32 multiSamples, const Vk::ImageLayoutOp& layout, const LinearColor& clearColor, const Vk::AttachmentOp& colorOp);
	void AddDepthStencilAttachment(VkImageView imageView, FORMAT_TYPE format, int32 multiSamples, const Vk::ImageLayoutOp& layout, 
		const Vk::AttachmentOp& depthOp, float clearDepth, 
		const Vk::AttachmentOp& stencilOp, ushort clearStencil);

	void AddResolveAttachment(VkImageView imageView, FORMAT_TYPE format, int32 multisamples, const Vk::ImageLayoutOp& layout, const Vk::AttachmentOp& resolveOp);
	void Clear();

	bool operator==(const SubPassLayout& rhs);
	bool operator==(const SubPassLayout& rhs) const;
	bool operator!=(const SubPassLayout& rhs);
	bool operator!=(const SubPassLayout& rhs) const;

	bool Compare(const SubPassLayout& rhs) const;
};

class RenderPassLayout
{
public:
	RenderPassLayout();

	int32 AddSubPass(const SubPassLayout& subPassLayout);
	void RemoveSubPass(int32 index);
	void ClearSubPasses();

public:
	int32 Width;
	int32 Height;

	Array<SubPassLayout> SubPassLayouts;
};

struct SubPassAttachmentReference
{
	VkAttachmentReference ColorAttachments[_MAX_MRT];
	VkAttachmentReference InputAttachments[_MAX_MRT + 1];
	VkAttachmentReference DepthStencilAttachment;
	VkAttachmentReference ResolveAttachment;
	uint32 PreserveAttachments[_MAX_MRT];

	int32 NumColorAttachments;
	int32 NumInputAttachments;
	int32 NumPreserveAttachments;
	bool HasDepthStencilAttachment;
	bool HasResolveAttachment;

	void Reset();
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // end of namespace