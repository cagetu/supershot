// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class VulkanRenderPass;
class VkRenderPassLayout;

//------------------------------------------------------------------
/**	@class	VulkanFrameBuffer
	@desc	FrameBuffer 설정을 하기 위한 클래스 
*/
//------------------------------------------------------------------
class VulkanFrameBuffer
{
	__DeclareReference(VulkanFrameBuffer);
public:
	VulkanFrameBuffer();
	~VulkanFrameBuffer();

	bool Create(VulkanRenderPass* renderPass);
	bool Create(const Vk::RenderPassLayout& renderPassLayout);
	void Destroy();

public:
	VkFramebuffer Handle() const { return m_FrameBufferHandle; }

	VulkanRenderPass* RenderPass() const { return m_RenderPass; }

private:
	VkFramebuffer m_FrameBufferHandle;

	Ptr<VulkanRenderPass> m_RenderPass;
};
