// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Vulkan\vulkanheader.h"

//------------------------------------------------------------------
/**	@class	PhysicalDevice
	@desc	그래픽 카드 (Adaptor)에 대한 클래스.
			그래픽카드에 대한 Capability 등 정보를 가진다.
*/
//------------------------------------------------------------------
class VulkanPhysicalDevice
{
public:
	VkPhysicalDevice DeviceHandle;
	VkPhysicalDeviceProperties DeviceProps;
	VkPhysicalDeviceFeatures DeviceFeatures;

	uint32 QueueFlags;
	uint32 NumQueueFamily;
	VkBool32 bQueueSupportPresents[5];
	VkQueueFamilyProperties QueueFamilyProps[5];

	struct _FORMAT
	{
		VkFormat format;
		VkComponentMapping mapping;
		uint32 bpp;
		bool isSupported;
	};
	_FORMAT PixelFormats[FMT_MAX];
	VkFormatProperties FormatProperties[VK_FORMAT_RANGE_SIZE];
	//mutable std::map<VkFormat, VkFormatProperties> m_ExtensionFormatProperties;

	uint32 NumSurfaceFormats;
	VkSurfaceFormatKHR SurfaceFormats[VK_FORMAT_RANGE_SIZE];

	VkSurfaceCapabilitiesKHR SurfaceCapabilities;

	uint32 NumSurfacePresentModes;
	VkPresentModeKHR SurfacePresentModes[VK_PRESENT_MODE_RANGE_SIZE_KHR];

public:
	void Create(VkPhysicalDevice InDeviceHandle, VkSurfaceKHR InSurfaceHandle);
	void Destroy();

	bool IsDiscreteGPU() const;
	bool IsSupportGPU(VkQueueFlagBits operationType) const;
	bool IsSupportPresent(uint32 familyIndex) const;
	bool IsFormatSupported(VkFormat Format) const;

	VkFormat GetFormat(uint32 format) const;
	const VkComponentMapping& GetFormatCommpoentMapping(uint32 format) const;
	uint32 GetFormatBlockByteSize(uint32 format) const;
	VkSampleCountFlagBits GetSampleCount(uint32 sampleCount) const;

private:
	void SetupFormats();
	void SetFormat(uint32 format, VkFormat vkFormat, const VkComponentMapping& vkComponent);

	void EnumulateSurfaceCapacities(VkSurfaceKHR InSurfaceHandle);
};
