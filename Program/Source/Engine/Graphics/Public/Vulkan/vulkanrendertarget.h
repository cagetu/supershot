// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrendertarget.h"

class VulkanColorRenderTexture;
class VulkanDepthStencilRenderTexture;
class VulkanRenderTexture;

//------------------------------------------------------------------
/**	@class	RenderTarget
	@desc	하나의 RenderPass에서 사용하게 될 RenderTarget
			N개의 Surface를 연결할 수 있다.
				- Texture(Color)
				- Texture(Position)
				- Texture(Normal)
				- Texture(Depth)

			Vulkan에서는 SubPass와 1:1로 대응된다.

		  <pass name="MRT 장면렌더링" test="_MRTSCENE&!_MRTMATERIAL&!_MRTVELOCITY" mask="_MRT,_PRIMITIVE,_SORT,_USEGLOBALCLEARCOLOR,_CLEARCOLOR,_CLEARDEPTH,_RT_SCREENSIZE,_RT_SHARE_DEPTH">
			<pixelformat name="DepthBuffer">R16G16B16A16F</pixelformat>
			<pixelformat name="OutputBuffer">R16G16B16A16F</pixelformat>
			<pixelformat name="MaterialBuffer">R16G16B16A16F</pixelformat>
			<clearcolor>ffffffff</clearcolor>
			<cleardepth>1</cleardepth>
			<view>Test</view>
*/
//------------------------------------------------------------------
class VulkanRenderTarget : public IGraphicRenderTarget
{
	__DeclareRtti;
public:
	VulkanRenderTarget();
	virtual ~VulkanRenderTarget();

	virtual bool Create(int width, int height, int32 multisample = 1);
	virtual void Destroy();

	// ColorBuffer
	void SetRenderTexture(VulkanColorRenderTexture* surface);
	void AddRenderTexture(VulkanColorRenderTexture* surface);
	//VulkanRenderTexture* AddRenderTexture(FORMAT_TYPE format, RT_LOAD_TYPE loadAction = RT_LOAD_TYPE::Clear, RT_STORE_TYPE storeAction = RT_STORE_TYPE::Store);
	void ClearRenderTextures();
	VulkanRenderTexture* GetRenderTexture(int32 channel) const;
	VulkanRenderTexture* GetRenderTexture(const String& id) const;
	int32 GetNumRenderTextures() const;

	// DepthBuffer
	void SetDepthTexture(VulkanDepthStencilRenderTexture* surface);
	void CreateDepthTexture(FORMAT_TYPE format, RT_LOAD_TYPE loadAction = RT_LOAD_TYPE::Clear, RT_STORE_TYPE storeAction = RT_STORE_TYPE::Store, RT_LOAD_TYPE stencilLoadAction = RT_LOAD_TYPE::None, RT_STORE_TYPE stencilStoreAction = RT_STORE_TYPE::None);
	VulkanDepthStencilRenderTexture* GetDepthTexture() const;

private:
	Array<VulkanColorRenderTexture*> m_ColorSurfaces;
	VulkanDepthStencilRenderTexture* m_DepthStencilSurface;
};

//------------------------------------------------------------------
/**	@class	RenderTargetPool
	@desc	한 프레임에서 사용되는 렌더타겟에 대한 목록이 필요하다.
			렌더타겟을 관리하는 목록이 있어야 한다.
*/
//------------------------------------------------------------------
class VulkanRenderTargetPool
{

};
