// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshaderinstance.h"
#include "Vulkan/vulkandescriptorlayout.h"

class VulkanShaderModule;
class VulkanShaderModuleInstance;

//------------------------------------------------------------------
/**	@class	ShaderInstance
	@desc	셰이더 객체
			- vertexshader
			- pixelshader

			하나의 기능과 연결되는 셰이더 단위

*/
//------------------------------------------------------------------
class VulkanShaderInstance : public IGraphicShaderInstance
{
	__DeclareRtti;
public:
	VulkanShaderInstance();
	virtual ~VulkanShaderInstance();

	bool BindShader(IGraphicShaderModule* shaderModule);

private:
	// 셰이더
	//VulkanShaderModuleInstance* m_ShaderModuleInstances[ShaderType::NUM];

	// 버텍스 입력

	// 디스크립터들
	VulkanDescriptorSetsLayout m_DescriptorSetsLayout;
	VulkanDescriptorSets* m_DescriptorSets;
};

//------------------------------------------------------------------
/**	@class	ShaderModuleInstance
	@desc	각 종류 별 셰이더
			ShaderModule을 참고하여, 실제 값을 바인딩해준다.
*/
//------------------------------------------------------------------
class VulkanShaderModuleInstance : public IGraphicShaderModuleInstance
{
	__DeclareRtti;
public:
	VulkanShaderModuleInstance();
	virtual ~VulkanShaderModuleInstance();

	virtual bool Create(IGraphicShaderModule* shaderModule) override;
	virtual void Destroy() override;

	virtual void SetValue(uint32 index, const Variant& value) override;
	//virtual void UpdateValues(const CMaterial* material) = 0;
	virtual void ClearValues() override;

	//virtual void UpdateVertexAttributes(const VertexBuffer* vertexBuffer) {}
	//virtual void ClearVertexAttributes() {}

protected:
	VulkanShaderModule* m_ShaderModule;
};
