﻿// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Vulkan/vulkanresource.h"
#include "Vulkan/vulkandescriptor.h"

//------------------------------------------------------------------
/**	@class	VulkanBuffer
	@desc	Buffer 리소스 객체
			Buffer 생성 -> Buffer 메모리 할당 -> BufferLayout 설정 -> BufferView 생성
*/
//------------------------------------------------------------------
class VulkanBuffer : public VulkanResource
{
	__DeclareRtti;
public:
	VulkanBuffer();
	virtual ~VulkanBuffer();

	bool Create(uint32 size, BufferUsageFlags usageFlag);
	void Destroy();

	VkBuffer Handle() const { return m_BufferHandle; }

private:
	VkBuffer m_BufferHandle;
	VmaAllocation m_Allocation;
};

//------------------------------------------------------------------
/**	@class	VulkanBufferView
	@desc	Buffer 리소스 객체의 상호작용 객체
			Buffer 생성 -> Buffer 메모리 할당

			주로 셰이더 등에 Input Or Ouput으로 잘 활용.
*/
//------------------------------------------------------------------
class VulkanBufferView : public VulkanDescriptor
{
	__DeclareRtti;
public:
	VulkanBufferView();
	virtual ~VulkanBufferView();

	bool Create(VkBuffer buffer, int32 format);
	void Destroy();

private:
	VkBufferView m_BufferViewHandle;
};
