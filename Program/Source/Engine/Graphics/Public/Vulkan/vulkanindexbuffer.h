// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicindexbuffer.h"

class VulkanBuffer;

//------------------------------------------------------------------
/**	@class	VulkanIndexBuffer
	@desc	인덱스 버퍼
*/
//------------------------------------------------------------------
class VulkanIndexBuffer : public IGraphicIndexBuffer
{
	__DeclareRtti;
public:
	VulkanIndexBuffer();
	virtual ~VulkanIndexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;

private:
	VulkanBuffer* m_Buffer;
};
