// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "vulkanresource.h"

//------------------------------------------------------------------
/**	@class	VulkanUniformBuffer
	@desc	유니폼 버퍼
*/
//------------------------------------------------------------------
class VulkanUniformBuffer : public VulkanResource
{
	__DeclareRtti;
public:
	VulkanUniformBuffer();
	virtual ~VulkanUniformBuffer();
};
