// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

// Configuration
// API version we want to target.
#if TARGET_OS_WINDOWS
#define VULKAN_API_VERSION	VK_MAKE_VERSION(1, 0, 1)
#elif TARGET_OS_ANDROID
#define VULKAN_API_VERSION	VK_MAKE_VERSION(1, 0, 1)
#else
#error Unsupported platform!
#endif

#if _DEBUG || TARGET_OS_WINDOWS
#define VULKAN_HAS_DEBUGGING_ENABLED 1 //!!!
#else
#define VULKAN_HAS_DEBUGGING_ENABLED 0
#endif

// constants we probably will change a few times
#define VULKAN_UB_RING_BUFFER_SIZE								(8 * 1024 * 1024)
#define VULKAN_TEMP_FRAME_ALLOCATOR_SIZE						(8 * 1024 * 1024)


// Enables the VK_LAYER_LUNARG_api_dump layer and the report VK_DEBUG_REPORT_INFORMATION_BIT_EXT flag
#define VULKAN_ENABLE_API_DUMP									0
// Enables logging wrappers per Vulkan call
#define VULKAN_ENABLE_DUMP_LAYER								0
#define VULKAN_ENABLE_DRAW_MARKERS								TARGET_OS_WINDOWS
#define VULKAN_ALLOW_MIDPASS_CLEAR								0

#define VULKAN_SINGLE_ALLOCATION_PER_RESOURCE					0

#define VULKAN_CUSTOM_MEMORY_MANAGER_ENABLED					0

// This disables/overrides some if the graphics pipeline setup
// Please remove this after we are done with testing
#if TARGET_OS_WINDOWS
#define VULKAN_DISABLE_DEBUG_CALLBACK						0	/* Disable the DebugReportFunction() callback in VulkanDebug.cpp */
#define VULKAN_USE_MSAA_RESOLVE_ATTACHMENTS					0	/* 1 = use resolve attachments, 0 = Use a command buffer vkResolveImage for MSAA resolve */
#define VULKAN_USE_RING_BUFFER_FOR_GLOBAL_UBS				1
#else
#define VULKAN_DISABLE_DEBUG_CALLBACK						1	/* Disable the DebugReportFunction() callback in VulkanDebug.cpp */
#define VULKAN_USE_MSAA_RESOLVE_ATTACHMENTS					1
#define VULKAN_USE_RING_BUFFER_FOR_GLOBAL_UBS				1
#endif

