// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

struct VulkanLayer
{
	static void GetInstanceLayersAndExtensions(Array<const char*>& outInstanceExtensions, Array<const char*>& OutInstanceLayers);
	static void GetDeviceLayerAndExtensions(VkPhysicalDevice InDevice, Array<const char*>& outDeviceExtensions, Array<const char*>& outDeviceLayers, bool& outDebugMarkers);
};
