// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdevice.h"
#include "graphiccommandtypes.h"
#include "Vulkan\vulkanheader.h"

class VulkanPhysicalDevice;
class VulkanCommandQueue;
class VulkanQueueFamily;
class VulkanCommandAllocator;
class VulkanDescriptorPool;
class VulkanFenceManager;
class VulkanMemoryAllocator;
class VulkanDescriptorSetsLayout;
class VulkanDescriptorSets;

//------------------------------------------------------------------
/**	@class	RenderDevice
	@desc	논리적 디바이스에 대한 클래스
			렌더링 디바이스를 적용한다. 
			추후, Compute Device 등은 어떻게 처리할지 고민이 필요!
*/
//------------------------------------------------------------------
class VulkanDevice : public IGraphicDevice
{
	__DeclareRtti;
public:
	VulkanDevice();
	virtual ~VulkanDevice();

	bool Create(VulkanPhysicalDevice* InPhysicalDevice);
	void Destroy();

	bool WaitForIdle();

	// 생성된 논리적 디바이스 핸들
	VkDevice Handle() const { return m_DeviceHandle; }

	// 물리 디바이스 객체
	VulkanPhysicalDevice* PhysicalDevice() const { return m_PhysicalDevice; }

public:
	VulkanCommandAllocator* CreateCommandPool(COMMANDLIST_TYPE commandQueueType, uint32 queueFamilyIndex, Vk::CommandPoolCreateFlag::Type createFlags = Vk::CommandPoolCreateFlag::_RESET_COMMAND_BUFFER_BIT);
	void DestroyCommandPool(VulkanCommandAllocator* commandPool);

	VulkanCommandQueue* CreateCommandQueue(COMMANDLIST_TYPE commandQueueType, bool bSupportPresent = false);
	void DestroyCommandQueue(VulkanCommandQueue* queue);

	VulkanDescriptorSets* CreateDescriptorSet(VulkanDescriptorSetsLayout* layout);
	void DestroyDescriptorSet(VulkanDescriptorSets* descriptorSets);

public:
	VulkanFenceManager* FenceManager() const { return m_FenceManager; }
	VulkanMemoryAllocator* Allocator() const { return m_Allocator; }

private:
	VkDevice m_DeviceHandle;

	VulkanPhysicalDevice* m_PhysicalDevice;

	// Thread를 분리하게 된다면, Queue을 생성하는 역할만 해주고, 관리는 하지 않는다!
	// 단, 삭제할 때 타이밍만 고려해주자!
	Mutex m_QueueSync;
	Array<VulkanQueueFamily*> m_QueueFamilies;

	// Thread를 분리하게 된다면, CommandPool을 생성하는 역할만 해주고, 관리는 하지 않는다!
	// 단, 삭제할 때 타이밍만 고려해주자!
	Mutex m_CommandPoolSync;
	Array<VulkanCommandAllocator*> m_CommandPools;

	Array<VulkanDescriptorPool*> m_DescriptorPools;

	VulkanFenceManager* m_FenceManager;
	VulkanMemoryAllocator* m_Allocator;
};
