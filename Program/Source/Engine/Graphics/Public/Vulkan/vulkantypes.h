// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictypes.h"

namespace Vk
{
	struct AttachmentOp
	{
        /// 이 (서브) 패스에 추가될 Buffer가 Load 될지를 정의
		RT_LOAD_TYPE load;
        
        /// 이 (서브) 패스가 마무리 될 때, Buffer가 Store 될지를 정의
		RT_STORE_TYPE store;

		AttachmentOp()
		{
			load = RT_LOAD_TYPE::None;
			store = RT_STORE_TYPE::None;
		}
		AttachmentOp(RT_LOAD_TYPE loadAction, RT_STORE_TYPE storeAction)
		{
			load = loadAction;
			store = storeAction;
		}

        void Reset()
        {
            load = RT_LOAD_TYPE::None;
            store = RT_STORE_TYPE::None;
        }
        
		bool operator == (const AttachmentOp& rhs)
		{
			return load == rhs.load && store == rhs.store;
		}

		bool operator < (const AttachmentOp& rhs)
		{
			return load < rhs.load && store == rhs.store;
		}
	};

	struct ImageLayoutOp
	{
		///	이 렌더패스 인스턴스가 시작될 때, 이 이미지가 서브 이미지가 된다.
		VkImageLayout initLayout;

		/// 렌더 패스 인스턴스가 끝나면 이미지는 정의한 이미지 레이아웃으로 변환된다. 
		///	- 주어진 렌더 패스 인스턴스에서 attachment는 각 서브 패스의 필요에 따라 다른 레이아웃을 가질 수 있다. 
		VkImageLayout finalLayout;

		ImageLayoutOp()
		{
			initLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		}
		ImageLayoutOp(VkImageLayout initlayout, VkImageLayout finallayout)
		{
			initLayout = initlayout;
			finalLayout = finallayout;
		}

		void Reset()
		{
			initLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		}
	};

	struct CommandPoolCreateFlag
	{
		typedef int8 Type;
		enum
		{
			_NONE = 0,
			_TRANSIENT_BIT = 1 << 0,			// Pool로 부터 가져온 command buffer가 짧게 사용되고 사용 이후 Pool로 반환될 때
			_RESET_COMMAND_BUFFER_BIT = 1 << 1,	// vkResetCommandBuffer -> 명시적으로 Reset된다. vkBeginCommandBuffer -> 암묵적으로 Reset된다.
		};
	};
}

#define _MAX_MRT 8

template <>
struct TTypeTraits<VkDescriptorSetLayoutBinding> : public TTypeTraitsBase<VkDescriptorSetLayoutBinding>
{
	enum { IsBytewiseComparable = true };
};

extern VkAttachmentLoadOp LoadAction(RT_LOAD_TYPE loadAction);
extern VkAttachmentStoreOp StoreAction(RT_STORE_TYPE storeAction);

