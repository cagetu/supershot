// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	@class	Barrier
	@desc	같은 큐에 제출되는 커멘드들이나 같은 서브패스 내의 커멘드들 사이에
			종속성을 주입하기 위해 사용된다.
*/
//------------------------------------------------------------------


// ImageMemoryBarrier
//	- 지정한 이미지의 특정 범위(Subresource Range)에 대해서 메모리 접근을 적용한다.
//
//	1. 지정한 이미지의 특정 범위에 대해서 layout 전환을 수행할 때 (리소스 최적화를 위해서)
//	2. 하나의 Queue에서 다른 Queue로 이미지의 특정 범위에 대한 소유권을 넘길 때

/*
	VK_IMAGE_LAYOUT_UNDEFINED
	- device 접근을 지원하지 않는다.
	- 이 layout은 반드시 initialLayout이나 image transition에서 oldLayout에서 사용되어야만 한다.
	- 이 layout의 밖으로 transition 될 때, 메모리의 내용이 보존된다 것을 보장하지 않는다.

	VK_IMAGE_LAYOUT_GENERAL
	- 모든 타입의 device 접근을 허용한다.

	VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
	- 오직 VkFramebuffer에 color 혹은 resolve attachment로 사용되어야만 한다.
	- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들 대해서만 유효하다.

	VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
	- 오직 VkFramebuffer에 depth/stencil attachment로 사용되어야만 한다.
	- 이 layout은 오직 활성화된 VK_IAMGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들에 대해서만 유효하다.

	VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
	- 오직 VkFramebuffer에 depth/stencil attachment를 read-only로 사용되어야만 한다. (and/or) 셰이더 내에 read-only image로 사용된다. (samgpled image로 읽을 수 있고, image/sampler 와 input attachment를 조합할 수 있다.)
	- 이 layout은 오직 활성화된 VK_IAMGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들에 대해서만 유효하다.

	VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	- 셰이더 내에 read-only image로 사용된다. (samgpled image로 읽을 수 있고, image/sampler 와 input attachment를 조합할 수 있다.)
	- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_SAMPLED_BIT나 VK_IMAGE_USAGE_INPUT_ATTACHMENT usage bit를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

	VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
	- transfer command의 source image로 사용되어야만 한다. (VK_PIPELINE_STAGE_TRANSFER_BIT 참고)
	- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_TRANSFER_SRC_BIT를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

	VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
	- transfer command의 destination image로 사용되어야만 한다.
	- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_TRANSFER_DST_BIT를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

	VK_IMAGE_LAYOUT_PREINITIALIZED
	- device 접근을 지원하지 않는다.
	- 이 layout은 반드시 initialLayout이나 image transition에서 oldLayout에서 사용되어야만 한다.
	- layout 밖으로 transition 될 때, 메모리의 내용은 보존된다.
	- 이 layout은 host에 의해 쓰여진 내용의 이미지에 대해 초기 layout으로 사용된다는 의도이다. 그리고, 이런 이유로 최초 layout transtion 실행없이 데이터는 즉시 메모리에 작성할 수 있다.
	- 현재 VK_IMAGE_LAYOUT_PREINITIALIZED는 오직 VK_IMAGE_TILTING_LINEAR 이미지에 유용하다. 왜냐하면, 이것은 VK_IMAGE_TILING_OPTIMAL 이미지로 정의된 standard layout 이 아니다.

	VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
	- 디스플레이를 위해서 Swapchain 이미지를 Present하는데 사용되어야만 한다.
	- Swapchain 이미지는 반드시 VkQueuePresentKHR이 호출되기 전에 이 layout으로 transtion되어야만 한다.
	- VkQueuePresentKHR이 호출된 후에 이 layout으로 부터 transition 되어져야만 한다. (transition away)


	VK_ACCESS_INDIRECT_COMMAND_READ_BIT
	-
	VK_ACCESS_INDEX_READ_BIT = 0x00000002,
	VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT = 0x00000004,
	VK_ACCESS_UNIFORM_READ_BIT = 0x00000008,
	VK_ACCESS_INPUT_ATTACHMENT_READ_BIT = 0x00000010,
	VK_ACCESS_SHADER_READ_BIT = 0x00000020,
	VK_ACCESS_SHADER_WRITE_BIT = 0x00000040,
	VK_ACCESS_COLOR_ATTACHMENT_READ_BIT = 0x00000080,
	VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT = 0x00000100,
	VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT = 0x00000200,
	VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT = 0x00000400,
	VK_ACCESS_TRANSFER_READ_BIT
	- access는 transfer operation로 부터 읽기를 한다.

	VK_ACCESS_TRANSFER_WRITE_BIT
	- access는 transfer operation로 부터 쓰기를 한다.

	VK_ACCESS_HOST_READ_BIT = 0x00002000,
	VK_ACCESS_HOST_WRITE_BIT = 0x00004000,
	VK_ACCESS_MEMORY_READ_BIT
	- 메모리에 attach된 명시되지 않은 unit을 읽는다.
	- 이 unit은 vulkan device 외부 이거나 코어 vulkan pipeline의 일부가 아니다.
	- dstAccessMask에 포함되어 있을 때, srcStageMask에 파이프라인 스테이지들에 의해 수행된 srcAccessMask에 access type을 사용한 모든 쓰기는 메모리에 보여져야만 한다.

	VK_ACCESS_MEMORY_WRITE_BIT = 0x00010000,
	VK_ACCESS_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF
*/
