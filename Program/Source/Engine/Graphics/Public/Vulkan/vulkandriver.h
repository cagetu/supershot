// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdriver.h"
#include "Vulkan\vulkandevice.h"

class VulkanRenderWindow;
class VulkanCommandQueue;
class VulkanCommandAllocator;
class VulkanShaderCompiler;

//------------------------------------------------------------------
/**	@class	VulkanDriver
*/
//------------------------------------------------------------------
class VulkanDriver : public IGraphicDriver
{
	__DeclareRtti;
public:
	VulkanDriver();
	virtual ~VulkanDriver();

	virtual bool Create(const WindowHandle& windowHandle) override;
	virtual void Destroy() override;

	virtual void BeginFrame(Viewport* viewport) override;
	virtual void EndFrame() override;

	virtual void Present() override;

	virtual void Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil) override;

	virtual IGraphicDevice* CurrentDevice() override { return m_Device; }
	virtual IGraphicShaderCompiler* GetShaderCompiler() override;
	virtual IGraphicShaderModule* CompileShader(const ShaderDescription& desc) override;

public:
	VkInstance Instance() const { return m_InstanceHandle; }
	VkSurfaceKHR Surface() const { return m_SurfaceHandle; }
	VulkanDevice* Device() const { return m_Device; }

private:
	bool CreateInstance();
	void DestroyInstance();

	bool CreateSurface(const WindowHandle& windowHandle);
	void DestroySurface();

	bool CreateDevice();
	void DestroyDevice();

	bool CreateRenderWindow(const WindowHandle& windowHandle);
	void DestroyRenderWindow();

	void SetViewport(const Viewport* viewport);

private:
	VkInstance m_InstanceHandle;
	VkSurfaceKHR m_SurfaceHandle;

	Array<VulkanPhysicalDevice> m_PhysicalDevices;

	VulkanDevice* m_Device;

	VulkanRenderWindow* m_RenderWindow;
	VulkanShaderCompiler* m_ShaderCompiler;
};
