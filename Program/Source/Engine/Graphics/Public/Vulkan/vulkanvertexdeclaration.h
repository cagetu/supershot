// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicvertexdeclaration.h"

class VulkanVertexDeclaration : public IGraphicVertexDeclaration
{
	__DeclareRtti;
public:
	VulkanVertexDeclaration();
	virtual ~VulkanVertexDeclaration();
};
