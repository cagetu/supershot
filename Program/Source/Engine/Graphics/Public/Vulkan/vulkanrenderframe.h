// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrenderframe.h"

class VulkanCommandList;
class VulkanFrameBuffer;
class VkRenderPassLayout;

//------------------------------------------------------------------
/**	@class	VulkanRenderFrame
	@desc	Vulkan에서 렌더링 프로세싱을 하는 단위이다.

			다음과 같은 특징을 가진다.
				1. 하나의 쓰레드에 대응한다.
				2. 하나의 CommandPool을 가질 수 있다.
				3. 다수의 CommandBuffer를 가질 수 있다.
				4. CommandPool과 CommandBuffer를 이용해서 CommandRecording을 할 수 있다.
				5. 하나의 Queue를 가질 수 있다.
				6. 레코딩 된 CommandBuffer를 Queue에 Submit할 수 있다.
				7. 별도의 RenderThread의 Queue에서 CommandBuffer를 Submit하기 위해서는 동기화 및 메시지 처리가 필요하다

			RenderFrame
				- CommandPool
					- CommandBuffer
					- CommandBuffer
				- GfxQueue (Optional)
*/
//------------------------------------------------------------------
class VulkanRenderFrame : public IGraphicRenderFrame
{
	__DeclareRtti;
public:
	VulkanRenderFrame(VulkanCommandAllocator* commandPool, VulkanCommandQueue* gfxQueue);
	virtual ~VulkanRenderFrame();

	bool Create(VulkanFrameBuffer* framebuffer);
	void Destroy();

	void Reset();

	VulkanCommandList* CreateCommandList(bool bSecondary = false);
	void DestroyCommandList(VulkanCommandList* commandBuffer);

private:
	VulkanCommandQueue* m_GfxQueue;
	Ptr<VulkanCommandAllocator> m_CommandPool;
	Ptr<VulkanFrameBuffer> m_FrameBuffer;
};
