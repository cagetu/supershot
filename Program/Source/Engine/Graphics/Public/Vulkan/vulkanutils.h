// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictypes.h"

namespace VULKAN
{
	extern void CheckResult(VkResult result, const char* func, const char* file, const int line, int level);

	extern VkFormat ConvertFormat(uint32 format);

	enum class _RESULT
	{
		_SUCCESS = 0,
		_OUT_OF_DATE = -1,
		_SURFACE_LOST = -2,
	};
};

// Macro to check and display Vulkan return results
#define VK_RESULT(func) { const VkResult res = func; if (res != VK_SUCCESS) { VULKAN::CheckResult(res, #func, __FILE__, __LINE__, 0); }}
#define VK_RESULT_EXPAND(func) { const VkResult res = func; if (res < VK_SUCCESS) { VULKAN::CheckResult(res, #func, __FILE__, __LINE__, 0); }}
#define VK_RESULT_ASSERT(func) { const VkResult res = func;	if (res != VK_SUCCESS) { VULKAN::CheckResult(res, #func, __FILE__, __LINE__, 1); }}
