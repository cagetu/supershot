// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshadermodule.h"

//------------------------------------------------------------------
/**	@class	VulkanShaderModule
	@desc	SpirV 셰이더 코드에 대한 정보를 가진다.
			Spirv 셰이더에 대한 Inspector 정보를 가진다. (Descriptor 정보 등등)

			DescriptorSet
				- DescriptorBinding
				- DescriptorBinding
				- DescrpitorBinding
*/
//------------------------------------------------------------------
class VulkanShaderModule : public IGraphicShaderModule
{
	__DeclareRtti;
public:
	VulkanShaderModule();
	virtual ~VulkanShaderModule();

	virtual bool Create(ShaderType::TYPE type, uint32 elementSize, const void* elementData) override;
	virtual void Destroy() override;

	inline VkShaderModule Handle() const { return m_ShaderModule; }

public:
	struct Attribute
	{
		utf8::string name;
		utf8::string sementic;
		SizeT location = 0;

		bool operator==(const Attribute& rhs) const
		{
			if (location != rhs.location)
				return false;
			if (name != rhs.name)
				return false;
			if (sementic != rhs.sementic)
				return false;
			return true;
		}
	};

	struct Descriptor
	{
		utf8::string name;
		SizeT descriptorSet = 0;
		SizeT descriptorBinding = 0;

		struct MemberType
		{
			enum BaseType
			{
				Unknown,
				Void,
				Boolean,
				Char,
				Int,
				UInt,
				Int64,
				UInt64,
				AtomicCounter,
				Double,
				Float,
				Vector2,
				Vector3,
				Vector4,
				Matrix3,
				Matrix4,
				Struct,
				Image,
				SampledImage,
				Sampler
			};
			utf8::string name;
			SizeT offset = 0;
			SizeT size = 0;
			BaseType baseType = Unknown;
			SizeT vecSize = 1;
			SizeT columns = 1;
			SizeT arraySize = 0;

			bool operator==(const MemberType& rhs) const
			{
				if (name != rhs.name)
					return false;
				if (offset != rhs.offset)
					return false;
				if (size != rhs.size)
					return false;
				if (baseType != rhs.baseType)
					return false;
				if (vecSize != rhs.vecSize)
					return false;
				if (columns != rhs.columns)
					return false;
				if (arraySize != rhs.arraySize)
					return false;

				return true;
			}
		};
	};

	struct UniformBuffer : public Descriptor
	{
		SizeT size = 0;
		Array<MemberType> members;
	};

	struct PushConstant : public Descriptor
	{
		struct Range
		{
			unsigned index = 0;
			size_t offset = 0;
			size_t range = 0;

			bool operator==(const Range& rhs) const
			{
				if (index != rhs.index)
					return false;
				if (offset != rhs.offset)
					return false;
				if (range != rhs.range)
					return false;
				return true;
			}
		};

		Array<Range> bufferRanges;
	};

	struct SubpassInput : public Descriptor
	{
		SizeT input_attachment = 0;
	};

	inline const char* GetEntryName() const { return m_EntryPoint.c_str(); }
	inline const Array<Attribute>& GetInputs() const { return m_Inputs; }
	inline const Array<Attribute>& GetOuputs() const { return m_Outputs; }
	inline const Array<UniformBuffer>& GetImages() const { return m_Images; }
	inline const Array<UniformBuffer>& GetSamplers() const { return m_Samplers; }
	inline const Array<UniformBuffer>& GetSampledImages() const { return m_SampledImages; }
	inline const Array<UniformBuffer>& GetUniformBuffers() const { return m_UniformBuffers; }
	inline const Array<PushConstant>& GetPushConstants() const { return m_PushConstants; }
	inline const Array<SubpassInput>& GetSubpassInputs() const { return m_SubpassInputs; }

	inline uint32 NumDescriptors() const { return m_NumDescriptors; }
	inline uint32 NumUniformBuffers() const { return m_UniformBuffers.Num(); }
	inline uint32 NumSamplers() const { return m_Samplers.Num(); }
	inline uint32 NumImages() const { return m_NumImages; }

private:
	uint32 m_NumDescriptors;
	uint32 m_NumImages;

	utf8::string m_EntryPoint;
	Array<Attribute> m_Inputs;
	Array<Attribute> m_Outputs;
	Array<UniformBuffer> m_Images;
	Array<UniformBuffer> m_Samplers;
	Array<UniformBuffer> m_SampledImages;
	Array<UniformBuffer> m_UniformBuffers;
	Array<PushConstant> m_PushConstants;
	Array<SubpassInput> m_SubpassInputs;

private:
	/// vulkan pipeline에 바인딩하기 위한 Shader Code Wrapping Handle
	VkShaderModule m_ShaderModule;
};
