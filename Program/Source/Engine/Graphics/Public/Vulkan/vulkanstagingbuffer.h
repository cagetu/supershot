// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "vulkanresource.h"

//------------------------------------------------------------------
/**	@class	VulkanStagingBuffer
	@desc	StagingBuffer
*/
//------------------------------------------------------------------
class VulkanStagingBuffer : public VulkanResource
{
	__DeclareRtti;
public:
	VulkanStagingBuffer();
	virtual ~VulkanStagingBuffer();
};
