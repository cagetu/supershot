// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandlist.h"

class VulkanCommandAllocator;
class VulkanFrameBuffer;
class VulkanFence;
class VulkanSemaphore;
class VulkanCommandQueue;

//------------------------------------------------------------------
/**	@class	VulkanCommandList
	@desc	CommandPool에 의해 생성된 CommandBuffer
			CommandPool과 같은 쓰레드에서 레코딩 된다.
*/
//------------------------------------------------------------------
class VulkanCommandList : public IGraphicCommandList
{
	__DeclareRtti;
public:
	struct UsageFlags
	{
		typedef int8 Type;
		enum
		{
			_ONETIME_SUBMIT = 1 << 0,			// 한번만 SUBMIT 된 후, CommandBuffer가 Reset 된다.
			_RENDER_PASS_CONTINUE = 1 << 1,		// Secondary Command Buffer에 사용한다
			_SIMULTANEOUS_USE_BIT = 1 << 2,		// 한번 Recording 된 Primary Command Buffer 를 resubmit 가능하다.
		};
	};
	const static int32 BEGIN_ONETIME_SUBMIT = 0;

public:
	VulkanCommandList(VulkanCommandAllocator* commandPool, bool bSecondary = false);
	virtual ~VulkanCommandList();

	inline const VkCommandBuffer Handle() const { return m_BufferHandle; }

	inline bool IsSecondary() const { return m_bSecondary; }

public:
	// CommandQueue에 Submit 한다.
	void Submit(VulkanCommandQueue* gfxQueue,
                VulkanSemaphore* signalSemaphore = nullptr,
                VulkanSemaphore* waitSemaphore = nullptr,
                VkPipelineStageFlags waitStageMask = 0,
                bool bWaitIdle = false);
	void Reset();

	//~Begin Sync
	void WaitForIdle();

	void RefreshFenceStatus();

	VulkanFence* GetFence() const { return m_Fence; }
	uint64 GetFenceSignaledCounter() const { return m_FenceSignaledCounter; }
	//~End Sync

	//~Begin Record
public:
	void BeginRecord(UsageFlags::Type usageFlags = UsageFlags::_ONETIME_SUBMIT);
	void EndRecord();

	void ExcuteCommands(uint32 count, Array<VulkanCommandList*>& secondaryBuffers);

	void BeginRenderPass(VulkanFrameBuffer* framebuffer);
	void EndRenderPass();

	// ImageLayout Transition
	void SetImageBarrier(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout);
	void SetImageBarrier(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, const VkImageSubresourceRange& subresourceRange);
	void SetImageBarrier(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, VkAccessFlags srcAccessMask, VkAccessFlags dstAccessMask,
		VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, const VkImageSubresourceRange& subresourceRange);

	void ClearColorImage(VkImage imageHandle, VkImageLayout imageLayout, const VkClearColorValue& clearValue);
	void ClearDepthStencilImage(VkImage imageHandle, VkImageLayout imageLayout, const VkClearDepthStencilValue& clearValue);
	void ClearResolveImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageResolve* pRegions);
	void ClearAttachments(uint32 attachmentCount, const VkClearAttachment* pAttachments, uint32 rectCount, const VkClearRect* pRects);

	void SetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);
	void SetDepthBounds(float minDepthBounds, float maxDepthBounds);
	void SetBlendConstants(float blendConstants[4]);
	void SetStencilCompareMask(uint32 faceMask, uint32 compareMask);
	void SetStencilWriteMask(uint32 faceMask, uint32 writeMask);
	void SetStencilReference(uint32 faceMask, uint32 reference);

	void SetViewport(float width, float height, float posX, float posY, float minDepth, float maxDepth);
	void SetScissorRect(uint32 width, uint32 height, int32 posX, int32 posY);
	//~End Record

private:
	VkCommandBuffer m_BufferHandle;

	VulkanCommandAllocator* m_CommandPool;

	UsageFlags::Type m_UsageFlags;
	bool m_bSecondary;
	bool m_Recoreded;

	VulkanFence* m_Fence;
	volatile uint64 m_FenceSignaledCounter;
};
