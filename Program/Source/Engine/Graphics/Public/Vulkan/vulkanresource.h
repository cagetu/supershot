﻿// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"
#include "Vulkan/vulkanmemory.h"

//------------------------------------------------------------------
/**	@class	VulkanResource
	@desc	Vulkan에서 그래픽 리소스로 사용되는 것의 최상위 객체
			VulkanResource
				+ VulkanImage
				+ VulkanBuffer
*/
//------------------------------------------------------------------
class VulkanResource : public IGraphicResource
{
	__DeclareRtti;
public:
	VulkanResource();
	virtual ~VulkanResource();
};
