// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	@class	DescriptorSetLayout
	@desc	GPU Resource를 렌더링파이프라인에 Bind하는데 사용된다.
			자원을 GPU에게 서술해주는 경량의 자료구조라고 할 있다.
			
			* DescriptorSet에 VkDescriptorBinding 하기
			-> DescriptorSet을 만든다.			

			* PipelineLayout에 DescriptorSetLayouts 목록을 넘겨줘야 하기 때문에,
			전체 DescriptorSet을 관리한다.
*/
//------------------------------------------------------------------
class VulkanDescriptorSetsLayout
{
	__DeclareReference(VulkanDescriptorSetsLayout);
public:
	VulkanDescriptorSetsLayout();
	~VulkanDescriptorSetsLayout();

	// Add Descriptor
	void AddDescriptor(uint32 layoutSetID, const VkDescriptorSetLayoutBinding& descriptor);
	void Clear();

	void Submit();

	inline const Array<VkDescriptorSetLayout>& GetHandles() const { return m_DescriptorSetLayoutHandles; }
	inline const VkDescriptorSetLayout GetHandle(uint32 index) const { return m_DescriptorSetLayoutHandles[index]; }

	inline SizeT NumDescriptorSets() const { return m_DescriptorSetLayoutHandles.Num(); }
	inline SizeT NumUsedType(VkDescriptorType type) const { return m_NumDescriptorsPerType[type]; }

private:
	///	Descriptor Set는 하나의 그룹으로 파이프라인에 연결하는 셰이더 리소스의 세트이다.
	struct DescriptorSetLayout
	{
		/// DescriptorSetLayoutBinding은 UniformBuffer 하나와 1:1로 대응한다.
		Array<VkDescriptorSetLayoutBinding> layoutBings;
		uint32 hash;

		void GenerateHash();

		friend uint32 GetTypeHash(const DescriptorSetLayout& In)
		{
			return In.hash;
		}

		inline bool operator == (const DescriptorSetLayout& In) const
		{
			if (In.hash != hash)
			{
				return false;
			}

			const int32 NumBindings = layoutBings.Num();
			if (In.layoutBings.Num() != NumBindings)
			{
				return false;
			}

			if (NumBindings != 0 && Memory::MemCmp(In.layoutBings.Data(), layoutBings.Data(), NumBindings * sizeof(VkDescriptorSetLayoutBinding)) != 0)
			{
				return false;
			}

			return true;
		}

		inline bool operator != (const DescriptorSetLayout& In) const
		{
			return !(*this == In);
		}

	};

	/// DescriptorSetLayout 핸들
	Array<VkDescriptorSetLayout> m_DescriptorSetLayoutHandles;
	/// DescriptorSetLayoutBinding 들...
	Array<DescriptorSetLayout> m_SetLayouts;

	uint32 m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_RANGE_SIZE];
};

//---------------------------------------------------------------------------------------------------------------------------------
/**	@class	Descriptor Sets 클래스
	@desc	- DescriptorSet은 Descriptor Pool에 의해서 생성된다.
			- DescriptorPool이 없어질 때, 소멸된다.
			- 정의된 DescriptorSetLayout으로 Descriptor Set을 생성한다.
			- Descriptor Set이 사용되지 않을 때 리소스는 업데이트 된다.
			
			* 실제로 사용하는 클래스는 DescriptorSetLayout과 DescriptorSet 이니 DescriptorPool 은 숨겨도 될 듯.

	[2017-02-21]
		DescriptorSet들에 대한 모든 Layout을 하나의 DescriptorSets로 생성하는 것이 맞는지 아니면, DescriptorSet 하나씩으로 생성해서 관리하는 것이 맞는지 모르겠다.
*/
//---------------------------------------------------------------------------------------------------------------------------------
class VulkanDescriptorSets
{
public:
	VulkanDescriptorSets(IndexT poolID, VulkanDescriptorSetsLayout* layout);
	~VulkanDescriptorSets();

	inline const VkDescriptorSet GetHandle(int32 index) const { return m_DescriptorSetHandles[index]; }
	inline const Array<VkDescriptorSet>& GetHandles() const { return m_DescriptorSetHandles; }
	inline VulkanDescriptorSetsLayout* GetLayout() const { return m_Layout; }
	inline IndexT GetPoolID() const { return m_PoolID; }

private:
	friend class VulkanDescriptorPool;

	/// 생성 결과
	Array<VkDescriptorSet> m_DescriptorSetHandles;
	/// DescriptorSetLayout
	VulkanDescriptorSetsLayout* m_Layout;
	/// 생성해준 Pool 객체
	IndexT m_PoolID;
};
