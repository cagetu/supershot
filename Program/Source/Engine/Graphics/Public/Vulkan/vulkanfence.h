// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	@class	VulkanFence
	@desc	CPU와 GPU의 동기화를 위해서 사용된다.
			
			CPU에서 작성된 CommandList가 CommandQueue에 제출될 때, GPU에서 해당 CommandList가 처리가 끝났을 때까지 CPU를 대기 시킨다.

			디바이스 상에서 실행되는 어떤 태스크가 완료되었음을 호스트에게 알려주기 위해서 사용될 수 있다.
	
			(Queue엣 Submit된)CommandBuffer의 실행이 완료되었는지를 알려주는 신호 메커니즘

			VK_RESULT(::vkQueueSubmit(m_QueueHandle, 1, &submitInfo, fence->Handle()));
			- (fence가 NULL이 아니고, submitCount가 0이 아니면), pCommandBuffers에 정의된 모든 커맨드 실행이 끝날 때 fence는 종료를 알리는 신호를 받게 된다.
			- (fence가 NULL이 아니고, submitCount가 0이면), fence 신호는 이전의 큐에 제출한 모든 작업이 완료되었음을 의미한다.
*/
//------------------------------------------------------------------
class VulkanFence
{
public:
	VulkanFence(bool bIsSignaled);
	~VulkanFence();

	void Reset();
	bool GetStatus();
	bool WaitForSignal(uint64 timeout = UINT64_MAX);

	inline VkFence Handle() const { return m_FenceHandle; }
	inline const VkFence* HandlePtr() const { return &m_FenceHandle; }
	inline bool IsSignaled() const { return m_IsSignaled; }

private:
	friend class VulkanFenceManager;

	VkFence m_FenceHandle;

	bool m_IsSignaled;
};

//------------------------------------------------------------------
/**	@class	VulkanFenceManager
	@desc	Fence를 재활용해서 사용하기 위한 Fence Pool
*/
//------------------------------------------------------------------
class VulkanFenceManager
{
public:
	VulkanFenceManager();
	~VulkanFenceManager();

	void Create();
	void Destroy();

	VulkanFence* AllocateFence(bool bCreateSignaled = false);
	void ReleaseFence(VulkanFence*& fence);

	bool CheckFenceState(VulkanFence* fence);
	void ResetFence(VulkanFence* fence);

	bool WaitForFence(VulkanFence* fence, uint64 timeout);
	void WaitAndReleaseFence(VulkanFence*& fence, uint64 timeout);

	bool IsSignaled(VulkanFence* fence);

private:
	Array<VulkanFence*> m_FreeFences;
	Array<VulkanFence*> m_UsedFences;
};
