// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictexture.h"

//------------------------------------------------------------------
/**	@class	VulkanSampler
	@desc	Texture Sampler
			- Vulkan Texture는 Sampler에 의해서만 접근될 수 있다.
			- 같은 텍스쳐 데이터 이지만, 다른 Sampling 정보가 필요하면 다른 Sampler를 만들어야 한다.
*/
//------------------------------------------------------------------
class VulkanSampler
{
public:
	VulkanSampler();
	VulkanSampler(VkFilter minFilter, VkFilter magFilter,
				VkSamplerAddressMode addressU, VkSamplerAddressMode addressV, VkSamplerAddressMode addressW,
				int32 maxLodLevel, int32 minLodLevel,
				float mipLodBias = 0.0f, VkSamplerMipmapMode mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
				VkBorderColor borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
				bool anisotropyEnable = false,
				bool compareEnable = false, VkCompareOp compareOp = VK_COMPARE_OP_NEVER);
	~VulkanSampler();

	bool Create(VkFilter minFilter, VkFilter magFilter, 
				VkSamplerAddressMode addressU, VkSamplerAddressMode addressV, VkSamplerAddressMode addressW,
				int32 maxLodLevel, int32 minLodLevel, 
				float mipLodBias = 0.0f, VkSamplerMipmapMode mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
				VkBorderColor borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE, 
				bool anisotropyEnable = false,
				bool compareEnable = false, VkCompareOp compareOp = VK_COMPARE_OP_NEVER);
	void Destroy();

	VkSampler Handle() const { return m_SamplerHandle; }

private:
	VkSampler m_SamplerHandle;
};
