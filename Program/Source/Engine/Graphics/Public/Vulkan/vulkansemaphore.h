// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class VulkanDevice;

//------------------------------------------------------------------
/**	@class	VulkanSemaphore
	@desc	여러 개의 Queue 사이의 리소스에 대한 접근을 제어하기 위해서 사용된다.
	
			Queue 내부 또는 Queue 간의 Command 동기화를 처리
			- 세마포어는 Queue 동기화를 위해서 사용된다.
			- 하나의 Queue는 어떤 처리가 끝났을 때 Semaphore 하나를 Signal한다.
			- 다른 Queue는 Semaphore가 Signal될 때까지 Semaphore에서 대기한다.
			- 그리고 나서, Queue는 Command Buffer들을 통해서 Submit된 처리를 수행할 것이다.
*/
//------------------------------------------------------------------
class VulkanSemaphore
{
public:
	VulkanSemaphore();
	~VulkanSemaphore();

	VkSemaphore Handle() const { return m_SemaphoreHandle; }
	const VkSemaphore* HandlePtr() const { return &m_SemaphoreHandle; }

private:
	VkSemaphore m_SemaphoreHandle;
};
