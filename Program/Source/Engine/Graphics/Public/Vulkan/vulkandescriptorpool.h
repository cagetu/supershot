// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class VulkanDescriptorSetsLayout;
class VulkanDescriptorSets;

//---------------------------------------------------------------------------------------------------------------------------------
/**	@class	DescriptorPool
	@desc	- DescriptorSet을 생성한다.
			- Pool이 최대한 할당할 수 있는 DescriptionSets의 수를 체크한다.
			- Device에서 DescriptorPool을 관리한다.

			- DescriptorPool은 여러 쓰레드에서 동시에 접근해서는 안된다.

			- Pool이 삭제되면 이 Pool에서 생성된 DescriptorSet들은 어떻게 관리가 되는거지???

			- 다중 Pool과 관리 방법 공부해보자!!
*/		
//---------------------------------------------------------------------------------------------------------------------------------
class VulkanDescriptorPool
{
public:
	VulkanDescriptorPool(VulkanDevice* device);
	~VulkanDescriptorPool();

	inline VkDescriptorPool Handle() const { return m_PoolHandle; }
	inline IndexT ID() const { return m_PoolID; }

	bool CanAllocate(const VulkanDescriptorSetsLayout& Layout) const;
	bool IsEmpty() const { return m_NumAllocatedDescriptorSets == 0 ? true : false; }

	VulkanDescriptorSets* CreateDescriptorSet(VulkanDescriptorSetsLayout* layout);
	void DestroyDescriptorSet(VulkanDescriptorSets* descriptorSets);

	void Reset();

private:
	void TrackAddUsage(const VulkanDescriptorSetsLayout& Layout);
	void TrackRemoveUsage(const VulkanDescriptorSetsLayout& Layout);

	uint32 m_MaxDescriptorSets;
	uint32 m_NumAllocatedDescriptorSets;
	uint32 m_PeakAllocatedDescriptorSets;

	// Tracks number of allocated types, to ensure that we are not exceeding our allocated limit
	int32 m_MaxAllocatedPerType[VK_DESCRIPTOR_TYPE_RANGE_SIZE];
	int32 m_NumAllocatedPerType[VK_DESCRIPTOR_TYPE_RANGE_SIZE];
	int32 m_PeakAllocatedPerType[VK_DESCRIPTOR_TYPE_RANGE_SIZE];

	Array<VulkanDescriptorSets*> m_DescriptorSets;

	IndexT m_PoolID;
	VkDescriptorPool m_PoolHandle;
};