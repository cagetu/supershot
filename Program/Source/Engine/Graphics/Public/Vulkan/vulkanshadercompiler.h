// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshadercompiler.h"

//------------------------------------------------------------------
/**	@class	VulkanShaderCompiler
	@desc	Vulkan 셰이더 컴파일러

			입력된 셰이더 코드를 Spir-V 셰이더로 컴파일한다.
*/
//------------------------------------------------------------------
class VulkanShaderCompiler : public IGraphicShaderCompiler
{
    __DeclareRtti;
public:
	VulkanShaderCompiler();
	virtual ~VulkanShaderCompiler();

	virtual IGraphicShaderModule* Compile(const ShaderDescription& desc) override;

private:
};