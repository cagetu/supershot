// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictexture.h"
#include "Vulkan/vulkanimage.h"
#include "color.h"

//------------------------------------------------------------------
/**	@class	VulkanTexture
	@desc	Sampler
*/
//------------------------------------------------------------------
class VulkanTexture : public IGraphicTexture
{
	__DeclareRtti;
public:
	VulkanTexture();
	virtual ~VulkanTexture();

	VulkanImage* Image() const { return m_Image; }
	VulkanImageView* ImageView() const { return m_ImageView; }

	VkImage ImageHandle() const { return (m_Image == nullptr) ? VK_NULL_HANDLE : m_Image->Handle(); }
	VkImageView ImagViewHandle() const { return (m_ImageView == nullptr) ? VK_NULL_HANDLE : m_ImageView->Handle(); }

protected:
	VulkanImage* m_Image;
	VulkanImageView* m_ImageView;
};

//------------------------------------------------------------------
/**	@class	VulkanTexture2D
	@desc	Sampler
*/
//------------------------------------------------------------------
class VulkanTexture2D : public VulkanTexture
{
};

//------------------------------------------------------------------
/**	@class	Vulkan RenderTarget Texture
	@desc	렌더링 대상이 되는 텍스쳐이다.
*/
//------------------------------------------------------------------
class VulkanRenderTexture : public VulkanTexture
{
	__DeclareRtti;
public:
	VulkanRenderTexture();
	virtual ~VulkanRenderTexture();

	virtual bool Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) = 0;
	virtual bool Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) = 0;
	virtual bool Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1);
	void Destroy();

	Vk::ImageLayoutOp& ImageLayout() { return m_ImageLayoutOp; }

	void SetName(const String& name) { m_Name = name; }
	const String& GetName() const { return m_Name; }

	int32 Multisamples() const { return m_Multisamples; }

protected:
	String m_Name;

	Vk::ImageLayoutOp m_ImageLayoutOp;

	int32 m_Multisamples;
	bool m_bCreated;
};

//------------------------------------------------------------------
/**	@class	VulkanColorRenderTexture
	@desc	Surface
*/
//------------------------------------------------------------------
class VulkanColorRenderTexture : public VulkanRenderTexture
{
	__DeclareRtti;
public:
	VulkanColorRenderTexture();
	virtual ~VulkanColorRenderTexture();

	virtual bool Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;
	virtual bool Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;
	virtual bool Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;

	Vk::AttachmentOp& ColorLoadStore() { return m_LoadStoreOp; }

	LinearColor& ClearColor() { return m_ClearColor; }

protected:
	Vk::AttachmentOp m_LoadStoreOp;

	LinearColor m_ClearColor;
};

//------------------------------------------------------------------
/**	@class	VulkanDepthStencilRenderTexture
	@desc	Surface
*/
//------------------------------------------------------------------
class VulkanDepthStencilRenderTexture : public VulkanRenderTexture
{
	__DeclareRtti;
public:
	VulkanDepthStencilRenderTexture();
	virtual ~VulkanDepthStencilRenderTexture();

	virtual bool Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;
	virtual bool Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;
	virtual bool Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;

	Vk::AttachmentOp& DepthLoadStore() { return m_DepthLoadStoreOp; }
	Vk::AttachmentOp& StencilLoadStore() { return m_StencilLoadStoreOp; }

	float& ClearDepth() { return m_ClearDepth; }
	int32& ClearStencil() { return m_ClearStencil; }

private:
	Vk::AttachmentOp m_DepthLoadStoreOp;
	Vk::AttachmentOp m_StencilLoadStoreOp;

	float m_ClearDepth;
	int32 m_ClearStencil;
};

//------------------------------------------------------------------
/**	@class	ResolveTexture
	@desc	Surface
*/
//------------------------------------------------------------------
class VulkanResolveRenderTexture : public VulkanRenderTexture
{
	__DeclareRtti;
public:
	VulkanResolveRenderTexture();
	virtual ~VulkanResolveRenderTexture();

	virtual bool Create(int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;
	virtual bool Create(VkImage imageHandle, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;
	virtual bool Create(VulkanImage* image, VulkanImageView* imageView, int32 width, int32 height, FORMAT_TYPE format, int32 mipMaps = 1, int32 multiSamples = 1) override;

	Vk::AttachmentOp& ResolveLoadStore() { return m_ResolveLoadStoreOp; }

private:
	Vk::AttachmentOp m_ResolveLoadStoreOp;
};
