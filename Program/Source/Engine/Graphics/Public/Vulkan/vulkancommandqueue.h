// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandqueue.h"

class VulkanCommandList;
class VulkanSwapchain;
class VulkanSemaphore;
class VulkanFence;
class VulkanDevice;

//------------------------------------------------------------------
/**	@class	VulkanCommandQueue
	@desc	VulkanQueueFamily에 의해 생성된 정해진 기능을 하는 Queue이다.
			CommandList를 Submit할 수 있다.
			다수의 Queue가 존재할 수 있다. Queue 간의 CommandList 동기화 처리를 해줘야 한다.

			- 다수의 쓰레드가 하나의 Queue (혹은 여러 Queue)에 작업을 submit 할 수 있다.
			- Queue는 CommandBuffer들 submissions으로 GPU 작업을 승락한다.
				- submit work / wait idle
			- Queue submission은 queue에 대해 sync primitive를 포함할 수 있다.
				- submit된 작업을 처리하기 전에 대기
				- 이 submission에서 작업을 완료했을 때 signal
			- Queue "Family"는 다른 타입의 작업을 수락할 수 있다.
*/
//------------------------------------------------------------------
class VulkanCommandQueue : public IGraphicCommandQueue
{
	__DeclareRtti;
public:
	VulkanCommandQueue(VulkanDevice* device, uint32 familyIndex, uint32 queueIndex, COMMANDLIST_TYPE queueType);
	virtual ~VulkanCommandQueue();

	void Submit(VulkanCommandList* commandBuffer, 
				VulkanFence* fence,							/// 커멘드 버퍼의 실행이 완료되었는지
				VulkanSemaphore* signalSemaphore = nullptr,	/// 실행이 완료되면 보내지는 시그널 세마포어
				VulkanSemaphore* waitSemaphore = nullptr,	/// 실행하기 전에 신호가 오기를 대기하는 세마포어
				VkPipelineStageFlags waitStageMask = 0,	    /// 세마포어 Wait가 발생에 대응하는 스테이지
                bool bWaitIdle = false);                    /// 완료 후 Idle이 될 때까지 기다릴 것인가?

	void WaitForIdle();

public:
	inline VkQueue Handle() const { return m_QueueHandle; }
	inline uint32 FamilyIndex() const { return m_FamilyIndex; }
	inline uint32 QueueIndex() const { return m_QueueIndex; }

private:
	uint32 m_FamilyIndex;	// QueueFamilyIndex
	uint32 m_QueueIndex;	// QueueIndex

	VkQueue m_QueueHandle;
};

//------------------------------------------------------------------
/**	@class	QueueFamily
	@desc	Vulkan은 Queue의 종류에 따라서 그룹을 만든다.
			QueueFamily는 그 그룹에 대한 클래스이다.
			다중 큐를 사용하게 될 경우, QueueFamily가 생성 가능한 Queue르 만들어준다.
*/
//------------------------------------------------------------------
class VulkanQueueFamily
{
public:
	VulkanQueueFamily(uint32 familyIndex, uint32 queueCount, COMMANDLIST_TYPE queueType);
	~VulkanQueueFamily();

	VulkanCommandQueue* CreateCommandQueue(VulkanDevice* device);
	void DestroyCommandQueue(VulkanCommandQueue* queue);
	void ClearCommandQueues();

public:
	inline uint32 FamilyIndex() const { return m_FamilyIndex; }
	inline uint32 QueueCount() const { return m_QueueCount; }
	inline COMMANDLIST_TYPE QueueType() const { return m_QueueType; }

private:
	uint32 m_FamilyIndex;
	uint32 m_QueueCount;
	COMMANDLIST_TYPE m_QueueType;

	Array<VulkanCommandQueue*> m_CommandQueues;
	Array<int32> m_Usages;
};
