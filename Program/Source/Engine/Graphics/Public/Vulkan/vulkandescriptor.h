// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include  "graphicdescriptor.h"

//------------------------------------------------------------------
/**	@class	Vulkan Descriptor
	@desc	GPU Resource를 렌더링파이프라인에 Bind하는데 사용된다.
			자원을 GPU에게 서술해주는 경량의 자료구조라고 할 있다.
*/
//------------------------------------------------------------------
class VulkanDescriptor : public IGraphicDescriptor
{
	__DeclareRtti;
public:
	VulkanDescriptor();
	virtual ~VulkanDescriptor();
};
