// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "vulkanrenderpasslayout.h"

//------------------------------------------------------------------
/**	@class	RenderPass
	@desc	- 각 패스는 장면의 다른 부분을 생성하거나, 포스트 프로세싱의 전체 프레임 효과, 사용자 인터페이스 렌더링 등의 한 렌더링 패스를 정의한다.
			- 각 패스는 여러 개의 SubPass로 구성된다.
			- 현재, 한 개의 렌더링 패스는 단일 출력 이미지를 가지도록 하나의 서브 패스를 가진다.
*/
//------------------------------------------------------------------
class VulkanRenderPass
{
	__DeclareReference(VulkanRenderPass)
public:
	VulkanRenderPass();
	~VulkanRenderPass();

	bool Create(const Vk::RenderPassLayout& renderpassLayout);
	void Destroy();

public:
	VkRenderPass Handle() const { return m_RenderPassHandle; }

	int32 Width() const { return m_Width; }
	int32 Height() const { return m_Height; }

	const Array<VkClearValue>& ClearValues() const { return m_ClearValues; }
	/// 현재 렌더패스에서 사용되는 전체 Attachment의 이미지 Views
	const Array<VkImageView>& AttachmentViews() const { return m_AttachmentViews; }

	//const Array<VkAttachmentDescription>& Attachments() const { return m_AttachmentDecs; }

	/// 서브 패스 
	//const Array<VkSubpassDescription>& SubPasses() const { return m_SubPassDescs; }
	//const Array<VkSubPassAttachment>& SubPassAttachments() const { return m_SubPassAttachments; }

private:
	/// 렌더 패스 핸들
	VkRenderPass m_RenderPassHandle;

	int32 m_Width;
	int32 m_Height;

	/// 렌더 패스에 할당되는 Attachment 정의 목록 (3개 모두 순서가 맞아야 한다)
	Array<VkAttachmentDescription> m_AttachmentDecs;
	Array<VkImageView> m_AttachmentViews;
	Array<VkClearValue> m_ClearValues;

	/// 서브패스 정의 목록
	Array<VkSubpassDescription> m_SubPassDescs;
	/// 서브패스에서 사용되는 Attachment 정보
	Array<Vk::SubPassAttachmentReference> m_SubPassAttachmentReferences;
};
