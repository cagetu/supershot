// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#if TARGET_OS_WINDOWS

//#define NOMINMAX
//#define WIN32_LEAN_AND_MEAN
//#include <Windows.h>
//#define VK_USE_PLATFORM_WIN32_KHR

// Uncomment to test including `vulkan.h` on your own before including VMA.
//#include <vulkan/vulkan.h>

/*
In every place where you want to use Vulkan Memory Allocator, define appropriate
macros if you want to configure the library and then include its header to
include all public interface declarations. Example:
*/

//#define VMA_HEAVY_ASSERT(expr) assert(expr)
//#define VMA_USE_STL_CONTAINERS 1
//#define VMA_DEDICATED_ALLOCATION 0
//#define VMA_DEBUG_MARGIN 16
//#define VMA_DEBUG_DETECT_CORRUPTION 1
//#define VMA_DEBUG_INITIALIZE_ALLOCATIONS 1
//#define VMA_RECORDING_ENABLED 0
//#define VMA_DEBUG_MIN_BUFFER_IMAGE_GRANULARITY 256
//#define VMA_USE_STL_SHARED_MUTEX 0
//#define VMA_DEBUG_GLOBAL_MUTEX 1
/*
#define VMA_DEBUG_LOG(format, ...) do { \
		printf(format, __VA_ARGS__); \
		printf("\n"); \
	} while(false)
*/

#pragma warning(push, 4)
#pragma warning(disable: 4127) // conditional expression is constant
#pragma warning(disable: 4100) // unreferenced formal parameter
#pragma warning(disable: 4189) // local variable is initialized but not referenced

#include "vk_mem_alloc.h"

#pragma warning(pop)

#else // #ifdef _WIN32

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wtautological-compare" // comparison of unsigned expression < 0 is always false
#endif

//#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#endif // #ifdef _WIN32

enum VulkanMemoryUsage 
{
	VK_MEMORY_USAGE_UNKNOWN,
	VK_MEMORY_USAGE_GPU_ONLY,
	VK_MEMORY_USAGE_CPU_ONLY,
	VK_MEMORY_USAGE_CPU_TO_GPU,
	VK_MEMORY_USAGE_GPU_TO_CPU,
	VK_MEMORY_USAGES,
};

enum VulkanAllocationType
{
	VK_ALLOCATION_TYPE_FREE,
	VK_ALLOCATION_TYPE_BUFFER,
	VK_ALLOCATION_TYPE_IMAGE,
	VK_ALLOCATION_TYPE_IMAGE_LINEAR,
	VK_ALLOCATION_TYPE_IMAGE_OPTIMAL,
	VK_ALLOCATION_TYPES,
};

class VulkanDevice;

//------------------------------------------------------------------
/**	@class	VulkanMemoryAllocator
	@desc	Vulkan 메모리 할당자.
			(vma를 기반으로 한다.)
*/
//------------------------------------------------------------------
class VulkanMemoryAllocator
{
public:
	VulkanMemoryAllocator(VulkanDevice* device);
	~VulkanMemoryAllocator();

	const VmaAllocator& Handle() const { return m_Allocator; }

	bool CreateImage(VkImage* imageHandle, const VkImageCreateInfo* pCreateInfo, const VmaAllocationCreateInfo* allocCreateInfo, VmaAllocation* pAllocation, VmaAllocationInfo* pAllocationInfo);
	void DestroyImage(VkImage imageHandle, VmaAllocation pAllocation);

	bool CreateBuffer(VkBuffer* bufferHandle, const VkBufferCreateInfo* pCreateInfo, const VmaAllocationCreateInfo* allocCreateInfo, VmaAllocation* pAllocation, VmaAllocationInfo* pAllocationInfo);
	void DestroyBuffer(VkBuffer bufferHandle, VmaAllocation pAllocation);

private:
	VmaAllocator m_Allocator;
};
