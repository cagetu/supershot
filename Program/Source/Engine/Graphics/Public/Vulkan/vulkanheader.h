// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "vulkanconfig.h"

/** How many back buffers to cycle through */
#define NUM_RENDER_BUFFERS 2

#ifndef VK_PROTOTYPES
#define VK_PROTOTYPES	1
#endif

#if TARGET_OS_WINDOWS
#define VK_USE_PLATFORM_WIN32_KHR 1
#endif
#if TARGET_OS_ANDROID
#define VK_USE_TARGET_OS_ANDROID_KHR 1
#endif

#pragma warning(disable: 4819) // 949

#include "vulkanloader.h"	// 최상위!!
#include "vulkanlayers.h"
#include "vulkantypes.h"
#include "vulkanutils.h"
//#include "vulkanmemory.h"

class VulkanDevice;

extern VulkanDevice* GVulkanDevice;
