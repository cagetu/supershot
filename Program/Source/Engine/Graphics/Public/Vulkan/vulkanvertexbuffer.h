// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicvertexbuffer.h"

class VulkanBuffer;

//------------------------------------------------------------------
/**	@class	VertexBuffer
	@desc	버텍스 버퍼 객체
*/
//------------------------------------------------------------------
class VulkanVertexBuffer : public IGraphicVertexBuffer
{
	__DeclareRtti;
public:
	VulkanVertexBuffer();
	virtual ~VulkanVertexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;

private:
	VulkanBuffer* m_Buffer;
	VulkanBufferView* m_BufferView;
};
