// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshader.h"

class VulkanShaderModule;

//------------------------------------------------------------------
/**	@class	ShaderInstance
	@desc	셰이더 객체
			- vertexshader
			- pixelshader

			하나의 기능과 연결되는 셰이더 단위

			skinshader.json

			"":
			{

			}
*/
//------------------------------------------------------------------
class VulkanShader : public IGraphicShader
{
	__DeclareRtti;
public:
	VulkanShader();
	virtual ~VulkanShader();

private:

	// 셰이더
	VulkanShaderModule* m_ShaderModuleInstances[ShaderType::NUM];

	// 버텍스 입력

	// 디스크립터들

};
