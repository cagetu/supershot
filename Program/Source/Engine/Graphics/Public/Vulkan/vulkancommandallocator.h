// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandallocator.h"

class VulkanDevice;
class VulkanCommandList;

//------------------------------------------------------------------
/**	@class	VulkanCommandAllocator
	@desc	CommandBuffer를 생성하는 역할을 한다.
			멀티 쓰레드 처리 효율을 위해서 쓰레드 당 하나의 CommandPool을 생성한다.
			CommandPool이 생성된 CommandBuffer는 같은 쓰레드에서 레코딩되지만,
			Queue에 Submit 될 때에는 동기화처리가 필요하다.
*/
//------------------------------------------------------------------
class VulkanCommandAllocator : public IGraphicCommandAllocator
{
	__DeclareRtti;
public:
	VulkanCommandAllocator(COMMANDLIST_TYPE commandQueueType, uint32 queueFamilyIndex, Vk::CommandPoolCreateFlag::Type createFlags = Vk::CommandPoolCreateFlag::_RESET_COMMAND_BUFFER_BIT);
	virtual ~VulkanCommandAllocator();

	virtual void Reset() override;

	void RefreshFenceStatus();

	VulkanCommandList* CreateCommandList(bool bSecondary = false);
	void DestroyCommandList(VulkanCommandList* buffer);
	void DestroyAllCommandLists();

public:
	VkCommandPool Handle() const { return m_VkCommandPool; }

	// _RESET_COMMAND_BUFFER_BIT이 설정되어 있을 때에만, Reset이 가능하다.
	inline bool CanBeReset() const { return (m_CreateFlags&Vk::CommandPoolCreateFlag::_RESET_COMMAND_BUFFER_BIT) ? true : false; }

private:
	VkCommandPool m_VkCommandPool;

	Array<VulkanCommandList*> m_CommandLists;
	Vk::CommandPoolCreateFlag::Type m_CreateFlags;
	uint32 m_QueueFamilyIndex;
};
