// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

enum CLEAR_TYPE
{
	CLEAR_COLOR = 0x00000001,
	CLEAR_DEPTH = 0x00000002,
	CLEAR_STENCIL = 0x00000004,
	LOAD_COLOR = 0x00000010,
	LOAD_DEPTH = 0x00000020,
	LOAD_STENCIL = 0x00000040,
	NONE_COLOR = 0x00000100,
	NONE_DEPTH = 0x00000200,
	NONE_STENCIL = 0x00000400,
};

enum PRIMITIVE_TYPE
{
	PT_TRIANGLELIST = 0,
	PT_TRIANGLESTRIP,
	PT_TRIANGLEFAN,
	PT_LINELIST,
	PT_LINESTRIP,
	PT_POINTLIST,
	PT_PATCHLIST,
};

enum FORMAT_TYPE
{
	FMT_NONE = 0,
	// 부호 없는 포멧
	FMT_R5G6B5,
	FMT_R8G8B8,
	FMT_R8G8B8X8,
	FMT_R5G5B5A1,
	FMT_R4G4B4A4,
	FMT_R8G8B8A8,
	FMT_B8G8R8A8,
	// 부호 있는 포멧
	FMT_U8V8W8Q8,		// 32 비트의 범프 맵 포맷으로, 채널 마다 8 비트를 사용한다.
	FMT_U16V16W16Q16,	// 64 비트의 범프 맵 포맷으로, 성분 마다 16 비트를 사용한다
	// 버퍼포멧
	FMT_D32,
	FMT_D24S8,
	FMT_D24X8,
	FMT_D16,
	FMT_D15S1,
	FMT_DEPTH_BYTE,
	FMT_DEPTH_INT,
	FMT_DEPTH_SHORT,
	FMT_ALPHA,
	FMT_LUMINANCE,
	FMT_LUMINANCEALPHA,
	// 부동 소수점 포멧
	FMT_R16F,
	FMT_G16R16F,
	FMT_A16B16G16R16F,
	FMT_R32F,
	FMT_G32R32F,
	FMT_A32B32G32R32F,
	FMT_MAX,
};

/**
*	Resource usage flags - for vertex and index buffers.
*/
enum class BufferUsageFlags
{
	// Mutually exclusive write-frequency flags
	BUF_Static = 0x0001, // The buffer will be written to once.
	BUF_Dynamic = 0x0002, // The buffer will be written to occasionally.
	BUF_Volatile = 0x0004, // The buffer will be written to frequently.

	// Mutually exclusive bind flags.
	BUF_UnorderedAccess = 0x0008, // Allows an unordered access view to be created for the buffer.

	/** Create a byte address buffer, which is basically a structured buffer with a uint32 type. */
	BUF_ByteAddressBuffer = 0x0020,
	/** Create a structured buffer with an atomic UAV counter. */
	BUF_UAVCounter = 0x0040,
	/** Create a buffer that can be bound as a stream output target. */
	BUF_StreamOutput = 0x0080,
	/** Create a buffer which contains the arguments used by DispatchIndirect or DrawIndirect. */
	BUF_DrawIndirect = 0x0100,
	/**
	* Create a buffer that can be bound as a shader resource.
	* This is only needed for buffer types which wouldn't ordinarily be used as a shader resource, like a vertex buffer.
	*/
	BUF_ShaderResource = 0x0200,

	/**
	* Request that this buffer is directly CPU accessible
	* (@todo josh: this is probably temporary and will go away in a few months)
	*/
	BUF_KeepCPUAccessible = 0x0400,

	/**
	* Provide information that this buffer will contain only one vertex, which should be delivered to every primitive drawn.
	* This is necessary for OpenGL implementations, which need to handle this case very differently (and can't handle GL_HALF_FLOAT in such vertices at all).
	*/
	BUF_ZeroStride = 0x0800,

	/** Buffer should go in fast vram (hint only) */
	BUF_FastVRAM = 0x1000,

	// Helper bit-masks
	BUF_AnyDynamic = (BUF_Dynamic | BUF_Volatile),
};

enum TextureCreateFlags
{
	TEX_RenderTarget = 1 << 0,
	TEX_DepthStencilTarget = 1 << 1,
	TEX_ResolveRenderTarget = 1 << 2,
	TEX_ResolveDepthStencilTarget = 1 << 3,
	TEX_ShaderResource = 1 << 4,
	TEX_SRGB = 1 << 5,
	TEX_DYNAMIC = 1 << 6,
	TEX_Present = 1 << 7,
	TEX_FastVRAM = 1 << 8,
};

/**
* Action to take when a rendertarget is set.
*/
enum class RT_LOAD_TYPE
{
	None,
	Load,
	Clear,
};

/**
* Action to take when a rendertarget is unset or at the end of a pass.
*/
enum class RT_STORE_TYPE
{
	None,
	Store,
	MultisampleResolve,
};

// 정렬 계층... 낮을수록 뒤에 그려짐...
enum class SORT_TYPE
{
	ORDER_SKY = -50,
	ORDER_BACKGROUND = -40,
	ORDER_WATER = -30,
	ORDER_TERRAIN = -20,
	ORDER_MODEL = 0,
	ORDER_FOREGROUND = 10,
};

enum RESOURCE_TYPE
{
	UID_MATERIAL = 0x100000,
	UID_SHADER = 0x200000,
	UID_PRIMITIVE = 0x500000,
};

enum LOCK_TYPE
{
	LOCK_READONLY = 1 << 1,
	LOCK_DISCARD = 1 << 2,
	LOCK_NOOVERWRITE = 1 << 3,
	LOCK_NOSYSLOCK = 1 << 4,
	LOCK_DONOTWAIT = 1 << 5,
	LOCK_NO_DIRTY_UPDATE = 1 << 6,
};

enum MSAA_TYPE : uint8
{
	MSAA_NONE = 1,
	MSAA_2X = 2,
	MSAA_4X = 4
};

struct ShaderType
{
	enum TYPE
	{
		_INVALID = -1,

		VERTEXSHADER = 0,
		FRAGMENTSHADER = 1,
		COMPUTESHADER = 2,
		GEOMETRYSHADER = 3,

		NUM = GEOMETRYSHADER + 1,
	};

	enum
	{
		VERTEX_BIT = 1 << 0,
		FRAGMENT_BIT = 1 << 1,
		COMPUTE_BIT = 1 << 2,
		GEOMETRY_BIT = 1 << 3,
	};
	typedef int32 MASK;

	static TYPE ToType(int32 index) { return (TYPE)index; }
};

// #define _SKINNING	0
struct ShaderDefine
{
	String name;
	String value;
};

struct ShaderDescription
{
	ShaderType::TYPE type;
	String filename;
	String code;
	String entryPoint;
	String outputPath;
	Array<ShaderDefine> defines;

	ShaderDescription()
	{
		type = ShaderType::_INVALID;
		entryPoint = TEXT("main");
	}
};

enum class DESCRIPTOR_HEAP_TYPE : uint8
{
	STANDARD,
	RENDERTARGET,
	DEPTHSTENCIL,
	SAMPLER,
	NUM,
};
