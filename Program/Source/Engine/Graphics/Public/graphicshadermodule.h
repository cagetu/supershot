// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictypes.h"

//------------------------------------------------------------------
/**	@class	ShaderModule
	@desc	컴파일된 셰이더 코드
*/
//------------------------------------------------------------------
class IGraphicShaderModule
{
	__DeclareRootRtti(IGraphicShaderModule);
	__DeclareReference(IGraphicShaderModule);
public:
	IGraphicShaderModule();
	virtual ~IGraphicShaderModule();

	virtual bool Create(ShaderType::TYPE type, uint32 elementSize, const void* elementData);
	virtual void Destroy();

	inline SizeT GetByteSize() const { return m_ElementSize; }

public:
	ShaderType::TYPE m_Type;	/// 셰이더 타입
	uint32 m_ElementSize;		/// element 크기	
	uint8* m_ElementData;		/// element 데이터
};
