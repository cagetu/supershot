#pragma once

#include "bitflags.h"
#include "Container\map.h"

/*	Shader Feature
	- 엔진 내부에서 필요한 기능들은 먼저 등록해둔다.
		- SKINNED
		- INSTANCING 등...
*/
class ShaderFeature
{
public :
	typedef CBitFlags<512> MASK;

	enum
	{
		_SKINNING = 1,
		_INSTANCING,
		_VERTEXCOMPRESS,

		_USE_NORMAL,
		_USE_VERTEXCOLOR,
		_USE_ALPHA,

		_MAX_DEFINED_FEATURE,
	};
public :
	static void Initialize();
	static void Shutdown();

	static void Reset();

	static MASK GetShaderMask(const Array<String>& defines);
	static MASK GetShaderMask(const wchar* shadermask);
	static String GetMaskName(MASK mask);

	static int GetMacro(MASK mask, utf8::string* macro, utf8::string* macroCode);

private:
	static void ReserveBit(int32 bitIndex, const String& def);
	static int32 RegisterBit(const String& def);
	static int32 FindBit(const String& def);

	static Array<utf8::string> m_Masks;

	static Map<String, int32> m_StringToBit;
	static Array<String> m_BitToString;

	static CBitFlags<512> m_FeatureBits;
	//static const int32 _MAXBIT = 32;
	static int32 m_AllocBit;
};
