// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictypes.h"

class IGraphicDriver;
class IGraphicCommandQueue;

//------------------------------------------------------------------
/** @class	Swapchain
	@desc	BackBuffer를 교체해주는 Swapchain
*/
//------------------------------------------------------------------
class IGraphicSwapchain
{
	__DeclareRootRtti(IGraphicSwapchain);
	__DeclareReference(IGraphicSwapchain);
public:
	IGraphicSwapchain();
	virtual ~IGraphicSwapchain();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, IGraphicCommandQueue* commandQueue = nullptr, uint32 surfaceCount = 2) = 0;
	virtual void Destroy() = 0;

	virtual bool Resize(const WindowHandle& windowHandle) = 0;

	SizeT GetBackBufferCount() const { return m_BackBufferCount; }

public:
	int32 Width() const { return m_WindowHandle.Width(); }
	int32 Height() const { return m_WindowHandle.Height(); }
	bool IsFullScreen() const { return m_WindowHandle.IsFullScreen(); }
	FORMAT_TYPE Format() const { return m_BackBufferFormat; }

protected:
	WindowHandle m_WindowHandle;

	FORMAT_TYPE m_BackBufferFormat;

	SizeT m_BackBufferCount;
};