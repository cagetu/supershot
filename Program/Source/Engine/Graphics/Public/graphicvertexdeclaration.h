// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"

class IGraphicVertexDeclaration : public IGraphicResource
{
	__DeclareRtti;
public:
	IGraphicVertexDeclaration();
	virtual ~IGraphicVertexDeclaration();
};
