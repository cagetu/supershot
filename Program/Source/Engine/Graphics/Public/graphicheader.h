// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "Foundation.h"

#include "graphiccoordinate.h"
#include "graphictypes.h"
#include "graphicshadercompiler.h"

#if _SUPPORT_D3D11 || _SUPPORT_D3D12
#include "DirectX\d3dcoordinate.h"
#endif
#if _SUPPORT_OPENGL_ES
#include "OpenGL\openglcoordinate.h"
#endif
#if _SUPPORT_VULKAN
#include "Vulkan\vulkancoordinate.h"
#endif

#if _SUPPORT_D3D11
#include "DirectX\DX11\d3d11driver.h"
#endif
#if _SUPPORT_D3D12
#include "DirectX\DX12\d3d12driver.h"
#endif
#if _SUPPORT_OPENGL_ES
#include "OpenGL\ES\openglesdriver.h"
#include "OpenGL\ES\openglesshadercompiler.h"
#endif
#if _SUPPORT_VULKAN
#include "Vulkan\vulkandriver.h"
#include "Vulkan\vulkanshadercompiler.h"
#endif
