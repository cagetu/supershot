// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class IGraphicRenderFrame
{
	__DeclareRootRtti(IGraphicRenderFrame);
public:
	IGraphicRenderFrame();
	virtual ~IGraphicRenderFrame();
};
