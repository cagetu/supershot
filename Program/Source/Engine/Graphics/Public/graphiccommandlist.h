// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandtypes.h"

//------------------------------------------------------------------
/**	@class	Base Command List
	@desc	
*/
//------------------------------------------------------------------
class IGraphicCommandList
{
	__DeclareRootRtti(IGraphicCommandList);
public:
	IGraphicCommandList();
	virtual ~IGraphicCommandList();

public:
	enum class _STATE
	{
		READYTOBEGIN = 0,
		BEGIN_RECORD,
			RENDERPASS,
		END_RECORD,
		SUBMITTED,
	};

	inline _STATE GetState() const { return m_State; }

	inline bool IsSubmitted() const { return (m_State == _STATE::SUBMITTED) ? true : false; }
	inline bool IsEndRecorded() const { return (m_State == _STATE::END_RECORD) ? true : false; }

protected:
	_STATE m_State;
};
