// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"
#include "graphicshaderfeature.h"

#include "graphictypes.h"

class Variant;

class IGraphicShaderModule;

//------------------------------------------------------------------
/**	@class	Shader
	@desc	셰이더 객체

			Shader
				- VertexProgram
				- FragmentProgram
				- ....

			의 모습으로 파이프라인에서 사용되는 셰이더를 가진다.

		하나의 셰이더는 조합이 완성된 형태의 셰이더를 의미한다. (Vertex + Fragment + xxx)
		따라서, Shader 내에는 내부적으로 타입 단위로 구분되는 한 단위 셰이더 객체 단위로 정보를 가지고 있다. (UnitShader)
*/
//------------------------------------------------------------------
class IGraphicShaderInstance
{
	__DeclareRootRtti(IGraphicShaderInstance);
public:
	IGraphicShaderInstance();
	virtual ~IGraphicShaderInstance();

	const ShaderFeature::MASK& GetFeatureFlags() const;

private:
	ShaderFeature::MASK m_FeatureFlags;
};

//------------------------------------------------------------------
/**	@class	ShaderProgram
	@desc	각 종류 별 셰이더
*/
//------------------------------------------------------------------
class IGraphicShaderModuleInstance
{
	__DeclareRootRtti(IGraphicShaderModuleInstance);
public:
	IGraphicShaderModuleInstance();
	virtual ~IGraphicShaderModuleInstance();

	virtual bool Create(IGraphicShaderModule* shaderModule) = 0;
	virtual void Destroy() = 0;

	virtual void SetValue(uint32 index, const Variant& value) = 0;
	//virtual void UpdateValues(const CMaterial* material) = 0;
	virtual void ClearValues() = 0;

	//virtual void UpdateVertexAttributes(const VertexBuffer* vertexBuffer) {}
	//virtual void ClearVertexAttributes() {}

public:
	bool HasShaderType(ShaderType::TYPE type) const { return m_ShaderTypes & (1 << type) ? true : false; }

protected:
	ShaderType::MASK m_ShaderTypes;
};
