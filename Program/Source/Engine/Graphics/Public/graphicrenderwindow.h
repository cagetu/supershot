// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictypes.h"

//------------------------------------------------------------------
/**	@class	IGraphicRenderWindow
	@desc	Render 대상 윈도우 클래스
*/
//------------------------------------------------------------------
class IGraphicRenderWindow
{
	__DeclareRootRtti(IGraphicRenderWindow);
	__DeclareReference(IGraphicRenderWindow);
public:
	enum { _NUM_SWAP_BUFFERS = 3 };

public:
	IGraphicRenderWindow();
	virtual ~IGraphicRenderWindow();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, bool bUseDepthSurface = true);
	virtual void Destroy() = 0;

	virtual void Resize(const WindowHandle& windowHandle) = 0;

public:
	int32 Width() const { return m_WindowHandle.Width(); }
	int32 Height() const { return m_WindowHandle.Height(); }
	FORMAT_TYPE Format() const { return (FORMAT_TYPE)m_Format; }
	bool IsFullScreen() const { return m_WindowHandle.IsFullScreen(); }
	bool HasDepthSurface() const { return m_bUseDepthSurface; }

protected:
	WindowHandle m_WindowHandle;
	FORMAT_TYPE m_Format;
	bool m_bUseDepthSurface;
};
