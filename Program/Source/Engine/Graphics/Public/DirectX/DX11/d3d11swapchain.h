// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicswapchain.h"
#include "d3d11header.h"

class D3D11Device;

class D3D11Swapchain : public IGraphicSwapchain
{
	__DeclareRtti;
public:
	D3D11Swapchain(D3D11Device* device);
	virtual ~D3D11Swapchain();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, IGraphicCommandQueue* commandQueue = nullptr, uint32 surfaceCount = 2) override;
	virtual void Destroy() override;

	virtual bool Resize(const WindowHandle& windowHandle) override;

	void Present();

public:
	IDXGISwapChain* Handle() const { return m_SwapChain; }
	ID3D11RenderTargetView* ColorBuffer() const { return m_BackbufferRTV; }

private:
	bool CreateRenderBuffer();

	D3D11Device* m_Device;

	IDXGISwapChain*	m_SwapChain;
	ID3D11RenderTargetView* m_BackbufferRTV;
};
