// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrenderwindow.h"
#include "d3d11header.h"

class D3D11Device;
class D3D11Swapchain;

class D3D11RenderWindow : public IGraphicRenderWindow
{
	__DeclareRtti;
public:
	D3D11RenderWindow(D3D11Device* device);
	virtual ~D3D11RenderWindow();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, bool bUseDepthBuffer = true) override;
	virtual void Destroy() override;

	virtual void Resize(const WindowHandle& windowHandle) override;

	void Begin();
	void End();

	void Present();

private:
	bool CreateDepthBuffer();

	D3D11Device* m_Device;
	D3D11Swapchain* m_SwapChain;

	ID3D11DepthStencilView* m_BackbufferDSV;
};
