// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshadercompiler.h"

//------------------------------------------------------------------
/**	@class	D3D11ShaderCompiler
	@desc	Vulkan 셰이더 컴파일러

			입력된 셰이더 코드를 Spir-V 셰이더로 컴파일한다.
*/
//------------------------------------------------------------------
class D3D11ShaderCompiler : public IGraphicShaderCompiler
{
    __DeclareRtti;
public:
	D3D11ShaderCompiler();
	virtual ~D3D11ShaderCompiler();

	virtual IGraphicShaderModule* Compile(const ShaderDescription& desc) override;

private:
};