// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdevice.h"
#include "color.h"
#include "d3d11header.h"

class Viewport;

class D3D11Device : public IGraphicDevice
{
	__DeclareRtti;
public:
	D3D11Device(ID3D11Device* InDevice, ID3D11DeviceContext* InDeviceContext, IDXGIFactory* dxgiFactory);
	virtual ~D3D11Device();

	void SetViewport(int32 x, int32 y, int32 width, int32 height, float minDepth = 0.0f, float maxDepth = 1.0f);

	bool CreateRenderTargetView(ID3D11Resource* resources, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc, ID3D11RenderTargetView **ppRTView);
	bool CreateDepthStencilView(ID3D11Resource* resources, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc, ID3D11DepthStencilView **ppDepthStencilView);

	bool CreateBuffer(const D3D11_BUFFER_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Buffer **ppBuffer);
	bool CreateTexture2D(const D3D11_TEXTURE2D_DESC* pDesc, const D3D11_SUBRESOURCE_DATA* pInitialData, ID3D11Texture2D **ppTexture2D);

	void SetRenderTarget(ID3D11RenderTargetView* renderTargetView, ID3D11DepthStencilView* depthStencilView);
	void SetRenderTargets(const Array<ID3D11RenderTargetView*>& renderTargetViews, ID3D11DepthStencilView* depthStencilView);

	void ClearRenderTargetView(const LinearColor& clearColor);
	void ClearDepthStencilView(unsigned long clearFlags = CLEAR_DEPTH, float depth = 1.0f, unsigned char stencil = 0);

public:
	ID3D11Device* Device() const { return m_Device; }
	ID3D11DeviceContext* Context() const { return m_DeviceContext; }
	IDXGIFactory* Factory() const { return m_DxgiFactory; }

private:
	ID3D11Device* m_Device;
	ID3D11DeviceContext* m_DeviceContext;
	IDXGIFactory* m_DxgiFactory;

	Array<ID3D11RenderTargetView*> m_CurrentRTV;
	ID3D11DepthStencilView* m_CurrentDSV;
};