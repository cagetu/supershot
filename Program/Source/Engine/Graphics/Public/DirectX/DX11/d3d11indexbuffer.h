// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicindexbuffer.h"

class D3D11IndexBuffer : public IGraphicIndexBuffer
{
public:
	D3D11IndexBuffer();
	virtual ~D3D11IndexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;
};
