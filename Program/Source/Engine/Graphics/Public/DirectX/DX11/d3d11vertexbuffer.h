// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicvertexbuffer.h"

class D3D11VertexBuffer : public IGraphicVertexBuffer
{
public:
	D3D11VertexBuffer();
	virtual ~D3D11VertexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;
};
