// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdriver.h"
#include "DirectX/DX11/d3d11device.h"

class D3D11RenderWindow;
class D3D11ShaderCompiler;

class D3D11Driver : public IGraphicDriver
{
	__DeclareRtti;
public:
	D3D11Driver();
	virtual ~D3D11Driver();

	virtual bool Create(const WindowHandle& windowHandle) override;
	virtual void Destroy() override;

	virtual void BeginFrame(Viewport* viewport) override;
	virtual void EndFrame() override;

	virtual void Present() override;

	virtual void Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil) override;

	virtual IGraphicDevice* CurrentDevice() override { return m_Device; }

	virtual IGraphicShaderCompiler* GetShaderCompiler() override;
	virtual IGraphicShaderModule* CompileShader(const ShaderDescription& desc) override;

private:
	friend class D3D11RenderWindow;

private:
	bool CreateDevice();
	void SetViewport(const Viewport* viewport);

	D3D11Device* m_Device;
	D3D11RenderWindow* m_RenderWindow;
	D3D11ShaderCompiler* m_ShaderCompiler;
};
