// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "DirectX/DX12/d3d12header.h"
#include "graphictypes.h"
#include "graphiccommandtypes.h"

namespace D3D12
{
	/**
	 * Checks that the given result isn't a failure.  If it is, the application exits with an appropriate error message.
	 * @param	Result - The result code to check
	 * @param	Code - The code which yielded the result.
	 * @param	Filename - The filename of the source file containing Code.
	 * @param	Line - The line number of Code within Filename.
	 */
	extern void VerifyD3D12Result(HRESULT Result, const char* Code, const char* Filename, uint32 Line, ID3D12Device* Device, String Message = String());

	/**
	 * Convert D3D12 Format
	*/
	extern DXGI_FORMAT D3DFormat(FORMAT_TYPE format);

	extern D3D12_COMMAND_LIST_TYPE D3DCommandListType(COMMANDLIST_TYPE commandType);
}

#define VERIFYD3D12RESULT(x)		{ HRESULT hres = x; if (FAILED(hres)) { D3D12::VerifyD3D12Result(hres, #x, __FILE__, __LINE__, nullptr); }}

