// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "DirectX/DX12/d3d12header.h"

//------------------------------------------------------------------
/**	@class	DirectX12 PipelineState
	@desc	이건 뭐지????
*/
//------------------------------------------------------------------
class D3D12PipelineState
{
public:
	ID3D12PipelineState* GetHandle() const { return m_PipelineStateHandle; }

private:
	ID3D12PipelineState* m_PipelineStateHandle;
};
