﻿// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class D3D12Device;
class D3D12CommandList;
class D3D12Fence;

#include "graphiccommandqueue.h"
#include "DirectX/DX12/d3d12header.h"

//------------------------------------------------------------------
/**	@class	DirectX12 CommandQueue 
	@desc	GPU에 CommandList를 Submit 하기 위해서는 CommandQueue가 필요하다.
			CommandQueue는 여러 다른 타입이 있고, 타입에 맞는 CommandList가 Submit 되도록 허용된다.

			CPU에서 그리기 명령을 CommandList에 담아서, GPU에 그리기 위해서 CommandQueue에 제출한다.
*/
//------------------------------------------------------------------
class D3D12CommandQueue : public IGraphicCommandQueue
{
	__DeclareRtti;
public:
	D3D12CommandQueue(D3D12Device* device, COMMANDLIST_TYPE commandQueueType,
		D3D12_COMMAND_QUEUE_FLAGS queueFlags = D3D12_COMMAND_QUEUE_FLAG_NONE, D3D12_COMMAND_QUEUE_PRIORITY queuePriority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL,
		uint32 nodeMask = 0);
	virtual ~D3D12CommandQueue();

	// CommandList를 CommandQueue에 제출한다. (제출한다고 바로 실행되는 것은 아니다. 처리가 가능할 때, 꺼내서 처리)
	void Submit(D3D12CommandList* commandList);
	void Submit(const Array<D3D12CommandList*>& commandLists);

	// 새 Fence 지점을 설정하는 명령을 추가한다. (GPU가 fence를 fenceID로 설정하는 명령을 CommandQueue에 추가한다)
	bool Signal(D3D12Fence* fence, uint64 fenceValue);
	// CommandQueue가 기다리는 fenceID. 펜스의 현재 값이 이 값보다 크거나 같으면 기다림이 끝난다)
	bool Wait(D3D12Fence* fence, uint64 fenceValue);

public:
	ID3D12CommandQueue* GetHandle() const { return m_D3DQueue.Get(); }

private:
	Ptr<ID3D12CommandQueue> m_D3DQueue;
};