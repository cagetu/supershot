// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "DirectX/DX12/d3d12header.h"

struct D3D12AdapterDesc
{
	D3D12AdapterDesc()
		: AdapterIndex(-1)
		, MaxSupportedFeatureLevel((D3D_FEATURE_LEVEL)0)
		, NumDeviceNodes(0)
	{
	}

	D3D12AdapterDesc(DXGI_ADAPTER_DESC& DescIn, int32 InAdapterIndex, D3D_FEATURE_LEVEL InMaxSupportedFeatureLevel, uint32 NumNodes)
		: AdapterIndex(InAdapterIndex)
		, MaxSupportedFeatureLevel(InMaxSupportedFeatureLevel)
		, Desc(DescIn)
		, NumDeviceNodes(NumNodes)
	{
	}

	bool IsValid() const { return MaxSupportedFeatureLevel != (D3D_FEATURE_LEVEL)0 && AdapterIndex >= 0; }

	int32 AdapterIndex;
	D3D_FEATURE_LEVEL MaxSupportedFeatureLevel;
	DXGI_ADAPTER_DESC Desc;
	uint32 NumDeviceNodes;
};

//------------------------------------------------------------------
/**	@class	PhysicalDevice
	@desc	그래픽 카드 (Adaptor)에 대한 클래스.
			그래픽카드에 대한 Capability 등 정보를 가진다.
*/
//------------------------------------------------------------------
class D3D12Adapter
{
	__DeclareReference(D3D12Adapter);
public:
	D3D12Adapter(const D3D12AdapterDesc& desc);
	~D3D12Adapter();

	void Create(IDXGIAdapter* inAdapter);
	void Destroy();

	const D3D12AdapterDesc& GetDesc() const { return m_AdapterDesc; }
	IDXGIAdapter* GetHandle() const { return m_AdapterHandle; }

private:
	D3D12AdapterDesc m_AdapterDesc;
	IDXGIAdapter* m_AdapterHandle;
};