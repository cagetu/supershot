// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandlist.h"
#include "DirectX/DX12/d3d12header.h"

class D3D12Device;
class D3D12CommandAllocator;
class D3D12PipelineState;

//------------------------------------------------------------------
/**	@class	DirectX12 CommandQueueList
	@desc	CommandQueue 가 Command를 실행 시키기 위해서는 CommandList를 이용해야 한다.
*/
//------------------------------------------------------------------
class D3D12CommandList : public IGraphicCommandList
{
	__DeclareRtti;
public:
	D3D12CommandList();
	virtual ~D3D12CommandList();

	bool Create(D3D12Device* device, uint32 nodeMask, D3D12CommandAllocator* commandAllocator, D3D12PipelineState* initState = nullptr);
	void Destroy();

	// Command List를 닫는다. (Command Record 가 끝났음을 알린다)
	bool Close();
	// Command List를 처음 생성했을 때와 같은 상태로 만든다. (내무 메모리를 재활용할 수 있다. 단, CommandQueue가 CommandList 처리 완료 후에 진행해야 한다.)
	bool Reset(D3D12CommandAllocator* commandAllocator, D3D12PipelineState* initState);

public:
	COMMANDLIST_TYPE GetCommandListType() const;

//<@ CommandList 명령어 목록
public:
	void ClearState(D3D12PipelineState* pipelineState);
//>@ CommandList 명령어 목록

public:
	ID3D12GraphicsCommandList* GetHandle() const { return m_D3DCommandList; }

private:
	Ptr<ID3D12GraphicsCommandList> m_D3DCommandList;
	D3D12CommandAllocator* m_CommandAllocator;

	COMMANDLIST_TYPE m_CommandListType;

	bool m_bOpended;
};