// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdriver.h"
#include "DirectX/DX12/d3d12device.h"

class D3D12RenderWindow;
class D3D12ShaderCompiler;
class D3D12Adapter;
class D3D12CommandQueue;

//------------------------------------------------------------------
/**	@class	D3D12Driver
*	@desc	Driver는 DX12의 Device 생성 등을 책임진다.
*/
//------------------------------------------------------------------
class D3D12Driver : public IGraphicDriver
{
	__DeclareRtti;
public:
	D3D12Driver();
	virtual ~D3D12Driver();

	virtual bool Create(const WindowHandle& windowHandle) override;
	virtual void Destroy() override;

	virtual void BeginFrame(Viewport* viewport) override;
	virtual void EndFrame() override;

	virtual void Present() override;

	virtual void Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil) override;

	virtual IGraphicDevice* CurrentDevice() override { return m_Device; }

	virtual IGraphicShaderCompiler* GetShaderCompiler() override;
	virtual IGraphicShaderModule* CompileShader(const ShaderDescription& desc) override;

private:
	bool CreateDevice();
	void DestroyDevice();

	bool CreateRenderWindow(const WindowHandle& windowHandle);
	void DestroyRenderWindow();

	void SetViewport(const Viewport* viewport);

	D3D12Device* m_Device;

	//Array<D3D12Adapter*> m_Adapters;

	D3D12RenderWindow* m_RenderWindow;
	D3D12ShaderCompiler* m_ShaderCompiler;
};
