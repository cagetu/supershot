// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictexture.h"
#include "DirectX/DX12/d3d12header.h"

class D3D12Device;

//------------------------------------------------------------------
/**	@class	D3D12Texture
	@desc	D3D12 베이트 텍스쳐 클래스
*/
//------------------------------------------------------------------
class D3D12Texture : public IGraphicTexture
{
	__DeclareRtti;
public:
	D3D12Texture(D3D12Device* device);
	virtual ~D3D12Texture();

	ID3D12Resource* GetResource() const { return m_Resource; }

protected:
	D3D12Device* m_Device;

	ID3D12Resource* m_Resource;
};

//------------------------------------------------------------------
/**	@class	D3D12 Render Target Texture
	@desc	
*/
//------------------------------------------------------------------
class D3D12RenderTargetTexture : public D3D12Texture
{
	__DeclareRtti;
public:
	D3D12RenderTargetTexture(D3D12Device* device);
	D3D12RenderTargetTexture(D3D12Device* device, ID3D12Resource* textureHandle, const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView);
	virtual ~D3D12RenderTargetTexture();

	void SetRenderTargetView(const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView);
	D3D12_CPU_DESCRIPTOR_HANDLE GetRenderTargetView() const { return m_RenderTargetView; }

protected:
	D3D12_CPU_DESCRIPTOR_HANDLE m_RenderTargetView;
};

//------------------------------------------------------------------
/**	@class	D3D12 Colored Render Target Texture
	@desc
*/
//------------------------------------------------------------------
class D3D12ColorBuffer : public D3D12RenderTargetTexture
{
	__DeclareRtti;
public:
	D3D12ColorBuffer(D3D12Device* device);
	D3D12ColorBuffer(D3D12Device* device, ID3D12Resource* textureHandle, const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView);
	virtual ~D3D12ColorBuffer();
};

//------------------------------------------------------------------
/**	@class	D3D12 DepthStencil Render Target Texture
	@desc
*/
//------------------------------------------------------------------
class D3D12DepthStencilBuffer : public D3D12RenderTargetTexture
{
	__DeclareRtti;
public:
	D3D12DepthStencilBuffer(D3D12Device* device);
	D3D12DepthStencilBuffer(D3D12Device* device, ID3D12Resource* textureHandle, const D3D12_CPU_DESCRIPTOR_HANDLE& renderTaretView);
	virtual ~D3D12DepthStencilBuffer();
};
