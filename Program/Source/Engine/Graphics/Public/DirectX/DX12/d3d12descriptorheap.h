// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class D3D12Device;

#include "DirectX/DX12/d3d12header.h"
#include "DirectX/DX12/d3d12resource.h"

//------------------------------------------------------------------
/**	@class	D3D12 Descriptor Heap (서술자 힙)
*	@desc	서술자(Descriptor)/뷰를 담을 수 있는 서술자 힙을 만들어야 한다.
			DescriptorHeap을 통해서 Descriptor를 접근할 수 있다.
* 
*			서술자 힙은 서술자의 종류마다 따로 만들어야 한다.
*/
//------------------------------------------------------------------
class D3D12DescriptorHeap : public GfxReferenceCount
{
public:
	D3D12DescriptorHeap();
	virtual ~D3D12DescriptorHeap();

	bool Create(D3D12Device* device, uint32 numDescriptors, D3D12_DESCRIPTOR_HEAP_TYPE descriptorType, D3D12_DESCRIPTOR_HEAP_FLAGS descriptorFlags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE);
	void Destroy();

	inline ID3D12DescriptorHeap* GetHandle() const { return m_HeapHandle; }
	inline D3D12_DESCRIPTOR_HEAP_TYPE GetHeapType() const { return m_HeapType; }

	inline uint32 GetNumDescriptors() const { return m_NumDescriptors; }
	inline uint32 GetDescriptorSize() const { return m_DescriptorSize; }

	D3D12_CPU_DESCRIPTOR_HANDLE GetDescriptorHandleCPU(uint32 offsetInDescriptor);
	D3D12_GPU_DESCRIPTOR_HANDLE GetDescriptorHandleGPU(uint32 offsetInDescriptor);

private:
	ID3D12DescriptorHeap* m_HeapHandle;

	D3D12_DESCRIPTOR_HEAP_TYPE m_HeapType;
	D3D12_DESCRIPTOR_HEAP_FLAGS m_HeapFlags;

	uint32 m_NumDescriptors;
	uint32 m_DescriptorSize;
};
