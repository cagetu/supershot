// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicindexbuffer.h"

//------------------------------------------------------------------
/**	@class	D3D12IndexBuffer
	@desc	DX12에서 그래픽 리소스로 사용되는 인덱스 버퍼
*/
//------------------------------------------------------------------
class D3D12IndexBuffer : public IGraphicIndexBuffer
{
public:
	D3D12IndexBuffer();
	virtual ~D3D12IndexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;
};
