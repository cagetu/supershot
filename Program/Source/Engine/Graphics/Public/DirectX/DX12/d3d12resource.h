// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"
#include "DirectX/DX12/d3d12header.h"

//------------------------------------------------------------------
/**	@class	D3D12Resource
	@desc	DX12에서 그래픽 리소스로 사용되는 것의 최상위 객체
			D3D12Resource
*/
//------------------------------------------------------------------
class D3D12Resource : public IGraphicResource
{
	__DeclareRtti;
public:
	D3D12Resource();
	virtual ~D3D12Resource();

	D3D12_RESOURCE_DESC GetDesc() const { return m_ResourceDesc; }

protected:
	D3D12_RESOURCE_DESC m_ResourceDesc;
};
