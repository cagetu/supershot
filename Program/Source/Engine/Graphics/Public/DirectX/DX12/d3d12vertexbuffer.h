// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicvertexbuffer.h"

//------------------------------------------------------------------
/**	@class	D3D12VertexBuffer
	@desc	DX12에서 그래픽 리소스로 사용되는 버텍스 버퍼
*/
//------------------------------------------------------------------
class D3D12VertexBuffer : public IGraphicVertexBuffer
{
public:
	D3D12VertexBuffer();
	virtual ~D3D12VertexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;
};
