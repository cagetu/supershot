// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandallocator.h"
#include "DirectX/DX12/d3d12header.h"

class D3D12Device;
class D3D12CommandList;

//------------------------------------------------------------------
/**	@class	DirectX12 Command Allocator
	@desc	CommandList를 생성하기 위해서는 CommandAllocator가 필요하다.
			- 일반적인 linear Allocator 형태로 되어있다.
			- Command Allocator로 부터 사용되는 메모리는 ID3D12CommandAllocator::Reset이 호출되기 전까지 유지된다.

			- Command Allocator는 Thread Safe하지 않다. 따라서, Command List가 필요한 쓰레드마다 하나 이상의 Command Allocator를 가져야한다.
			- Dynamic Command List를 처리할 때 쓰레드 마다 다수의 CommandAllocator가 필요하다.
				- Command Buffer가 실행 중이 동안에는 Reset 함수를 호출 할 수 없기 때문이다.

			(TODO) 최적의 Command Allocator와 Command List를 운영하는 방법은 찾아봐야 한다.
*/
//------------------------------------------------------------------
class D3D12CommandAllocator : public IGraphicCommandAllocator
{
	__DeclareRtti;
public:
	D3D12CommandAllocator(D3D12Device* device, COMMANDLIST_TYPE commandQueueType);
	virtual ~D3D12CommandAllocator();

	/*
	 * Command Allocator로 부터 사용되는 메모리는 ID3D12CommandAllocator::Reset이 호출되기 전까지 유지된다.
	 * Commandbuffer가 실행되는 동안에는 Reset 함수를 호출 할 수 없다. (동기화 필요)
	 * Command Allocator의 메모리를 다음 프레임을 위해 재활용 가능
	 * GPU가 Command Allocator에 담긴 모든 명령이 실행했음을 확신하기 전까지는 Command Allocator의 Reset을 하지 말아야 한다.
	 */
	virtual void Reset() override;

public:
	ID3D12CommandAllocator* GetHandle() const { return m_D3DAllocator.Get(); }

private:
	Ptr<ID3D12CommandAllocator> m_D3DAllocator;
	COMMANDLIST_TYPE m_CommandListType;
};
