// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdevice.h"
#include "graphiccommandtypes.h"
#include "DirectX/DX12/d3d12header.h"

class D3D12Adapter;
class D3D12CommandList;
class D3D12CommandQueue;
class D3D12CommandAllocator;
class D3D12DescriptorPool;

//------------------------------------------------------------------
/**	@class	D3D12 Device
	@desc	논리적 디바이스에 대한 클래스
			렌더링 디바이스를 적용한다.
			추후, Compute Device 등은 어떻게 처리할지 고민이 필요!
*/
//------------------------------------------------------------------
class D3D12Device : public IGraphicDevice
{
	__DeclareRtti;
public:
	D3D12Device(const Ptr<D3D12Adapter>& adapter);
	virtual ~D3D12Device();

	ID3D12Device* GetDevice() const { return m_Device.Get(); }
	//void SetViewport(int32 x, int32 y, int32 width, int32 height, float minDepth = 0.0f, float maxDepth = 1.0f);

public:
	// CommandList 할당자를 생성한다. (Thread 당 하나를 만들어줘야 한다)
	D3D12CommandAllocator* CreateCommandAllocator(COMMANDLIST_TYPE commandListType);
	void DestroyCommandAllocator(D3D12CommandAllocator* commandAllocator);

	// GPU에 CommandList를 Submit 하기 위한 CommandQueue를 생성한다.
	D3D12CommandQueue* CreateCommandQueue(COMMANDLIST_TYPE commandListType, D3D12_COMMAND_QUEUE_FLAGS commandQueueFlags);
	void DestroyCommandQueue(D3D12CommandQueue* queue);

	int32 CheckSupportMSAA(FORMAT_TYPE format);

private:
	Ptr<ID3D12Device> m_Device;
	Ptr<D3D12Adapter> m_Adapter;

	//Ptr<D3D12DescriptorPool> m_DescriptorPool;
};
