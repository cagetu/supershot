// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "DirectX/DX12/d3d12header.h"

class D3D12Device;

//------------------------------------------------------------------
/**	@class	D3D12 DescriptorPool
*	@desc	
*/
//------------------------------------------------------------------
class D3D12DescriptorPool
{
public:
	D3D12DescriptorPool(D3D12Device* device);
	~D3D12DescriptorPool();

public:
};
