// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicswapchain.h"

class D3D12Driver;
class D3D12CommandQueue;
class D3D12DescriptorHeap;
class D3D12ColorBuffer;

struct IDXGISwapChain3;

//------------------------------------------------------------------
/**	@class	DirectX12 Swap Chain
	@desc	Swapchain은 멀티플 버퍼링을 위해서 여러 이미지를 가진다.
			
			- Swapchain은 하나의 CommandQueue를 가진다.
			- Swapchain은 BackBuffer를 가지고 전환처리를 한다.
*/
//------------------------------------------------------------------
class D3D12Swapchain : public IGraphicSwapchain
{
	__DeclareRtti;
public:
	D3D12Swapchain(D3D12Device* device);
	virtual ~D3D12Swapchain();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE backBufferFormat = FMT_R8G8B8A8, IGraphicCommandQueue* commandQueue = nullptr, uint32 backBufferCount = 2) override;
	virtual void Destroy() override;

	virtual bool Resize(const WindowHandle& windowHandle) override;

	void Present();

	uint32 GetCurrentBackbufferIndex() const;
	D3D12ColorBuffer* GetBackBuffer(uint32 backBufferIndex) const;

private:
	bool CreateSwapChain(const WindowHandle& windowHandle, IGraphicCommandQueue* commandQueue, FORMAT_TYPE format, uint32 surfaceCount);
	void DestroySwapChain();

	bool CreateBackBuffers(uint32 surfaceCount);
	void DestroyBackBuffers();

	IDXGISwapChain3* m_SwapChainHandle;

	D3D12Device* m_Device;

	/// BackBuffer 용 DescriptorHeap
	D3D12DescriptorHeap* m_BackBufferDescriptorHeap;
	Array<D3D12ColorBuffer*> m_BackBuffers;
};
