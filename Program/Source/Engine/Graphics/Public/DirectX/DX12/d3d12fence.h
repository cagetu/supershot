// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "DirectX/DX12/d3d12header.h"

class D3D12Device;

//------------------------------------------------------------------
/**	@class	DirectX12 Fence
	@desc	CPU와 GPU 동기화를 위한 객체

			CPU에서 작성된 CommandList가 CommandQueue에 제출될 때, GPU에서 해당 CommandList가 처리가 끝났을 때까지 CPU를 대기 시킨다.
*/
//------------------------------------------------------------------
class D3D12Fence
{
public:	
	D3D12Fence(D3D12Device* device, uint64 initialValue = 0, D3D12_FENCE_FLAGS fenceFlags = D3D12_FENCE_FLAG_NONE);
	~D3D12Fence();

	// GPU가 현재 Fence 지점에 도달했으면, 이벤트를 발동한다.
	bool SetEventOnCompletion(uint64 value, HANDLE eventHandle);
	// Fence의 값을 내가 원하는 값으로 바꾼다.
	bool Signal(uint64 value);
	// 현재 Fence 오브젝트가 가지고 있는 값을 반환한다.
	uint64 GetCompletedValue() const;

public:
	ID3D12Fence* GetHandle() const { return m_D3DFence.Get(); }

private:
	Ptr<ID3D12Fence> m_D3DFence;
	D3D12_FENCE_FLAGS m_FenceFlags;
};

//------------------------------------------------------------------
/**	@class	FencePool
	@desc	Fence를 재활용해서 사용하기 위한 Fence Pool
*/
//------------------------------------------------------------------
class D3D12FencePool
{
public:
	D3D12FencePool(D3D12Device* device);
	~D3D12FencePool();

	void Destroy();

	D3D12Fence* AllocateFence();
	void ReleaseFence(D3D12Fence* fence, uint64 currentFenceValue);

private:
	D3D12Device* m_Device;

	CriticalSection CS;

	Array<D3D12Fence*> m_FreeFences;
	Array<D3D12Fence*> m_UsedFences;
};

//------------------------------------------------------------------
/**	@class	동기화 위치
	@desc	CPU와 GPU 동기화를 위한 객체
*/
//------------------------------------------------------------------
class D3D12SyncPoint
{
public:
	explicit D3D12SyncPoint()
		: m_Fence(nullptr)
		, m_Value(0)
	{
	}

	explicit D3D12SyncPoint(D3D12Fence* InFence, uint64 InValue)
		: m_Fence(InFence)
		, m_Value(InValue)
	{
	}

private:
	D3D12Fence* m_Fence;
	uint64 m_Value;
};
