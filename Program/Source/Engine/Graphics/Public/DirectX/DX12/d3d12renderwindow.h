// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrenderwindow.h"

class D3D12Device;
class D3D12Swapchain;
class D3D12CommandQueue;
class D3D12CommandAllocator;
class D3D12CommandList;
class D3D12DepthStencilBuffer;

//------------------------------------------------------------------
/**	@class	D3D12 RenderWindow
	@desc	������ ���� Ÿ�� Ŭ����
*/
//------------------------------------------------------------------
class D3D12RenderWindow : public IGraphicRenderWindow
{
	__DeclareRtti;
public:
	D3D12RenderWindow(D3D12Device* device);
	virtual ~D3D12RenderWindow();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, bool bUseDepthBuffer = true) override;
	virtual void Destroy() override;

	virtual void Resize(const WindowHandle& windowHandle) override;

	void Begin();
	void End();

	void Present();

private:
	bool CreateDepthBuffer(uint32 width, uint32 height, FORMAT_TYPE format = FMT_D24S8);
	void DestroyDepthBuffer();

	bool CreateCommandLists(int32 requireBufferCount);
	void DestroyCommandLists();

	D3D12Device* m_Device;

	Ptr<D3D12Swapchain> m_SwapChain;

	D3D12DescriptorHeap* m_DepthStencilDescriptorHeap;
	D3D12DepthStencilBuffer* m_DepthStencilBuffer;

	D3D12CommandAllocator* m_CommandAllocator;
	D3D12CommandQueue* m_CommandQueue;
	D3D12CommandList* m_CommandLists[_NUM_SWAP_BUFFERS];
};
