// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdescriptor.h"

class D3D12Device;

//------------------------------------------------------------------
/**	@class	D3D12 Descriptor
	@desc	GPU Resource를 렌더링파이프라인에 Bind하는데 사용된다.
			단일 리소스에 대한 바인딩의 기본 단위이이다.

			GPU Resource는 범용적인 메모리이기 때문에, 같은 자원을 
			렌더링 파이프라인의 서로 다른 단계(Stage)에서도 사용할 수 있다.

			자원 자체는 렌더파이프라인에 어떻게 묶일지에 대한 정보를 전혀 가지고 있지 않기 때문에,
			Descriptor가 파이프라인의 어떤 단계에 묶어야 하는지 등의 정보를 알려준다.

			(* View는 Descriptor와 동의어이다. 상수 버퍼 뷰는 상수 버퍼 서술자와 같은 것이다.)
*/
//------------------------------------------------------------------
class D3D12Descriptor : public IGraphicDescriptor
{
	__DeclareRtti;
public:
	D3D12Descriptor();
	virtual ~D3D12Descriptor();
};


