// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include  "graphictypes.h"

//------------------------------------------------------------------
/** @class	Graphic Settings
	@desc	그래픽 설정에 필요한 옵션 설정을 설정한다.
*/
//------------------------------------------------------------------
class GraphicSettings
{
public:
	bool bVSyncEnable = false;
	uint32 RefreshRateNumerator = 1;
	uint32 RefreshRateDenominator = 60;

	MSAA_TYPE MultiSampleType = MSAA_TYPE::MSAA_NONE;
	uint32 MultiSampleQuality = 1;

	uint32 SwapBufferCount = 2;

public:
	static GraphicSettings& Get();
};
