// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicresource.h"

class IGraphicIndexBuffer : public IGraphicResource
{
	__DeclareRtti;
public:
	IGraphicIndexBuffer();
	virtual ~IGraphicIndexBuffer();

	virtual bool Create(int32 bufferSize) = 0;
	virtual void Destroy() = 0;
};