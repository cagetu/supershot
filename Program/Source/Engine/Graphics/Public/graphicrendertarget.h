// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "color.h"

//------------------------------------------------------------------
/**	@class	����Ÿ�ٿ� ���� Ŭ����
*/
//------------------------------------------------------------------
class IGraphicRenderTarget
{
	__DeclareRootRtti(IGraphicRenderTarget);
public:
	IGraphicRenderTarget();
	virtual ~IGraphicRenderTarget();

	virtual bool Create(int width, int height, int32 multisample = 1) = 0;
	virtual void Destroy() = 0;

public:
	int32 Width() const { return m_Width; }
	int32 Height() const { return m_Height; }

	void SetClearMode(int32 flags) { m_ClearFlags = flags; }
	void SetClearColor(const LinearColor& color) { m_ClearColor = color; }
	void SetClearDepth(float depth) { m_ClearDepth = depth; }
	void SetClearStencil(ushort stencil) { m_ClearStencil = stencil; }

protected:
	int32 m_Width;
	int32 m_Height;

	/// Mipmap
	int32 m_MipLevel;
	int32 m_NumMips;

	/// MSAA
	int32 m_Multisample;

	/// Clear
	uint32 m_ClearFlags;
	LinearColor m_ClearColor;
	float m_ClearDepth;
	ushort m_ClearStencil;
};
