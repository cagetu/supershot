// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshadermodule.h"

//------------------------------------------------------------------
/**	@class	IGraphicShaderCompiler
	@desc	셰이더 컴파일러의 인터페이스 객체
 
            그래픽 API에 맞게 셰이더를 컴파일 해준다.
            (HLSL -> Spir-V or GLSL -> Spir-V)
*/
//------------------------------------------------------------------
class IGraphicShaderCompiler
{
	__DeclareRootRtti(IGraphicShaderCompiler);
public:
	IGraphicShaderCompiler();
	virtual ~IGraphicShaderCompiler();

	virtual IGraphicShaderModule* Compile(const ShaderDescription& desc) = 0;
};


#define DXC_PATH	TEXT("dxc.exe")
#define SHADER_PATH	TEXT("C:\\supershot\\Program\\Source\\Sample\\")
