// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class GfxReferenceCount
{
public:
	GfxReferenceCount()
	{
	}
	virtual ~GfxReferenceCount()
	{
		ASSERT(NumRefs.GetCount() == 0);
	}
	uint32 AddRef() const
	{
		int32 NewValue = NumRefs.Increment();
		ASSERT(NewValue > 0);
		return uint32(NewValue);
	}
	uint32 Release() const
	{
		int32 NewValue = NumRefs.Decrement();
		if (NewValue == 0)
		{
			delete this;
		}
		ASSERT(NewValue >= 0);
		return uint32(NewValue);
	}
	uint32 GetRefCount() const
	{
		int32 CurrentValue = NumRefs.GetCount();
		ASSERT(CurrentValue >= 0);
		return uint32(CurrentValue);
	}

private:
	mutable ThreadSafeCounter NumRefs;
};

//------------------------------------------------------------------
/**	@class	GraphicResource
	@desc	그래픽 리소스 최상위 객체

			- Texture, Shader, ....

			취합해서 통계에 사용하자...
*/
//------------------------------------------------------------------
class IGraphicResource : public GfxReferenceCount
{
	__DeclareRootRtti(IGraphicResource);
public:
	IGraphicResource();
	virtual ~IGraphicResource();

protected:
	int64 m_ID;
};
