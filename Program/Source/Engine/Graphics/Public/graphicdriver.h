// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdevice.h"

class Viewport;
class IGraphicDevice;
class IGraphicShaderModule;
class IGraphicShaderCompiler;
struct ShaderDescription;
struct LinearColor;

//------------------------------------------------------------------
/**	@class	GraphicDriver
	@desc	그래픽 API에 대한 인터페이스 객체이다.
				- VulkanDriver
				- OpenGLESDriver
				- D3D11Driver
				- D3D12Driver
*/
//------------------------------------------------------------------
class IGraphicDriver
{
	__DeclareRootRtti(IGraphicDriver);
public:
	IGraphicDriver() = default;
	virtual ~IGraphicDriver() = default;

	virtual bool Create(const WindowHandle& InDisplay) = 0;
	virtual void Destroy() = 0;

	virtual void Present() = 0;

	virtual void BeginFrame(Viewport* viewport) = 0;
	virtual void EndFrame() = 0;

	virtual void Clear(const LinearColor& color, float depth, unsigned long clearFlags, unsigned char stencil) = 0;

	virtual IGraphicDevice* CurrentDevice() = 0;

	virtual IGraphicShaderCompiler* GetShaderCompiler() = 0;

	virtual IGraphicShaderModule* CompileShader(const ShaderDescription& desc) = 0;
};
