// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

enum class COMMANDQUEUE_TYPE : uint8
{
	DEFAULT = 0,
	COPY,
	ASYNC
};

enum class COMMANDLIST_TYPE : uint8
{
	DIRECT = 0,
	BUNDLE,
	COMPUTE,
	COPY,
	SPARSE,

	NUM
};

enum class COMMANDQUEUE_PRIORITY : uint8
{
	NORMAL = 0,
	HIGH,
	VERY_HIGH
};
