// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphictypes.h"

class Viewport;

//------------------------------------------------------------------
/**	@class	RenderDevice
	@desc	그래픽 카드 (Adaptor)에 대한 클래스. 
			그래픽카드에 대한 Capability 등 정보를 가진다.
*/
//------------------------------------------------------------------
class IGraphicDevice
{
	__DeclareRootRtti(IGraphicDevice);
	__DeclareReference(IGraphicDevice);
public:
	IGraphicDevice();
	virtual ~IGraphicDevice();
};
