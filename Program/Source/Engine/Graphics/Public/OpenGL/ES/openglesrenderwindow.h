// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicrenderwindow.h"

class OpenGLESDriver;
class OpenGLESSwapchain;

class OpenGLESRenderWindow : public IGraphicRenderWindow
{
	__DeclareRtti;
public:
	OpenGLESRenderWindow();
	virtual ~OpenGLESRenderWindow();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8, int32 multiSamples = 1, bool bUseDepthBuffer = true) override;
	virtual void Destroy() override;

	virtual void Resize(const WindowHandle& windowHandle) override;

	void Begin();
	void End();

	void Present();

public:
	void SetColorBuffer(int32 colorBufferHandle);
	void SetDepthBuffer(int32 depthBufferHandle);

	int32 GetColorBuffer() const;
	int32 GetDepthBuffer() const;

public:
#ifdef WIN32
	EGLDisplay m_eglDisplay;
	EGLContext m_eglContext;
	EGLSurface m_eglSurface;
#endif

private:
	OpenGLESSwapchain* m_SwapChain;

	int32 m_DepthBufferId;

	enum
	{
		_DIRTY_COLORBUFFER = 1 << 1,
		_DIRTY_DEPTHBUFFER = 1 << 2,
	};
	int32 m_DirtyFlags;
};
