// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicswapchain.h"

class OpenGLESDriver;

class OpenGLESSwapchain : public IGraphicSwapchain
{
	__DeclareRtti;
public:
	OpenGLESSwapchain();
	virtual ~OpenGLESSwapchain();

	virtual bool Create(const WindowHandle& windowHandle, FORMAT_TYPE format = FMT_R8G8B8A8) override;
	virtual void Destroy() override;

	virtual bool Resize(const WindowHandle& windowHandle) override;

	void Begin();
	void End();

	void Present();

public:
	void SetColorBuffer(int32 colorBufferHandle);
	int32 GetColorBuffer() const { return m_ColorBufferId; }

private:
	int32 m_ColorBufferId;

	enum
	{
		_DIRTY_COLORBUFFER = 1<<1,
	};
	int32 m_DirtyFlags;
};
