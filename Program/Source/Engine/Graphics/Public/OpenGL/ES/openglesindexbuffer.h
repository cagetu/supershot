// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicindexbuffer.h"

class OpenGLESIndexBuffer : public IGraphicIndexBuffer
{
	__DeclareRtti;
public:
	OpenGLESIndexBuffer();
	virtual ~OpenGLESIndexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;

private:
	GLuint m_Handle;
	int32 m_BufferSize;
};
