// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshadercompiler.h"

//------------------------------------------------------------------
/**	@class	OpenGLESShaderCompiler
	@desc	OpenGLES 셰이더 컴파일러

			hlsl -> glsl 컴파일
*/
//------------------------------------------------------------------
class OpenGLESShaderCompiler : public IGraphicShaderCompiler
{
	__DeclareRtti;
public:
	OpenGLESShaderCompiler();
	virtual ~OpenGLESShaderCompiler();

	virtual IGraphicShaderModule* Compile(const ShaderDescription& desc) override;

private:
};
