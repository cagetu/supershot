// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshader.h"

//------------------------------------------------------------------
/**	@class	Shader
	@desc	셰이더 객체
*/
//------------------------------------------------------------------
class OpenGLESShader : public IGraphicShader
{
	__DeclareRtti;
public:
	OpenGLESShader();
	virtual ~OpenGLESShader();

private:

};
