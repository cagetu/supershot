// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicvertexbuffer.h"

class OpenGLESVertexBuffer : public IGraphicVertexBuffer
{
	__DeclareRtti;
public:
	OpenGLESVertexBuffer();
	virtual ~OpenGLESVertexBuffer();

	virtual bool Create(int32 bufferSize) override;
	virtual void Destroy() override;

private:
	GLuint m_Handle;
	int32 m_BufferSize;
};
