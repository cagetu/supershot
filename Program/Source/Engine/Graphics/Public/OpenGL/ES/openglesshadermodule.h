// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicshadermodule.h"

//------------------------------------------------------------------
/**	@class	OpenGLESShaderModule
	@desc	SpirV 셰이더 코드에 대한 정보를 가진다.
			SpirV-Cross 라이브러리를 이용해서 정보를 얻어온다.
*/
//------------------------------------------------------------------
class OpenGLESShaderModule : public IGraphicShaderModule
{
	__DeclareRtti;
public:
	OpenGLESShaderModule();
	virtual ~OpenGLESShaderModule();

	virtual bool Create(ShaderType::TYPE type, uint32 elementSize, const void* elementData) override;
	virtual void Destroy() override;
};

