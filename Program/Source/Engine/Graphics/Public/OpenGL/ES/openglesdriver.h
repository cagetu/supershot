// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdriver.h"
#include "OpenGL/ES/openglesdevice.h"

class OpenGLESRenderWindow;
class OpenGLESShaderCompiler;
class IGraphicShaderModule;

class OpenGLESDriver : public IGraphicDriver
{
	__DeclareRtti;
public:
	OpenGLESDriver();
	virtual ~OpenGLESDriver();

	virtual bool Create(const WindowHandle& windowHandle) override;
	virtual void Destroy() override;

	virtual void BeginFrame(Viewport* viewport) override;
	virtual void EndFrame() override;

	virtual void Present() override;

	virtual void Clear(const Color& color, float depth, unsigned long clearFlags, unsigned char stencil) override;

	virtual IGraphicDevice* CurrentDevice() override { return m_Device; }
	virtual IGraphicShaderCompiler* GetShaderCompiler() override;
	virtual IGraphicShaderModule* CompileShader(const ShaderDescription& desc) override;

private:
	friend class OpenGLESRenderWindow;
	friend class OpenGLESDevice;

	void SetViewport(const Viewport* viewport);

	OpenGLESDevice* m_Device;
	OpenGLESRenderWindow* m_RenderWindow;
	OpenGLESShaderCompiler* m_ShaderCompiler;

	enum
	{
		_SHADERBINARY = 1 << 1,
	};
	unsigned long m_Flags;
};

