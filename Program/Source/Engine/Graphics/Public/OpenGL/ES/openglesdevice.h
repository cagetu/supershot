// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdevice.h"
#if defined(WIN32) || defined(TARGET_OS_ANDROID)
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2platform.h>
#include <GLES3/gl3.h>
#include <GLES3/gl31.h>
#include <GLES3/gl3platform.h>
#endif

class OpenGLESDriver;

class OpenGLESDevice : public IGraphicDevice
{
	__DeclareRtti;
public:
	OpenGLESDevice();
	virtual ~OpenGLESDevice();

	void SetViewport(int32 x, int32 y, int32 width, int32 height, float minDepth = 0.0f, float maxDepth = 1.0f);

	bool CheckCapacity();

private:
	int32 m_MaxVertexAttribs;
};