// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphicdriver.h"

class Viewport;
class IGraphicShaderModule;
struct LinearColor;

//------------------------------------------------------------------
/**	@class	Gfx
	@desc	Graphics API 에 대한 인터페이스 클래스
			Low Level 3D API
*/
//------------------------------------------------------------------
class Gfx
{
public:
	// Gfx API Type
	enum TYPE
	{
		_NONE = 0,
		_D3D11,
		_D3D12,
		_OPENGLES,
		_VULKAN,
	};

public:
	static void _Startup(TYPE type);
	static void _Shutdown();

	static bool Create(const WindowHandle& displayHandle);
	static void Destroy();

	static void BeginFrame(Viewport* viewport);
	static void EndFrame();

	static void Clear(const LinearColor& color, float depth, unsigned long clearFlags = CLEAR_DEPTH, unsigned char stencil = 0);
	static void Present();

	static TYPE DriverType();
	static IGraphicDriver* Driver();

	static IGraphicShaderModule* CompileShader(const ShaderDescription& desc);
};
