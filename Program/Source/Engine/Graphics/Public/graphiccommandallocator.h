// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "graphiccommandtypes.h"

//------------------------------------------------------------------
/**	@class	Base Command Allocator
	@desc
*/
//------------------------------------------------------------------
class IGraphicCommandAllocator : public Noncopyable, public RefCount
{
	__DeclareRootRtti(IGraphicCommandAllocator);
public:
	IGraphicCommandAllocator(COMMANDLIST_TYPE commandQueueType);
	virtual ~IGraphicCommandAllocator();

	COMMANDLIST_TYPE GetCommandQueueType() const { return m_CommandQueueType; }

	virtual void Reset() abstract;

protected:
	COMMANDLIST_TYPE m_CommandQueueType;
};
