// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	@class	Engine
	@desc	Engine 클래스
*/
//------------------------------------------------------------------
class Engine
{
public:
	Engine();
	~Engine();

	bool Initialize();
	void Shutdown();
};