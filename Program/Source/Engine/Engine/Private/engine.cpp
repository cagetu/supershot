// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "foundation.h"
#include "graphic.h"
#include "engine.h"
#include "crc.h"
#include "name.h"
#include "assignRegistry.h"

Engine::Engine()
{
}

Engine::~Engine()
{
}

bool Engine::Initialize()
{
	Crc::Init();
	Name::_Startup();
	Log::_Startup();
	AssignRegistry::_Startup();;

	return true;
}

void Engine::Shutdown()
{
	Gfx::_Shutdown();
	Log::_Shutdown();
	Name::_Shutdown();
	AssignRegistry::_Shutdown();
}

