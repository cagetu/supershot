// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "graphicheader.h"
#include "graphicshader.h"
#include "material.h"
#include "materialasset.h"

//------------------------------------------------------------------
//------------------------------------------------------------------
Material::Material(MaterialResource* parent, const Array<MaterialProperty>& properties, const ShaderFeature::MASK& featureFlags)
	: m_Parent(parent)
	, m_Properties(properties)
	, m_FeatureFlags(featureFlags)
{
}
Material::~Material()
{
}

const ShaderFeature::MASK& Material::GetFeature() const
{
	return m_FeatureFlags;
}
