// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "materialtypes.h"

class MaterialResource;
class MaterialInstance;
class IGraphicShader;

//------------------------------------------------------------------
/** @class  Material
	@desc   필요한 기능에 따라 생성된 머터리얼 셰이더
			MaterialResource을 통해서 생성된다.
 */
 //------------------------------------------------------------------
class Material
{
public:
	Material(MaterialResource* parent, const Array<MaterialProperty>& properties, const ShaderFeature::MASK& featureFlags);
	virtual ~Material();

	const ShaderFeature::MASK& GetFeature() const;

protected:
	MaterialResource* m_Parent;

	Array<MaterialProperty> m_Properties;
	ShaderFeature::MASK m_FeatureFlags;

	// 실제 생성된 셰이더에 대한 객체
	IGraphicShader* m_Shader;

	// 생성된 머터리얼 인스턴스에 대한 관리
};
