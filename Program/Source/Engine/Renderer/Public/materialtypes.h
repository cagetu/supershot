// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

// Properties
struct MaterialProperty
{
	String name;      // name
	String define;    // define
	String value;     // default value

	bool operator==(const MaterialProperty& rhs) const
	{
		if (name != rhs.name)
			return false;
		if (define != rhs.define)
			return false;
		if (value != rhs.value)
			return false;
		return true;
	}
};

// Shaders
struct MaterialShader
{
	ShaderType::TYPE type;      // kind of shader
	String name;                // name
	String value;               // file name

	bool operator==(const MaterialShader& rhs) const
	{
		if (type != rhs.type)
			return false;
		if (name != rhs.name)
			return false;
		if (value != rhs.value)
			return false;
		return true;
	}
};

// RenderStates
struct MaterialRenderState
{
	bool operator==(const MaterialRenderState& rhs) const
	{
		return true;
	}
};
