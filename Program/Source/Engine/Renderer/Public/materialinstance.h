// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class Material;

//------------------------------------------------------------------
/** @class  MaterialInstance
    @desc   실제로 사용하게될 머터리얼 인스턴스 클래스.
 */
//------------------------------------------------------------------
class MaterialInstance
{
public:
    MaterialInstance(Material* material);
    virtual ~MaterialInstance();

protected:
	Material* m_Material;
};
