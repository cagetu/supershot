// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "graphicheader.h"
#include "shaderasset.h"

//------------------------------------------------------------------
//------------------------------------------------------------------
__ImplementRtti(Gfx, ShaderResource, IAsset);

ShaderResource::ShaderResource()
{
}
ShaderResource::ShaderResource(const String& name, ShaderType::TYPE type, uint8* codeBytes, int32 codeSize)
	: m_Name(name)
	, m_Type(type)
	, m_CodeBytes(codeBytes)
	, m_CodeSize(codeSize)
{
}
ShaderResource::~ShaderResource()
{
	if (m_CodeBytes)
	{
		delete[] m_CodeBytes;
	}
	m_CodeBytes = nullptr;
}
//------------------------------------------------------------------
//------------------------------------------------------------------
Map<String, ShaderResource*> ShaderLoader::m_ShaderResources;

void ShaderLoader::Initialize()
{
}

void ShaderLoader::Shutdown()
{
	auto itend = m_ShaderResources.end();
	for (auto it = m_ShaderResources.begin(); it != itend; it++)
		delete it->value;
	m_ShaderResources.Clear();
}

ShaderResource* ShaderLoader::Load(const TCHAR* path, ShaderType::TYPE shaderType)
{
	IndexT index = m_ShaderResources.FindIndex(path);
	if (index != INVALID_INDEX)
	{
		return m_ShaderResources.ValueAt(index);
	}

	int32 codeSize = 0;
	uint8* codeBytes = nullptr;
	{
		FileStream fileStream(path);
		int32 codeSize = fileStream.Size();
		CHECK(codeSize > 0, TEXT("codeSize == 0"));
		uint8* codeBytes = new uint8[codeSize];
		fileStream.ReadBytes(codeBytes, codeSize);
		fileStream.Close();
	}

	ShaderResource* resource = new ShaderResource(path, shaderType, codeBytes, codeSize);
	m_ShaderResources.Insert(path, resource);
	return resource;
}

//ShaderResource* ShaderLoader::Load(const TCHAR* path, ShaderType::TYPE shaderType, uint8* codeBytes, int32 codeSize)
//{
//	IndexT index = m_ShaderResources.FindIndex(path);
//	if (index != INVALID_INDEX)
//	{
//		return m_ShaderResources.ValueAt(index);
//	}
//
//	ShaderResource* resource = new ShaderResource(path, shaderType, codeBytes, codeSize);
//	m_ShaderResources.Insert(path, resource);
//	return resource;
//}

//ShaderResource* ShaderLoader::FindShaderResource(const TCHAR* path)
//{
//	IndexT index = m_ShaderResources.FindIndex(path);
//	if (index != INVALID_INDEX)
//	{
//		return m_ShaderResources.ValueAt(index);
//	}
//
//	return nullptr;
//}
