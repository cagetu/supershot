// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#include "graphicheader.h"
#include "graphicshader.h"
#include "graphic.h"
#include "material.h"
#include "materialasset.h"

//------------------------------------------------------------------
//------------------------------------------------------------------
__ImplementRtti(Gfx, MaterialResource, IAsset);

MaterialResource::MaterialResource()
{
    
}
MaterialResource::~MaterialResource()
{
	Clear();
}

Material* MaterialResource::Create(const Array<String>& propertyNames)
{
	Array<MaterialProperty> properties;
	Array<String> defines;
	for (SizeT index = 0; index < propertyNames.Num(); index++)
	{
		const String& name = propertyNames[index];

		IndexT elementID = m_Properties.FindIndex(name);
		if (elementID != INVALID_INDEX)
		{
			properties.Add(m_Properties.ValueAt(elementID));
			defines.Add(m_Properties.ValueAt(elementID).define);
		}
	}
	ShaderFeature::MASK shaderMask = ShaderFeature::GetShaderMask(defines);

	for (SizeT index = 0; index < m_Materials.Num(); index++)
	{
		if (m_Materials[index]->GetFeature() == shaderMask)
		{
			return m_Materials[index];
		}
	}

	Material* newMaterial = new Material(this, properties, shaderMask);
	
	// 1. 셰이더 컴파일
	// 2. 셰이더 생성

	m_Materials.Add(newMaterial);

	return newMaterial;
}

void MaterialResource::Destroy(Material* material)
{
	IndexT index = m_Materials.FindIndex(material);
	if (index != INVALID_INDEX)
	{
		delete m_Materials[index];
		m_Materials.RemoveAt(index);
		return;
	}
}

void MaterialResource::Clear()
{
	for (Array<Material*>::iterator it = m_Materials.begin(); it != m_Materials.end(); it++)
		delete (*it);
	m_Materials.Clear();
}

//------------------------------------------------------------------
//------------------------------------------------------------------
Map<String, MaterialResource*> MaterialResources;

void MaterialLoader::Initialize()
{
}

void MaterialLoader::Shutdown()
{
	for (Map<String, MaterialResource*>::iterator it = MaterialResources.begin(); it != MaterialResources.end(); it++)
		delete it->value;
	MaterialResources.Clear();
}

MaterialResource* MaterialLoader::Load(const String& filename)
{
	IndexT index = MaterialResources.FindIndex(filename);
	if (index != INVALID_INDEX)
	{
		return MaterialResources.ValueAt(index);
	}

	MaterialResource* resource = new MaterialResource();

	// Json 정보 파싱해서 MaterialResource 객체 생성

	MaterialResources.Insert(filename, resource);
	return resource;
}

MaterialResource* MaterialLoader::Reload(const String& filename)
{
	return nullptr;
}
