// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

/*
	Asset을 관리하기 위해서 뭔가 필요한 것들을 하자..

	1. AssetRegistry 같은 거?
	2. Asset을 로딩 및 리로딩 같은 시스템
*/
class IAsset
{
	__DeclareRootRtti(IAsset);
public:
	IAsset() {}
	virtual ~IAsset() {}
};
