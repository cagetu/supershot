// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "asset.h"
#include "Container\map.h"

//------------------------------------------------------------------
/** @class  ShaderResource
	@desc   로딩된 셰이더 파일 (GLSL / HLSL)
 */
 //------------------------------------------------------------------
class ShaderResource : public IAsset
{
	__DeclareRtti;
public:
	ShaderResource();
	ShaderResource(const String& name, ShaderType::TYPE type, uint8* codeBytes, int32 codeSize);
	virtual ~ShaderResource();

	const String& Name() const { return m_Name; }
	ShaderType::TYPE Type() const { return m_Type; }
	const uint8* CodeBytes() const { return m_CodeBytes; }
	int32 CodeSize() const { return m_CodeSize; }

private:
	String m_Name;				// filePath
	uint8* m_CodeBytes;
	int32 m_CodeSize;
	ShaderType::TYPE m_Type;	// vertex. fragment, compute, ...
};

//------------------------------------------------------------------
/** @class  ShaderSystem
	@desc   Shader File (Shader 리소스) 관리를 주로 한다.
 */
 //------------------------------------------------------------------
class ShaderLoader
{
public:
	static void Initialize();
	static void Shutdown();

	static ShaderResource* Load(const TCHAR* path, ShaderType::TYPE shaderType);
	//static ShaderResource* Load(const TCHAR* path, ShaderType::TYPE shaderType, uint8* codeBytes, int32 codeSize);
	//static ShaderResource* FindShaderResource(const TCHAR* path);

private:
	static Map<String, ShaderResource*> m_ShaderResources;
};

