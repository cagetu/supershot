// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

#include "asset.h"
#include "materialtypes.h"
#include "Container\map.h"

class Material;

//------------------------------------------------------------------
/** @class  Material
    @desc   머터리얼에 대한 정의.
 */
//------------------------------------------------------------------
class MaterialResource : public IAsset
{
	__DeclareRtti;
public:
	MaterialResource();
    virtual ~MaterialResource();
    
    // 머터리얼 생성
    Material* Create(const Array<String>& propertyNames);
	void Destroy(Material* Material);
	void Clear();

private:
	// Property 리스트
    Map<String, MaterialProperty> m_Properties;
	// Shader 리스트
    Map<String, MaterialShader> m_Shaders;

	// 생성된 머터리얼에 대한 관리 (property 단위로 생성)
	Array<Material*> m_Materials;
};

//------------------------------------------------------------------
/** @class  Material
	@desc   필요한 기능에 따라 생성된 머터리얼 셰이더
			MaterialResource을 통해서 생성된다.
 */
 //------------------------------------------------------------------
class MaterialLoader
{
public:
	static void Initialize();
	static void Shutdown();

	static MaterialResource* Load(const String& filename);
	static MaterialResource* Reload(const String& filename);

private:
	static Map<String, MaterialResource*> MaterialResources;
};
