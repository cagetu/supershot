precision mediump float;

/// INTERNAL DEFINES

#define _FIXED_SPECULA_POW		1	// 고정된 Specular Pow 값 사용
#define _HALFLAMBERT			0	// Half Lambert 사용
#define _PER_PIXEL_LIGHTING		1	// Per Pixel 라이팅 사용 (성능상의 이슈~)
#define _TANGENTSPACE_LIGHTING	0	// Tangent Space VS Object Space (Tangent 공간이 조금 연산이 적다.)
#define _ENCODEDEPTH_565		0	// Depth를 565 사이즈로 인코딩할래?! (나중에 사용할지도...)
#define _FASTGAMMACORRECT		1	// 빠른 GAMMACORRECTION 적용
#define _NOBRDF					1	//
#define _NOSKYLIGHTING			0	//
#define _PER_VERTEX_RIMLIGHTING	1	//
 
//#define _READSRGB
//#define _WRITESRGB

#define _PI 3.141592654

#ifdef _READSRGB
 #if (_FASTGAMMACORRECT)
	#define READ_SRGB(rgb) (rgb * rgb)
 #else
	#define READ_SRGB(rgb) pow(rgb, vec3(2.2))
 #endif
#else
	#define READ_SRGB(Color) (Color)
#endif // _READSRGB
#ifdef _WRITESRGB
 #if (_FASTGAMMACORRECT)
	#define WRITE_SRGB(rgb) sqrt(rgb)
 #else
	#define WRITE_SRGB(rgb) pow(rgb, vec3(1.0/2.2))
 #endif
#else
	#define WRITE_SRGB(Color) (Color)
#endif // _WRITESRGB

#define saturate(x)				clamp(x, 0.0, 1.0)

#if defined(_ENCODERGBM) || defined(_DECODERGBM)
vec4 EncodeRGBM(vec3 color)
{
	vec4 rgbm;
	color *= 1.0/6.0;	// max range : 6

	rgbm.a = saturate( max(max(color.r, color.g), max(color.b, 1e-6)) );
	rgbm.a = ceil( rgbm.a * 255.0 ) / 255.0;
	rgbm.rgb = color / rgbm.a;
	return rgbm;
}

vec3 DecodeRGBM(vec4 rgbm)
{
	return 6.0 * rgbm.rgb * rgbm.a;
}
#endif

/// 

#define saturate(x)					clamp(x, 0.0, 1.0)
#define frac(x)						fract(x)
#define _max(x)						max(0.0, x)

#define BlendScreen(base, blend)	(1.0 - (1.0-base) * (1.0-blend))
#define BlendAdd(base, blend)		(base + blend)

/// INPUT

varying mediump vec4 v_Color;
varying mediump vec3 v_EyeDir;
varying mediump vec4 v_Texcoord0;
varying mediump vec3 v_Normal;
#if defined(_USE_NORMALMAP) && !(_TANGENTSPACE_LIGHTING)
varying mediump vec3 v_Tangent;
varying mediump vec3 v_Binormal;
#endif
#if defined(_USE_NORMALMAP) && ((_TANGENTSPACE_LIGHTING) || defined(_USE_PARALLAX))
varying mediump vec3 v_TangentLightDir;
varying mediump vec3 v_TangentHalfVector;
varying mediump vec3 v_TangentEyeDir;
#endif
#ifdef _LOCALPOINTLIGHT
varying highp vec4 v_LocalLightPos;
varying mediump vec4 v_LocalLightColor;
#endif
varying highp vec4 v_WorldPos;

/// VARIABLES
uniform highp vec3 GLOBALLIGHTDIRECTION;
uniform highp vec3 GLOBALLIGHTPOSITION;
uniform mediump vec4 GLOBALLIGHTCOLOR;

#ifdef _USE_AMBIENT
uniform lowp vec4 AMBIENTCOLOR;
#endif
#ifdef _USE_HEMISPHERE
uniform lowp vec4 HEMISPHEREUPPER;
uniform lowp vec4 HEMISPHERELOWER;
uniform lowp vec4 HEMISPHERECOLOR;
#endif
#ifdef _USE_RIMLIGHT
uniform lowp vec4 RIMLIGHTCOLOR;
uniform lowp vec4 RIMLIGHTPARAM;
#endif

#ifdef _USE_MATAMBIENT
uniform lowp vec4 MATAMBIENT;
#endif
#ifdef _USE_MATDIFFUSE
uniform lowp vec4 MATDIFFUSE;
#endif
#if defined(_USE_MATSPECULAR) && defined(_LIGHTING_PS)
uniform lowp vec4 MATSPECULAR;
#endif
#ifdef _USE_EMISSIVE
uniform lowp vec4 MATEMISSIVE;
#endif

#ifdef _SILHOUETTE
uniform lowp vec4 SILHOUETTEPARAM;
#endif
#if defined (_FAKESSS) || defined (_USE_SIMPLESKIN)
uniform lowp vec4 SKINPARAM;
#endif

/// SAMPLERS

#ifdef _USE_DIFFUSEMAP
uniform sampler2D DIFFUSEMAP;
#endif
#ifdef _RAMPSHADE
uniform sampler2D RAMPMAP;
#endif

#ifdef _ALPHATEST
#define ALPHAKILL(a) if (a <= ALPHATESTREF) { discard; }
uniform float ALPHATESTREF;
#else
#define ALPHAKILL(a)
#endif

#ifdef GL_EXT_shader_framebuffer_fetch
	#if (__VERSION__ >= 300)"
		vec4 FramebufferFetchES2() { return gl_FragColor; }
	#else
		vec4 FramebufferFetchES2() { return gl_LastFragData[0]; }
	#endif
#else
	#ifdef GL_ARM_shader_framebuffer_fetch
		vec4 FramebufferFetchES2() { return gl_LastFragColorARM; }
	#else
		vec4 FramebufferFetchES2() { return vec4(65000.0, 65000.0, 65000.0, 65000.0); }
	#endif
#endif

#ifdef GL_ARM_shader_framebuffer_fetch_depth_stencil
	float DepthbufferFetchES2(float OptionalDepth, float C1, float C2) { float w = 1.0f/(gl_LastFragDepthARM*C1-C2); return clamp(w, 0.0f, 65000.0f); }
#else
	float DepthbufferFetchES2(float OptionalDepth, float C1, float C2) { return OptionalDepth; }
#endif

/// SHADER CODES

vec3 GetSkyColor(vec3 Normal, vec3 SkyVector, vec3 DiffuseColor, vec3 LowerColor, vec3 UpperColor)
{
	// HemiSphere SkyLighting
	float NormalContribution = dot(SkyVector, Normal);
	NormalContribution = NormalContribution * 0.5 + 0.5; // -1 ~ 1 => 0 ~ 1

	vec2 weight = NormalContribution * vec2(0.5, -0.5) + vec2(0.5, 0.5);
	weight = weight * weight;

	vec3 UpperLighting = DiffuseColor.rgb * weight.x;
	vec3 LowerLighting = DiffuseColor.rgb * weight.y;

	return UpperLighting * UpperColor + LowerLighting * LowerColor;
}

vec4 EncodeHDR(vec4 Color)
{
	return Color;
}

vec3 DecodeHDR(vec4 Encoded)
{
	return Encoded.rgb;
}

vec3 FrameBufferBlend(vec4 Source)
{
	vec4 Dest = FramebufferFetchES2();
	Dest.rgb = DecodeHDR(Dest);

#if defined(ALPHABLEND_TRANSLUCENT)
	return (Source.rgb*Source.a) + (Dest.rgb*(1.0 - Source.a));
#elif defined(ALPHABLEND_ADD)
	return Source.rgb + Dest.rgb;
#elif defined(ALPHALBNED_ALPHAEDADD)
	return (Srouce.rgb*Source.a) + Dest.rgb;
#elif defined(ALPHABLEND_MULTIPLY)
	return Source.rgb * Dest.rgb;
#elif defined(ALPHABLEND_INVMULTIPLY)
	return (1.0 - Source.rgb) * Dest.rgb;
#else
	return Source.rgb;
#endif
}

void main(void)
{
	vec4 OUT = vec4(1, 1, 0, 1);

	vec3 Normal = vec3(0.0, 0.0, 0.0);
#ifdef _USE_NORMAL
	Normal = v_Normal;
#endif
	
	vec2 BaseUV0 = v_Texcoord0.xy;

	vec4 AlbedoColor = vec4(0.0, 0.0, 0.0, 1.0);
#ifdef _USE_DIFFUSEMAP
	AlbedoColor = texture2D(DIFFUSEMAP, BaseUV0);
	AlbedoColor.xyz = READ_SRGB(AlbedoColor.xyz);
	OUT = AlbedoColor;
#endif

	float ndotl = dot(Normal, GLOBALLIGHTDIRECTION);

	vec3 eyeDir = normalize(v_EyeDir);

	vec3 shadeColor = vec3(0.0);

	float df = max(0.0, ndotl);

	vec3 diffuseLightColor = GLOBALLIGHTCOLOR.xyz * GLOBALLIGHTCOLOR.w * df;
	 
	OUT.xyz *= diffuseLightColor;

	shadeColor = diffuseLightColor;

	vec3 envColor = vec3(0.0);
#ifdef _USE_HEMISPHERE
	envColor = GetSkyColor(Normal, vec3(0, 1, 0), HEMISPHERECOLOR.rgb, HEMISPHERELOWER.rgb, HEMISPHEREUPPER.rgb);
	OUT.xyz += envColor * AlbedoColor.xyz;
#endif
#ifdef _USE_AMBIENT
	envColor += AMBIENTCOLOR.rgb;
	OUT.xyz += AMBIENTCOLOR.rgb;
#endif

#ifdef _ENCODERGBM
	OUT = EncodeRGBM(OUT.xyz);
#endif

	//OUT.xyz = WRITE_SRGB(OUT.xyz);
	gl_FragColor = OUT;
}
