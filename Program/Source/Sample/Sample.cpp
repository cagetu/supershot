// Sample.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "Sample.h"

#include "foundation.h"

#include "graphic.h"
#include "viewport.h"
#include "crc.h"
#include "name.h"
#include "assignRegistry.h"
#include "color.h"
#include "commandArgs.h"

#include "Math\vector.h"
#include "Container\map.h"
#include "Container\array.h"
#include "Container\set.h"

#if _USE_EASTL
#include <EASTL/allocator.h>
#include <EASTL/vector.h>
#include <EASTL/map.h>
#include <EASTL/set.h>
#endif

#include <map>

void * operator new [](size_t size, const  char * pName, int flags,
	unsigned debugFlags, const  char * file, int line)
{
	return operator new[](size);
}
void * operator new [](size_t size, size_t alignment, size_t alignmentOffset,
	const  char * pName, int flags, unsigned debugFlags, const  char * file, int line)
{
	// does not support alignment 
	return operator new[](size);
}

void operator delete [](void * ptr, const  char * pName, int flags,
	unsigned debugFlags, const  char * file, int line)
{
	operator delete[](ptr);
}
void operator delete [](void * ptr, size_t alignment, size_t alignmentOffset,
	const char * pName, int flags, unsigned debugFlags, const  char * file, int line)
{
	// does not support alignment 
	operator delete[](ptr);
}

#define MAX_LOADSTRING 100

// 전역 변수:
HINSTANCE hInst;                                // 현재 인스턴스입니다.
WCHAR szTitle[MAX_LOADSTRING];                  // 제목 표시줄 텍스트입니다.
WCHAR szWindowClass[MAX_LOADSTRING];            // 기본 창 클래스 이름입니다.

// 이 코드 모듈에 들어 있는 함수의 정방향 선언입니다.
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	// 전역 문자열을 초기화합니다.
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_SAMPLE, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	Crc::Init();
	Name::_Startup();
	Log::_Startup();
	AssignRegistry::_Startup();

	Log::Register(new LogFile(TEXT("log.txt"), true, true));

	String path;
	AssignRegistry::Register(TEXT("root"), TEXT("C:\\supershot\\"));
	AssignRegistry::Register(TEXT("engine"), TEXT("C:\\supershot\\Program\\Source\\"));
	AssignRegistry::Register(TEXT("shader"), TEXT("C:\\supershot\\Program\\Source\\Shader"));
	AssignRegistry::Register(TEXT("build"), TEXT("C:\\Supershot\\Program\\Source\\Build"));
	AssignRegistry::Register(TEXT("saved"), TEXT("C:\\Supershot\\Program\\Source\\Saved"));
	AssignRegistry::Register(TEXT("dxc"), TEXT("C:\\supershot\\Program\\Source\\ThirdParty\\DXC\\"));

	//Gfx::_Startup(Gfx::_VULKAN);
	//Gfx::_Startup(Gfx::_OPENGLES);
	Gfx::_Startup(Gfx::_D3D12);

    // TODO: 여기에 코드를 입력합니다.
	Array<int32> TEST2 = { 0, 1, 2, 3, 6, 5 };
	TEST2.Reserve(32);
	IndexT index = 0;
	int32 aaa = TEST2[2];
	TEST2.Sort();
	int32 bbb = TEST2[4];
	TEST2 += {5, 6, 7};

	int32 attt = Algorithm::LowerBound(TEST2, 3);

	index = TEST2.AddZeroed();
	int32 a = TEST2[index];

	index = TEST2.AddDefaulted();
	int32 b = TEST2[index];

	Array<Vec4> TEST3;
	TEST3.Push(Vec4::ZERO); // l-value
	TEST3.Push(Vec4(1.0f, 0.0f, 0.0f, 1.0f)); // r-value
	Vec4 item = TEST3.Pop();
	TEST3.RemoveAt(0);

	index = TEST3.Add(Vec4(0.0f, 1.0f, 1.0f, 0.0f));
	index = TEST3.AddUnique(Vec4(0.0f, 1.0f, 1.0f, 0.0f));

	{
		int a = 1, b = 2;
		unicode::string str = unicode::format(L"%d %d", a, b);

		//CHECK(0, TEXT("%d %d"), a, b);

		unicode::string aaa = unicode::convert("aaaaa");
		utf8::string bbb = unicode::convert(L"bbb");

		unicode::string test;
		test.append(L"TEST");
		test.append(unicode::string(L"TEST"));
		test.append(unicode::string(L"AAA"), 3);

		test = L"AAABBBB\t";
		test.trim(L" \t\n\r");

		test.replace(0, 3, L"CCC");

		unicode::string input = L"-c0 v0 -c1 v1 -c2 v2";
		Array<unicode::string> tokens;
		unicode::tokenize(tokens, input.c_str(), L" ");

		unicode::string sub = test.substr(0, 3, true);
		unicode::string sub2 = test.substr(3);
		
		String ttest = TEXT("TEST");
	}

#if _USE_EASTL
	eastl::vector<float> EATEST;
	EATEST.push_back(1.0f);
#endif

	std::map<int32, Vec4> map2;
	map2.insert(std::make_pair(0, Vec4::ZERO));
	map2.insert(std::make_pair(1, Vec4::ONE));
	for (std::map<int32, Vec4>::iterator it = map2.begin(); it != map2.end(); it++)
	{
		auto test = it->second;
		ULOG(Log::_Debug, "%d: %f %f %f", index, test.x, test.y, test.z);
	}

	Map<int32, Vec4> mapTest;
	index = mapTest.Insert(0, Vec4::ZERO);
	index = mapTest.Insert(0, Vec4::ONE);
	//if (index != INVALID_INDEX)
	//{
	//	mapTest.ValueAt(index) =Vec4::ONE;
	//}
	//index = mapTest.Add(1, Vec4(0.0f, 1.0f, 1.0f, 1.0f));
	for (SizeT index = 0; index < mapTest.Num(); index++)
	{
		Vec4 test = mapTest.ValueAt(index);
		ULOG(Log::_Debug, "%d: %f %f %f", index, test.x, test.y, test.z);
	}

	index = mapTest.FindIndex(1);
	bool test = mapTest.Contains(0);

	Set<int32> testSet;
	testSet.Add(1);
	testSet.Add(0);
	testSet.Add(2);

	int32 v0 = testSet.ValueAt(0);
	int32 v1 = testSet.ValueAt(1);
	int32 v3 = testSet.ValueAt(2);

	//Log::Debug(Log::Debug, __FILE__, __FUNCTION__, __LINE__, TEXT("테스트"));

	ULOG(Log::_Debug, "안녕하세요");
	ULOG(Log::_Info, "정보!!!: %d", testSet.Num());
	ULOG(Log::_Warn, "경고!");
	ULOG(Log::_Error, "에러");

	CommandArgs args(TEXT("-name=Jack Shaftoe -age=30 -male"));
	if (args.Num() > 0)
	{
		String test = args.GetString(TEXT("name"));
		ULOG(Log::_Debug, "%s", test.c_str());

		int32 inttest = args.GetInt(TEXT("age"));
		
		bool ddd = args.GetBool(TEXT("male"));

		int32 a = 1;
	}

	{
		Name Test(TEXT("aaasefefef"));

		Name Test2(TEXT("aaasefefef"));

		if (Test == Test2)
		{
			int a = 1;
		}
	}

	//char stackAlloc[2048];
	//StackAllocator alloc(stackAlloc, 2048);

	//base::__string<char, StackAllocator> stackTest;
	//stackTest = "aaaa";

	//utf8::string testaaa;
	//testaaa = "aaaa";
	//testaaa = "bbb";

	//unicode::string teststr;
	//teststr = TEXT("aaaa");

    // 응용 프로그램 초기화를 수행합니다.
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SAMPLE));

    MSG msg;

    // 기본 메시지 루프입니다.
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);

			while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) == 0)
			{
				//if (s_client.Update() == true)
				//	PostQuitMessage(0);

				Viewport vp;
				Gfx::BeginFrame(&vp);
				{
					Gfx::Clear(LinearColor::GREEN, 1.0f, 0x00000001l| 0x00000002l, 0);
				}
				Gfx::EndFrame();
				Gfx::Present();
			}
        }
    }

	Gfx::_Shutdown();
	Log::_Shutdown();
	Name::_Shutdown();
	AssignRegistry::_Shutdown();

    return (int) msg.wParam;
}



//
//  함수: MyRegisterClass()
//
//  목적: 창 클래스를 등록합니다.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SAMPLE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_SAMPLE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   함수: InitInstance(HINSTANCE, int)
//
//   목적: 인스턴스 핸들을 저장하고 주 창을 만듭니다.
//
//   설명:
//
//        이 함수를 통해 인스턴스 핸들을 전역 변수에 저장하고
//        주 프로그램 창을 만든 다음 표시합니다.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 인스턴스 핸들을 전역 변수에 저장합니다.

   if (Gfx::DriverType() == Gfx::_D3D11)
   {
	   unicode::sprintf(szTitle, 100, L"Gfx::_D3D11");
   }
   else if (Gfx::DriverType() == Gfx::_OPENGLES)
   {
	   unicode::sprintf(szTitle, 100, L"Gfx::_OPENGLES");
   }

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   RECT rt;
   GetWindowRect(hWnd, &rt);


   if (!Gfx::Create(WindowHandle(hWnd, hInst, 800, 600, false)))
   {
	   return FALSE;
   }

   ShaderDescription shaderDesc;
   shaderDesc.type = ShaderType::VERTEXSHADER;
   shaderDesc.filename = TEXT("test.hlsl");
   shaderDesc.entryPoint = TEXT("mainVS");

   ShaderDefine& define = shaderDesc.defines.Add();
   define.name = TEXT("_USE_NORMALMAP");

   IGraphicShaderModule* shaderCode = Gfx::CompileShader(shaderDesc);

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  함수: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  목적:  주 창의 메시지를 처리합니다.
//
//  WM_COMMAND  - 응용 프로그램 메뉴를 처리합니다.
//  WM_PAINT    - 주 창을 그립니다.
//  WM_DESTROY  - 종료 메시지를 게시하고 반환합니다.
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 메뉴 선택을 구문 분석합니다.
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 여기에 hdc를 사용하는 그리기 코드를 추가합니다.
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// 정보 대화 상자의 메시지 처리기입니다.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
