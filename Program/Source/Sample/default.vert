precision mediump float;

/// INTERNAL DEFINES
#define _MAX_BONE 59
#define _MAX_BONEWEIGHT 4

#if defined(_USE_NORMALMAP)
	#define _TANGENTSPACE
#endif

/// 

#define saturate(x)			clamp(x, 0.0, 1.0)
#define frac(x)				fract(x)

/// INPUT

attribute highp vec3	POSITION;
#ifdef _USE_NORMAL
attribute mediump vec3	NORMAL;
#endif
#ifdef _SKINNING
attribute mediump vec4	BONEINDICES; 
attribute mediump vec4	BONEWEIGHTS;
#endif
#ifdef _USE_VERTEXCOLOR
attribute mediump vec4	DIFFUSE;
#endif
#ifdef _USE_TEXCOORD
attribute mediump vec2	TEXCOORD0;
#endif
#ifdef _USE_LIGHTMAP
attribute mediump vec2	TEXCOORD1;
#endif
#ifdef _USE_NORMALMAP
attribute mediump vec4	TANGENT;
#endif

/// OUTPUT

varying mediump vec4 v_Color;
varying mediump vec3 v_EyeDir;
varying mediump vec4 v_Texcoord0;
varying mediump vec3 v_Normal;
#if defined(_USE_NORMALMAP) && !(_TANGENTSPACE_LIGHTING)
varying mediump vec3 v_Tangent;
varying mediump vec3 v_Binormal;
#endif
#if defined(_USE_NORMALMAP) && ((_TANGENTSPACE_LIGHTING) || defined(_USE_PARALLAX))
varying mediump vec3 v_TangentLightDir;
varying mediump vec3 v_TangentHalfVector;
varying mediump vec3 v_TangentEyeDir;
#endif
varying highp vec4 v_WorldPos;

/// VARIABLES

uniform highp mat4	WORLDVIEWPROJECTION;
uniform highp mat4	VIEWPROJECTION;
uniform highp mat4	WORLD;
uniform highp mat3	WORLDROTATION;
#ifdef _NEED_WORLDVIEW
uniform highp mat4	WORLDVIEW;
#endif
#ifdef _SKINNING
uniform highp mat4	TMARRAY[_MAX_BONE];
#endif
#ifdef _ALPHAFADE
uniform highp vec4 LOCALBOUND;
#endif

uniform highp vec3 CAMERAPOSITION;
uniform mediump vec4 CAMERAFOV;
uniform mediump vec4 SCREENSIZE;

#ifdef _USE_FOG
uniform lowp vec4 FOGPARAM;
uniform lowp vec4 FOGPOS;
#endif

#define MulMat3(m, v)	vec3(dot(vec3(m[0].x, m[1].x, m[2].x), v), dot(vec3(m[0].y, m[1].y, m[2].y), v), dot(vec3(m[0].z, m[1].z, m[2].z), v))

///
#ifdef _SKINNING
vec4 GetSkinnedPosToWorld(vec4 LocalPos)
{
	mediump ivec4 BoneIndices = ivec4(BONEINDICES.xyzw);

	highp vec4 pos = vec4(0.0);

	pos  = (TMARRAY[BoneIndices.x] * LocalPos) * BONEWEIGHTS.x;
	pos += (TMARRAY[BoneIndices.y] * LocalPos) * BONEWEIGHTS.y;
 #if _MAX_BONEWEIGHT > 2
	pos += (TMARRAY[BoneIndices.z] * LocalPos) * BONEWEIGHTS.z;
	pos += (TMARRAY[BoneIndices.w] * LocalPos) * BONEWEIGHTS.w;
 #endif
	return vec4(pos.xyz, 1.0);
}

vec3 GetSkinnedNormal(vec4 LocalNormal)
{
	mediump ivec4 BoneIndices = ivec4(BONEINDICES.xyzw);

	mediump vec3 norm = vec3(0.0);
	norm  = (TMARRAY[BoneIndices.x] * LocalNormal).xyz * BONEWEIGHTS.x;
	norm += (TMARRAY[BoneIndices.y] * LocalNormal).xyz * BONEWEIGHTS.y;
#if _MAX_BONEWEIGHT > 2
	norm += (TMARRAY[BoneIndices.z] * LocalNormal).xyz * BONEWEIGHTS.z;
	norm += (TMARRAY[BoneIndices.w] * LocalNormal).xyz * BONEWEIGHTS.w;
#endif
	return normalize(norm.xyz);
}
#endif // _SKINNING

vec4 GetVertexTexcoord()
{
	vec4 texcoord = vec4(0);
#ifdef _USE_TEXCOORD
	texcoord = vec4(TEXCOORD0.x, 1.0-TEXCOORD0.y, 0.0, 0.0);
#endif

	return texcoord;
}

/// SHADER CODES

void main(void)
{
	highp vec4 LocalPos = vec4(POSITION, 1.0);
#ifdef _USE_NORMAL
	mediump vec4 LocalNormal = vec4(NORMAL, 0.0);
#endif

	highp vec4 worldPos, worldviewprojPos;
	mediump vec3 worldNormal = vec3(0);

#ifdef _SKINNING
	worldPos = WORLD * GetSkinnedPosToWorld(LocalPos);
	worldviewprojPos = VIEWPROJECTION * worldPos;
 #if defined(_USE_NORMAL)
	worldNormal = GetSkinnedNormal(LocalNormal);
	worldNormal = normalize((WORLD * vec4(worldNormal, 1.0)).xyz);
 #endif
#else
	worldPos = WORLD * LocalPos;
	worldviewprojPos = WORLDVIEWPROJECTION * LocalPos;
  #ifdef _USE_NORMAL
	worldNormal = normalize((WORLD * LocalNormal).xyz);
  #endif
#endif // _SKINNING

	v_WorldPos = worldPos;

	vec3 eyeDir = CAMERAPOSITION - worldPos.xyz;

	gl_Position = worldviewprojPos;
	v_Texcoord0 = GetVertexTexcoord();
	v_Normal = worldNormal;
#ifdef _USE_NORMALMAP
	mediump vec3 worldTangent = normalize((WORLD * vec4(TANGENT.xyz, 0.0)).xyz);
	mediump vec3 worldBinormal = normalize(cross(worldNormal, worldTangent) * TANGENT.w);
  #if !(_TANGENTSPACE_LIGHTING)
	v_Tangent = worldTangent;
	v_Binormal = worldBinormal;
  #endif
#endif // _USE_NORMALMAP
#ifdef _USE_VERTEXCOLOR
    v_Color = DIFFUSE.zyxw;		// ARGB -> ABGR
#else
	v_Color = vec4(1.0);
#endif // _USE_VERTEXCOLOR

	v_EyeDir = normalize(eyeDir);

//// 종류 별로 셰이더 구분.....

#ifdef _NEED_WORLDVIEWPOS
	vec4 worldViewPos = WORLDVIEW * LocalPos;
#endif

#if defined(_USE_NORMALMAP) && ((_TANGENTSPACE_LIGHTING) || defined(_USE_PARALLAX))
	mat3 tangentToWorld = mat3(worldTangent, worldBinormal, worldNormal);
	v_TangentLightDir = normalize(tangentToWorld * -GLOBALLIGHTDIRECTION);
	v_TangentHalfVector = normalize(v_TangentLightDir + v_TangentEyeDir);
	v_TangentEyeDir = normalize(tangentToWorld * v_EyeDir);
#endif

	/// pointlight 계산
#ifdef _LOCALPOINTLIGHT
	v_LocalLightPos = GetLocalLightPos(worldPos, v_Normal);
	v_LocalLightColor = GetLocalLightColor(v_LocalLightPos, worldPos);
#endif

//**************************** FOG ****************************

//**************************** ETC ****************************

//**************************** END ****************************
}
