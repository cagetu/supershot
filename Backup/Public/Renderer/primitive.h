#pragma once

//================================================================
/** Primitive(SubMesh) Class
	@author		cagetu
	@since		2015. 4. 20
	@remarks	
		- 렌더링 최소 단위 기하 객체.
		- Material 단위 하위 객체
		- Primitive 단위로 정렬된다.
*/
//================================================================
class CPrimitive : public RefCount
{
public :
	CPrimitive();
	virtual ~CPrimitive();

	int GetUID() const { return m_UID; }

	void Render(IShader* shader, const CMaterial* material, uint32 renderstate = 0);

public:
	void SetPrimitiveType(PRIMITIVETYPE type);

	void SetVertexBuffer(const Ptr<VertexBuffer>& vb, int numVerts, int offset);
	void SetIndexBuffer(const Ptr<IndexBuffer>& ib, int numFaces, int startIndex = 0, int baseIndex = 0);

	void SetLocalBound(const AABB& bound);
	const AABB& GetLocalBound() const { return m_LocalAABB; }

	uint32 GetFVF() const;

protected:
	int m_UID;

	PRIMITIVETYPE m_PrimitiveType;
	int m_PrimitieveCount;
	int m_StartVertexIndex;
	int m_BaseVertexIndex;

	uint32 m_VertexFormat;
	int m_VertexCount;
	int m_VertexOffset;

	Ptr<VertexBuffer> m_VertexBuffer;
	Ptr<IndexBuffer> m_IndexBuffer;

	AABB m_LocalAABB;
};
