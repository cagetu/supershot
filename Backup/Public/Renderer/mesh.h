#pragma once

class CBoneController;

//================================================================
/** Mesh Class
	@author		cagetu
	@since		2015. 4. 20
	@remarks
		- 파일 단위 객체
		- 하위 Mesh를 가진다.
		- StaticMesh와 SkinnedMesh로 나뉘어야 한다.
		- 애니메이션은 Rigid, Skinned가 모두 되어야 한다.
*/
//================================================================
class CMesh : public RefCount
{
	__DeclareRootRtti(CMesh);
public :
	CMesh(const Ptr<CPrimitive>& primitive);
	virtual ~CMesh();

	virtual void Update(float dt);
	virtual void Render();

public:
	bool IsVisible() const { return true; }

public:
	void SetWorldTM(const Mat4& tm);

	const Mat4& GetWorldTM() const { return m_WorldTM; }
	const AABB& GetLocalBound() const;
	const AABB& GetWorldBound() const;

public:
	//void SetShader(IShader* shader);
	void SetMaterial(CMaterial* material);

protected:
	Mat4 m_WorldTM;
	mutable AABB m_WorldAABB;
	mutable bool m_DirtyWorldBound;

	Ptr<CPrimitive> m_Primitive;

	//Ptr<IShader> m_Shader;
	Ptr<CMaterial> m_Material;

protected:
	enum
	{
		_DUPLICATED = 1 << 2,
		_MERGED = 1 << 3,

		_DIRTY_BOUND = 1 << 5,
		_DIRTY_SPHERE = 1 << 6,
		_SCALE = 1 << 7,

		_SKINNED = 1 << 10,
		_LIGHTMAP = 1 << 11,
		_TRANSLUCENT = 1 << 12,
		_LOCALLIGHT = 1 << 13,
		_FADEALPHA = 1 << 14,
		_TRANSITIONMAP = 1 << 15,

		_HIDE = 1 << 16,

		_SKINNED_CPU = 1 << 20,
	};
	unsigned long m_Flags;
};

class CModelMesh : public CMesh
{
	__DeclareRtti;
public:
	CModelMesh(const Ptr<CPrimitive>& primitive);
	virtual ~CModelMesh();

	void Update(float dt) OVERRIDE;
	void Render() OVERRIDE;

public:
	void	SetBoneController(const Ptr<CBoneController>& controller, int * boneTable, int boneNum);
	void	SetBoneController(const Ptr<CBoneController>& controller, int boneindex);

	int		GetTransforms(Mat4* tmlist, bool includelastTM = false);
	void	StoreTransforms();

	bool	CheckTransforms() const;// { return (m_BoneCount<_MAX_BONE); }

public:
	int *	m_BoneTable;
	int		m_BoneCount;
	int		m_BoneID;

	Ptr<CBoneController> m_BoneController;
};

/*
	같은 메쉬
		1. primitive가 같다.
		2. shader가 같다.
		3. renderstate가 같다.
		4. material value가 같다.
*/