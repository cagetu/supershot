#pragma once

namespace Vulkan
{
	class RenderTargetLayout;
	struct RenderTargetDesc;
}

/*	VulkanDriver
	- VulkanRenderer
*/
class VulkanDriver : public IRenderDriver
{
public:
	VulkanDriver();
	virtual ~VulkanDriver();

	virtual bool Create(const IDisplayContext* displayHandle) OVERRIDE;
	virtual void Destroy() OVERRIDE;

	virtual void Clear(COLOR clearColor, float clearDepth = 1.0f, ushort clearStencil = 0, uint32 clearFlags = CLEAR_COLOR | CLEAR_DEPTH) OVERRIDE;

	virtual void BeginScene(IRenderTarget* rt = NULL, uint32 rs_or = 0, uint32 rs_and = 0, int32 clearFlags = CLEAR_COLOR | CLEAR_DEPTH, COLOR clearColor = 0xffffffff, float clearDepth = 1.0f, ushort clearStencil = 0) OVERRIDE;
	virtual void EndScene() OVERRIDE;

	virtual void Present() OVERRIDE;

	virtual void DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint StartVertex, uint VertexCount, uint32 StartInstance = 1, uint32 InstanceCount = 1, uint32 NumControlPoints = 0) OVERRIDE;
	virtual void DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint BaseVertex, uint StartIndex, uint IndexCount, IndexBuffer* IdxBuffer, uint32 StartInstance = 1, uint32 InstanceCount = 1, uint32 NumControlPoints = 0) OVERRIDE;

public:
	void SetRenderTarget(IRenderTarget* renderTarget) OVERRIDE;
	void DiscardRenderTarget(bool depth, bool stencil, uint32 colors = 0) OVERRIDE;

	void SetViewport(IViewport* viewport) OVERRIDE;

	void SetVertexBuffer(VertexBuffer* vertexBuffer) OVERRIDE;
	void SetIndexBuffer(IndexBuffer* indexBuffer) OVERRIDE;

	void SetShader(IShader* shader, const VertexBuffer* vertexBuffer, const CMaterial* material) OVERRIDE;

	void SetRenderState(uint32 rs) OVERRIDE;

public:
	IViewport* CreateViewport() OVERRIDE;
	void DestroyViewport(IViewport* viewport) OVERRIDE;

	IShader* CreateShader() OVERRIDE;
	void DestroyShader(IShader* shader) OVERRIDE;

	VertexBuffer* CreateVertexBuffer() OVERRIDE;
	void DestroyVertexBuffer(VertexBuffer* vertexBuffer) OVERRIDE;

	IndexBuffer* CreateIndexBuffer() OVERRIDE;
	void DestroyIndexBuffer(IndexBuffer* indexBuffer) OVERRIDE;

public:
	inline VkInstance GetInstance() const { return m_Instance; }
	inline VulkanDevice* GetDevice() const { return m_Device; }

private:
	bool CreateInstance();
	void DestroyInstance();
	void GetInstanceLayersAndExtensions(std::vector<const char*>& instanceExtensions, std::vector<const char*>& instanceLayers);

	bool CreateDevice();
	void DestroyDevice();

	// 그래픽카드 디바이스
	std::vector<VulkanDevice*> m_Devices;

	// 현재 디바이스
	VulkanDevice* m_Device;

	VkInstance m_Instance;

	VulkanViewport* m_Viewport;

private:
	VulkanRenderTarget* m_RenderTarget;

	// BackBuffer
	VulkanRenderTarget* m_BackBuffer;
	//VulkanDepthStencilSurface* m_BackDepthStencil;

private:
	VulkanPipeline* m_Pipeline;

	VulkanShader* m_Shader;
	VulkanVertexBuffer* m_VertexBuffer;
	VulkanIndexBuffer* m_IndexBuffer;

	VulkanRenderState* m_RenderState;
	uint32 m_RenderStateFilter[2];

private:
	VulkanRenderPass* FindOrAddRenderPass(const Vulkan::RenderTargetDesc& RTDesc);
	VulkanFrameBuffer* FindOrAddFrameBuffer(VulkanRenderPass* renderPass, const Vulkan::RenderTargetDesc& RTDesc);

	std::vector<VulkanRenderPass*> m_RenderPasses;
	std::vector<VulkanFrameBuffer*> m_FrameBuffers;

#ifndef _SYNC_CMDBUFFER
	VulkanCmdBuffer* m_ActiveCmdBuffer;
#endif

public:
	void SetRenderPass(VulkanRenderPass* renderPass, int32 SubpassIndex = 0);

	void SetViewportRect(int32 x, int32 y, float width, float height, float minDepth = 0.0f, float maxDepth = 1.0f);
	void SetScissorRect(int32 x, int32 y, int32 width, int32 height);

	void SetPrimitiveTopology(PRIMITIVETYPE topology);
	void SetVertexDeclaration(VulkanVertexDeclaration* vertexDeclaration);

	void SetPatchControlPoints(uint32 controlPoints);

	void SetRasterizerEnable(bool bEnable);
	void SetFillMode(FILLMODE::TYPE type);
	void SetCullMode(CULLMODE::TYPE type);

	void SetDepthTestEnable(bool bEnable);
	void SetDepthWriteEnable(bool bEnable);
	void SetDepthFunc(COMPAREFUNC::TYPE type);

	void SetDepthBoundTest(bool bEnable);
	void SetDepthBound(float minDepthBound, float maxDepthBound);

	void SetBlendEnable(bool bEnable, int32 attacnmentIndex = 0);
	void SetBlendWriteMask(int32 writeMask, int32 attacnmentIndex = 0);
	void SetBlendColorFunc(BLENDFACTOR::TYPE srcFactor, BLENDFACTOR::TYPE destFactor, int32 attacnmentIndex = 0, BLENDFUNC::TYPE mode = BLENDFUNC::_ADD);
	void SetBlendAlphaFunc(BLENDFACTOR::TYPE srcFactor, BLENDFACTOR::TYPE destFactor, int32 attacnmentIndex = 0, BLENDFUNC::TYPE mode = BLENDFUNC::_ADD);

	void SetStencilTestEnable(bool bEnable);
	void SetStencilWriteMask(int32 writeMask, bool bFrontFace = true);
	void SetStencilMode(STENCILFUNC::TYPE failOp, STENCILFUNC::TYPE passOp, STENCILFUNC::TYPE depthFailOp, bool bFrontFace = true);
	void SetStencilFunc(COMPAREFUNC::TYPE compareFunc, int32 compareMask, int32 reference, bool bFrontFace = true);

	void ClearStates();

private:
	// 병렬로 처리하려면 여기에 저장하는 것이 맞는지, 별도로 State 객체가 있어서, 거기에 채워서 넘겨주는 것이 맞는지는 진행하면서 확인해보도록 하자!
	GfxPipelineContext m_GfxPipelineContext;
};
