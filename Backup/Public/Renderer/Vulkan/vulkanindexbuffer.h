#pragma once

class VulkanIndexBuffer : public IndexBuffer, public IVulkanBuffer
{
	__DeclareRtti;
public:
	VulkanIndexBuffer(VulkanDevice* device);
	virtual ~VulkanIndexBuffer();

	bool Create(int32 indexCount, int32 stride = sizeof(unsigned short)) OVERRIDE;
	bool Create(int32 indexCount, void* indexData, int32 stride = sizeof(unsigned short)) OVERRIDE;
	void Destroy() OVERRIDE;

	unsigned char* GetBytes() const OVERRIDE;

	void* Lock(int32 length = 0, int32 lockFlag = 0) OVERRIDE;
	void  Unlock(ushort baseVertIdx = -1) OVERRIDE;

	int32 SetIndices() OVERRIDE;

private:
	bool CreateBuffer(uint32 bufferSize, uint32 usage);

	Vulkan::StagingBuffer* m_StagingBuffer;
};