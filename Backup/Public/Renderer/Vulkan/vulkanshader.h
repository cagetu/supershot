#ifndef __VULKAN_SHADER_H__
#define __VULKAN_SHADER_H__

//
//	Shader (Spirv)의 변수 (Descriptor)에 대한 클래스
//		- DescriptorPool
//		- DescriptorSetLayout
//		- DescriptorSet
//
#define _MAX_BONE		128

class VulkanShaderProgram;
class VulkanShaderResource;

/*
	하나의 셰이더는 조합이 완성된 형태의 셰이더를 의미한다. (Vertex + Fragment + xxx)
	따라서, Shader 내에는 내부적으로 타입 단위로 구분되는 한 단위 셰이더 객체 단위로 정보를 가지고 있다. (UnitShader)
*/
class VulkanShader : public IShader
{
	__DeclareRtti;
public:
	VulkanShader(VulkanDevice* device);
	virtual ~VulkanShader();

	bool Create(const SHADERCODE& shaderCode, ShaderFeature::MASK mask) OVERRIDE;
	void Destroy() OVERRIDE;

	void Begin(const CMaterial* material, const VertexBuffer* vb) OVERRIDE;
	void End() OVERRIDE;

	void UpdateValues(const CMaterial* material) OVERRIDE;
	void ClearValues() OVERRIDE;

	void UpdateVertexAttributes(const VertexBuffer* vertexBuffer) OVERRIDE;
	void ClearVertexAttributes() OVERRIDE;

	bool CompareTo(VulkanShader* other);

public:
	// Shader
	VulkanShaderProgram* GetShaderProgram(int32 type) const { return m_ShaderPrograms[type]; }

	// Vertex Attributes
	const Ptr<VulkanVertexDeclaration>& GetVertexAttributes() const { return m_VertexInput; }	
	// PiplineLayout
	VulkanPipelineLayout* GetPipelineLayout() const { return m_PipelineLayout; }
	// DescriptorSetLayout
	VulkanDescriptorSetsLayout* GetDescriptorSetLayout() const { return m_DescriptorSetsLayout; }
	// DescriptorSets
	VulkanDescriptorSets* GetDescriptorSets() const { return m_DescriptorSets; }

protected:
	void SetValue(uint32 index, const Variant& value) OVERRIDE;

	bool CreateVertexDeclaration(VertexDescription* vertexDesc);
	void DestroyVertexDeclaration();

	// Add Descriptor for DescriptorSetLayout
	void AddDescriptor(int32 descriptorSetIndex, const VkDescriptorSetLayoutBinding& descriptorBinding);

	// WriteDescriptorSets
	void WriteTextureDescriptorSet(uint32 binding, int32 descriptorSetIndex, VkDescriptorImageInfo* imageInfo);
	void WriteRenderTargetDescriptorSet(uint32 binding, int32 descriptorSetIndex, VkDescriptorImageInfo* imageInfo);
	void WriteUniformBufferDescriptorSet(uint32 binding, int32 descriptorSetIndex, VkDescriptorBufferInfo* bufferInfo);
	void UpdateDescriptorSets();

private:
	friend class VulkanDriver;
	friend class VulkanShaderProgram;
	friend class VulkanVertexShaderProgram;

	VulkanDevice* m_Device;

	// Shaders
	VulkanShaderProgram* m_ShaderPrograms[ShaderType::NUM];

	// VertexInput
	Ptr<VulkanVertexDeclaration> m_VertexInput;

	// DescriptorLayout
	VulkanPipelineLayout* m_PipelineLayout;
	VulkanDescriptorSetsLayout* m_DescriptorSetsLayout;

	// DescriptorSets
	VulkanDescriptorSets* m_DescriptorSets;
	std::vector<VkWriteDescriptorSet> m_DescriptorWrites;
};

////////////////////////////////////////////////////////////////////////////////////
class VulkanShaderProgram : public IShaderProgram
{
	__DeclareRtti;
public:
	VulkanShaderProgram(VulkanDevice* Device, VulkanShader* InOwner, ShaderType::MASK shaderTypes);
	virtual ~VulkanShaderProgram();

	virtual bool Create(const unicode::string& filename, ShaderFeature::MASK mask);
	virtual void Destroy();

	virtual void SetValue(uint32 index, const Variant& value) OVERRIDE;
	virtual void UpdateValues(const CMaterial* material) OVERRIDE;
	virtual void ClearValues() OVERRIDE;

	ShaderType::TYPE GetShaderType() const;
	VulkanShaderResource* GetShaderResource() const { return m_ShaderResource; }

	static VulkanShaderProgram* NewObject(VulkanDevice* InDevice, VulkanShader* InOwner, ShaderType::TYPE type);

protected:
	friend class VulkanShader;

	void InitDescriptors(VulkanDevice* InDevice);
	void InitShaderParams(VulkanDevice* InDevice);

	void UpdateDescriptorSets();

	void SetValueInternal(void* mappedPtr, const Variant& value);

	VulkanShader* m_Owner;
	VulkanDevice* m_Device;

	VulkanShaderResource* m_ShaderResource;

	struct UniformBuffer
	{
		// Description
		int32 binding = 0;				// Binding
		int32 descriptorSetIndex = 0;	// DescriptorSetsLayout->GetHandle(descriptorSetIndex);
		bool bDirty = false;

		struct Element
		{
			int32 parentId;	// Parent's Index (UniformBuffer)
			int32 uid;		// Element ID
			uint32 offset;	// offset;
			Variant	value;	// value;
		};

		VkDescriptorBufferInfo bufferInfo;
	};
	UniformBuffer* m_UniformBuffers;
	uint32 m_NumUniformBuffers;
	UniformBuffer::Element* m_UniformParams;
	uint32 m_NumUniformParams;
};

////////////////////////////////////////////////////////////////////////////////////
class VulkanVertexShaderProgram : public VulkanShaderProgram
{
	__DeclareRtti;
public:
	VulkanVertexShaderProgram(VulkanDevice* InDevice, VulkanShader* InOwner);
	virtual ~VulkanVertexShaderProgram();

	virtual bool Create(const unicode::string& filename, ShaderFeature::MASK mask) override;
	virtual void Destroy() override;

	virtual void UpdateVertexAttributes(const VertexBuffer* vertexBuffer) OVERRIDE;
	virtual void ClearVertexAttributes() OVERRIDE;

protected:
	// Vertex Descriptions
	VertexDescription m_VertexLayout;
};

class VulkanFragmentShaderProgram : public VulkanShaderProgram
{
	__DeclareRtti;
public:
	VulkanFragmentShaderProgram(VulkanDevice* InDevice, VulkanShader* InOwner);
	virtual ~VulkanFragmentShaderProgram();
};

extern VkShaderStageFlagBits ShaderTypeToStage(ShaderType::TYPE type);

//class VulkanComputeShader

#endif