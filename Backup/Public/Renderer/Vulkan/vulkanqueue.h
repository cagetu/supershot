#pragma once

class VulkanCmdBuffer;
class VulkanSemaphore;

class VulkanQueue
{
public:
	VulkanQueue(VulkanDevice* device, uint32 familyIndex, uint32 queueIndex);
	~VulkanQueue();

	void Submit(VulkanCmdBuffer* commandBuffer, VulkanSemaphore* signalSemaphore = nullptr, VulkanSemaphore* waitSemaphore = nullptr, VkPipelineStageFlags WaitStageMask = 0);
	//void Flush(VulkanCmdBuffer* commandBuffer, VulkanFence* fence);

	void WaitForIdle();

public:
	inline VkQueue GetHandle() const { return m_Queue; }
	inline uint32 GetFamilyIndex() const { return m_FamilyIndex; }
	inline uint32 GetQueueIndex() const { return m_QueueIndex; }

private:
	VulkanDevice* m_Device;

	uint32 m_FamilyIndex;
	uint32 m_QueueIndex;

	VkQueue m_Queue;
};

/*	VulkanQueue
	- 다수의 쓰레드가 하나의 Queue (혹은 여러 Queue)에 작업을 submit 할 수 있다.
	- Queue는 CommandBuffer들 submissions으로 GPU 작업을 승락한다.
		- submit work / wait idle
	- Queue submission은 queue에 대해 sync primitive를 포함할 수 있다.
		- submit된 작업을 처리하기 전에 대기
		- 이 submission에서 작업을 완료했을 때 signal
	- Queue "Family"는 다른 타입의 작업을 수락할 수 있다.
*/