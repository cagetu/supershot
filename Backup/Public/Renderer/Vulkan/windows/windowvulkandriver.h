#pragma once

//
//
//
class WindowVulkanDriver : public VulkanDriver
{
public:
	WindowVulkanDriver();
	virtual ~WindowVulkanDriver();

	bool Create(const IDisplayContext* displayHandle) OVERRIDE;
	void Destroy() OVERRIDE;
};
