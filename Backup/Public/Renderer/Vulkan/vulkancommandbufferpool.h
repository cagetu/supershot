#pragma once

#define _SYNC_CMDBUFFER

struct PoolCreateFlags
{
	typedef int8 Type;
	enum
	{
		_NONE = 0,
		_TRANSIENT_BIT = 1 << 0,
		_RESET_COMMAND_BUFFER_BIT = 1 << 1,
	};
};

class VulkanCmdBufferPool
{\
public:
	VulkanCmdBufferPool();
	~VulkanCmdBufferPool();

	void Init(VulkanDevice* device, PoolCreateFlags::Type createFlags = PoolCreateFlags::_RESET_COMMAND_BUFFER_BIT);
	void Destroy();

	void Reset();

	// 이용가능한 Command 버퍼를 얻어온다.
	VulkanCmdBuffer* GetActiveCommandBuffer();
	VulkanCmdBuffer* GetUploadCommandBuffer();

	void RefreshFenceStatus();
	void PrepareForNewActiveCommandBuffer();

	void WaitForCmdBuffer(VulkanCmdBuffer* cmdBuffer, float timeOut = 1.0f);

	void SubmitUploadCmdBuffer(bool bWaitForFence);
	void SubmitActiveCmdBuffer(bool bWaitForFence);

	void Initialize(uint32 ImageCount);
	VulkanCmdBuffer* GetCmdBuffer(uint32 index) { return m_CommandBuffers[index]; }

public:
	inline const VkCommandPool GetHandle() const { _ASSERT(m_CommandPool != VK_NULL_HANDLE); return m_CommandPool; }
	
	inline bool CanBeReset() const { return (m_CreateFlags&PoolCreateFlags::_RESET_COMMAND_BUFFER_BIT) ? true : false; }
	inline PoolCreateFlags::Type GetCreateFlags() const { return m_CreateFlags; }

private:
	VulkanCmdBuffer* CreateCmdBuffer();
	void DestroyCmdBuffer(VulkanCmdBuffer* instance);

	std::vector<VulkanCmdBuffer*> m_CommandBuffers;
	VulkanCmdBuffer* m_UploadCmdBuffer;
	VulkanCmdBuffer* m_ActiveCmdBuffer;

	VulkanDevice* m_Device;
	VkCommandPool m_CommandPool;
	PoolCreateFlags::Type m_CreateFlags;
};
