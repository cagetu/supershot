#pragma once

/*	장치에 대한 클래스
	- 물리적 장치 (Physical Device)에 대한 정보를 이용해서, 논리적 장치 핸들을 생성한다.
*/
class VulkanDevice
{
public:
	VulkanDevice(VkPhysicalDevice GPU);
	~VulkanDevice();

public:
	bool Create();

	void PrepareForDestroy();
	void Destroy();

	void WaitForIdle();

public:
	bool IsSupportGPU(VkQueueFlagBits operationType = VK_QUEUE_GRAPHICS_BIT) const;
	bool IsDiscreteGPU() const;

	// Physical Device
	inline const VkPhysicalDevice GetGpuHandle() const { return m_Gpu; }
	inline const VkPhysicalDeviceProperties& GetProperties() const { return m_GpuProps; }
	inline const VkPhysicalDeviceLimits& GetLimits() const { return m_GpuProps.limits; }
	inline const VkPhysicalDeviceFeatures& GetFeatures() const { return m_GpuFeatures; }
	inline const VkQueueFamilyProperties& GetQueueFamilyProps(int32 familyIndex) const { return m_QueueFamilyProps[familyIndex]; }

	// Logical Device
	inline const VkDevice GetHandle() const { return m_Device; }

	// Graphic Queue
	inline VulkanQueue* GetGfxQueue() const { return m_Queue; }

public:
	// Memory & Resource Management
	inline Vulkan::DeviceMemoryPool& MemoryManager() { return m_MemoryManager; }
	inline Vulkan::ResourceHeapPool& ResourceHeapPool() { return m_ResourceHeapManager; }
	inline Vulkan::StagingBufferPool& StagingBufferPool() { return m_StagingBufferManager; }

	inline VulkanGfxPipelineManager* GfxPipelineManager() { return m_GfxPipelineManager; }
	inline VulkanCmdBufferPool& CommandBufferPool() { return m_CmdBufferPool; }

	inline VulkanFencePool& FencePool() { return m_FencePool; }

public:
	VkFormat GetFormat(uint32 format) const;
	const VkComponentMapping& GetFormatCommpoentMapping(uint32 format) const;
	uint32 GetFormatBlockByteSize(uint32 format) const;
	bool IsFormatSupported(VkFormat format) const;

	VkSampleCountFlagBits GetSampleCount(uint32 sampleCount) const;

//~ Context 기능들...
public:
	VulkanDescriptorPool* AllocateDescriptorSets(/*const VkDescriptorSetAllocateInfo& allocateInfo, */const VulkanDescriptorSetsLayout& layout, VkDescriptorSet* OutSets);

	VulkanUniformBufferUploader* GetUniformBufferUploader() { return m_UniformBufferUploader; }
//~ Context 기능들..

private:
	void SetupFormat();

	void SetFormat(uint32 format, VkFormat vkFormat, const VkComponentMapping& vkComponent);

private:
	void GetDeviceLayerAndExtensions(std::vector<const char*>& deviceExtensions, std::vector<const char*>& deviceLayers, bool& outDebugMarkers);

	VkPhysicalDevice m_Gpu;
	VkPhysicalDeviceProperties m_GpuProps;
	VkPhysicalDeviceFeatures m_GpuFeatures;

	Vulkan::DeviceMemoryPool m_MemoryManager;
	Vulkan::ResourceHeapPool m_ResourceHeapManager;
	Vulkan::StagingBufferPool m_StagingBufferManager;

	VulkanFencePool m_FencePool;
	VulkanCmdBufferPool m_CmdBufferPool;
	VulkanGfxPipelineManager* m_GfxPipelineManager;

	std::vector<VulkanDescriptorPool*> m_DescriptorPools;

	VkFormatProperties m_FormatProperties[VK_FORMAT_RANGE_SIZE];
	mutable std::map<VkFormat, VkFormatProperties> m_ExtensionFormatProperties;

	std::vector<VkQueueFamilyProperties> m_QueueFamilyProps;
	uint32 m_QueueFlags;

	// Graphics Queue, 여기에 CommandBuffer를 Submit할 것이다.
	VulkanQueue* m_Queue;

	VulkanUniformBufferUploader* m_UniformBufferUploader;

#if VULKAN_ENABLE_DRAW_MARKERS
	PFN_vkCmdDebugMarkerBeginEXT m_CmdDebugMarkerBegin;
	PFN_vkCmdDebugMarkerEndEXT m_CmdDebugMarkerEnd;
	PFN_vkDebugMarkerSetObjectNameEXT m_DebugMarkerSetObjectName;
#endif

	VkDevice m_Device;

public:
	static void CheckResult(VkResult result, const char* func, const char* file, const int line, int level);
};

// Macro to check and display Vulkan return results
#define VK_RESULT(func) { const VkResult res = func; if (res != VK_SUCCESS) { VulkanDevice::CheckResult(res, #func, __FILE__, __LINE__, 0); }}
#define VK_RESULT_EXPAND(func) { const VkResult res = func; if (res < VK_SUCCESS) { VulkanDevice::CheckResult(res, #func, __FILE__, __LINE__, 0); }}
#define VK_RESULT_ASSERT(func) { const VkResult res = func;	if (res != VK_SUCCESS) { VulkanDevice::CheckResult(res, #func, __FILE__, __LINE__, 1); }}
