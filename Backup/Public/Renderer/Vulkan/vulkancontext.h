#pragma once

class VulkanContext : public IRenderContext
{
public:
	VulkanContext(VulkanDevice* inDevice);
	virtual ~VulkanContext();

	virtual void SetVertexBuffer(VertexBuffer* vertexbuffer) OVERRIDE;
	virtual void SetIndexBuffer(IndexBuffer* indexbuffer) OVERRIDE;

	virtual void SetRenderTarget(IRenderTarget* renderTarget) OVERRIDE;

	virtual void SetShader(IShader* shader, const VertexBuffer* vb, const CMaterial* material) OVERRIDE;

	virtual void SetRenderState(uint32 rs) OVERRIDE;

	virtual void DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint32 StartVertex, uint32 PrimitiveCount) OVERRIDE;
	virtual void DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint32 BaseVertex, uint32 StartIndex, uint32 PrimitiveCount, IndexBuffer* IdxBuffer) OVERRIDE;

private:
	VulkanDevice* m_Device;
};
