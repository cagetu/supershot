#pragma once

#include "vulkanconfig.h"

/** How many back buffers to cycle through */
#define NUM_RENDER_BUFFERS 2

#ifndef VK_PROTOTYPES
#define VK_PROTOTYPES	1
#endif

#if TARGET_OS_WINDOWS
#define VK_USE_PLATFORM_WIN32_KHR 1
#endif
#if TARGET_OS_ANDROID
#define VK_USE_TARGET_OS_ANDROID_KHR 1
#endif

class VulkanDriver;
class VulkanDevice;
class VulkanViewport;
class VulkanRenderPass;
class VulkanFrameBuffer;
class VulkanCmdBuffer;
class VulkanRenderTarget;
class VulkanRenderTexture2D;
class VulkanDepthStencilSurface;
class VulkanBackBuffer;
class VulkanVertexBuffer;
class VulkanIndexBuffer;
class VulkanPipeline;
class VulkanGfxPipeline;
class VulkanComputePipeline;
class VulkanPipelineManager;
class VulkanGfxPipelineManager;
class VulkanComputePipelineManager;
class VulkanRenderState;
class VulkanUniformBufferUploader;
class VulkanVertexDeclaration;
class VulkanShader;
class VulkanShaderProgram;

#include "vulkanloader.h"	// �ֻ���!!
#include "vulkancaps.h"
#include "vulkantransform.h"

#include "vulkansync.h"
#include "vulkanbarrier.h"

#include "vulkanqueue.h"
#include "vulkancommandbuffer.h"
#include "vulkancommandbufferpool.h"
#include "vulkandescriptor.h"
#include "vulkanswapchain.h"
#include "vulkanpipelinestate.h"
#include "vulkanpipeline.h"
#include "vulkanmemory.h"
#include "vulkanbuffer.h"
#include "vulkandevice.h"

#include "vulkandriver.h"

#if defined(TARGET_OS_WINDOWS)
	#include "Windows/windowvulkan.h"
#elif defined(TARGET_OS_ANDROID)
	#include "Android/androidvulkan.h"
#elif defined(TARGET_OS_IPHONE)
	#include "iOS/iosvulkan.h"
#endif

#include "vulkanresource.h"
#include "vulkanrenderpass.h"
#include "vulkanframebuffer.h"

#include "vulkanviewport.h"

#include "vulkantexture.h"
#include "vulkansurface.h"
#include "vulkanrendertarget.h"

#include "vulkanvertexbuffer.h"
#include "vulkanindexbuffer.h"
#include "vulkanuniformbuffer.h"

#include "vulkanvertexformat.h"
#include "vulkanvertexdeclarataion.h"

#include "vulkanshaderresource.h"
#include "vulkanshadercompiler.h"
#include "vulkanshader.h"

#include "vulkanrenderstate.h"
#include "vulkanpipelinegraphics.h"
#include "vulkanpipelinecompute.h"
