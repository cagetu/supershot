#pragma once

/*	Pipeline
	- 파이프라인은 Setup 시간에 validation이나 shader recompile과 같은 렌더링 타임에 불필요한 비싼 연산들에 대한 충분한 정보를 이용하여 재컴파일 하도록 설계되어 있다.
	- 즉, layout들과 state들은 pipeline 내에서 변경될 수 없다.
	- 혹시 다른 타입의 상태들을 가지고 렌더링 할 필요가 있다면, 다른 pipeline 객체가 필요할 것이다.
	- static state와 dynamic state를 구분해야 한다.

	- Compute / Graphic Pipeline이 있다.
*/
class VulkanPipeline : public IRenderResource
{
	__DeclareRtti;
public:
	VulkanPipeline(VulkanDevice* device, int32 pipelineIndex = 0);
	virtual ~VulkanPipeline();

	virtual VkPipelineBindPoint GetType() const ABSTRACT;

	inline VkPipeline GetHandle() const { return m_Handle; }
	int32 GetIndex() const { return m_Index; }

protected:
	VulkanDevice* m_Device;

	VkPipeline m_Handle;
	int32 m_Index;
};

class VulkanPipelineManager
{
public:
	virtual VkPipelineBindPoint GetType() const ABSTRACT;

	virtual void Init(VulkanDevice* device);
	virtual void Destroy();

	virtual bool Load(const unicode::string& filename);
	virtual bool Save(const unicode::string& filename);

protected:
	VulkanPipelineManager();
	virtual ~VulkanPipelineManager();

	struct PipelineCache
	{
		PipelineCache()
			: pipelineCache(VK_NULL_HANDLE) {}

		void Create(VulkanDevice* device, void* initialData = nullptr, uint32 initialDataSize = 0);
		void Destroy(VulkanDevice* device);
		void Merge(VulkanDevice* device);
		void* GetData(VulkanDevice* device);
		bool SaveToFile(const char* filename);

		VkPipelineCache pipelineCache;
	};
	PipelineCache m_PipelineCache;

	VulkanDevice* m_Device;
	friend class VulkanDevice;
};