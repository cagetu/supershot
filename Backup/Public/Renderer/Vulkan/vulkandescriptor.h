#pragma once

class VulkanDescriptorPool;
class VulkanDescriptorSetsLayout;


/*---------------------------------------------------------------------------------------------------------------------------------
여러 개의 DescriptorSet을 한번에 Binding 하기 Layout

	DescriptorSetsLayout
	- DescriptorSetLayout
	- DescriptorSetLayout
	- DescriptorSetLayout
	-> DescriptorSet을 만든다.
/*--------------------------------------------------------------------------------------------------------------------------------*/
class VulkanDescriptorSetsLayout
{
public:
	VulkanDescriptorSetsLayout(VulkanDevice* device);
	~VulkanDescriptorSetsLayout();

	inline const std::vector<VkDescriptorSetLayout>& GetHandles() const { return m_Handles; }
	inline const VkDescriptorSetLayout GetHandle(uint32 index) const { return m_Handles[index]; }

	inline uint32 NumUsedType(VkDescriptorType type) const { return m_NumDescriptorsPerType[type]; }

public:
	//	Descriptor Set는 하나의 그룹으로 파이프라인에 연결하는 셰이더 리소스의 세트이다.
	struct DescriptorSetLayout
	{
		std::vector<VkDescriptorSetLayoutBinding> Bindings;
		VkDescriptorSetLayout handle;
	};
	inline const std::map<int32, DescriptorSetLayout>& GetLayouts() const { return m_SetLayouts; }

	// Add Descriptor
	void AddDescriptor(int32 descriptorSetIndex, const VkDescriptorSetLayoutBinding& descriptor);

	// Create DescriptorSetLayouts
	void Compile();

private:
	VulkanDevice* m_Device;

	uint32 m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_RANGE_SIZE];

	std::map<int32, DescriptorSetLayout> m_SetLayouts;
	std::vector<VkDescriptorSetLayout> m_Handles;
};

/*---------------------------------------------------------------------------------------------------------------------------------
	PipelineLayout
/*--------------------------------------------------------------------------------------------------------------------------------*/
class VulkanPipelineLayout
{
public:
	VulkanPipelineLayout(VulkanDevice* device, VulkanDescriptorSetsLayout* descriptorSetsLayout);
	~VulkanPipelineLayout();

	inline VkPipelineLayout Handle() const { return m_Handle; }

private:
	VulkanDevice* m_Device;

	VkPipelineLayout m_Handle;
	uint32 m_NumDescriptorSets;
};

/*---------------------------------------------------------------------------------------------------------------------------------
	Descriptor Sets 클래스
		- DescriptorSet은 Descriptor Pool에 의해서 생성된다.
		- DescriptorPool이 없어질 때, 소멸된다.
		- 정의된 DescriptorSetLayout으로 Descriptor Set을 생성한다.
		- Descriptor Set이 사용되지 않을 때 리소스는 업데이트 된다.

	[2017-02-21]
		DescriptorSet들에 대한 모든 Layout을 하나의 DescriptorSets로 생성하는 것이 맞는지 아니면, DescriptorSet 하나씩으로 생성해서 관리하는 것이 맞는지 모르겠다.
/*---------------------------------------------------------------------------------------------------------------------------------*/
class VulkanDescriptorSets
{
public:
	VulkanDescriptorSets(VulkanDevice* device, VulkanDescriptorSetsLayout* layout);
	~VulkanDescriptorSets();

	inline const VkDescriptorSet GetHandle(int32 index) const { return m_Sets[index]; }
	inline const std::vector<VkDescriptorSet>& GetHandles() const { return m_Sets; }

private:
	VulkanDevice* m_Device;

	VulkanDescriptorPool* m_Pool;
	VulkanDescriptorSetsLayout* m_Layout;

	std::vector<VkDescriptorSet> m_Sets;
};

/*---------------------------------------------------------------------------------------------------------------------------------
	DescriptorPool
	- DescriptorSet을 생성한다.
	- Pool이 최대한 할당할 수 있는 DescriptionSets의 수를 체크한다. 
/*--------------------------------------------------------------------------------------------------------------------------------*/
class VulkanDescriptorPool
{
public:
	VulkanDescriptorPool(VulkanDevice* device);
	~VulkanDescriptorPool();

	bool CanAllocate(const VulkanDescriptorSetsLayout& Layout) const;

	void TrackAddUsage(const VulkanDescriptorSetsLayout& Layout);
	void TrackRemoveUsage(const VulkanDescriptorSetsLayout& Layout);

	inline bool IsEmpty() const { return m_NumAllocatedDescriptorSets == 0 ? true : false; }
	inline VkDescriptorPool	Handle() const { return m_DescriptorPool; }

private:
	VulkanDevice* m_Device;

	uint32 m_MaxDescriptorSets;
	uint32 m_NumAllocatedDescriptorSets;
	uint32 m_PeakAllocatedDescriptorSets;

	// Tracks number of allocated types, to ensure that we are not exceeding our allocated limit
	int32 m_MaxAllocatedTypes[VK_DESCRIPTOR_TYPE_RANGE_SIZE];
	int32 m_NumAllocatedTypes[VK_DESCRIPTOR_TYPE_RANGE_SIZE];
	int32 m_PeakAllocatedTypes[VK_DESCRIPTOR_TYPE_RANGE_SIZE];

	VkDescriptorPool m_DescriptorPool;
};
