#pragma once

class VulkanRenderTarget : public IRenderTarget
{
	__DeclareRtti;
public:
	VulkanRenderTarget(VulkanDevice* device);
	virtual ~VulkanRenderTarget();

	bool Create(int width, int height, int32 multisample = 1) OVERRIDE;

	void Begin() OVERRIDE;
	void End() OVERRIDE;

public:
	// ColorSurface
	void SetRenderTexture(const Ptr<VulkanRenderTexture2D>& surface, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore);
	void AddRenderTexture(const Ptr<VulkanRenderTexture2D>& surface, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore);
	IRenderTexture2D* AddRenderTexture(int32 format, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore) OVERRIDE;
	IRenderTexture2D* GetRenderTexture(int32 channel) const OVERRIDE;
	IRenderTexture2D* GetRenderTexture(const TCHAR* id) const OVERRIDE;
	void ClearRenderTextures() OVERRIDE;

	// DepthSurface
	void CreateDepthBuffer(int format, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore, RTLoadAction stencilLoadAction = RTLoadAction::ENoAction, RTStoreAction stencilStoreAction = RTStoreAction::ENoAction) OVERRIDE;
	void SetDepthBuffer(const Ptr<IDepthStencilSurface>& surface, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore, RTLoadAction stencilLoadAction = RTLoadAction::ENoAction, RTStoreAction stencilStoreAction = RTStoreAction::ENoAction) OVERRIDE;
	virtual const Ptr<IDepthStencilSurface>& GetDepthBuffer() const OVERRIDE;

	// Description
	const Vulkan::RenderTargetDesc& GetDescription() const;

private:
	void UpdateRenderTargetDesc() const;

	VulkanDevice* m_Device;

	struct ColorSurface
	{
		Ptr<VulkanRenderTexture2D> surface;
		RTLoadAction loadAction;
		RTStoreAction storeAction;
	};
	struct DepthStencilSurface
	{
		Ptr<IDepthStencilSurface> surface;
		RTLoadAction loadAction;
		RTStoreAction storeAction;
		RTLoadAction stencilLoadAction;
		RTStoreAction stencilStoreAction;
	};

	std::vector<ColorSurface> m_ColorSurfaces;
	DepthStencilSurface m_DepthStencilSurface;

	mutable Vulkan::RenderTargetDesc m_Description;
	mutable bool m_NeedUpdateDescription;
};
