#pragma once

namespace Vulkan
{

class ResourceHeap;
class ResourceHeapPage;
class ResourceAllocation;

class BufferSubAllocation;
class BufferAllocation;

/*	리소스 (버퍼, 이미지 등)의 메모리 관리자
	- UE4 (4.15)에서 발췌
	- 리소스 관리 타입 별로 별도의 ResourceHeap을 가진다.
*/
class ResourceHeapPool
{
public:
	ResourceHeapPool();
	~ResourceHeapPool();

	void Init(VulkanDevice* device);
	void Destroy();

	BufferSubAllocation* AllocateBuffer(uint32 Size, VkBufferUsageFlags BufferUsageFlags, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line);
	void ReleaseBuffer(BufferAllocation* BufferAllocation);
#if 0
	ImageSuballocation* AllocateImage(uint32 Size, VkImageUsageFlags ImageUsageFlags, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line);
	void ReleaseImage(ImageAllocation* ImageAllocation);
#endif

	ResourceAllocation* AllocateBufferMemory(const VkMemoryRequirements& MemoryReqs, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line);
	ResourceAllocation* AllocateImageMemory(const VkMemoryRequirements& MemoryReqs, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line);
	void ReleaseFreePages();

	inline VulkanDevice* GetOwner() const { return m_Device; }

private:
	void ReleaseFreedResources(bool bForce);
	void DestroyResourceAllocations();

	VulkanDevice* m_Device;

	std::vector<ResourceHeap*> m_ResourceTypeHeaps;	// 타입 별 리소스 힙

	ResourceHeap* m_GPUHeap;					// VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	ResourceHeap* m_UploadToGPUHeap;			// VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
	ResourceHeap* m_DownloadToCPUHeap;			// VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT

	enum
	{
		BufferAllocationSize = 1 * 1024 * 1024,
		ImageAllocationSize = 2 * 1024 * 1024,
	};

	std::vector<BufferAllocation*> m_UsedBufferAllocations;
	std::vector<BufferAllocation*> m_FreeBufferAllocations;
#if 0
	TArray<FImageAllocation*> UsedImageAllocations;
	TArray<FImageAllocation*> FreeImageAllocations;
#endif
};

/*	하나의 메모리 타입에 대한 리소스 메모리 힙
	- UE4 (4.15)에서 발췟
	- 내부적으로 ResourceHeapPage 단위로 메모리 풀을 만들어서 관리한다.
*/
class ResourceHeap
{
public:
	ResourceHeap(ResourceHeapPool* owner, uint32 memoryTypeIndex, uint32 pageSize);
	~ResourceHeap();

	void FreePage(ResourceHeapPage* page);
	void ReleaseFreePages(bool bForce);

	bool IsHostCachedSupported() const { return m_bIsHostCachedSupported; }
	bool IsLazilyAllocatedSupported() const { return m_bIsLazilyAllocatedSupported; }

protected:
	friend class ResourceHeapPool;
	ResourceHeapPool* m_Owner;

	uint32 m_MemoryTypeIndex;

	bool m_bIsHostCachedSupported;
	bool m_bIsLazilyAllocatedSupported;

	uint32 m_DefaultPageSize;
	uint32 m_PeakPageSize;
	uint64 m_UsedMemorySize;
	uint32 m_PageIDCounter;

	std::vector<ResourceHeapPage*> m_UsedBufferPages;
	std::vector<ResourceHeapPage*> m_UsedImagePages;
	std::vector<ResourceHeapPage*> m_FreePages;

	ResourceAllocation* AllocateResource(uint32 Size, uint32 Alignment, bool bIsImage, bool bMapAllocation, const char* File, uint32 Line);
};

struct AllocRange
{
	uint32 Offset;
	uint32 Size;

	inline bool operator<(const AllocRange& In) const {	return Offset < In.Offset;	}
	static void JoinConsecutiveRanges(std::vector<AllocRange>& Ranges);
};

/*	한 단위 할당된 디바이스 메모리 영역
	- UE4 (4.15)에서 발췟
	- 할당된 메모리 영역에서 잘라서 ResourceAllocation으로 할당해준다.
*/
class ResourceHeapPage
{
public:
	ResourceHeapPage(ResourceHeap* owner, DeviceMemoryAllocation* deviceMemoryAllocation, uint32 ID);
	~ResourceHeapPage();

	ResourceAllocation* TryAllocate(uint32 size, uint32 alignment, const char* File, uint32 Line);
	ResourceAllocation* Allocate(uint32 size, uint32 alignment, const char* File, uint32 Line);

	void ReleaseAllocation(ResourceAllocation* allocation);

protected:
	ResourceHeap* m_Owner;
	DeviceMemoryAllocation* m_DeviceMemoryAllocation;		// 할당된 디바이스 메모리

	std::vector<ResourceAllocation*> m_Allocations;		// 할당된 메모리

	uint32 m_MaxSize;
	uint32 m_UsedSize;
	int32 m_PeakNumAllocation;
	uint32 m_ID;
	uint32 m_FrameFreed;

	bool JoinFreeBlocks();

	std::vector<AllocRange> m_FreeList;
	friend class ResourceHeap;
};

/*	할당된 리소스 메모리 영역
*/
class ResourceAllocation
{
public:
	ResourceAllocation(ResourceHeapPage* InOwner, DeviceMemoryAllocation* InDeviceMemoryAllocation,
		uint32 InRequestedSize, uint32 InAlignedOffset,
		uint32 InAllocationSize, uint32 InAllocationOffset, const char* InFile, uint32 InLine);
	virtual ~ResourceAllocation();

	inline uint32 GetSize() const { return m_RequestedSize; }
	inline uint32 GetAllocationSize() { return m_AllocationSize; }
	inline uint32 GetOffset() const { return m_AlignedOffset; }

	inline VkDeviceMemory GetHandle() const	{ return m_DeviceMemoryAllocation->Handle(); }

	inline void* GetMappedPointer()
	{
		_ASSERT(m_DeviceMemoryAllocation->CanBeMapped());
		_ASSERT(m_DeviceMemoryAllocation->IsMapped());
		return (uint8*)m_DeviceMemoryAllocation->GetMappedData() + m_AlignedOffset;
	}

	inline uint32 GetMemoryTypeIndex() const { return m_DeviceMemoryAllocation->Type(); }

public:
	void BindBuffer(VulkanDevice* Device, VkBuffer Buffer);
	void BindImage(VulkanDevice* Device, VkImage Image);

private:
	ResourceHeapPage* m_Owner;

	uint32 m_AllocationSize;		// Total size of allocation
	uint32 m_AllocationOffset;		// Original offset of allocation
	uint32 m_RequestedSize;			// Requested size
	uint32 m_AlignedOffset;			// Requested alignment offset

	DeviceMemoryAllocation* m_DeviceMemoryAllocation;

#ifdef _DEBUG
	const char* File;
	uint32 Line;
#endif

	friend class ResourceHeapPage;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*																																				*/	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct ResourceSubAllocation
{
public:
	ResourceSubAllocation(uint32 InRequestedSize, uint32 InAlignedOffset, uint32 InAllocationSize, uint32 InAllocationOffset);

	inline uint32 GetOffset() const { return m_AlignedOffset; }
	inline uint32 GetSize() const { return m_AllocationSize; }

protected:
	uint32 m_RequestedSize;
	uint32 m_AlignedOffset;
	uint32 m_AllocationSize;
	uint32 m_AllocationOffset;
};

class SubResourceAllocator
{
public:
	SubResourceAllocator(ResourceHeapPool* owner, DeviceMemoryAllocation* deviceMemroyAllocation,
								uint32 InMemoryTypeIndex, VkMemoryPropertyFlags InMemoryPropertyFlags,
								uint32 InAlignment);
	virtual ~SubResourceAllocator();

	virtual ResourceSubAllocation* CreateSubAllocation(uint32 Size, uint32 AlignedOffset, uint32 AllocatedSize, uint32 AllocatedOffset) = 0;
	virtual void Destroy(VulkanDevice* Device) = 0;

	ResourceSubAllocation* TryAllocateNoLocking(uint32 InSize, uint32 InAlignment, const char* File, uint32 Line);
	inline ResourceSubAllocation* TryAllocateLocking(uint32 InSize, uint32 InAlignment, const char* File, uint32 Line)
	{
		//FScopeLock ScopeLock(&CS);
		return TryAllocateNoLocking(InSize, InAlignment, File, Line);
	}

	inline uint32 GetAlignment() const	{ return m_Alignment; }
	inline void* GetMappedPointer()	{ return m_MemoryAllocation->GetMappedData(); }

protected:
	ResourceHeapPool* m_Owner;
	uint32 m_MemoryTypeIndex;
	VkMemoryPropertyFlags m_MemoryPropertyFlags;
	DeviceMemoryAllocation* m_MemoryAllocation;
	uint32 m_MaxSize;
	uint32 m_Alignment;
	uint32 m_FrameFreed;
	int64 m_UsedSize;

	// List of free ranges
	std::vector<AllocRange> m_FreeList;

	// Active sub-allocations
	std::vector<ResourceSubAllocation*> m_Suballocations;
	bool JoinFreeBlocks();
};

class BufferAllocation;

class BufferSubAllocation : public ResourceSubAllocation
{
public:
	BufferSubAllocation(BufferAllocation* InOwner, VkBuffer InHandle,
		uint32 InRequestedSize, uint32 InAlignedOffset,
		uint32 InAllocationSize, uint32 InAllocationOffset);
	virtual ~BufferSubAllocation();

	inline VkBuffer GetHandle() const {	return m_Handle; }
	inline BufferAllocation* GetBufferAllocation() { return m_Owner; }

	void* GetMappedPointer();

protected:
	friend class BufferAllocation;

	BufferAllocation* m_Owner;
	VkBuffer m_Handle;
};

class BufferAllocation : public SubResourceAllocator
{
public:
	BufferAllocation(ResourceHeapPool* InOwner, DeviceMemoryAllocation* InDeviceMemoryAllocation,
		uint32 InMemoryTypeIndex, VkMemoryPropertyFlags InMemoryPropertyFlags,
		uint32 InAlignment,
		VkBuffer InBuffer, VkBufferUsageFlags InBufferUsageFlags);
	virtual ~BufferAllocation();

	virtual ResourceSubAllocation* CreateSubAllocation(uint32 Size, uint32 AlignedOffset, uint32 AllocatedSize, uint32 AllocatedOffset) override;
	virtual void Destroy(VulkanDevice* Device) override;

	void Release(BufferSubAllocation* Suballocation);

protected:
	VkBufferUsageFlags m_BufferUsageFlags;
	VkBuffer m_Buffer;
	friend class ResourceHeapPool;
};

} // End of Namespace "Vulkan"