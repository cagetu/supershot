#ifndef __VULKAN_SHADER_RESOURCE_H__
#define __VULKAN_SHADER_RESOURCE_H__


class VulkanShaderResource : public IShaderResource
{
	__DeclareRtti;
public:
	VulkanShaderResource(VulkanDevice* device);
	virtual ~VulkanShaderResource();

	bool Create(ICompiledShaderCode* code, ShaderType::TYPE type) OVERRIDE;
	void Destroy() OVERRIDE;

	inline VkShaderModule Handle() const { return m_ShaderModule; }

public:
	struct Descriptor
	{
		std::string name;

		struct MemberType
		{
			std::string name;
			uint32 offset = 0;
			uint32 size = 0;

			enum BaseType
			{
				Unknown,
				Void,
				Boolean,
				Char,
				Int,
				UInt,
				Int64,
				UInt64,
				AtomicCounter,
				Double,
				Float,
				Vector2,
				Vector3,
				Vector4,
				Matrix3,
				Matrix4,
				Struct,
				Image,
				SampledImage,
				Sampler
			};
			BaseType basetype = Unknown;
			uint32 vecsize = 1;
			uint32 columns = 1;
			uint32 arraysize = 0;
		};
	};

	struct Attribute : Descriptor
	{
		uint32_t location = 0;
	};

	struct Uniform : Descriptor
	{
		uint32_t descriptorSet = 0;
		uint32_t binding = 0;
		uint32_t size = 0;

		std::vector<MemberType> members;
	};

	struct PushConstant : Descriptor
	{
		struct Range
		{
			unsigned index = 0;
			size_t offset = 0;
			size_t range = 0;
		};

		std::vector<Range> bufferRanges;
	};

	struct SubpassInput : Descriptor
	{
		uint32_t descriptorSet = 0;
		uint32_t binding = 0;
		uint32_t input_attachment = 0;
	};

	inline const char* GetEntryName() const { return m_EntryPoint.c_str(); }
	inline const std::vector<Attribute>& GetInputs() const { return m_Inputs; }
	inline const std::vector<Attribute>& GetOuputs() const { return m_Outputs; }
	inline const std::vector<Uniform>& GetImages() const { return m_Images; }
	inline const std::vector<Uniform>& GetSamplers() const { return m_Samplers; }
	inline const std::vector<Uniform>& GetSampledImages() const { return m_SampledImages; }
	inline const std::vector<Uniform>& GetUniformBuffers() const { return m_UniformBuffers; }
	inline const std::vector<PushConstant>& GetPushConstants() const { return m_PushConstants; }
	inline const std::vector<SubpassInput>& GetSubpassInputs() const { return m_SubpassInputs; }

	inline uint32 NumDescriptors() const { return m_NumDescriptors; }
	inline uint32 NumUniformBuffers() const { return m_UniformBuffers.size(); }
	inline uint32 NumSamplers() const { return m_Samplers.size(); }
	inline uint32 NumImages() const { return m_NumImages; }

private:
	VulkanDevice* m_Device;
	VkShaderModule m_ShaderModule;

	uint32 m_NumDescriptors;
	uint32 m_NumImages;

	std::string m_EntryPoint;
	std::vector<Attribute> m_Inputs;
	std::vector<Attribute> m_Outputs;
	std::vector<Uniform> m_Images;
	std::vector<Uniform> m_Samplers;
	std::vector<Uniform> m_SampledImages;
	std::vector<Uniform> m_UniformBuffers;
	std::vector<PushConstant> m_PushConstants;
	std::vector<SubpassInput> m_SubpassInputs;
};

#endif // __VULKAN_SHADER_RESOURCE_H__