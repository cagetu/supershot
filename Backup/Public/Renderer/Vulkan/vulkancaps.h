#pragma once

struct VulkanCaps
{
	static void Setup(VkPhysicalDevice device);

	static bool bSupportsASTC;
};