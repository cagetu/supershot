#pragma once

class VulkanShaderResource;

/******************************************************************************
	셰이더 버텍스 입력 (Attribute) 정의
	- 사실 VertexDeclaration 클래스를 만들어서 사용하는 것이 맞다.
	- 플랫폼 별로 공통되는 방법을 찾을 때까지 이런 식으로 진행하고, 추후에 추상 클래스를 만들어보자.
*******************************************************************************/

struct VulkanVertexDescription : public IVertexDescription
{
	// Vertex Declaration 에서 하는 일을 여기서 한다...
	VertexStreamDesc streams[4];	// 4개의 스트림까지 가능
	int32 locations[16];			// 16개의 버텍스 로케이션 설정 가능
	uint32 vertexSize;				// Vertex에 대한 정의

	void Clear();

	static void GetDesc(VulkanVertexDescription* Output, const VulkanShaderResource* vertexShader, bool packed = false);
};

typedef VulkanVertexDescription VertexDescription;
