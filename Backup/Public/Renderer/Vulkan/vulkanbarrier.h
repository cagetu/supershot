#pragma once

/******************************************************************************
	Pipeline Barrier
	: 파이프라인의 상태를 명시적으로 전환한다.

	- Command buffer내 초기(eariler) command의 세트와 command buffer내 이후(later) command의 세트 사이의 메모리 의존성과 실행 의존성을 추가한다.
	- dstStageMask에 오직 VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT와 실행 의존성은 다음 순서의 Commands를 지연시키지 않을 것이다.
	- VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT는 다음 access가 다른 queue에서나 presentation engine에 의해 완료되었을 memory barriers와 layout transition을 완수하는데 유용하다.
	- 이 경우에 같은 Queue에서 다음 순서의 command들은 대기할 필요가 없지만, barrier나 transition은 반드시 batch 시그널과 관련된 이전의 세마포어들이 완료되어야만 한다.
	- 만약 구현이 파이프라인의 어떤 명시된 스테이지에 이벤트 스테이트를 업데이트를 할 수 없다면, 대신에 논리적으로 마지막 Stage에 업데이트한다.
*******************************************************************************/

namespace Vulkan
{
	void TransitionImageLayout(VulkanCmdBuffer* CmdBuffer, VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout);
	void TransitionImageLayout(VulkanCmdBuffer* CmdBuffer, VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, const VkImageSubresourceRange& subresourceRange);
	void TransitionImageLayout(VulkanCmdBuffer* CmdBuffer, VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, 
							   VkAccessFlags srcAccessMask, VkAccessFlags dstAccessMask, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, 
							   const VkImageSubresourceRange& subresourceRange);
}