#pragma once

struct VulkanCompiledShaderCode : public ICompiledShaderCode
{
	std::vector<uint32_t> code;
};

class VulkanShaderCompiler : public IShaderCompiler
{
public:
	enum Option
	{

	};
public :
	static bool Initialize();
	static void Shutdown();

	static ICompiledShaderCode* Find(const TCHAR* id);

	static ICompiledShaderCode* Compile(const TCHAR* id, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option = 0);

private:
	static std::map <String, VulkanCompiledShaderCode*> m_ShaderCache;
};

typedef VulkanShaderCompiler ShaderCompiler;