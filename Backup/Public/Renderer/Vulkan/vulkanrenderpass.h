#pragma once

/*	Render Pass 객체
	: 각 패스는 장면의 다른 부분을 생성하거나, 포스트 프로세싱의 전체 프레임 효과, 사용자 인터페이스 렌더링 등의 한 렌더링 패스를 정의한다.
	: 각 패스는 여러 개의 SubPass로 구성된다.
	: 현재, 한 개의 렌더링 패스는 단일 출력 이미지를 가지도록 하나의 서브 패스를 가진다.
*/
class VulkanRenderPass
{
public:
	VulkanRenderPass(VulkanDevice* device, const Vulkan::RenderTargetDesc& RTDesc);
	~VulkanRenderPass();

	inline VkRenderPass GetHandle() const { return m_RenderPass; }
	inline const Vulkan::RenderTargetDesc& GetRenderTargetDesc() const { return m_RenderTargetDesc; }

	int32 GetColorAttachmentCount() const;

private:
	VulkanDevice* m_Device;

	VkRenderPass m_RenderPass;
	Vulkan::RenderTargetDesc m_RenderTargetDesc;
};
