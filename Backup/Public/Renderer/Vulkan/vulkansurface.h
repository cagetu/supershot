#pragma once

/////////////////////////////////////////////////////////////////////////////

class VulkanColorSurface : public VulkanRenderTexture2D
{
	__DeclareRtti;
public:
	VulkanColorSurface(VulkanDevice* device);
	virtual ~VulkanColorSurface();
};

/////////////////////////////////////////////////////////////////////////////

class VulkanDepthStencilSurface : public IVulkanTexture, public IDepthStencilSurface
{
	__DeclareRtti;
public:
	VulkanDepthStencilSurface(VulkanDevice* device);
	virtual ~VulkanDepthStencilSurface();

	bool Create(uint32 width, uint32 height, uint32 format, uint32 samples = 1) OVERRIDE;
	void Destroy() OVERRIDE;

	void SetSurface() OVERRIDE;

private:
	Vulkan::DeviceMemoryAllocation* m_MemoryAllocation;
};

/////////////////////////////////////////////////////////////////////////////

class VulkanBackBuffer : public VulkanRenderTexture2D
{
	__DeclareRtti;
public:
	VulkanBackBuffer(VulkanDevice* device, VkImage imageHandle);
	virtual ~VulkanBackBuffer();
};
