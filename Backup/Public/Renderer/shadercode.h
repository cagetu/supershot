#pragma once

struct ShaderType
{
	enum TYPE
	{
		_INVALID = -1,

		VERTEXSHADER = 0,
		FRAGMENTSHADER = 1,
		COMPUTESHADER = 2,
		GEOMETRYSHADER = 3,

		NUM = GEOMETRYSHADER + 1,
	};

	enum
	{
		VERTEX_BIT = 1 << 0,
		FRAGMENT_BIT = 1 << 1,
		COMPUTE_BIT = 1 << 2,
		GEOMETRY_BIT = 1 << 3,
	};
	typedef int32 MASK;

	static TYPE ToType(int32 index) { return (TYPE)index; }
};

class ShaderCode
{
public:
	static bool Initialize();
	static void Shutdown();

	static ShaderCode* Load(const TCHAR* id, ShaderType::TYPE shaderType);
	static ShaderCode* Load(const TCHAR* id, ShaderType::TYPE shaderType, const char* codeBytes, int codeSize);

	static ShaderCode* Find(const TCHAR* id);

private:
	static std::map <String, ShaderCode*> m_ShaderCodes;

public:
	const String& GetID() const { return m_ID; }
	ShaderType::TYPE GetShaderType() const { return m_ShaderType; }
	const std::string& GetShaderCode() const { return m_ShaderCode;  }

protected :
	String m_ID;
	std::string m_ShaderCode;
	ShaderType::TYPE m_ShaderType;
};

////

/*
	"vert" : "../Bin/default.vert",
	"frag" : "../Bin/default.frag",
*/
struct SHADERCODE
{
	unicode::string shaderId[ShaderType::NUM];

	void Insert(ShaderType::TYPE type, const String& filename)
	{
		shaderId[type] = filename;
	}

	bool IsValid(int32 Index) const
	{
		return shaderId[Index].empty() ? true : false;
	}

	ShaderType::TYPE GetType(int32 Index) const
	{
		return (ShaderType::TYPE)Index;
	}

	const unicode::string& GetID(int32 Index) const
	{
		return shaderId[Index];
	}

	bool operator == (const SHADERCODE& rhs) const
	{
		return shaderId == rhs.shaderId;
	}
};

