#pragma once

class IVertexDeclaration : public IObject
{
	__DeclareRtti;
public :
	IVertexDeclaration();
	virtual ~IVertexDeclaration();

	virtual bool CompareTo(IVertexDeclaration* other) { return false;	}
};