#pragma once

#include "core.h"

#include "renderresource.h"
#include "viewport.h"

#include "renderdriver.h"

//#include "vertexelement.h"
#include "vertexformat.h"
#include "vertexdeclaration.h"
#include "vertexbuffer.h"
#include "indexbuffer.h"
#include "uniformbuffer.h"

#include "transform.h"

#include "shadervariant.h"
#include "shaderparameter.h"
#include "shaderfeature.h"
#include "shadercode.h"
#include "shadercompiler.h"
#include "shader.h"
#include "shadercore.h"

#include "renderstate.h"
#include "pipelinestate.h"

#include "surface.h"
#include "texture.h"
#include "material.h"
#include "primitive.h"
#include "mesh.h"
#include "bonecontroller.h"

//#include "resourcecore.h"

#include "rendertarget.h"
#include "renderpipeline.h"
#include "renderpass.h"

// 그래픽 API 별로 선택...

#if defined(_DEVICE_GLES)
#include "OpenGLES/opengles.h"
#elif defined(_DEVICE_VULKAN)
#include "Vulkan/vulkaninclude.h"
#endif


