#pragma once

class GLESVertexBuffer : public VertexBuffer
{
public:
	GLESVertexBuffer();
	virtual ~GLESVertexBuffer();

	bool Create(uint32 fvf, int vertexCount) OVERRIDE;
	bool Create(uint32 fvf, int vertexCount, void* vertexData) OVERRIDE;
	void Destroy() OVERRIDE;

	void* Lock(int stride = 0, int numVert = 0, int lockFlag = 0) OVERRIDE;
	void  Unlock() OVERRIDE;

	void SetStreamSource(int channel = 0, int offset = 0) OVERRIDE;

	unsigned char* GetBytes() const OVERRIDE;

	bool IsVBO() const { return m_IsVBO; }

private:
	bool CreateBuffer(int byteSize, uint32 usage);

	GLuint m_Buffer;
	bool m_IsVBO;
};

/*	TO DO
	- Vertex Buffer Object 처리
	- Multiple Vertex Buffer Object 사용 가능하도록. 
	 (이건 예전에 DX9에서 Multiple Vertex Buffer Bind와 비슷한 듯하니, 참고해서 인터페이스를 만들자)
	- Vertex Array Object 구현 
	- glMapBuffer 사용
*/