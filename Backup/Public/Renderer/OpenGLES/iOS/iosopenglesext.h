#pragma once

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/EAGLDrawable.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

struct IOSOpenGLESCaps : public COpenGLESCaps
{
};

typedef IOSOpenGLESCaps PlatformDriverCaps;

/// FBO
#define glIsRenderbuffer							glIsRenderbufferOES
#define glBindRenderbuffer							glBindRenderbufferOES
#define glDeleteRenderbuffers						glDeleteRenderbuffersOES
#define glGenRenderbuffers							glGenRenderbuffersOES
#define glRenderbufferStorage						glRenderbufferStorageOES
#define glGetRenderbufferParameteriv				glGetRenderbufferParameterivOES
#define glIsFramebuffer								glIsFramebufferOES
#define glBindFramebuffer							glBindFramebufferOES
#define glDeleteFramebuffers						glDeleteFramebuffersOES
#define glGenFramebuffers							glGenFramebuffersOES
#define glCheckFramebufferStatus					glCheckFramebufferStatusOES
#define glFramebufferTexture2D						glFramebufferTexture2DOES
#define glFramebufferRenderbuffer					glFramebufferRenderbufferOES
#define glGetFramebufferAttachmentParameteriv		glGetFramebufferAttachmentParameterivOES
#define glGenerateMipmap							glGenerateMipmapOES
