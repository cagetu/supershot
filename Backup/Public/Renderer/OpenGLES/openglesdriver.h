#pragma once

class OpenGLESDriver : public IRenderDriver
{
public:
	OpenGLESDriver();
	virtual ~OpenGLESDriver();

	void Clear(COLOR clearColor, float clearDepth = 1.0f, ushort clearStencil = 0, uint32 clearFlags = CLEAR_COLOR | CLEAR_DEPTH) OVERRIDE;

	void BeginScene(IRenderTarget* rt = NULL, uint32 rs_or = 0, uint32 rs_and = 0, int32 clearFlags = -1, COLOR clearColor = 0xffffffff, float clearDepth = 1.0f, ushort clearStencil = 0) OVERRIDE;
	void EndScene() OVERRIDE;

	void DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint StartVertex, uint VertexCount, uint32 StartInstance = 0, uint32 InstanceCount = 1, uint32 NumControlPoints = 0) OVERRIDE;
	void DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint BaseVertex, uint StartIndex, uint IndexCount, IndexBuffer* IdxBuffer = nullptr, uint32 StartInstance = 0, uint32 InstanceCount = 1, uint32 NumControlPoints = 0) OVERRIDE;

	void SetVertexBuffer(VertexBuffer* vertexbuffer) OVERRIDE;
	void SetIndexBuffer(IndexBuffer* indexbuffer) OVERRIDE;

	void SetRenderTarget(IRenderTarget* renderTarget) OVERRIDE;
	void DiscardRenderTarget(bool depth, bool stencil, uint32 colors = 0) OVERRIDE;

	void SetShader(IShader* shader, const VertexBuffer* vb, const CMaterial* material) OVERRIDE;

	void SetRenderState(uint32 rs) OVERRIDE;

	IViewport* CreateViewport() OVERRIDE;
	void DestroyViewport(IViewport* viewport) OVERRIDE;
	void SetViewport(IViewport* viewport) OVERRIDE;

	IShader* CreateShader() OVERRIDE;
	void DestroyShader(IShader* shader) OVERRIDE;

protected:
	void CheckDriverCaps();

	GLESShader* m_Shader;
	GLESRenderTarget* m_RenderTarget;

	GLESRenderState* m_RenderState;

	IViewport* m_Viewport;

	uint32 m_RenderStateFilter[2];
	int m_AlphaRef;

	int m_FrameBuffer;
	int m_BackBuffer;
};

////

bool TestGLError(const TCHAR* function);
