#pragma once

class GLESRenderTargetSurface : public GLESRenderTexture
{
	__DeclareRtti;
public :
	GLESRenderTargetSurface();
	virtual ~GLESRenderTargetSurface();
};


class GLESDepthStencilSurface : public IDepthStencilSurface
{
	__DeclareRtti;
public :
	GLESDepthStencilSurface();
	virtual ~GLESDepthStencilSurface();

	bool Create(int width, int height, int format, int samples = 1,
				RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore,
				RTLoadAction stencilLoadAction = RTLoadAction::ENoAction, RTStoreAction stencilStoreAction = RTStoreAction::ENoAction) OVERRIDE;
	void Destroy() OVERRIDE;

	void SetSurface() OVERRIDE;

	GLuint GetDepthBuffer() const { return m_DepthBuffer; }
	int GetMultisamples() const { return m_MultiSamples; }

private :
	GLuint m_DepthBuffer;
};