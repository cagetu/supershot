#pragma once

class GLESRenderState : public RenderState
{
public:
	GLESRenderState();

	void Update(uint32 renderState, int32 alphaRef = 128) OVERRIDE;
	void Reset() OVERRIDE;
};

typedef GLESRenderState CRenderState;