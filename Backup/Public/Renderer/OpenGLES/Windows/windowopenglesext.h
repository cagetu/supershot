#pragma once

/* Type definitions for pointers to functions returned by eglGetProcAddress*/
typedef void (GL_APIENTRY * PFNGLMULTIDRAWELEMENTS) (GLenum mode, GLsizei *count, GLenum type, const GLvoid **indices, GLsizei primcount); // glvoid
typedef void* (GL_APIENTRY * PFNGLMAPBUFFEROES)(GLenum target, GLenum access);
typedef GLboolean (GL_APIENTRY * PFNGLUNMAPBUFFEROES)(GLenum target);
typedef void (GL_APIENTRY * PFNGLGETBUFFERPOINTERVOES)(GLenum target, GLenum pname, void** params);
typedef void (GL_APIENTRY * PFNGLMULTIDRAWARRAYS) (GLenum mode, int *first, GLsizei *count, GLsizei primcount); // glvoid
typedef void (GL_APIENTRY * PFNGLDISCARDFRAMEBUFFEREXT)(GLenum target, GLsizei numAttachments, const GLenum *attachments);
typedef void (GL_APIENTRY * PFNGLRENDERBUFFERSTORAGEMULTISAMPLEAPPLE)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
typedef void (GL_APIENTRY * PFNGLRESOLVEMULTISAMPLEFRAMEBUFFERAPPLE) (void);

typedef void (GL_APIENTRY * PFNGLBINDVERTEXARRAYOES)(GLuint array);
typedef void (GL_APIENTRY * PFNGLDELETEVERTEXARRAYSOES)(GLsizei, const GLuint *arrays);
typedef void (GL_APIENTRY * PFNGLGENVERTEXARRAYSOES)(GLsizei n, GLuint *arrays);
typedef GLboolean (GL_APIENTRY * PFNGLISVERTEXARRAYOES)(GLuint array);

/* GL_EXT_multi_draw_arrays */
extern PFNGLMULTIDRAWELEMENTS				glMultiDrawElementsEXT;
extern PFNGLMULTIDRAWARRAYS					glMultiDrawArraysEXT;

/* GL_EXT_multi_draw_arrays */
extern PFNGLMAPBUFFEROES                   glMapBufferOES;
extern PFNGLUNMAPBUFFEROES                 glUnmapBufferOES;
extern PFNGLGETBUFFERPOINTERVOES           glGetBufferPointervOES;

/* GL_EXT_discard_framebuffer */
extern PFNGLDISCARDFRAMEBUFFEREXT			glDiscardFramebufferEXT;

/* GL_APPLE_framebuffer_multisample */
extern PFNGLRENDERBUFFERSTORAGEMULTISAMPLEAPPLE	glRenderbufferStorageMultisampleAPPLE;
extern PFNGLRESOLVEMULTISAMPLEFRAMEBUFFERAPPLE	glResolveMultisampleFramebufferAPPLE;

// Declares the binary program functions
extern PFNGLGETPROGRAMBINARYOESPROC		glGetProgramBinaryOES;
extern PFNGLPROGRAMBINARYOESPROC		glProgramBinaryOES;

extern PFNGLBINDVERTEXARRAYOES		glBindVertexArrayOES;
extern PFNGLDELETEVERTEXARRAYSOES	glDeleteVertexArraysOES;
extern PFNGLGENVERTEXARRAYSOES		glGenVertexArraysOES;

struct WindowOpenGLESCaps : public COpenGLESCaps
{
	static void Setup();
};

typedef WindowOpenGLESCaps PlatformDriverCaps;