#pragma once

//
//
//
struct WindowESContext
{
	EGLNativeDisplayType eglNativeDisplay;	/// Display handle
	EGLNativeWindowType  eglNativeWindow;	/// Window handle
	EGLDisplay  eglDisplay;					/// EGL display	
	EGLContext  eglContext;					/// EGL context
	EGLSurface  eglSurface;					/// EGL surface
};

typedef WindowESContext	ESContext;

extern bool TestEGLError(const wchar* function);

//
//
//
class WindowOpenGLES : public OpenGLESDriver
{
public:
	WindowOpenGLES();
	virtual ~WindowOpenGLES();

	bool Create(const IDisplayContext* displayHandle) OVERRIDE;
	void Destroy() OVERRIDE;

	void Present() OVERRIDE;

private:
	ESContext m_ESContext;
};
