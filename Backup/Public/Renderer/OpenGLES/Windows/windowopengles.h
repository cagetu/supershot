#pragma once

#include <EGL/egl.h>
#if _OPENGL_ES3
#include <GLES3/gl3.h>
#include <GLES2/gl2ext.h>
//#include <GLES3/gl3ext.h>
#elif _OPENGL_ES2
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2extimg.h>
#endif

#include "windowopenglesext.h"
#include "windowopenglesdriver.h"
