#pragma once

class ITexture : public ISurface
{
	__DeclareRtti;
public :
	ITexture();
	virtual ~ITexture();

	virtual void SetTexture() {};

	void SetID(const String& id) { m_ID = id; }
	const String& GetID() const { return m_ID; }

protected :
	String m_ID;
};

///////////

class ITexture2D : public ITexture
{
	__DeclareRtti;
public:
	ITexture2D();

	virtual bool Load(const TCHAR* filename) { return false; }
	virtual void Destroy() {}

	//이거 처리할 것....!!! 

	void SetFilterMode(FILTERMODE::TYPE minFilter, FILTERMODE::TYPE magFilter);
	void SetAddressMode(ADDRESSMODE::TYPE U, ADDRESSMODE::TYPE V, ADDRESSMODE::TYPE W);
	void SetMipLodBias(float mipLodBias);

protected:
	FILTERMODE::TYPE m_FilterMode[2];
	ADDRESSMODE::TYPE m_AddressMode[3];
	float m_MipLodBias;
};

///////////

class IRenderTexture2D : public ITexture
{
	__DeclareRtti;
public:
	virtual bool Create(int32 width, int32 height, int32 format) { return false; }
	virtual void Destroy() {}

	virtual void SetSurface(int32 channel) {}
};

///////////

class IWritableTexture : public ITexture
{
	__DeclareRtti;
public:
	virtual bool Create(int32 width, int32 height, int32 format) { return false; }
	virtual void Destroy() {}

	virtual void* Lock(int32 *pitch, int32 lv = 0, int32 flags = 0) { return NULL; }
	virtual void Unlock(bool force = true, int32* rect = NULL) {}
};
