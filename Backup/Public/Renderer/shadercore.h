#pragma once

class ShaderCore
{
public :
	static void Initialize();
	static void Shutdown();

	static IShaderResource* Build(const TCHAR* shaderId, ShaderType::TYPE shaderType, ShaderFeature::MASK shaderMask);
	static Ptr<IShader> Build(const SHADERCODE& shaderCodeID, ShaderFeature::MASK shaderMask);

	static void Release(IShader* shader);

	static void Begin(IShader* shader, const CMaterial* material, const VertexBuffer* vb);
	static void End(IShader* shader);

protected :
	static Ptr<IShader> Find(const SHADERCODE& shaderCodeID, ShaderFeature::MASK shaderMask);

	struct SHADERKEY
	{
		SHADERCODE code;
		ShaderFeature::MASK mask;

		Ptr<IShader> instance;
	};
	static std::vector <SHADERKEY> m_ShaderList;

	static std::map <String, IShaderResource*> m_IShaderResources;
};

/*	TODO 
	- 플랫폼 API 별로 어떻게 SHADER 객체를 생성해주게 할지를 고민해보자...
	- 셰이더 관리
		- 같은 셰이더는 한번만 생성 (컴파일) 되도록 한다.
		- Uber Shader 방식은 아니지만 define을 변행해서 사용하기 때문에, macro까지 포함해서 셰이더 컴파일을 확인해야 한다.
		- 셰이더 키를 어떻게 설정할 수 있을까????
		- 셰이더 컴파일 타임을 로딩타임에 할 수 없을까?
			- 렌더스테이트가 변경된다면, 셰이더 컴파일을 언제 해야 할 것인가?

		1. 셰이더 코드 관리
		2. 빌드된 셰이더 관리
		: ShaderCodeManager 에서 셰이더 코드를 얻어온다.
		: Material은 해당 셰이더 코드만 가지고 있고, Build는 ShaderManager에 해달라고 한다.
		: 단, 빌드는 최대한 로딩타임에 하도록 유도한다.
*/
