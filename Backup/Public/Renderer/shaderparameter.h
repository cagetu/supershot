#pragma once

class ShaderParameter
{
public:
	enum _PARAM
	{
		// 미리 정의된 엔진 내부 셰이더 변수
		WORLD,
		VIEW,
		PROJECTION,
		CAMERA,
		CAMERAPOSITION,
		CAMERADIRECTION,
		CAMERADISTANCE,
		CAMERAFOV,
		FRUSTUMCORNERS,

		GLOBALLIGHTDIRECTION,
		GLOBALLIGHTPOSITION,
		GLOBALLIGHTCOLOR,

		GLOBALAMBIENTCOLOR,
	
		HEMISPHERECOLOR,
		HEMISPHEREUPPER,
		HEMISPHERELOWER,

		RIMLIGHTDIRECTION,
		RIMLIGHTCOLOR,
		RIMLIGHTPARAM,

		SCREENSIZE,

		DIFFUSEMAP,

		TMARRAY,

		_MAX_RESERVED_PARAM,	// 최대 내부 변수

		// 프로그램 내부에서 계산
		WORLDVIEWPROJECTION = _MAX_RESERVED_PARAM,
		WORLDVIEW,
		VIEWPROJECTION,

		SCREENPROJSCALE,
		SCREENPROJBIAS,

		//QUICKGAUSSIANWEIGHTS,
		//QUICKGAUSSIANOFFSETS,

		_MAX_PARAMCOUNT, 
	};
	typedef int PID;

	static void Initialize();
	static void Shutdown();

	static void Reset();

	// 셰이더 변수 등록
	static int Register(const wchar* sementic);
	static int Find(const wchar* sementic);

	// 예약된 셰이더 변수 (잘못 쓰면 죽을걸??)
	static void SetCommonValue(int pid, const Variant& value);
	static const Variant& GetCommonValue(int pid);

private :
	static void Register(const wchar* sementic, PID uid);

	static std::map<String, PID> m_ParamRegistry;

	static Variant m_CommonValue[_MAX_RESERVED_PARAM];
};

#define _SHADERPARAMETER(type, value) \
	ShaderParameter::SetCommonValue(type, value)

/*
	셰이더 변수는 크게 2가지로 나눈다.
	1. 엔진 내부 변수
		- 트랜스폼 정보 (WORLD, VIEW, PROJECTION, BONE_MATRIX 등)
		- 카메라 정보 (Camera Near, Far ...)
		: 엔진 내부 변수는 미리 정의해둔다.

	2. 엔진 외부 변수
		- 머터리얼 정보
			- 텍스쳐
			- 칼라
			- 라이팅 세팅
*/
