#pragma once

class ITexture;

struct Variant
{
public:
	enum Type
	{
		_Void,
		_Int,
		_Float,
		_Bool,
		_Vector2,
		_Vector3,
		_Vector4,	
		_Matrix3,
		_Matrix4,
		_Texture,	// Texture의 포인터를 물고 있을 예정!!!
		_Float_Array,
		_Vector2_Array,
		_Vector3_Array,
		_Vector4_Array,
		_Matrix_Array,
	} ;

	//------------------------------------------------------
	// Methods
	//------------------------------------------------------
	Variant() : m_type(_Void) {}
	Variant(const Variant& rhs) : m_type(_Void) { operator = (rhs); }
	Variant(int val) : m_type(_Int) { m_Value.Int = val; }
	Variant(float val) : m_type(_Float) { m_Value.Float[0] = val; }
	Variant(bool val) : m_type(_Bool) { m_Value.Bool = val; }
	Variant(const Vector2& val) : m_type(_Vector2) { m_Value.Float[0] = val.x; m_Value.Float[1] = val.y; }
	Variant(const Vector3& val) : m_type(_Vector3) { m_Value.Float[0] = val.x; m_Value.Float[1] = val.y; m_Value.Float[2] = val.z; }
	Variant(const Vector4& val) : m_type(_Vector4) { m_Value.Float[0] = val.x; m_Value.Float[1] = val.y; m_Value.Float[2] = val.z; m_Value.Float[3] = val.w; }
	Variant(const Mat3& val) : m_type(_Matrix3) { m_Value.Matrix = val.ToMat4(); }
	Variant(const Mat4& val) : m_type(_Matrix4) { m_Value.Matrix = val; }
	Variant(ITexture * val) : m_type(_Texture) { m_Value.Texture = val; }
	Variant(const float* val, unsigned int count) : m_type(_Float_Array) { Duplicate(val, sizeof(float), count); }
	Variant(const Vector2* val, unsigned int count) : m_type(_Vector2_Array) { Duplicate(val, sizeof(Vector2), count); }
	Variant(const Vector3* val, unsigned int count) : m_type(_Vector3_Array) { Duplicate(val, sizeof(Vector3), count); }
	Variant(const Vector4* val, unsigned int count) : m_type(_Vector4_Array) { Duplicate(val, sizeof(Vector4), count); }
	Variant(const Mat4* val, unsigned int count) : m_type(_Matrix_Array) { Duplicate(val, sizeof(Mat4), count); }

	~Variant() { Delete(); }

	void clear() { Delete(); }
	void reset() { Reset(); }

	Type type() const { return m_type; }
	bool empty() const { return (m_type == _Void) ? true : false; }
	unsigned int count() const 
	{ 
		_ASSERT(m_type == _Float_Array || m_type == _Vector2_Array || m_type == _Vector3_Array || m_type == _Vector4_Array || m_type == _Matrix_Array); 
		return m_Value.Array.Count;
	}
	unsigned int size() const;

	operator int() const { _ASSERT(m_type == _Int); return m_Value.Int; }
	operator float() const { _ASSERT(m_type == _Float); return m_Value.Float[0]; }
	operator bool() const { _ASSERT(m_type == _Bool); return m_Value.Bool; }
	operator const Vector4& () const { _ASSERT(m_type == _Vector4); return *(Vector4*)m_Value.Float; }
	operator const Vector4* () const { _ASSERT(m_type == _Vector4); return (Vector4*)m_Value.Float; }
	operator const Vector3& () const { _ASSERT(m_type == _Vector3); return *(Vector3*)m_Value.Float; }
	operator const Vector3* () const { _ASSERT(m_type == _Vector3); return (Vector3*)m_Value.Float; }
	operator const Vector2& () const { _ASSERT(m_type == _Vector2); return *(Vector2*)m_Value.Float; }
	operator const Vector2* () const { _ASSERT(m_type == _Vector2); return (Vector2*)m_Value.Float; }
	operator const Mat4& () const { _ASSERT(m_type == _Matrix4); return m_Value.Matrix; }
	operator const Mat4* () const { _ASSERT(m_type == _Matrix4); return &m_Value.Matrix; }
	operator const Mat3& () const { _ASSERT(m_type == _Matrix3); static Mat3 m; m.FromMat4(m_Value.Matrix); return m; }
	operator const Mat3* () const { _ASSERT(m_type == _Matrix3); static Mat3 m; m.FromMat4(m_Value.Matrix); return &m; }

	operator Vector4& () { _ASSERT(m_type == _Vector4); return *(Vector4*)m_Value.Float; }

/*	operator Vector4& () { _ASSERT(m_type == _Vector); return *(Vector4*)m_Value.Float; }
	operator Vector4 * () { _ASSERT(m_type == _Vector); return (Vector4*)m_Value.Float; }
	operator Vector3& () { _ASSERT(m_type == _Vector); return *(Vector3*)m_Value.Float; }
	operator Vector3 * () { _ASSERT(m_type == _Vector); return (Vector3*)m_Value.Float; }
	operator Mat4& () { _ASSERT(m_type == _Matrix); return m_Value.Matrix; }
	operator Mat4* () { _ASSERT(m_type == _Matrix); return &m_Value.Matrix; } */
	operator ITexture * () const { _ASSERT(m_type == _Texture); return m_Value.Texture; }

	const float* GetFloatArray() const { _ASSERT(m_type == _Float_Array); return m_Value.Array.Float; }
	const Vector2* GetVector2Array() const { _ASSERT(m_type == _Vector2_Array); return m_Value.Array.Vector2; }
	const Vector3* GetVector3Array() const { _ASSERT(m_type == _Vector3_Array); return m_Value.Array.Vector3; }
	const Vector4* GetVector4Array() const { _ASSERT(m_type == _Vector4_Array); return m_Value.Array.Vector4; }
	const Mat4* GetMatrixArray() const { _ASSERT(m_type == _Matrix_Array); return m_Value.Array.Matrix; }

	bool operator == (const Variant& rhs) const;
	bool operator != (const Variant& rhs) const { return !operator ==(rhs); }

	Variant& operator = (const Variant& rhs);

private:
	union value
	{
		int			Int;
		bool		Bool;
		float		Float[4];
		Mat4		Matrix;
		ITexture*	Texture;

		struct
		{
			unsigned int	Count;
			union
			{
				float *		Float;
				Vector2 *	Vector2;
				Vector3 *	Vector3;
				Vector4 *	Vector4;
				Mat4 *		Matrix;
				void*		ptr;
			} ;
			void * _empty;
		}	Array ;
	}	m_Value;

	Type m_type;

	void Duplicate(const void * ptr, int pitch, int num);
	void Delete();
	void Reset();

public :
	static Variant null;
};