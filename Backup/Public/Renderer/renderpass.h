#pragma once

//
//	IRenderPass
//
class RenderPass : public RefCount
{
public :
	RenderPass();
	virtual ~RenderPass();

	virtual void Initialize() {}
	virtual void Destroy() ABSTRACT;

	virtual void Update() ABSTRACT;
	virtual void Render() ABSTRACT;

protected :
	friend class RenderPipeline;

	virtual IRenderTarget* GetRenderTarget();
	virtual IRenderTexture2D* GetRenderTexture(const TCHAR* name);
	virtual RenderPipeline::RenderCallback* GetRenderCallback();

	virtual float GetWidth() const;
	virtual float GetHeight() const;

	bool IsRequired(const TCHAR* renderTexture) const;

	RenderPipeline* m_Parent;

protected:
	virtual bool Load(CMiniXML::iterator& node, RenderPipeline* parent);

	void LoadModes(const TCHAR* mode);

	enum _MODE
	{
		_CLEARCOLOR = 1 << 1,
		_CLEARDEPTH = 1 << 2,
		_CLEARSTENCIL = 1 << 3,
		_DISCARDCOLORS = 1 << 4,
		_DISCARDDEPTHSTENCIL = 1 << 5,
		_FIXEDSIZE = 1 << 6,
	};

	struct _RENDERTARGET
	{
		float width;
		float height;
		String name[4];
		uint32 pixelformat[4];

		String depthbuffer;
		uint32 depthformat;

		_RENDERTARGET()
		{
			width = height = 0.0f;
			pixelformat[0] = pixelformat[1] = pixelformat[2] = pixelformat[3] = 0;
			depthformat = 0;
		}
	};

	String m_Name;

	// Features
	uint32 m_Features;

	// Camera

	// Viewport
	IViewport::RelativeExtent m_Viewport;

	// Clear
	uint32 m_ClearColor;
	float m_ClearDepth;
	ushort m_ClearStencil;

	// RenderState
	uint32 m_RenderState;

	_RENDERTARGET m_RenderTarget;
	std::map <String, String> m_RenderTextures;

//<@ FACTORY
public :
	static RenderPass* CreateClass(const char* className);

	static void Initlaize();
	static void Shutdown();

protected:
	typedef RenderPass* (*CreateFunc)();
	typedef std::map <std::string, CreateFunc> FactoryMap;

	static void RegisterObject(const char* className, CreateFunc factoryFunc);

	static FactoryMap* m_RenderPassFactory;
//@>
};

#include "renderpass.inl"

//
//
//
class SceneRenderPass : public RenderPass
{
	__DeclareRenderPass(SceneRenderPass);
public:
	SceneRenderPass();
	virtual ~SceneRenderPass();

	virtual void Destroy() OVERRIDE;

	virtual void Update() OVERRIDE;
	virtual void Render() OVERRIDE;

private:
	virtual bool Load(CMiniXML::iterator& node, RenderPipeline* parent) OVERRIDE;
};

//
//
//
class PostRenderPass : public RenderPass
{
	__DeclareRenderPass(PostRenderPass);
public :
	PostRenderPass();
	virtual ~PostRenderPass();

	virtual void Destroy() OVERRIDE;

	virtual void Update() OVERRIDE;
	virtual void Render() OVERRIDE;

private:
	virtual bool Load(CMiniXML::iterator& node, RenderPipeline* parent) OVERRIDE;

private:
	void ApplyValue();

	Ptr<CMaterial> m_Material;
	Ptr<IShader> m_Shader;

	CMesh* m_ScreenQuad;
};

//
//
//
class CallbackPass : public RenderPass
{
	__DeclareRenderPass(CallbackPass);
public:
	CallbackPass();
	virtual ~CallbackPass();

	virtual void Destroy() OVERRIDE;

	virtual void Update() OVERRIDE;
	virtual void Render() OVERRIDE;
};
