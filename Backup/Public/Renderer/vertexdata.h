#pragma once

/* 테스트로 작석 중인 VertexData
	VertexData
		- StreamData
			: 하나의 버퍼를 만들 수 있는 버퍼 데이터

	(이건 좀 이상하다... 일단 놔두고, 나중에 의미에 맞게 변형하자!)
*/
class VertexData
{
public:
	class StreamData
	{
	public:
		StreamData(const void* p, int32 usage, int32 stride, int32 count)
			: m_Data(p), m_Usage(usage), m_Stride(stride), m_Count(count)
		{
		}
		~StreamData()
		{
			if (m_Data) { delete[] m_Data; }
			m_Data = NULL;
		}

		const void* GetData() const { return m_Data; }
		const int32 GetUsage() const { return m_Usage; }
		const int32 GetCount() const { return m_Count; }
		const int32 GetStride() const { return m_Stride; }

	private:
		const void* m_Data;
		const int32 m_Usage;
		const int32 m_Count;
		const int32 m_Stride;
	};

public:
	VertexData();
	~VertexData();

	void Bind(const void *p, int32 usage, int32 stride, int32 count);

	void Clear();

	int32 GetCount() const { return m_Count; }
	int32 GetStride() const;
	int32 GetFVF() const;

	void FillBuffer(void* dest);

private:
	void Update() const;

	std::map<int, StreamData*> m_Streams;

	mutable int32 m_Stride;
	mutable int32 m_FVF;

	int32 m_Count;

	enum
	{
		_NEED_UPDATE = 1 << 1,
	};
	mutable unsigned int m_Flags;
};
