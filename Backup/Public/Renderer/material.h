#pragma once

#define PASS_SHADER
//#define PASS_VALUES

class CMaterial : public RefCount
{
public :
	CMaterial();
	virtual ~CMaterial();

	// 머터리얼 ID
	int32 GetUID() const { return m_UID; }

	// 머터리얼 파라미터
	void SetParameter(const TCHAR* sementic, const Variant& value);
	bool GetParameter(const TCHAR* sementic, Variant& output) const;
	bool GetParameter(int32 paramId, Variant& output) const;

	//
#ifndef PASS_SHADER
	void SetShaderCode(const SHADERCODE& shaderCode);
	void SetShaderMask(ShaderFeature::MASK shaderMask);

	const SHADERCODE& GetShaderCode() const { return m_ShaderCode; }
	ShaderFeature::MASK GetShaderMask() const { return m_ShaderMask; }
#endif

	// 머터리얼 렌더스테이트
	void SetRenderState(uint32 renderstate);
	uint32 GetRenderState() const { return m_RenderState; }

public:
	class _PASS
	{
	public:
		void SetRenderState(uint32 _renderstate)
		{
			renderstate = _renderstate;
		}
		uint32 GetRenderState() const
		{
			return renderstate;
		}
		const Ptr<IShader>& GetShader() const
		{
			return shader;
		}
#ifdef PASS_VALUES
		void SetParemter(const TCHAR* sementic, const Variant& value)
		{
			parameters[sementic] = value;
		}
		const std::map<String, Variant>& GetParemters() const
		{
			return parameters;
		}
#endif
	protected:
		_PASS() : renderstate(0) {}

		Ptr<IShader> shader;
		uint32 renderstate;

#ifdef PASS_VALUES
		std::map<String, Variant> parameters;
#endif
		friend class CMaterial;
	};

	// 패스 렌더링
	_PASS* BeginPass(uint passIndex);
	void EndPass();

	// 패스 추가
	_PASS* AddPass(const Ptr<IShader>& shader, uint32 renderstate = 0);
	_PASS* GetPass(uint passIndex) const;	
	uint NumPasses() const { return m_Passes.size(); }

	// 렌더스테이트
	void SetRenderState(uint passIndex, uint32 renderstate);
#ifdef PASS_VALUES
	void SetPassParameter(uint passIndex, const TCHAR* sementic, const Variant& value);
#endif

private :
	void InsertParameter(int pid, const Variant& value);
	void InsertParameter(const TCHAR* sementic, const Variant& value);

	int32 FindPID(const TCHAR* sementic) const;

	// MaterialID
	int32 m_UID;

	/// 셰이더 패스
	std::vector<_PASS*> m_Passes;

	/// 셰이더 Value
	std::map <int32, Variant> m_Values;
	std::map <String, int32> m_Types;

	// 렌더스테이트
	uint32 m_RenderState;

	/// 셰이더
#ifndef PASS_SHADER
	SHADERCODE m_ShaderCode;
	uint32 m_ShaderMask;
#endif
};

/*	머터리얼
		- 텍스쳐
		- 렌더스테이트
		- 셰이더
			- material data 파일에서 Material 파일로 변환할 때, Shader가 읽어진다.
			- ShaderCode는 어떤 형태로 든 코드 형태로 셰이더에 저장된다.


	같은 머터리얼
		1. 같은 pass shader
		2. 같은 pass renderstate
		3. 같은 material values
*/
/*
material
{
	parameter["SILHOUETTEPARAM"] = {0.0, 0.0, 0.0, 3.0};
	parameter["DIFFUSEMAP"] = "";

	pass Geometry
	{
		vertexshader = "default.vert";
		pixelshader = "default.frag";

		define = "_USE_DIFFUSEMAP,_USE_NORMAL,_USE_TEXCOORD,_SKINNING";
		renderstate = "CCW";
	}

	pass Silhouette
	{
		vertexshader = "default.vert";
		pixelshader = "default.frag";

		define = "_USE_DIFFUSEMAP,_USE_NORMAL,_USE_TEXCOORD,_SKINNING,_SILHOUETTE";
		renderstate = "CW";
	}
}
*/