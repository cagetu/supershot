#pragma once

class ShaderFeature
{
public :
	typedef uint32 MASK;

public :
	static void Initialize();
	static void Shutdown();

	static void Reset();

	static MASK GetShaderMask(const wchar* shadermask);
	static String GetMaskName(MASK mask);

	static int GetMacro(uint32 mask, std::string* macro, std::string* macroCode);

public :
	enum
	{
		_SKINNING		= 1,
		_INSTANCING,
		_VERTEXCOMPRESS,

		_USE_NORMAL,
		_USE_VERTEXCOLOR,
		_USE_ALPHA,

	};

private:
	static int RegisterBit(const wchar* def);
	static int FindBit(const wchar* def);

	static std::vector <String> m_BitToString;
	static std::vector <std::string> m_Masks;

	static std::map <String, int>	m_StringToBit;

	static const int _MAXBIT = 32;
	static int m_AllocBit;
};

/*	Shader Feature
	- 엔진 내부에서 필요한 기능들은 먼저 등록해둔다.
		- SKINNED
		- INSTANCING 등...
	- 32비트 플래그를 사용하기 때문에, 32개만 사용이 가능한데, 확장이 가능한 방법을 찾아봐야 한다. (이건 Softnette 걸로.. ㅎ)
*/