//================================================================
// File:				: VertexElement.inl
// Related Header File	: VertexElement.h
// Original Author		: changhee
// Creation Date		: 2009. 4. 10
//================================================================
//----------------------------------------------------------------
//inline
//VertexElement::VertexElement()
//	: sementic_(Invalid)
//	, location_(0)
//	, format_(Float)
//{
//}
//----------------------------------------------------------------
inline
VertexElement::VertexElement(uint32 location, Sementic eUsage, Format eFormat, uint32 offset, uint32 streamID, bool instanced)
	: sementic_(eUsage)
	, location_(location)
	, stream_(streamID)
	, format_(eFormat)
	, offset_(offset)
	, instanced_(instanced)
{
}

//----------------------------------------------------------------
//** @brief	component ������ */
//----------------------------------------------------------------
inline
uint32 VertexElement::GetByteSize() const
{
	switch (format_)
	{
		case Float:     return sizeof(float);
		case Float2:    return sizeof(float) * 2;
		case Float3:    return sizeof(float) * 3;
		case Float4:    return sizeof(float) * 4;
		case UByte4:    return sizeof(unsigned char) * 4;
		case Short2:    return sizeof(short) * 2;
		case Short4:    return sizeof(short) * 4;
		case UByte4N:   return sizeof(unsigned char) * 4;
		case Short2N:   return sizeof(short) * 2;
		case Short4N:   return sizeof(short) * 4;
	}
	return 0;
}