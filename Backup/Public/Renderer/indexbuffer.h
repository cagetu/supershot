#pragma once

class IndexBuffer : public IRenderResource
{
	__DeclareRtti;
public :
	IndexBuffer();
	virtual ~IndexBuffer();

	virtual bool Create(int32 indexCount, int32 stride = sizeof(unsigned short)) ABSTRACT;
	virtual bool Create(int32 indexCount, void* indexData, int32 stride = sizeof(unsigned short)) ABSTRACT;
	virtual void Destroy();

	virtual int32 SetIndices() ABSTRACT;

	void SetStride(int32 stride);

	inline int32 GetLength() const { return m_Length; }
	inline int32 GetOffset() const { return m_Offset; }
	inline int32 GetStride() const { return m_Stride; }
	inline int32 GetCount() const { return m_Length / m_Stride; }

	inline bool IsIndex16() const { return m_Flags&_INDEX16 ? true : false; }

	virtual unsigned char* GetBytes() const { return m_IdxBuf; };

public:
	virtual void* Lock(int32 length = 0, int32 lockFlag = 0) ABSTRACT;
	virtual void  Unlock(ushort baseVertIdx = -1) ABSTRACT;

protected:
	enum
	{
		_RELOAD = 1 << 1,
		_BACKUPDATA = 1 << 2,
		_INDEX16 = 1 << 3,
		_INDEX32 = 1 << 4,
	};
	int32 m_Flags;
	int32 m_BufferUsage;

	int32 m_Offset, m_Length;
	int32 m_LockFlags;
	int32 m_Stride;

	unsigned char* m_IdxBuf;

	int32 m_ValidCount;
};

class IndexBufferCache
{
public :
	bool Register(IndexBuffer* resource);
	bool Unregister(IndexBuffer* resource);

	void Clear();

private :
	std::vector <IndexBuffer*> m_Resources;
};