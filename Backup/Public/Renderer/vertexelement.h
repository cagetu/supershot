//================================================================
// File:           : VertexElement.h
// Original Author : changhee
// Creation Date   : 2009. 4. 10
//================================================================
#pragma once

//================================================================
/** VertexElement
	@author		changhee
	@brief		버텍스의 구성요소 하나의 정보
*/
//================================================================
class VertexElement
{
public:
	enum Sementic
	{
		Position	=0,
		PositionT,
		Normal,
		NormalUB4N,
		Uv0,
		Uv0S2,
		Uv1,
		Uv1S2,
		Uv2,
		Uv2S2,
		Uv3,
		Uv3S2,
		Color,
		ColorUB4N,
		Tangent,
		TangentUB4N,
		Binormal,
		BinormalUB4N,
		BlendWeights,
		BlendWeightsUB4N,
		BlendIndices,
		BlendIndicesUB4,

		Invalid
	};

	enum Format
	{
		Float,      //> one-component float, expanded to (float, 0, 0, 1)
		Float2,     //> two-component float, expanded to (float, float, 0, 1)
		Float3,     //> three-component float, expanded to (float, float, float, 1)
		Float4,     //> four-component float
		UByte4,     //> four-component unsigned byte
		Short2,     //> two-component signed short, expanded to (value, value, 0, 1)
		Short4,     //> four-component signed short
		UByte4N,    //> four-component normalized unsigned byte (value / 255.0f)
		Short2N,    //> two-component normalized signed short (value / 32767.0f)
		Short4N,    //> four-component normalized signed short (value / 32767.0f)
	};

	//----------------------------------------------------------------
	//	Methods
	//----------------------------------------------------------------
	VertexElement(uint32 location, Sementic eUsage, Format eFormat, uint32 offset, uint32 streamID, bool instanced = false);

	uint32 GetLocation() const { return location_; }
	uint32 GetStreamID() const { return stream_; }
	uint32 GetOffset() const { return offset_; }
	Sementic GetSementic() const { return sementic_; }
	uint32 GetByteSize() const;
	Format GetFormat() const { return format_; }
	bool IsInstanced() const { return instanced_; }

private:
	uint32 location_;	//> location
	uint32 stream_;		//> bind Stream Index (a.k.a Bind VertexBuffer Index)
	uint32 offset_;		//> vertex stride offset
	Sementic sementic_;	//> element Name
	Format format_;		//> element Format
	bool instanced_;	//> instanced
};

#include "vertexelement.inl"