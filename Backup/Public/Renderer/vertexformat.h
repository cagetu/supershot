#pragma once

/*	VERTEX FORMAT 
	- 전반적으로 다시 구성해야 한다. 지금 상태로는 너무 유연함이 떨어진다. (2015-05-27)
	- 외부에서 format과 데이터를 모두 설정할 수 있도록 변경하도록 하자.
*/
enum FVF_TYPE
{
	FVF_XYZ = 1 << 0,
	FVF_XYZW = 1 << 1,
	FVF_XYZRHW = 1 << 2,
	FVF_NORMAL = 1 << 3,
	FVF_NORMAL_PACKED = 1 << 4,
	FVF_DIFFUSE = 1 << 5,
	FVF_SPECULAR = 1 << 6,
	FVF_TEX0 = 1 << 7,
	FVF_TEX0_PACKED = 1 << 8,
	FVF_TEX1 = 1 << 9,
	FVF_TEX1_PACKED = 1 << 10,
	//	FVF_TEX1_EX			= 1<<11,
	FVF_TANSPC = 1 << 12,
	FVF_TANSPC_PACKED = 1 << 13,
	FVF_SKIN_INDEX = 1 << 14,
	FVF_SKIN_WEIGHT = 1 << 15,
	FVF_INST_TM = 1 << 16,
	FVF_INST_TM_INDEX = 1 << 17,
	FVF_SPEEDTREE_PARAM = 1 << 18,
	FVF_SHCOEFF = 1 << 19,
	FVF_VERTEXAO = 1 << 20,
	FVF_DELCALTM = 1 << 21,
	FVF_LIGHTDATA = 1 << 22,
	FVF_POINTLIST = 1 << 23,
	//FVF_COMPRESS		= 0x00000800,
	//FVF_BLEND_PACKED	= 0x00100000,
};

//////////////////////////////////////////////////////////////////////////////////

// VertexStream에 대한 Vertex 정보
struct VertexStreamDesc
{
	int32 elements;		// fvf
	int32 stride;		// stride size
	bool instanced;		// 인스턴싱 stream인가?

	VertexStreamDesc(int32 format = 0);

	bool Contains(int32 element) const;
	int32 GetStride() const;
};

struct IVertexDescription
{
	static int32 GetOffsets(uint32 fvf, int32 offset[16]);
	static int32 GetStride(uint32 fvf);
};

//////////////////////////////////////////////////////////////////////////////////

struct VertexCompress
{
	static void TexcoordToShort2(short* Output, const float uv[2]);
	static void NormalToUBYTE4N(unsigned char* Output, const Vector3& n);
	static void TangentToUBYTE4N(unsigned char* Output, const Vector4& n);
	//static void BinormalToUBYTE4N(unsigned char* Output, const Vec3& n);
	static void SkinIndexToUBYTE4(unsigned char* Output, const int* indices);
	static void SkinWeightToUBYTE4N(unsigned char* Output, const float* weights);
};

struct VertexDepress
{
	static void UByte4ToSkinIndex(const unsigned char* input, unsigned int* output);
	static void UByte4NToSkinWeight(const unsigned char* input, float* output);
};
