#pragma once

//----------------------------------------------------------------------------
/*	Shader
	- ����� ���̴� ��ü
*/
class IShader : public IRenderResource
{
	__DeclareRtti;
public :
	IShader();
	virtual ~IShader();

	int32 GetUID() const { return m_UID; }

	virtual bool Create(const SHADERCODE& shaderCode, ShaderFeature::MASK mask) ABSTRACT;
	virtual void Destroy() ABSTRACT;

	virtual void Begin(const CMaterial* material, const VertexBuffer* vb) ABSTRACT;
	virtual void End() ABSTRACT;

public:
	virtual void SetValue(uint index, const Variant& value) ABSTRACT;
	
	virtual void UpdateValues(const CMaterial* material) ABSTRACT;
	virtual void ClearValues() ABSTRACT;

	virtual void UpdateVertexAttributes(const VertexBuffer* vertexBuffer) ABSTRACT;
	virtual void ClearVertexAttributes() ABSTRACT;

private:
	friend class ShaderCore;

	int32 m_UID;
};

//----------------------------------------------------------------------------
/*	Shader Bind Parameter
*/
class IShaderProgram : public IObject
{
	__DeclareRtti;
public:
	IShaderProgram(ShaderType::MASK shaderTypes);
	virtual ~IShaderProgram();

	bool HasShaderType(ShaderType::TYPE type) const { return m_ShaderTypeBits & (1 << type) ? true : false; }

	virtual void SetValue(uint32 index, const Variant& value) ABSTRACT;
	virtual void UpdateValues(const CMaterial* material) ABSTRACT;
	virtual void ClearValues() ABSTRACT;

	virtual void UpdateVertexAttributes(const VertexBuffer* vertexBuffer) {}
	virtual void ClearVertexAttributes() {}

protected:
	virtual void UpdateInternal(int32 index, int32 pid);

	ShaderType::MASK m_ShaderTypeBits;
};

//----------------------------------------------------------------------------
/*	Shader Source
	- Load�� ���̴� �ڵ�
*/
class IShaderResource : public IObject
{
	__DeclareRtti;
public:
	IShaderResource();
	virtual ~IShaderResource();

	virtual bool Create(ICompiledShaderCode* code, ShaderType::TYPE type) ABSTRACT;
	virtual void Destroy() ABSTRACT;

public:
	ShaderType::TYPE GetType() const { return m_ShaderType; }

protected:
	ShaderType::TYPE m_ShaderType;
};

IShaderResource* CreateShaderResource(ICompiledShaderCode* code, ShaderType::TYPE type);
