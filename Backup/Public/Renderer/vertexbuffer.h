#pragma once

class VertexBuffer : public IRenderResource
{
	__DeclareRtti;
public :
	VertexBuffer();
	virtual ~VertexBuffer();

	virtual bool Create(uint32 fvf, int32 vertexCount) ABSTRACT;
	virtual bool Create(uint32 fvf, int32 vertexCount, void* vertexData) ABSTRACT;
	virtual void Destroy();

	int32 SetFVF(uint32 fvf);

	uint32 GetFVF() const { return m_FVF; }
	int32 GetLength() const { return m_Length; }
	int32 GetStride() const { return m_Stride; }
	int32 GetOffset() const { return m_Offset; }
	int32 GetCount() const { return m_Length / m_Stride; }

	virtual unsigned char* GetBytes() const { return &m_VtxBuf[m_Offset]; }

	virtual void SetStreamSource(int32 channel = 0, int32 offset = 0) ABSTRACT;

public:
	virtual void* Lock(int32 stride = 0, int32 numVert = 0, int32 lockFlag = 0) ABSTRACT;
	virtual void  Unlock() ABSTRACT;

	void Fill(const Vector3 * v, const Vector3 * norm, const uint32 *diffuse,
			  const float(*uv)[2], const float(*uv2)[2], const Vector4 * tan,
			  const unsigned char(*skinindex)[4], const unsigned char(*skinweight)[4],
			  const float* shcoeff,	const uint32* vtxao);

protected:
	enum
	{
		_COMPRESS = 1 << 1,
		_REFERENCE = 1 << 2,
		_RELOAD = 1 << 3,
		_BACKUPDATA = 1 << 4,
	};
	uint32 m_Flags;
	uint32 m_BufferUsage;

	int32 m_Stride;
	int32 m_Offset;
	int32 m_Length;
	uint32 m_FVF;

	unsigned char* m_VtxBuf;

	int32 m_ValidCount;

public :
	static void FillData(OUT void* dest, int32 stride, uint32 fvf, int32 offset, int32 count,
						const Vector3 * v, const Vector3 * norm, const uint32 *diffuse,
						const float(*uv)[2], const float(*uv2)[2],
						const Vector4 * tan,
						const unsigned char(*skinindex)[4], const unsigned char(*skinweight)[4],
						const float* shcoeff,
						const uint32* vtxao);
};
