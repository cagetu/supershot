#pragma once

class	ViewFrustum
{
public :
	void SetProjection(const Mat4 & camtm, float fovrad, float neardistance, float fardistance, float ratio=3.0f/4.0f, bool flip=false);
	void SetOrthogonal(const Mat4 & camtm, float width, float neardistance, float fardistance, float ratio=3.0f/4.0f, bool flip=false);

	bool CullTest(const AABB & aabb) const; //	aabb
	bool CullTest(const AABB & aabb, const Mat4 & tm) const; //	obb
	bool CullTest(const AABB & bound, bool& inbound) const; //
	bool CullTest(const Sphere & sphere) const; //	sphere

	const Vector3* GetVertices() const { return m_Corners; }

	int ClipPolygon(const Vector3* v, int vn, Vector3* result) const { return ClipPolygonRecur(v, vn, result); }

protected :
	Plane	m_Plane[6];
	Vector3	m_Corners[8];

private :
	int ClipPolygonRecur(const Vector3* v, int vn, Vector3* result, int seq=0) const;
} ;