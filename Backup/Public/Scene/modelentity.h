#pragma once

class CModel : public SceneEntity
{
	__DeclareRtti;
public:
	CModel();
	virtual ~CModel();

	void Update(float dt);
	void Render();

public:
	void SetWorldTM(const Mat4& tm) OVERRIDE;

	void GetWorldBound(OUT AABB* aabb) const OVERRIDE;
	void GetWorldBound(OUT Sphere* sphere) const OVERRIDE;

	void GetLocalBound(OUT AABB* output) const;
	void GetLocalBound(OUT Sphere* output) const;

public:
	CMesh* CreateMesh(CMeshData* geom, const wchar* obj, int idx);
	CMesh* CreateMesh(CMeshData* geom, int index);
	CMesh* CreateMesh(const CMeshData::_GEOM* geom, CMeshData* meshData);

	void AddMesh(CMesh* mesh);
	void RemoveMesh(CMesh* mesh);
	void RemoveAllMeshes();

	const Ptr<CBoneController>& GetBoneController() const;
	const Ptr<CMotionSynth>& GetMotionSynth() const;

protected:
	void UpdateLocalBound() const;

	mutable AABB m_LocalAABB;

	enum _FLAGS
	{
		_UPDATE_LOCAL_BOUND = 1 << 1,
		_UPDATE_WORLD_BOUND = 1 << 2,
	};

	mutable unsigned int m_Flags;

	std::vector <CMesh*> m_Meshes;

protected:
	Ptr<CBoneController> m_BoneController;
	Ptr<CMotionSynth> m_MotionSynth;
};