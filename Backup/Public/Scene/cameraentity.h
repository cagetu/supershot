#pragma once

//================================================================
/** Camera Class
@author		cagetu
@since		2015. 5. 18
@remarks	- 카메라 설정
			- 카메라 관리
			- Frustum Culling
			- Mesh Filter 기능
			- 렌더 패스와 연동
*/
//================================================================
class SceneView : public SceneEntity
{
	__DeclareRtti;
public :
	static void Initialize();
	static void Shutdown();

	static Ptr<SceneView> Create(const TCHAR* name);
	static void Release(const Ptr<SceneView>& camera);

public:
	void SetCamera(const Vector3& position, const Vector3& lookat);
	void SetProjection(float fovrad, float neardistance, float fardistance, float aspectRatio, bool flip = false);
	void SetOrthogonal(float widthsize, float neardistance, float fardistance, float aspectRatio, bool flip = false);

	void SetViewport(float x, float y, float width, float height, float minZ = 0.0f, float maxZ = 1.0f);

public:
	void SetPosition(const Vector3& position);
	void SetLookAt(const Vector3& lookAt);

	const Vector3& GetPosition() const { return m_CameraPosition; }
	const Vector3& GetLookAt() const { return m_CameraLookAt; }
	const Vector3& GetDirection();

//~Scene에 대한 처리.... 이건 자리를 다시 확인해봐야 한다....
public :
	bool BeginScene();
	void EndScene();

	bool CullTest(const AABB& aabb);
	bool CullTest(const Sphere& sphere);
//~Scene에 대한 처리.... 이건 자리를 다시 확인해봐야 한다....

private:
	SceneView(const TCHAR* name);
	virtual ~SceneView();

	void UpdateCamera();
	void UpdateShader();

	String m_Name;

	float m_CameraFOV;			// Perspective
	float m_CameraOtrhoWidth;	// Orthogonal
	float m_CameraNearClip;
	float m_CameraFarClip;
	float m_CameraAspectRatio;

	Vector3 m_CameraPosition;
	Vector3 m_CameraLookAt;
	Vector3 m_CameraDir;

	Mat4 m_CameraTM;
	Mat4 m_ViewTM;
	Mat4 m_ProjectionTM;

	IViewport::RelativeExtent m_Viewport;
	ViewFrustum m_Frustum;

	enum _PROJ
	{
		_PERSPECTIVE = 0,
		_ORTHGONAL = 1,
	};
	int m_ProjectionMode;

	enum _FLAGS
	{
		_PERSPECTIVE_INFINITE = 1 << 1,
		_FLIP = 1 << 2,
		_NEED_UPDATE = 1 << 3,
		_HAS_FRUSTUM = 1 << 4,
		_NEED_UPDATE_CAMERA = 1 << 5,
	};
	uint m_Flags;

private :
	static std::map<String, SceneView*> m_CameraCache;
};