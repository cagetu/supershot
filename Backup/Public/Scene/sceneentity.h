#pragma once

class SceneEntity : public RefCount
{
	__DeclareRootRtti(SceneEntity);
public:
	SceneEntity();
	virtual ~SceneEntity();

	virtual void SetWorldTM(const Mat4& tm);
	const Mat4& GetWorldTM() const;

	virtual void GetWorldBound(OUT AABB* aabb) const;
	virtual void GetWorldBound(OUT Sphere* sphere) const;

protected:
	Mat4 m_WorldTM;

	mutable AABB m_WorldAABB;
};