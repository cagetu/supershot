// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

// Platform
#include "Platform\platform.h"

// Math
#include "Math\mathutil.h"
#include "Math\vector.h"
#include "Math\quat.h"
#include "Math\matrix.h"
#include "Math\aabb.h"
#include "Math\obb.h"
#include "Math\capsule.h"
#include "Math\plane.h"
#include "Math\sphere.h"
#include "Math\spline.h"
#include "Math\rect2d.h"
#include "Math\ray.h"
#include "Math\transform.h"

// Container
#include "Container\algorithm.h"
#include "Container\pair.h"
#include "Container\array.h"
#include "Container\set.h"
#include "Container\map.h"
#include "Container\queue.h"
#include "Container\stack.h"
#include "Container\unicode.h"
#include "Container\string.h"

// Core
#include "Core\ptr.h"
#include "Core\rtti.h"
#include "Core\object.h"

// Utilities
#include "Util\crc.h"
#include "Util\name.h"
#include "Util\args.h"
#include "Util\log.h"
#include "Util\logstream.h"
#include "Util\color.h"
#include "Util\minixml.h"
#include "Util\ini.h"
#include "Util\bitflags.h"
#include "Util\variant.h"
#include "Util\assign.h"
#include "Util\assignregistry.h"


