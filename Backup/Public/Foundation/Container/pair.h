#pragma once

//------------------------------------------------------------------==========================
/*	Dynamic Map Container Class
	
	@oryol::keyvaluepair
*/
//------------------------------------------------------------------==========================
template <typename KEY, typename VALUE>
class Pair
{
public:
	/// 기본 생성자
	Pair();
	/// 복사 생성자 
	Pair(const KEY& k, const VALUE& v);
	/// 이동 생성자
	Pair(KEY&& k, VALUE&& v);
	/// 복사 생성자
	Pair(const Pair& rhs);
	/// 이동 생성자
	Pair(Pair&& rhs);
	/// 소멸자
	~Pair();

	/// copy-assignment
	void operator=(const Pair& rhs);
	/// move-assignment
	void operator=(Pair&& rhs);

	/// test equality
	bool operator==(const Pair& rhs) const;
	/// test inequality
	bool operator!=(const Pair& rhs) const;
	/// test less-then
	bool operator<(const Pair& rhs) const;
	/// test greater-then
	bool operator>(const Pair& rhs) const;
	/// test less-or-equal
	bool operator<=(const Pair& rhs) const;
	/// test greater-or-equal
	bool operator>=(const Pair& rhs) const;

	/// key
	const KEY& Key() const;
	/// value
	const VALUE& Value() const;
	/// read/write value
	VALUE& Value();

	KEY key;
	VALUE value;
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
Pair<KEY, VALUE>::Pair()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
Pair<KEY, VALUE>::Pair(const KEY& k, const VALUE& v)
	: key(k)
	, value(v)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
Pair<KEY, VALUE>::Pair(KEY&& k, VALUE&& v)
	: key(std::move(k))
	, value(std::move(v))
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
Pair<KEY, VALUE>::Pair(const Pair& rhs)
	: key(rhs.key)
	, value(rhs.value)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
Pair<KEY, VALUE>::Pair(Pair&& rhs)
	: key(std::move(rhs.key))
	, value(std::move(rhs.value))
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
Pair<KEY, VALUE>::~Pair()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
void Pair<KEY, VALUE>::operator=(const Pair& rhs)
{
	if (this == &rhs)
	{
		key = rhs.key;
		value = rhs.value;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
void Pair<KEY, VALUE>::operator=(Pair&& rhs)
{
	if (this == &rhs)
	{
		key = std::move(rhs.key);
		value = std::move(rhs.value);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
bool Pair<KEY, VALUE>::operator==(const Pair& rhs) const
{
	return key == rhs.key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
bool Pair<KEY, VALUE>::operator!=(const Pair& rhs) const
{
	return key != rhs.key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
bool Pair<KEY, VALUE>::operator<(const Pair& rhs) const
{
	return key < rhs.key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
bool Pair<KEY, VALUE>::operator>(const Pair& rhs) const
{
	return key > rhs.key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
bool Pair<KEY, VALUE>::operator<=(const Pair& rhs) const
{
	return key <= rhs.key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
bool Pair<KEY, VALUE>::operator>=(const Pair& rhs) const
{
	return key >= rhs.key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
const KEY& Pair<KEY, VALUE>::Key() const
{
	return key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
const VALUE& Pair<KEY, VALUE>::Value() const
{
	return value;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE>
VALUE& Pair<KEY, VALUE>::Value()
{
	return value;
}