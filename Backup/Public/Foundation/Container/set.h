#pragma once

//------------------------------------------------------------------==========================
/*	Dynamic Set Container Class
	- value가 순차적으로 정렬되어서 삽입된다.
	- 
*/
//------------------------------------------------------------------==========================
template <typename TYPE, typename ALLOCATOR = Allocator>
class Set
{
public:
	Set();
	Set(SizeT initCapacity);
	Set(std::initializer_list<TYPE> list);
	Set(const Set<TYPE, ALLOCATOR>& other);
	Set(Set<TYPE, ALLOCATOR>&& other);
	~Set();

	void operator=(const Set& rhs);
	void operator=(Set&& rhs);

	IndexT Add(const TYPE& value);
	void Remove(const TYPE& value);

	SizeT Size() const;
	SizeT Capacity() const;
	bool IsEmpty() const;

	void Reserve(SizeT size);
	void Shrink();
	void Clear();

	bool Contains(const TYPE& value) const;
	IndexT FindIndex(const TYPE& value) const;

	TYPE& ValueAt(IndexT index);
	const TYPE& ValueAt(IndexT index) const;

	/// C++ conform begin, MAY RETURN nullptr!
	TYPE* begin();
	/// C++ conform begin, MAY RETURN nullptr!
	const TYPE* begin() const;
	/// C++ conform end,  MAY RETURN nullptr!
	TYPE* end();
	/// C++ conform end, MAY RETURN nullptr!
	const TYPE* end() const;

private:
	Array<TYPE, ALLOCATOR> m_Elements;
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Set<TYPE, ALLOCATOR>::Set()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Set<TYPE, ALLOCATOR>::Set(SizeT initCapacity)
	: m_Elements(initCapacity)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Set<TYPE, ALLOCATOR>::Set(const Set<TYPE, ALLOCATOR>& other)
	: m_Elements(other.m_Elements)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Set<TYPE, ALLOCATOR>::Set(Set<TYPE, ALLOCATOR>&& other)
	//: m_Elements(std::move(other.m_Elements))
	: m_Elements(Template::Move(other.m_Elements))
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Set<TYPE, ALLOCATOR>::Set(std::initializer_list<TYPE> list)
	: Set()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Set<TYPE, ALLOCATOR>::~Set()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Set<TYPE, ALLOCATOR>::operator=(const Set& other)
{
	if (&other != this)
	{
		m_Elements = other.m_Elements;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Set<TYPE, ALLOCATOR>::operator=(Set&& other)
{
	if (&other != this)
	{
		//m_Elements = std::move(other.m_Elements);
		m_Elements = Template::Move(other.m_Elements);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Set<TYPE, ALLOCATOR>::Size() const
{
	return m_Elements.Size();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Set<TYPE, ALLOCATOR>::Capacity() const
{
	return m_Elements.Capacity();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
bool Set<TYPE, ALLOCATOR>::IsEmpty() const
{
	return m_Elements.IsEmpty();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Set<TYPE, ALLOCATOR>::Reserve(SizeT size)
{
	m_Elements.Reserve(size);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Set<TYPE, ALLOCATOR>::Shrink()
{
	m_Elements.Shrink();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Set<TYPE, ALLOCATOR>::Clear()
{
	m_Elements.Clear();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
bool Set<TYPE, ALLOCATOR>::Contains(const TYPE& value) const
{
	return std::binary_search(m_Elements.begin(), m_Elements.end(), value);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Set<TYPE, ALLOCATOR>::FindIndex(const TYPE& value) const
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), value);
	if (it != m_Elements.end())
		return IndexT(it - m_Elements.begin());

	return INVALID_INDEX;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE& Set<TYPE, ALLOCATOR>::ValueAt(IndexT index)
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	//_ASSERT(index < Size());

	return m_Elements[index];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
const TYPE& Set<TYPE, ALLOCATOR>::ValueAt(IndexT index) const
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	//_ASSERT(index < Size());

	return m_Elements[index];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Set<TYPE, ALLOCATOR>::Add(const TYPE& value)
{
	const TYPE* begin = m_Elements.begin();
	const TYPE* end = m_Elements.end();
	const TYPE* ptr = std::lower_bound(begin, end, value);
	if ((ptr != end) && (value == *ptr))
		return (IndexT)Template::Distance(begin, ptr);

	IndexT index = (IndexT)Template::Distance(begin, ptr);
	m_Elements.Insert(index, value);
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Set<TYPE, ALLOCATOR>::Remove(const TYPE& value)
{
	const TYPE* begin = m_Elements.begin();
	const TYPE* end = m_Elements.end();
	const TYPE* ptr = std::lower_bound(begin, end, value);
	if (ptr != end) 
	{
		IndexT index = (IndexT)Template::Distance(begin, ptr);
		m_Elements.RemoveAt(index);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE* Set<TYPE, ALLOCATOR>::begin()
{
	return m_Elements.begin();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
const TYPE* Set<TYPE, ALLOCATOR>::begin() const
{
	return m_Elements.begin();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE* Set<TYPE, ALLOCATOR>::end()
{
	return m_Elements.end();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
const TYPE* Set<TYPE, ALLOCATOR>::end() const
{
	return m_Elements.end();
}

