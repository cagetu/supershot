#pragma once

//------------------------------------------------------------------==========================
/*	Dynamic Map Container Class
	- Red-Black Tree를 사용하는 Map이 아니라, Pair<Key, Value>를 가지는 Array 컨테이너이다.
*/
//------------------------------------------------------------------==========================
template <typename KEY, typename VALUE, typename ALLOCATOR = Allocator>
class Map
{
public:
	Map();
	Map(const Map& other);
	Map(Map&& other);
	~Map();

	void operator=(const Map& other);
	void operator=(Map&& other);

	// Key 값에 값을 넣어준다. 단, 같은 Key가 들어올 때에는 Overwrite하지 않는다.
	IndexT Insert(const KEY& key, const VALUE& value);
	// Key 값에 값을 넣어준다. 단, 같은 Key가 들어올 때에는 Overwrite하지 않는다.
	IndexT Insert(const Pair<KEY, VALUE>& item);
	// Key 값에 값을 넣어준다. 단, 같은 Key가 들어올 때에는 Overwrite하지 않는다.
	IndexT Insert(Pair<KEY, VALUE>&& item);

	void Remove(const KEY& key);
	void RemoveAll(const KEY& key);
	void RemoveAt(IndexT index, bool bShrink = false);

	VALUE& operator[](const KEY& key);
	const VALUE& operator[](const KEY& key) const;

	const KEY& KeyAt(IndexT index) const;
	VALUE& ValueAt(IndexT index);
	const VALUE& ValueAt(IndexT index) const;

	SizeT Size() const;
	SizeT Capacity() const;
	bool IsEmpty() const;

	void Reserve(SizeT size);
	void Shrink();
	void Clear();

	bool Contains(const KEY& key) const;
	IndexT FindIndex(const KEY& key) const;
	VALUE& FindOrAdd(const KEY& key);

	/// C++ conform begin, MAY RETURN nullptr!
	Pair<KEY, VALUE>* begin();
	/// C++ conform begin, MAY RETURN nullptr!
	const Pair<KEY, VALUE>* begin() const;
	/// C++ conform end,  MAY RETURN nullptr!
	Pair<KEY, VALUE>* end();
	/// C++ conform end, MAY RETURN nullptr!
	const Pair<KEY, VALUE>* end() const;

private:
	Array<Pair<KEY, VALUE>, ALLOCATOR> m_Elements;
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::Map()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::Map(const Map& other)
	: m_Elements(other.m_Elements)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::Map(Map&& other)
	//: m_Elements((Array<Pair<KEY, VALUE>>&&)other.m_Elements)	
	//: m_Elements(std::move(other.m_Elements))
	: m_Elements(Template::Move(other.m_Elements))
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Map<KEY, VALUE, ALLOCATOR>::~Map()
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::operator=(const Map& other)
{
	m_Elements = other.m_Elements;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::operator=(Map&& other)
{
	//m_Elements = std::move(other.m_Elements);
	m_Elements = Template::Move(other.m_Elements);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
SizeT Map<KEY, VALUE, ALLOCATOR>::Size() const
{
	return m_Elements.Size();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
SizeT Map<KEY, VALUE, ALLOCATOR>::Capacity() const
{
	return m_Elements.Capacity();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
bool Map<KEY, VALUE, ALLOCATOR>::IsEmpty() const
{
	return m_Elements.IsEmpty();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Reserve(SizeT size)
{
	m_Elements.Reserve(size);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Shrink()
{
	m_Elements.Shrink();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Clear()
{
	m_Elements.Clear();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
VALUE& Map<KEY, VALUE, ALLOCATOR>::operator[](const KEY& key)
{
	_ASSERT(m_Elements.Data());
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	_ASSERT(it != m_Elements.end() && (key == it->key));
	return it->Value();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const VALUE& Map<KEY, VALUE, ALLOCATOR>::operator[](const KEY& key) const
{
	_ASSERT(m_Elements.Data());
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	_ASSERT(it != m_Elements.end() && (key == it->key));
	return it->Value();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const KEY& Map<KEY, VALUE, ALLOCATOR>::KeyAt(IndexT index) const
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	_ASSERT(index < Size());

	return m_Elements[index].key;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const VALUE& Map<KEY, VALUE, ALLOCATOR>::ValueAt(IndexT index) const
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	//_ASSERT(index < Size());

	return m_Elements[index].value;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
VALUE& Map<KEY, VALUE, ALLOCATOR>::ValueAt(IndexT index)
{
	_ASSERT(index > INVALID_INDEX);
	_ASSERT(m_Elements.Data());
	//_ASSERT(index < Size());

	return m_Elements[index].value;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
bool Map<KEY, VALUE, ALLOCATOR>::Contains(const KEY& key) const
{
	return std::binary_search(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::FindIndex(const KEY& key) const
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	if ((it != m_Elements.end()) && (key == it->Key()))
		return IndexT(it - m_Elements.begin());

	return INVALID_INDEX;
}

////-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
VALUE& Map<KEY, VALUE, ALLOCATOR>::FindOrAdd(const KEY& key)
{
	IndexT index = FindIndex(key);
	if (index == INVALID_INDEX)
	{
		index = Insert(key, VALUE());
	}
	return ValueAt(index);
}

////-----------------------------------------------------------------------------------------------------------------------------------------------------
//template <typename KEY, typename VALUE, typename ALLOCATOR>
//IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const KEY& key, const VALUE& value)
//{
//	return Insert(Pair<KEY, VALUE>(key, value));
//}
//
////-----------------------------------------------------------------------------------------------------------------------------------------------------
//template <typename KEY, typename VALUE, typename ALLOCATOR>
//IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const Pair<KEY, VALUE>& item)
//{
//	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
//	IndexT index = IndexT(it - m_Elements.begin());
//	return m_Elements.Insert(index, item);
//}
//
////-----------------------------------------------------------------------------------------------------------------------------------------------------
//template <typename KEY, typename VALUE, typename ALLOCATOR>
//IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(Pair<KEY, VALUE>&& item)
//{
//	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
//	IndexT index = IndexT(it - m_Elements.begin());
//	return m_Elements.Insert(index, item);
//}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const KEY& key, const VALUE& value)
{
	return Insert(Pair<KEY, VALUE>(key, value));
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(const Pair<KEY, VALUE>& item)
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
	if ((it != m_Elements.end()) && (item.key == it->key))
	{
		//return IndexT(it - m_Elements.begin());
		return INVALID_INDEX; // 같은 값이 있으면 실패라고 해버리자!!!
	}

	IndexT index = IndexT(it - m_Elements.begin());	// Size() - 1;
	return m_Elements.Insert(index, item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
IndexT Map<KEY, VALUE, ALLOCATOR>::Insert(Pair<KEY, VALUE>&& item)
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), item);
	if ((it != m_Elements.end()) && (item.key == it->key))
	{
		//return IndexT(it - m_Elements.begin());
		return INVALID_INDEX; // 같은 값이 있으면 실패라고 해버리자!!!
	}

	IndexT index = IndexT(it - m_Elements.begin());	// Size() - 1;
	return m_Elements.Insert(index, item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::Remove(const KEY& key)
{
	IndexT index = FindIndex(key);
	if (index > INVALID_INDEX)
	{
		m_Elements.RemoveAt(index);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::RemoveAll(const KEY& key)
{
	auto it = std::lower_bound(m_Elements.begin(), m_Elements.end(), Pair<KEY, VALUE>(key, VALUE()));
	if (it != m_Elements.end())
	{
		const IndexT index = IndexT(it - m_Elements.begin());
		while (index < m_Elements.Size() && m_Elements[index].Key() == key)
		{
			m_Elements.RemoveAt(index);
		}
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
void Map<KEY, VALUE, ALLOCATOR>::RemoveAt(IndexT index, bool bShrink)
{
	m_Elements.RemoveAt(index, bShrink);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::begin()
{
	return m_Elements.begin();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::begin() const
{
	return m_Elements.begin();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::end()
{
	return m_Elements.end();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename KEY, typename VALUE, typename ALLOCATOR>
const Pair<KEY, VALUE>* Map<KEY, VALUE, ALLOCATOR>::end() const
{
	return m_Elements.end();
}

