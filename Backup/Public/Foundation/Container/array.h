#pragma once
// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************

/**	Dynamic Array Container Class
	@author cagetu
	@desc
		- UE4의 TArray의 영향을 많이 받았다.
		- oryol의 Array 클래스도 같이 참고
*/
template <typename TYPE, typename ALLOCATOR = Allocator>
class Array
{
public:
	Array();
	Array(SizeT initCapacity);
	Array(std::initializer_list<TYPE> list);
	Array(const Array<TYPE, ALLOCATOR>& other);
	Array(Array&& other);
	~Array();

	/// type defines
	typedef TYPE& reference;
	typedef const TYPE& const_reference;
	typedef TYPE* iterator;
	typedef const TYPE* const_iterator;

	/// operators
	TYPE& operator[](IndexT Index) const;
	Array& operator=(const Array<TYPE, ALLOCATOR>& other);
	Array& operator=(Array<TYPE, ALLOCATOR>&& other);
	Array& operator=(std::initializer_list<TYPE> list);

	Array<TYPE, ALLOCATOR>& operator+=(std::initializer_list<TYPE> list);
	Array<TYPE, ALLOCATOR>& operator+=(const Array<TYPE, ALLOCATOR>& other);
	Array<TYPE, ALLOCATOR>& operator+=(Array<TYPE, ALLOCATOR>&& other);

	bool operator==(const Array<TYPE, ALLOCATOR>& other) const;
	bool operator!=(const Array<TYPE, ALLOCATOR>& other) const;

	/// 사용 중인 Element 개수
	SizeT Size() const;
	/// 할당된 최대 Element 개수
	SizeT Capacity() const;
	/// 비어 있는지 확인
	bool IsEmpty() const;
	/// 사용 중이 메모리 크기
	SizeT UsedBytes() const;
	/// 할당된 메모리 크기
	SizeT AllocatedBytes() const;
	/// Element 포인터
	TYPE* Data() const;

	/// Size만큼 버퍼 크기를 확보한다.
	void Reserve(SizeT size);
	/// Size만큼 버퍼 크기를 할당한다.
	void Resize(SizeT size);
	/// 사용하는 크기에 맞게 최대 크기를 맞춘다.
	void Shrink();
	/// Size를 0으로 하지만, 메모리 변경이 없이 초기화 한다.
	void Reset(SizeT newSize = 0);
	/// Size를 0으로 한다.
	void Clear(SizeT newSize = 0);

	/// Elements를 추가한다.
	IndexT Add(const TYPE& item);
	/// Element를 추가한다. Move Sementic으로 임시 객체 추가시 시용
	IndexT Add(TYPE&& item);
	/// Element를 추가한다. 같은 Element가 있다면 Index를 반환한다. (FindOrAdd)
	IndexT AddUnique(const TYPE& item);
	/// Element를 추가한다. 같은 Element가 있다면 Index를 반환한다. Move로 임시 객체 추가 시 사용 됨. (FindOrAdd)
	IndexT AddUnique(TYPE&& item);

	// Elements를 추가한다.
	IndexT Insert(IndexT index, std::initializer_list<TYPE> list);
	IndexT Insert(IndexT index, const Array<TYPE, ALLOCATOR>& items);
	IndexT Insert(IndexT index, TYPE* ptr, SizeT count);
	IndexT Insert(IndexT index, const TYPE& item);
	IndexT Insert(IndexT index, TYPE&& item);

	// Elements를 추가한다.
	void Append(const TYPE* ptr, SizeT count);
	void Append(std::initializer_list<TYPE> list);
	void Append(const Array<TYPE, ALLOCATOR>& other);
	void Append(Array<TYPE, ALLOCATOR>&& other);

	IndexT Push(const TYPE& item);
	IndexT Push(TYPE&& item);
	TYPE Pop(bool bShrink = false);

	// Element를 제거한다.
	SizeT Remove(const TYPE& item);
	SizeT RemoveAll(const TYPE& item);
	SizeT RemoveAt(IndexT index, bool bShrink = false);
	SizeT RemoveAt(IndexT index, SizeT count, bool bShrink = false);
	SizeT RemoveSwap(const TYPE& item, bool bShrink = false);
	SizeT RemoveSwapAll(const TYPE& item, bool bShrink = false);
	SizeT RemoveAtSwap(IndexT index, bool bShrink = false);

	IndexT FindIndex(const TYPE& item) const;
	TYPE& Find(IndexT index) const;

	TYPE& Front();
	TYPE& Back();

	/// C++ conform begin
	TYPE* begin(); 
	/// C++ conform begin
	const TYPE* begin() const;
	/// C++ conform end
	TYPE* end();
	/// C++ conform end
	const TYPE* end() const;

	void Sort();
	void Swap(IndexT firstIndex, IndexT secondIndex);
	bool BinarySearch(const TYPE& item);

public:
	/// array 안으로 count 만큼의 element를 추가한다. (생성자 호출없이 element를 생성한다. 필요에 따라 생성자 호출해줘야 한다.)
	IndexT AddUnInitialized(SizeT count = 1);
	/// array 안으로 count 만큼의 element를 추가한다. (기본 생성자로 element를 생성한다.)
	IndexT AddDefaulted(SizeT count = 1);
	/// array 안으로 count 만큼의 element를 추가한다. (Zero Initialized)
	IndexT AddZeroed(SizeT count = 1);

	/// array 안으로 count 만큼의 element를 추가한다. (생성자 호출없이 element를 생성한다. 필요에 따라 생성자 호출해줘야 한다.) 원하는 위치 이후의 Element들에 대해서는 재배치가 된다.
	void InsertUnInitialized(IndexT index, SizeT count = 1);
	/// array 안으로 count 만큼의 element를 추가한다. (기본 생성자로 element를 생성한다.) 원하는 위치 이후의 Element들에 대해서는 재배치가 된다.
	void InsertDefaulted(IndexT index, SizeT count = 1);
	/// array 안으로 count 만큼의 element를 추가한다. (Zero Initialized) 원하는 위치 이후의 Element들에 대해서는 재배치가 된다.
	void InsertZeroed(IndexT index, SizeT count = 1);

private:
	/// Element만큼 할당
	void Allocate(SizeT newCapacity);
	/// 모든 Element들을 제거한다.
	void Deallocate();
	/// 최대 사이즈만큼 재할당한다.
	void Reallocate(SizeT newCapacity);
	/// 다른 Array의 Element를 복사한다.
	void Copy(const Array<TYPE, ALLOCATOR>& other);
	/// 버퍼를 늘린다. 
	void ResizeGrow(SizeT size);
	/// 버퍼 늘린 크기를 구한다. 
	SizeT ComputeGrothCapacity(SizeT size, SizeT capacity);

	/// 끝에 하나의 Element 하나를 추가한다. 
	template <typename... ArgsType> FORCEINLINE IndexT Emplace(ArgsType&&... Args);
	/// 원하는 위치에 Element 하나를 추가한다.
	template <typename... ArgsType>	FORCEINLINE void EmplaceAt(IndexT Index, ArgsType&&... Args);

	template <typename ArgsType> int32 __AddUnique(ArgsType&& Args);

	/// RemoveAt...
	void __RemoveAt(IndexT index, SizeT count, bool bShrink = false);
	void __RemoveAtSwap(IndexT index, SizeT count, bool bShrink = false);

	ALLOCATOR m_Allocator;
	TYPE* m_Elements;

	SizeT m_Size;			// 현재 개수
	SizeT m_Capacity;		// 최대 개수
};

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>::Array()
	: m_Size(0)
	, m_Capacity(0)
	, m_Elements(nullptr)
{
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>::Array(SizeT initCapacity)
	: m_Size(0)
	, m_Capacity(initCapacity)
	, m_Elements(nullptr)
{
	if (m_Capacity > 0)
	{
		Reserve(capacity);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>::Array(const Array<TYPE, ALLOCATOR>& other)
	: m_Size(0)
	, m_Capacity(0)
	, m_Elements(nullptr)
	, m_Allocator(other.m_Allocator)
{
	Allocate(other.Size());
	Memory::ConstructItems<TYPE>(m_Elements, other.m_Elements, other.Size());
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>::Array(Array&& other)
	: m_Size(0)
	, m_Capacity(0)
	, m_Elements(nullptr)
	, m_Allocator(other.m_Allocator)
{
	m_Elements = other.m_Elements;
	m_Capacity = other.m_Capacity;
	m_Size = other.m_Size;

	other.m_Size = 0;
	other.m_Capacity = 0;
	other.m_Elements = nullptr;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>::Array(std::initializer_list<TYPE> list)
	: Array()
{
	SizeT count = (SizeT)list.size();
	Allocate(count);
	Memory::ConstructItems<TYPE>(m_Elements, list.begin(), count);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>::~Array()
{
	Deallocate();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE& Array<TYPE, ALLOCATOR>::operator[](IndexT index) const
{
	_ASSERT(index < (int32)m_Size);
	return m_Elements[index];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>& Array<TYPE, ALLOCATOR>::operator=(const Array<TYPE, ALLOCATOR>& other)
{
	Copy(other);
	//if (this != &other)
	//{
	//	Memory::DestructItems(m_Elements, m_Size);
	//	Copy(other);
	//}
	return *this;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>& Array<TYPE, ALLOCATOR>::operator=(Array<TYPE, ALLOCATOR>&& other)
{
	Deallocate();
	m_Size = other.m_Size;
	m_Capacity = other.m_Capacity;
	m_Elements = other.m_Elements;

	other.m_Size = 0;
	other.m_Capacity = 0;
	other.m_Elements = nullptr;
	return *this;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>& Array<TYPE, ALLOCATOR>::operator=(std::initializer_list<TYPE> list)
{
	Memory::DestructItems(m_Elements, m_Size);

	m_Size = list.size();
	if (m_Size)
	{
		Resize(m_Size);
		Memory::ConstructItems(m_Elements, list.begin(), m_Size);
	}
	else
	{
		m_Capacity = 0;
	}
	return *this;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
bool Array<TYPE, ALLOCATOR>::operator==(const Array<TYPE, ALLOCATOR>& other) const
{
	if (Size() != other.Size())
		return false;

	const int32 num = (int32)Size();
	for (IndexT index = 0; index < num; index++)
	{
		if (m_Elements[index] != other.m_Elements[index])
			return false;
	}
	return true;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
bool Array<TYPE, ALLOCATOR>::operator!=(const Array<TYPE, ALLOCATOR>& other) const
{
	return !(*this == other);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>& Array<TYPE, ALLOCATOR>::operator+=(const Array<TYPE, ALLOCATOR>& other)
{
	Append(other);
	return *this;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>& Array<TYPE, ALLOCATOR>::operator+=(Array<TYPE, ALLOCATOR>&& other)
{
	Append(other);
	return *this;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
Array<TYPE, ALLOCATOR>& Array<TYPE, ALLOCATOR>::operator+=(std::initializer_list<TYPE> list)
{
	Append(list);
	return *this;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Allocate(SizeT newCapacity)
{
	m_Elements = m_Allocator.NewPODs<TYPE>(newCapacity);
	m_Size = newCapacity;
	m_Capacity = newCapacity;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Deallocate()
{
	if (m_Elements)
	{
		m_Allocator.DeletePODs<TYPE>(m_Elements, m_Capacity);
		m_Elements = nullptr;
	}
	m_Size = 0;
	m_Capacity = 0;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Reallocate(SizeT newCapacity)
{
	TYPE* elements = m_Allocator.NewPODs<TYPE>(newCapacity);
	if (m_Elements)
	{
		const int32 num = (int32)Size();
		Memory::ConstructItems<TYPE>(elements, num);
		for (IndexT index = 0; index < num; index++)
			elements[index] = m_Elements[index];
		m_Allocator.DeletePODs<TYPE>(m_Elements, m_Capacity);
	}

	m_Elements = elements;
	m_Capacity = newCapacity;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Copy(const Array<TYPE, ALLOCATOR>& other)
{
	if (*this == other)
		return;

	// Realloc
	if (other.Size() > m_Capacity)
	{
		Deallocate();
		Allocate(other.Size());
	}

	m_Size = other.Size();
	for (IndexT index = 0; index < m_Size; index++)
		m_Elements = rhs.m_Elements[index];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::ResizeGrow(SizeT size)
{
	const SizeT newCapacity = ComputeGrothCapacity(size, m_Capacity);
	Reallocate(newCapacity);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::ComputeGrothCapacity(SizeT size, SizeT capacity)
{
#if 0
	/*	FBVector의 방법이 제일 마음에 들어서 적용해본다. (fbvector::computePushBackCapacity)
		1. initGrow: 64Byte보다 크도록 개수를 설정해준다.
		2. 아주 작거나 큰 vector에 대해서는 2배 크기로 성장한다. (이건 테스트 해보니 그렇다고 한다.)
		3. 중간 크기의 버퍼에 대해서는 1.5x 배로 성장한다.
	*/
	if (capacity == 0)
		return 4; // return ::max(64 / sizeof(TYPE), 1);

	if (capacity < 4096 / sizeof(TYPE))
		return capacity * 2;

	if (capacity < 4096 * 32 / sizeof(TYPE))
		return capacity * 2;

	return (capacity * 3 + 1) / 2;
#else
	// UE4에서 사용하는 방식이다. 
	SizeT groth = 4;
	if (capacity > 0 || size > groth)
	{
		groth = size + 3 * size / 8 + 16;
	}
	if (size > groth)
	{
		groth = (int32)0x7fffffff;
	}
	return groth;
#endif
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Reserve(SizeT size)
{
	//SizeT newSize = m_Size + size;
	if (size > m_Capacity)
	{
		Reallocate(size);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Resize(SizeT newCapacity)
{
	if (newCapacity)
	{
		newCapacity = ComputeGrothCapacity(newCapacity, m_Capacity);
	}

	if (newCapacity != m_Capacity)
	{
		m_Capacity = newCapacity;
		Reallocate(m_Capacity);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Shrink()
{
	if (m_Size < m_Capacity)
	{
		Reallocate(m_Size);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Reset(SizeT newSize)
{
	if (newSize <= m_Capacity)
	{
		Memory::DestructItems(m_Elements, m_Size);
		m_Size = 0;
	}
	else
	{
		Clear(newSize);
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Clear(SizeT newSize)
{
	Memory::DestructItems(m_Elements, m_Size);

	CHECK(newSize >= 0, TEXT("Array::Clear"));

	m_Size = 0;

	Resize(newSize);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::Size() const
{
	return m_Size; 
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::Capacity() const
{
	return m_Capacity; 
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::AllocatedBytes() const
{
	return sizeof(TYPE) * m_Capacity;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::UsedBytes() const
{
	return sizeof(TYPE) * m_Size;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE* Array<TYPE, ALLOCATOR>::Data() const
{
	return m_Elements;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::AddUnInitialized(SizeT count)
{
	IndexT oldNum = m_Size;

	m_Size += count;
	if (m_Size > m_Capacity)
	{
		ResizeGrow(oldNum);
	}
	return oldNum;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::AddZeroed(SizeT count)
{
	IndexT index = AddUnInitialized(count);

	void* ptr = Data() + index;
	Memory::MemZero(ptr, count * sizeof(TYPE));
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::AddDefaulted(SizeT count)
{
	IndexT index = AddUnInitialized(count);

	TYPE* ptr = Data() + index;
	Memory::ConstructItems<TYPE>(ptr, count);
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::InsertUnInitialized(IndexT index, SizeT count)
{
	IndexT oldNum = m_Size;

	m_Size += count;
	if (m_Size > m_Capacity)
	{
		ResizeGrow(oldNum);
	}
	
	TYPE* src = Data() + index;
	void* dest = src + count;

	Memory::RelocateConstructItems<TYPE>(dest, src, oldNum - index);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::InsertZeroed(IndexT index, SizeT count)
{
	InsertUnInitialized(index, count);

	TYPE* ptr = Data() + index;
	Memory::MemZero(ptr, count * sizeof(TYPE));
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::InsertDefaulted(IndexT index, SizeT count)
{
	InsertUnInitialized(index, count);

	TYPE* ptr = Data() + index;
	Memory::ConstructItems<TYPE>(ptr, count);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
template <typename... ArgsType>
IndexT Array<TYPE, ALLOCATOR>::Emplace(ArgsType&&... Args)
{
	const IndexT index = AddUnInitialized(1);

	TYPE* ptr = Data() + index;
	//new(ptr) TYPE(std::forward<ArgsType>(Args)...);
	new(ptr) TYPE(Template::Forward<ArgsType>(Args)...);
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
template <typename... ArgsType>
void Array<TYPE, ALLOCATOR>::EmplaceAt(IndexT index, ArgsType&&... Args)
{
	InsertUnInitialized(index, 1);
	
	TYPE* ptr = Data() + index;
	new(ptr) TYPE(Template::Forward<ArgsType>(Args)...);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
bool Array<TYPE, ALLOCATOR>::IsEmpty() const
{
	return (m_Size == 0);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE& Array<TYPE, ALLOCATOR>::Front()
{
	return m_Elements[0];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE& Array<TYPE, ALLOCATOR>::Back()
{
	return m_Elements[m_Size - 1];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE* Array<TYPE, ALLOCATOR>::begin()
{
	return (m_Size > 0) ? &m_Elements[0] : nullptr;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
const TYPE* Array<TYPE, ALLOCATOR>::begin() const
{
	return (m_Size > 0) ? &m_Elements[0] : nullptr;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE* Array<TYPE, ALLOCATOR>::end()
{
	return (m_Size > 0) ? &m_Elements[m_Size] : nullptr;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
const TYPE* Array<TYPE, ALLOCATOR>::end() const
{
	return (m_Size > 0) ? &m_Elements[m_Size] : nullptr;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Push(const TYPE& item)
{
	return Emplace(item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Push(TYPE&& item)
{
	return Emplace(item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE Array<TYPE, ALLOCATOR>::Pop(bool bShrink)
{
	TYPE lastElement = m_Elements[m_Size-1];
	RemoveAt(m_Size - 1, bShrink);
	return lastElement;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Append(const Array<TYPE, ALLOCATOR>& other)
{
	SizeT num = other.Size();
	if (!num)
		return;

	Reserve(m_Size + other.Size());
	Memory::ConstructItems<TYPE>(m_Elements + m_Size, other.Data(), num);

	m_Size += num;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Add(const TYPE& item)
{
	return Emplace(item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Add(TYPE&& item)
{
	return Emplace(item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::AddUnique(const TYPE& item)
{
	return __AddUnique(item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::AddUnique(TYPE&& item)
{
	return __AddUnique(item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
template <typename ArgsType> 
int32 Array<TYPE, ALLOCATOR>::__AddUnique(ArgsType&& Args)
{
	IndexT index = FindIndex(Args);
	if (index > INVALID_INDEX)
		return (IndexT)index;

	return Add(std::forward<ArgsType>(Args));
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::Remove(const TYPE& item)
{
	IndexT index = FindIndex(item);
	_ASSERT(index > INVALID_INDEX);

	RemoveAt(index, false);
	return 1;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
// Array안에 있는 같은 Element들을 모두 지운다.
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::RemoveAll(const TYPE& item)
{
	const SizeT count = m_Size;

	IndexT readIndex = 0;
	IndexT destIndex = 0;
	bool NotMatch = !(m_Elements[readIndex] == item);
	do
	{
		int32 RunStartIndex = readIndex++;
		while (readIndex < count && NotMatch == !(m_Elements[readIndex] == item))
		{
			readIndex++;
		}
		int32 RunLength = readIndex - RunStartIndex;
		if (NotMatch)
		{
			// this was a non-matching run, we need to move it
			if (destIndex != RunStartIndex)
			{
				Memory::MemMove(&m_Elements[destIndex], &m_Elements[RunStartIndex], sizeof(TYPE)* RunLength);
			}
			destIndex += RunLength;
		}
		else
		{
			// this was a matching run, delete it
			DestructItems(m_Elements + RunStartIndex, RunLength);
		}
		NotMatch = !NotMatch;
	} while (readIndex < count);

	m_Size = writeIndex;
	return count - m_Size;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::RemoveAt(IndexT index, bool bShrink)
{
	__RemoveAt(index, 1, bShrink);
	return 1;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::RemoveAt(IndexT index, SizeT count, bool bShrink)
{
	__RemoveAt(index, count, bShrink);
	return count;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::RemoveSwap(const TYPE& item, bool bShrink)
{
	IndexT index = FindIndex(item);
	_ASSERT(index >= 0);

	__RemoveAtSwap(index, 1, bShrink);
	return 1;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::RemoveSwapAll(const TYPE& item, bool bShrink)
{
	const int32 OriginalNum = m_Size;
	for (int32 Index = 0; Index < m_Size; Index++)
	{
		if ((*this)[Index] == item)
		{
			RemoveAtSwap(Index--);
		}
	}
	return OriginalNum - m_Size;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
SizeT Array<TYPE, ALLOCATOR>::RemoveAtSwap(IndexT index, bool bShrink)
{
	__RemoveAtSwap(index, 1, bShrink);
	return 1;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::__RemoveAt(IndexT index, SizeT count, bool bShrink)
{
	SizeT numElements = m_Size - index - count;

	TYPE* dest = m_Elements + index;
	Memory::DestructItems(dest, count);

	void* src = m_Elements + (index + count);
	Memory::MemMove(dest, src, numElements * sizeof(TYPE));

	m_Size -= count;

	if (bShrink)
		Shrink();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
// 앞에서 Index에서 Count 만큼을 지우고, 뒤에서 Index + Count 만큼에서 채운다.
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::__RemoveAtSwap(IndexT index, SizeT count, bool bShrink)
{
	TYPE* dest = m_Elements + index;
	Memory::DestructItems(dest, count);

	// 뒤에서 count 만큼... 
	SizeT swapCount = m_Size - (index + count);
	swapCount = Math::Min<SizeT>(count, swapCount);
	if (swapCount > 0)
	{
		void* src = m_Elements + swapCount;
		Memory::MemCpy(dest, src, sizeof(TYPE) * swapCount);
	}
	m_Size -= count;

	if (bShrink)
		Shrink();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Insert(IndexT index, std::initializer_list<TYPE> list)
{
	const SizeT count = (SizeT)list.size();

	InsertUnInitialized(index, count);

	for (IndexT i = 0; i < count; i++)
		CONSTRUCT(m_Elements + offset++, TYPE, items[i]);

	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Insert(IndexT index, const Array<TYPE, ALLOCATOR>& items)
{
	const SizeT count = items.Size();

	InsertUnInitialized(index, items.Size());
	Memory::ConstructItems<TYPE>(m_Elements + index, items.Data(), items.Size());

	//IndexT offset = index;
	//for (IndexT i = 0; i < count; i++)
	//	CONSTRUCT(m_Elements + offset++, TYPE, items[i]);

	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Insert(IndexT index, TYPE* ptr, SizeT count)
{
	InsertUnInitialized(index, count);
	Memory::ConstructItems<TYPE>(m_Elements + index, ptr, count);
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Insert(IndexT index, const TYPE& item)
{
	InsertUnInitialized(index, 1);
	CONSTRUCT(m_Elements + index, TYPE, item);
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::Insert(IndexT index, TYPE&& item)
{
	InsertUnInitialized(index, 1);
	CONSTRUCT(m_Elements + index, TYPE, item);
	return index;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Append(Array<TYPE, ALLOCATOR>&& other)
{
	SizeT num = other.Size();
	if (!num)
		return;

	Reserve(m_Size + other.Size());

	// Move (Source는 모두 사라진다.)
	Memory::RelocateConstructItems<TYPE>(m_Elements + m_Size, other.Data(), num);
	other.m_Size = 0;

	m_Size += num;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Append(const std::initializer_list<TYPE> list)
{
	SizeT count = (SizeT)list.size();

	IndexT index = AddUnInitialized(count);
	Memory::ConstructItems<TYPE>(m_Elements + index, list.begin(), count);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Append(const TYPE* ptr, SizeT count)
{
	IndexT index = AddUnInitialized(count);
	Memory::ConstructItems<TYPE>(m_Elements + index, ptr, count);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Sort()
{
	std::sort(m_Elements, m_Elements + m_Size, std::less<TYPE>());
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
void Array<TYPE, ALLOCATOR>::Swap(IndexT firstIndex, IndexT secondIndex)
{
	TYPE element = m_Elements[firstIndex];
	m_Elements[firstIndex] = m_Elements[secondIndex];
	m_Elements[secondIndex] = Template::Forward<TYPE>(element);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
bool Array<TYPE, ALLOCATOR>::BinarySearch(const TYPE& item)
{
	return std::binary_search(begin(), end(), item);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
TYPE& Array<TYPE, ALLOCATOR>::Find(IndexT index) const
{
	_ASSERT(index < (int32)m_Size);
	return m_Elements[index];
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
template <typename TYPE, typename ALLOCATOR>
IndexT Array<TYPE, ALLOCATOR>::FindIndex(const TYPE& item) const
{
	TYPE* start = m_Elements;
	const TYPE* end = start + m_Size;

	for (const TYPE* element = start; element != end; ++element)
	{
		if (*element == item)
		{
			return static_cast<IndexT>(element - start);
		}
	}

	return INVALID_INDEX;
}
