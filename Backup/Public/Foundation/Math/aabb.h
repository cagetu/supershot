#ifndef _aabb_
#define _aabb_

struct Mat4;
struct Sphere;
struct Plane;

struct AABB
{
	float	minx, miny, minz;
	float	maxx, maxy, maxz;

	AABB() { minx = miny = minz = 99999.0f; maxx = maxy = maxz = -99999.0f; }
	AABB(const Vec3 & min, const Vec3 & max) { minx = min.x, miny = min.y, minz = min.z, maxx = max.x, maxy = max.y, maxz = max.z; }
	AABB(const Vec3 & p) { minx = maxx = p.x, miny = maxy = p.y, minz = maxz = p.z; }
	AABB(const Vec3* points, int num);

	void Reset() { minx = miny = minz = 99999.0f; maxx = maxy = maxz = -99999.0f; }

	const Vec3 & Min() const { return *(Vec3*)&minx; }
	const Vec3 & Max() const { return *(Vec3*)&maxx; }

	Vec3 Center() const { return Vec3((minx+maxx)*0.5f, (miny+maxy)*0.5f, (minz+maxz)*0.5f); }
	Vec3 Size() const { return Max() - Min(); }

	void Expand(const Vec3 & p);
	void Expand(const AABB & bound);
	void Expand(float size);
	void Union(const AABB & bound) { Expand(bound); }
	void Intersection(const AABB & bound);	// ������

	AABB Transform(const Mat4 & tm) const;
	void Translate(const Vec3& v);

	void GetPlanes(Plane* planes) const;
	void GetPoints(Vec3* points) const;
	float Distance(Vec3& point) const;

	bool IntersectTest(const AABB & bound) const;
	bool IntersectTest(const Sphere& s) const;

	float RayIntersect(const Vec3 & p, const Vec3 & dir, Plane* out=NULL) const;
	float SweepTest(const Sphere& pos, const Vec3& vec, Plane* out=NULL) const;

	bool BoundTest(const Vec3 &startp, const Vec3 &direct, float radius) const;
	bool InBound(const Vec3 & p) const { return p.x >= minx && p.x <= maxx && p.y >= miny && p.y <= maxy && p.z >= minz && p.z <= maxz; }
};

#endif