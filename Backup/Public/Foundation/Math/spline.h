#ifndef __SPLINE__
#define __SPLINE__

namespace Spline
{
	//
	//	Catmull-Rom Spine
	//
	Vec2 CatmullRom(const Vec2 &p0, const Vec2 &p1, const Vec2 &p2, const Vec2 &p3, float t);
	Vec3 CatmullRom(const Vec3 &p0, const Vec3 &p1, const Vec3 &p2, const Vec3 &p3, float t);
	Vec4 CatmullRom(const Vec4 &p0, const Vec4 &p1, const Vec4 &p2, const Vec4 &p3, float t);
}

#endif	// _spline_h_