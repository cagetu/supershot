#ifndef _math_h_
#define _math_h_

//	기본 수학 함수들

#define _PI 			(3.1415926535897932f)
// Aux constants.
#define INV_PI			(0.31830988618f)
#define HALF_PI			(1.57079632679f)

// Copied from float.h
#define MAX_FLT 3.402823466e+38F
#define __EPSILON (1.0e-30f)

#define DEG2RAD(d)	((d)*(_PI/180.0f))
#define RAD2DEG(d)	((d)*(180.0f/_PI))
#define _RAD(d) (d)

#ifndef MAX
#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

struct Math
{
	template<class T>	static CONSTEXPR FORCEINLINE T Min(const T a, const T b) { return (a<b) ? a : b; }
	template<class T>	static CONSTEXPR FORCEINLINE T Max(const T a, const T b) { return (a>b) ? a : b; }
	template<class T>	static CONSTEXPR FORCEINLINE T Abs(const T A) { return (A >= (T)0) ? A : -A; }
	template<class T>	static CONSTEXPR FORCEINLINE T Sign(const T A) { return (A > (T)0) ? (T)1 : ((A < (T)0) ? (T)-1 : (T)0); }
	template<class T>	static CONSTEXPR FORCEINLINE void Swap(T &a, T &b) { T tmp = a; a = b; b = tmp; }

	static FORCEINLINE int32 Trunc(float value) { return (int32)value; }
	static FORCEINLINE int32 Round(float value) { return ((value - ::floor(value) > 0.5) ? (int32)::ceil(value) : (int32)::floor(value)); }
	static FORCEINLINE float Ceil(float value) { return ::ceilf(value); }
	static FORCEINLINE float Floor(float value) { return ::floor(value); }
	static FORCEINLINE float Fractional(float Value) { return Value - (float)Trunc(Value); }
	static FORCEINLINE float Frac(float Value) { return Value - Floor(Value); }

	static FORCEINLINE float Saturate(float value) { return MAX(0.0f, MIN(1.0f, value)); }

	static FORCEINLINE float Clamp(float target, float minValue, float maxValue) { return MAX(minValue, MIN(maxValue, target)); }
	static FORCEINLINE int32 Clamp(int32 target, int32 minValue, int32 maxValue) { return MAX(minValue, MIN(maxValue, target)); }
	static FORCEINLINE uint32 Clamp(uint32 target, uint32 minValue, uint32 maxValue) { return MAX(minValue, MIN(maxValue, target)); }
	//template <class _T> static FORCEINLINE const _T Clamp(const _T& value, const _T& low, const _T& high) { return MIN(MAX(value, low), high);	}

	static FORCEINLINE float Lerp(float x, float y, float l) { return x + l * (y - x); }
	template <class T> static FORCEINLINE T Lerp(T x, T y, float l) { return x + l * (y - x); }

	static FORCEINLINE float Smoothstep(float a, float b, float x)
	{
		float t = Saturate((x - a) / (b - a));
		return t*t*(3.0f - (2.0f - t));
	}

	static FORCEINLINE float Exp(float Value) { return expf(Value); }
	static FORCEINLINE float LogE(float Value) { return logf(Value); }
	static FORCEINLINE float LogX(float Base, float Value) { return LogE(Value) / LogE(Base); }
	static FORCEINLINE float Log2(float Value) { return LogE(Value) * 1.4426950f /* 1.0 / Loge(2) = 1.4426950f */; }

	static FORCEINLINE float FMod(float X, float Y) { return fmodf(X, Y); }
	static FORCEINLINE float Pow(float A, float B) { return powf(A, B); }
	static FORCEINLINE float Sqrt(float value) { return ::sqrtf(value); }
	static FORCEINLINE float InvSqrt(float value) { return 1.0f / ::sqrtf(value); }

	static FORCEINLINE float Cos(float rad) { return ::cosf(rad); }
	static FORCEINLINE float Sin(float rad) { return ::sinf(rad); }
	static FORCEINLINE float Tan(float rad) { return ::tanf(rad); }

	static FORCEINLINE float ACos(float rad)
	{
		if (-1.0f < rad)
			return rad < 1.0f ? (float)::acos(rad) : 0.0f;
		return _PI;
	}
	static FORCEINLINE float ASin(float rad)
	{
		if (-1.0f < rad)
			return rad < 1.0f ? (float)::asin(rad) : HALF_PI;	// Half _PI
		return -HALF_PI;
	}

	static FORCEINLINE float ATan(float rad) { return ::atanf(rad);	}
	static FORCEINLINE float ATan2(float height, float width) { return ::atan2f(height, width); }	// 높이, 너비로 직각삼각형 각도 계산

	static FORCEINLINE float Tan2Sin(float t) { return t / ::sqrtf(t*t + 1.0f); }
	static FORCEINLINE float Tan2Cos(float t) { return 1.0f / ::sqrtf(t*t + 1.0f); }

	static FORCEINLINE bool IsPower2(int32 x) { return (!((x)& ((x)-1))); }

	static FORCEINLINE bool IsNaN(float A) { return ((*(uint*)&A) & 0x7FFFFFFF) > 0x7F800000; }
	static FORCEINLINE bool IsFinite(float A) {	return ((*(uint*)&A) & 0x7F800000) != 0x7F800000; }

	static FORCEINLINE bool IsNearlyEqual(float A, float B, float ErrorTolerance = __EPSILON) {	return Abs<float>(A - B) <= ErrorTolerance;	}
	static FORCEINLINE bool IsNearlyEqual(double A, double B, double ErrorTolerance = __EPSILON) { return Abs<double>(A - B) <= ErrorTolerance; }
	static FORCEINLINE bool IsNearlyZero(float Value, float ErrorTolerance = __EPSILON) { return Abs<float>(Value) <= ErrorTolerance; }
	static FORCEINLINE bool IsNearlyZero(double Value, double ErrorTolerance = __EPSILON) {	return Abs<double>(Value) <= ErrorTolerance; }

	static FORCEINLINE int32 Rand() { return rand(); }

	static void RandInit(int32 seed);
	static int32 RandInt();
	static float RandFloat();
};

#endif	// _math_h_