#ifndef _QUAT_
#define _QUAT_

struct Vec3;

struct Quat4
{
	float x, y, z, w;

	Quat4() {}
	Quat4(float qx, float qy, float qz, float qw) { x = qx; y = qy; z = qz; w = qw; }

	void Decompose(Vec3& axis, float radian) const;

	Quat4 operator - () { return Quat4(-x, -y, -z, -w); }

	inline Quat4 operator += (const Quat4& Q) {	x += Q.x; y += Q.y; z += Q.z; w += Q.w; return *this; }
	inline Quat4 operator -= (const Quat4& Q) {	x -= Q.x; y -= Q.y; z -= Q.z; w -= Q.w;	return *this; }
	inline Quat4 operator *= (const Quat4& Q) { *this = Multiply(*this, Q); return *this; }
	inline Quat4 operator *= (float scale) { x *= scale; y *= scale; z *= scale; w *= scale; return *this; }

	static Quat4 MakeFrom(const Vec3& va, const Vec3& vb);
	static Quat4 MakeFromAxisAngle(const Vec3& axis, float rad);
	static Quat4 Multiply(const Quat4& qa, const Quat4& qb);
	static Quat4 Slerp(const Quat4 &qa, const Quat4 &qb, float w);
	static float DotProduct(const Quat4 &qa, const Quat4 &qb) { return qa.x*qb.x + qa.y*qb.y + qa.z*qb.z + qa.w*qb.w; }

	static Quat4 MakeFromEuler(float rx, float ry, float rz);
	static void	MakeToEuler(const Quat4 &q, float *r);
} ;

inline Quat4 operator + (const Quat4& Q1, const Quat4& Q2) { return Quat4(Q1.x + Q2.x, Q1.y + Q2.y, Q1.z + Q2.z, Q1.w + Q2.w); }
inline Quat4 operator - (const Quat4& Q1, const Quat4& Q2) { return Quat4(Q1.x - Q2.x, Q1.y - Q2.y, Q1.z - Q2.z, Q1.w - Q2.w); }
inline Quat4 operator * (const Quat4& Q1, const Quat4& Q2) { return Quat4::Multiply(Q1, Q2); }
inline Quat4 operator * (const Quat4& Q1, float scale) { return Quat4(Q1.x * scale, Q1.y * scale, Q1.z * scale, Q1.w * scale); }

#endif