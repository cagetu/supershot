#ifndef _obb_
#define _obb_

#include "matrix.h"

struct	OBB
{
	Vec3 Center;
	Vec3 Axis[3];
	float Extent[3];

	OBB() {}
	OBB(const Vec3 & center, const Vec3 * axis, const float * extent);

	static OBB Create(const Vec3* vertices, unsigned int count);

	const Vec3& GetCenter() const { return Center; }
	const Vec3* GetAxises() const { return Axis; }
	const float* GetExtents() const { return Extent; }

	void Expand(const OBB& obb);

	OBB Transform(const Mat4& tm) const;
	void Translate(const Vec3& v);

	void GetVertices(Vec3 * vertices) const;

	// Intersect
	bool IntersectTest(const Vec3& origin, const Vec3& dir, float& outDistance);
	bool IntersectTest(const Vec3& origin, const Vec3& dir);
	bool IntersectTest(const OBB& Obb);
};

#endif
