#ifndef _CAPSULE_
#define _CAPSULE_

struct Sphere;
struct Plane;

struct Capsule
{
	Vec3 v1, v2;
	float radius;

	Capsule() {}
	Capsule(const Vec3 & startp, const Vec3 & endp, float r) { v1 = startp; v2 = endp; radius = r; }

	float SweepTest(const Sphere & sphere, const Vec3 & direct, Plane * out) const;
	bool ContactTest(const Sphere & sphere) const;
} ;

#endif