#ifndef _sphere_
#define _sphere_

struct Plane;
struct Capsule;

struct Sphere
{
	float x, y, z, radius;

	Sphere() {}
	Sphere(const Vec3 &v, float radius) { this->x = v.x, this->y = v.y, this->z = v.z, this->radius = radius; }

	Vec3 & Position() { return *(Vec3*)&x; }
	const Vec3 & Position() const { return *(Vec3*)&x; }

	float	SweepTest(const Sphere& pos, const Vec3& vec, Plane* out=NULL) const;
	float	RayIntersect(const Vec3& pos, const Vec3& vec, Plane* out=NULL) const;
	bool	ContactTest(const Sphere& sphere) const;

	static float SweepTest(const Sphere & sphere, const Vec3 & direct, const Sphere & body, Plane * out) { return body.SweepTest(sphere, direct, out); }
	static float SweepTest(const Sphere & sphere, const Vec3 & direct, const Vec3 * poly, int vtxnum, Plane * out);
	static float SweepTest(const Sphere & sphere, const Vec3 & direct, const Capsule & capsule, Plane * out);
} ;

#endif