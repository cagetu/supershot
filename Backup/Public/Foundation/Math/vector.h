// Copyright (c) 2010~. softnette (visualworks)
//
//******************************************************************
#ifndef _vector_
#define _vector_


//------------------------------------------------------------------
/*	@class Vector2
*/
//------------------------------------------------------------------
struct	Vec2
{
	float x, y;

	Vec2() {}
	Vec2(float x, float y) { this->x = x; this->y = y; }
	Vec2(const float * f) { this->x = f[0]; this->y = f[1]; }  
	Vec2(const Vec2& v) { x = v.x; y = v.y; }

	//	������ 2����
	inline Vec2 xy() const { return Vec2(x, y); }
	inline Vec2 yx() const { return Vec2(y, x); }

	Vec2 operator - () { return Vec2(-x, -y); }

	Vec2& operator += (const Vec2 & v) { this->x += v.x; this->y += v.y; return *this; }
	Vec2& operator -= (const Vec2 & v) { this->x -= v.x; this->y -= v.y; return *this; }
	Vec2& operator *= (float f) { this->x *= f; this->y *= f; return *this; }
	Vec2& operator /= (float f) { this->x /= f; this->y /= f; return *this; }
	
	float Magnitude() const { return Math::Sqrt(x*x + y*y); }
	float MagnitudeSqared() const { return (x*x + y*y); }

	static Vec2 Lerp(const Vec2& a, const Vec2& b, float w) { return Vec2(a.x+(b.x-a.x)*w, a.y+(b.y-a.y)*w); }
	static float Magnitude(const Vec2 &v) { return Math::Sqrt(v.x*v.x + v.y*v.y); }
	static float DotProduct(const Vec2& v1, const Vec2& v2) { return v1.x*v2.x + v1.y*v2.y; }
	static float CrossProduct(const Vec2& v1, const Vec2& v2) { return v1.x*v2.y - v1.y*v2.x; }
	static Vec2 Normalize(const Vec2 &v) { float s = 1.0f / Math::Sqrt(v.x*v.x + v.y*v.y); return Vec2(v.x*s, v.y*s); }
	
	static Vec2 ZERO;
	static Vec2 ONE;
};

inline Vec2 operator - (const Vec2& v1, const Vec2& v2) { return Vec2(v1.x-v2.x, v1.y-v2.y); }
inline Vec2 operator + (const Vec2& v1, const Vec2& v2) { return Vec2(v1.x+v2.x, v1.y+v2.y); }
inline Vec2 operator * (const Vec2& v, float s) { return Vec2(v.x*s, v.y*s); }
inline Vec2 operator * (float s, const Vec2& v) { return Vec2(v.x*s, v.y*s); }

#define Point2 Vec2

extern bool IntersectSegments(const Vec2& seg1Start, const Vec2& seg1End, const Vec2& seg2Start, const Vec2& seg2End, Point2& outContactPoint);

//------------------------------------------------------------------
/*	@class Vector3
*/
//------------------------------------------------------------------
struct	Vec3
{
	float	x, y, z;

	Vec3() {}
	Vec3(float x, float y, float z) { this->x = x, this->y = y, this->z = z; }
	Vec3(const float * f) { this->x = f[0]; this->y = f[1]; this->z = f[2]; } 
	
	//	������ 2����
	inline Vec2 xy() const	{	return Vec2( x, y );	}
	inline Vec2 xz() const	{	return Vec2( x, z );	}
	inline Vec2 yx() const	{	return Vec2( y, x );	}
	inline Vec2 yz() const	{	return Vec2( y, z );	}
	inline Vec2 zx() const	{	return Vec2( z, x );	}
	inline Vec2 zy() const	{	return Vec2( z, y );	}

	//	������ 3����
	inline Vec3 xyz() const	{	return (*this);	}
	inline Vec3 xzy() const	{	return Vec3( x, z, y );	}
	inline Vec3 yxz() const	{	return Vec3( y, x, z );	}
	inline Vec3 yzx() const	{	return Vec3( y, z, x );	}
	inline Vec3 zxy() const	{	return Vec3( z, x, y );	}
	inline Vec3 zyx() const	{	return Vec3( z, y, x );	}

	inline Vec3 operator - () const { return Vec3(-x, -y, -z); }

	inline Vec3& operator += (const Vec3& v) {x += v.x; y += v.y; z += v.z; return *this; }
	inline Vec3& operator -= (const Vec3& v) {x -= v.x; y -= v.y; z -= v.z; return *this; }
	inline Vec3& operator *= (float f) { x *= f; y *= f; z *= f; return *this; }
	inline Vec3& operator /= (float f) { x /= f; y /= f; z /= f; return *this; }
	
	bool operator < (const Vec3 & v) const { return (x < v.x && y < v.y && z < v.z) ? true : false; }
	bool operator > (const Vec3 & v) const { return (x > v.x && y > v.y && z > v.z) ? true : false; }
	
	Vec3 & Normalize() { float s = 1.0f / sqrt(x*x+y*y+z*z); x *= s; y *= s; z *=s; return *this; }

	float Magnitude() const { return Math::Sqrt(x*x + y*y + z*z); }
	float MagnitudeSqared() const { return (x*x + y*y + z*z); }

	static Vec3 Lerp(const Vec3& a, const Vec3& b, float w) { return Vec3(a.x+(b.x-a.x)*w, a.y+(b.y-a.y)*w, a.z+(b.z-a.z)*w); }
	static float Magnitude(const Vec3 &v) { return Math::Sqrt(v.x*v.x + v.y*v.y + v.z*v.z); }
	static Vec3 Multiply(const Vec3& a, const Vec3& b) { return Vec3(a.x*b.x, a.y*b.y, a.z*b.z); }
	static Vec3 Normalize(const Vec3 &v) { float s = 1.0f / Math::Sqrt(v.x*v.x + v.y*v.y + v.z*v.z); return Vec3(v.x*s, v.y*s, v.z*s); }
	static Vec3 CrossProduct(const Vec3 &a, const Vec3 &b) { return Vec3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x); }
	static float DotProduct(const Vec3 &a, const Vec3 &b) { return a.x*b.x + a.y*b.y + a.z*b.z; }

	static Vec3 ZERO;
	static Vec3 ONE;

	static Vec3 XAXIS, YAXIS, ZAXIS;
} ;

inline Vec3 operator * (const Vec3 & v, float s) { return Vec3(v.x*s, v.y*s, v.z*s); }
inline Vec3 operator * (float s, const Vec3 & v) { return Vec3(v.x*s, v.y*s, v.z*s); }
inline Vec3 operator + (const Vec3 & a, const Vec3 & b) { return Vec3(a.x+b.x, a.y+b.y, a.z+b.z); }
inline Vec3 operator - (const Vec3 & a, const Vec3 & b) { return Vec3(a.x-b.x, a.y-b.y, a.z-b.z); }

inline bool operator == (const Vec3 & v1, const Vec3 & v2) { return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z ? true : false; }
inline bool operator != (const Vec3 & v1, const Vec3 & v2) { return !(operator == (v1, v2)); }

#define Point3 Vec3

//------------------------------------------------------------------
/*	@class Vector4
*/
//------------------------------------------------------------------
struct	Vec4
{
	float x, y, z, w;

	Vec4() {}
	Vec4(float x, float y, float z, float w) { this->x = x, this->y = y, this->z = z; this->w = w; }
	Vec4(const float * f) { this->x = f[0]; this->y = f[1]; this->z = f[2]; this->w = f[3]; } 
	Vec4(const Vec3& v, float w) { this->x = v.x; this->y = v.y; this->z = v.z; this->w = w; }

	//	������ 2����
	inline Vec2 xy() const { return Vec2(x, y); }
	inline Vec2 xz() const { return Vec2(x, z); }
	inline Vec2 yx() const { return Vec2(y, x); }
	inline Vec2 yz() const { return Vec2(y, z); }
	inline Vec2 zx() const { return Vec2(z, x); }
	inline Vec2 zy() const { return Vec2(z, y); }

	//	������ 3����
	inline Vec3 xyz() const { return Vec3(x, y, z); }
	inline Vec3 xzy() const { return Vec3(x, z, y); }
	inline Vec3 yxz() const { return Vec3(y, x, z); }
	inline Vec3 yzx() const { return Vec3(y, z, x); }
	inline Vec3 zxy() const { return Vec3(z, x, y); }
	inline Vec3 zyx() const { return Vec3(z, y, x); }

	Vec4& operator += (const Vec4 & v) { this->x += v.x; this->y += v.y; this->z += v.z; this->w += v.w; return *this; }
	Vec4& operator -= (const Vec4 & v) { this->x -= v.x; this->y -= v.y; this->z -= v.z; this->w -= v.w; return *this; }
	Vec4& operator *= (float f) { this->x *= f; this->y *= f; this->z *= f; this->w *= f; return *this; }
	Vec4& operator /= (float f) { this->x /= f; this->y /= f; this->z /= f; this->w /= f; return *this; }

	static Vec4 Lerp(const Vec4& a, const Vec4& b, float ww) { return Vec4(a.x+(b.x-a.x)*ww, a.y+(b.y-a.y)*ww, a.z+(b.z-a.z)*ww, a.w+(b.w-a.w)*ww); }

	static Vec4 ZERO;
	static Vec4 ONE;
} ;

inline Vec4 operator * (const Vec4 & v, float s) { return Vec4(v.x*s, v.y*s, v.z*s, v.w*s); }
inline Vec4 operator * (float s, const Vec4 & v) { return Vec4(v.x*s, v.y*s, v.z*s, v.w*s); }
inline Vec4 operator + (const Vec4 & a, const Vec4 & b) { return Vec4(a.x+b.x, a.y+b.y, a.z+b.z, a.w+b.w); }
inline bool operator == (const Vec4 & v1, const Vec4 & v2) { return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w) ? true : false; }
inline bool operator != (const Vec4 & v1, const Vec4 & v2) { return !(operator == (v1, v2)); }



#endif