// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class ICoordinate;

//------------------------------------------------------------------
/** Transform
	@author cagetu
	@desc 그래픽 API에 기반한 좌표계에 맞는 변환 클래스 렌더링 정의할 때 같이 정의 되어야 할 듯
*/
//------------------------------------------------------------------
class Transform
{
public:
	enum COORDINATE
	{
		_D3D = 0,
		_OPENGL,
		_VULKAN,
	};
	static void _Init(COORDINATE coordinate);
	static void _Shutdown();

public:
	static Vec4 TransformVector(const Mat4& m, const Vec4& p);
	static Vec3 TransformPoint(const Mat4& m, const Vec3& p);
	static Vec3 TransformNormal(const Mat4& m, const Vec3& p);
	static Vec3 TransformHomogen(const Mat4& m, const Vec3& p);

	static Vec4 Multiply(const Mat4& m, const Vec4& p);
	static Mat4 Multiply(const Mat4& a, const Mat4& b);

	static Mat4 RotationX(float rad);
	static Mat4 RotationY(float rad);
	static Mat4 RotationZ(float rad);
	static Mat4 Rotation(const Quat4& q);
	static Mat4 Scaling(const Vec3& s);
	static Mat4 Scaling(float s);
	static Mat4 Translate(const Vec3& p);
	static Mat4 Translate(float x, float y, float z);

	static void Decompose(const Mat4& input, Vec3& translate, Quat4& rotate, Vec3& scale);

	static Mat4 LookAt(const Vec3& pivot, const Vec3& target, const Vec3& upvec);
	static Mat4 LookAt(const Vec3& pivot, const Vec3& target);
	static Mat4 LookAtZAxis(const Vec3& pivot, const Vec3& zaxis);

	static Mat4 Projection(float nearWidth, float nearHeight, float nearClip, float farClip);
	static Mat4 ProjectionWithFov(float fov, float nearClip, float farClip, float aspectRatio);
	static Mat4 ProjectionOffCenter(float left, float right, float bottom, float top, float nearClip, float farClip);
	static Mat4 InfiniteProjectionWithFov(float fov, float nearClip, float aspectRatio, float epsilon = 2.4f*10e-7f);
	static Mat4 Ortho(float w, float h, float nearClip, float farClip);
	static Mat4 OrthoOffCenter(float left, float right, float bottom, float top, float nearClip, float farClip);

private:
	static ICoordinate* m_pCoordinate;
};

typedef Transform Coordinate;
