// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/*	@class Ray
	@desc Ray를 표현한다.
*/
//------------------------------------------------------------------
struct Ray
{
	Point3 Origin;
	Vec3 Direction;

	Ray(const Point3& orig, const Vec3& dir);

	Point3 GetPoint(float t) const;
};

//------------------------------------------------------------------
inline
Ray::Ray(const Point3& orig, const Vec3& dir)
	: Origin(orig), Direction(dir)
{
}

//------------------------------------------------------------------
inline
Point3 Ray::GetPoint(float t) const
{
	return Origin + t * Direction;
}