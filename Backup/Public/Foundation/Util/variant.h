// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#ifndef _variant_
#define _variant_

#ifdef WIN32
#include <crtdbg.h>
#endif

//class CTexture;

struct Variant
{
public:
	enum EType
	{
		Void,
		Int,
		Float,
		Bool,
		Vector2,
		Vector3,
		Vector4,	
		Matrix3,
		Matrix,
		Texture,	// CTexture의 포인터를 물고 있을 예정!!!
		FloatArray,
		Vector2Array,
		Vector3Array,
		Vector4Array,
		MatrixArray,
	} ;

	//------------------------------------------------------
	// Methods
	//------------------------------------------------------
	Variant() : m_Type(Void) {}
	Variant(const Variant& rhs) : m_Type(Void) { operator = (rhs); }
	Variant(int32 val) : m_Type(Int) { m_Value.Int = val; }
	Variant(float val) : m_Type(Float) { m_Value.Float[0] = val; }
	Variant(bool val) : m_Type(Bool) { m_Value.Bool = val; }
	Variant(const Vec2& val) : m_Type(Vector2) { m_Value.Float[0] = val.x; m_Value.Float[1] = val.y; }
	Variant(const Vec3& val) : m_Type(Vector3) { m_Value.Float[0] = val.x; m_Value.Float[1] = val.y; m_Value.Float[2] = val.z; }
	Variant(const Vec4& val) : m_Type(Vector4) { m_Value.Float[0] = val.x; m_Value.Float[1] = val.y; m_Value.Float[2] = val.z; m_Value.Float[3] = val.w; }
	Variant(const Mat3& val) : m_Type(Matrix3) { m_Value.Matrix = val.ToMat4(); }
	Variant(const Mat4& val) : m_Type(Matrix) { m_Value.Matrix = val; }
	//Variant(CTexture * val) : m_Type(Texture) { m_Value.Texture = val; }
	Variant(const float* val, uint32 count) : m_Type(FloatArray) { Duplicate(val, sizeof(float), count); }
	Variant(const Vec2* val, uint32 count) : m_Type(Vector2Array) { Duplicate(val, sizeof(Vec2), count); }
	Variant(const Vec3* val, uint32 count) : m_Type(Vector3Array) { Duplicate(val, sizeof(Vec3), count); }
	Variant(const Vec4* val, uint32 count) : m_Type(Vector4Array) { Duplicate(val, sizeof(Vec4), count); }
	Variant(const Mat4* val, uint32 count) : m_Type(MatrixArray) { Duplicate(val, sizeof(Mat4), count); }
	~Variant() { Delete(); }

	operator int32 () const { ASSERT(Int == m_Type); return m_Value.Int; }
	operator float () const { ASSERT(Float == m_Type); return m_Value.Float[0]; }
	operator bool () const { ASSERT( m_Type == Bool ); return m_Value.Bool; }
	operator const Vec4& () const { ASSERT(m_Type == Vector4); return *(Vec4*)m_Value.Float; }
	operator const Vec4* () const { ASSERT(m_Type == Vector4); return (Vec4*)m_Value.Float; }
	operator const Vec3& () const { ASSERT(m_Type == Vector3); return *(Vec3*)m_Value.Float; }
	operator const Vec3* () const { ASSERT(m_Type == Vector3); return (Vec3*)m_Value.Float; }
	operator const Vec2& () const { ASSERT(m_Type == Vector2); return *(Vec2*)m_Value.Float; }
	operator const Vec2* () const { ASSERT(m_Type == Vector2); return (Vec2*)m_Value.Float; }
	operator const Mat4& () const { ASSERT(m_Type == Matrix); return m_Value.Matrix; }
	operator const Mat4* () const { ASSERT(m_Type == Matrix); return &m_Value.Matrix; }
	operator const Mat3& () const { ASSERT(m_Type == Matrix3); static Mat3 m; m.FromMat4(m_Value.Matrix); return m; }
	operator const Mat3* () const { ASSERT(m_Type == Matrix3); static Mat3 m; m.FromMat4(m_Value.Matrix); return &m; }

	operator Vec4& () { ASSERT(m_Type == Vector4); return *(Vec4*)m_Value.Float; }

/*	operator Vec4& () { ASSERT(m_Type == Vector); return *(Vec4*)m_Value.Float; }
	operator Vec4 * () { ASSERT(m_Type == Vector); return (Vec4*)m_Value.Float; }
	operator Vec3& () { ASSERT(m_Type == Vector); return *(Vec3*)m_Value.Float; }
	operator Vec3 * () { ASSERT(m_Type == Vector); return (Vec3*)m_Value.Float; }
	operator Mat4& () { ASSERT(m_Type == Matrix); return m_Value.Matrix; }
	operator Mat4* () { ASSERT(m_Type == Matrix); return &m_Value.Matrix; } */

	//operator CTexture * () const { ASSERT(m_Type == Texture); return m_Value.Texture; }

	const float* GetFloatArray() const { ASSERT(m_Type == FloatArray); return m_Value.Array.Float; }
	const Vec2* GetVector2Array() const { ASSERT(m_Type == Vector2Array); return m_Value.Array.Vector2; }
	const Vec3* GetVector3Array() const { ASSERT(m_Type == Vector3Array); return m_Value.Array.Vector3; }
	const Vec4* GetVector4Array() const { ASSERT(m_Type == Vector4Array); return m_Value.Array.Vector4; }
	const Mat4* GetMatrixArray() const { ASSERT(m_Type == MatrixArray); return m_Value.Array.Matrix; }

	bool operator == (const Variant& rhs) const;
	bool operator != (const Variant& rhs) const { return !operator ==(rhs); }

	Variant& operator = (const Variant& rhs);

public:
	void Clear() { Delete(); }
	void Reset();

	EType Type() const { return m_Type; }

	bool IsEmpty() const { return (m_Type == Void) ? true : false; }
	uint32 Count() const { ASSERT(m_Type == FloatArray || m_Type == Vector2Array || m_Type == Vector3Array || m_Type == Vector4Array || m_Type == MatrixArray); return m_Value.Array.Count; }

private:
	union value
	{
		int32		Int;
		bool		Bool;
		float		Float[4];
		Mat4		Matrix;
		//CTexture*	Texture;

		struct
		{
			uint32	Count;
			union
			{
				float *		Float;
				Vec2 *		Vector2;
				Vec3 *		Vector3;
				Vec4 *		Vector4;
				Mat4 *		Matrix;
				void *		ptr;
			} ;
			void * _empty;
		}	Array ;
	}	m_Value;

	EType m_Type;

	void Duplicate(const void * ptr, int32 pitch, int32 num);
	void Delete();

public :
	static Variant Null;
};

#endif