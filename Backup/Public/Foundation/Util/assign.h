// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/** Assign
	@author cagetu
	@desc
		- https://floooh.github.io/2007/02/17/nebula3-io-subsystem-uris-and-assigns.html
		- URI를 기반으로 "content:texture.png" 의 경로 정보
*/
//------------------------------------------------------------------
class Assign
{
public:
	Assign() = default;
	Assign(const String& name, const String& path) : m_Name(name), m_Path(path) { CHECK(!name.empty() && !path.empty(), TEXT("empty")); }

	const String& Name() const { return m_Name; }
	const String& Path() const { return m_Path; }

private:
	String m_Name;
	String m_Path;
};