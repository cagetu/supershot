// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#ifndef _BIT_FLAGS_
#define _BIT_FLAGS_

template <int maxsize, class _Tx=int>
class CBitFlags
{
public :
	CBitFlags() { memset(m_flags, 0, (maxsize+31)/32*sizeof(unsigned long)); }

#ifdef _WIN32
	inline void SetFlags(_Tx idx, bool set) { _ASSERT(idx < maxsize); m_flags[idx>>5] = set ? (m_flags[idx>>5]|(1<<(idx&31))) : (m_flags[idx>>5]&(~(1<<(idx&31)))); }
	inline bool GetFlags(_Tx idx) const { _ASSERT(idx < maxsize); return (m_flags[idx>>5] & (1<<(idx&31))) ? true : false; }
#else
	inline void SetFlags(_Tx idx, bool set) { m_flags[idx>>5] = set ? (m_flags[idx>>5]|(1<<(idx&31))) : (m_flags[idx>>5]&(~(1<<(idx&31)))); }
	inline bool GetFlags(_Tx idx) const { return (m_flags[idx>>5] & (1<<(idx&31))) ? true : false; }
#endif
	inline void Reset() { memset(m_flags, 0, (maxsize+31)/32*sizeof(unsigned long)); }

	inline void operator |= (const CBitFlags& f) { for(int i=0; i<(maxsize+31)>>5; i++) m_flags[i] |= f.m_flags[i]; }
	inline void operator = (const CBitFlags& f) { for(int i=0; i<(maxsize+31)>>5; i++) m_flags[i] = f.m_flags[i]; }
	inline bool operator[] (_Tx idx) const { return GetFlags(idx); }

private :
	unsigned long m_flags[(maxsize+31)/32];
} ;

#endif