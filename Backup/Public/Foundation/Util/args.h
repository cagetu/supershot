// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**
	@class CommandLine Args
	@brief
		- access command line args
		- Oryol::Args, Nebula3::CommandlineArgs ...
*/
//------------------------------------------------------------------
class CommandArgs
{
public:
	CommandArgs();
	CommandArgs(const String& args);
	CommandArgs(int32 argc, const char** argv);

	bool HasArg(const String& arg) const;
	SizeT Size() const;

	const String& GetString(const String& arg, const String& defaultValue = TEXT("")) const;
	int32 GetInt(const String& arg, int32 defaultValue = 0) const;
	float GetFloat(const String& arg, float defaultValue = 0.0f) const;
	bool GetBool(const String& arg, bool defaultValue = false) const;

private:
	struct Element
	{
		String name;
		String value;

		Element() = default;
		Element(const String& n) : name(n) {}
		Element(const String& n, const String& v) : name(n), value(v) {}

		bool operator == (const Element& rhs) const
		{
			return (name == rhs.name && value == rhs.value) ? true : false;
		}
	};

	Element* Find(const String& arg) const;

	Array<Element> m_Args;
};