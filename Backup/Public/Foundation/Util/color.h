// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#ifndef _color_
#define _color_

struct Vec4;

struct Color
{
	union
	{
		struct 
		{
			float r, g, b, a;	/* 0.0f ~ 1.0f */
		};
		float channel[4]; // {r, g, b, a}
	};

	Color() : r(1.0f), g(1.0f), b(1.0f), a(1.0f) {}
	Color(const Color& rhs) : r(rhs.r), g(rhs.g), b(rhs.b), a(rhs.a) {}
	Color(float red, float green, float blue, float alpha = 1.0): r(red), g(green), b(blue), a(alpha) {}
	Color(const int8 red, const int8 green, const int8 blue, const int8 alpha);
	Color(RGBA rgba);

	operator RGBA () const { return TO_RGBA((unsigned int)((r)*255.f),(unsigned int)((g)*255.f),(unsigned int)((b)*255.f),(unsigned int)((a)*255.f)); }
	operator Vec4 & () const { return *(Vec4*)&r; }

	bool operator == (const Color& rhs) const { return (r == rhs.r && g == rhs.g && b == rhs.b && a == rhs.a); }
	bool operator != (const Color& rhs) const { return !(*this == rhs); }

	Color& operator = (RGBA c) { (*this) = Color(c); return *this; }
	Color& operator = (const Color& rhs) { r = rhs.r; g = rhs.g; b = rhs.b; a = rhs.a;  return *this; }

	static Color WHITE;
	static Color BLACK;
	static Color RED;
	static Color GREEN;
	static Color BLUE;
	static Color YELLOW;
};

inline Color operator * (const Color & c, float s) { return Color(c.r*s, c.g*s, c.b*s, c.a*s); }
inline Color operator + (const Color & a, const Color & b) { return Color(a.r+b.r, a.g+b.g, a.b+b.b, a.a+b.a); }
inline Color operator - (const Color & a, const Color & b) { return Color(a.r-b.r, a.g-b.g, a.b-b.b, a.a-b.a); }

#endif