// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/**	Log
	@author cagetu
	@desc 로그 스트림
*/
//------------------------------------------------------------------
class ILogStream : public RefCount
{
public:
	virtual ~ILogStream() {}

	virtual void Text(const TCHAR* text) = 0;
	virtual void Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber) = 0;
	virtual void Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber) = 0;
	virtual void Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber) = 0;
};

//------------------------------------------------------------------
/**	Log
	@author cagetu
	@desc 로그 클래스
*/
//------------------------------------------------------------------
class Log
{
public:
	/// log levels
	enum Level
	{
		None = 0,
		_Error,
		_Warn,
		_Info,
		_Debug,

		Max,
		InvalidLevel
	};

	/// 최초 시작
	static void _Init();
	/// 최종 마무리
	static void _Shutdown();

	static void Register(const Ptr<ILogStream>& stream);
	static void Unregister(const Ptr<ILogStream>& stream);

	static void SetLevel(Level level);

	static void Debug(Level level, const char* fileName, const char* funcName, int32 lineNumber, const TCHAR* format, ...);

private:
	static void Text(const TCHAR* text);
	static void Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);
	static void Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);
	static void Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber);

	static Array<Ptr<ILogStream> > LogStreams;
	static Level LogLevel;
};

#define ULOG(level, format, ...) \
( \
	Log::Debug(level, __FILE__, __FUNCTION__, __LINE__, TEXT(format), ##__VA_ARGS__) \
)