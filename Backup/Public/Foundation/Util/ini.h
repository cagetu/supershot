// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#ifndef _INI_
#define _INI_

class Ini
{
public :
	Ini();
	~Ini();

	bool LoadINI(const TCHAR* fname);
	bool SaveINI(const TCHAR* fname);
	void SetKey(const TCHAR* key);

	bool GetBool(const TCHAR* name, bool def=false);
	int	GetInt(const TCHAR* name, int def=0);
	float GetFloat(const TCHAR* name, float def=0.0f);
	const TCHAR *GetString(const TCHAR* name, const TCHAR* def= TEXT(""));

	void Set(const TCHAR* name, bool b);
	void Set(const TCHAR* name, int n);
	void Set(const TCHAR* name, float f);
	void Set(const TCHAR* name, const TCHAR* str);

private :
	const TCHAR* Find(const TCHAR *key);
	bool Parse(char* buffer, int len);
	void MemWrite(void *data, int len);

	Map<String, String> m_List;
	char m_EncryptKey[256];
	char* m_Buffer;
	int	m_Length;
	int	m_CurPos;
};

#endif