#pragma once

class IDisplayContext
{
public:
	virtual int32 GetWidth() const = 0;
	virtual int32 GetHeight() const = 0;
	virtual void* GetWindowHandle() const = 0;
};
