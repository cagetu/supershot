// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class IMutex
{
public:
	IMutex();
	virtual ~IMutex();

	virtual void Lock() = 0;
	virtual void Unlock() = 0;
};

class MutexScopeLock
{
public:
	MutexScopeLock(IMutex* mutex)
		: m_Mutex(mutex)
	{
		m_Mutex->Lock();
	}
	MutexScopeLock()
	{
		Leave();
	}

	void Leave()
	{
		if (m_Mutex)
		{
			m_Mutex->Unlock();
		}
	}

private:
	IMutex* m_Mutex;
};