#pragma once

class WindowMutex : public IMutex
{
public:
	WindowMutex();
	virtual ~WindowMutex();

	void Lock();
	void Unlock();

private:
	HANDLE m_hMutex;
};