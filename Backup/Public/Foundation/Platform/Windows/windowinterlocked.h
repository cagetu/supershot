
#pragma once

struct WindowInterlocked : public BaseInterlocked
{
	static FORCEINLINE int32 InterlockedIncrement(volatile int32* value)
	{
		return (int32)::InterlockedIncrement((LPLONG)value);
	}

	static FORCEINLINE int64 InterlockedIncrement(volatile int64* value)
	{
		return (int64)::InterlockedIncrement64((LONGLONG*)value);
	}

	static FORCEINLINE int32 InterlockedDecrement(volatile int32* value)
	{
		return (int)::InterlockedDecrement((long*)value);
	}

	static FORCEINLINE int64 InterlockedDecrement(volatile int64* value)
	{
		return (int64)::InterlockedDecrement((uint64*)value);
	}

	static FORCEINLINE int32 InterlockedAdd(volatile int32* Value, int32 Amount)
	{
		return (int32)::InterlockedExchangeAdd((LPLONG)Value, (LONG)Amount);
	}

	static FORCEINLINE int64 InterlockedAdd(volatile int64* Value, int64 Amount)
	{
		return (int64)::InterlockedExchangeAdd64((LONGLONG*)Value, (LONGLONG)Amount);
	}

	static FORCEINLINE int32 InterlockedExchange(volatile int32* Value, int32 Exchange)
	{
		return (int32)::InterlockedExchange((LPLONG)Value, (LONG)Exchange);
	}

	static FORCEINLINE int64 InterlockedExchange(volatile int64* Value, int64 Exchange)
	{
		return (int64)::InterlockedExchange64((LONGLONG*)Value, (LONGLONG)Exchange);
	}

	static FORCEINLINE int32 InterlockedCompareExchange(volatile int32* Dest, int32 Exchange, int32 Comparand)
	{
		return (int32)::InterlockedCompareExchange((LPLONG)Dest, (LONG)Exchange, (LONG)Comparand);
	}
};


typedef WindowInterlocked	Interlocked;
