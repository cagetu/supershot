
#pragma once

class WindowCriticalSection : public ICriticalSection
{
public :
	WindowCriticalSection()
	{
		InitializeCriticalSection(&m_CS);
		SetCriticalSectionSpinCount(&m_CS, 4000);
 #ifdef _DEBUG
		m_ThreadID = 0;
		m_Depth = 0;
 #endif
	}
	~WindowCriticalSection()
	{
		DeleteCriticalSection(&m_CS);
	}

	void Lock()
	{
		if (::TryEnterCriticalSection(&m_CS) == FALSE)
		{
			EnterCriticalSection(&m_CS);
 #ifdef _DEBUG
			m_Depth++;
			m_ThreadID = GetCurrentThreadId();
 #endif
		}
	}
	void Unlock()
	{
		::LeaveCriticalSection(&m_CS);
#ifdef _DEBUG
		--m_Depth;
#endif
	}

#ifdef _DEBUG
	bool CheckValid()
	{
		return m_Depth != 0 && m_ThreadID == GetCurrentThreadId();
		return true;
	}

	int GetDepth() const 
	{
		return m_Depth;
	}
#endif

private :
	CRITICAL_SECTION m_CS;

#ifdef _DEBUG
	volatile int m_Depth;
	volatile uint32 m_ThreadID;
#endif
} ;

typedef WindowCriticalSection	CriticalSection;