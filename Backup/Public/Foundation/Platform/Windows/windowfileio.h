#pragma once

class	MemoryMappedIO : public MemoryIO
{
public:
	~MemoryMappedIO();

	int Read(uint8 * output, int len);

	static MemoryMappedIO* Open(const wchar* fname);
	static MemoryMappedIO* Close(MemoryMappedIO* file);

protected:
	MemoryMappedIO(HANDLE hFile, HANDLE hMapFile, int len);
	bool Load(uint32 offset, uint32 length);
	void Unload();

private:
	void * m_pAddress;
	HANDLE m_hMapFile, m_hFile;
};

class	SyncedIO : public IFileIO
{
public:
	SyncedIO(IFileIO *f, int off, int len, CriticalSection* cs);

	int Read(uint8 * output, int len);
	int Seek(int offset, _SEEK pos);
	int GetSize() const { return m_Length; }

protected:
	uint32 m_Length;
	uint32 m_Pivot;
	uint32 m_Offset;

	IFileIO* m_fp;
	CriticalSection* m_CS;
};

/*
class	CAsyncStdIO : public IFileIO
{
public:
	~CAsyncStdIO();

	int Read(uint8 * output, int len);
	int Seek(int offset, _SEEK pos);
	int GetSize() const { return m_Length; }

	static CAsyncStdIO* Open(const TCHAR * fname);

private:
	CAsyncStdIO(void* handle, int len);

	void * m_Handle;
	int m_Offset;
	int m_Length;

	static CConcurrentLoader & Loader() { static CConcurrentLoader _Loader; return _Loader; }
};
*/