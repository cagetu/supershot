#pragma once

//
//
//
class WindowDisplayContext : public IDisplayContext
{
public:
	WindowDisplayContext() 
	{ 
		m_WindowHandle = NULL; 
		m_WindowWidth = m_WindowHeight = -1; 
	}
	WindowDisplayContext(HWND hwnd, int32 _width, int32 _height)
	{
		m_WindowHandle = hwnd;
		m_WindowWidth = _width;
		m_WindowHeight = _height;
	}

	int32 GetWidth() const override { return m_WindowWidth; }
	int32 GetHeight() const override { return m_WindowHeight; }
	void* GetWindowHandle() const override { return m_WindowHandle; }

private:
	HWND m_WindowHandle;

	int32 m_WindowWidth;
	int32 m_WindowHeight;
};

