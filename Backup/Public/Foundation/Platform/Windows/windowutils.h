#pragma once

struct WindowSys : public BaseSys
{
	static void Sleep(float sec);

	static void DebugOut(const TCHAR *format, ...);

	static int Dialog(const TCHAR* text, const TCHAR* caption, int type = 0);

	static void Prefetch(void* p, int32 offset);

	// File IO
	static FILE* fopen(const TCHAR* fileName, const TCHAR* mode);
	static void fclose(FILE * fp);

	static int32 remove(const TCHAR* fileName);
	static int32 rename(const TCHAR* oldFileName, const TCHAR* newFileName);
	static int32 access(const TCHAR* fileName, int32 accmode);
};

typedef WindowSys PlatformSys;

#define DebugOutMsg PlatformSys::DebugOut
#define DialogMsg PlatformSys::Dialog
