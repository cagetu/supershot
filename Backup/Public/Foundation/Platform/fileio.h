// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class	IFileIO /*: public RefCount*/
{
public :
	enum _SEEK { _SET, _CUR, _END } ;
	virtual int Seek(int offset, _SEEK pos) = 0;
	virtual int GetSize() const { return 0; }

	virtual int Read(uint8 * output, int len) = 0;
	uint8 GetChar() { uint8 ch; return Read(&ch, 1) == 1 ? ch : -1; }
};

//------------------------------------------------------------------
/** File IO
	@author cagetu
	@remarks
		Read Only
*/
//------------------------------------------------------------------
class	FileIO : public IFileIO
{
public :
	enum Mode
	{
		ReadOnly = 0,
		WriteOnly,
	};

	~FileIO();

	int Read(uint8* output, int len);
	int Seek(int offset, _SEEK pos);
	int Tell();
	void Flush();

	int Write(void * ptr, int len);

	int GetSize() const { return m_Length; }

	static FileIO* Open(const wchar * fname, Mode mode = ReadOnly);
	static FileIO* Open(FILE* fp, int off=0, int len=0, Mode mode = ReadOnly);
	static FileIO* Close(FileIO* file);

protected :
	FileIO(FILE *fp, int off, int len, Mode mode);

	FILE *	m_FP;
	uint32	m_Length;
	uint32	m_CurPos;
	Mode m_Mode;
} ;

//------------------------------------------------------------------
/**
*/
//------------------------------------------------------------------
class	MemoryIO : public IFileIO
{
public:
	MemoryIO(const uint8* buffer, int len, bool bAutoDestroy);
	~MemoryIO();

	int Read(uint8* output, int len);
	int Seek(int offset, _SEEK pos);

	int GetSize() const { return m_Length; }
	bool IsAutoDestroy() const { return m_bAutoDestroy; }

protected:
	const uint8*m_Data;
	uint32		m_Length;
	uint32		m_Offset;
	bool		m_bAutoDestroy;
};
