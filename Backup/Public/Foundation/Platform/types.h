// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

// HAL (Hardware Abstraction Layer)

// Unreal Style test...

//---------------------------------------------------------------------
// Utility for automatically setting up the pointer-sized integer type
//---------------------------------------------------------------------

template<typename T32BITS, typename T64BITS, int PointerSize>
struct SelectIntPointerType
{
	// nothing here are is it an error if the partial specializations fail
};

template<typename T32BITS, typename T64BITS>
struct SelectIntPointerType<T32BITS, T64BITS, 8>
{
	typedef T64BITS TIntPointer; // select the 64 bit type
};

template<typename T32BITS, typename T64BITS>
struct SelectIntPointerType<T32BITS, T64BITS, 4>
{
	typedef T32BITS TIntPointer; // select the 64 bit type
};

// Generic Type Defines
struct TypeDefines
{
	typedef unsigned char		uchar;
	typedef unsigned short		ushort;
	typedef unsigned int		uint;
	typedef unsigned long		ulong;

	typedef unsigned char 		uint8;		// 8-bit  unsigned.
	typedef unsigned short int	uint16;		// 16-bit unsigned.
	typedef unsigned int		uint32;		// 32-bit unsigned.
	typedef unsigned long long	uint64;		// 64-bit unsigned.

	typedef	signed char			int8;		// 8-bit  signed.
	typedef signed short int	int16;		// 16-bit signed.
	typedef signed int	 		int32;		// 32-bit signed.
	typedef signed long long	int64;		// 64-bit signed.

	typedef char	ansichar;		// ANSI CHARACTER
	typedef wchar_t widechar;		// WIDE CHARACTER
	typedef uchar	CHAR8;		// An 8-bit character type - In-memory only.  8-bit representation.  Should really be char8_t but making this the generic option is easier for compilers which don't fully support C++11 yet (i.e. MSVC).
	typedef ushort	CHAR16;		// A 16-bit character type - In-memory only.  16-bit representation.  Should really be char16_t but making this the generic option is easier for compilers which don't fully support C++11 yet (i.e. MSVC).
	typedef uint	CHAR32;		// A 32-bit character type - In-memory only.  32-bit representation.  Should really be char32_t but making this the generic option is easier for compilers which don't fully support C++11 yet (i.e. MSVC).
#ifdef _UNICODE
	typedef widechar	TCHAR;		// A switchable character - In - memory only.Either ANSICHAR or WIDECHAR, depending on a licensee's requirements.
#else
	typedef ansichar	TCHAR;
#endif

	typedef SelectIntPointerType<uint32, uint64, sizeof(void*)>::TIntPointer UPTRINT;	// unsigned int the same size as a pointer
	typedef SelectIntPointerType<int32, int64, sizeof(void*)>::TIntPointer PTRINT;		// signed int the same size as a pointer
	typedef UPTRINT SIZE_T;																// unsigned int the same size as a pointer
	typedef PTRINT SSIZE_T;																// unsigned int the same size as a pointer

	typedef int32 TYPE_NULL;
	typedef decltype(nullptr) TYPE_NULLPTR;
};

#ifndef MAX
	#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef MIN
	#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#define BIT(a)	(1<<a)

#ifndef checkAtCompileTime
	#define checkAtCompileTime(expr, msg)  static_assert(expr, #msg)
#endif

#if defined(TARGET_OS_WINDOWS)
	#include "Platform/Windows/windowtypes.h"
#elif defined(TARGET_OS_ANDROID)
	#include "Platform/Android/androidtypes.h"	
#elif defined(TARGET_OS_IPHONE)
	#include "Platform/iOS/iostypes.h"	
#endif

#ifndef PLATFORM_TCHAR_IS_4_BYTES
	#define PLATFORM_TCHAR_IS_4_BYTES			0
#endif

// These is computed, not predefined
#define PLATFORM_32BITS					(!PLATFORM_64BITS)

// Type Defines
typedef PlatformTypes::uchar		uchar;
typedef PlatformTypes::ushort		ushort;
typedef PlatformTypes::uint			uint;
typedef PlatformTypes::uint32		uint32;

// Unsigned base types.
typedef PlatformTypes::uint8		uint8;		///< An 8-bit unsigned integer.
typedef PlatformTypes::uint16		uint16;		///< A 16-bit unsigned integer.
typedef PlatformTypes::uint32		uint32;		///< A 32-bit unsigned integer.
typedef PlatformTypes::uint64		uint64;		///< A 64-bit unsigned integer.

// Signed base types.
typedef	PlatformTypes::int8			int8;		///< An 8-bit signed integer.
typedef PlatformTypes::int16		int16;		///< A 16-bit signed integer.
typedef PlatformTypes::int32		int32;		///< A 32-bit signed integer.
typedef PlatformTypes::int64		int64;		///< A 64-bit signed integer.

typedef PlatformTypes::ansichar		achar;
typedef PlatformTypes::widechar		wchar;

typedef PlatformTypes::CHAR8		CHAR8;
typedef PlatformTypes::CHAR16		CHAR16;
typedef PlatformTypes::CHAR32		CHAR32;
typedef PlatformTypes::TCHAR		TCHAR;

typedef PlatformTypes::TYPE_NULL	TYPE_NULL;
typedef PlatformTypes::TYPE_NULLPTR	TYPE_NULLPTR;

/// An unsigned integer the same size as a pointer
typedef PlatformTypes::UPTRINT		UPTRINT;
/// A signed integer the same size as a pointer
typedef PlatformTypes::PTRINT		PTRINT;
/// unsigned int the same size as a pointer
typedef PlatformTypes::SIZE_T		SIZE_T;
/// unsigned int the same size as a pointer
typedef PlatformTypes::SSIZE_T		SSIZE_T;

typedef PlatformTypes::uint32 SizeT;
typedef PlatformTypes::int32 IndexT;

#define INVALID_INDEX -1

// Color
typedef unsigned long	RGBA;

#define TO_ARGB(a,r,g,b) ((RGBA)((((a)&0xff)<<24)|(((r)&0xff)<<16)|(((g)&0xff)<<8)|((b)&0xff)))
#define TO_RGBA(r,g,b,a) TO_ARGB(a,r,g,b)
#define TO_XRGB(r,g,b)   TO_ARGB(0xff,r,g,b)
//#define TO_COLOR(r,g,b,a) TO_RGBA((unsigned int)((r)*255.f),(unsigned int)((g)*255.f),(unsigned int)((b)*255.f),(unsigned int)((a)*255.f))
#define TO_R(color)		(unsigned char)(color >> 16) / 255.0f
#define TO_G(color)		(unsigned char)(color >> 8) / 255.0f
#define TO_B(color)		(unsigned char)(color) / 255.0f
#define TO_A(color)		(unsigned char)(color >> 24) / 255.0f
#define RGBA_WHITE	0xffffffff
#define RGBA_BLACK	0x00000000

// UByte4
typedef int32 ubyte4;
// (x, y, z, w) assuming these are in the range 0..1
#define UBYTE4(x, y, z, w) (((int)(x * 255.0f) & 0xff) << 24) | (((int)(y * 255.0f) & 0xff) << 16) | (((int)(z * 255.0f) & 0xff) << 8) | (((int)(w * 255.0f) & 0xff))	// the endianness might be wrong, but you get the idea

#ifndef _countof
#define _countof(A)	(sizeof(A)/sizeof(*A))
#endif

#ifndef BYTE
typedef unsigned char       BYTE;
#endif

//------------------------------------------------------------------
// Test the global types (By Unreal Engine 4)
//------------------------------------------------------------------
namespace TypeTests
{
	template <typename A, typename B>
	struct TAreTypesEqual
	{
		enum { Value = false };
	};

	template <typename T>
	struct TAreTypesEqual<T, T>
	{
		enum { Value = true };
	};


	checkAtCompileTime(!PLATFORM_TCHAR_IS_4_BYTES || sizeof(wchar) == 4, TypeTests_WCHAR_size);
	checkAtCompileTime(PLATFORM_TCHAR_IS_4_BYTES || sizeof(wchar) == 2, TypeTests_WCHAR_size);
}
