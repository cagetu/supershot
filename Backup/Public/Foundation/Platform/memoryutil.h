// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

namespace Memory
{
	template <class TYPE>
	static FORCEINLINE void MemZero(TYPE& src)
	{
		PlatformSys::MemZero(&src, sizeof(src));
	}

	template <class TYPE>
	static FORCEINLINE void MemSet(TYPE& src, int32 val)
	{
		PlatformSys::MemSet(src, val, sizeof(src));
	}

	static FORCEINLINE void MemZero(void* dest, size_t size)
	{
		PlatformSys::MemZero(dest, size);
	}

	static FORCEINLINE void MemCpy(void* dest, const void* src, size_t size)
	{
		PlatformSys::MemCpy(dest, src, size);
	}

	static FORCEINLINE int32 MemCmp(void* dest, const void* src, size_t size)
	{
		return PlatformSys::MemCmp(dest, src, size);
	}

	static FORCEINLINE void MemMove(void* dest, const void* src, size_t size)
	{
		PlatformSys::MemMove(dest, src, size);
	}

	template <typename TYPE>
	static FORCEINLINE const TYPE Align(const TYPE Ptr, uint64 Alignment)
	{
		static_assert(TIsIntegral<TYPE>::Value || TIsPointer<TYPE>::Value, "IsAligned expects an integer or pointer type");

		return (TYPE)(((PTRINT)Ptr + Alignment - 1) & ~(Alignment - 1));
	}

	template <typename TYPE>
	static FORCEINLINE constexpr bool IsAligned(TYPE Val, uint64 Alignment)
	{
		static_assert(TIsIntegral<TYPE>::Value || TIsPointer<TYPE>::Value, "IsAligned expects an integer or pointer type");

		return !((uint64)Val & (Alignment - 1));
	}

	template <class TYPE>
	static FORCEINLINE TYPE* Construct(void* ptr)
	{
		return new (ptr) TYPE();
	}

	template <class TYPE>
	static FORCEINLINE void Destruct(void* ptr)
	{
		((TYPE*)(ptr))->~TYPE();
	}

	template <class TYPE>
	static FORCEINLINE void ConstructItems(void* address, SizeT count)
	{
		TYPE* element = (TYPE*)address;
		while (count)
		{
			new (element) TYPE;
			++element;
			--count;
		}
	}

	template <class TYPE>
	static FORCEINLINE void ConstructItems(void* dest, const TYPE* src, SizeT count)
	{
		while (count)
		{
			new (dest) TYPE(*src);
			++(TYPE*&)dest;
			++src;
			--count;
		}
	}

	template <class TYPE>
	static FORCEINLINE void DestructItems(TYPE* elements, SizeT count)
	{
		while (count)
		{
			elements->~TYPE();
			++elements;
			--count;
		}
	}

	template <class TYPE>
	static FORCEINLINE void RelocateConstructItems(void* dest, const TYPE* src, SizeT count)
	{
		while (count)
		{
			new (dest) TYPE(*src);
			++(TYPE*&)dest;
			(src++)->~TYPE();
			--count;
		}
	}

	template <class TYPE>
	static FORCEINLINE void MoveConstructItems(void* dest, const TYPE* src, SizeT count)
	{
		while (count)
		{
			new (dest) TYPE((TYPE&&)*src);
			++(TYPE*&)dest;
			++src;
			--count;
		}
	}
}