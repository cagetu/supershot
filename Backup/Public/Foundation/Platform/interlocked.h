// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

struct BaseInterlocked
{
	static FORCEINLINE int32 InterlockedIncrement(volatile int32* value)
	{
		*value += 1;
		return *value;
	}

	static FORCEINLINE int64 InterlockedIncrement(volatile int64* value)
	{
		*value += 1;
		return *value;
	}

	static FORCEINLINE int32 InterlockedDecrement(volatile int32* value)
	{
		*value -= 1;
		return *value;
	}

	static FORCEINLINE int64 InterlockedDecrement(volatile int64* value)
	{
		*value -= 1;
		return *value;
	}

	static FORCEINLINE int32 InterlockedAdd(volatile int32* Value, int32 Amount)
	{
		*Value += Amount;
		return *Value;
	}

	static FORCEINLINE int64 InterlockedAdd(volatile int64* Value, int64 Amount)
	{
		*Value += Amount;
		return *Value;
	}

	static FORCEINLINE int32 InterlockedExchange(volatile int32* Value, int32 Exchange)
	{
		*Value = Exchange;
		return *Value;
	}

	static FORCEINLINE int64 InterlockedExchange(volatile int64* Value, int64 Exchange)
	{
		*Value = Exchange;
		return *Value;
	}

	static FORCEINLINE int32 InterlockedCompareExchange(volatile int32* Dest, int32 Exchange, int32 Comparand)
	{
		if (*Dest == Comparand)
		{
			*Dest = Exchange;
		}
		return *Dest;
	}

};