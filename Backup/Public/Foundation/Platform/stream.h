// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

class Stream
{
public:
	virtual int32 ReadBytes(uint8* output, int32 size) abstract;
	virtual int32 WriteBytes(const uint8* input, int32 size) abstract;
};

class MemoryStream : public Stream
{
public:
	MemoryStream();
	MemoryStream(uint8* buffer, bool bAutoDestroy = false);
	~MemoryStream();

	virtual int32 ReadBytes(uint8* output, int32 size);
	virtual int32 WriteBytes(const uint8* input, int32 size);

	uint8* CurrentData();

private:
	uint8* m_Data;
	uint32 m_CurPos;
	bool m_bAutoDestroy;
};

// Stream Utility

template<class T> int StreamReadFlat(Stream* stream, T& out_value);
template<class T> int StreamWriteFlat(Stream* stream, const T& value);

template<class T>
int StreamReadFlat(Stream* stream, T& out_value) 
{
	return stream->ReadBytes(reinterpret_cast<uint8_t*>(&out_value), sizeof(T));
}

template <class T>
int StreamWriteFlat(Stream* stream, const T& value)
{
	return stream->WriteBytes(reinterpret_cast<uint8_t*>(&value), sizeof(T));
}