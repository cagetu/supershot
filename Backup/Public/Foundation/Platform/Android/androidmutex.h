#pragma once

class AndroidMutex : public IMutex
{
public:
	AndroidMutex();
	virtual ~AndroidMutex();

	void Lock() override;
	void Unlock() override;

private:
	mutable pthread_mutex_t m_Mutex;
};