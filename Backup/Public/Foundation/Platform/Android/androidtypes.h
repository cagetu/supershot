#pragma once

struct AndroidTypes : public TypeDefines
{
	typedef char16_t	CHAR16;
	typedef char16_t	widechar;
	typedef widechar	TCHAR;
};

typedef AndroidTypes PlatformTypes;

#ifndef BYTE
typedef unsigned char       BYTE;
#endif

#if _DEBUG 
#define FORCEINLINE	inline									/* Easier to debug */
#else
#define FORCEINLINE inline __attribute__ ((always_inline))	/* Force code to be inline */
#endif

#ifndef TEXT
#define TEXT(x) L##x
#endif

#ifndef _ASSERT
#define _ASSERT(X) assert(X)
#endif

#ifndef NULL
#define NULL 0
#endif

#define PLATFORM_TCHAR_IS_4_BYTES					1