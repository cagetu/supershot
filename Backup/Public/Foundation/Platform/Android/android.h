
#pragma once

#include <cctype>	// tolower
#include <unistd.h>
//#include <assert.h>
//#include <stdarg.h>
#include <android/asset_manager_jni.h>
#include <android/log.h>

#include "Platform\Android\androidutils.h"
#include "Platform\Android\androidmutex.h"
#include "Platform\Android\androidcriticalsection.h"
#include "Platform\Android\androidinterlocked.h"
#include "Platform\Android\androidconsole.h"

