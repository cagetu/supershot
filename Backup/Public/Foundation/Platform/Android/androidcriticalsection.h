#pragma once

class AndroidCriticalSection : public ICriticalSection
{
public:
	AndroidCriticalSection();
	virtual ~AndroidCriticalSection();

	void Lock() override;
	void Unlock() override;

private:
	mutable pthread_mutex_t m_Mutex;
};

typedef AndroidCriticalSection	CriticalSection;