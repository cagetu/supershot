#pragma once

class	AndroidOutputConsole : public OutputConsole
{
public:
	AndroidOutputConsole();
	~AndroidOutputConsole();

	virtual void Show(bool show);
	virtual bool IsShow() const;

	void	Log(const char* msg);
	//void	Log(const wchar* msg);
};

typedef AndroidOutputConsole PlatformOutuptConsole;