// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
//	@class
//------------------------------------------------------------------
class RefCount
{
public:
	RefCount();
	virtual ~RefCount();

	void AddRef();
	int Release();

protected:
	volatile int m_RefCount;
};

//------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------
#define __DeclareReference(classname) \
public : \
	void AddRef() { Interlocked::InterlockedIncrement( &m_RefCount ); } \
	int Release() { Interlocked::InterlockedDecrement( &m_RefCount); if (m_RefCount <= 0) { delete this; } return m_RefCount; } \
private : \
	volatile int32 m_RefCount; \
private :

#define __ConstructReference \
	m_RefCount = 0; 

#define __DestructReference \
	CHECK(m_RefCount == 0, TEXT("RefCount == 0"));
