#pragma once

//----------------------------------------------------------------------------------------
//	Allocator
//	- Allocator Interface Class (Default Heap Allocator)
//----------------------------------------------------------------------------------------
class Allocator
{
public:
	static const unsigned int DEFAULT_ALIGN = 4;

	Allocator();
	virtual ~Allocator();

	virtual void*	Allocate(uint32 size, uint32 align = DEFAULT_ALIGN);
	virtual void	Deallocate(void* ptr);
	virtual void*	Reallocate(void* Original, SIZE_T Count, uint32 Alignment = DEFAULT_ALIGN);
	virtual int32	Size() const;
	virtual void	Reset();

	// Helper
public:
	//-------------------------------------------------------------------
	//	Allocate / Deallocate Object Type
	//-------------------------------------------------------------------
	template <class T>
	T* NewObject()
	{
		return new (Allocate(sizeof(T), __alignof(T))) T();
	}

	template <class T>
	void DeleteObject(T* object)
	{
		if (object)
		{
			object->~T();
			Deallocate(object);
		}
	}

	template <class T>
	T* NewObjects(SizeT count)
	{
		T* ptr = static_cast<T*>(Allocate(sizeof(T) * count, __alignof(T)));
		Memory::ConstructItems<T>(ptr, count);
		return ptr;
	}

	template <class T>
	void DeleteObjects(T* array, SizeT count)
	{
		Memory::DestructItems<T>(array, count);
		Deallocate(array);
	}

	//-------------------------------------------------------------------
	//	Allocate / Deallocate POD Type
	//-------------------------------------------------------------------
	template <class T>
	T* NewPOD()
	{
		return Allocate(sizeof(T), __alignof(T));
	}

	template <class T>
	void DeletePOD(T* object)
	{
		if (object)
		{
			Deallocate(object);
		}
	}

	template <class T>
	T* NewPODs(SizeT count)
	{
		T* ptr = static_cast<T*>(Allocate(sizeof(T) * count, __alignof(T)));
		return ptr;
	}

	template <class T>
	void DeletePODs(T* array, SizeT count)
	{
		Deallocate(array);
	}

private:
	/// Allocators cannot be copied.
	Allocator(const Allocator& other);
	Allocator& operator=(const Allocator& other);
};

namespace Memory
{
	void Initliaize();
	void Shutdown();

	static const unsigned int DEFAULT_ALIGN = 4;
	static const unsigned int ALLOCATION_OFFSET = sizeof(int32);
	static const unsigned int COUNT_OFFSET = sizeof(int32);

	struct Finalizer
	{
		void(*fn)(void* ptr);
		Finalizer* next;
	};

	template <class T>
	T* AllocObject(Allocator* allocator)
	{
		return new (allocator->Allocator(sizeof(T), __alignof(T))) T;
	}

	template <class T, typename Arg>
	T* AllocObject(Allocator* allocator, const Arg& arg)
	{
		return new (allocator->Allocator(sizeof(T), __alignof(T))) T(arg);
	}

	template <class T>
	void DeallocObject(Allocator* allocator, T* object)
	{
		if (object)
		{
			Memory::Destruct<T>(object);
			allocator->Deallocate(object);
		}
	}

	template <class T>
	T* AllocObjects(Allocator* allocator, SizeT count)
	{
		T* ptr = static_cast<T*>(allocator->Allocate(sizeof(T) * count, __alignof(T)));
		Memory::ConstructItems<T>(ptr, count);
		return ptr;
	}

	template <class T>
	void DeallocObjects(Allocator* allocator, T* ptr, SizeT count)
	{
		Memory::DestructItems<T>(ptr, count);
		allocator->Deallocate(ptr);
	}

	Allocator& DefaultAllocator();
}

#define CONSTRUCT(p, T, ...)	(new (p) T(__VA_ARGS__))
#define DESCTRUCT(p, T)			((T*)(p))->~T()


/// Creates a new object of type T using the allocator a to allocate the memory.
#define MAKE_NEW(T, ...)	(new (Memory::DefaultAllocator().Allocate(sizeof(T), alignof(T))) T(__VA_ARGS__))

/// Frees an object allocated with MAKE_NEW.
#define MAKE_DELETE(T, p)	{ if (p) { Memory::DeallocObject<T>(&Memory::DefaultAllocator(), p); p = NULL; } }