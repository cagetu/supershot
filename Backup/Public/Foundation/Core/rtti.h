// Copyright (c) 2006~. cagetu
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/** Rtti
	@author cagetu
	@desc
*/
//------------------------------------------------------------------
class Rtti
{
public:
	Rtti(const char* Name, const Rtti* pParent);

	/** 타입 이름 / 기본 타입
	*/
	const utf8::string& GetName() const { return m_Name; }
	const Rtti*	GetParent() const { return m_Parent; }

	/** Hierarchy
	*/
	bool IsInstanceOf(const Rtti& Type) const;
	bool IsInstanceOf(const utf8::string& TypeName) const;

	bool IsA(const Rtti& Type) const;
	bool IsA(const utf8::string& TypeName) const;

	/** operator
	*/
	bool operator ==(const Rtti& Rhs) const;
	bool operator ==(const Rtti* Rhs) const;

private:
	utf8::string	m_Name;		//!< 타입 이름
	const Rtti*		m_Parent;	//!< 부모 타입
};

//******************************************************************
//	매크로
//******************************************************************

//------------------------------------------------------------------
// insert in root class declaration
#define __DeclareRootRtti(Name) \
public: \
	static Rtti RTTI; \
    virtual Rtti* GetRTTI() const	{	return &RTTI;	} \
	\
    static bool IsExactOf(const Rtti* pRTTI, const Name* pObject) \
    { \
        if (!pObject) \
		        { \
            return false; \
		        } \
        return pObject->IsExactOf(pRTTI); \
    } \
    \
    bool IsExactOf(const Rtti* pRTTI) const \
    { \
        return (GetRTTI() == pRTTI); \
    } \
    \
    static bool IsDerivedFrom(const Rtti* pRTTI, const Name* pObject) \
    { \
        if (!pObject) \
		{ \
            return false; \
		 } \
        return pObject->IsDerivedFrom(pRTTI); \
    } \
    \
    bool IsDerivedFrom(const Rtti* pRTTI) const \
    { \
        return GetRTTI()->IsA(*pRTTI); \
    } \
    \
	bool IsInstanceOf(const Rtti& Type) const \
	{ \
		return GetRTTI()->IsInstanceOf(Type); \
	} \
	\
	bool IsInstanceOf(const utf8::string& TypeName) const \
	{ \
		return GetRTTI()->IsInstanceOf(TypeName); \
	} \
	\
	bool IsA(const Rtti& Type) const \
	{ \
		return GetRTTI()->IsA(Type); \
	} \
	\
	bool IsA(const utf8::string& TypeName) const \
	{ \
		return GetRTTI()->IsA(TypeName); \
	} \
	\
private:

// insert in root class source file
#define __ImplementRootRtti(rootName) \
    Rtti rootName::RTTI(#rootName, 0)

//------------------------------------------------------------------
#define __DeclareAbstractClass(class_name) \
public: \
    static Core::Rtti RTTI; \
    virtual Rtti* GetRTTI() const { return &RTTI; } \
private:

#define __ImplementAbstractClass(Namespace, Class, Parent) \
    Rtti Class::RTTI(#Namespace"@"#Class, &Parent::RTTI)

//------------------------------------------------------------------
#define __DeclareRtti \
public: \
    static Rtti RTTI; \
    virtual Rtti* GetRTTI() const { return &RTTI; } \
private:

#define __ImplementRtti(Namespace, Class, Parent) \
    Rtti Class::RTTI(#Namespace"@"#Class, &Parent::RTTI)

//------------------------------------------------------------------
#define __DeclareTemplateRtti \
public: \
    static Rtti RTTI; \
    virtual Rtti* GetRTTI() const { return &RTTI; }

#define __ImplementTemplateRtti(Namespace, Class, Parent) \
    template <> \
    Rtti Class::RTTI(#Namespace"@"#Class, &Parent::RTTI)

//------------------------------------------------------------------
#define __DeclareClass(Class)	\
public: \
    static Rtti	RTTI; \
    virtual Rtti*		GetRTTI() const { return &RTTI; } \
	\
	static Class*		Create(); \
private:

#define __ImplementClass(Namespace, Class, Parent)	\
	Class* Class::Create() \
	{ \
		Class* result = new Class(); \
		return result; \
	} \
	Rtti Class::RTTI(#Namespace"@"#Class, &Parent::RTTI);
