// Copyright (c) 2018~. cagetu79@gmail.com
//
//******************************************************************
#pragma once

namespace Template
{
	template <typename T> struct TRemoveReference { typedef T Type; };
	template <typename T> struct TRemoveReference<T& > { typedef T Type; };
	template <typename T> struct TRemoveReference<T&&> { typedef T Type; };

	template <typename T> struct TRValueToLValueReference { typedef T  Type; };
	template <typename T> struct TRValueToLValueReference<T&&> { typedef T& Type; };

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename TYPE>
	FORCEINLINE typename TRemoveReference<TYPE>::Type&& Move(TYPE&& value)
	{
		typedef typename TRemoveReference<TYPE>::Type CastType;

		return (CastType&&)value;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename TYPE>
	FORCEINLINE TYPE&& Forward(typename TRemoveReference<TYPE>::Type& value)
	{
		return (TYPE&&)value;
	}

	template <typename TYPE>
	FORCEINLINE TYPE&& Forward(typename TRemoveReference<TYPE>::Type&& value)
	{
		return (TYPE&&)value;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename TYPE>
	FORCEINLINE TYPE Copy(TYPE& Val)
	{
		return const_cast<const TYPE&>(Val);
	}

	template <typename TYPE>
	FORCEINLINE TYPE Copy(const TYPE& Val)
	{
		return Val;
	}

	template <typename TYPE>
	FORCEINLINE TYPE&& Copy(TYPE&& Val)
	{
		// If we already have an rvalue, just return it unchanged, rather than needlessly creating yet another rvalue from it.
		return Move(Val);
	}

	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	template <typename TYPE>
	FORCEINLINE SIZE_T Distance(TYPE* first, TYPE* last)
	{
		return last - first;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
/**
 * Traits class which tests if a type is integral.
 */
template <typename T>
struct TIsIntegral
{
	enum { Value = false };
};

template <> struct TIsIntegral<         bool> { enum { Value = true }; };
template <> struct TIsIntegral<         char> { enum { Value = true }; };
template <> struct TIsIntegral<signed   char> { enum { Value = true }; };
template <> struct TIsIntegral<unsigned char> { enum { Value = true }; };
template <> struct TIsIntegral<         char16_t> { enum { Value = true }; };
template <> struct TIsIntegral<         char32_t> { enum { Value = true }; };
template <> struct TIsIntegral<         wchar_t> { enum { Value = true }; };
template <> struct TIsIntegral<         short> { enum { Value = true }; };
template <> struct TIsIntegral<unsigned short> { enum { Value = true }; };
template <> struct TIsIntegral<         int> { enum { Value = true }; };
template <> struct TIsIntegral<unsigned int> { enum { Value = true }; };
template <> struct TIsIntegral<         long> { enum { Value = true }; };
template <> struct TIsIntegral<unsigned long> { enum { Value = true }; };
template <> struct TIsIntegral<         long long> { enum { Value = true }; };
template <> struct TIsIntegral<unsigned long long> { enum { Value = true }; };

template <typename T> struct TIsIntegral<const          T> { enum { Value = TIsIntegral<T>::Value }; };
template <typename T> struct TIsIntegral<      volatile T> { enum { Value = TIsIntegral<T>::Value }; };
template <typename T> struct TIsIntegral<const volatile T> { enum { Value = TIsIntegral<T>::Value }; };

//-----------------------------------------------------------------------------------------------------------------------------------------------------
/**
 * Traits class which tests if a type is a pointer.
 */
template <typename T>
struct TIsPointer
{
	enum { Value = false };
};

template <typename T> struct TIsPointer<               T*> { enum { Value = true }; };
template <typename T> struct TIsPointer<const          T*> { enum { Value = true }; };
template <typename T> struct TIsPointer<      volatile T*> { enum { Value = true }; };
template <typename T> struct TIsPointer<const volatile T*> { enum { Value = true }; };

template <typename T> struct TIsPointer<const          T> { enum { Value = TIsPointer<T>::Value }; };
template <typename T> struct TIsPointer<      volatile T> { enum { Value = TIsPointer<T>::Value }; };
template <typename T> struct TIsPointer<const volatile T> { enum { Value = TIsPointer<T>::Value }; };

//-----------------------------------------------------------------------------------------------------------------------------------------------------
/**
* Binary predicate class for sorting elements in reverse order.  Assumes < operator is defined for the template type.
*
* See: http://en.cppreference.com/w/cpp/utility/functional/greater
*/
template <typename T = void>
struct TGreater
{
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return B < A;
	}
};

template <>
struct TGreater<void>
{
	template <typename T>
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return B < A;
	}
};

/**
* Binary predicate class for sorting elements in order.  Assumes < operator is defined for the template type.
* Forward declaration exists in ContainersFwd.h
*
* See: http://en.cppreference.com/w/cpp/utility/functional/less
*/
template <typename T /*= void */>
struct TLess
{
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return A < B;
	}
};

template <>
struct TLess<void>
{
	template <typename T>
	FORCEINLINE bool operator()(const T& A, const T& B) const
	{
		return A < B;
	}
};


/**
 * Includes a function in an overload set if the predicate is true.  It should be used similarly to this:
 *
 * // This function will only be instantiated if SomeTrait<T>::Value is true for a particular T
 * template <typename T>
 * typename TEnableIf<SomeTrait<T>::Value, ReturnType>::Type Function(const T& Obj)
 * {
 *     ...
 * }
 *
 * ReturnType is the real return type of the function.
 */
template <bool Predicate, typename Result = void>
class TEnableIf;

template <typename Result>
class TEnableIf<true, Result>
{
public:
	typedef Result Type;
};

template <typename Result>
class TEnableIf<false, Result>
{ };


/** Tests whether two typenames refer to the same type. */
template<typename A, typename B>
struct TAreTypesEqual;

template<typename, typename>
struct TAreTypesEqual
{
	enum { Value = false };
};

template<typename A>
struct TAreTypesEqual<A, A>
{
	enum { Value = true };
};

#define ARE_TYPES_EQUAL(A,B) TAreTypesEqual<A,B>::Value


/**
 * Reverses the order of the bits of a value.
 * This is an TEnableIf'd template to ensure that no undesirable conversions occur.  Overloads for other types can be added in the same way.
 *
 * @param Bits - The value to bit-swap.
 * @return The bit-swapped value.
 */
template <typename T>
FORCEINLINE typename TEnableIf<TAreTypesEqual<T, uint32>::Value, T>::Type ReverseBits(T Bits)
{
	Bits = (Bits << 16) | (Bits >> 16);
	Bits = ((Bits & 0x00ff00ff) << 8) | ((Bits & 0xff00ff00) >> 8);
	Bits = ((Bits & 0x0f0f0f0f) << 4) | ((Bits & 0xf0f0f0f0) >> 4);
	Bits = ((Bits & 0x33333333) << 2) | ((Bits & 0xcccccccc) >> 2);
	Bits = ((Bits & 0x55555555) << 1) | ((Bits & 0xaaaaaaaa) >> 1);
	return Bits;
}
