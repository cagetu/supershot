// Copyright (c) 2006~. cagetu
//
//******************************************************************
#ifndef __OBJECT_H__
#define __OBJECT_H__

typedef unsigned long ObjectID;

//#include <map>

//------------------------------------------------------------------
/** IObject
	@author
		cagetu
	@since
		2006년 9월 20일
	@remarks
		객체의 최상위 클래스, 객체가 가져야 하는 기본 속성을 가진다.
		<delete this로 삭제한다는 것은 동적 메모리 할당해서 생성하겠다는 것을 뜻한다.>
	@see
		3D Game Engine Architecture( by Eberly ) - WildMagic3
*/
//------------------------------------------------------------------
class IObject //: public RefCount
{
	__DeclareRootRtti(IObject);
	__DeclareReference(IObject);
public:
	typedef Map<ObjectID, IObject*>	ObjectMap;

	//------------------------------------------------------------------
	//	Methods
	//------------------------------------------------------------------
	virtual ~IObject();

	ObjectID GetUID() const;

protected:
	/** 생성자
		@remarks
			이 객체만을 사용할 수 없음.
	*/
	IObject();

	//------------------------------------------------------------------
	//	Variables
	//------------------------------------------------------------------
	ObjectID		m_ulID;						//!< 객체 고유 ID
	static ObjectID	ms_ulNextID;				//!< 다음 객체에 할당할 ID

private:
	static ObjectMap ms_InUsed;					//!< 생성된 객체 리스트 (원하는 시점에서 객체의 존재 여부를 알아온다.)
};

// casting
template <class T>	T* StaticCast( IObject* pObject );
template <class T>	const T* StaticCast( const IObject* pObject );
template <class T>	T* DynamicCast( IObject* pObject );
template <class T>	const T* DynamicCast( const IObject* pObject );

#include "object.inl"

#endif	// __IObject_H__