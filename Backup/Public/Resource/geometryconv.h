#ifndef __GEOMETRY_CONVERTER
#define __GEOMETRY_CONVERTER

#include "geometry.h"

class	CGeometryConverter : public CGeometry
{
public :
	CGeometryConverter()
	{
		m_FP = NULL;
		m_Count = 0;
	}
	~CGeometryConverter()
	{
		if (m_FP != NULL)
			fclose(m_FP);
	}

	bool Open(const TCHAR* fname)
	{
		m_FP = unicode::fopen(fname, TEXT("wb"));
		return m_FP != NULL ? true : false;
	}

	bool Load(const TCHAR* fname, bool split=false, int splitcount=40)
	{
		if (split)
		{
			m_Flags |= CMeshData::_OBJ_SPLIT;
			m_SplitBoneCount = splitcount;
		}
		return CGeometry::Load(fname);
	}

	void Insert(const _DESC& desc)
	{
		if (m_Count++ == 0)
		{
			Write((unsigned char)0xff);
			Write((unsigned long)m_GUID);

			for(unsigned int i=0; i<m_BoneList.size(); i++)
			{
				Write((unsigned char)0x09);
				Write(m_BoneList[i].name);
				Write(m_BoneList[i].parent >= 0 ? m_BoneList[m_BoneList[i].parent].name : L"");
				Write(&m_BoneList[i].worldTM, sizeof(Mat4));
				Write(&m_BoneList[i].localTM, sizeof(Mat4));
			}
		}

		Write((unsigned char)0x08);

		Write(desc.name);
		Write(desc.diffuseMap);
		Write(desc.specularMap);
		Write(desc.normalMap);

		unsigned short flags = 0;

		flags |= desc.vtx ? 0x0001 : 0;
		flags |= desc.norm ? 0x0002 : 0;
		flags |= desc.diffuse ? 0x0004 : 0;
		flags |= desc.uv1 ? 0x0008 : 0;
		flags |= desc.uv2 ? 0x0010 : 0;
		flags |= desc.tan ? 0x0020 : 0;
		flags |= desc.shcoeff ? 0x0040 : 0;
		flags |= desc.vtxao ? 0x0080 : 0;
		flags |= desc.skinindex && desc.skinweight ? 0x0100 : 0;
		flags |= desc.bonecount > 0 ? 0x0200 : 0;

		Write((unsigned short)flags);
		Write((unsigned long)desc.vtxnum);

		_ASSERT(desc.vtxnum < 65536);

		if (flags&0x0001)
			Write(desc.vtx, sizeof(Vector3)*desc.vtxnum);

		if (flags&0x0002)
			Write(desc.norm, sizeof(Vector3)*desc.vtxnum);

		if (flags&0x0004)
			Write(desc.diffuse, sizeof(unsigned long)*desc.vtxnum);

		if (flags&0x0008)
			Write(desc.uv1, sizeof(float)*2*desc.vtxnum);

		if (flags&0x0010)
			Write(desc.uv2, sizeof(float)*2*desc.vtxnum);

		if (flags&0x0020)
			Write(desc.tan, sizeof(Vector4)*desc.vtxnum);

		if (flags&0x0040)
			Write(desc.shcoeff, sizeof(float)*9*desc.vtxnum);

		if (flags&0x0080)
			Write(desc.vtxao, sizeof(unsigned long)*desc.vtxnum);

		if (flags&0x0100)
		{
			Write(desc.skinindex, sizeof(unsigned char)*4*desc.vtxnum);
			Write(desc.skinweight, sizeof(unsigned char)*4*desc.vtxnum);
		}

		Write((unsigned long)desc.facen);
		Write(desc.face, sizeof(unsigned short)*desc.facen*3);

		Write((unsigned long)desc.bonecount);
		if (desc.bonecount > 0)
			Write(desc.bonetable, sizeof(int)*desc.bonecount);
	}
private :
	void Write(const TCHAR* fname)
	{
		char fname8[1024];
		string::conv_s(fname8, sizeof(fname8), fname);
		Write(fname8, strlen(fname8)+1);
	}
	void Write(unsigned short value) { Write(&value, 2); }
	void Write(unsigned long value) { Write(&value, 4); }
	void Write(unsigned char v) { Write(&v, 1); }
	void Write(const void* data, int l) { fwrite(data, 1, l, m_FP); }

	FILE* m_FP;
	int m_Count;
} ;

#endif