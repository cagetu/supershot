#ifndef _BONE_CONTROLLER_
#define _BONE_CONTROLLER_

class	CMeshData;

typedef int BONEHANDLE;

class	CBoneController : public RefCount
{
public :
	CBoneController(const CMeshData& geom);
	CBoneController(const CBoneController& ctl);
	virtual ~CBoneController();

	void SetTransform(const Mat4& tm) { m_WorldTM = tm; }
	const Mat4& GetTransform() const { return m_WorldTM; }

	void Update();

public:
	void Apply(const CBoneController& bonectl, float w = 1.0f, int *list = NULL, int listnum = 0);
	void ApplyDelta(CBoneController& bonectl, CBoneController& pivot);

	void Normalize();

public:
	void operator = (const CBoneController& ctl) const;

public:
	void SetWorldTM(int bonehandle, const Mat4 & tm);

	void SetLocalTM(int bonehandle, const Mat4 & tm);
	void SetLocalTM(int bonehandle, const Quat4& q);
	void SetLocalTM(int bonehandle, const Quat4& q, const Vector3 &p, float blendweight);

	void SetDeltaTM(const Mat4& tm);

	void GetTM(int bonehandle, OUT Mat4& tm) const;

	const Vector3& GetLocalPosition(int bonehandle) const;
	void GetLocalRotation(int bonehandle, OUT Quat4& q) const;
	Quat4 GetLocalRotation(int bonehandle) const;

	const Mat4& GetLocalTM(int bonehandle) const;
	const Mat4& GetWorldTM(int bonehandle) const;

	BONEHANDLE GetParent(int bonehandle) const;
	const TCHAR* GetBoneName(int bonehandle) const;
	int GetBoneCount() const;

	BONEHANDLE FindBone(const TCHAR* boneName) const;

private :
	void Reset();

	mutable Mat4* m_BoneTM;
	int m_BoneCount;
	Mat4 m_WorldTM;
	Mat4* m_LocalTM;

	Mat4 m_DeltaTM;

	const int * m_ParentTable;
	const CMeshData& m_Geom;
} ;

#endif