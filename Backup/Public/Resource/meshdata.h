#pragma once

class CPolygonSoup;
class CPrimitive;
class CBoneController;

//================================================================
/** MeshData Class
	@author		cagetu
	@since		2015. 5. 18
	@remarks	- Raw Data Mesh
*/
//================================================================
class CMeshData
{
public:
	CMeshData();
	virtual ~CMeshData();

	enum _OPTION
	{
		_BONELIST = 1 << 0,
		_VERTEXONLY = 1 << 1,
		_USE_VERTEXCOLOR = 1 << 2,
		_USE_SKIN = 1 << 3,
		_CALC_TANGENT = 1 << 4,
		_NOMATERIAL = 1 << 5,
		_FLIPV = 1 << 6,
		_REVERSE_WIND = 1 << 7,

		_EXTRA_UV = 1 << 10,
		_EXTRA_TRANSLUCENT = 1 << 11,
		_EXTRA_VERTEXAO = 1 << 12,

		_STOREMESH = 1 << 15,
		_SPLITMESH = 1 << 16,

		_OBJ_MERGE = 1 << 20,
		_OBJ_SPLIT = 1 << 21,
	};

	struct	_GEOM
	{
		String name;
		String bonename;

		int *bonetable;
		int bonecount;
		int boneindex;

		int vtxnum;
		Vector3 * vtx;
		Vector3 * norm;
		uint32 *diffuse;
		float(*uv1)[2];
		float(*uv2)[2];
		Vector4 * tan;

		int facen;
		unsigned short* face;

		CPrimitive * primitive;

		String diffuseMap;
		String specularMap;
		String normalMap;

		_GEOM() 
			: vtxnum(0), bonetable(NULL), vtx(NULL), norm(NULL), diffuse(NULL), uv1(NULL), uv2(NULL), tan(NULL)
			, facen(0), face(NULL)
			, primitive(NULL)
		{}
	};

	uint32 GetGUID() const { return m_GUID; }

	const _GEOM * FindGeom(const TCHAR* name, int index) const;
	const _GEOM * FindGeom(int index) const;

	unsigned int GetGeomCount() const;

	Ptr<CBoneController> CreateBoneController();

public:
	int FindBone(const TCHAR * bonename) const;
	int GetBoneCount() const { return (int)m_BoneList.size(); }
	const TCHAR* GetBoneName(unsigned int idx) const { return idx < m_BoneList.size() ? m_BoneList[idx].name.c_str() : NULL; }

	const int * GetBoneParentTable() const { return m_BoneParentTable; }
	const Mat4&	GetBoneInvTM(int h) const { return m_BoneInvTMTable[h]; }
	const Mat4&	GetBoneWorldTM(int h) const;

public:
	int FindSplit(const TCHAR* name, String* list);

protected:
	uint32 m_GUID;

	uint32 m_Flags;

	std::vector <_GEOM> m_GeomList;

protected:
	enum
	{
		_SKINNED = 1 << 1,
		_SKELETAL = 1 << 5,
	};

	struct _BONE
	{
		String name;

		int parent;

		Mat4 worldTM;
		Mat4 invWorldTM;
		Mat4 localTM;

		bool operator == (const String& bonename) const { return !unicode::strcmp(bonename.c_str(), name.c_str()); }
	};
	std::vector <_BONE> m_BoneList;

	std::map <String, int> m_BoneTable;
	String* m_BoneNameTable;
	Mat4* m_BoneInvTMTable;
	int * m_BoneParentTable;

//private:
//	static std::map <String, CMeshData*> m_GeomList;
};