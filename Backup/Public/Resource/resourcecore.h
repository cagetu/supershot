#pragma once

#include "renderer.h"

#include "imagehandler.h"

#include "polygonsoup.h"
#include "meshdata.h"
#include "geometry.h"
#include "geometryconv.h"
#include "geometryreader.h"
#include "modelloader.h"

//#include "bonecontroller.h"
#include "motioncontroller.h"
#include "animationcontroller.h"
#include "eventhandler.h"
#include "motionsynth.h"

#if defined(TARGET_OS_WINDOWS)
	#include "Windows/windowresource.h"
#elif defined(TARGET_OS_ANDROID)
	#include "Android/androidresource.h"
#elif defined(TARGET_OS_IPHONE)
	#include "iOS/iosresource.h"
#endif

