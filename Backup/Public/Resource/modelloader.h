#pragma once

class ModelData;

struct ModelLoader
{
	static bool Initialize() { return true; }
	static void Shutdown() {}

	static ModelData* LoadFBX(const TCHAR* filename) { return NULL; }
	static ModelData* LoadModel(const TCHAR* filename);	// by visualworks
};

/*
	1. Model Loader로 모델 파일을 읽어서, ModelData Cache 화
	2. 이후는 ModelData -> CModel로 인스턴스를 생성한다.

	ModelLoader -> ModelData 관리
	CModel 관리
*/


class ModelData
{
public:
	struct _MESH
	{
		// name
		String name;

		// material
		int materialID;

		// vertexbuffer
		unsigned int vertexCount;
		Vector3 * vertices;
		Vector3 * normals;
		uint32 * colors;
		float(*uv1)[2];
		float(*uv2)[2];
		Vector4 * tangents;
		unsigned char(*skinindex)[4];
		unsigned char(*skinweight)[4];

		// indexbuffer
		unsigned int faceCount;
		unsigned short* faces;

		AABB bound;

		///
		int *boneTable;
		int boneCount;

		VertexBuffer* vertexBuffer;
		IndexBuffer* indexBuffer;

		_MESH()
			: vertexCount(0), vertices(NULL), normals(NULL), colors(NULL), uv1(NULL), uv2(NULL), tangents(NULL)
			, materialID(-1)
			, faceCount(0), faces(NULL)
			, boneCount(0), boneTable(NULL)
			, skinindex(NULL), skinweight(NULL)
		{}

		void CalcTangent();
		void Construct();
	};

	struct _MATERIAL
	{
		String name;

		enum _TEXTURE
		{
			DIFFUSE = 1,
			SPECULAR,
			AMBIENT,
			EMISSIVE,
			HEIGHT,
			NORMALS,
			SHININESS,
			OPACITY,
			DISPLACEMENT,
			LIGHTMAP,
			REFLECTION,

			MAX_TEXTURE,
		};
		String textures[MAX_TEXTURE];
	};

	struct _ANIMATION
	{
	};

	struct _BONE
	{
		String name;

		int parent;

		Mat4 worldTM;
		Mat4 invWorldTM;
		Mat4 localTM;

		bool operator == (const String& bonename) const { return !unicode::strcmp(bonename.c_str(), name.c_str()); }
	};
	std::vector <_BONE> m_BoneList;

	std::vector<_MESH*> meshList;
	std::vector<_MATERIAL*> materialList;
};
