#ifndef _MOTIONSYNTHESIS
#define _MOTIONSYNTHESIS

class CMotionSynth : public RefCount
{
public :
	static CMotionSynth* Create(CMiniXML::const_iterator node, CModel* model);
	static void Release(CMotionSynth* ms);

public:
	void Update(float dt);
	void PreUpdate(float dt, CEventListener* listener = NULL);

public:
	CMotionSynth* Duplicate(CModel* model);

	void Reset();

	float GetLength(const TCHAR* motion);
	bool IsFinished(const TCHAR* id);

	void SetEnable(const TCHAR* tag, bool enable);
	void SetEnable(const TCHAR* tag);

	void SetAnimation(const TCHAR* name, const TCHAR* motion, bool force, bool notransition);
	void SetValue(const TCHAR* name, const Vector3& val);
	void SetValue(const TCHAR* name, const TCHAR* val);
	void SetValue(const TCHAR* name, float val);
	Vector3 GetMoveDelta();

	void Impulse(const Vector3 &pivot, const Vector3 &force);

	CBoneController* GetBoneController(const TCHAR* id);
	Vector3 FindTarget(const TCHAR *id);

private :
	CMotionSynth();
	~CMotionSynth();

	bool Init(CMiniXML::const_iterator node, CModel* model);

	CMotionController* FindAnimation(const TCHAR* anim) const;

private:
	friend class CModel;
	friend class CMotionController;

	std::set <String> m_Enable;
	std::map <String, Vector3> m_VecValue;

	Ptr<CBoneController> m_BoneController;

	typedef std::map <String, CMotionController*> AnimMap;

	AnimMap m_AnimationList;
	AnimMap* m_AnimationListRef;

	String m_DefaultCtrlrName;

	std::map <String, CMotionController*> m_MotionTable;
	std::vector <CMotionController*> m_MotionList;
	std::vector <CMotionController*> m_ActiveMotions;
} ;

#endif