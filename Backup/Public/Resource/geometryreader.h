#ifndef _GEOM_READER
#define _GEOM_READER

class	CGeometryReader
{
public :
	CGeometryReader(CFileIO * fp) { m_fp = fp; }
	
	String ReadString()
	{
		char text[1024]= {0,};
		int i=0;
		do
		{
			_ASSERT(i < sizeof(text));
			text[i] = ReadChar();
		}	while(text[i++] != '\0');
		return unicode::mbstring(text);
	}

	char ReadChar() { char ch; Read(&ch, 1); return ch; }
	float ReadFloat() { float f; Read(&f, 4); return f; }
	short ReadShort() { short s; Read(&s, 2); return s; }
	unsigned short ReadUShort() { unsigned short s; Read(&s, 2); return s; }
	unsigned long ReadULong() { unsigned long s; Read(&s, 4); return s; }
	Vector3 ReadVector() { float f[3]; Read(f, 4*3); return Vector3(f[0], f[2], f[1]); }
	Quat4 ReadQuat() { float f[4]; Read(f, 4*4); return Quat4(f[0], f[2], f[1], -f[3]); }
	Mat4 ReadMatrix()
	{
		Mat4 m = Mat4::IDENTITY;
		*(Vector3*)&m._11 = ReadVector();
		*(Vector3*)&m._31 = ReadVector();
		*(Vector3*)&m._21 = ReadVector();
		*(Vector3*)&m._41 = ReadVector();
		return m;
	}
	int Read(void * ptr, int len) { return m_fp->Read(ptr, len); }
	int Seek(int off, CFileIO::_SEEK pivot) { return m_fp->Seek(off, pivot); }

private :
	CFileIO * m_fp;
} ;

#endif