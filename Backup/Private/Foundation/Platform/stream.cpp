#include "foundation.h"

MemoryStream::MemoryStream()
	: m_Data(NULL)
	, m_CurPos(0)
	, m_bAutoDestroy(false)
{
}

MemoryStream::MemoryStream(uint8* buffer, bool bAutoDestroy)
	: m_Data(buffer)
	, m_CurPos(0)
	, m_bAutoDestroy(bAutoDestroy)
{
}

MemoryStream::~MemoryStream()
{
	if (m_bAutoDestroy)
		delete[] m_Data;

	m_Data = NULL;
	m_CurPos = 0;
}

int32 MemoryStream::ReadBytes(uint8* output, int32 size)
{
	if (output == NULL)
		return 0;

	Memory::MemCpy(output, CurrentData(), size);
	m_CurPos += size;

	return size;
}

int32 MemoryStream::WriteBytes(const uint8* input, int32 size)
{
	if (input == NULL)
		return 0;

	Memory::MemCpy(CurrentData(), input, size);
	m_CurPos += size;

	return size;
}

uint8* MemoryStream::CurrentData()
{
	return m_Data + m_CurPos;
}