#include "foundation.h"
#include <io.h>

void BaseSys::MemMove(void* dest, const void* src, size_t size)
{
	::memmove(dest, src, size);
}

void BaseSys::MemCpy(void* dest, const void* src, size_t size)
{
	::memcpy(dest, src, size);
}

void BaseSys::MemSet(void* dest, int32 val, size_t size)
{
	::memset(dest, val, size);
}

int32 BaseSys::MemCmp(const void* buf1, const void* buf2, size_t size)
{
	return ::memcmp(buf1, buf2, size);
}

void BaseSys::MemZero(void* dest, size_t size)
{
	::memset(dest, 0, size);
}

template <typename T>
static FORCEINLINE void Valswap(T& A, T& B)
{
	// Usually such an implementation would use move semantics, but
	// we're only ever going to call it on fundamental types and MoveTemp
	// is not necessarily in scope here anyway, so we don't want to
	// #include it if we don't need to.
	T Tmp = A;
	A = B;
	B = Tmp;
}

FILE * BaseSys::fopen(const TCHAR* fileName, const TCHAR* mode)
{
	return NULL;
}

void BaseSys::fclose(FILE * fp)
{
	CHECK(fp, TEXT("fp == NULL"));
	::fclose(fp);
}

int32 BaseSys::remove(const TCHAR* fileName)
{
	return -1;
}

int32 BaseSys::rename(const TCHAR* oldFileName, const TCHAR* newFileName)
{
	return -1;
}

int32 BaseSys::access(const TCHAR* fileName, int32 accmode)
{
	return -1;
}

int32 BaseSys::fread(FILE* fp, void* buffer, int32 elementSize, int32 elementCount)
{
	return (int32)::fread(buffer, elementSize, elementCount, fp);
}

int32 BaseSys::fseek(FILE* fp, int32 offset, int32 mode)
{
	return ::fseek(fp, offset, mode);
}

void BaseSys::Sleep(float sec)
{
	int32 milliSecs = int32(sec * 1000.0);
	::Sleep(milliSecs);
}

void BaseSys::Exit(int32 exitCode)
{
	::exit(exitCode);
}