#include "foundation.h"

WindowMutex::WindowMutex()
{
	m_hMutex = CreateMutex(NULL, FALSE, NULL);
}

WindowMutex::~WindowMutex()
{
	if (m_hMutex)
	{
		CloseHandle(m_hMutex);
	}
	m_hMutex = NULL;
}

void WindowMutex::Lock()
{
	WaitForSingleObject(m_hMutex, INFINITE);
}

void WindowMutex::Unlock()
{
	ReleaseMutex(m_hMutex);
}