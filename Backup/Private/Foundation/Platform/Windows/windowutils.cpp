#include "foundation.h"
#include <io.h>

void WindowSys::DebugOut(const TCHAR* format, ...)
{
	TCHAR temp[1024];
	va_list ap;
	int r;
	va_start(ap, format);
#if _UNICODE
	r = vswprintf(temp, _countof(temp), format, ap);
#else
	r = vsprintf(temp, _countof(temp), format, ap);
#endif
	va_end(ap);

	OutputDebugString(temp);
}

int WindowSys::Dialog(const TCHAR* text, const TCHAR* caption, int type)
{
	return ::MessageBox(NULL, text, caption, type);
}

void WindowSys::Prefetch(void* p, int32 offset)
{
	_mm_prefetch((const char*)((char*)p + offset), _MM_HINT_T0);
}

FILE * WindowSys::fopen(const TCHAR* fileName, const TCHAR* mode)
{
	FILE *fp = NULL;
#if _UNICODE
	::_wfopen_s(&fp, fileName, mode);
#else
	::fopen_s(&fp, fileName, mode);
#endif
	return fp;
}

void WindowSys::fclose(FILE * fp)
{
	::fclose(fp);
}

int32 WindowSys::remove(const TCHAR* fileName)
{
#if _UNICODE
	return ::_wremove(fileName);
#else
	return ::remove(fileName);
#endif
}

int32 WindowSys::rename(const TCHAR* oldFileName, const TCHAR* newFileName)
{
#if _UNICODE
	return ::_wrename(oldFileName, newFileName);
#else
	return ::rename(oldFileName, newFileName);
#endif
}

int32 WindowSys::access(const TCHAR* fileName, int32 accmode)
{
#if _UNICODE
	return ::_waccess(fileName, accmode);
#else
	return ::access(fileName, accmode);
#endif
}

void WindowSys::Sleep(float sec)
{
	DWORD milliSecs = DWORD(sec * 1000.0);
	::Sleep(milliSecs);
}