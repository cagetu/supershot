#include "foundation.h"

void IOSSys::DebugOut(const wchar* msg)
{
	NSLog(msg);
}

void IOSSys::DebugOutFormat(const wchar* format, ...)
{
	wchar logtxt[1024] = { 0, };
	va_list ap;
	va_start(ap, format);
	unicode::_sprintf(logtxt, _countof(logtxt), format, ap);
	va_end(ap);

	NSLog(logtxt);
}

FILE * IOSSys::fopen(const wchar * fname, const wchar * mode)
{
#if 1
	unicode::string strName = fname;
	strName.make_lower();

	char f[256] = { 0, };
	char t[16] = { 0, };
	unicode::convert(f, sizeof(f), strName.c_str());
	unicode::convert(t, sizeof(t), mode);
	FILE *fp = ::fopen(strName.c_str(), t);
	return fp;
#else
	unicode::string strName = fname;
	strName.make_lower();

	string fullpath = string(g_DataPath) + strName.c_str();
	FILE *fp = NULL;
	char f[256] = { 0, };
	char t[16] = { 0, };
	unicode::convert(f, sizeof(f), fullpath.c_str());
	unicode::convert(t, sizeof(t), mode);
	fp = ::fopen(f, t);

	if (fp == NULL)
	{
		fullpath = string(g_DataPath) + fname;
		unicode::convert(f, sizeof(f), fullpath.c_str());
		fp = ::fopen(f, t);
	}

	if (fp == NULL)
	{
		fullpath = string(g_ResourcePath) + strName.c_str();
		unicode::convert(f, sizeof(f), fullpath.c_str());
		fp = ::fopen(f, t);
	}

	if (fp == NULL)
	{
		fullpath = string(g_ResourcePath) + fname;
		unicode::convert(f, sizeof(f), fullpath.c_str());
		fp = ::fopen(f, t);
	}
	return fp;
#endif
}

void IOSSys::fclose(FILE * fp)
{
	::fclose(fp);
}

int IOSSys::remove(const char* fname)
{
	return ::remove(fname);
}

int IOSSys::remove(const wchar* fname)
{
	char f[256];
	unicode::convert(f, _countof(f), fname);
	return ::remove(f);
}

int IOSSys::rename(const char* foldname, const char* fnewname)
{
	return ::rename(foldname, fnewname);
}

int IOSSys::rename(const wchar* foldname, const wchar* fnewname)
{
	char fo[256];
	char fn[256];
	unicode::convert(fo, _countof(fo), foldname);
	unicode::convert(fn, _countof(fn), fnewname);
	return ::rename(fo, fn);
}

int IOSSys::access(const char* fname, int accmode)
{
	return ::_access(fname, accmode);
}

int IOSSys::access(const wchar* fname, int accmode)
{
	char f[256];
	unicode::convert(f, _countof(f), fname);
	return ::access(f, accmode);
}
