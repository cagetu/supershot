#include "foundation.h"

////////////////////////////////////////////////////////////////////////////

FileIO::FileIO(FILE *fp, int off, int len, Mode mode)
{
	m_Mode = mode;

	m_FP = fp;
	m_CurPos = off;
	if (len == 0)
	{
		fseek(fp, 0, SEEK_END);
		m_Length = ftell(fp) - m_CurPos;
	}
	else
	{
		m_Length = len;
	}
	fseek(fp, off, SEEK_SET);
}

FileIO::~FileIO()
{
	fclose(m_FP);
}

FileIO* FileIO::Open(const wchar * fname, Mode mode)
{
#ifdef TARGET_OS_ANDROID

	// android rule : 1) read ext path --> 2) read assets (apk)
	FILE *fp = NULL;
//	unicode::string extfullpath = CEngine::m_cachePath + L"/" + fname;// + L".jet";
//	extfullpath.make_lower();
//
//	char _extfullpath[256] = {0,};
//	unicode::utf16toutf8(_extfullpath, sizeof(_extfullpath), extfullpath.c_str());
//
//	// 1) Try to read from Ext sdcard
//	fp = ::fopen(_extfullpath, "rb");
//	if (fp)
//	{
//#ifdef _DEBUG
//		__android_log_print(ANDROID_LOG_INFO, "ENGINE-LOG", "--> Open from SDCARD %s OK! (FileIO)", _extfullpath);
//#endif
//		return fp ? FileIO::Open(fp, 0, 0) : NULL;
//	}
//
//	// 2) Try to read from APK (Assets)
//	char _assetbuffer[256] = { 0, };
//	unicode::string fullassetname = unicode::string(fname) + L".jet";
//	fullassetname.make_lower();
//	unicode::utf16toutf8(_assetbuffer, sizeof(_assetbuffer), fullassetname.c_str());
//	AAsset* asset = AAssetManager_open(CEngine::m_assets, _assetbuffer, AASSET_MODE_UNKNOWN);
//
//	if( asset == NULL )
//	{
//#ifdef _DEBUG
//		__android_log_print(ANDROID_LOG_ERROR, "ENGINE-LOG", "--> Not found %s from Android Assets..(fopen)", _assetbuffer);
//#endif
//		return NULL;
//	}
//
//	off_t outStart, outLength;
//	int fd = AAsset_openFileDescriptor(asset, &outStart, &outLength);
//	AAsset_close(asset);
//
//	if( fd >= 0 )
//	{
//		fp = fdopen(fd, "r");
//#ifdef _DEBUG
//		__android_log_print(ANDROID_LOG_DEBUG, "ENGINE-LOG", "--> Open from Assets %s OK! (%d, %d)", _assetbuffer, (int)outStart, (int)outLength);
//#endif
//		return FileIO::Open(fp, outStart, outLength);
//	}
//	else
//	{
//#ifdef _DEBUG
//		__android_log_print(ANDROID_LOG_ERROR, "ENGINE-LOG", "--> Open Android Assets Error: %s", _assetbuffer);
//#endif
//	}

	return NULL;
#else
	wchar* m;
	if (mode == FileIO::ReadOnly) m = L"rb";
	else if (mode == FileIO::WriteOnly)	m = L"wb";
	else
	{
		assert(0);
		return nullptr;
	}

	FILE* fp = PlatformSys::fopen(fname, m);
	return fp ? FileIO::Open(fp, 0, 0, mode) : nullptr;
#endif
}

FileIO* FileIO::Open(FILE* fp, int off, int len, Mode mode)
{
	return new FileIO(fp, off, len, mode);
}

FileIO* FileIO::Close(FileIO* file)
{
	delete file;
	return nullptr;
}

int FileIO::Seek(int offset, _SEEK pos)
{
	switch(pos)
	{
	case _SET:
		fseek(m_FP, m_CurPos + offset, SEEK_SET); 
		break;
	case _CUR:
		fseek(m_FP, offset, SEEK_CUR); 
		break;
	case _END:
		fseek(m_FP, m_CurPos + m_Length + offset, SEEK_SET); 
		break;
	}
	return ftell(m_FP) - m_CurPos;
}

int FileIO::Tell()
{
	return ftell(m_FP);
}

void FileIO::Flush()
{
	fflush(m_FP);
}

int FileIO::Read(uint8* output, int len)
{
	return (int)fread(output, sizeof(uint8), len, m_FP);
}

int FileIO::Write(void* ptr, int len)
{
#if UNICODE
	return (int)fwrite(ptr, sizeof(wchar), len, m_FP);
#else
	return (int)fwrite(ptr, sizeof(char), len, m_FP);
#endif
}

//////////////////////////////////////////////////////////////////


MemoryIO::MemoryIO(const uint8* buffer, int len, bool bAutoDestroy)
{
	m_Data = buffer;
	m_Length = len;
	m_Offset = 0;
	m_bAutoDestroy = bAutoDestroy;
}

MemoryIO::~MemoryIO()
{
	if (m_bAutoDestroy && m_Data)
		delete[] m_Data;
}

int MemoryIO::Read(uint8* output, int len)
{
	if (len + m_Offset > m_Length)
		return Read(output, m_Length - m_Offset);

	if (len > 0)
	{
		PlatformSys::MemCpy(output, &m_Data[m_Offset], len);
		m_Offset += len;
		return len;
	}

	return 0;
}

int MemoryIO::Seek(int offset, _SEEK pos)
{
	switch (pos)
	{
	case _SET: 
		m_Offset = offset;
		break;
	case _CUR:
		m_Offset += offset;
		break;
	case _END:
		m_Offset = m_Length + offset;
		break;
	}
	return m_Offset;
}


////////////////////////////////////////////////////////////////////////////

//std::map <unicode::string, unicode::string> CFileIO::m_PathTable;
//std::vector <unicode::string> CFileIO::m_BasePath;
//unicode::string CFileIO::m_Basezip;
//
//void CFileIO::SetBasePath(const TCHAR* basepath)
//{
//	m_PathTable.clear();
//	m_BasePath.push_back(basepath == NULL ? TEXT("") : basepath);
//
//	UpdateBasePath();
//}
//
//void CFileIO::SetBasePath(const std::vector<unicode::string>& pathlist)
//{
//	m_PathTable.clear();
//	m_BasePath = pathlist;
//
//	UpdateBasePath();
//}
//
//void CFileIO::UpdateBasePath()
//{
//	for (unsigned int n = 0; n<m_BasePath.size(); n++)
//	{
//		std::vector <unicode::string> list;
//
//#ifdef WIN32
//		FindFiles(list, m_BasePath[n]);
//#elif defined(TARGET_OS_ANDROID)
//		FindFiles(list, CEngine::m_cachePath.c_str());
//#endif
//
//		for (unsigned int i = 0; i<list.size(); i++)
//		{
//			const TCHAR* path = list[i];
//			if (unicode::strchr(path, '//'))
//			{
//				TCHAR filename[128];
//				unicode::strcpy_s(filename, _countof(filename), unicode::strrchr(path, '//') + 1);
//				unicode::strlwr_s(filename, _countof(filename));
//				m_PathTable[filename] = path;
//			}
//		}
//	}
//}
//
//void CFileIO::SetEncrypt(bool enable)
//{
//	CZip::SetEncrypt(enable);
//}
//
//void CFileIO::GetFilesList(std::vector <unicode::string> &list)
//{
//	std::map <unicode::string, unicode::string>::iterator it;
//	for (it = m_PathTable.begin(); it != m_PathTable.end(); it++)
//		list.push_back((*it).second);
//}
//
//const TCHAR* CFileIO::GetPath(const TCHAR* fname, bool rescanenable)
//{
//	TCHAR fname_lwr[512];
//	unicode::strcpy_s(fname_lwr, _countof(fname_lwr), fname);
//	unicode::strlwr_s(fname_lwr, _countof(fname_lwr));
//
//	std::map <unicode::string, unicode::string>::const_iterator it = m_PathTable.find(fname_lwr);
//	if (it != m_PathTable.end())
//		return (*it).second;
//
//	if (rescanenable == true)
//	{
//		UpdateBasePath();
//
//		it = m_PathTable.find(fname_lwr);
//		if (it != m_PathTable.end())
//			return (*it).second;
//	}
//	return fname;
//}
//
//
//bool CFileIO::GetFileList(const TCHAR* packagefile, std::vector <unicode::string>& list)
//{
//	return CZip::GetFileList(packagefile, list);
//}
//
//bool CFileIO::AddFile(const TCHAR* packagefile, const TCHAR *fname, const TCHAR* zipfname)
//{
//	CZip::Refresh(packagefile);
//	bool ret = CZip::AddFile(packagefile, fname, zipfname);
//	CZip::Refresh(packagefile);
//	return ret;
//}
//
//bool CFileIO::RemoveFile(const TCHAR* packagefile, const TCHAR *fname)
//{
//	CZip::Refresh(packagefile);
//	bool ret = CZip::RemoveFile(packagefile, fname);
//	CZip::Refresh(packagefile);
//	return ret;
//}
//
//int CFileIO::FindFile(const TCHAR* packagefile, const TCHAR* fname, int type)
//{
//	CZip::Refresh(packagefile);
//	return CZip::FindFile(packagefile, fname, type);
//}
//
//bool CFileIO::Access(const TCHAR* fname, bool writable)
//{
//	const TCHAR* path = GetPath(fname);
//	return unicode::access(path, writable ? 02 : 04) != -1 ? true : false;
//
//}
//
//bool CFileIO::Optimize(const TCHAR* packagefile)
//{
//	CZip::Refresh(packagefile);
//	return CZip::Optimize(packagefile);
//}
//
//void* CFileIO::ReadFile(const TCHAR* fname, int* length)
//{
//	CFileIO * f = CFileIO::OpenFile(fname);
//
//	if (f == NULL)
//		return NULL;
//
//	unsigned char *inputbuf = new unsigned char[f->GetSize()];
//	*length = f->Read(inputbuf, f->GetSize());
//	_ASSERT(*length == f->GetSize());
//	CFileIO::Release(f);
//	return inputbuf;
//}
//
//void CFileIO::Remove(const TCHAR* fname)
//{
//	unicode::remove(fname);
//}