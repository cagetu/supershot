// Copyright (c) 2006~. cagetu
//
//******************************************************************

#include "foundation.h"

//------------------------------------------------------------------
__ImplementRootRtti( IObject );
//------------------------------------------------------------------
ObjectID IObject::ms_ulNextID = 0;
IObject::ObjectMap IObject::ms_InUsed;

//------------------------------------------------------------------
//Const/Dest
IObject::IObject()
{
	__ConstructReference;
	m_ulID = ms_ulNextID++;
	ms_InUsed.Insert(m_ulID, this);
}
//------------------------------------------------------------------
IObject::~IObject()
{
	__DestructReference;
	IndexT index = ms_InUsed.FindIndex(m_ulID);
	CHECK(index > INVALID_INDEX, TEXT("IObject::~IObject"));
	ms_InUsed.RemoveAt(index);
}
