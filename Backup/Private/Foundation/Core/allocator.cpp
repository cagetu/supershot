#include "foundation.h"

//------------------------------------------------------------------==
//	Allocator
//------------------------------------------------------------------==
Allocator::Allocator()
{
}
Allocator::~Allocator()
{
}

void* Allocator::Allocate(uint32 size, uint32 align)
{
	assert(size > 0);

	size = Memory::Align(size, align);
	return ::malloc(size);
}

void Allocator::Deallocate(void* ptr)
{
	return ::free(ptr);
}

void* Allocator::Reallocate(void* ptr, SIZE_T size, uint32 alignment)
{
	assert(size > 0);

	size = Memory::Align(size, alignment);
	return ::realloc(ptr, size);
}

int32 Allocator::Size() const
{
	return 0;
}

void Allocator::Reset()
{
}