#include "foundation.h"

//------------------------------------------------------------------
Rtti::Rtti(const char* Name, const Rtti* Parent)
	: m_Name(Name)
	, m_Parent(Parent)
{
}

//------------------------------------------------------------------
bool Rtti::IsA(const Rtti& Type) const
{
	const Rtti* type = this;
	while (type)
	{
		if (type == &Type)
			return true;

		type = type->m_Parent;
	}
	return false;
}

//------------------------------------------------------------------
bool Rtti::IsA(const utf8::string& TypeName) const
{
	const Rtti* type = this;
	while (type)
	{
		if (type->GetName() == TypeName)
			return true;

		type = type->m_Parent;
	}
	return false;
}

//--------------------------------------------------------------
bool Rtti::IsInstanceOf(const Rtti& Type) const
{
	return &Type == this;
}

bool Rtti::IsInstanceOf(const utf8::string& TypeName) const
{
	return TypeName == GetName();
}

//--------------------------------------------------------------
bool Rtti::operator ==(const Rtti& Rhs) const
{
	return this->GetName() == Rhs.GetName();
}

bool Rtti::operator ==(const Rtti* Rhs) const
{
	return this->GetName() == Rhs->GetName();
}

