#include "foundation.h"

RefCount::RefCount()
	: m_RefCount(0)
{
}
RefCount::~RefCount()
{
	CHECK(m_RefCount == 0, TEXT("RefCount == 0"));
}

void RefCount::AddRef()
{
	//++m_RefCount;
	Interlocked::InterlockedIncrement(&m_RefCount);
}

int RefCount::Release()
{
	//--m_RefCount;
	Interlocked::InterlockedDecrement(&m_RefCount);
	if (m_RefCount <= 0)
		delete this;

	return m_RefCount;
}