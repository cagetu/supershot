#include "foundation.h"

float Capsule::SweepTest(const Sphere & sphere, const Vec3 & direct, Plane * out) const
{
//	if (Math::IsNearlyZero(Vec3::Magnitude(v1-v2)) == true)
//		return SweepTest(sphere, direct, Sphere(v1, radius), out);

	float r = (sphere.radius + radius);
	float rr = r * r;

	Vec3 va = (sphere.Position()-v1) - (v2-v1) * (Vec3::DotProduct(sphere.Position()-v1, v2-v1) / Vec3::DotProduct(v2-v1, v2-v1));
	Vec3 vb = (sphere.Position()+direct-v1) - (v2-v1) * (Vec3::DotProduct(sphere.Position()+direct-v1, v2-v1) / Vec3::DotProduct(v2-v1, v2-v1));

	float dp_v2_v1 = Vec3::DotProduct(vb-va, vb-va);

	if (::abs(dp_v2_v1) < 0.00001f)
	{	//	v1 == v2 (capsule 에 평행한 경우 직선인 경우)
		if (Vec3::DotProduct(va, va) > rr)
			return 1.0f;
	}
	else
	{
		float tt = Vec3::DotProduct(-va, vb-va) / dp_v2_v1;
		float hh = Vec3::Magnitude(va + (vb-va) * tt);

		if (hh > r)
			return 1.0f;

		tt -= sqrt(rr - hh*hh) / Vec3::Magnitude(vb-va);

 		if (tt >= 0.0f && tt < 1.0f)
		{
			Vec3 p2 = sphere.Position() + direct * tt;
			Vec3 p1 = p2 - (va + (vb-va) * tt);

			float d1 = Vec3::DotProduct(p1-v1, v2-v1);

			if (d1 <= 0)
			{
				Sphere s1(v1, radius);
				return s1.SweepTest(sphere, direct, out);
			}
			if (d1 >= Vec3::DotProduct(v2-v1, v2-v1))
			{
				Sphere s2(v2, radius);
				return s2.SweepTest(sphere, direct, out);
			}

			Vec3 norm = Vec3::Normalize(p2-p1);

			if (out != NULL)
				*out = Plane(norm, radius == 0 ? p1 : p1 + norm*radius);
			return tt;
		}

		if (tt > 1.0f)
			return 1.0f;
	}

	if (Vec3::DotProduct(direct, v2-v1) < 0)
	{
		Sphere s1(v1, radius);
		return s1.SweepTest(sphere, direct, out);
	}
	else
	{
		Sphere s2(v2, radius);
		return s2.SweepTest(sphere, direct, out);
	}
}

bool Capsule::ContactTest(const Sphere & sphere) const
{
	float r = (sphere.radius + radius);
	float rr = r * r;

	float t = Vec3::DotProduct(sphere.Position()-v1, v2-v1) / Vec3::DotProduct(v2-v1, v2-v1);
	if (t < 0)
		t = 0;
	else if (t > 1)
		t = 1;

	return Vec3::Magnitude((sphere.Position()-v1) - (v2-v1) * t) <= sphere.radius ? true : false;
}