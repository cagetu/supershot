#include "foundation.h"
#include "mtrand.h"

#define _USE_MTRAND	1

#if _USE_MTRAND
static MTRand mtRandPrivate;
#endif

void Math::RandInit(int32 seed) 
{
#if _USE_MTRAND
	mtRandPrivate.seed(seed);
#else
	::srand(seed);
#endif
}

int32 Math::RandInt()
{
#if _USE_MTRAND
	return mtRandPrivate.randInt();
#else
	return ::rand();
#endif
}

float Math::RandFloat()
{
#if _USE_MTRAND
	return mtRandPrivate.rand();
#else
	return ::rand() / (float)RAND_MAX; 
#endif
}
