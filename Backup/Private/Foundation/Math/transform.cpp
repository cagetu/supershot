#include "foundation.h"
#include "DirectX/d3dtransform.h"
#include "OpenGL/opengltransform.h"
#include "Vulkan/vulkantransform.h"

ICoordinate* Transform::m_pCoordinate = nullptr;

void Transform::_Init(COORDINATE coordinate)
{
	// 플랫폼에 따른 Impl 작성해서 적용해주자!!!!
	switch (coordinate)
	{
	case _D3D:
		m_pCoordinate = new D3DCoordinate();
		break;

	case _OPENGL:
		m_pCoordinate = new OpenGLCoordinate();
		break;

	case _VULKAN:
		m_pCoordinate = new VulkanCoordinate();
		break;

	default:
		_ASSERT(0);
	}
}

void Transform::_Shutdown()
{
	if (m_pCoordinate)
	{
		delete m_pCoordinate;
		m_pCoordinate = nullptr;
	}
}

Mat4 Transform::RotationX(float rad)
{
	float cs = cosf(rad);
	float sn = sinf(rad);
	Mat4 m = {
		1, 0, 0, 0,
		0, +cs, +sn, 0,
		0, -sn, +cs, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::RotationY(float rad)
{
	float cs = cosf(rad);
	float sn = sinf(rad);
	Mat4 m = {
		+cs, 0, -sn, 0,
		0, 1, 0, 0,
		+sn, 0, +cs, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::RotationZ(float rad)
{
	float cs = cosf(rad);
	float sn = sinf(rad);
	Mat4 m = {
		+cs, +sn, 0, 0,
		-sn, +cs, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::Rotation(const Quat4& q)
{
	float xx, yy, zz, xy, xz, yz, wx, wy, wz;
	Mat4 m;

	xx = 2.0f * q.x * q.x;
	yy = 2.0f * q.y * q.y;
	zz = 2.0f * q.z * q.z;
	xy = 2.0f * q.x * q.y;
	xz = 2.0f * q.x * q.z;
	yz = 2.0f * q.y * q.z;
	wx = 2.0f * q.w * q.x;
	wy = 2.0f * q.w * q.y;
	wz = 2.0f * q.w * q.z;

	m._11 = 1 - (yy + zz);
	m._12 = (xy - wz);
	m._13 = (xz + wy);
	m._14 = 0;

	m._21 = (xy + wz);
	m._22 = 1 - (xx + zz);
	m._23 = (yz - wx);
	m._24 = 0;

	m._31 = (xz - wy);
	m._32 = (yz + wx);
	m._33 = 1 - (xx + yy);
	m._34 = 0;

	m._41 = 0;
	m._42 = 0;
	m._43 = 0;
	m._44 = 1;

	return m;
}

Mat4 Transform::Multiply(const Mat4& a, const Mat4& b)
{
	Mat4 m;

	m._11 = a._11 * b._11 + a._12 * b._21 + a._13 * b._31 + a._14 * b._41;
	m._12 = a._11 * b._12 + a._12 * b._22 + a._13 * b._32 + a._14 * b._42;
	m._13 = a._11 * b._13 + a._12 * b._23 + a._13 * b._33 + a._14 * b._43;
	m._14 = a._11 * b._14 + a._12 * b._24 + a._13 * b._34 + a._14 * b._44;

	m._21 = a._21 * b._11 + a._22 * b._21 + a._23 * b._31 + a._24 * b._41;
	m._22 = a._21 * b._12 + a._22 * b._22 + a._23 * b._32 + a._24 * b._42;
	m._23 = a._21 * b._13 + a._22 * b._23 + a._23 * b._33 + a._24 * b._43;
	m._24 = a._21 * b._14 + a._22 * b._24 + a._23 * b._34 + a._24 * b._44;

	m._31 = a._31 * b._11 + a._32 * b._21 + a._33 * b._31 + a._34 * b._41;
	m._32 = a._31 * b._12 + a._32 * b._22 + a._33 * b._32 + a._34 * b._42;
	m._33 = a._31 * b._13 + a._32 * b._23 + a._33 * b._33 + a._34 * b._43;
	m._34 = a._31 * b._14 + a._32 * b._24 + a._33 * b._34 + a._34 * b._44;

	m._41 = a._41 * b._11 + a._42 * b._21 + a._43 * b._31 + a._44 * b._41;
	m._42 = a._41 * b._12 + a._42 * b._22 + a._43 * b._32 + a._44 * b._42;
	m._43 = a._41 * b._13 + a._42 * b._23 + a._43 * b._33 + a._44 * b._43;
	m._44 = a._41 * b._14 + a._42 * b._24 + a._43 * b._34 + a._44 * b._44;
	return m;
}

Vec4 Transform::Multiply(const Mat4& m, const Vec4& p)
{
	return TransformVector(m, p);
}

Mat4 Transform::Scaling(const Vec3& s)
{
	Mat4 m = 
	{
		s.x, 0, 0, 0,
		0, s.y, 0, 0,
		0, 0, s.z, 0,
		0, 0, 0, 1
	};
	return m;
}

Mat4 Transform::Scaling(float s)
{
	return Scaling(Vec3(s, s, s));
}

Mat4 Transform::Translate(const Vec3& p)
{
	Mat4 m = 
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		p.x, p.y, p.z, 1
	};
	return m;
}

Mat4 Transform::Translate(float x, float y, float z)
{
	Mat4 m = 
	{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		x, y, z, 1
	};
	return m;
}

Vec4 Transform::TransformVector(const Mat4& m, const Vec4& p)
{
	Vec4 out(p.x*m._11 + p.y*m._21 + p.z*m._31 + p.w*m._41,
			p.x*m._12 + p.y*m._22 + p.z*m._32 + p.w*m._42,
			p.x*m._13 + p.y*m._23 + p.z*m._33 + p.w*m._43,
			p.x*m._14 + p.y*m._24 + p.z*m._34 + p.w*m._44);
	return out;
}

Vec3 Transform::TransformPoint(const Mat4& m, const Vec3& p)
{
	return Vec3(p.x*m._11 + p.y*m._21 + p.z*m._31 + m._41,
				p.x*m._12 + p.y*m._22 + p.z*m._32 + m._42,
				p.x*m._13 + p.y*m._23 + p.z*m._33 + m._43);
}

Vec3 Transform::TransformNormal(const Mat4& m, const Vec3& p)
{
	return Vec3(p.x*m._11 + p.y*m._21 + p.z*m._31,
				p.x*m._12 + p.y*m._22 + p.z*m._32,
				p.x*m._13 + p.y*m._23 + p.z*m._33);
}

Vec3 Transform::TransformHomogen(const Mat4& m, const Vec3& p)
{
	Vec4 out(p.x*m._11 + p.y*m._21 + p.z*m._31 + m._41,
			p.x*m._12 + p.y*m._22 + p.z*m._32 + m._42,
			p.x*m._13 + p.y*m._23 + p.z*m._33 + m._43,
			p.x*m._14 + p.y*m._24 + p.z*m._34 + m._44);
	return Vec3(out.x / out.w, out.y / out.w, out.z / out.w);
}

void Transform::Decompose(const Mat4& input, Vec3& translate, Quat4& rotate, Vec3& scale)
{
	scale = input.GetScale();
	translate = input.GetPosition();
	input.GetRotation(rotate);
}

Mat4 Transform::LookAt(const Vec3& pivot, const Vec3& target, const Vec3& upvec)
{
	Vec3 zaxis = Vec3::Normalize(target - pivot);
	Vec3 xaxis = Vec3::Normalize(Vec3::CrossProduct(upvec, zaxis));
	//	Vec3 yaxis = Vec3::Normalize(Vec3::CrossProduct(zaxis, xaxis));
	Vec3 yaxis = Vec3::CrossProduct(zaxis, xaxis);

	Mat4 m;
	m._11 = xaxis.x; m._12 = xaxis.y; m._13 = xaxis.z; m._14 = 0.0f;
	m._21 = yaxis.x; m._22 = yaxis.y; m._23 = yaxis.z; m._24 = 0.0f;
	m._31 = zaxis.x; m._32 = zaxis.y; m._33 = zaxis.z; m._34 = 0.0f;
	m._41 = pivot.x; m._42 = pivot.y; m._43 = pivot.z; m._44 = 1.0f;
	return m;
}

Mat4 Transform::LookAt(const Vec3& pivot, const Vec3& target)
{
	Vec3 z = Vec3::Normalize(target - pivot);
	return LookAt(pivot, target, ::abs(z.y) > 0.999f ? Vec3::ZAXIS : Vec3::YAXIS);
}

Mat4 Transform::LookAtZAxis(const Vec3& pivot, const Vec3& zaxis)
{
	Vec3 upvec = ::abs(zaxis.y) > 0.999f ? Vec3::ZAXIS : Vec3::YAXIS;

	Vec3 xaxis = Vec3::Normalize(Vec3::CrossProduct(upvec, zaxis));
	Vec3 yaxis = Vec3::CrossProduct(zaxis, xaxis);

	Mat4 m;
	m._11 = xaxis.x; m._12 = xaxis.y; m._13 = xaxis.z; m._14 = 0.0f;
	m._21 = yaxis.x; m._22 = yaxis.y; m._23 = yaxis.z; m._24 = 0.0f;
	m._31 = zaxis.x; m._32 = zaxis.y; m._33 = zaxis.z; m._34 = 0.0f;
	m._41 = pivot.x; m._42 = pivot.y; m._43 = pivot.z; m._44 = 1.0f;
	return m;
}

Mat4 Transform::Projection(float nearWidth, float nearHeight, float nearClip, float farClip)
{
	return m_pCoordinate->Projection(nearWidth, nearHeight, nearClip, farClip);
}

//	@desc : z 구간으로 매핑되는 정규 시야 영역으로 변환
Mat4 Transform::ProjectionWithFov(float fov, float nearClip, float farClip, float aspectRatio)
{
	return m_pCoordinate->ProjectionWithFov(fov, nearClip, farClip, aspectRatio);
}

// @desc : z 구간으로 매핑되는 정규 시야 영역으로 변환
Mat4 Transform::ProjectionOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	return m_pCoordinate->ProjectionOffCenter(l, r, b, t, zn, zf);
}

//	@desc : z 구간으로 매핑되는 정규 시야 영역으로 변환
Mat4 Transform::InfiniteProjectionWithFov(float fov, float nearClip, float aspectRatio, float epsilon)
{
	return m_pCoordinate->InfiniteProjectionWithFov(fov, nearClip, aspectRatio, epsilon);
}

// @desc : 직교 투영
Mat4 Transform::OrthoOffCenter(float left, float right, float bottom, float top, float nearClip, float farClip)
{
	return m_pCoordinate->OrthoOffCenter(left, right, bottom, top, nearClip, farClip);
}

// @desc : 직교투영
Mat4 Transform::Ortho(float width, float height, float nearClip, float farClip)
{
	return m_pCoordinate->Ortho(width, height, nearClip, farClip);
}
