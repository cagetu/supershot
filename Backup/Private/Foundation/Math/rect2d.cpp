#include "foundation.h"

//------------------------------------------------------------------

Rect2D::Rect2D(float x, float y, float width, float height) 
	: X(x), Y(y), Width(width), Height(height) 
{
}

Rect2D::Rect2D(const Vec2& leftTop, const Vec2& rightBottom)
{
	X = leftTop.x;
	Y = leftTop.y;
	Width = (rightBottom.x - leftTop.x);
	Height = (rightBottom.y - leftTop.y);
}

bool Rect2D::IntersectTest(const Vec2& segmentStart, const Vec2& segmentEnd)
{
	// 사각형을 가로지르는 라인의 경우, 교차점이 두 개가 될 수 있다.
	// 2개의 교차점을 가지고 와야 하는 경우에는 어느 Edge에서 교차가 되었고, Edge의 교차점이 무엇인지를 반환해줘야 한다.

	const Vec2 Rect[4] =
	{
		Vec2(X, Y),					// Left, Top
		Vec2(X + Width, Y),			// Right, Top
		Vec2(X + Width, Y + Height),	// Right, Bottom
		Vec2(X, Y + Height),			// Left, Bottom
	};
	for (int32 index = 0; index < 4; index++)
	{
		Point2 ContactPoint;
		if (IntersectSegments(Rect[index], Rect[(index + 1) % 4], segmentStart, segmentEnd, ContactPoint))
			return true;
	}
	return false;
}

//------------------------------------------------------------------

Box2D::Box2D(const Vec2& min, const Vec2& max)
	: Min(min), Max(max)
{

}

bool Box2D::Intersect(const Box2D& other) const
{
#pragma message("TODO")
	return false;
}