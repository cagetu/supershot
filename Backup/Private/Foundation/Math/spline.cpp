#include "foundation.h"

namespace Spline
{
	Vec2 CatmullRom(const Vec2 &p0, const Vec2 &p1, const Vec2 &p2, const Vec2 &p3, float t)
	{
		float t2 = t * t;
		float t3 = t2 * t;

		return Vec2(
			0.5f * ((2.0f*p1.x) + (-p0.x+p2.x)*t + (2.0f*p0.x - 5.0f*p1.x + 4*p2.x - p3.x)*t2 + (-p0.x + 3.0f*p1.x - 3.0f*p2.x + p3.x)*t3),
			0.5f * ((2.0f*p1.y) + (-p0.y+p2.y)*t + (2.0f*p0.y - 5.0f*p1.y + 4*p2.y - p3.y)*t2 + (-p0.y + 3.0f*p1.y - 3.0f*p2.y + p3.y)*t3));
	}

	//
	// Vector3
	//
	Vec3 CatmullRom(const Vec3 &p0, const Vec3 &p1, const Vec3 &p2, const Vec3 &p3, float t)
	{
		float t2 = t * t;
		float t3 = t2 * t;

		return Vec3(
			0.5f * ((2.0f*p1.x) + (-p0.x+p2.x)*t + (2.0f*p0.x - 5.0f*p1.x + 4*p2.x - p3.x)*t2 + (-p0.x + 3.0f*p1.x - 3.0f*p2.x + p3.x)*t3),
			0.5f * ((2.0f*p1.y) + (-p0.y+p2.y)*t + (2.0f*p0.y - 5.0f*p1.y + 4*p2.y - p3.y)*t2 + (-p0.y + 3.0f*p1.y - 3.0f*p2.y + p3.y)*t3),
			0.5f * ((2.0f*p1.z) + (-p0.z+p2.z)*t + (2.0f*p0.z - 5.0f*p1.z + 4*p2.z - p3.z)*t2 + (-p0.z + 3.0f*p1.z - 3.0f*p2.z + p3.z)*t3));
	}
	
	//
	// Vector4
	//
	Vec4 CatmullRom(const Vec4 &p0, const Vec4 &p1, const Vec4 &p2, const Vec4 &p3, float t)
	{
		float t2 = t * t;
		float t3 = t2 * t;

		return Vec4(
			0.5f * ((2.0f*p1.x) + (-p0.x+p2.x)*t + (2.0f*p0.x - 5.0f*p1.x + 4*p2.x - p3.x)*t2 + (-p0.x + 3.0f*p1.x - 3.0f*p2.x + p3.x)*t3),
			0.5f * ((2.0f*p1.y) + (-p0.y+p2.y)*t + (2.0f*p0.y - 5.0f*p1.y + 4*p2.y - p3.y)*t2 + (-p0.y + 3.0f*p1.y - 3.0f*p2.y + p3.y)*t3),
			0.5f * ((2.0f*p1.z) + (-p0.z+p2.z)*t + (2.0f*p0.z - 5.0f*p1.z + 4*p2.z - p3.z)*t2 + (-p0.z + 3.0f*p1.z - 3.0f*p2.z + p3.z)*t3),
			1.0f);
	}
}

