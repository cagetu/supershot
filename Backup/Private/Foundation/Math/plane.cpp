#include "foundation.h"

Plane::Plane(const Point3 &v1, const Point3 &v2, const Point3 &v3)
{
	Vec3 norm = Vec3::Normalize(Vec3::CrossProduct(v2-v1, v3-v1));
	a = norm.x, b = norm.y, c = norm.z, d = -Vec3::DotProduct(norm, v1);
}

Plane Plane::Transform(const Mat4 & tm) const
{
	Vec3 va(-d * a, -d * b, -d*c);
	Vec3 vb = va * tm;
	Vec3 vv = tm.TransformNormal(Normal());
	return Plane(vv, -Vec3::DotProduct(vv, vb));
}

void Plane::Normalize()
{
	float s = 1.0f / sqrtf(a*a + b*b + c*c);
	a *= s;
	b *= s;
	c *= s;
	d *= s;
}