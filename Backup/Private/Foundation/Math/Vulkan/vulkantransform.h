#pragma once

#include "../transformimpl.h"
//#include "Math/matrix.h"

class VulkanCoordinate : public ICoordinate
{
public:
	virtual Mat4 Projection(float nearwidth, float nearheight, float nearclip, float farclip) override;
	virtual Mat4 ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio) override;
	virtual Mat4 ProjectionOffCenter(float l, float r, float b, float t, float nearclip, float farclip) override;
	virtual Mat4 InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon = 2.4f*10e-7f) override;
	virtual Mat4 Ortho(float w, float h, float nearclip, float farclip) override;
	virtual Mat4 OrthoOffCenter(float l, float r, float b, float t, float nearclip, float farclip) override;
};