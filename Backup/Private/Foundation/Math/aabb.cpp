#include "foundation.h"

AABB::AABB(const Vec3* points, int num)
{
	AABB aabb = points[0];
	for(int i=1; i<num; i++)
		aabb.Expand(points[i]);
	*this = aabb;
}

void AABB::Expand(const Vec3 & p)
{
	if (p.x < minx) minx = p.x;
	if (p.x > maxx) maxx = p.x;
	if (p.y < miny) miny = p.y;
	if (p.y > maxy) maxy = p.y;
	if (p.z < minz) minz = p.z;
	if (p.z > maxz) maxz = p.z;
}

void AABB::Expand(const AABB & bound)
{
	if (bound.minx < minx) minx = bound.minx;
	if (bound.maxx > maxx) maxx = bound.maxx;
	if (bound.miny < miny) miny = bound.miny;
	if (bound.maxy > maxy) maxy = bound.maxy;
	if (bound.minz < minz) minz = bound.minz;
	if (bound.maxz > maxz) maxz = bound.maxz;
}

void AABB::Expand(float size)
{
	minx -= size;
	miny -= size;
	minz -= size;
	maxx += size;
	maxy += size;
	maxz += size;
}

void AABB::Intersection(const AABB & bound)
{
	if (bound.minx > minx) minx = bound.minx;
	if (bound.maxx < maxx) maxx = bound.maxx;
	if (bound.miny > miny) miny = bound.miny;
	if (bound.maxy < maxy) maxy = bound.maxy;
	if (bound.minz > minz) minz = bound.minz;
	if (bound.maxz < maxz) maxz = bound.maxz;
}

bool AABB::IntersectTest(const AABB & bound) const
{
	if (bound.maxx < minx) return false;
	if (bound.minx > maxx) return false;
	if (bound.maxy < miny) return false;
	if (bound.miny > maxy) return false;
	if (bound.maxz < minz) return false;
	if (bound.minz > maxz) return false;
	return true;
}

bool AABB::IntersectTest(const Sphere& s) const
{
	if(s.x < minx && minx - s.x > s.radius ) return false;
	if(s.x > maxx && s.x - maxx > s.radius ) return false;
	if(s.y < miny && miny - s.y > s.radius ) return false;
	if(s.y > maxy && s.y - maxy > s.radius ) return false;
	if(s.z < minz && minz - s.z > s.radius ) return false;
	if(s.z > maxz && s.z - maxz > s.radius ) return false;
	return true;
}

void AABB::Translate(const Vec3& v)
{
	minx += v.x;
	miny += v.y;
	minz += v.z;
	maxx += v.x;
	maxy += v.y;
	maxz += v.z;
}

AABB AABB::Transform(const Mat4 & tm) const
{
	Vec3 p = Min() * tm;
	AABB hull(p, p);
	for(int i=1; i<8; i++)
	{
		p = Vec3((i&1) ? minx : maxx, (i&2) ? miny : maxy, (i&4) ? minz : maxz) * tm;
		if (p.x < hull.minx) hull.minx = p.x;
		if (p.x > hull.maxx) hull.maxx = p.x;
		if (p.y < hull.miny) hull.miny = p.y;
		if (p.y > hull.maxy) hull.maxy = p.y;
		if (p.z < hull.minz) hull.minz = p.z;
		if (p.z > hull.maxz) hull.maxz = p.z;
	}
	return hull;
}

float AABB::RayIntersect(const Vec3 & p, const Vec3 & dir, Plane* out) const
{
	if (p.x < minx)
	{
		if (p.x + dir.x < minx)
			return 1.0f;

		float t = (minx - p.x) / dir.x;

		float y = p.y + dir.y * t;
		float z = p.z + dir.z * t;

		if (y >= miny && y <= maxy && z >= minz && z <= maxz)
		{
			if (out)
				*out = Plane(-1, 0, 0, minx);
			return t;
		}
	}
	if (p.x > maxx)
	{
		if (p.x + dir.x > maxx)
			return 1.0f;

		float t = (maxx - p.x) / dir.x;

		float y = p.y + dir.y * t;
		float z = p.z + dir.z * t;

		if (y >= miny && y <= maxy && z >= minz && z <= maxz)
		{
			if (out)
				*out = Plane(1, 0, 0, maxx);
			return t;
		}
	}


	if (p.y < miny)
	{
		if (p.y + dir.y < miny)
			return 1.0f;

		float t = (miny - p.y) / dir.y;

		float x = p.x + dir.x * t;
		float z = p.z + dir.z * t;

		if (x >= minx && x <= maxx && z >= minz && z <= maxz)
		{
			if (out)
				*out = Plane(0, -1, 0, miny);
			return t;
		}
	}
	if (p.y > maxy)
	{
		if (p.y + dir.y > maxy)
			return 1.0f;

		float t = (maxy - p.y) / dir.y;

		float x = p.x + dir.x * t;
		float z = p.z + dir.z * t;

		if (x >= minx && x <= maxx && z >= minz && z <= maxz)
		{
			if (out)
				*out = Plane(0, 1, 0, maxy);
			return t;
		}
	}


	if (p.z < minz)
	{
		if (p.z + dir.z < minz)
			return 1.0f;

		float t = (minz - p.z) / dir.z;

		float x = p.x + dir.x * t;
		float y = p.y + dir.y * t;

		if (x >= minx && x <= maxx && y >= miny && y <= maxy)
		{
			if (out)
				*out = Plane(0, 0, -1, minz);
			return t;
		}
	}
	if (p.z > maxz)
	{
		if (p.z + dir.z > maxz)
			return 1.0f;

		float t = (maxz - p.z) / dir.z;

		float x = p.x + dir.x * t;
		float y = p.y + dir.y * t;

		if (x >= minx && x <= maxx && y >= miny && y <= maxy)
		{
			if (out)
				*out = Plane(0, 0, 1, maxz);
			return t;
		}
	}

	return 1.0f;
}

void AABB::GetPlanes(Plane* planes) const
{
	// cw ������.. ?!?!
	static const int _face[6][4] =
	{
		0, 2, 3, 1, // -z
		1, 3, 7, 5, // +x
		3, 2, 6, 7, // +y
		2, 0, 4, 6, // -x
		0, 1, 5, 4, // -y
		7, 6, 4, 5, // +z
	} ;

	static Vec3 v[8];
	GetPoints(v);

	for(int i=0; i<6; i++)
	{
		planes[i] = Plane(v[_face[i][0]], v[_face[i][1]], v[_face[i][2]]);
		planes[i].Normalize();
	}
}

void AABB::GetPoints(Vec3* points) const 
{
	/*
			6---------7
		   /|        /|  
	      / |       / |
	     2---------3  |
		 |	4- - - | -5
		 | /       | / 
		 |/        |/
		 0---------1
	*/
	for(int i=0; i<8; i++)
		points[i] = Vec3((i&1) ? maxx : minx, (i&2) ? maxy : miny, (i&4) ? maxz :minz);
}


float AABB::Distance(Vec3& point) const
{
	float dist = 0.f;
	// x
	{
		if( point.x < minx ) dist += (minx - point.x) * (minx - point.x);
		if( point.x > maxx ) dist += (point.x - maxx) * (point.x - maxx);
	}

	// y
	{
		if( point.y < miny ) dist += (miny - point.y) * (miny - point.y);
		if( point.y > maxy ) dist += (point.y - maxy) * (point.y - maxy);
	}

	// z
	{
		if( point.z < minz ) dist += (minz - point.z) * (minz - point.z);
		if( point.z > maxz ) dist += (point.z - maxz) * (point.z - maxz);
	}
	return dist;
}

bool AABB::BoundTest(const Vec3 &startp, const Vec3 &direct, float radius) const
{
	float t1=0.0f, t2=1.0f;
	Vec3 dv;
	int i;

	const float* v = &direct.x;
	const float* min = &minx;
	const float* max = &maxx;
	const float* startv = &startp.x;

	for(i=0; i<3; i++)
	{
		if (v[i] > 0)
		{
			t1 = Math::Max((min[i] - radius - startv[i]) / v[i], t1);
			t2 = Math::Min((max[i] + radius - startv[i]) / v[i], t2);
		}	else
		if (v[i] < 0)
		{
			t1 = Math::Max((max[i] + radius - startv[i]) / v[i], t1);
			t2 = Math::Min((min[i] - radius - startv[i]) / v[i], t2);
		}	else
		{
			if (min[i] > startv[i] + radius || max[i] < startv[i] - radius)
				return false;
			continue;
		}

		if (t1 > t2)
			return false;
	}

	return true;
}

static int _box_idx[][4] =
{
	0, 2, 6, 4,
	5, 7, 3, 1,
	5, 1, 0, 4,
	3, 7, 6, 2,
	1, 3, 2, 0,
	4, 6, 7, 5,	
} ;

float AABB::SweepTest(const Sphere& pos, const Vec3& vec, Plane* out) const
{
	if (BoundTest(pos.Position(), vec, pos.radius) == true)
	{
		Vec3 v[8];
		float t, mint=1.0f;
		Plane pl;
		int i, j;

		for(i=0; i<8; i++)
		{
			v[i] = Vec3((i&1) ? minx : maxx, (i&2) ? miny : maxy, (i&4) ? minz : maxz);
		}

		const float* dv = &vec.x;
		for(i=0; i<3; i++)
		{
			int off = dv[i] < 0 ? i*2 : i*2+1;
			Vec3 p[4];
			for(j=0; j<4; j++)
				p[j] = v[_box_idx[off][j]];
			t = Sphere::SweepTest(pos, vec, p, 4, &pl);
			if (t < mint)
			{
				mint = t;
				if (out != NULL)
					*out = pl;
			}
		}

		return mint;
	}
	return 1.0f;
}
