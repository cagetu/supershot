#include "foundation.h"

Mat4 Mat4::IDENTITY = { 1, 0, 0, 0, 0, 1, 0, 0,  0, 0, 1, 0, 0, 0, 0, 1 };

Mat4 Mat4::Inverse() const
{
	float fA0 = _11*_22 - _12*_21;
	float fA1 = _11*_23 - _13*_21;
	float fA2 = _11*_24 - _14*_21;
	float fA3 = _12*_23 - _13*_22;
	float fA4 = _12*_24 - _14*_22;
	float fA5 = _13*_24 - _14*_23;
	float fB0 = _31*_42 - _32*_41;
	float fB1 = _31*_43 - _33*_41;
	float fB2 = _31*_44 - _34*_41;
	float fB3 = _32*_43 - _33*_42;
	float fB4 = _32*_44 - _34*_42;
	float fB5 = _33*_44 - _34*_43;

	float fDet = fA0*fB5-fA1*fB4+fA2*fB3+fA3*fB2-fA4*fB1+fA5*fB0;	

	if ( fabs(fDet) <= __EPSILON )
	{		
		return Mat4::IDENTITY;
	}

	Mat4 kInv;
	kInv._11 = + _22*fB5 - _23*fB4 + _24*fB3;
	kInv._21 = - _21*fB5 + _23*fB2 - _24*fB1;
	kInv._31 = + _21*fB4 - _22*fB2 + _24*fB0;
	kInv._41 = - _21*fB3 + _22*fB1 - _23*fB0;
	kInv._12 = - _12*fB5 + _13*fB4 - _14*fB3;
	kInv._22 = + _11*fB5 - _13*fB2 + _14*fB1;
	kInv._32 = - _11*fB4 + _12*fB2 - _14*fB0;
	kInv._42 = + _11*fB3 - _12*fB1 + _13*fB0;
	kInv._13 = + _42*fA5 - _43*fA4 + _44*fA3;
	kInv._23 = - _41*fA5 + _43*fA2 - _44*fA1;
	kInv._33 = + _41*fA4 - _42*fA2 + _44*fA0;
	kInv._43 = - _41*fA3 + _42*fA1 - _43*fA0;
	kInv._14 = - _32*fA5 + _33*fA4 - _34*fA3;
	kInv._24 = + _31*fA5 - _33*fA2 + _34*fA1;
	kInv._34 = - _31*fA4 + _32*fA2 - _34*fA0;
	kInv._44 = + _31*fA3 - _32*fA1 + _33*fA0;

	float fInvDet = ((float)1.0)/fDet;
	kInv._11 *= fInvDet;
	kInv._12 *= fInvDet;
	kInv._13 *= fInvDet;
	kInv._14 *= fInvDet;
	kInv._21 *= fInvDet;
	kInv._22 *= fInvDet;
	kInv._23 *= fInvDet;
	kInv._24 *= fInvDet;
	kInv._31 *= fInvDet;
	kInv._32 *= fInvDet;
	kInv._33 *= fInvDet;
	kInv._34 *= fInvDet;
	kInv._41 *= fInvDet;
	kInv._42 *= fInvDet;
	kInv._43 *= fInvDet;
	kInv._44 *= fInvDet;

	return kInv;
}

Mat4 Mat4::Transpose() const
{
	Mat4 m;
	m._11 = _11; m._12 = _21; m._13 = _31; m._14 = _41;
	m._21 = _12; m._22 = _22; m._23 = _32; m._24 = _42;
	m._31 = _13; m._32 = _23; m._33 = _33; m._34 = _43;
	m._41 = _14; m._42 = _24; m._43 = _34; m._44 = _44;
	return m;
}

Vec3 Mat4::Transform(const Vec3 & p) const
{
	return Vec3(p.x*_11 + p.y*_21 + p.z*_31 + _41, p.x*_12 + p.y*_22 + p.z*_32 + _42, p.x*_13 + p.y*_23 + p.z*_33 + _43);
}

Vec4 Mat4::Transform(const Vec4 & p) const
{
	return Transform::Multiply(*this, p);
}

Vec3 Mat4::TransformNormal(const Vec3 & p) const
{
	return Vec3(p.x*_11 + p.y*_21 + p.z*_31, p.x*_12 + p.y*_22 + p.z*_32, p.x*_13 + p.y*_23 + p.z*_33);
}

Vec3 Mat4::TransformHomogen(const Vec3& p) const
{
	Vec4 out;
	out = Vec4(p.x*_11 + p.y*_21 + p.z*_31 + _41, p.x*_12 + p.y*_22 + p.z*_32 + _42, p.x*_13 + p.y*_23 + p.z*_33 + _43, p.x*_14 + p.y*_24 + p.z*_34 + _44);
	return Vec3(out.x/out.w, out.y/out.w, out.z/out.w);
}

Mat4 Mat4::Multiply(const Mat4 & a, const Mat4 & b)
{
	return Transform::Multiply(a, b);
}

Mat4 Mat4::Make(const Quat4& rot, const Vec3& translate)
{
	Mat4 tm = Transform::Rotation(rot);
	tm.GetPosition() = translate;
	return tm;
}

Mat4 Mat4::Make(const Vec3& xaxis, const Vec3& yaxis, const Vec3& zaxis, const Vec3& pivot)
{
	Mat4 tm = Mat4::IDENTITY;
	tm._11 = xaxis.x; tm._12 = xaxis.y; tm._13 = xaxis.z;
	tm._21 = yaxis.x; tm._22 = yaxis.y; tm._23 = yaxis.z;
	tm._31 = zaxis.x; tm._32 = zaxis.y; tm._33 = zaxis.z;
	tm._41 = pivot.x; tm._42 = pivot.y; tm._43 = pivot.z;
	return tm;
}

Mat4 Mat4::Make(const Vec3& axis, float rad)
{
	float	c = cos(rad),
			s = sin(rad),
			t = 1.0f - c;

	Vec3::Normalize(axis);
	Mat4 m = {	t * axis.x * axis.x + c,
				t * axis.x * axis.y - s * axis.z,
				t * axis.x * axis.z + s * axis.y,
				0.0,
				t * axis.x * axis.y + s * axis.z,
				t * axis.y * axis.y + c,
				t * axis.y * axis.z - s * axis.x,
				0.0,
				t * axis.x * axis.z - s * axis.y,
				t * axis.y * axis.z + s * axis.x,
				t * axis.z * axis.z + c,
				0.0,
				0.0, 0.0, 0.0, 1.0 };

	return m;

}

// Mirrored 된 매트릭스인지를 판정할 때 사용한다.
float Mat4::GetDeterminant() const
{
	return _11 * _22 * _33;
}

void Mat4::SetAxises(const Vec3* axises)
{
	_ASSERT(axises);

	// Row1
	_11 = axises[0].x;
	_12 = axises[0].y;
	_13 = axises[0].z;

	// Row2
	_21 = axises[1].x;
	_22 = axises[1].y;
	_23 = axises[1].z;

	// Row2
	_31 = axises[2].x;
	_32 = axises[2].y;
	_33 = axises[2].z;
}

void Mat4::GetAxises(Vec3* outAxises)
{
	_ASSERT(outAxises);

	// Row1
	outAxises[0].x = _11;
	outAxises[0].y = _12;
	outAxises[0].z = _13;

	// Row2
	outAxises[1].x = _21;
	outAxises[1].y = _22;
	outAxises[1].z = _23;

	// Row2
	outAxises[2].x = _31;
	outAxises[2].y = _32;
	outAxises[2].z = _33;
}

void Mat4::Decompose(Vec3& translate, Quat4& rotate, Vec3& scale) const
{
#ifdef _D3D9
	D3DXMatrixDecompose((D3DXVec3*)&scale.x, (D3DXQUATERNION*)&rotate.x, (D3DXVec3*)&translate, (const D3DXMATRIX*)this);
#endif
}

void Mat4::Decompose(Vec3& translate, Vec3& rotate, Vec3& scale) const
{
	translate = Vec3(_41, _42, _43);
	scale.x =  Vec3::Magnitude(Vec3(_11, _12, _13));
	scale.y =  Vec3::Magnitude(Vec3(_21, _22, _23));
	scale.z =  Vec3::Magnitude(Vec3(_31, _32, _33));

	float sx = _32 / scale.z;
	float cx = sqrtf(1.f - sx * sx);

	if( cx < 0.0000001f )
	{
		rotate.x = sx > 0 ? _PI / 2.f : -_PI / 2.f;
		rotate.y = atan2f( _31 / scale.z, _11 / scale.x );
		rotate.z = 0.f;
	}
	else
	{
		rotate.x = atan2f(sx, cx);
		rotate.y = atan2f(-_31 / scale.z, _33 / scale.z);
		rotate.z = atan2f(-_12 / scale.x, _22 / scale.y);
	}
}

void Mat4::SetRotation(const Quat4& q)
{
	*this = Transform::Rotation(q);
}

void Mat4::GetRotation(Quat4& q) const
{
	float t = _11 + _22+ _33, l, s;

	if (t > 0.0f)
	{
		l = (float) sqrt(t + 1.0f);
		s = 0.5f / l;
		q.w = l * 0.5f;
		q.x = (_32 - _23) * s;
		q.y = (_13 - _31) * s;
		q.z = (_21 - _12) * s;
	}	else
	{
		switch((_11 > _22) ? (_11 > _33 ? 0 : 2) : (_22 > _33 ? 1 : 2))
		{
		case 0 :
			l = (float) sqrt(1.0f + _11 - _22 - _33);
			s = 0.5f / l;
			q.x = l * 0.5f;
			q.y = (_21 + _12) * s;
			q.z = (_13 + _31) * s;
			q.w = (_32 - _23) * s;
			break;

		case 1 :
			l = (float) sqrt(1.0f - _11 + _22 - _33);
			s = 0.5f / l;
			q.x = (_12 + _21) * s;
			q.y = l * 0.5f;
			q.z = (_23 + _32) * s;
			q.w = (_13 - _31) * s;
			break;

		case 2 :
			l = (float) sqrt(1.0f - _11 - _22 + _33);
			s = 0.5f / l;
			q.x = (_31 + _13) * s;
			q.y = (_23 + _32) * s;
			q.z = l * 0.5f;
			q.w = (_21 - _12) * s;
			break;
		}
	}
}

void Mat4::GetRotation(Mat4& rot) const
{
	rot = *this;
	rot._14 = 0.0f;
	rot._24 = 0.0f;
	rot._34 = 0.0f;
	rot._41 = 0.0f;
	rot._42 = 0.0f;
	rot._43 = 0.0f;
}

void Mat4::SetScale(float scale)
{
	_11 = scale;
	_22 = scale;
	_33 = scale;
}

void Mat4::SetScale(const Vec3& scale)
{
	_11 = scale.x;
	_22 = scale.y;
	_33 = scale.z;
}

void Mat4::SetPosition(const Vec3& p)
{
	_41 = p.x; _42 = p.y; _43 = p.z;
}

void Mat4::Translate(const Vec3 & p)
{
	_41 = p.x; 
	_42 = p.y; 
	_43 = p.z;
}

void Mat4::Rotate(const Quat4& q)
{
	*this = Transform::Rotation(q);
}

void Mat4::Scale(float scale)
{
	_11 = scale;
	_22 = scale;
	_33 = scale;
}

Mat4 Mat4::Mirror(const Vec3& n, const Point3& p)
{
	float k = Vec3::DotProduct(n, p);

	Mat4 m = { 1.0f-2.0f*n.x*n.x,-2.0f*n.y*n.x,		-2.0f*n.z*n.x,      0.0f,
	  		  -2.0f*n.x*n.y,	  1.0f-2.0f*n.y*n.y,-2.0f*n.z*n.y,	    0.0f,
			  -2.0f*n.x*n.z,	 -2.0f*n.y*n.z,		 1.0f-2.0f*n.z*n.z, 0.0f,
			   2.0f*n.x*k,		  2.0f*n.y*k,		 2.0f*n.z*k,		1.0f } ;
	return m;
}


///// MAT4RIGID

Mat4rigid Mat4rigid::Transpose() const
{
	Mat4rigid m;
	m._11 = _11; m._12 = _21; m._13 = _31; m._14 = _41;
	m._21 = _12; m._22 = _22; m._23 = _32; m._24 = _42;
	m._31 = _13; m._32 = _23; m._33 = _33; m._34 = _43;
	m._41 = _14; m._42 = _24; m._43 = _34; m._44 = _44;
	return m;
}

/// Mat3

Mat3 Mat3::IDENTITY = { 1, 0, 0,
						0, 1, 0, 
						0, 0, 1 };

void Mat3::FromMat4(const Mat4& m)
{
	_11 = m._11; _12 = m._12; _13 = m._13;
	_21 = m._21; _22 = m._22; _23 = m._23;
	_31 = m._31; _32 = m._32; _33 = m._33;
}

Mat4 Mat3::ToMat4() const
{
	Mat4 m;
	m._11 = _11;  m._12 = _21;  m._13 = _31;  m._14 = 0.0f;
	m._21 = _12;  m._22 = _22;  m._23 = _32;  m._24 = 0.0f;
	m._31 = _13;  m._32 = _23;  m._33 = _33;  m._34 = 0.0f;
	m._41 = 0.0f; m._42 = 0.0f; m._43 = 0.0f; m._44 = 1.0f;
	return m;
}

void Mat3::SetPosition(const Vec2& p)
{
	_31 = p.x;
	_32 = p.y;
}

void Mat3::SetRotation(float rad)
{
	float c = cos(rad);
	float s = sin(rad);

	_11 = c;	_12 = s;  _13 = 0.0f;
	_21 = -s;	_22 = c;  _23 = 0.0f;
	_31 = 0;	_32 = 0;  _33 = 1.0f;
}

float Mat3::GetRotation() const
{
	return Math::ACos(_11);
}

void Mat3::SetScale(float scale)
{
	_11 = scale;
	_22 = scale;
}

void Mat3::SetScale(const Vec2& scale)
{
	_11 = scale.x;
	_22 = scale.y;
}

Vec2 Mat3::GetScale() const
{
	return Vec2(_11, _22);
}

Mat3 Mat3::Rotation(float rad)
{
	float c = cos(rad);
	float s = sin(rad);

	Mat3 m;
	m._11 = c;  m._12 = s;  m._13 = 0.0f;
	m._21 =-s;  m._22 = c;  m._23 = 0.0f;
	m._31 = 0;  m._32 = 0;  m._33 = 1.0f;
	return m;
}

Mat3 Mat3::Make(const Vec2& pos, float rad)
{
	float c = cos(rad);
	float s = sin(rad);

	Mat3 m;
	m._11 = c;	   m._12 = s;	 m._13 = 0.0f;
	m._21 =-s;	   m._22 = c;	  m._23 = 0.0f;
	m._31 = pos.x; m._32 = pos.y;  m._33 = 1.0f;
	return m;
}

Mat3 Mat3::Scaling(float w, float h)
{
	Mat3 m;
	m._11 = w;  m._12 = 0;  m._13 = 0;
	m._21 = 0;  m._22 = h;  m._23 = 0;
	m._31 = 0;  m._32 = 0;  m._33 = 1;
	return m;
}

Mat3 Mat3::Translate(const Vec2 & p)
{
	Mat3 m;
	m._11 = 1;   m._12 = 0;   m._13 = 0;
	m._21 = 0;	 m._22 = 1;   m._23 = 0;
	m._31 = p.x; m._32 = p.y; m._33 = 1;
	return m;
}

Mat3 Mat3::Multiply(const Mat3& a, const Mat3 & b)
{
	Mat3 m;
	m._11 = a._11 * b._11 + a._12 * b._21 + a._13 * b._31;
	m._12 = a._11 * b._12 + a._12 * b._22 + a._13 * b._32;
	m._13 = a._11 * b._13 + a._12 * b._23 + a._13 * b._33;

	m._21 = a._21 * b._11 + a._22 * b._21 + a._23 * b._31;
	m._22 = a._21 * b._12 + a._22 * b._22 + a._23 * b._32;
	m._23 = a._21 * b._13 + a._22 * b._23 + a._23 * b._33;

	m._31 = a._31 * b._11 + a._32 * b._21 + a._33 * b._31;
	m._32 = a._31 * b._12 + a._32 * b._22 + a._33 * b._32;
	m._33 = a._31 * b._13 + a._32 * b._23 + a._33 * b._33;
	return m;
}

Vec2 Mat3::Transform(const Vec2& v, const Mat3& m)
{
	Vec2 p;
	p.x = v.x * m._11 + v.y * m._21 + m._31;
	p.y = v.x * m._12 + v.y * m._22 + m._32;
	return p;
}