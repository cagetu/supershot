#include "foundation.h"

//
// Vector2
//
Vec2 Vec2::ZERO(0, 0);
Vec2 Vec2::ONE(1, 1);

bool IntersectSegments(const Vec2& seg1Start, const Vec2& seg1End, const Vec2& seg2Start, const Vec2& seg2End, Point2& outContactPoint)
{
	const Vec2 Segment1Dir = seg1End - seg1Start;
	const Vec2 Segment2Dir = seg2End - seg2Start;

	const float Determinant = Vec2::CrossProduct(Segment1Dir, Segment2Dir);
	if (!Math::IsNearlyZero(Determinant))
	{
		const Vec2 SegmentStartDelta = seg2Start - seg1Start;
		const float OneOverDet = 1.0f / Determinant;
		const float Seg1Intersection = Vec2::CrossProduct(SegmentStartDelta, Segment2Dir) * OneOverDet;
		const float Seg2Intersection = Vec2::CrossProduct(SegmentStartDelta, Segment1Dir) * OneOverDet;

		outContactPoint = Seg1Intersection * Segment1Dir + seg1Start;

		const float Epsilon = 0.000001f;//1 / 128.0f;
		return (Seg1Intersection > Epsilon && Seg1Intersection < 1.0f - Epsilon && Seg2Intersection > Epsilon && Seg2Intersection < 1.0f - Epsilon);
	}

	return false;
}

//
// Vector3
//
Vec3 Vec3::ZERO(0, 0, 0);
Vec3 Vec3::ONE(1, 1, 1);
Vec3 Vec3::XAXIS(1, 0, 0);
Vec3 Vec3::YAXIS(0, 1, 0);
Vec3 Vec3::ZAXIS(0, 0, 1);

//
// Vector4
//
Vec4 Vec4::ZERO(0, 0, 0, 0);
Vec4 Vec4::ONE(1, 1, 1, 1);
