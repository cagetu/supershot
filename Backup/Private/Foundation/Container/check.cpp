#include "foundation.h"

namespace Assertion
{
	/* #Assertions
		- http://twvideo01.ubm-us.net/o1/vault/gdc2012/slides/Programming%20Track/Pieket_Developing_Imperfect_Software.pdf 
		- 스킵 가능한 Assert를 만든다. 사용자에게 최대한 사용이 실패하지 않도록 한다. 툴 개발자에게 최대한 많은 정보를 남기도록 하여, 서버에 전송하여 DB에 리포팅한 정보를 남긴다.

		// http://www.itshouldjustworktm.com/?p=376
		// This is of course just a placeholder for a real assert handler.
		//The real thing will display a graphical interface and offer options such as skip or debug, and include additional useful data such as a call stack.
	*/

	Scope* Scope::current = NULL;

	void HandleSkippableAssert(const TCHAR* fileName, const TCHAR* functionName, int lineNumber, const char* expression)
	{
		String msg;
		{
			TCHAR temp[2048];

			msg += TEXT("\n************************************* [ Assert ] *************************************\n");
			if (Scope::current != NULL)
			{
				SPrintf(temp, 2048, TEXT("* This message is for: %s\n"), Scope::current->name);
				msg += temp;
			}

			SPrintf(temp, 2048, TEXT("* Assertion failed in %s, line %d, function: %s\n"), fileName, lineNumber, functionName);
			msg += temp;

			SPrintf(temp, 2048, TEXT("* Expression: %s\n*** Message: "), Convert(expression).c_str());
			msg += temp;

			msg += TEXT("\n************************************* [ Assert ] *************************************\n");
		}
		PlatformSys::DebugOut(msg.c_str());

#ifdef WIN32
		msg += TEXT("\n계속 하시겠습니까?");
		if (PlatformSys::Dialog(msg.c_str(), TEXT("error"), MB_YESNO) == IDNO)
		{
			assert(0);
		}
#endif
	}

	void HandleSkippableAssert(const TCHAR* fileName, const TCHAR* functionName, int lineNumber, const char* expression, const TCHAR* format, ...)
	{
		String msg;
		{
			TCHAR temp[1024];

			va_list ap;
			va_start(ap, format);
			VSPrintf(temp, 1024, format, ap);
			va_end(ap);

			TCHAR temp2[2048];
			msg += TEXT("\n************************************* [ Assert ] *************************************\n");
			if (Scope::current != NULL)
			{
				SPrintf(temp2, 2048, TEXT("* This message is for: %s\n"), Scope::current->name);
				msg += temp2;
			}

			SPrintf(temp2, 2048, TEXT("* Assertion failed in %s, line %d, function: %s\n"), fileName, lineNumber, functionName);
			msg += temp2;

			SPrintf(temp2, 2048, TEXT("* Expression: %s\n*** Message: "), Convert(expression).c_str());
			msg += temp2;

			msg += temp;

			msg += TEXT("\n************************************* [ Assert ] *************************************\n");
		}

		PlatformSys::DebugOut(msg.c_str());

#ifdef WIN32
		msg += TEXT("\n계속 하시겠습니까?");
		if (PlatformSys::Dialog(msg.c_str(), TEXT("error"), MB_YESNO) == IDNO)
		{
			assert(0);
		}
#endif
	}
}