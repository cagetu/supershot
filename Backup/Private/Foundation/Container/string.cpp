#include "foundation.h"

namespace Str
{
	int32 Length(const TCHAR* str)
	{
		return Strlen(str);
	}

	bool IsEqual(const TCHAR* dest, const TCHAR* src)
	{
		CHECK(dest && src, TEXT("dest && src is NULL"));
		return Strcmp(dest, src) == 0 ? true : false;
	}
}

int32 Strlen(const TCHAR* str)
{
#ifdef _UNICODE
	return unicode::strlen(str);
#else
	return utf8::strlen(str);
#endif
}

const TCHAR* Strchr(const TCHAR* str, TCHAR ch) 
{
#ifdef _UNICODE
	return unicode::strchr(str, ch);
#else
	return utf8::strchr(str, ch);
#endif
}

const TCHAR* Strrchr(const TCHAR* str, TCHAR ch)
{
#ifdef _UNICODE
	return unicode::strrchr(str, ch);
#else
	return utf8::strrchr(str, ch);
#endif
}

const TCHAR* Strstr(const TCHAR* str1, const TCHAR* str2)
{
#ifdef _UNICODE
	return unicode::strstr(str1, str2);
#else
	return utf8::strstr(str1, str2);
#endif
}

int32 Strcmp(const TCHAR* str1, const TCHAR* str2)
{
#ifdef _UNICODE
	return unicode::strcmp(str1, str2);
#else
	return utf8::strcmp(str1, str2);
#endif
}

int32 Stricmp(const TCHAR* str1, const TCHAR* str2)
{
#ifdef _UNICODE
	return unicode::stricmp(str1, str2);
#else
	return utf8::stricmp(str1, str2);
#endif
}

void Strcpy(TCHAR* dest, int32 destsize, const TCHAR* src)
{
#ifdef _UNICODE
	return unicode::strcpy_s(dest, destsize, src);
#else
	return utf8::strcpy(dest, destsize, src);
#endif
}

TCHAR* Strncpy(TCHAR* dest, int32 destsize, TCHAR* source, int32 count)
{
#ifdef _UNICODE
	return unicode::strncpy_s(dest, destsize, source, count);
#else
	return utf8::strncpy_s(dest, destsize, source, count);
#endif
}

TCHAR* Strcat(TCHAR* dest, const TCHAR* src)
{
#ifdef _UNICODE
	return unicode::strcat(dest, src);
#else
	return utf8::strcat(dest, src);
#endif
}

TCHAR* Strlwr(TCHAR* str, int32 sizeinwords)
{
#ifdef _UNICODE
	return unicode::strlwr(str, sizeinwords);
#else
	return utf8::strlwr(str, sizeinwords);
#endif
}

String Strlwr(const String& str)
{
#ifdef _UNICODE
	return unicode::lowercase(str);
#else
	return unicode::lowercase(str);
#endif
}

void StrTime(TCHAR* str, int32 sizeinwords)
{
#ifdef _UNICODE
	return unicode::strtime(str, sizeinwords);
#else
	return utf8::strtime(str, sizeinwords);
#endif
}

void StrDate(TCHAR* str, int32 sizeinwords)
{
#ifdef _UNICODE
	return unicode::strdate(str, sizeinwords);
#else
	return utf8::strdate(str, sizeinwords);
#endif
}

void StrcpyPart(TCHAR* dest, TCHAR* src, size_t size)
{
#ifdef _UNICODE
	return unicode::strcpypart(dest, src, size);
#else
	return utf8::strcpypart(dest, src, size);
#endif
}

////------------------------------------------------------------
bool StrToBool(const TCHAR* str)
{
#ifdef _UNICODE
	return unicode::atob(str);
#else
	return utf8::atob(str);
#endif
}

float StrToFloat(const TCHAR* str)
{
#ifdef _UNICODE
	return unicode::atof(str);
#else
	return utf8::atof(str);
#endif
}

long StrToLong(const TCHAR* str)
{
#ifdef _UNICODE
	return unicode::atol(str);
#else
	return utf8::atol(str);
#endif
}

void StrToVec2(const TCHAR* str, const Vec2* vec)
{
#ifdef _UNICODE
	return unicode::atov(str, vec);
#else
	return utf8::atov(str, vec);
#endif
}

void StrToVec3(const TCHAR* str, const Vec3* vec)
{
#ifdef _UNICODE
	return unicode::atov(str, vec);
#else
	return utf8::atov(str, vec);
#endif

}

void StrToVec4(const TCHAR* str, const Vec4* vec)
{
#ifdef _UNICODE
	return unicode::atov(str, vec);
#else
	return utf8::atov(str, vec);
#endif
}

int32 StrToInt(const TCHAR* str)
{
#ifdef _UNICODE
	return unicode::atoi(str);
#else
	return utf8::atoi(str);
#endif
}

void StrToInt2(const TCHAR* str, int* int2)
{
#ifdef _UNICODE
	return unicode::atoi2(str, int2);
#else
	return utf8::atoi2(str, int2);
#endif
}

void StrToInt3(const TCHAR* str, int* int3)
{
#ifdef _UNICODE
	return unicode::atoi3(str, int3);
#else
	return utf8::atoi3(str, int3);
#endif
}

TCHAR* IntToStr(int32 val, TCHAR* buf, int32 radix)
{
#ifdef _UNICODE
	return unicode::itoa(val, buf, radix);
#else
	return utf8::itoa(val, buf, radix);
#endif
}

int32 StrToHex(const TCHAR* buf, int32 len)
{
#ifdef _UNICODE
	return unicode::gethex(buf, len);
#else
	return utf8::gethex(buf, len);
#endif
}

void HexToStr(uint32 val, TCHAR* buf, unsigned radix, int32 negative)
{
#ifdef _UNICODE
	return unicode::xtoa(val, buf, radix, negative);
#else
	return utf8::xtoa(val, buf, radix, negative);
#endif
}

TCHAR* FloatToStr(float val, TCHAR* buf, int32 dp)
{
#ifdef _UNICODE
	return unicode::ftoa(val, buf, dp);
#else
	return utf8::ftoa(val, buf, dp);
#endif
}

TCHAR* IntToStr(int32 value, int32 width, int32 trim)
{
#ifdef _UNICODE
	return unicode::int_str(value, width, trim);
#else
	return utf8::int_str(value, width, trim);
#endif
}

//	format
// %d, %i, %l , %f, %s only!!!
int32 SScanf(const TCHAR* buffer, const TCHAR* format, ...)
{
	int32 result = 0;
	va_list ap;
	va_start(ap, format);
#ifdef _UNICODE
	result = unicode::vsscanf(buffer, format, ap);
#else
	result = utf8::vsscanf(buffer, format, ap);
#endif
	va_end(ap);
	return result;
}

TCHAR* SPrintf(TCHAR* dest, int32 destlen, const TCHAR* format, ...)
{
	TCHAR* result = NULL;
	va_list ap;
	va_start(ap, format);
#ifdef _UNICODE
	result = unicode::vsprintf(dest, destlen, format, ap);
#else
	result = utf8::vsprintf(dest, destlen, format, ap);
#endif
	va_end(ap);
	return result;
}

TCHAR* VSPrintf(TCHAR* dest, int32 destlen, const TCHAR* format, va_list ap)
{
	TCHAR* result = NULL;
#ifdef _UNICODE
	result = unicode::vsprintf(dest, destlen, format, ap);
#else
	result = utf8::vsprintf(dest, destlen, format, ap);
#endif
	return result;
}

String SFormat(const TCHAR* fmt, ...)
{
	String result;

	va_list ap;
	va_start(ap, fmt);
#ifdef _UNICODE
	result = unicode::vformat(fmt, ap);
#else
	result = utf8::vformat(fmt, ap);
#endif
	va_end(ap);
	return result;
}

String VFormat(const TCHAR* fmt, va_list ap)
{
	String result;

#ifdef _UNICODE
	result = unicode::vformat(fmt, ap);
#else
	result = utf8::vformat(fmt, ap);
#endif
	return result;
}

//	FilePath
String SplitFilePath(const TCHAR* path)
{
#ifdef _UNICODE
	return unicode::splitFilePath(path);
#else
	return utf8::splitFilePath(path);
#endif
}

String SplitFileName(const TCHAR* path)
{
#ifdef _UNICODE
	return unicode::splitFileName(path);
#else
	return utf8::splitFileName(path);
#endif
}

String SplitFileExt(const TCHAR* path)
{
#ifdef _UNICODE
	return unicode::splitFileExt(path);
#else
	return utf8::splitFileExt(path);
#endif
}

String SplitFileNameExt(const TCHAR* path)
{
#ifdef _UNICODE
	return unicode::splitFileNameExt(path);
#else
	return utf8::splitFileNameExt(path);
#endif
}

void SplitPath(TCHAR* path, TCHAR* drive, TCHAR* dir, TCHAR* fname, TCHAR* ext)
{
#ifdef _UNICODE
	return unicode::splitPath(path, drive, dir, fname, ext);
#else
	return utf8::splitPath(path, drive, dir, fname, ext);
#endif
}

SizeT Tokenize(Array<String>& output, const String& input, const String& whitespace)
{
#ifdef _UNICODE
	return unicode::tokenize(output, input, whitespace);
#else
	return utf8::tokenize(output, input, whitespace);
#endif
}

String& Trim(String& str, const String whitespace)
{
	return str.trim(whitespace);
}

String Convert(const char* str)
{
#ifdef _UNICODE
	return unicode::convert(str);
#else
	return String(str);
#endif
}

int32 ConvertToUTF8(char* dest, int32 destsize, const TCHAR* src)
{
#ifdef _UNICODE
	return unicode::convert(dest, destsize, src);
#else
	utf8::strcpy(dest, destsize, src);
	return utf8::strlen(dest);
#endif
}

int32 ConvertToWide(wchar* dest, int32 destsize, const TCHAR* src)
{
#ifdef _UNICODE
	unicode::strcpy_s(dest, destsize, src);
	return unicode::strlen(dest);
#else
	return unicode::convert(dest, destsize, src);
#endif
}
