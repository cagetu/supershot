#include "foundation.h"
#include "ConvertUTF.h"

#ifdef TARGET_OS_IOS
char g_ResourcePath[2048] = {0, };
char g_DataPath[2048] = {0, };
#endif

#ifndef _TRUNCATE
#define _TRUNCATE ((size_t)-1)
#endif

#define _CONVERT_UTF 1

uint32 locale_lower(uint32 x)
{
#pragma message("이건 다시 확인해봐야 한다.... ansi????")
	// ansi
	return x >= 'A' && x <= 'Z' ? x + 0x20 : x;
}

uint32 locale_upper(uint32 x)
{
	// ansi
	return x >= 'a' && x <= 'z' ? x + ('A' - 'a') : x;
}

namespace utf8
{
	//--------------------------------------------------------------------------------------------------------
	//	utf8
	//--------------------------------------------------------------------------------------------------------
	int32 gethex(const char* buf, int32 len)
	{
		uint32 hex = 0;
		for (int32 i = 0; i < len; i++)
		{
			hex *= 16;

			if (buf[i] >= '0' && buf[i] <= '9')
				hex += buf[i] - '0';
			else if (buf[i] == 'x' || buf[i] == 'X')	// bit style string, x character is skipped...
				continue;
			else if (buf[i] == 'a' || buf[i] == 'A')
				hex += 10;
			else if (buf[i] == 'b' || buf[i] == 'B')
				hex += 11;
			else if (buf[i] == 'c' || buf[i] == 'C')
				hex += 12;
			else if (buf[i] == 'd' || buf[i] == 'D')
				hex += 13;
			else if (buf[i] == 'e' || buf[i] == 'E')
				hex += 14;
			else if (buf[i] == 'f' || buf[i] == 'F')
				hex += 15;
			else
				return -1;
		}
		return hex;
	}

	void xtoa(uint32 val, char* buf, unsigned radix, int32 negative)
	{
		char* p;
		char* firstdig;
		char temp;
		unsigned digval;
		p = buf;

		if (negative)
		{
			*p++ = '-';
			val = (uint32)(-(int32)val);
		}

		firstdig = p;
		do
		{
			digval = (unsigned)(val % radix);
			val /= radix;

			if (digval > 9)
				*p++ = (char)(digval - 10 + 'a');
			else
				*p++ = (char)(digval + '0');
		} while (val > 0);

		*p-- = '\0';

		do
		{
			temp = *p;
			*p = *firstdig;
			*firstdig = temp;
			p--;
			firstdig++;
		} while (firstdig < p);
	}

	char* int_str(int32 value, int32 width, int32 trim)
	{
		static char asc_string[21];
		int32 digit, save_value, i, j;

		if (width > 10)
			width = 10;

		save_value = value;
		value = Math::Abs(value);

		for (i = 1; i <= width; i++)
		{
			digit = value % 10;
			asc_string[width - i] = digit + '0';
			value = value / 10;
		}

		asc_string[width] = 0;

		if (trim == 1)
		{
			for (i = 0; i < (width - 1); i++)
				if (asc_string[i] != '0') break;

			if (save_value < 0)
			{
				i--;
				asc_string[i] = '-';
			}

			for (j = 0; j <= (width - i); j++)
				asc_string[j] = asc_string[j + i];
		}

		return asc_string;
	}

	char* ftoa(float val, char* buf, int32 dp)
	{
		if (val < 0)
		{
			buf[0] = '-';
			ftoa(-val, &buf[1], dp);
			return buf;
		}

		int32 dpVal = 1;
		int32 dp1, intPart, fracPart;
		if (dp > 8) dp = 8;

		dp1 = dp;
		intPart = (int32 )val;

		while (dp1 > 0)
		{
			dpVal = dpVal * 10;
			dp1--;
		}

		int32 bufsize = strlen(buf);

		fracPart = (int32 )fabsf((val - intPart) *dpVal + 0.5f);
		utf8::strcpy(buf, bufsize, int_str(intPart, 10, 1));
		utf8::strcat(buf, bufsize, ".");
		utf8::strcat(buf, bufsize, int_str(fracPart, dp, 0));
		return buf;
	}

	string lowercase(const string& str)
	{
		char temp[1024];
		utf8::strcpy(temp, _countof(temp), str.c_str());
		utf8::strlwr(temp, _countof(temp));
		return string(temp);
	}
	void strcpypart(char* dest, char* src, size_t size)
	{
		memcpy(dest, src, size * sizeof(char));
		dest[size] = 0;
	}

	int32 sscanf(const char* buffer, const char* format, ...)
	{
		int32 _Result;
		va_list _ArgList;
		va_start(_ArgList, format);

#pragma warning(push)
#pragma warning(disable: 4996) // Deprecation
		_Result = ::vsscanf_s(buffer, format, _ArgList);
#pragma warning(pop)

		va_end(_ArgList);
		return _Result;
	}

	int32 vsscanf(const char* buffer, const char* format, va_list ap)
	{
		int32 _Result;
#pragma warning(push)
#pragma warning(disable: 4996) // Deprecation
		_Result = ::vsscanf_s(buffer, format, ap);
#pragma warning(pop)
		return _Result;
	}

	bool atob(const char* str)
	{
		static const char* bools[] = {
			"no", "yes", "off", "on", "false", "true", 0
		};

		IndexT i = 0;
		while (bools[i])
		{
			if (!utf8::strcmp(bools[i], str))
			{
				return (i & 1) == 1;
			}
			i++;
		}
		return false;
	}

	void atov(const char* str, const Vec2* vec)
	{
		utf8::sscanf(str, "%f %f", &vec->x, &vec->y);
	}

	void atov(const char* str, const Vec3* vec)
	{
		utf8::sscanf(str, "%f %f %f", &vec->x, &vec->y, &vec->z);
	}
	void atov(const char* str, const Vec4* vec)
	{
		utf8::sscanf(str, "%f %f %f %f", &vec->x, &vec->y, &vec->z, &vec->w);
	}
	void atoi2(const char* str, int32 * int2)
	{
		utf8::sscanf(str, "%d %d", &int2[0], &int2[1]);
	}
	void atoi3(const char* str, int32 * int3)
	{
		utf8::sscanf(str, "%d %d %d", &int3[0], &int3[1], &int3[2]);
	}

	string format(const char* fmt, ...)
	{
		char temp[1024] = { 0, };
		char* p = temp;

		va_list ap;
		va_start(ap, fmt);
		p = utf8::vsprintf(temp, _countof(temp), fmt, ap);
		int32 l = utf8::strlen(p);
		va_end(ap);

		string str(p, l);
		if (p != temp)
		{
			PlatformSys::DebugOut(TEXT("[Warning] Change System!!!"));
			//delete[] p;
		}
		return str;
	}

	string vformat(const char* fmt, va_list ap)
	{
		char temp[1024] = { 0, };
		char* p = temp;

		int32 l = utf8::strlen(p);

		string str(p, l);
		if (p != temp)
		{
			PlatformSys::DebugOut(TEXT("[Warning] Change System!!!"));
		}
		return str;
	}

	string splitFilePath(const char* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('/');
		if (t == -1)
		{
			t = filepath.findLast('\\');
			if (t == -1)
				return string("");
		}
		return filepath.substr(0, t);
	}

	string splitFileName(const char* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('/');
		if (t == -1)
		{
			t = filepath.findLast('\\');
			if (t != -1)
				filepath = filepath.substr(t + 1, -1);
		}
		else
		{
			filepath = filepath.substr(t + 1, -1);
		}

		t = filepath.findLast('.');
		if (t == -1)
			return string("");

		return filepath.substr(0, t);
	}

	string splitFileExt(const char* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('.');
		if (t == -1)
			return string("");

		return filepath.substr(t, -1);
	}

	string splitFileNameExt(const char* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('/');
		if (t == -1)
		{
			t = filepath.findLast('\\');
			if (t == -1)
				return string("");
		}
		return filepath.substr(t + 1, -1);
	}

	void splitPath(char* path, char* drive, char* dir, char* fname, char* ext)
	{
		if (drive)
			utf8::strcpy(drive, utf8::strlen(drive), "");

		char* lastslash = 0;
		for (char* p = path; *p != '\0'; p++)
		{
			if (*p == '/')
				lastslash = p;
		}

		if (dir)
		{
			if (lastslash == 0)
				utf8::strcpy(dir, utf8::strlen(dir), "");
			else
				utf8::strcpypart(dir, path, lastslash - path + 1);
		}

		char* lastdot = 0;
		char* begin = (lastslash != 0) ? lastslash + 1 : path;
		for (char* p = begin; *p != '\0'; p++)
		{
			if (*p == '.')
				lastdot = p;
		}

		if (lastdot == 0)
		{
			if (fname)
				utf8::strcpy(fname, utf8::strlen(fname), begin);

			if (ext)
				utf8::strcpy(ext, utf8::strlen(ext), "");
		}
		else
		{
			if (fname)
				utf8::strcpypart(fname, begin, lastdot - begin);

			if (ext)
				utf8::strcpy(ext, utf8::strlen(ext), lastdot);
		}
	}

	SizeT tokenize(Array<string>& output, const string& input, const string& whitespace)
	{
		output.Clear();

		char* ptr = const_cast<char*>(input.c_str());

		char* token = NULL;
		char* context = NULL;
		while (0 != (token = ::strtok_s(ptr, whitespace.c_str(), &context)))
		{
			output.Add(token);
			ptr = 0;
		}
		return output.Size();
	}
}

namespace	unicode
{
#if _CONVERT_UTF
	// utf-8 --> unicode (utf-16 / utf-32)
	int32 convert(wchar* dest, int32 destSize, const char* src, bool utf8)
	{
		ASSERT((0 != src) && (0 != dest));
		int32 result = 0;
		const UTF8* srcPtr = (UTF8*)src;
		int32 srcNumBytes = (int32 )::strlen(src);

		// need to keep 1 wchar_t for the terminating 0
		ConversionResult convRes = conversionOK;
		wchar_t* dstEndPtr = dest + ((destSize / sizeof(wchar_t)) - 1);
		ASSERT(dstEndPtr > dest);
		if (sizeof(wchar_t) == 4) {
			UTF32* dstPtr = (UTF32*)dest;
			convRes = ConvertUTF8toUTF32(&srcPtr, (UTF8*)src + srcNumBytes, &dstPtr, (UTF32*)dstEndPtr, strictConversion);
			ASSERT(dstPtr < (UTF32*)(dest + (destSize / sizeof(wchar_t))));
			*dstPtr = 0;
			if (conversionOK == convRes) {
				result = int32 (dstPtr - (UTF32*)dest) + 1;
			}
		}
		else {
			ASSERT(2 == sizeof(wchar_t));
			UTF16* dstPtr = (UTF16*)dest;
			convRes = ConvertUTF8toUTF16(&srcPtr, (UTF8*)src + srcNumBytes, &dstPtr, (UTF16*)dstEndPtr, strictConversion);
			ASSERT(dstPtr < (UTF16*)(dest + (destSize/ sizeof(wchar_t))));
			*dstPtr = 0;
			if (conversionOK == convRes) {
				result = int32 (dstPtr - (UTF16*)dest) + 1;
			}
		}
		if (conversionOK != convRes) 
		{
			const TCHAR* err = TEXT("");
			if (sourceExhausted == convRes) err = TEXT("sourceExhausted");
			else if (targetExhausted == convRes) err = TEXT("targetExhausted");
			else if (sourceIllegal == convRes) err = TEXT("sourceIllegal");
			DebugOutMsg(TEXT("StringConverter: conversion failed with '%s'\n"), err);
			return 0;
		}
		return result;
	}

	// unicode (utf-16 / utf-32) --> utf-8
	int32 convert(char* dest, int32 destSize, const wchar* src, bool utf8)
	{
		ASSERT((0 != src) && (0 != dest));
		int32 result = 0;
		UTF8* dstPtr = (UTF8*)dest;
		UTF8* dst = (UTF8*)dest;
		UTF8* dstEnd = (dst + destSize) - 1;
		int32 srcNumChars = (int32 )wcslen(src);

		// need to keep 1 char free for 0-termination
		ConversionResult convRes = conversionOK;
		CHECK(dstEnd > dst, TEXT("unicode::convert"));
		
		if (sizeof(wchar_t) == 4) {
			ASSERT(4 == sizeof(wchar_t));
			const UTF32* srcPtr = (const UTF32*)src;
			const UTF32* srcEndPtr = (UTF32*)src + srcNumChars;
			convRes = ConvertUTF32toUTF8(&srcPtr, srcEndPtr, &dstPtr, dstEnd, strictConversion);
			ASSERT(dstPtr < (dst + destSize));
			*dstPtr = 0;
			if (conversionOK == convRes) {
				result = int32 (dstPtr - dst) + 1;
			}
		}
		else {
			ASSERT(2 == sizeof(wchar_t));
			const UTF16* srcPtr = (UTF16*)src;
			const UTF16* srcEndPtr = (UTF16*)src + srcNumChars;
			convRes = ConvertUTF16toUTF8(&srcPtr, srcEndPtr, &dstPtr, dstEnd, strictConversion);
			ASSERT(dstPtr < (dst + destSize));
			*dstPtr = 0;
			if (conversionOK == convRes) {
				result = int32 (dstPtr - dst) + 1;
			}
		}
		if (conversionOK != convRes) 
		{
			const TCHAR* err = TEXT("");
			if (sourceExhausted == convRes) err = TEXT("sourceExhausted");
			else if (targetExhausted == convRes) err = TEXT("targetExhausted");
			else if (sourceIllegal == convRes) err = TEXT("sourceIllegal");
			DebugOutMsg(TEXT("StringConverter: conversion failed with '%s'\n"), err);
			return 0;
		}
		return result;
	}

	string convert(const char * str)
	{
		// char -> wchar
		wchar t[1024] = { 0, };
		convert(t, 1024, str);
		return string(t);
	}

	string convert(const utf8::string& str)
	{
		wchar t[1024] = { 0, };
		convert(t, 1024, str.c_str());
		return string(t);
	}

	utf8::string convert(const wchar * str)
	{
		char t[1024] = { 0 };
		convert(t, 1024, str);
		return utf8::string(t);
	}

	utf8::string convert(const string& str)
	{
		char t[1024] = { 0 };
		convert(t, 1024, str.c_str());
		return utf8::string(t);
	}

#else
	int32 utf16toutf8(char* dest, int32 destsize, const wchar* src)
	{
		int32 size = 0;
		const wchar* tmp = src;
		while(*tmp)
		{
			if     (*tmp <= 0x007f) size += 1;
			else if(*tmp <= 0x07ff) size += 2;
			else if(*tmp <= 0xffff) size += 3;
			++ tmp;
		}
		_ASSERT(size <= destsize);

		tmp = src;
		char* utf8_tmp = &dest[0];
		while(*tmp)
		{
			if     (*tmp <= 0x007f) {*(utf8_tmp ++) = (char)  *tmp;}
			else if(*tmp <= 0x07ff) {*(utf8_tmp ++) = (char)((*tmp&0x07C0) >>  6) | 0xc0; *(utf8_tmp ++) = (char) (*tmp&0x003f)       | 0x80;}
			else                    {*(utf8_tmp ++) = (char)((*tmp&0xf000) >> 12) | 0xe0; *(utf8_tmp ++) = (char)((*tmp&0x0fc0) >> 6) | 0x80; *(utf8_tmp ++) = (char)(*tmp&0x003f) | 0x80;}
			++ tmp;
		}
		return size;
	}

	int32 utf8toutf16(wchar* dest, int32 destsize, const char* src)
	{
		int32 size = 0;
		const char* utf8_tmp = src;
		while(*utf8_tmp)
		{
			if     ((*utf8_tmp & 0x80) == 0x00) utf8_tmp += 1;
			else if((*utf8_tmp & 0xe0) == 0xc0) utf8_tmp += 2;
			else if((*utf8_tmp & 0xf0) == 0xe0) utf8_tmp += 3;
			else
			{
				utf8_tmp ++;
				while((*utf8_tmp & 0xc0) == 0x80) ++ utf8_tmp;
			}
			++ size;
		}
		_ASSERT(size <= destsize);

		utf8_tmp = src;
		wchar* unicode_tmp = &dest[0];
		while(*utf8_tmp)
		{
			if     ((*utf8_tmp & 0x80) == 0x00) {*unicode_tmp = (wchar) *(utf8_tmp ++);}
			else if((*utf8_tmp & 0xe0) == 0xc0) {*unicode_tmp = (wchar)(*(utf8_tmp ++)&0x1f) <<  6; *unicode_tmp |= (wchar) *(utf8_tmp ++)&0x3f;}
			else if((*utf8_tmp & 0xf0) == 0xe0) {*unicode_tmp = (wchar)(*(utf8_tmp ++)&0x0f) << 12; *unicode_tmp |= (wchar)(*(utf8_tmp ++)&0x3f) << 6; *unicode_tmp |= (wchar)*(utf8_tmp ++)&0x3f;}
			else
			{
				*unicode_tmp = '?';
				utf8_tmp ++;
				while((*utf8_tmp & 0xc0) == 0x80) ++ utf8_tmp;
			}
			++ unicode_tmp;
		}
		return size;
	}

	// utf-8 --> unicode (utf-16)
	int32 convert(wchar* dest, int32 destsize, const char *src, bool utf8)
	{
#ifdef TARGET_OS_WINDOWS
		return MultiByteToWideChar(utf8 == true ? CP_UTF8 : CP_ACP, 0, src, (int32 )::strlen(src) + 1, dest, destsize == _TRUNCATE ? (int32 )::strlen(src) + 1 : destsize);
#else
		//memset(dest, 0, sizeof(wchar) * destsize);
		int32 len = utf8toutf16(dest, destsize, src);
		dest[len] = 0;
		return len;
#endif
	}

	// unicode (utf-16) --> utf-8
	int32 convert(char * dest, int32 destsize, const wchar*src, bool utf8)
	{
#ifdef TARGET_OS_WINDOWS
		return WideCharToMultiByte(utf8 == true ? CP_UTF8 : CP_ACP, 0, src, (int32 )wcslen(src) + 1, dest, destsize == _TRUNCATE ? (int32 )wcslen(src) * 2 + 1 : destsize, NULL, NULL);
#else
		//memset(dest, 0, sizeof(char) * destsize);
		int32 len = utf16toutf8(dest, destsize, src);
		dest[len] = 0;
		return len;
#endif
	}

	string convert(const char * str)
	{
		// char -> wchar
		wchar t[1024] = { 0, };
		utf8toutf16(t, 1024, str);
		return string(t);
	}

	string convert(const utf8::string& str)
	{
		wchar t[1024] = { 0, };
		utf8toutf16(t, 1024, str.c_str());
		return string(t);
	}

#endif

	const wchar* __wcschr(const wchar* input, wchar c)
	{
		for (int32 i = 0; input[i]; i++)
			if (input[i] == c)
				return &input[i];
		return NULL;
	}

	const wchar* __wcsrchr(const wchar*input, wchar c)
	{
		const wchar* retval = NULL;
		for (int32 i = 0; input[i]; i++)
			if (input[i] == c)
				retval = &input[i];
		return retval;
	}

	int32 __wcscmp(const wchar* str1, const wchar* str2)
	{
		int32 ret = 0;
		while (!(ret = *str1 - *str2) && *str2)
			++str1, ++str2;

		if (ret < 0)
			ret = -1;
		else if (ret > 0)
			ret = 1;
		return ret;
	}

	int32 __wcsicmp(const wchar* str1, const wchar* str2)
	{
		int32 ret = 0;
		while (!(ret = locale_lower(*str1) - locale_lower(*str2)) && *str2)
			++str1, ++str2;

		if (ret < 0)
			ret = -1;
		else if (ret > 0)
			ret = 1;
		return ret;
	}

	int32 __wcslen(const wchar* str)
	{
		int32 l = 0;
		const wchar* pstr = str;
		while (*pstr++)
		{
			l++;
		}
		return l;
	}

	wchar*__wcsstr(const wchar* str1, const wchar* str2)
	{
		wchar*cp = (wchar*)str1;
		wchar*s1, *s2;

		if (!*str2) return (wchar*)str1;

		while (*cp)
		{
			s1 = cp;
			s2 = (wchar*)str2;

			while (*s1 && *s2 && !(*s1 - *s2)) s1++, s2++;
			if (!*s2) return cp;
			cp++;
		}
		return NULL;
	}

	//--------------------------------------------------------------------------------------------------------
	//	widechar
	//--------------------------------------------------------------------------------------------------------
	int32 strlen(const wchar* str) { return __wcslen(str); }
	const wchar* strchr(const wchar* str, wchar ch) { return __wcschr(str, ch); }
	const wchar* strrchr(const wchar* str, wchar ch) { return __wcsrchr(str, ch); }
	const wchar* strstr(const wchar* str1, const wchar* str2) { return __wcsstr(str1, str2); }
	int32 strcmp(const wchar* str1, const wchar* str2) { return __wcscmp(str1, str2); }
	int32 stricmp(const wchar* str1, const wchar* str2) { return __wcsicmp(str1, str2); }

	void strcpy(wchar* dest, const wchar* src)
	{
		wchar*cp = dest;
		while (*src)
		{
			*cp++ = *src++;
		}
		*cp = '\0';
	}

	void strcpy_s(wchar* dest, int32 destsize, const wchar* src)
	{
		int32 i = 0;
		wchar*cp = dest;
		while (destsize > i && *src)
		{
			*cp++ = *src++;
			i++;
		}
		*cp = '\0';
	}

	wchar* strncpy(wchar* dest, wchar* source, int32 count)
	{
		wchar* start = dest;
		while (count && (*dest++ = *source++))
			count--;

		if (count)
			while (--count)
				*dest++ = L'\0';
		return (start);
	}

	wchar* strncpy_s(wchar* dest, int32 destsize, wchar* source, int32 count)
	{
		return unicode::strncpy(dest, source, count);
	}

	void strcpypart(wchar* dest, wchar* src, size_t size)
	{
		memcpy(dest, src, size * sizeof(wchar));
		dest[size] = 0;
	}

	wchar* strcat(wchar* dst, const wchar* src)
	{
		wchar*cp = dst;
		while (*cp) cp++;
		while (*cp++ = *src++);
		*cp = '\0';

		return dst;
	}

	wchar* strcat_s(wchar* dst, int32 destsize, const wchar*src) 
	{
		return unicode::strcat(dst, src);
	}

	wchar* strlwr(wchar* str, int32 sizeinwords)
	{
		unicode::string s(str);
		s.to_lower();
		//memcpy(str, s.c_str(), sizeof(wchar)*sizeinwords);
		memcpy(str, s.c_str(), sizeof(wchar)*s.size());
		return str;
	}

	string lowercase(const string& str)
	{
		wchar temp[1024];
		unicode::strcpy_s(temp, _countof(temp), str.c_str());
		unicode::strlwr(temp, _countof(temp));
		return string(temp);
	}

	void atov(const wchar* str, const Vec2* vec)
	{
		unicode::sscanf(str, L"%f %f", &vec->x, &vec->y);
	}
	void atov(const wchar* str, const Vec3* vec)
	{
		unicode::sscanf(str, L"%f %f %f", &vec->x, &vec->y, &vec->z);
	}
	void atov(const wchar* str, const Vec4* vec)
	{
		unicode::sscanf(str, L"%f %f %f %f", &vec->x, &vec->y, &vec->z, &vec->w);
	}
	void atoi2(const wchar* str, int32 * int2)
	{
		unicode::sscanf(str, L"%d %d", &int2[0], &int2[1]);
	}
	void atoi3(const wchar* str, int32 * int3)
	{
		unicode::sscanf(str, L"%d %d %d", &int3[0], &int3[1], &int3[2]);
	}

	int32 gethex(const wchar*buf, int32 len)
	{
		uint32 hex = 0;
		for (int32 i = 0; i < len; i++)
		{
			hex *= 16;

			if (buf[i] >= L'0' && buf[i] <= L'9')
				hex += buf[i] - L'0';
			else if (buf[i] == L'x' || buf[i] == L'X')	// bit style string, x character is skipped...
				continue;
			else if (buf[i] == L'a' || buf[i] == L'A')
				hex += 10;
			else if (buf[i] == L'b' || buf[i] == L'B')
				hex += 11;
			else if (buf[i] == L'c' || buf[i] == L'C')
				hex += 12;
			else if (buf[i] == L'd' || buf[i] == L'D')
				hex += 13;
			else if (buf[i] == L'e' || buf[i] == L'E')
				hex += 14;
			else if (buf[i] == L'f' || buf[i] == L'F')
				hex += 15;
			else
				return -1;
		}
		return hex;
	}
	
	int32 atoi(const wchar* str)
	{
		int32 i;
		for (i = 0; str[i] && str[i] <= ' '; i++);
		if (str[i] == L'-')
			return -unicode::atoi(&str[i + 1]);

		int32 val = 0;
		for (; str[i] >= L'0' && str[i] <= L'9'; i++)
			val = 10 * val + (str[i] - L'0');
		return val;
	}

	float atof(const wchar* str)
	{
		int32 i;
		for (i = 0; str[i] && str[i] <= ' '; i++);
		if (str[i] == L'-')
			return -unicode::atof(&str[i + 1]);

		float val = 0, fact = 1;
		int32 point_seen = 0;
		for (; (str[i] >= L'0' && str[i] <= L'9') || (str[i] == '.'); i++)
		{
			if (str[i] == '.')
				point_seen = 1;
			else
			{
				val = 10.0f * val + (str[i] - L'0');
				fact *= point_seen == 0 ? 1.0f : 10.0f;
			}
		}
		return val / fact;
	}

	long atol(const wchar* str)
	{
		return (int32)unicode::atoi(str);
	}

	bool atob(const wchar* str)
	{
		static const wchar* bools[] = {
			L"no", L"yes", L"off", L"on", L"false", L"true", 0
		};

		IndexT i = 0;
		while (bools[i])
		{
			if (!unicode::strcmp(bools[i], str))
			{
				return (i & 1) == 1;
			}
			i++;
		}
		return false;
	}

	void xtoa(uint32 val, wchar* buf, unsigned radix, int32 negative)
	{
		wchar* p;
		wchar* firstdig;
		wchar temp;
		unsigned digval;
		p = buf;

		if (negative)
		{
			*p++ = L'-';
			val = (uint32)(-(int32)val);
		}

		firstdig = p;
		do
		{
			digval = (unsigned)(val % radix);
			val /= radix;

			if (digval > 9)
				*p++ = (wchar)(digval - 10 + L'a');
			else
				*p++ = (wchar)(digval + L'0');
		} while (val > 0);

		*p-- = L'\0';

		do
		{
			temp = *p;
			*p = *firstdig;
			*firstdig = temp;
			p--;
			firstdig++;
		} while (firstdig < p);
	}

	wchar* itoa(int32 val, wchar* buf, int32 radix)
	{
		if (radix == 10 && val < 0)
			unicode::xtoa((uint32)val, buf, radix, 1);
		else
			unicode::xtoa((uint32)(uint32)val, buf, radix, 0);

		return buf;
	}

	wchar* itoa_s(int32 val, wchar* buf, int32 bufsize, int32 radix)
	{
		return unicode::itoa(val, buf, radix);
	}

	int32 sscanf(const wchar* buffer, const wchar* format, ...)
	{
		va_list ap;
		float *f;
		uint32 *l;
		int32 conv = 0, *i, index;
		wchar*a, *fp, *sp = (wchar*)buffer, buf[512] = { '\0' };
		va_start(ap, format);

		for (fp = (wchar*)format; *fp != L'\0' && *sp != L'\0'; fp++)
		{
			memset(buf, 0, sizeof(buf));

			for (index = 0; *sp != L'\0' && *sp != L' '; index++)
				buf[index] = *sp++;

			while (*sp == L' ') sp++;
			while (*fp != L'%') fp++;

			if (*fp == L'%')
			{
				switch (*++fp)
				{
				case L'D':
				case L'd':
				case L'i':
				case L'l':
					i = va_arg(ap, int32 *);
					*i = unicode::atoi(buf);
					break;

				case L'F':
				case L'f':
					f = va_arg(ap, float *);
					*f = (float)unicode::atof(buf);
					break;
				case L'S':
				case L's':
					a = va_arg(ap, wchar*);
					strncpy(a, buf, __wcslen(buf) + 1);
					break;
				case L'X':
				case L'x':
				{
					l = va_arg(ap, uint32 *);
					*l = gethex(buf, __wcslen(buf));
				}
				break;
				}
				conv++;
			}
		}

		va_end(ap);
		return conv;
	}

	int32 vsscanf(const wchar* buffer, const wchar* format, va_list ap)
	{
		float *f;
		uint32 *l;
		int32 conv = 0, *i, index;
		wchar*a, *fp, *sp = (wchar*)buffer, buf[512] = { '\0' };

		for (fp = (wchar*)format; *fp != L'\0' && *sp != L'\0'; fp++)
		{
			memset(buf, 0, sizeof(buf));

			for (index = 0; *sp != L'\0' && *sp != L' '; index++)
				buf[index] = *sp++;

			while (*sp == L' ') sp++;
			while (*fp != L'%') fp++;

			if (*fp == L'%')
			{
				switch (*++fp)
				{
				case L'D':
				case L'd':
				case L'i':
				case L'l':
					i = va_arg(ap, int32 *);
					*i = unicode::atoi(buf);
					break;

				case L'F':
				case L'f':
					f = va_arg(ap, float *);
					*f = (float)unicode::atof(buf);
					break;
				case L'S':
				case L's':
					a = va_arg(ap, wchar*);
					strncpy(a, buf, __wcslen(buf) + 1);
					break;
				case L'X':
				case L'x':
				{
					l = va_arg(ap, uint32 *);
					*l = gethex(buf, __wcslen(buf));
				}
				break;
				}
				conv++;
			}
		}

		return conv;
	}

	wchar* int_str(int32 value, int32 width, int32 trim)
	{
		static wchar asc_string[21];
		int32 digit, save_value, i, j;

		if (width > 10)
			width = 10;

		save_value = value;
		value = abs(value);

		for (i = 1; i <= width; i++)
		{
			digit = value % 10;
			asc_string[width - i] = digit + L'0';
			value = value / 10;
		}

		asc_string[width] = 0;

		if (trim == 1)
		{
			for (i = 0; i < (width - 1); i++)
				if (asc_string[i] != L'0') break;

			if (save_value < 0)
			{
				i--;
				asc_string[i] = L'-';
			}

			for (j = 0; j <= (width - i); j++)
				asc_string[j] = asc_string[j + i];
		}
		return asc_string;
	}

	wchar* ftoa(float val, wchar* buf, int32 dp)
	{
		if (val < 0)
		{
			buf[0] = '-';
			ftoa(-val, &buf[1], dp);
			return buf;
		}

		int32 dpVal = 1;
		int32 dp1, intPart, fracPart;
		if (dp > 8) dp = 8;

		dp1 = dp;
		intPart = (int32 )val;

		while (dp1 > 0)
		{
			dpVal = dpVal * 10;
			dp1--;
		}

		fracPart = (int32 )fabsf((val - intPart) *dpVal + 0.5f);
		unicode::strcpy(buf, int_str(intPart, 10, 1));
		unicode::strcat(buf, L".");
		unicode::strcat(buf, int_str(fracPart, dp, 0));
		return buf;
	}

	wchar* sprintf(wchar*dest, int32 destlen, const wchar* format, ...)
	{
		double f;
		wchar*a;
		wchar c;
		uint32 l;

		int32 index = 0;
		wchar*fp;
		int32 width = 0;
		int32 widthset = 0;
		bool done = false;
		wchar* origin = dest;

		va_list ap;
		va_start(ap, format);

		for (fp = (wchar*)format; *fp != L'\0'; fp++)
		{
			wchar temp[64] = { 0, };
			wchar* p = temp;

			if (*fp == L'%')
			{
				while (!done)
				{
					wchar t = *++fp;
					if (isdigit(t))
					{
						++widthset;
						width = width * 10 + (t - '0');
					}
					else
					{
						switch (t)
						{
						case L'F':
						case L'f':
						{
							f = va_arg(ap, double);
							ftoa((float)f, p, width > 0 ? width : 5);
							int32 len = __wcslen(p);
							if (len > 0)
							{
								width = widthset = 0;
								done = true;
							}
						}
						break;
#ifdef TARGET_OS_WINDOWS
						case L'C':
						case L'c':
						{
							c = va_arg(ap, wchar);
							if (a)
							{
								*p = c;

								width = widthset = 0;
								done = true;
							}
						}
						break;
#endif
						case L'S':
						case L's':
						{
							a = va_arg(ap, wchar*);
							if (a)
							{
								p = a;

								width = widthset = 0;
								done = true;
							}
						}
						break;

						case L'd':
						case L'D':
						case L'i':
						case L'I':
						case L'l':
						case L'L':
						case L'x':
						case L'X':
						{
							int32 radix = 10;
							if (t == 'X' || t == 'x')
								radix = 16;

							l = va_arg(ap, uint32);
							itoa(l, p, radix);
							int32 len = __wcslen(p);
							if (len > 0)
							{
								int32 i;

								if (len < width)
								{
									for (i = 0; i < len; i++)
										p[(width - 1) - i] = p[(len - i) - 1];
									for (i = len; i < width; i++)
										p[i - len] = '0';
									p[i] = '\0';
								}

								width = widthset = 0;
								done = true;
							}
						}
						break;
						}
					}
				}
			}
			else
			{
				p[0] = *fp;
				p[1] = '\0';
			}

			int32 len = __wcslen(p);
			if (len > 0)
			{
				if (index + len >= destlen)
				{
					CHECK(0, TEXT("index + len >= destlen"));

					//wchar* pp = new wchar[destlen * 2];
					//memcpy(pp, dest, index * sizeof(wchar));

					//if (dest != origin)
					//	delete[] dest;

					//dest = pp;
					//destlen = destlen * 2;
				}

				memcpy(&dest[index], p, len * sizeof(wchar));
				index += len;
			}

			done = false;
		}

		va_end(ap);

		dest[index] = 0;
		return dest;
	}

	wchar* vsprintf(wchar* dest, int32 destlen, const wchar* format, va_list ap)
	{
		double f;
		wchar*a;
		wchar c;
		uint32 l;

		int32 index = 0;
		wchar*fp;
		int32 width = 0;
		int32 widthset = 0;
		bool done = false;
		wchar* origin = dest;

		for (fp = (wchar*)format; *fp != L'\0'; fp++)
		{
			wchar temp[64] = { 0, };
			wchar* p = temp;

			if (*fp == L'%')
			{
				while (!done)
				{
					wchar t = *++fp;
					if (isdigit(t))
					{
						++widthset;
						width = width * 10 + (t - '0');
					}
					else
					{
						switch (t)
						{
						case L'F':
						case L'f':
						{
							f = va_arg(ap, double);
							ftoa((float)f, p, width > 0 ? width : 5);
							int32 len = __wcslen(p);
							if (len > 0)
							{
								width = widthset = 0;
								done = true;
							}
						}
						break;
#ifdef TARGET_OS_WINDOWS
						case L'C':
						case L'c':
						{
							c = va_arg(ap, wchar);
							if (a)
							{
								*p = c;

								width = widthset = 0;
								done = true;
							}
						}
						break;
#endif
						case L'S':
						case L's':
						{
							a = va_arg(ap, wchar*);
							if (a)
							{
								p = a;

								width = widthset = 0;
								done = true;
							}
						}
						break;

						case L'd':
						case L'D':
						case L'i':
						case L'I':
						case L'l':
						case L'L':
						case L'x':
						case L'X':
						{
							int32 radix = 10;
							if (t == 'X' || t == 'x')
								radix = 16;

							l = va_arg(ap, uint32);
							itoa(l, p, radix);
							int32 len = __wcslen(p);
							if (len > 0)
							{
								int32 i;

								if (len < width)
								{
									for (i = 0; i < len; i++)
										p[(width - 1) - i] = p[(len - i) - 1];
									for (i = len; i < width; i++)
										p[i - len] = '0';
									p[i] = '\0';
								}

								width = widthset = 0;
								done = true;
							}
						}
						break;
						}
					}
				}
			}
			else
			{
				p[0] = *fp;
				p[1] = '\0';
			}

			int32 len = __wcslen(p);
			if (len > 0)
			{
				if (index + len >= destlen)
				{
					CHECK(0, TEXT("index + len >= destlen"));
					//wchar* pp = new wchar[destlen * 2];
					//memcpy(pp, dest, index * sizeof(wchar));

					//if (dest != origin)
					//	delete[] dest;

					//dest = pp;
					//destlen = destlen * 2;
				}

				memcpy(&dest[index], p, len * sizeof(wchar));
				index += len;
			}

			done = false;
		}

		dest[index] = 0;
		return dest;
	}

	string format(const wchar* fmt, ...)
	{
		wchar temp[1024] = { 0, };
		wchar*p = temp;

		va_list ap;
		va_start(ap, fmt);
		p = unicode::vsprintf(p, _countof(temp), fmt, ap);
		int32 l = unicode::strlen(p);
		va_end(ap);

		string str(p, l);
		if (p != temp)
		{
			PlatformSys::DebugOut(TEXT("[Warning] Change System!!!"));
			delete[] p;
		}
		return str;
	}

	string vformat(const wchar* fmt, va_list ap)
	{
		wchar temp[2048] = { 0, };
		wchar* p = temp;

		p = unicode::vsprintf(p, _countof(temp), fmt, ap);

		int32 l = unicode::strlen(p);

		string str(p, l);
		if (p != temp)
		{
			PlatformSys::DebugOut(TEXT("[Warning] Change System!!!"));
		}
		return str;
	}

	SizeT tokenize(Array<string>& output, const string& input, const string& whitespace)
	{
		output.Clear();

		wchar* ptr = const_cast<wchar*>(input.c_str());

		wchar* token = NULL;
		wchar* context = NULL;
		while (0 != (token = ::wcstok_s(ptr, whitespace.c_str(), &context)))
		{
			output.Add(token);
			ptr = context;
		}
		return output.Size();
	}

	string splitFilePath(const wchar* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('/');
		if (t == -1)
		{
			t = filepath.findLast('\\');
			if (t == -1)
				return string(L"");
		}
		return filepath.substr(0, t);
	}

	string splitFileName(const wchar* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('/');
		if (t == -1)
		{
			t = filepath.findLast('\\');
			if (t != -1)
				filepath = filepath.substr(t + 1, -1);
		}
		else
		{
			filepath = filepath.substr(t + 1, -1);
		}

		t = filepath.findLast('.');
		if (t == -1)
			return string(L"");

		return filepath.substr(0, t);
	}

	string splitFileExt(const wchar* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('.');
		if (t == -1)
			return string(L"");

		return filepath.substr(t, -1);
	}
	string splitFileNameExt(const wchar* path)
	{
		string filepath(path);
		int32 t = filepath.findLast('/');
		if (t == -1)
		{
			t = filepath.findLast('\\');
			if (t == -1)
				return string(L"");
		}
		return filepath.substr(t + 1, -1);
	}

	void splitPath(wchar* path, wchar* drive, wchar* dir, wchar* fname, wchar* ext)
	{
		if (drive)
			unicode::strcpy(drive, L"");

		wchar* lastslash = 0;
		for (wchar* p = path; *p != '\0'; p++)
		{
			if (*p == '/')
				lastslash = p;
		}

		if (dir)
		{
			if (lastslash == 0)
				unicode::strcpy(dir, L"");
			else
				unicode::strcpypart(dir, path, lastslash - path + 1);
		}

		wchar* lastdot = 0;
		wchar* begin = (lastslash != 0) ? lastslash + 1 : path;
		for (wchar* p = begin; *p != '\0'; p++)
		{
			if (*p == '.')
				lastdot = p;
		}

		if (lastdot == 0)
		{
			if (fname)
				unicode::strcpy(fname, begin);
			if (ext)
				unicode::strcpy(ext, L"");
		}
		else
		{
			if (fname)
				unicode::strcpypart(fname, begin, lastdot - begin);
			if (ext)
				unicode::strcpy(ext, lastdot);
		}
	}

	//********************************************************************************************************************************

#ifdef TARGET_OS_ANDROID
	//int32 	loadfile( const wchar * fname, char **ptr )
	//{
	//	int32 size = 0;

	//	char assetbuffer[1024] = { 0, };
	//	unicode::string fullassetname = unicode::string(fname) + L".jet";
	//	fullassetname.to_lower();
	//	unicode::utf16toutf8(assetbuffer, 1024, fullassetname.c_str());

	//	// Read from Android Assets.
	//	AAsset* asset = AAssetManager_open(Platform::m_assets, (const char *)assetbuffer, AASSET_MODE_UNKNOWN);
	//	if( asset )
	//	{
	//		//DebugOut(L"Load %s From Android Assets..(loadfile)\n"), fullassetname.c_str());
	//		size = AAsset_getLength(asset);
	//		*ptr = new char [size];
	//		int32 rsize = AAsset_read(asset, *ptr, size);
	//		AAsset_close(asset);
	//		return size;
	//	}

	//	return -1;
	//}
#endif

	FILE * fopen(const wchar * fname, const wchar * m, bool jet)
	{
#ifdef TARGET_OS_WINDOWS
		FILE *fp = NULL;
		_wfopen_s(&fp, fname, m);
		return fp;
#elif defined (TARGET_OS_ANDROID)

		// android rule : 1) read ext path --> 2) read assets (apk)
		FILE *fp = NULL;

#if 1
		char _extfullpath[256] = { 0, };
		utf16toutf8(_extfullpath, sizeof(_extfullpath), fname);
		char _mode[16] = { 0, };
		utf16toutf8(_mode, sizeof(_mode), m);
		// 1) Try to read from Ext sdcard
		fp = ::fopen(_extfullpath, _mode);
		if (fp)
		{
#ifdef _DEBUG
			__android_log_print(ANDROID_LOG_INFO, "ENGINE-LOG", "--> Open from SDCARD %s (%s) OK!", _extfullpath, _mode);
#endif
			return fp;
		}

		return fp;
#else
		string extfullpath;

		if (jet)
			extfullpath = Platform::m_cachePath + L"/" + fname + L".jet";
		else
			extfullpath = Platform::m_cachePath + L"/" + fname;

		extfullpath.to_lower();

		char _extfullpath[256] = { 0, };
		utf16toutf8(_extfullpath, sizeof(_extfullpath), extfullpath.c_str());
		char _mode[16] = { 0, };
		utf16toutf8(_mode, sizeof(_mode), m);

		// 1) Try to read from Ext sdcard
		fp = ::fopen(_extfullpath, _mode);
		if (fp)
		{
#ifdef _DEBUG
			__android_log_print(ANDROID_LOG_INFO, "ENGINE-LOG", "--> Open from SDCARD %s (%s) OK!", _extfullpath, _mode);
#endif
			return fp;
		}

		// assets read only!
		//assert( m[0] == 'r' );

		// 2) Try to read from APK (Assets)
		char _assetbuffer[256] = { 0, };

		string fullassetname;

		if (jet)
			fullassetname = string(fname) + L".jet";
		else
			fullassetname = string(fname);

		fullassetname.to_lower();
		unicode::utf16toutf8(_assetbuffer, sizeof(_assetbuffer), fullassetname.c_str());

		AAsset* asset = AAssetManager_open(Platform::m_assets, _assetbuffer, AASSET_MODE_UNKNOWN);
		if (asset == NULL)
		{
#ifdef _DEBUG
			__android_log_print(ANDROID_LOG_ERROR, "ENGINE-LOG", "--> Not found %s from Android Assets..(fopen)", _assetbuffer);
#endif
			return NULL;
		}

		off_t outStart, outLength;
		int32 fd = AAsset_openFileDescriptor(asset, &outStart, &outLength);

		if (fd >= 0)
		{
			fp = fdopen(fd, "r");
#ifdef _DEBUG
			__android_log_print(ANDROID_LOG_DEBUG, "ENGINE-LOG", "--> Open from Assets %s OK!", _assetbuffer);
#endif
		}
		else
		{
#ifdef _DEBUG		
			__android_log_print(ANDROID_LOG_ERROR, "ENGINE-LOG", "--> Open Android Assets Error: %s", _assetbuffer);
#endif
		}

		AAsset_close(asset);
		fseek(fp, outStart, SEEK_SET);
		return fp;
#endif

#elif defined ( TARGET_OS_IOS )

		//zardai path.....
		unicode::string strName = fname;
		strName.to_lower();

		string fullpath = string(g_DataPath) + strName.c_str();
		FILE *fp = NULL;
		char f[256] = { 0, };
		char t[16] = { 0, };
		utf16toutf8(f, sizeof(f), fullpath.c_str());
		utf16toutf8(t, sizeof(t), m);
		fp = ::fopen(f, t);

		if (fp == NULL)
		{
			fullpath = string(g_DataPath) + fname;
			utf16toutf8(f, sizeof(f), fullpath.c_str());
			fp = ::fopen(f, t);
		}

		if (fp == NULL)
		{
			fullpath = string(g_ResourcePath) + strName.c_str();
			utf16toutf8(f, sizeof(f), fullpath.c_str());
			fp = ::fopen(f, t);
		}

		if (fp == NULL)
		{
			fullpath = string(g_ResourcePath) + fname;
			utf16toutf8(f, sizeof(f), fullpath.c_str());
			fp = ::fopen(f, t);
		}
		return fp;
#endif
	}

	void fclose(FILE * fp)
	{
		::fclose(fp);
	}

} // end of namespace