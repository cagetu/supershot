#include "foundation.h"

LogFile::LogFile(const TCHAR* filePath, bool bConsoleOut, bool bDebugOut)
	: m_File(nullptr)
	, m_FilePath(filePath)
	, m_bDebugOut(bConsoleOut)
	, m_bConsoleOut(bDebugOut)
	, m_Indentation(0)
{
	Open();
}

LogFile::~LogFile()
{
	Close();
}

bool LogFile::Open()
{
	m_File = FileIO::Open(*m_FilePath, FileIO::WriteOnly);
	if (!m_File)
		return false;

	unsigned short litteEndianBOM = 0xFEFF;
	m_File->Write(&litteEndianBOM, sizeof(unsigned short));

	if (m_bConsoleOut)
	{
		m_Console.Open();
	}

	PutHeader();
	return true;
}

void LogFile::Close()
{
	PutFooter();

	if (m_bConsoleOut)
	{
		m_Console.Close();
	}

	if (m_File)
		FileIO::Close(m_File);
	m_File = nullptr;
}

void LogFile::Text(const TCHAR* text)
{
	PutTab(m_Indentation);
	PutString(text);
	PutLine();
}

void LogFile::Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber)
{
	if (m_bConsoleOut)
		m_Console.SetColor(TO_XRGB(0, 255, 255), true);

	String file = Convert(fileName);
	String func = Convert(funcName);

	PutLine();
	PutString(TEXT("----------------------[Info]----------------------"));
	PutLine();
	PutString(TEXT("File: %s, Func: %s, Line: %d"), file.c_str(), func.c_str(), lineNumber);
	PutLine();
	PutString(TEXT("Desc: %s"), text);
	PutLine();
	PutString(TEXT("---------------------------------------------------"));
	PutLine();

	if (m_bConsoleOut)
		m_Console.SetColor(RGBA_WHITE, false);
}

void LogFile::Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber)
{
	if (m_bConsoleOut)
		m_Console.SetColor(TO_XRGB(255, 255, 0), true);

	String file = Convert(fileName);
	String func = Convert(funcName);

	PutLine();
	PutString(TEXT("----------------------[Warn]----------------------"));
	PutLine();
	PutString(TEXT("File: %s, Func: %s, Line: %d"), file.c_str(), func.c_str(), lineNumber);
	PutLine();
	PutString(TEXT("Desc: %s"), text);
	PutLine();
	PutString(TEXT("---------------------------------------------------"));
	PutLine();

	if (m_bConsoleOut)
		m_Console.SetColor(RGBA_WHITE, false);
}

void LogFile::Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber)
{
	if (m_bConsoleOut)
		m_Console.SetColor(TO_XRGB(255, 0, 0), true);

	String file = Convert(fileName);
	String func = Convert(funcName);

	PutLine();
	PutString(TEXT("----------------------[Error]----------------------"));
	PutLine();
	PutString(TEXT("File: %s, Func: %s, Line: %d"), file.c_str(), func.c_str(), lineNumber);
	PutLine();
	PutString(TEXT("Desc: %s"), text);
	PutLine();
	PutString(TEXT("---------------------------------------------------"));
	PutLine();

	if (m_bConsoleOut)
		m_Console.SetColor(RGBA_WHITE, false);
}

void LogFile::Begin()
{
	m_Indentation++;
}

void LogFile::End()
{
	if (--m_Indentation < 0)
		m_Indentation = 0;
}

void LogFile::PutString(const TCHAR* format, ...)
{
	CSScopeLock lock(m_CS);

	TCHAR temp[2048];
	TCHAR* p = temp;

	va_list ap;
	va_start(ap, format);
	p = VSPrintf(p, 2048, format, ap);
	int l = Strlen(p);
	va_end(ap);

	int pos = m_File->Tell();
	if (pos < 0)
		return;

	m_File->Write(p, l);
	m_File->Flush();

	if (m_bDebugOut)
		DebugOutMsg(p);

	if (m_bConsoleOut)
		m_Console.Print(p);
}

void LogFile::PutTime()
{
	CSScopeLock lock(m_CS);

	TCHAR buffer[256];
	StrTime(buffer, 256);

	PutTab();
	PutString(buffer);
}

void LogFile::PutDate()
{
	CSScopeLock lock(m_CS);

	TCHAR buffer[256];
	StrDate(buffer, 256);

	PutTab();
	PutString(buffer);
}

void LogFile::PutLine(uint32 numLines)
{
	CSScopeLock lock(m_CS);

	for (uint32 i = 0; i < numLines; ++i)
		PutString(TEXT("\r\n"));
}

void LogFile::PutTab(uint32 numTabs)
{
	CSScopeLock lock(m_CS);

	for (uint32 i = 0; i < numTabs; ++i)
		PutString(TEXT("\t"));
}

void LogFile::PutHeader()
{
	PutLine(2);
	PutString(TEXT("[:::::::::::::::::::: Begin Log ::::::::::::::::::::]"));
	PutLine();
	PutDate();
	PutLine();
	PutTime();
	PutLine();
	PutString(TEXT("[:::::::::::::::::::: Begin Log ::::::::::::::::::::]"));
	PutLine();
}

void LogFile::PutFooter()
{
	m_Indentation = 0;

	PutLine(2);
	PutString(TEXT("[:::::::::::::::::::: End Log ::::::::::::::::::::]"));
	PutLine();
	PutDate();
	PutLine();
	PutTime();
	PutLine();
#pragma message("전체 경로를 사용해야 한다. FilePath 클래스 만들어서 사용하자!!!")
	PutString(*m_FilePath);
	PutLine();
	PutString(TEXT("[:::::::::::::::::::: End Log ::::::::::::::::::::]"));
	PutLine();
}
