#include "foundation.h"

Log::Level Log::LogLevel = Log::_Debug;
Array<Ptr<ILogStream> > Log::LogStreams;

void Log::_Init()
{
	///
}

void Log::_Shutdown()
{
	LogStreams.Reset();
}

void Log::Register(const Ptr<ILogStream>& stream)
{
	LogStreams.Add(stream);
}

void Log::Unregister(const Ptr<ILogStream>& stream)
{
	LogStreams.Remove(stream);
}

void Log::SetLevel(Level level)
{
	LogLevel = level;
}

void Log::Info(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber)
{
	if (LogLevel < Level::_Warn)
		return;

	int32 size = LogStreams.Size();
	for (int32 i = 0; i < size; i++)
	{
		LogStreams[i]->Info(text, fileName, funcName, lineNumber);
	}
}

void Log::Warn(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber)
{
	if (LogLevel < Level::_Warn)
		return;

	int32 size = LogStreams.Size();
	for (int32 i = 0; i < size; i++)
	{
		LogStreams[i]->Warn(text, fileName, funcName, lineNumber);
	}
}

void Log::Error(const TCHAR* text, const char* fileName, const char* funcName, int32 lineNumber)
{
	if (LogLevel < Level::_Error)
		return;

	int32 size = LogStreams.Size();
	for (int32 i = 0; i < size; i++)
	{
		LogStreams[i]->Error(text, fileName, funcName, lineNumber);
	}
}

void Log::Text(const TCHAR* text)
{
	if (LogLevel < Level::_Debug)
		return;

	int32 size = LogStreams.Size();
	for (int32 i = 0; i < size; i++)
	{
		LogStreams[i]->Text(text);
	}
}

void Log::Debug(Level level, const char* fileName, const char* funcName, int32 lineNumber, const TCHAR* format, ...)
{
	TCHAR temp[1024] = { 0, };
	TCHAR* p = temp;

	va_list ap;
	va_start(ap, format);
	p = VSPrintf(p, 1024, format, ap);
	va_end(ap);

	if (level == Level::_Error)
	{
		Error(p, fileName, funcName, lineNumber);
	}
	else if (level == Level::_Warn)
	{
		Warn(p, fileName, funcName, lineNumber);
	}
	else if (level == Level::_Info)
	{
		Info(p, fileName, funcName, lineNumber);
	}
	else
	{
		Text(p);
	}
}