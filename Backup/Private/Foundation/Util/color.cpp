#include "foundation.h"

Color Color::WHITE(TO_RGBA(255, 255, 255, 255));
Color Color::BLACK(TO_RGBA(0, 0, 0, 255));
Color Color::RED(TO_RGBA(255, 0, 0, 255));
Color Color::GREEN(TO_RGBA(0, 255, 0, 255));
Color Color::BLUE(TO_RGBA(0, 0, 255, 255));
Color Color::YELLOW(TO_RGBA(255, 255, 0, 255));

Color::Color(const int8 red, const int8 green, const int8 blue, const int8 alpha)
{
	r = (float)red / 255.0f;
	g = (float)green / 255.0f;
	b = (float)blue / 255.0f;
	a = (float)alpha / 255.0f;
}

Color::Color(RGBA rgba)
{
	a = static_cast<unsigned char>(rgba >> 24) / 255.0f;
	r = static_cast<unsigned char>(rgba >> 16) / 255.0f;
	g = static_cast<unsigned char>(rgba >> 8) / 255.0f;
	b = static_cast<unsigned char>(rgba) / 255.0f;
}
