#include "foundation.h"

#if _HAS_THREADS
#include <mutex>
static std::mutex lockMutex;
#define SCOPED_LOCK std::lock_guard<std::mutex> lock(lockMutex)
#else
#define SCOPED_LOCK
#endif

Map<String, String> AssignRegistry::m_Assigns;

void AssignRegistry::_Init()
{
}

void AssignRegistry::_Shutdown()
{
	m_Assigns.Clear();
}

void AssignRegistry::Insert(const String& assign, const String& path)
{
	SCOPED_LOCK
	if (m_Assigns.Contains(assign))
	{
		m_Assigns[assign] = path;
	}
	else
	{
		m_Assigns.Insert(assign, path);
	}
}

void AssignRegistry::Remove(const String& assign)
{
	SCOPED_LOCK
	m_Assigns.Remove(assign);
}

bool AssignRegistry::Has(const String& assign)
{
	SCOPED_LOCK
	return m_Assigns.Contains(assign);
}

String AssignRegistry::Find(const String& assign)
{
	SCOPED_LOCK
	IndexT index = m_Assigns.FindIndex(assign);
	if (index == INVALID_INDEX)
		return String();

	return m_Assigns.ValueAt(index);
}

String AssignRegistry::Resolve(const String& uri)
{
	SCOPED_LOCK
	String result = uri;

	IndexT index;
	while (index = result.findFirst(TEXT(':')))
	{
		if (index > 1)
		{
			String assign = result.substr(0, index);

			if (Has(assign))
			{
				String postStr = result.substr(index + 1, result.size() - (index + 1));
				String replace = Find(assign);

				if (!replace.empty())
				{
					if (replace[replace.size() - 1] != TEXT(':')
						&& (replace[replace.size() - 1] != TEXT('/')
							|| replace[replace.size() - 2] != TEXT(':')))
					{
						replace.append(TEXT("/"));
					}
					replace.append(postStr);
				}
				result = replace;
			}
			else
			{
				break;
			}
		}
		else
		{
			break;
		}
	}

	result.replace(TEXT('\\'), TEXT('/'));
	result.trim(TEXT("/"));

	return result;
}
