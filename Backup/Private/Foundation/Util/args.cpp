#include "foundation.h"
#include <vector>

CommandArgs::CommandArgs()
{
}

CommandArgs::CommandArgs(const String& args)
{
	Array<String> token;
	Tokenize(token, args, TEXT("\t\n\r-"));

	m_Args.Reserve(token.Size());
	for (SizeT i = 0; i < token.Size(); i++)
	{
		Array<String> KeyValue;
		Tokenize(KeyValue, token[i], TEXT("="));

		if (KeyValue.Size() == 2)
		{
			ULOG(Log::_Debug, "%s %s", *KeyValue[0], *KeyValue[1]);
			m_Args.Add(Element(KeyValue[0], KeyValue[1]));
		}
		else if (KeyValue.Size() == 1)
		{
			ULOG(Log::_Debug, "%s", *KeyValue[0]);
			m_Args.Add(Element(KeyValue[0]));
		}
	}
}

CommandArgs::CommandArgs(int32 argc, const char** argv)
{
	m_Args.Reserve(argc);

	for (int32 i = 0; i < argc; i++)
	{
		m_Args.Add(Element(Convert(argv[i])));
	}
}

CommandArgs::Element* CommandArgs::Find(const String& arg) const
{
	for (SizeT index = 0; index < m_Args.Size(); index++)
	{
		if (m_Args[index].name == arg)
			return &m_Args[index];
	}

	return NULL;
}

SizeT CommandArgs::Size() const
{
	return m_Args.Size();
}

bool CommandArgs::HasArg(const String& arg) const
{
	return (m_Args.FindIndex(arg) > INVALID_INDEX) ? true : false;
}

const String& CommandArgs::GetString(const String& arg, const String& defaultValue) const 
{
	CommandArgs::Element* element = Find(arg);
	return element ? element->value : defaultValue;
}

int32 CommandArgs::GetInt(const String& arg, int32 defaultValue) const
{
	CommandArgs::Element* element = Find(arg);
	return element ? StrToInt(element->value.c_str()) : defaultValue;
}

float CommandArgs::GetFloat(const String& arg, float defaultValue) const
{
	CommandArgs::Element* element = Find(arg);
	return element ? StrToFloat(element->value.c_str()) : defaultValue;
}

bool CommandArgs::GetBool(const String& arg, bool defaultValue) const
{
	CommandArgs::Element* element = Find(arg);
	return element ? StrToBool(element->value.c_str()) : defaultValue;
}
