#include "foundation.h"

Variant Variant::Null;

Variant & Variant::operator = ( const Variant& rhs )
{
	Delete();

	m_Type = rhs.m_Type;

	switch (m_Type)
	{
		case Void:
			break;
		case Int:
			m_Value.Int = rhs.m_Value.Int;
			break;
		case Float:
			m_Value.Float[0] = rhs.m_Value.Float[0];
			break;
		case Bool:
			m_Value.Bool = rhs.m_Value.Bool;
			break;
		case Vector2:
		case Vector3:
		case Vector4:
			m_Value.Float[0] = rhs.m_Value.Float[0];
			m_Value.Float[1] = rhs.m_Value.Float[1];
			m_Value.Float[2] = rhs.m_Value.Float[2];
			m_Value.Float[3] = rhs.m_Value.Float[3];
			break;
		case Matrix3:
		case Matrix:
			m_Value.Matrix = rhs.m_Value.Matrix;
			break;
		case Texture:
			//m_Value.Texture = rhs.m_Value.Texture;
			break;
		case FloatArray:
			Duplicate(rhs.m_Value.Array.Float, sizeof(float), rhs.m_Value.Array.Count);
			break;
		case Vector2Array:
			Duplicate(rhs.m_Value.Array.Vector2, sizeof(Vec2), rhs.m_Value.Array.Count);
			break;
		case Vector3Array:
			Duplicate(rhs.m_Value.Array.Vector3, sizeof(Vec3), rhs.m_Value.Array.Count);
			break;
		case Vector4Array:
			Duplicate(rhs.m_Value.Array.Vector4, sizeof(Vec4), rhs.m_Value.Array.Count);
			break;
		case MatrixArray:
			Duplicate(rhs.m_Value.Array.Matrix, sizeof(Mat4), rhs.m_Value.Array.Count);
			break;
		default:
			WARNNING(TEXT("Variant ="));
			break;
	}
	return *this;
}

bool Variant::operator == ( const Variant& rhs ) const
{
	if (rhs.m_Type == m_Type)
	{
		switch (rhs.m_Type)
		{
		case Void:
			return true;
		case Int:
			return (m_Value.Int == rhs.m_Value.Int);
		case Bool:
			return (m_Value.Bool == rhs.m_Value.Bool);
		case Float:
			return (m_Value.Float[0] == rhs.m_Value.Float[0]);
		case Vector2:
		case Vector3:
		case Vector4:
			return !memcmp(m_Value.Float, rhs.m_Value.Float, sizeof(float)*4);
		case Texture:
			//return (m_Value.Texture == rhs.m_Value.Texture);
		case Matrix :
		case Matrix3 :
			return !memcmp(&m_Value.Matrix, &rhs.m_Value.Matrix, sizeof(Mat4));
		case FloatArray :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Float, rhs.m_Value.Array.Float, sizeof(float)*Count()) ? true : false;
		case Vector2Array:
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Vector2, rhs.m_Value.Array.Vector2, sizeof(Vec2)*Count()) ? true : false;
		case Vector3Array :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Vector3, rhs.m_Value.Array.Vector3, sizeof(Vec3)*Count()) ? true : false;
		case Vector4Array :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Vector4, rhs.m_Value.Array.Vector4, sizeof(Vec4)*Count()) ? true : false;
		case MatrixArray :
			return Count() == rhs.Count() && !memcmp(m_Value.Array.Matrix, rhs.m_Value.Array.Matrix, sizeof(Matrix)*Count()) ? true : false;
		default:
			WARNNING(TEXT("Variant =="));
			return false;
		}
	}
	return false;
}

void Variant::Delete()
{
    if (FloatArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
			delete[] (m_Value.Array.Float);
    }
    else if (Vector2Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec2) > minsize)
			delete[] (m_Value.Array.Vector2);
    }
    else if (Vector3Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec3) > minsize)
			delete[] (m_Value.Array.Vector3);
    }
    else if (Vector4Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec4) > minsize)
			delete[] (m_Value.Array.Vector4);
    }
    else if (MatrixArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
			delete[] (m_Value.Array.Matrix);
    }
	m_Type = Void;
}

void Variant::Reset()
{
    if (FloatArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Float[i] = 0.0f;
		}
    }
    else if (Vector2Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec2) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector2[i].x = m_Value.Array.Vector2[i].y = 0.0f;
		}
    }
    else if (Vector3Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec3) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector3[i].x = m_Value.Array.Vector3[i].y = m_Value.Array.Vector3[i].z = 0.0f;
		}
    }
    else if (Vector4Array == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vec4) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector4[i].x = m_Value.Array.Vector4[i].y = m_Value.Array.Vector4[i].z = m_Value.Array.Vector4[i].w = 0.0f;
		}
    }
    else if (MatrixArray == m_Type)
    {
		const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
		{
			for(uint32 i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Matrix[i] = Mat4::IDENTITY;
		}
    }
}

void Variant::Duplicate(const void * ptr, int32 pitch, int32 num)
{
	const uint32 minsize = sizeof(m_Value) - (int32)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
	uint32 size = pitch * num;

	m_Value.Array.Count = num;
	m_Value.Array.ptr = size <= minsize ? &m_Value.Array._empty : (void*)(new char [size]);
	PlatformSys::MemCpy(m_Value.Array.ptr, ptr, size);
}

//float _ttt23232[11];
//Variant _ttt2312(_ttt23232, 11);