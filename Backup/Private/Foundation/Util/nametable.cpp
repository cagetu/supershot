#include "foundation.h"
#include "nametable.h"

NameTable::NameTable()
	: m_NumFreeIndices(0)
{
}

// Returns the newly created index
NAME_INDEX NameTable::CreateName(const TCHAR* str)
{
	if (str == NULL)
		return INVALID_INDEX;

	if (!IsValidData(str))
		return INVALID_INDEX;

	NAME_INDEX newIndex = PopFreeIndex();
	NameChunk& chunk = m_ChunkPool[newIndex];
	CHECK(Strlen(str) <= kMaxNameLength, TEXT("Name Length is too long!!"));

	Strcpy(chunk.data, _countof(chunk.data), str);
	return newIndex;
}

// You don't need to call this when you finished using the Name
// This method is for Unregister the name because it is not needed to be
// Cached
void NameTable::DeleteName(NAME_INDEX index)
{
	CHECK(IsValidIndex(index), TEXT("index: %d"), index);

	m_FreePool.Push(index);
}

const TCHAR* NameTable::GetData(NAME_INDEX index) const
{
	if (!IsValidIndex(index))
		return NULL;

	return m_ChunkPool[index].data;
}

NAME_INDEX NameTable::FindName(const TCHAR* str)
{
	// Couldn't use chunk_pool_.FindSlow() because of the T type mismatch
	for (int32 index = 0; index < kMaxChunkCount; index++)
	{
		NameChunk& chunk = m_ChunkPool[index];

		if (Str::IsEqual(str, chunk.data))
			return index;
	}
	return INVALID_INDEX;
}

// Returns a free index
NAME_INDEX NameTable::PopFreeIndex()
{
	if (m_FreePool.IsEmpty())
		return m_NumFreeIndices++;

	return m_FreePool.Pop();
}

bool NameTable::IsValidData(const TCHAR* str) const
{
	if (str == NULL)
		return false;

	int32 length = Strlen(str);
	if (length == 0 || (length > kMaxNameLength))
		return false;

	return true;
}

