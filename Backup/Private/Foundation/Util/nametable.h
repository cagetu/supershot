// Copyright(c) 2018~.cagetu
//
//******************************************************************
#pragma once

//------------------------------------------------------------------
/** @class	NameTable
	@author cagetu
	@desc	Name의 String 정보를 가지고 있는 Table
			https://github.com/zho7611/dg.git 를 인용
*/
//------------------------------------------------------------------
class NameTable
{
public:
	static const int kMaxNameLength = 127;

public:
	NameTable();
	~NameTable() = default;

	/// Create Name
	NAME_INDEX CreateName(const TCHAR* str);
	/// Delete Name
	void DeleteName(NAME_INDEX index);
	/// FindName (Slow)
	NAME_INDEX FindName(const TCHAR* str);
	/// Name To Data
	const TCHAR* GetData(NAME_INDEX index) const;

private:
	struct NameChunk
	{
		TCHAR data[kMaxNameLength + 1];

		NameChunk()
		{
			Memory::MemSet(data, 0);
		}
		~NameChunk() = default;
	};

	typedef Array<NAME_INDEX> FreeIndexType;
	static const int kMaxChunkCount = 1024;

	NameChunk m_ChunkPool[kMaxChunkCount];
	FreeIndexType m_FreePool;
	int32 m_NumFreeIndices;

	NAME_INDEX PopFreeIndex();

	bool IsValidIndex(NAME_INDEX index) const {
		return (index >= 0) && (index < kMaxChunkCount);
	}
	bool IsValidData(const TCHAR* str) const;
};