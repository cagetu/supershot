#include "foundation.h"
#include "nametable.h"

NameTable* Name::s_NameTable = NULL;

void Name::_Init()
{
	s_NameTable = MAKE_NEW(NameTable);
}

void Name::_Shutdown()
{
	MAKE_DELETE(NameTable, s_NameTable);
}

Name::Name()
	: m_Index(INVALID_INDEX)
{
}

Name::Name(const TCHAR* str)
	: m_Index(INVALID_INDEX)
{
	Set(str);
}

Name::Name(const String& str)
	: m_Index(INVALID_INDEX)
{
	Set(str);
}

Name::Name(const Name& str)
	: m_Index(str.m_Index)
{
}

void Name::Set(const TCHAR* str)
{
	if (!str)
	{
		m_Index = INVALID_INDEX;
		return;
	}

	NAME_INDEX index = s_NameTable->FindName(str);
	m_Index = (index == INVALID_INDEX) ? s_NameTable->CreateName(str) : index;
}

void Name::Set(const String& str)
{
	Set(str.c_str());
}

void Name::Set(const Name& str)
{
	m_Index = str.m_Index;
}

void Name::Append(const TCHAR* str)
{
	String src(Get());
	String dest(str);

	Set(src.append(dest));
}

void Name::Append(const String& str)
{
	String src(Get());

	Set(src.append(str));
}

const TCHAR* Name::Get() const
{
	return (m_Index == INVALID_INDEX) ? NULL : s_NameTable->GetData(m_Index);
}

bool Name::IsEqual(const TCHAR* str) const
{
	return Str::IsEqual(Get(), str);
}

Name& Name::operator=(const Name& other)
{
	Set(other);
	return *this;
}

bool Name::operator==(const Name& other)
{
	return IsEqual(other);
}