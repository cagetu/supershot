#include "renderer.h"

__ImplementRtti(VisualShock, ISurface, IRenderResource);
//
ISurface::ISurface()
	: m_Width(0)
	, m_Height(0)
	, m_Depth(1)
	, m_PixelFormat(0)
	, m_MultiSamples(1)
	, m_MipmapCount(1)
	, m_MipmapLevel(1)
	, m_LoadAction(RTLoadAction::EClear)
	, m_StoreAction(RTStoreAction::EStore)
{
}
ISurface::~ISurface()
{
}

int32 ISurface::GetByteSize() const
{
	int bpp = 0;
	switch (m_PixelFormat)
	{
	case RT_R5G6B5:
	case RT_R5G5B5A1:
	case RT_R4G4B4A4:
		bpp = 16;
		break;
	case RT_R8G8B8:
		bpp = 24;
		break;
	case RT_R8G8B8A8:
		bpp = 32;
		break;
	default:
		_ASSERT(0);
	};
	return bpp / 8;
}

//
//
//

__ImplementRtti(VisualShock, IDepthStencilSurface, ISurface);
//
IDepthStencilSurface::IDepthStencilSurface()
	: m_StencilLoadAction(RTLoadAction::ENoAction)
	, m_StencilStoreAction(RTStoreAction::ENoAction)
{
}
IDepthStencilSurface::~IDepthStencilSurface()
{
}