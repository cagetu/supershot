#include "renderer.h"

RenderState::RenderState()
	: m_RenderState(0)
{
}

void RenderState::Reset()
{
	m_RenderState = (uint32)-1;
	Update(0);
}

