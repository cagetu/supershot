#include "renderer.h"

Mat4 D3D11Transform::Multiply(const Mat4 & a, const Mat4 & b)
{
	Mat4 m;
#ifdef _D3D9
	D3DXMatrixMultiply((D3DXMATRIX*)&m, (const D3DXMATRIX*)&a, (const D3DXMATRIX*)&b);
#else
	m._11 = a._11 * b._11 + a._12 * b._21 + a._13 * b._31 + a._14 * b._41;
	m._12 = a._11 * b._12 + a._12 * b._22 + a._13 * b._32 + a._14 * b._42;
	m._13 = a._11 * b._13 + a._12 * b._23 + a._13 * b._33 + a._14 * b._43;
	m._14 = a._11 * b._14 + a._12 * b._24 + a._13 * b._34 + a._14 * b._44;

	m._21 = a._21 * b._11 + a._22 * b._21 + a._23 * b._31 + a._24 * b._41;
	m._22 = a._21 * b._12 + a._22 * b._22 + a._23 * b._32 + a._24 * b._42;
	m._23 = a._21 * b._13 + a._22 * b._23 + a._23 * b._33 + a._24 * b._43;
	m._24 = a._21 * b._14 + a._22 * b._24 + a._23 * b._34 + a._24 * b._44;

	m._31 = a._31 * b._11 + a._32 * b._21 + a._33 * b._31 + a._34 * b._41;
	m._32 = a._31 * b._12 + a._32 * b._22 + a._33 * b._32 + a._34 * b._42;
	m._33 = a._31 * b._13 + a._32 * b._23 + a._33 * b._33 + a._34 * b._43;
	m._34 = a._31 * b._14 + a._32 * b._24 + a._33 * b._34 + a._34 * b._44;

	m._41 = a._41 * b._11 + a._42 * b._21 + a._43 * b._31 + a._44 * b._41;
	m._42 = a._41 * b._12 + a._42 * b._22 + a._43 * b._32 + a._44 * b._42;
	m._43 = a._41 * b._13 + a._42 * b._23 + a._43 * b._33 + a._44 * b._43;
	m._44 = a._41 * b._14 + a._42 * b._24 + a._43 * b._34 + a._44 * b._44;
#endif
	return m;
}

// z : [0, 1]
Mat4 D3D11Transform::OrthoOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	Mat4 m;
#ifdef _D3D9
	D3DXMatrixOrthoOffCenterLH((D3DXMATRIX*)&m, l, r, b, t, zn, zf);
#else
	m = CommonTransform::OrthoOffCenter(l, r, b, t, zn, zf);
#endif // _D3D9
	return m;
}

Vector4 D3D11Transform::Transform(const Mat4 & m, const Vector4 & p)
{
#ifdef _D3D9
	Vector4 out;
	D3DXVector4Transform((D3DXVECTOR4*)&out, (const D3DXVECTOR4*)&p, (const D3DXMATRIX*)this);
	return out;
#else
	Vector4 out(p.x*m._11 + p.y*m._21 + p.z*m._31 + p.w*m._41,
		p.x*m._12 + p.y*m._22 + p.z*m._32 + p.w*m._42,
		p.x*m._13 + p.y*m._23 + p.z*m._33 + p.w*m._43,
		p.x*m._14 + p.y*m._24 + p.z*m._34 + p.w*m._44);
	return out;
#endif
}

void D3D11Transform::Decompose(const Mat4& input, Vector3& translate, Quat4& rotate, Vector3& scale)
{
#ifdef _D3D9
	D3DXMatrixDecompose((D3DXVECTOR3*)&scale.x, (D3DXQUATERNION*)&rotate.x, (D3DXVECTOR3*)&translate, (const D3DXMATRIX*)&input._11);
#endif
}