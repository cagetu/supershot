#include "renderer.h"

//////

class RenderTargetPool
{
public:
	static void Initialize();
	static void Shutdown();

	static IRenderTarget* Create(int width, int height, uint32 format[4], int mipmapLevel = 0, int multisamples = 0);
	static void Release(IRenderTarget* rt);

	static void Reset();

	static void Begin();
	static void End();

private:
	struct _RENDERTARGET
	{
		bool busy;

		int width, hight;
		uint32 format[4];
		int mipmaplevel;
		int multisamples;

		IRenderTarget* rt;

		bool operator == (IRenderTarget* rt) { return this->rt == rt ? true : false; }
	};

	static std::vector<_RENDERTARGET> m_RenderTargetPool;
};


/////
////
////
void RenderPipeline::Initialize()
{
}

void RenderPipeline::Shutdown()
{
	RenderTargetPool::Shutdown();
	RenderPass::Shutdown();
}


RenderPipeline::RenderPipeline()
{
	m_RenderCallback = NULL;
}

RenderPipeline::~RenderPipeline()
{
	Destroy();
}

bool RenderPipeline::Create(const TCHAR* filename)
{
	CMiniXML miniXML;
	bool result = miniXML.Open(filename);
	if (result == false)
		return false;

	CMiniXML::iterator node = miniXML.GetRoot();
	for (CMiniXML::iterator iter = node.GetChild(); iter != NULL; iter = iter.Next())
	{
		const TCHAR* className = iter.GetName();
		//PlatformUtil::DebugOutFormat(L"%s\n", name);

		char temp[128];
		string::conv_s(temp, 128, className);
		RenderPass* renderPass = RenderPass::CreateClass(temp);
		_ASSERT(renderPass != NULL);

		if (renderPass->Load(iter, this) == false)
			continue;

		renderPass->Initialize();

		m_RenderPasses.push_back(renderPass);
	}
	return true;
}

void RenderPipeline::Destroy()
{
	{
		std::vector <RenderPass*>::iterator it, itend;
		itend = m_RenderPasses.end();
		for (it = m_RenderPasses.begin(); it != itend; it++)
			delete (*it);
		m_RenderPasses.clear();
	}
	{
		std::map <String, IDepthStencilSurface*>::iterator it;
		for (; it != m_DepthBuffers.end(); it++)
			it->second->DecRef();
		m_DepthBuffers.clear();
	}
	m_RenderTextures.clear();
}

void RenderPipeline::Update()
{
	std::vector <RenderPass*>::iterator it, itend;

	itend = m_RenderPasses.end();
	for (it = m_RenderPasses.begin(); it != itend; it++)
		(*it)->Update();
}

void RenderPipeline::Render()
{
	Reset();

	std::vector <RenderPass*>::iterator it, itend;

	itend = m_RenderPasses.end();
	for (it = m_RenderPasses.begin(); it != itend; it++)
	{
		RenderPass* r = (*it);

		r->Render();
	}
}

void RenderPipeline::SetCallback(RenderPipeline::RenderCallback* callback)
{
	m_RenderCallback = callback;
}

RenderPipeline::RenderCallback* RenderPipeline::GetRenderCallback() const
{
	return m_RenderCallback;
}

IRenderTarget* RenderPipeline::CreateRenderTarget(int width, int height, const TCHAR* name[4], uint32 format[4], int mipmapLevel, int multisamples)
{
	IRenderTarget* rt = RenderTargetPool::Create(width, height, format, mipmapLevel, multisamples);
	for (int i = 0; i < 4; i++)
	{
		IRenderTexture2D* texture = rt->GetRenderTexture(i);
		if (texture)
		{
			m_RenderTextures.insert(std::make_pair(name[i], texture));
		}
	}
	return rt;
}

IRenderTexture2D* RenderPipeline::GetRenderTexture(const TCHAR* name) const
{
	std::map <String, IRenderTexture2D*>::const_iterator it = m_RenderTextures.find(name);
	if (it == m_RenderTextures.end())
		return NULL;

	return it->second;
}

Ptr<IDepthStencilSurface> RenderPipeline::CreateDepthBuffer(const TCHAR* name, int width, int height, int format, int multisamples)
{
	std::map<String, IDepthStencilSurface*>::iterator it = m_DepthBuffers.find(name);
	if (it != m_DepthBuffers.end())
	{
		IDepthStencilSurface* surface = it->second;
		_ASSERT(surface->GetWidth() == width && surface->GetHeight() == height && surface->GetFormat() == format && surface->GetMultisamples() == multisamples);
		return it->second;
	}

	IDepthStencilSurface* depthBuffer = CreateDepthStencilSurface();
	bool result = depthBuffer->Create(width, height, format, multisamples);
	_ASSERT(result == true);

	m_DepthBuffers.insert(std::make_pair(name, depthBuffer));
	depthBuffer->IncRef();

	return depthBuffer;
}

Ptr<IDepthStencilSurface> RenderPipeline::GetDepthBuffer(const TCHAR* name) const
{
	std::map<String, IDepthStencilSurface*>::const_iterator it = m_DepthBuffers.find(name);
	if (it == m_DepthBuffers.end())
		return NULL;

	return it->second;
}

void RenderPipeline::Reset()
{
	RenderTargetPool::Reset();
	m_RenderTextures.clear();
}

/////////////////////////////
std::vector<RenderTargetPool::_RENDERTARGET> RenderTargetPool::m_RenderTargetPool;

void RenderTargetPool::Initialize()
{
}

void RenderTargetPool::Shutdown()
{
	{
		std::vector <_RENDERTARGET>::iterator it;
		for (; it != m_RenderTargetPool.end(); it++)
		{
			_RENDERTARGET& rt = (*it);
			delete (rt.rt);
		}
		m_RenderTargetPool.clear();
	}
}

IRenderTarget* RenderTargetPool::Create(int width, int height, uint32 format[4], int mipmapLevel, int multisamples)
{
	std::vector <_RENDERTARGET>::iterator it = m_RenderTargetPool.begin();
	for (; it != m_RenderTargetPool.end(); it++)
	{
		_RENDERTARGET& rt = (*it);

		if (rt.busy == true)
			continue;

		if (rt.width == width &&
			rt.hight == height &&
			rt.format[0] == format[0] && rt.format[1] == format[1] && rt.format[2] == format[2] && rt.format[3] == format[3] &&
			rt.mipmaplevel == mipmapLevel &&
			rt.multisamples == multisamples)
		{
			rt.busy = true;
			return rt.rt;
		}
	}

	IRenderTarget* IRenderTarget = CreateRenderTarget();
	if (IRenderTarget->Create(width, height) == false)
	{
		_ASSERT(0);
		return NULL;
	}

	for (int i = 0; i<4; i++)
	{
		if (format[i] > 0)
			IRenderTarget->AddRenderTexture(format[i]);
	}

	_RENDERTARGET rt;
	rt.busy = true;
	rt.width = width;
	rt.hight = height;
	rt.rt = IRenderTarget;
	rt.mipmaplevel = mipmapLevel;
	rt.multisamples = multisamples;
	rt.format[0] = format[0];
	rt.format[1] = format[1];
	rt.format[2] = format[2];
	rt.format[3] = format[3];

	m_RenderTargetPool.push_back(rt);

	return IRenderTarget;
}

void RenderTargetPool::Release(IRenderTarget* rt)
{
	for (std::vector <_RENDERTARGET>::iterator it = m_RenderTargetPool.begin(); it != m_RenderTargetPool.end(); it++)
	{
		if ((*it).rt == rt)
		{
			(*it).busy = false;
			break;
		}
	}
}

void RenderTargetPool::Reset()
{
	for (std::vector <_RENDERTARGET>::iterator it = m_RenderTargetPool.begin(); it != m_RenderTargetPool.end(); it++)
	{
		(*it).busy = false;
	}
}
