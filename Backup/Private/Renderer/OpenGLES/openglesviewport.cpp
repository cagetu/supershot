#include "renderer.h"

__ImplementRtti(VisualShock, GLESViewport, IViewport);

GLESViewport::GLESViewport()
{

}

GLESViewport::~GLESViewport()
{

}

bool GLESViewport::Create(void* windowHandle, int32 width, int32 height, int32 format, uint32 featureFlag, const RelativeExtent& extent)
{
	return false;
}

void GLESViewport::Destroy()
{

}

void GLESViewport::Begin()
{

}

void GLESViewport::End()
{

}

void GLESViewport::Present()
{
}

void GLESViewport::Resize(int32 width, int32 height, int32 format, bool fullscreen)
{
}