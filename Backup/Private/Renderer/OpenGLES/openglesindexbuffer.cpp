#include "renderer.h"

__ImplementRtti(VisualShock, GLESIndexBuffer, IndexBuffer);

GLESIndexBuffer::GLESIndexBuffer()
	: m_Buffer(0)
	, m_IsIBO(false)
{
}

GLESIndexBuffer::~GLESIndexBuffer()
{
}

bool GLESIndexBuffer::Create(int indexCount, int32 stride)
{
	int byteSize = indexCount * stride;

	if (CreateBuffer(byteSize, BUF_Static) == false)
		return false;

	SetStride(stride);
	m_Length = byteSize;
	m_IsIBO = true;

	return true;
}

bool GLESIndexBuffer::Create(int indexCount, void* indexData, int32 stride)
{
	if (Create(indexCount, stride) == false)
		return false;

	void* ptr = Lock();
	{
		Memory::Memcopy(ptr, indexData, m_Length);
	}
	Unlock();

	return true;

}

void GLESIndexBuffer::Destroy()
{
	if (m_BufferUsage&BUF_Static)
	{
		if (m_Buffer != 0)
		{
			glDeleteBuffers(1, &m_Buffer);
		}
	}

	IndexBuffer::Destroy();
}

unsigned char* GLESIndexBuffer::GetBytes() const
{
	if (m_IsIBO == true)
		return NULL;

	return IndexBuffer::GetBytes();
}

bool GLESIndexBuffer::CreateBuffer(int byteSize, uint32 usage)
{
	glGenBuffers(1, &m_Buffer);

	m_Length = byteSize;
	m_BufferUsage = usage;

	return m_Buffer == 0 ? false : true;
}

void* GLESIndexBuffer::Lock(int length, int lockFlag)
{
	if (m_BufferUsage&BUF_Static)
	{
		_ASSERT(length <= m_Length);

		if (m_Buffer != 0 && m_IdxBuf == NULL)
		{
			m_IdxBuf = new unsigned char[m_Length];
		}
		return &m_IdxBuf[m_Offset];
	}
	else if (m_BufferUsage&BUF_Dynamic)
	{

	}

	return NULL;
}

void GLESIndexBuffer::Unlock(ushort baseVertIdx)
{
	if (m_BufferUsage&BUF_Static)
	{
		// Index Buffer Object ����

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Buffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Length, m_IdxBuf, GL_STATIC_DRAW);
		TestGLError(TEXT("[GLESIndexBuffer::Unlock] glBufferData"));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	else if (m_BufferUsage&BUF_Dynamic)
	{
	}
}

int GLESIndexBuffer::SetIndices()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IsIBO() ? m_Buffer : 0);
	TestGLError(TEXT("[CIndexBuffer::SetIndices] glBindBuffer"));

	return m_Offset / GetStride();
}