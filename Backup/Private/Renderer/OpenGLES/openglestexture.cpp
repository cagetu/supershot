#include "renderer.h"
#include <lodepng.h>
#include <tga.h>
#include <OGLES2/PVRTTextureAPI.h>
#include <PVRTTexture.h>


GLESTexture::GLESTexture()
	: m_Texture(0)
	, m_TextureTarget(GL_TEXTURE_2D)
{
}

GLESTexture::~GLESTexture()
{
	if (m_Texture != 0)
	{
		glDeleteTextures(1, &m_Texture);
		m_Texture = 0;
	}
}

GLESTexture::_PIXELFORMAT GLESTexture::ConvertPixelFormat(uint32 format)
{
	_PIXELFORMAT output;
	output.internalformat = GL_RGBA;
	output.format = GL_RGBA;
	output.type = GL_UNSIGNED_BYTE;

	switch (format)
	{
	case RT_R5G6B5:
		output.internalformat = GL_RGB;
		output.format = GL_RGB;
		output.type = GL_UNSIGNED_SHORT_5_6_5;
		break;
	case RT_R8G8B8:
		output.internalformat = GL_RGB;
		output.format = GL_RGB;
		output.type = GL_UNSIGNED_BYTE;
		break;
	case RT_R5G5B5A1:
		output.internalformat = GL_RGBA;
		output.format = GL_RGBA;
		output.type = GL_UNSIGNED_SHORT_5_5_5_1;
		break;
	case RT_R4G4B4A4:
		output.internalformat = GL_RGBA;
		output.format = GL_RGBA;
		output.type = GL_UNSIGNED_SHORT_4_4_4_4;
		break;
	case RT_R8G8B8A8:
		output.internalformat = GL_RGBA;
		output.format = GL_RGBA;
		output.type = GL_UNSIGNED_BYTE;
		break;
	case RT_DEPTH_BYTE:
		output.internalformat = GL_RGBA;
		output.format = GL_DEPTH_COMPONENT;
		output.type = GL_UNSIGNED_BYTE;
		break;
	case RT_DEPTH_INT:
		output.internalformat = GL_DEPTH_COMPONENT16;
		output.format = GL_DEPTH_COMPONENT;
		output.type = GL_UNSIGNED_INT;
		break;
	case RT_DEPTH_SHORT:
		output.internalformat = GL_DEPTH_COMPONENT16;
		output.format = GL_DEPTH_COMPONENT;
		output.type = GL_UNSIGNED_SHORT;
		break;
	case RT_R16F:
		if (COpenGLESCaps::IsSupportColorBufferFloat())
		{
			output.internalformat = GL_R16F_EXT;
			output.format = GL_RED_EXT;
			output.type = GL_HALF_FLOAT_OES;
		}
		break;
	case RT_A16B16G16R16F:
		if (COpenGLESCaps::IsSupportTextureHalfFloat() && COpenGLESCaps::IsSupportColorBufferHalfFloat())
		{
#ifdef __ANDROID__
			output.internalformat = GL_RGBA;
			output.format = GL_RGBA16F_EXT;
			output.type = GL_HALF_FLOAT_OES;
#else
			output.internalformat = GL_RGBA;
			output.format = GL_RGBA;
			output.type = GL_HALF_FLOAT_OES;
#endif
		}
		else
		{
			output.internalformat = GL_RGBA;
			output.format = GL_RGBA;
			output.type = GL_UNSIGNED_BYTE;
		}
		break;
	//case RT_ALPHA :
	//	pixelFmt = GL_ALPHA;
	//	pixelType = GL_ALPHA;
	//	break;
	//case RT_LUMINANCE :
	//	pixelFmt = GL_LUMINANCE;
	//	pixelType = GL_LUMINANCE;
	//	break;
	//case RT_LUMINACEALPHA :
	//	pixelFmt = GL_LUMINANCE_ALPHA;
	//	pixelType = GL_LUMINANCE_ALPHA;
	//	break;
	default:
		_ASSERT(0);
	}

	return output;
}

/////////////////////////////////////////////////////////////////////////
//
__ImplementRtti(VisualShock, GLESTexture2D, ITexture2D);

GLESTexture2D::GLESTexture2D()
{
}

GLESTexture2D::~GLESTexture2D()
{
}

bool GLESTexture2D::Load(const TCHAR* fname)
{
	char filename[128];
	string::conv_s(filename, 128, fname);

	String fext = string::splitFileExt(fname);
	if (fext == TEXT(".png"))
	{
		std::vector<unsigned char> image;
		unsigned width, height;
		unsigned error = lodepng::decode(image, width, height, filename);

		if (error)
		{
			const char* log = lodepng_error_text(error);

			return false;
		}
	}
	else if (fext == TEXT(".tga"))
	{
		TGA* tga = TGAOpen(filename, "r");
		if (!tga || tga->last != TGA_OK)
		{
			return false;
		}

		TGAData data;

		/* the TGA_IMAGE_ID tells the TGAReadImage() to read the image
		id, the TGA_IMAGE_DATA tells it to read the whole image data
		and any color map data if existing. NOTE: the image header is
		read always no matter what options were specified.
		At last we pass over the TGA_RGB flag so the returned data is
		in RGB format and not BGR */
		data.flags = TGA_IMAGE_DATA | TGA_IMAGE_ID | TGA_RGB;
		if (TGAReadImage(tga, &data) != TGA_OK) {
			/* error handling goes here */
			return false;
		}

		TGAClose(tga);
	}
	else if (fext == TEXT(".pvr"))
	{
		GLuint textureHandle;
		PVRTextureHeaderV3 header;

		EPVRTError errorCode = PVRTTextureLoadFromPVR(filename, &textureHandle, &header);
		if (errorCode == PVR_FAIL)
		{
			return false;
		}
		else if (errorCode == PVR_OVERFLOW)
		{
			return false;
		}

		m_ID = fname;

		m_Width = header.u32Width;
		m_Height = header.u32Height;
		m_Depth = header.u32Depth;

		//m_PixelFormat = header.u64PixelFormat;

		m_Texture = textureHandle;
		m_TextureTarget = header.u32TextureTarget;
		return true;
	}

	return false;
}

void GLESTexture2D::SetTexture()
{
	glBindTexture(m_TextureTarget, m_Texture);
	TestGLError(TEXT("[GLESTexture2D::SetTexture] texture"));
}

/////////////////////////////////////////////////////////////////////////
//
__ImplementRtti(VisualShock, GLESWritableTexture, IWritableTexture);

GLESWritableTexture::GLESWritableTexture()
{
	m_BufferSize = 0;
	m_Buffer = NULL;
}

GLESWritableTexture::~GLESWritableTexture()
{
	if (m_Buffer != NULL)
	{
		delete[] m_Buffer;
		m_Buffer = NULL;
		m_BufferSize = 0;
	}
}

bool GLESWritableTexture::Create(int width, int height, int format)
{
	_ASSERT(width > 0 && height > 0 && format > 0);

	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	if (glGetError())
	{
		PlatformUtil::Dialog("[GLESWritableTexture::Create] failed: glBindTexture() failed. ", NULL, 0);
		PlatformUtil::DebugOut("[GLESWritableTexture::Create] failed: glBindTexture() failed. \n");
		return false;
	}

	_PIXELFORMAT fmt = ConvertPixelFormat(format);

	glTexImage2D(GL_TEXTURE_2D, 0, fmt.internalformat, width, height, 0, fmt.format, fmt.type, NULL);
	if (glGetError())
	{
		PlatformUtil::DebugOut("[GLESWritableTexture::Create] failed: glTexImage2D() failed. \n");
		return false;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	m_Width = width;
	m_Height = height;
	m_PixelFormat = format;

	m_BufferSize = width * height * GetByteSize();
	m_Buffer = new unsigned char[m_BufferSize];

	m_MipmapCount = 0;
	m_MipmapLevel = 0;

	m_TextureTarget = GL_TEXTURE_2D;
	return true;
}

void* GLESWritableTexture::Lock(int *pitch, int lv, int flags)
{
	int stride = GetByteSize();

	if (pitch)
		*pitch = -m_Width * stride;
	return &m_Buffer[(m_Height - 1) * (m_Width * stride)];
}

void GLESWritableTexture::Unlock(bool force, int* rect)
{
	if (rect != NULL)
	{
		m_LockRect[0] = MIN(rect[0], m_LockRect[0]);
		m_LockRect[1] = MIN(rect[1], m_LockRect[1]);
		m_LockRect[2] = MAX(rect[2], m_LockRect[2]);
		m_LockRect[3] = MAX(rect[3], m_LockRect[3]);
	}

	_PIXELFORMAT pixelfmt = ConvertPixelFormat(m_PixelFormat);

	glBindTexture(GL_TEXTURE_2D, m_Texture);
	TestGLError(TEXT("[GLESWritableTexture::Unlock] failed: glBindTexture() failed"));

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	/*	NOTE
	- 일반적으로 glTexSubImage2D가 (tile based deferred rendering)기반의 디바이스에서는 매우 속도가 느린 것으로 알려져 있다.
	- 전체이미지를 전송한다고 한다면, glTexImage2D가 조금 더 빠른거 같다...
	- 현재 텍스쳐 크기만큼 버퍼크기를 잡아서 값을 쓰고 있기 때문에, x축은 그냥 pitch 크기 만큼 복사하고, y의 범위만 가지고 쓰도록 하자! (2011-11-01)
	(단, opengles의 uv 0,0은 좌측 하단이다. 따라서, 텍스쳐버퍼의 밑에서부터 위로 값을 기록해나가기 때문에, 거기에 맞춰서 오프셋을 결정해주어야 한다!)
	*/
#if 0
	glTexImage2D(GL_TEXTURE_2D, 0, pixelfmt.internalformat, m_Size[0], m_Size[1], 0, pixelfmt.format, pixelfmt.type, m_TextureBuf);
	if (glGetError())
		util::DebugOut(L"[GLESWritableTexture::Unlock] failed: glTexSubImage2D() failed.\n");
#else
	//if ((m_LockRect[2]-m_LockRect[0])==m_Size[0] && (m_LockRect[3]-m_LockRect[1])==m_Size[1])
	//{
	//	glTexImage2D(GL_TEXTURE_2D, 0, pixelfmt, m_Size[0], m_Size[1], 0, pixelfmt, pixeltype, m_TextureBuf);
	//	if (glGetError())
	//		util::DebugOut(L"[CTexture::Unlock] failed: glTexImage2D() failed.\n");

	//	util::DebugOut(L"[CTexture::Unlock] Update All\n");
	//}
	//else
	{
		int yoffset = (m_Height - m_LockRect[3]);
		int height = m_LockRect[3] - m_LockRect[1];
		_ASSERT(height > 0);

		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, yoffset, m_Width, height, pixelfmt.format, pixelfmt.type, &m_Buffer[yoffset*m_Width * GetByteSize()]);
		TestGLError(TEXT("[GLESWritableTexture::Unlock] glTexSubImage2D"));

		//util::DebugOut(L"[CTexture::Unlock] Update(x:%d, y:%d, w:%d, h:%d)\n", 0, (m_Size[1]-m_LockRect[3]), m_Size[0], m_LockRect[3]-m_LockRect[1]);
	}
#endif
	glBindTexture(GL_TEXTURE_2D, 0);

	// 초기화...
	m_LockRect[0] = 99999;
	m_LockRect[1] = 99999;
	m_LockRect[2] = 0;
	m_LockRect[3] = 0;

	//util::DebugOut(L"[CTexture::Unlock] Update Time: %.1fms\n", (float)(Platform::GetTick() - tick));
}

/////////////////////////////////////////////////////////////////////////
//
__ImplementRtti(VisualShock, GLESRenderTexture, IRenderTexture2D);

GLESRenderTexture::GLESRenderTexture()
{
}

GLESRenderTexture::~GLESRenderTexture()
{
}

bool GLESRenderTexture::Create(int width, int height, int format)
{
	_ASSERT(width > 0 && height > 0 && format > 0);

	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	if (glGetError())
	{
		PlatformUtil::Dialog("[GLESRenderTexture::Create] failed: glBindTexture() failed. ", NULL, 0);
		PlatformUtil::DebugOut("[GLESRenderTexture::Create] failed: glBindTexture() failed. \n");
		return false;
	}

	_PIXELFORMAT fmt = ConvertPixelFormat(format);

	glTexImage2D(GL_TEXTURE_2D, 0, fmt.internalformat, width, height, 0, fmt.format, fmt.type, NULL);
	if (glGetError())
	{
		PlatformUtil::DebugOut("[GLESRenderTexture::Create] failed: glTexImage2D() failed. \n");
		return false;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	m_Width = width;
	m_Height = height;
	m_PixelFormat = format;

	m_MipmapCount = 0;
	m_MipmapLevel = 0;

	m_TextureTarget = GL_TEXTURE_2D;
	return true;
}

void GLESRenderTexture::SetSurface(int channel)
{
	if (m_PixelFormat == RT_DEPTH_BYTE || m_PixelFormat == RT_DEPTH_INT || m_PixelFormat == RT_DEPTH_SHORT)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_Texture, 0);
	else
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_Texture, 0);

	switch (glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		PlatformUtil::DebugOut("[GLESRenderTexture::Create] GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		PlatformUtil::DebugOut("[GLESRenderTexture::Create] GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
		PlatformUtil::DebugOut("[GLESRenderTexture::Create] GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS\n");
		break;
	case GL_FRAMEBUFFER_UNSUPPORTED:
		//텍스쳐의 mipmap이 설정되어 있지 않으면, 발생!!!
		//http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=266099
		PlatformUtil::DebugOut("[GLESRenderTexture::Create] GL_FRAMEBUFFER_UNSUPPORTED\n");
		break;
	}
	_ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}

void GLESRenderTexture::SetTexture()
{
	glBindTexture(m_TextureTarget, m_Texture);
	TestGLError(TEXT("[GLESRenderTexture::SetTexture] texture"));
}
