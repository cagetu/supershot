#include "renderer.h"

/*	거울 변환 (Mirroring Transform)
	오른손 시야 좌표계 (음의 z축 방향으로 내려다보는)를 왼손 정규 장치 좌표계로 변환하는 것
	(Real Time Rendering p.83)
*/
static Mat4 inv = { 1, 0, 0, 0, 
					0, 1, 0, 0, 
					0, 0, -1, 0, 
					0, 0, 0, 1 };

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 구간으로 매핑되는 정규 시야 영역으로 변환
	2*zn/w  0       0				0
	0       2*zn/h  0				0
	0       0      -(zf+zn)/(zf-zn)	-1
	0       0      -2*zf*zn/(zf-zn)	0
*/
//--------------------------------------------------------------
Mat4 GLESTransform::Projection(float nearwidth, float nearheight, float nearclip, float farclip)
{
	// z : [-1, 1]
	float	q = 1.0f / (farclip - nearclip);

	Mat4	m;
	m._11 = 2.0f*nearclip / nearwidth;	m._12 = 0.0f;						m._13 = 0.0f;						m._14 = 0.0f;
	m._21 = 0.0f;						m._22 = 2.0f*nearclip / nearheight;	m._23 = 0.0f;						m._24 = 0.0f;
	m._31 = 0.0f;						m._32 = 0.0f;						m._33 = -(farclip + nearclip) * q;	m._34 = -(farclip*nearclip*2.0f) * q;
	m._41 = 0.0f;						m._42 = 0.0f;						m._43 = -1.0f;						m._44 = 0.0f;

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 구간으로 매핑되는 정규 시야 영역으로 변환
		xScale  0          0				0
		0      yScale      0				0
		0          0      -(zf+zn)/(zf-zn)	-1
		0          0      -2*zn*zf/(zf-zn)	0
	where:
		yScale = cot(fovY/2)
		xScale = aspect ratio / yScale
*/
//--------------------------------------------------------------
Mat4 GLESTransform::ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio)
{
	// z : [-1, 1]
	float	f = 1.0f / (float)tan(fov * 0.5f);
	float	q = 1.0f / (farclip - nearclip);

	Mat4	m;
	m._11 = f*aspectratio; 	m._12 = 0.0f;	m._13 = 0.0f;						m._14 = 0.0f;
	m._21 = 0.0f;			m._22 = f;		m._23 = 0.0f;						m._24 = 0.0f;
	m._31 = 0.0f;			m._32 = 0.0f;	m._33 = -(farclip + nearclip) * q;	m._34 = -(farclip*nearclip*2.0f) * q;
	m._41 = 0.0f;			m._42 = 0.0f;	m._43 = -1.0f;						m._44 = 0.0f;

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 구간으로 매핑되는 정규 시야 영역으로 변환
*/
//--------------------------------------------------------------
Mat4 GLESTransform::ProjectionOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	float	m00 = 2 * zn / (r - l),
			m02 = (r + l) / (r - l),
			m11 = 2 * zn / (t - b),
			m12 = (t + b) / (t - b),
			m22 = -(zf + zn) / (zf - zn),
			m23 = -2 * zf * zn / (zf - zn),
			m32 = -1;

	Mat4 m = {	m00, 0.0, m02, 0.0,
				0.0, m11, m12, 0.0,
				0.0, 0.0, m22, m23,
				0.0, 0.0, m32, 0.0 };

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1-e] 구간으로 매핑되는 정규 시야 영역으로 변환
		xScale  0          0				0
		0      yScale      0				0
		0          0      -(zf+zn)/(zf-zn)	-1
		0          0      -2*zn*zf/(zf-zn)	0
	where:
		yScale = cot(fovY/2)
		xScale = aspect ratio / yScale

	http://www.terathon.com/gdc07_lengyel.pdf
*/
//--------------------------------------------------------------
Mat4 GLESTransform::InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon)
{
	float	wtan = (float)tan(fov * 0.5f);
	float	f = 1.0f / wtan;

	// [-1, 1-e] 로 변환
	Mat4	m;
	m._11 = f / aspectratio;	m._12 = 0.0f;	m._13 = 0.0f;				m._14 = 0.0f;
	m._21 = 0.0f;				m._22 = f;		m._23 = 0.0f;				m._24 = 0.0f;
	m._31 = 0.0f;				m._32 = 0.0f;	m._33 = (epsilon - 1.0f);	m._34 = (epsilon - 2.0f) * nearclip;
	m._41 = 0.0f;				m._42 = 0.0f;	m._43 = -1.0f;				m._44 = 0.0f;

	m = inv * m.Transpose();
	return m;
}


//--------------------------------------------------------------
/*
	2/w     0       0			0
	0       2/h     0			0
	0       0      -2/(zf-zn)	0
	0		0		0			l
*/
Mat4 GLESTransform::Ortho(float width, float height, float nearclip, float farclip)
{
	// z : [-1, 1]
	float q = 1.0f / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f / width;	m._12 = 0.0f;			m._13 = 0.0f;		m._14 = 0.0f;
	m._21 = 0.0f;			m._22 = 2.0f / height;	m._23 = 0.0f;		m._24 = 0.0f;
	m._31 = 0.0f;			m._32 = 0.0f;			m._33 = -2.0f*q;	m._34 = -(farclip + nearclip) * q;
	m._41 = 0.0f;			m._42 = 0.0f;			m._43 = 0.0f;		m._44 = 1.0f;

	m = inv * m.Transpose();
	return m;
}


//--------------------------------------------------------------
/*
		2/(r-l)      0            0				  0
		0            2/(t-b)      0				  0
		0            0           -2/(zf-zn)		  0
	 -(r+l)/(r-l)  -(t+b)/(t-b) -(zn+zf)/(zn-zf)  l
	where:
		l = -w/2,
		r = w/2,
		b = -h/2, and
		t = h/2.
*/
Mat4 GLESTransform::OrthoOffCenter(float l, float r, float b, float t, float nearclip, float farclip)
{
	// z : [-1, 1]
	float q = 1.0f / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f / (r - l);	m._12 = 0.0f;			m._13 = 0.0f;		m._14 = -(r + l) / (r - l);
	m._21 = 0.0f;			m._22 = 2.0f / (t - b);	m._23 = 0.0f;		m._24 = -(t + b) / (t - b);
	m._31 = 0.0f;			m._32 = 0.0f;			m._33 = -2.0f*q;	m._34 = -(farclip + nearclip) * q;
	m._41 = 0.0f;			m._42 = 0.0f;			m._43 = 0.0f;		m._44 = 1.0f;

	m = inv * m.Transpose();
	return m;
}

