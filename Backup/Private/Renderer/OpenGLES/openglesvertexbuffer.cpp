#include "renderer.h"

__ImplementRtti(VisualShock, GLESVertexBuffer, VertexBuffer);

GLESVertexBuffer::GLESVertexBuffer()
	: m_Buffer(0)
	, m_IsVBO(false)
{
}

GLESVertexBuffer::~GLESVertexBuffer()
{
}

bool GLESVertexBuffer::Create(uint32 fvf, int vertexCount)
{
	int stride = VertexDescription::GetStride(fvf);
	int byteSize = stride * vertexCount;

	if (CreateBuffer(byteSize, BUF_Static) == false)
		return false;

	m_FVF = fvf;
	m_Stride = stride;
	m_Length = byteSize;
	m_IsVBO = true;
	return true;
}

bool GLESVertexBuffer::Create(uint32 fvf, int vertexCount, void* vertexData)
{
	if (Create(fvf, vertexCount) == false)
		return false;

	void* ptr = Lock();
	{
		PlatformUtil::Memcopy(ptr, vertexData, m_Length);
	}
	Unlock();

	return true;
}

void GLESVertexBuffer::Destroy()
{
	if (m_BufferUsage&BUF_Static)
	{
		if (m_Buffer != 0)
		{
			glDeleteBuffers(1, &m_Buffer);
		}
	}

	VertexBuffer::Destroy();
}

bool GLESVertexBuffer::CreateBuffer(int byteSize, uint32 usage)
{
	glGenBuffers(1, &m_Buffer);

	m_Length = byteSize;
	m_BufferUsage = usage;

	return m_Buffer == 0 ? false : true;
}

void* GLESVertexBuffer::Lock(int stride, int numVert, int lockFlag)
{
	if (m_BufferUsage&BUF_Static)
	{
		_ASSERT(stride * numVert < m_Length);

		if (m_Buffer != 0 && m_VtxBuf == NULL)
		{
			m_VtxBuf = new unsigned char[m_Length];
		}
		return &m_VtxBuf[m_Offset];
	}
	else if (m_BufferUsage&BUF_Dynamic)
	{

	}
	return NULL;
}

void GLESVertexBuffer::Unlock()
{
	if (m_BufferUsage&BUF_Static)
	{
		// Vertex Buffer Object 생성

		/*	1. DrawIndexed에서 Vertex에 Offset을 맞추기 위해서, VertexBuffer Lock에서 Offset만큼 옮겨서 버퍼에 넣어주도록 했다.
			glBufferData(GL_ARRAY_BUFFER, m_Length, &m_VtxBuf[m_Offset], GL_STATIC_DRAW);
				or
			glBufferData(GL_ARRAY_BUFFER, m_Length-m_Offset, &m_VtxBuf[m_Offset], GL_STATIC_DRAW);

			2. 정상적이라면,
			- glBufferData(GL_ARRAY_BUFFER, m_Length, &m_VtxBuf, GL_STATIC_DRAW); 로 처리하고,
			- DrawElement에서 VetexOffset 만큼 보정하도록 해야 한다.
		*/

		glBindBuffer(GL_ARRAY_BUFFER, m_Buffer);
		glBufferData(GL_ARRAY_BUFFER, m_Length, &m_VtxBuf[m_Offset], GL_STATIC_DRAW);
		TestGLError(TEXT("[CVertexBuffer::Unlock] glBufferData(_STATICBUFFER)"));

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if (m_BufferUsage&BUF_Dynamic)
	{
	}
}

void GLESVertexBuffer::SetStreamSource(int channel, int offset)
{
	_ASSERT(offset == 0);
	glBindBuffer(GL_ARRAY_BUFFER, IsVBO() ? m_Buffer : 0);
	TestGLError(TEXT("[CVertexBuffer::SetStreamSource] glBindBuffer"));
}

unsigned char* GLESVertexBuffer::GetBytes() const
{
	if (m_IsVBO == true)
		return NULL;

	return VertexBuffer::GetBytes();
}
