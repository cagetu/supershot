#include "renderer.h"

//

WindowOpenGLES::WindowOpenGLES()
{
	m_Width = 0;
	m_Height = 0;
}

WindowOpenGLES::~WindowOpenGLES()
{
}

bool WindowOpenGLES::Create(const IDisplayContext* displayHandle)
{
	WindowDisplayContext* windowHandle = (WindowDisplayContext*)displayHandle;

	HDC hDC = GetDC((HWND)windowHandle->GetWindowHandle());
	if (!hDC)
		return false;

	m_ESContext.eglNativeDisplay = hDC;
	m_ESContext.eglNativeWindow = (HWND)windowHandle->GetWindowHandle();
	m_ESContext.eglDisplay = eglGetDisplay(hDC);
	if (m_ESContext.eglDisplay == EGL_NO_DISPLAY)
	{
		m_ESContext.eglDisplay = eglGetDisplay((EGLNativeDisplayType)EGL_DEFAULT_DISPLAY);
	}
	// If a display still couldn't be obtained, return an error.
	if (m_ESContext.eglDisplay == EGL_NO_DISPLAY)
	{
		PlatformUtil::Dialog("Failed to get an EGLDisplay", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	int majorVersion, minorVersion;
	if (!eglInitialize(m_ESContext.eglDisplay, &majorVersion, &minorVersion))
		return false;

	/// 로그를 남기자...

	eglBindAPI(EGL_OPENGL_ES_API);
	TestEGLError(TEXT("eglBindAPI"));

	//
	EGLint glContextRenderableType = EGL_OPENGL_ES2_BIT;
#if _OPENGL_ES3 && defined(EGL_KHR_create_context)
	const char *extensions = eglQueryString(eglDisplay, EGL_EXTENSIONS);

	// check whether EGL_KHR_create_context is in the extension string
	if (extensions != NULL && strstr(extensions, "EGL_KHR_create_context"))
	{
		// extension is supported
		return EGL_OPENGL_ES3_BIT_KHR;
	}
#endif

	const int pi32ConfigAttribs[] =
	{
		EGL_RED_SIZE, 5,
		EGL_GREEN_SIZE, 6,
		EGL_BLUE_SIZE, 5,
		//EGL_ALPHA_SIZE,			8,
		EGL_LEVEL, 0,
		EGL_BUFFER_SIZE, 0,
		EGL_DEPTH_SIZE, 16,
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE, glContextRenderableType,
		EGL_NATIVE_RENDERABLE, EGL_DONT_CARE,
		EGL_NONE
	};

	EGLConfig eglConfig = 0;

	int iConfigs;
	if (!eglChooseConfig(m_ESContext.eglDisplay, pi32ConfigAttribs, &eglConfig, 1, &iConfigs) || (iConfigs != 1))
	{
		TestEGLError(TEXT("eglChooseConfig"));
		return false;
	}

	/// Create Surface

	m_ESContext.eglSurface = eglCreateWindowSurface(m_ESContext.eglDisplay, eglConfig, m_ESContext.eglNativeWindow, NULL);
	if (m_ESContext.eglSurface == EGL_NO_SURFACE)
	{
		eglGetError(); // Clear error
		m_ESContext.eglSurface = eglCreateWindowSurface(m_ESContext.eglDisplay, eglConfig, NULL, NULL);
	}
	if (eglGetError() == 0)
		return false;

	/// Creat Context....
#if _OPENGL_ES3
	int ai32ContextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE };
#endif
#if _OPENGL_ES2
	int ai32ContextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };
#endif

	m_ESContext.eglContext = eglCreateContext(m_ESContext.eglDisplay, eglConfig, NULL, ai32ContextAttribs);
	if (TestEGLError(TEXT("eglCreateContext")) == false)
		return false;

	eglMakeCurrent(m_ESContext.eglDisplay, m_ESContext.eglSurface, m_ESContext.eglSurface, m_ESContext.eglContext);
	if (TestEGLError(TEXT("eglMakeCurrent")) == false)
		return false;

#if 0
	RECT rect;
	GetClientRect(m_ESContext.eglNativeWindow, &rect);

	m_Width = (rect.right - rect.left);
	m_Height = (rect.bottom - rect.top);
#else
	eglQuerySurface(m_ESContext.eglDisplay, m_ESContext.eglSurface, EGL_WIDTH, &m_Width);
	eglQuerySurface(m_ESContext.eglDisplay, m_ESContext.eglSurface, EGL_HEIGHT, &m_Height);
#endif

	OpenGLESDriver::CheckDriverCaps();

	m_RenderState = new GLESRenderState();
	m_RenderState->Reset();

	// 백버퍼 설정 (2013-04-25 : cagetu)
	// 초기화 할 때, BackBuffer를 받아오지만, 엄밀히 말하면 프레임시작할 때, 얻어오는 것이 더 정확하다.
	// 최대한 질의를 하지 않는 편이 좋으니, 일단 초기값을 사용하도록 한다. 
	// 추후 정밀한 backbuffer 값이 필요하다면, viewport::begin에서 값을 얻어서 사용하는 식으로 변경하도록 하자!
	//glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_BackBufferId);
	return true;
}

void WindowOpenGLES::Destroy()
{
	eglSwapBuffers(m_ESContext.eglDisplay, m_ESContext.eglSurface);
	eglMakeCurrent(m_ESContext.eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
	eglDestroyContext(m_ESContext.eglDisplay, m_ESContext.eglContext);
	eglDestroySurface(m_ESContext.eglDisplay, m_ESContext.eglSurface);
	eglTerminate(m_ESContext.eglDisplay);
		
	glFinish();
}

void WindowOpenGLES::Present()
{
	eglSwapBuffers(m_ESContext.eglDisplay, m_ESContext.eglSurface);
}



////

bool TestEGLError(const TCHAR* function)
{
	GLint lastError = eglGetError();
	if (lastError != EGL_SUCCESS)
	{
		String text;

		switch (lastError)
		{
		case EGL_NOT_INITIALIZED:		text = TEXT("EGL_NOT_INITIALIZED");		break;
		case EGL_BAD_ACCESS:			text = TEXT("EGL_BAD_ACCESS");			break;
		case EGL_BAD_ALLOC:				text = TEXT("EGL_BAD_ALLOC");			break;
		case EGL_BAD_ATTRIBUTE:			text = TEXT("EGL_BAD_ATTRIBUTE");		break;
		case EGL_BAD_CONFIG:			text = TEXT("EGL_BAD_CONFIG");			break;
		case EGL_BAD_CONTEXT:			text = TEXT("EGL_BAD_CONTEXT");			break;
		case EGL_BAD_CURRENT_SURFACE:	text = TEXT("EGL_BAD_CURRENT_SURFACE");	break;
		case EGL_BAD_DISPLAY:			text = TEXT("EGL_BAD_DISPLAY");			break;
		case EGL_BAD_MATCH:				text = TEXT("EGL_BAD_MATCH");			break;
		case EGL_BAD_NATIVE_PIXMAP:		text = TEXT("EGL_BAD_NATIVE_PIXMAP");	break;
		case EGL_BAD_NATIVE_WINDOW:		text = TEXT("EGL_BAD_NATIVE_WINDOW");	break;
		case EGL_BAD_PARAMETER:			text = TEXT("EGL_BAD_PARAMETER");		break;
		case EGL_BAD_SURFACE:			text = TEXT("EGL_BAD_SURFACE");			break;
		case EGL_CONTEXT_LOST:			text = TEXT("EGL_CONTEXT_LOST");			break;
		};

		TCHAR temp[256];
		unicode::format(temp, TEXT("%s failed (%s).\n"), function, text.c_str());
		PlatformUtil::Dialog(temp, TEXT("[OpenGL Driver Error]"), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	return true;
}

static WindowOpenGLES* g_RenderDevice = nullptr;

IRenderDriver* CreateRenderDriver()
{
	if (g_RenderDevice == nullptr)
	{
		g_RenderDevice = new WindowOpenGLES();
	}
	return g_RenderDevice;
}

void DestroyRenderDriver()
{
	if (g_RenderDevice != nullptr)
	{
		delete g_RenderDevice;
		g_RenderDevice = nullptr;
	}
}

IRenderDriver* GetRenderDriver()
{
	return g_RenderDevice;
}


