#include "renderer.h"

#define PVRGetProcAddress(x) eglGetProcAddress(#x)

PFNGLMULTIDRAWELEMENTS glMultiDrawElementsEXT = NULL;
PFNGLMULTIDRAWARRAYS glMultiDrawArraysEXT = NULL;
PFNGLMAPBUFFEROES glMapBufferOES = NULL;
PFNGLUNMAPBUFFEROES glUnmapBufferOES = NULL;
PFNGLGETBUFFERPOINTERVOES glGetBufferPointervOES = NULL;
PFNGLDISCARDFRAMEBUFFEREXT glDiscardFramebufferEXT = NULL;
PFNGLRENDERBUFFERSTORAGEMULTISAMPLEAPPLE glRenderbufferStorageMultisampleAPPLE = NULL;
PFNGLRESOLVEMULTISAMPLEFRAMEBUFFERAPPLE glResolveMultisampleFramebufferAPPLE = NULL;
PFNGLGETPROGRAMBINARYOESPROC glGetProgramBinaryOES = NULL;
PFNGLPROGRAMBINARYOESPROC glProgramBinaryOES = NULL;
PFNGLBINDVERTEXARRAYOES	glBindVertexArrayOES = NULL;
PFNGLDELETEVERTEXARRAYSOES glDeleteVertexArraysOES = NULL;
PFNGLGENVERTEXARRAYSOES	glGenVertexArraysOES = NULL;

void WindowOpenGLESCaps::Setup()
{
	// Supported extensions provide new entry points for OpenGL ES 2.0.

	const GLubyte *pszGLExtensions;

	/* Retrieve GL extension string */
	pszGLExtensions = glGetString(GL_EXTENSIONS);

	/* GL_EXT_multi_draw_arrays */
	if (strstr((char *)pszGLExtensions, "GL_EXT_multi_draw_arrays"))
	{
		glMultiDrawElementsEXT = (PFNGLMULTIDRAWELEMENTS)PVRGetProcAddress(glMultiDrawElementsEXT);
		glMultiDrawArraysEXT = (PFNGLMULTIDRAWARRAYS)PVRGetProcAddress(glMultiDrawArraysEXT);
	}

	/* EXP_map_buffer_range */
	if (strstr((char *)pszGLExtensions, "EXP_map_buffer_range"))
	{
	}

	/* GL_OES_mapbuffer */
	if (strstr((char *)pszGLExtensions, "GL_OES_mapbuffer"))
	{
		glMapBufferOES = (PFNGLMAPBUFFEROES)PVRGetProcAddress(glMapBufferOES);
		glUnmapBufferOES = (PFNGLUNMAPBUFFEROES)PVRGetProcAddress(glUnmapBufferOES);
		glGetBufferPointervOES = (PFNGLGETBUFFERPOINTERVOES)PVRGetProcAddress(glGetBufferPointervOES);
	}

	/* GL_OES_vertex_array_object */
	if (strstr((char *)pszGLExtensions, "GL_OES_vertex_array_object"))
	{
		glBindVertexArrayOES = (PFNGLBINDVERTEXARRAYOES)PVRGetProcAddress(glBindVertexArrayOES);
		glDeleteVertexArraysOES = (PFNGLDELETEVERTEXARRAYSOES)PVRGetProcAddress(glDeleteVertexArraysOES);
		glGenVertexArraysOES = (PFNGLGENVERTEXARRAYSOES)PVRGetProcAddress(glGenVertexArraysOES);
	}

	//wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)PVRGetProcAddress("wglChoosePixelFormatARB");

#if (GL_EXT_discard_framebuffer)
	/* GL_EXT_discard_framebuffer */
	if (strstr((char *)pszGLExtensions, "GL_EXT_discard_framebuffer"))
	{
		glDiscardFramebufferEXT = (PFNGLDISCARDFRAMEBUFFEREXT)PVRGetProcAddress(glDiscardFramebufferEXT);
	}
#endif

#if (GL_APPLE_framebuffer_multisample)
	/* GL_APPLE_framebuffer_multisample */
	if (strstr((char *)pszGLExtensions, "GL_APPLE_framebuffer_multisample"))
	{
		glRenderbufferStorageMultisampleAPPLE = (PFNGLRENDERBUFFERSTORAGEMULTISAMPLEAPPLE)PVRGetProcAddress(glRenderbufferStorageMultisampleAPPLE);
		glResolveMultisampleFramebufferAPPLE = (PFNGLRESOLVEMULTISAMPLEFRAMEBUFFERAPPLE)PVRGetProcAddress(glResolveMultisampleFramebufferAPPLE);
	}
#endif

	// Retrieves the functions needed to use the extension.
	if (IsGLExtensionSupported("GL_OES_get_program_binary"))
	{
		glGetProgramBinaryOES = (PFNGLGETPROGRAMBINARYOESPROC)PVRGetProcAddress(glGetProgramBinaryOES);
		glProgramBinaryOES = (PFNGLPROGRAMBINARYOESPROC)PVRGetProcAddress(glProgramBinaryOES);
	}

	CheckDriverCaps();
}