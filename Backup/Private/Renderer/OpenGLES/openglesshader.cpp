#include "renderer.h"

__ImplementRtti(VisualShock, GLESShader, IShader);

GLESShader::GLESShader()
	: m_Program(0)
	, m_ShaderParam(NULL)
	, m_VertexBuffer(NULL)
	, m_Material(NULL)
{
}

GLESShader::~GLESShader()
{
	_ASSERT(m_Program == 0);
}

bool GLESShader::Create(const SHADERCODE& shaderCode, ShaderFeature::MASK mask)
{
	///// Create Program
	_ASSERT(m_Program == 0);

	m_Program = glCreateProgram();
	if (TestGLError(TEXT("[GLESShader::CreateShader] glCreateProgram")) == false)
		return false;

	for (auto pair : shaderCode.shaderId)
	{
		const ShaderType::TYPE type = pair.first;
		const String filename = pair.second;

		GLESShaderResource* shaderResource = (GLESShaderResource*)ShaderCore::Build(filename.c_str(), type, mask);
		glAttachShader(m_Program, shaderResource->GetShaderHandle());
	}

	// Init Shader Parameters
	if (Initialize() == false)
		return false;

	return true;
}

void GLESShader::Destroy()
{
	if (m_ShaderParam)
	{
		delete m_ShaderParam;
		m_ShaderParam = NULL;
	}

	if (m_Program != 0)
	{
		GLint Deleted;
		glGetProgramiv(m_Program, GL_DELETE_STATUS, &Deleted);
		if (Deleted == GL_FALSE)
		{
			glDeleteProgram(m_Program);
		}
		m_Program = 0;
	}
}

void GLESShader::Begin(const CMaterial* material, const VertexBuffer* vb)
{

	// 현재 활성화된 셰이더가 같은 것이라면, 다시 이렇게 설정해줄 필요가 없다.
	/* 이렇게 하는 것보다 차라리 그냥 ShaderManager ShaderCore에서 Begin(IShader* shader) 같은 형태가 더 나을 것 같기도 하다.
	*/
	//glUseProgram(m_Program);
	//TestGLError(TEXT("glUseProgram"));
	//UpdateVertexAttributes(vb);

	GetRenderDriver()->SetShader(this, vb, material);
}

void GLESShader::End()
{
	// 현재 활성화된 셰이더가 같은 것이라면, 다시 이렇게 설정해줄 필요가 없다.
	//ClearAttributes();
	//glUseProgram(0);
}

bool GLESShader::Initialize()
{
	_ASSERT(m_Program != 0);

	// 셰이더 링크 (이제 조회가 가능하다.)
	glLinkProgram(m_Program);

	GLint Linked;
	glGetProgramiv(m_Program, GL_LINK_STATUS, &Linked);
	if (!Linked)
	{
		int i32InfoLogLength, i32CharsWritten;
		glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &i32InfoLogLength);
		char* pszInfoLog = new char[i32InfoLogLength];
		glGetProgramInfoLog(m_Program, i32InfoLogLength, &i32CharsWritten, pszInfoLog);

		PlatformUtil::DebugOutFormat("Shader Link Fail: %s\n", pszInfoLog);

		delete[] pszInfoLog;
		return false;
	}

	// Setup Vertex Format
	{
		VertexDescription::GetDesc(m_Program, &m_VertexDesc);
	}
	
	// Setup Uniforms
	m_ShaderParam = new GLESShaderProgram(ShaderType::VERTEX_BIT | ShaderType::FRAGMENT_BIT);
	m_ShaderParam->Initialize(m_Program);
	return true;
}

void GLESShader::UpdateVertexAttributes(const VertexBuffer* vertexBuffer)
{
	if (m_VertexBuffer != buffer)
	{
		int32 pitch = buffer->GetStride();
		uint32 fvf = buffer->GetFVF();
		unsigned char* bytes = buffer->GetBytes();

		BindShaderVertexDesc(m_VertexDesc, fvf, pitch, bytes);

		m_VertexBuffer = vertexBuffer;
	}
}

void GLESShader::ClearVertexAttributes()
{
	UnbindShaderVertexDesc(m_VertexDesc);
	m_VertexBuffer = NULL;
}

void GLESShader::SetValue(uint32 index, const Variant& value)
{
	m_ShaderParam->SetValue(index, value);
}

void GLESShader::ClearValues()
{
	m_ShaderParam->ClearValues();

	m_Material = NULL;
}

void GLESShader::UpdateValues(const CMaterial* material)
{
	//if (m_Material == material)
	//	return;

	m_Material = material;

	m_ShaderParam->UpdateValues(material);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, GLESShaderProgram, IShaderProgram);

GLESShaderProgram::GLESShaderProgram(uint32 shaderTypes)
	: IShaderProgram(shaderTypes)
	, m_Uniforms(NULL)
	, m_UniformCount(0)
{
}
GLESShaderProgram::~GLESShaderProgram()
{
	if (m_Uniforms != NULL)
	{
		delete[] m_Uniforms;
		m_Uniforms = NULL;
	}
	m_UniformCount = 0;
}

bool IsTextureType(GLuint type)
{
	if (type == GL_SAMPLER_2D || type == GL_SAMPLER_CUBE)
		return true;

	return false;
}

bool GLESShaderProgram::Initialize(GLuint program)
{
	int uniformCount = 0;
	int uniformMaxLength = 0;
	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &uniformCount);
	glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &uniformMaxLength);

	char* name = new char[(sizeof(char)* uniformMaxLength)];

	int size = 0, length = 0, textureId = 0;
	unsigned int type = 0;

	std::vector <Uniform> _list;

	for (int i = 0; i < uniformCount; i++)
	{
		glGetActiveUniform(program, i, uniformMaxLength, &length, &size, &type, name);

		GLint location = glGetUniformLocation(program, name);
		_ASSERT(location >= 0);

		// 
		String sementic;

		if (size > 1)	// Array
		{
			// OpenGL ES의 경우, 배열은 [0]이 붙는다. ex)mat4 TMARRAY[_MAX_BONES] -> TMARRAY[0]
			TCHAR temp[64];
			string::conv_s(temp, _countof(temp), name);
			const TCHAR* t = string::strstr(temp, TEXT("[0]"));

			sementic = (t != NULL) ? String(temp, t - temp) : temp;
		}
		else
		{
			sementic = name;
		}

		Uniform uniform;
		uniform.location = location;
		uniform.offset = IsTextureType(type) ? textureId++ : 0;
		uniform.uid = ShaderParameter::Register(sementic);

		_list.push_back(uniform);
	}
	delete[] name;

	m_Uniforms = uniformCount > 0 ? new Uniform[uniformCount] : NULL;
	m_UniformCount = uniformCount;

	for (int i = 0; i < uniformCount; i++)
	{
		m_Uniforms[i].location = _list[i].location;
		m_Uniforms[i].offset = _list[i].offset;
		m_Uniforms[i].uid = _list[i].uid;
		m_Uniforms[i].value = Variant();
	}
	return true;
}

void GLESShaderProgram::SetUniformValue(GLuint location, GLuint offset, const Variant& value)
{
	switch (value.type())
	{
	case Variant::_Void:
		break;
	case Variant::_Int:
		glUniform1i(location, value);
		break;
	case Variant::_Float:
		glUniform1f(location, value);
		break;
	case Variant::_Bool:
		//glUniform1f(location, value);
		_ASSERT(0);
		break;
	case Variant::_Vector2:
		glUniform2fv(location, 1, (float*)&((const Vector2&)value).x);
		break;
	case Variant::_Vector3:
		glUniform3fv(location, 1, (float*)&((const Vector3&)value).x);
		break;
	case Variant::_Vector4:
		glUniform4fv(location, 1, (float*)&((const Vector4&)value).x);
		break;
	case Variant::_Matrix3:
		glUniformMatrix3fv(location, 1, 0, (float*)&((const Mat3&)value)._11);
		break;
	case Variant::_Matrix4:
		glUniformMatrix4fv(location, 1, 0, (float*)&((const Mat4&)value)._11);
		break;
	case Variant::_Texture:
	{
		ITexture * tex = (ITexture*)value;
		if (tex != NULL)
		{
			glUniform1i(location, offset);
			glActiveTexture(GL_TEXTURE0 + offset);

			tex->SetTexture();
#if 0
			// 혹시 텍스쳐 바인딩이 불필요하게 많이 나오는 건 아닐까 테스트..
			//if (lasttexture == tex->GetTextureHandle() || lastoffset == offset)
			{
				//util::DebugOut(L"[Warning] Texture Binding is Skipppable!!!!! \n");
			}
#endif
		}
		else
		{
			glUniform1i(location, offset);
			glActiveTexture(GL_TEXTURE0 + offset);
			glBindTexture(GL_TEXTURE_2D, 0);

			//lasttexture = (unsigned int)-1;
			//lastoffset = (unsigned int)-1;
		}
		break;
	}
	case Variant::_Float_Array:
		glUniform1fv(location, value.count(), value.GetFloatArray());
		break;
	case Variant::_Vector2_Array:
		glUniform2fv(location, value.count(), &value.GetVector2Array()[0].x);
		break;
	case Variant::_Vector3_Array:
		glUniform3fv(location, value.count(), &value.GetVector3Array()[0].x);
		break;
	case Variant::_Vector4_Array:
		glUniform4fv(location, value.count(), &value.GetVector4Array()[0].x);
		break;
	case Variant::_Matrix_Array:
		glUniformMatrix4fv(location, value.count(), 0, &value.GetMatrixArray()[0]._11);
		break;
	default:
		_ASSERT(0);
		break;
	}
}

void GLESShaderProgram::SetValue(uint index, const Variant& value)
{
	Uniform* uniform = &m_Uniforms[index];

	// 1. 중복 검사
	if (uniform->value == value)
		return;

	SetUniformValue(uniform->location, uniform->offset, value);

	if (value.type() == Variant::_Texture)
	{
		ITexture* texture = static_cast<ITexture*>(value);
		if (texture == NULL)
		{
			uniform->value.clear();
			return;
		}
	}

	uniform->value = value;
}

void GLESShaderProgram::UpdateValues(const CMaterial* material)
{
	const int uniformCount = m_UniformCount;
	Uniform* uniformData = m_Uniforms;

	Variant value;

	for (int32 index = 0; index < uniformCount; index++)
	{
		ShaderParameter::PID pid = uniformData[index].uid;

		if (pid < ShaderParameter::_MAX_RESERVED_PARAM)		// 엔진 내부 예약된 변수
		{
			if (material && material->GetValue(pid, value) == true)
			{
				SetValue(index, value);
			}
			else
			{
				SetValue(index, ShaderParameter::GetCommonValue(pid));
			}
		}
		else if (pid >= ShaderParameter::_MAX_PARAMCOUNT)	// 머터리얼 변수
		{
			if (material && material->GetValue(pid, value) == true)
				SetValue(index, value);
		}
		else if (pid < ShaderParameter::_MAX_PARAMCOUNT)	// 엔진 내부 계산되는 변수
		{
			UpdateInternal(index, pid);;
		}
	}
}

void GLESShaderProgram::ClearValues()
{
	for (int32 index = 0; index < m_UniformCount; index++)
	{
		//if (m_Uniforms[index].value.type() == Variant::_Texture)
		m_Uniforms[index].value.clear();
	}
}

///////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, GLESShaderResource, IShaderResource);

GLESShaderResource::GLESShaderResource()
	: m_ShaderHandle(0)
{
}

GLESShaderResource::~GLESShaderResource()
{
	Destroy();
}

GLuint GLShaderType(ShaderType::TYPE type)
{
	switch (type)
	{
	case ShaderType::VERTEXSHADER:		return GL_VERTEX_SHADER;
	case ShaderType::FRAGMENTSHADER:	return GL_FRAGMENT_SHADER;
	}

	_ASSERT(0);
	return 0;
}

std::string GLShaderTypeDesc(ShaderType::TYPE type)
{
	switch (type)
	{
	case ShaderType::VERTEXSHADER:		return "VertexShader";
	case ShaderType::FRAGMENTSHADER:	return "FragmentShader";
	}

	_ASSERT(0);
	return "";
}

bool GLESShaderResource::Create(ICompiledShaderCode* code, ShaderType::TYPE type)
{
	GLESCompiledShaderCode* shaderCode = (GLESCompiledShaderCode*)code;

	const char* codeBytes = shaderCode->code.c_str();
	uint32 codeSize = shaderCode->code.size();

	GLuint shader = glCreateShader(GLShaderType(type));
	if (TestGLError(TEXT("[GLESShaderResource::CreateShader] glCreateShader")) == false)
		return false;

	glShaderSource(shader, 1, &codeBytes, 0);
	glCompileShader(shader);
	int check_compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &check_compiled);
	if (!check_compiled)
	{
		PlatformUtil::DebugOut("[GLESShaderResource::CreateShader] compile failed :");

		int infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
		if (infoLen > 1)
		{
			char * log = (char *)malloc(sizeof(char) * infoLen);
			glGetShaderInfoLog(shader, infoLen, 0, log);

			std::string out = GLShaderTypeDesc(type);
			out += log;

			PlatformUtil::DebugOutFormat("%s\n", out.c_str());
			PlatformUtil::Dialog(out.c_str(), NULL, 0);
			free(log);
		}
		glDeleteShader(shader);
		return false;
	}

	m_ShaderType = type;
	m_ShaderHandle = shader;
	return true;
}

void GLESShaderResource::Destroy()
{
	if (m_ShaderHandle > 0)
	{
		glDeleteShader(m_ShaderHandle);
	}
	m_ShaderHandle = 0;
}

///////////////////////////////////////////////////////////////////

IShaderResource* CreateShaderResource(ICompiledShaderCode* code, ShaderType::TYPE type)
{
	GLESShaderResource* IShaderResource = new GLESShaderResource();
	if (IShaderResource->Create(code, type) == true)
		return IShaderResource;
	
	delete IShaderResource;
	return NULL;
}