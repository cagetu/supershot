#include "renderer.h"

/// Global Variables

int COpenGLESCaps::MaxVertexAttribs = 0;
int COpenGLESCaps::MaxCombinedTextureImageUnits = 0;
int COpenGLESCaps::MaxVertexTextureImageUnits = 0;
int COpenGLESCaps::MaxGeometryTextureImageUnits = 0;
int COpenGLESCaps::MaxHullTextureImageUnits = 0;
int COpenGLESCaps::MaxDomainTextureImageUnits = 0;
int COpenGLESCaps::MaxVertexUniformComponents = 0;
int COpenGLESCaps::MaxPixelUniformComponents = 0;
int COpenGLESCaps::MaxGeometryUniformComponents = 0;
int COpenGLESCaps::MaxHullUniformComponents = 0;
int COpenGLESCaps::MaxDomainUniformComponents = 0;
int COpenGLESCaps::MaxVaryingVectors = 0;

bool COpenGLESCaps::bSupportMapBuffer = false;
bool COpenGLESCaps::bSupportDepthTexture = false;
bool COpenGLESCaps::bSupportOcclusionQueries = false;
bool COpenGLESCaps::bSupportDisjointTimeQueries = false;
bool COpenGLESCaps::bTimerQueryCanBeDisjoint = true;
bool COpenGLESCaps::bSupportRGBA8 = false;
bool COpenGLESCaps::bSupportBGRA8888 = false;
bool COpenGLESCaps::bSupportBGRA8888RenderTarget = false;

bool COpenGLESCaps::bSupportVertexHalfFloat = false;
bool COpenGLESCaps::bSupportTextureFloat = false;
bool COpenGLESCaps::bSupportTextureHalfFloat = false;
bool COpenGLESCaps::bSupportSGRB = false;
bool COpenGLESCaps::bSupportMultisampledRenderToTexture = false;
bool COpenGLESCaps::bSupportColorBufferFloat = false;
bool COpenGLESCaps::bSupportColorBufferHalfFloat = false;
bool COpenGLESCaps::bSupportShaderFramebufferFetch = false;
bool COpenGLESCaps::bSupportShaderDepthStencilFetch = false;
bool COpenGLESCaps::bSupportDXT = false;
bool COpenGLESCaps::bSupportPVRTC = false;
bool COpenGLESCaps::bSupportATITC = false;
bool COpenGLESCaps::bSupportETC1 = false;
bool COpenGLESCaps::bSupportVertexArrayObjects = false;
bool COpenGLESCaps::bSupportDiscardFrameBuffer = false;
bool COpenGLESCaps::bSupportNVFrameBufferBlit = false;
bool COpenGLESCaps::bSupportPackedDepthStencil = false;
bool COpenGLESCaps::bSupportShaderTextureLod = false;
bool COpenGLESCaps::bSupportTextureStorageEXT = false;
bool COpenGLESCaps::bSupportCopyTextureLevels = false;
bool COpenGLESCaps::bSupportTextureNPOT = false;
bool COpenGLESCaps::bSupportRGB10A2 = false;
bool COpenGLESCaps::bSupportStandardDerivativesExtension = false;

int COpenGLESCaps::ShaderLowPrecision = 0;
int COpenGLESCaps::ShaderMediumPrecision = 0;
int COpenGLESCaps::ShaderHighPrecision = 0;

/// 

bool COpenGLESCaps::IsGLExtensionSupported(const char* extension)
{
	// The recommended technique for querying OpenGL extensions;
	// from http://opengl.org/resources/features/OGLextensions/
	const GLubyte *extensions = NULL;
	const GLubyte *start;
	GLubyte *where, *terminator;

	/* Extension names should not have spaces. */
	where = (GLubyte *) strchr(extension, ' ');
	if (where || *extension == '\0')
		return 0;

	extensions = glGetString(GL_EXTENSIONS);

	/* It takes a bit of care to be fool-proof about parsing the
	OpenGL extensions string. Don't be fooled by sub-strings, etc. */
	start = extensions;
	for (;;) {
		where = (GLubyte *) strstr((const char *) start, extension);
		if (!where)
			break;
		terminator = where + strlen(extension);
		if (where == start || *(where - 1) == ' ')
			if (*terminator == ' ' || *terminator == '\0')
				return true;
		start = terminator;
	}

	return false;
}

void COpenGLESCaps::CheckDriverCaps()
{
	PlatformUtil::DebugOut("\n***************************[Device Info]*********************************\n");

	const GLubyte* vender = glGetString(GL_VENDOR);
	PlatformUtil::DebugOutFormat("[Driver Vender] %s\n", (const char*)vender);

	const GLubyte* renderer = glGetString(GL_RENDERER);
	PlatformUtil::DebugOutFormat("[Driver Renderer] %s\n", (const char*)renderer);

	const GLubyte* glslVer = glGetString(GL_SHADING_LANGUAGE_VERSION);
	PlatformUtil::DebugOutFormat("[GLSL Version] %s\n", (const char*)glslVer);

	const GLubyte* esVer = glGetString(GL_VERSION);
	PlatformUtil::DebugOutFormat("[OpenGL ES Version] %s\n", (const char*)esVer);

	GLboolean shadercompilerSupport;
	glGetBooleanv(GL_SHADER_COMPILER, &shadercompilerSupport);
	_ASSERT(shadercompilerSupport == GL_TRUE);

	// 바이너리 형식이 사용 가능한지 결정
	int numbinaryformat;
	glGetIntegerv(GL_NUM_SHADER_BINARY_FORMATS, &numbinaryformat);
	if (numbinaryformat > 0)
	{
		int* binaryformats = new int[numbinaryformat];

		// formats은 지원된 바이너리 형식을 얻어온다.
		glGetIntegerv(GL_SHADER_BINARY_FORMATS, binaryformats);

		PlatformUtil::DebugOut("glShaderBinary Support!\n");
		for (int i = 0; i<numbinaryformat; i++)
			PlatformUtil::DebugOutFormat("\tformat : %d\n", binaryformats[i]);

		delete[] binaryformats;
	}

	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &MaxVertexAttribs);
	_ASSERT(MaxVertexAttribs <= 16);
	glGetIntegerv(GL_MAX_VARYING_VECTORS, &MaxVaryingVectors);
	glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &MaxVertexUniformComponents);
	glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &MaxPixelUniformComponents);

	bSupportMapBuffer = IsGLExtensionSupported("GL_OES_mapbuffer");
	bSupportDepthTexture = IsGLExtensionSupported("GL_OES_depth_texture");
	bSupportOcclusionQueries = IsGLExtensionSupported("GL_ARB_occlusion_query2") || IsGLExtensionSupported("GL_EXT_occlusion_query_boolean");
	bSupportDisjointTimeQueries = IsGLExtensionSupported("GL_EXT_disjoint_timer_query") || IsGLExtensionSupported("GL_NV_timer_query");
	bTimerQueryCanBeDisjoint = !IsGLExtensionSupported("GL_NV_timer_query");
	bSupportRGBA8 = IsGLExtensionSupported("GL_OES_rgb8_rgba8");
	bSupportBGRA8888 = IsGLExtensionSupported("GL_APPLE_texture_format_BGRA8888") || IsGLExtensionSupported("GL_IMG_texture_format_BGRA8888") || IsGLExtensionSupported("GL_EXT_texture_format_BGRA8888");
	bSupportBGRA8888RenderTarget = bSupportBGRA8888;

	bSupportVertexHalfFloat = IsGLExtensionSupported("GL_OES_vertex_half_float");
	bSupportTextureFloat = IsGLExtensionSupported("GL_OES_texture_float");
	bSupportTextureHalfFloat = IsGLExtensionSupported("GL_OES_texture_half_float");
	bSupportSGRB = IsGLExtensionSupported("GL_EXT_sRGB");

	bSupportColorBufferFloat = IsGLExtensionSupported("GL_EXT_color_buffer_float");
	bSupportColorBufferHalfFloat = IsGLExtensionSupported("GL_EXT_Color_buffer_half_float");

	bSupportShaderFramebufferFetch = IsGLExtensionSupported("GL_EXT_shader_framebuffer_fetch") || IsGLExtensionSupported("GL_NV_shader_framebuffer_fetch") || IsGLExtensionSupported("GL_ARM_shader_framebuffer_fetch");
	bSupportShaderDepthStencilFetch = IsGLExtensionSupported("GL_ARM_shader_framebuffer_fetch_depth_stencil");
	bSupportMultisampledRenderToTexture = IsGLExtensionSupported("GL_EXT_multisampled_render_to_texture");

	bSupportDXT = IsGLExtensionSupported("GL_NV_texture_compression_s3tc") || IsGLExtensionSupported("GL_EXT_texture_compression_s3tc");
	bSupportPVRTC = IsGLExtensionSupported("GL_IMG_texture_compression_pvrtc");
	bSupportATITC = IsGLExtensionSupported("GL_ATI_texture_compression_atitc") || IsGLExtensionSupported("GL_AMD_compressed_ATC_texture");
	bSupportETC1 = IsGLExtensionSupported("GL_OES_compressed_ETC1_RGB8_texture");
	bSupportVertexArrayObjects = IsGLExtensionSupported("GL_OES_vertex_array_object");
	bSupportDiscardFrameBuffer = IsGLExtensionSupported("GL_EXT_discard_framebuffer");
	bSupportNVFrameBufferBlit = IsGLExtensionSupported("GL_NV_framebuffer_blit");
	bSupportPackedDepthStencil = IsGLExtensionSupported("GL_OES_packed_depth_stencil");
	bSupportShaderTextureLod = IsGLExtensionSupported("GL_EXT_shader_texture_lod");
	bSupportTextureStorageEXT = IsGLExtensionSupported("GL_EXT_texture_storage");
	bSupportCopyTextureLevels = bSupportTextureStorageEXT && IsGLExtensionSupported("GL_APPLE_copy_texture_levels");
	bSupportTextureNPOT = IsGLExtensionSupported("GL_OES_texture_npot") || IsGLExtensionSupported("GL_ARB_texture_non_power_of_two");
	bSupportRGB10A2 = IsGLExtensionSupported("GL_OES_vertex_type_10_10_10_2");
	bSupportStandardDerivativesExtension = IsGLExtensionSupported("GL_OES_standard_derivatives");
	if (!bSupportStandardDerivativesExtension)
	{
		PlatformUtil::DebugOut("GL_OES_standard_derivatives not supported. There may be rendering errors if materials depend on dFdx, dFdy, or fwidth.");
	}

	// @todo ios7: SRGB support does not work with our texture format setup (ES2 docs indicate that internalFormat and format must match, but they don't at all with sRGB enabled)
	//             One possible solution us to use GLFormat.InternalFormat[bSRGB] instead of GLFormat.Format
	bSupportSGRB = false;//ExtensionsString.Contains(TEXT("GL_EXT_sRGB"));

	// Report shader precision
	int Range[2];
	glGetShaderPrecisionFormat(GL_FRAGMENT_SHADER, GL_LOW_FLOAT, Range, &ShaderLowPrecision);
	glGetShaderPrecisionFormat(GL_FRAGMENT_SHADER, GL_MEDIUM_FLOAT, Range, &ShaderMediumPrecision);
	glGetShaderPrecisionFormat(GL_FRAGMENT_SHADER, GL_HIGH_FLOAT, Range, &ShaderHighPrecision);
	PlatformUtil::DebugOutFormat("Fragment shader lowp precision: %d", ShaderLowPrecision);
	PlatformUtil::DebugOutFormat("Fragment shader mediump precision: %d", ShaderMediumPrecision);
	PlatformUtil::DebugOutFormat("Fragment shader highp precision: %d", ShaderHighPrecision);

	PlatformUtil::DebugOut("\n***************************[Device Info]*********************************\n");
}