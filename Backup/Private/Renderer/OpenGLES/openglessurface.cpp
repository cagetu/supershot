#include "renderer.h"

//
//
//
__ImplementRtti(VisualShock, GLESRenderTargetSurface, GLESRenderTexture);
//
GLESRenderTargetSurface::GLESRenderTargetSurface()
{
}
GLESRenderTargetSurface::~GLESRenderTargetSurface()
{
}

//
//
//
__ImplementRtti(VisualShock, GLESDepthStencilSurface, IDepthStencilSurface);
//
GLESDepthStencilSurface::GLESDepthStencilSurface()
{
}
GLESDepthStencilSurface::~GLESDepthStencilSurface()
{
	if (m_DepthBuffer != 0)
	{
		glDeleteRenderbuffers(1, &m_DepthBuffer);
		m_DepthBuffer = 0;
	}
}

bool GLESDepthStencilSurface::Create(int width, int height, int format, int samples, RTLoadAction loadAction, RTStoreAction storeAction, RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction)
{
	//<@ 깊이 버퍼는 별도로 생성해서 관리하는 것이 좋겠다. DepthBuffer 클래스를 별도로 만들자..
	GLuint depthbuffer;
	glGenRenderbuffers(1, &depthbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer);

	if (samples > 1 && glRenderbufferStorageMultisampleAPPLE != NULL)
		glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, samples, GL_DEPTH_COMPONENT16, width, height);
	else
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);

	m_Width = width;
	m_Height = height;
	m_DepthBuffer = depthbuffer;
	m_MultiSamples = samples;

	m_LoadAction = loadAction;
	m_StoreAction = storeAction;
	m_StencilLoadAction = stencilLoadAction;
	m_StencilStoreAction = stencilStoreAction;

	return true;
}

void GLESDepthStencilSurface::Destroy()
{
	if (m_DepthBuffer != 0)
	{
		glDeleteRenderbuffers(1, &m_DepthBuffer);
		m_DepthBuffer = 0;
	}
}

void GLESDepthStencilSurface::SetSurface()
{
	if (m_DepthBuffer == 0)
		return;

	// 깊이 버퍼 바인딩
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_DepthBuffer);
	switch (glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		PlatformUtil::DebugOut("[GLESDepthStencilSurface::Bind] GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		PlatformUtil::DebugOut("[GLESDepthStencilSurface::Bind] GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
		break;
	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
		PlatformUtil::DebugOut("[GLESDepthStencilSurface::Bind] GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS\n");
		break;
	case GL_FRAMEBUFFER_UNSUPPORTED:
		//텍스쳐의 mipmap이 설정되어 있지 않으면, 발생!!!
		//http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=266099
		PlatformUtil::DebugOut("[GLESDepthStencilSurface::Bind] GL_FRAMEBUFFER_UNSUPPORTED\n");
		break;
	}
	_ASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
}