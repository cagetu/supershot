#include "renderer.h"

VertexStreamDesc::VertexStreamDesc(int32 format)
	: elements(format)
{
	stride = VertexDescription::GetStride(elements);
}

bool VertexStreamDesc::Contains(int32 element) const
{
	return (element == (elements & element));
}

int32 VertexStreamDesc::GetStride() const
{
	return stride;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

int32 IVertexDescription::GetOffsets(uint32 fvf, int32 off[16])
{
	memset(off, 0, sizeof(int) * 16);

	int32 offset = 0;

	if (fvf&FVF_XYZ)
	{
		off[0] = 0;
		offset += sizeof(float) * 3;
	}
	else if (fvf&FVF_XYZRHW)
	{
		off[0] = 0;
		offset += sizeof(float) * 4;
	}

	if (fvf&FVF_NORMAL)
	{
		off[1] = offset;
		offset += (fvf&FVF_NORMAL_PACKED) ? sizeof(unsigned char) * 4 : sizeof(float) * 3;
	}

	if (fvf&FVF_SKIN_INDEX)
	{
		off[2] = offset;
		offset += sizeof(unsigned char) * 4;
	}
	else if (fvf&FVF_DIFFUSE)
	{
		off[2] = offset;
		offset += sizeof(uint32);
	}

	if (fvf&FVF_SKIN_WEIGHT)
	{
		off[3] = offset;
		offset += sizeof(unsigned char) * 4;
	}

	if (fvf&FVF_TEX0)
	{
		off[4] = offset;
		offset += (fvf&FVF_TEX0_PACKED) ? sizeof(short) * 2 : sizeof(float) * 2;
	}

	if (fvf&FVF_TEX1)
	{
		off[5] = offset;
		offset += (fvf&FVF_TEX1_PACKED) ? sizeof(short) * 2 : sizeof(float) * 2;
	}

	if (fvf&FVF_TANSPC)
	{
		off[6] = offset;
		offset += (fvf&FVF_TANSPC_PACKED) ? sizeof(unsigned char) * 4 : sizeof(float) * 4;
	}

	if (fvf&FVF_VERTEXAO)
	{
		off[7] = offset;
		offset += sizeof(uint32);
	}
	else if (fvf&FVF_SHCOEFF)
	{
		off[7] = offset;
		offset += sizeof(float) * 3 * 3;
	}

	return offset;
}

int32 IVertexDescription::GetStride(uint32 fvf)
{
	int32 offset = 0;

	if (fvf&FVF_XYZ)
	{
		offset += sizeof(float) * 3;
	}
	if (fvf&FVF_XYZRHW)
	{
		offset += sizeof(float) * 4;
	}

	if (fvf&FVF_NORMAL)
	{
		offset += (fvf&FVF_NORMAL_PACKED) ? sizeof(unsigned char) * 4 : sizeof(float) * 3;
	}

	if (fvf&FVF_SKIN_INDEX)
	{
		offset += sizeof(unsigned char) * 4;
	}
	else if (fvf&FVF_DIFFUSE)
	{
		offset += sizeof(uint32);
	}

	if (fvf&FVF_SKIN_WEIGHT)
	{
		offset += sizeof(unsigned char) * 4;
	}
	if (fvf&FVF_TEX0)
	{
		offset += (fvf&FVF_TEX0_PACKED) ? sizeof(short) * 2 : sizeof(float) * 2;
	}

	if (fvf&FVF_TEX1)
	{
		offset += (fvf&FVF_TEX1_PACKED) ? sizeof(short) * 2 : sizeof(float) * 2;
	}

	if (fvf&FVF_TANSPC)
	{
		offset += (fvf&FVF_TANSPC_PACKED) ? sizeof(unsigned char) * 4 : sizeof(float) * 4;
	}

	if (fvf&FVF_SHCOEFF)
	{
		offset += sizeof(float) * 3 * 3;
	}
	else if (fvf&FVF_VERTEXAO)
	{
		offset += sizeof(uint32);
	}


	return offset;
}

// VertexCompress

void VertexCompress::TexcoordToShort2(short* Output, const float uv[2])
{
	Output[0] = (short)Math::round(uv[0] * 4096.0f);
	Output[1] = (short)Math::round(uv[1] * 4096.0f);
}

void VertexCompress::NormalToUBYTE4N(unsigned char* Output, const Vector3& n)
{
	//float x = (n.x+1.0f) * 0.5f;
	//float y = (n.y+1.0f) * 0.5f;
	//float z = (n.z+1.0f) * 0.5f;
	float x = (n.x*0.5f) + 0.5f;
	float y = (n.y*0.5f) + 0.5f;
	float z = (n.z*0.5f) + 0.5f;

	Output[0] = (unsigned char)(x * 255.0f);
	Output[1] = (unsigned char)(y * 255.0f);
	Output[2] = (unsigned char)(z * 255.0f);
	Output[3] = 0;
}

void VertexCompress::TangentToUBYTE4N(unsigned char* Output, const Vector4& n)
{
	//float x = (n.x+1.0f) * 0.5f;
	//float y = (n.y+1.0f) * 0.5f;
	//float z = (n.z+1.0f) * 0.5f;
	//float w = (n.w+1.0f) * 0.5f;
	float x = (n.x*0.5f) + 0.5f;
	float y = (n.y*0.5f) + 0.5f;
	float z = (n.z*0.5f) + 0.5f;
	float w = (n.w*0.5f) + 0.5f;

	Output[0] = (unsigned char)(x * 255.0f);
	Output[1] = (unsigned char)(y * 255.0f);
	Output[2] = (unsigned char)(z * 255.0f);
	Output[3] = (unsigned char)(w * 255.0f);
}

//void VertexCompress::BinormalToUBYTE4N(unsigned char* Output, const Vec3& n)
//{
//	float x = (n.x+1.0f) * 0.5f;
//	float y = (n.y+1.0f) * 0.5f;
//	float z = (n.z+1.0f) * 0.5f;
//
//	Output[0] = (unsigned char)(x * 255.0f);
//	Output[1] = (unsigned char)(y * 255.0f);
//	Output[2] = (unsigned char)(z * 255.0f);
//	Output[3] = 0;
//}

void VertexCompress::SkinIndexToUBYTE4(unsigned char* Output, const int* indices)
{
	Output[0] = (unsigned char)(indices[0]);
	Output[1] = (unsigned char)(indices[1]);
	Output[2] = (unsigned char)(indices[2]);
	Output[3] = (unsigned char)(indices[3]);
}

void VertexCompress::SkinWeightToUBYTE4N(unsigned char* Output, const float* weights)
{
	// weight 값은 0.0~1.0 사이의 값이므로, scale하지 않고 그대로 pack한다.
	Output[0] = (unsigned char)(weights[0] * 255.0f);
	Output[1] = (unsigned char)(weights[1] * 255.0f);
	Output[2] = (unsigned char)(weights[2] * 255.0f);
	Output[3] = (unsigned char)(weights[3] * 255.0f);
}

//  VertexDepress

void VertexDepress::UByte4ToSkinIndex(const unsigned char* input, unsigned int* output)
{
	output[0] = input[0];
	output[1] = input[1];
	output[2] = input[2];
	output[3] = input[3];
}

void VertexDepress::UByte4NToSkinWeight(const unsigned char* input, float* output)
{
	output[0] = input[0] / 255.0f;
	output[1] = input[1] / 255.0f;
	output[2] = input[2] / 255.0f;
	output[3] = input[3] / 255.0f;
}