#include "renderer.h"

// Initialize
std::map<String, int> ShaderParameter::m_ParamRegistry;
Variant ShaderParameter::m_CommonValue[];

void ShaderParameter::Initialize()
{
	static const struct
	{
		const int pid;
		const TCHAR* sementic;
	} _list[] =
	{
		WORLD, TEXT("WORLD"),
		VIEW, TEXT("VIEW"),
		PROJECTION, TEXT("PROJECTION"),
		CAMERA, TEXT("CAMERA"),
		CAMERAPOSITION, TEXT("CAMERAPOSITION"),
		CAMERADIRECTION, TEXT("CAMERADIRECTION"),
		CAMERADISTANCE, TEXT("CAMERADISTANCE"),
		CAMERAFOV, TEXT("CAMERAFOV"),
		FRUSTUMCORNERS, TEXT("FRUSTUMCORNERS"),
		GLOBALLIGHTDIRECTION, TEXT("GLOBALLIGHTDIRECTION"),
		GLOBALLIGHTPOSITION, TEXT("GLOBALLIGHTPOSITION"),
		GLOBALLIGHTCOLOR, TEXT("GLOBALLIGHTCOLOR"),
		GLOBALAMBIENTCOLOR, TEXT("GLOBALAMBIENTCOLOR"),
		HEMISPHERECOLOR, TEXT("HEMISPHERECOLOR"),
		HEMISPHEREUPPER, TEXT("HEMISPHEREUPPER"),
		HEMISPHERELOWER, TEXT("HEMISPHERELOWER"),
		RIMLIGHTDIRECTION, TEXT("RIMLIGHTDIRECTION"),
		RIMLIGHTCOLOR, TEXT("RIMLIGHTCOLOR"),
		RIMLIGHTPARAM, TEXT("RIMLIGHTPARAM"),
		SCREENSIZE, TEXT("SCREENSIZE"),
		DIFFUSEMAP, TEXT("DIFFUSEMAP"),

		TMARRAY, TEXT("TMARRAY"),

		///////////////////////////////////////
		WORLDVIEWPROJECTION, TEXT("WORLDVIEWPROJECTION"),
		WORLDVIEW, TEXT("WORLDVIEW"),
		VIEWPROJECTION, TEXT("VIEWPROJECTION"),

		SCREENPROJSCALE, TEXT("SCREENPROJSCALE"),
		SCREENPROJBIAS, TEXT("SCREENPROJBIAS"),
		//QUICKGAUSSIANWEIGHTS, TEXT("QUICKGAUSSIANWEIGHTS"),
		//QUICKGAUSSIANOFFSETS, TEXT("QUICKGAUSSIANOFFSETS"),
	};

	// 복사 금지, 변경 금지
	for (auto const& element : _list)
	{
		Register(element.sementic, element.pid);
	}
	//for (int i = 0; i < _countof(_list); i++)
	//	Register(_list[i].sementic, _list[i].pid);
}

void ShaderParameter::Shutdown()
{
}

int ShaderParameter::Register(const TCHAR* sementic)
{
	auto it = m_ParamRegistry.find(sementic);
	if (it != m_ParamRegistry.end())
	{
		return (*it).second;
	}

	//
	static int autoIncrement = _MAX_PARAMCOUNT;

	int uid = autoIncrement++;
	m_ParamRegistry.insert(std::make_pair(sementic, uid));
	return uid;
}

void ShaderParameter::Register(const TCHAR* sementic, int uid)
{
	m_ParamRegistry.insert(std::make_pair(sementic, uid));
}

int ShaderParameter::Find(const TCHAR* sementic)
{
	auto it = m_ParamRegistry.find(sementic);
	if (it != m_ParamRegistry.end())
		return (*it).second;

	return -1;
}

void ShaderParameter::SetCommonValue(int pid, const Variant& value)
{
	m_CommonValue[pid] = value;
}

const Variant& ShaderParameter::GetCommonValue(int pid)
{
	return m_CommonValue[pid];
}