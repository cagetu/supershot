#include "renderer.h"

class ScreenQuadCache
{
public:
	static void Initilaize()
	{

	}

	static void Shutdown()
	{
		std::vector <_SCREENQUAD>::iterator it, itend;

		itend = m_ScreenQuads.end();
		for (it = m_ScreenQuads.begin(); it != itend; it++)
		{
		}
		m_ScreenQuads.clear();
	}

	static CPrimitive* Create(int x, int y, int w, int h, float u1, float v1, float u2, float v2)
	{
		std::vector <_SCREENQUAD>::iterator it, itend;

		itend = m_ScreenQuads.end();
		for (it = m_ScreenQuads.begin(); it != itend; it++)
		{
			const _SCREENQUAD& cache = (*it);

			if (cache.x == x && cache.y == y && cache.w == w && cache.h == h &&
				cache.u1 == u1 && cache.v1 == v1 && cache.u2 == u2 && cache.v2 == v2)
			{
				return cache.primitive;
			}
		}

		//

		Vector3 p[4];
		float uv[4][2];
		int i;

		/*0(0,0) 		 1(0,1)
		+---------------+
		|				|
		|				|
		|				|
		|				|
		|				|
		|				|
		+---------------+
		2(0,1)			 3(1,1)
		*/
		for (i = 0; i<4; i++)
		{
			p[i] = Vector3((float)(!(i & 1) ? x : x + w), (float)(!(i & 2) ? y : y + h), (float)((i & 1) + ((i & 2) ^ 2)));
			uv[i][0] = (!(i & 1) ? u1 : u2);
			uv[i][1] = (!(i & 2) ? v1 : v2);
		}
		const unsigned short idx[] = {
			0, 1, 3, 0, 3, 2,
		};

		Ptr<VertexBuffer> vb = CreateVertexBuffer();
		{
			vb->Create(FVF_XYZ | FVF_TEX0, 4);
			void* vertexData = vb->Lock();
			VertexBuffer::FillData(vertexData, vb->GetStride(), vb->GetFVF(), 0, 4, p, NULL, NULL, uv, NULL, NULL, NULL, NULL, NULL, NULL);
			vb->Unlock();
		}

		Ptr<IndexBuffer> ib = CreateIndexBuffer();
		{
			ib->Create(6);
			void* indexData = ib->Lock();
			PlatformUtil::Memcopy(indexData, &idx[0], sizeof(idx));
			// 4바이트 정렬로 채운다.
			ib->Unlock();
		}

		CPrimitive* primitive = new CPrimitive();
		primitive->SetVertexBuffer(vb, 4, 0);
		primitive->SetIndexBuffer(ib, 6);

		_SCREENQUAD quad(x, y, w, h, u1, v1, u2, v2, primitive);
		m_ScreenQuads.emplace_back(quad);

		return primitive;
	}

private:
	struct _SCREENQUAD
	{
		Ptr<CPrimitive> primitive;

		int x, y, w, h;
		float u1, v1, u2, v2;

		_SCREENQUAD(int x, int y, int w, int h, float u1, float v1, float u2, float v2, const Ptr<CPrimitive>& primitive)
		{
			this->x = x; this->y = y; this->w = w; this->h = h;
			this->u1 = u1; this->v1 = v1;
			this->u2 = u2; this->v2 = v2;

			this->primitive = primitive;
		}
	};
	static std::vector <_SCREENQUAD> m_ScreenQuads;
};

std::vector <ScreenQuadCache::_SCREENQUAD> ScreenQuadCache::m_ScreenQuads;

RenderPass::FactoryMap* RenderPass::m_RenderPassFactory = NULL;

void RenderPass::Shutdown()
{
	if (m_RenderPassFactory != NULL)
	{
		m_RenderPassFactory->clear();
		delete m_RenderPassFactory;
		m_RenderPassFactory = NULL;
	}

	ScreenQuadCache::Shutdown();
}

RenderPass* RenderPass::CreateClass(const char* className)
{
	FactoryMap::iterator iter = m_RenderPassFactory->find(className);
	if (iter == m_RenderPassFactory->end())
		return NULL;

	RenderPass::CreateFunc func = iter->second;
	return func();
}

void RenderPass::RegisterObject(const char* className, RenderPass::CreateFunc factoryFunc)
{
	if (m_RenderPassFactory == NULL)
	{
		m_RenderPassFactory = new FactoryMap();
	}

	if (m_RenderPassFactory->empty())
	{
		m_RenderPassFactory->insert(FactoryMap::value_type(className, factoryFunc));
		return;
	}

	FactoryMap::iterator iter = m_RenderPassFactory->find(className);
	if (iter != m_RenderPassFactory->end())
		return;

	m_RenderPassFactory->insert(FactoryMap::value_type(className, factoryFunc));
}

////
RenderPass::RenderPass()
{
	m_ClearColor = 0xffffffff;
	m_ClearDepth = 1.0f;
	m_ClearStencil = 0;

	m_RenderState = 0;

	m_Features = 0;

	m_Parent = NULL;
}
RenderPass::~RenderPass()
{
}

bool RenderPass::Load(CMiniXML::iterator& node, RenderPipeline* parent)
{
	m_Parent = parent;

	m_Features = 0;

	const TCHAR* name = node.GetAttribute(TEXT("name"));
	if (name)
		m_Name = name;

	// RENDER TARGET
	m_RenderTarget.width = 0;
	m_RenderTarget.height = 0;
	memset(m_RenderTarget.pixelformat, 0, sizeof(m_RenderTarget.pixelformat));

	CMiniXML::iterator iter = node.FindChild(TEXT("RenderTarget"));
	if (iter != NULL)
	{
		const TCHAR* mode = iter.GetAttribute(TEXT("mode"));
		LoadModes(mode);

		CMiniXML::iterator clearnode = iter.FindChild(TEXT("Clear"));
		if (clearnode != NULL)
		{
			const TCHAR* clearColor = clearnode.GetAttribute(TEXT("clearColor"));
			if (clearColor)	{
				RGBA color;
				unicode::atov(clearColor, (Vector4*)&color.r);
				m_ClearColor = color;
			}

			const TCHAR* clearDepth = clearnode.GetAttribute(TEXT("clearDepth"));
			if (clearDepth)
				m_ClearDepth = unicode::atof(clearDepth);

			const TCHAR* clearStencil = clearnode.GetAttribute(TEXT("clearStencil"));
			if (clearStencil)
				m_ClearStencil = unicode::atoi(clearStencil);
		}

		CMiniXML::const_iterator viewportnode = iter.FindChild(TEXT("Viewport"));
		if (viewportnode != NULL)
		{
			const TCHAR* rect = viewportnode.GetAttribute(TEXT("rect"));
			if (rect)
			{
				Vector4 v;
				unicode::atov(rect, &v);

				m_Viewport.x = v.x;
				m_Viewport.y = v.y;
				m_Viewport.width = v.z;
				m_Viewport.height = v.w;
				m_Viewport.minZ = 0.0f;
				m_Viewport.maxZ = 1.0f;
			}
		}

		// RENDER SURFACE
		CMiniXML::iterator surfacenode = iter.FindChild(TEXT("surface"));
		if (surfacenode != NULL)
		{
			float width = 0;
			float height = 0;
			if (m_Features&_FIXEDSIZE)
			{
				width = unicode::atof(surfacenode.GetAttribute(TEXT("width")));
				height = unicode::atof(surfacenode.GetAttribute(TEXT("height")));
			}
			else
			{
				float scaleWidth = 1.0f;
				float scaleHeight = 1.0f;

				const TCHAR* s_width = surfacenode.GetAttribute(TEXT("width"));
				if (s_width != NULL)
					scaleWidth = unicode::atof(s_width);
				const TCHAR* s_height = surfacenode.GetAttribute(TEXT("height"));
				if (s_height != NULL)
					scaleHeight = unicode::atof(s_height);

				width = scaleWidth;
				height = scaleHeight;
			}
			m_RenderTarget.width = width;
			m_RenderTarget.height = height;

#ifdef WIN32
#define ENUM_RENDERSTATE(RS) L#RS, ##RS
#define	ENUM_MASK(SM) {##SM, L#SM}
#define	ENUM_FORMAT(F) L#F, RT_##F
#define ENUM_SORT(ST) L#ST, _SORT##ST
#elif defined(__APPLE__) || defined(__ANDROID__)
#define ENUM_RENDERSTATE(RS) L## #RS, RS
#define	ENUM_MASK(SM) {SM, L## #SM}
#define	ENUM_FORMAT(F) L## #F, RT_##F
#define ENUM_SORT(ST) L## #ST, _SORT##ST
#endif

			static struct
			{
				const TCHAR* format;
				int code;
			} _list[] =
			{
				ENUM_FORMAT(R5G6B5),
				ENUM_FORMAT(R5G5B5A1),
				ENUM_FORMAT(R4G4B4A4),
				ENUM_FORMAT(R8G8B8A8),
				ENUM_FORMAT(R8G8B8X8),
				ENUM_FORMAT(R16F),
				ENUM_FORMAT(G16R16F),
				ENUM_FORMAT(A16B16G16R16F),
				ENUM_FORMAT(R32F),
				ENUM_FORMAT(G32R32F),
				ENUM_FORMAT(A32B32G32R32F),
				ENUM_FORMAT(D24S8),
				ENUM_FORMAT(D24X8),
				ENUM_FORMAT(DEPTH_BYTE),
				ENUM_FORMAT(DEPTH_INT),
				ENUM_FORMAT(DEPTH_SHORT),
			};

			// renderTexture
			int channel = 0;
			for (CMiniXML::iterator it = surfacenode.FindChild(TEXT("rendertexture")); it != NULL; it = it.Find(TEXT("rendertexture")), channel++)
			{
				const TCHAR* name = it.GetAttribute(TEXT("name"));
				m_RenderTarget.name[channel] = name;

				const TCHAR* format = it.GetAttribute(TEXT("format"));
				for (int i = 0; i < _countof(_list); i++)
				{
					if (!unicode::strcmp(format, _list[i].format))
					{
						m_RenderTarget.pixelformat[channel] = _list[i].code;
						break;
					}
				}
			}

			CMiniXML::iterator it = surfacenode.FindChild(TEXT("depthtexture"));
			if (it != NULL)
			{
				m_RenderTarget.depthbuffer = it.GetAttribute(TEXT("name"));

				const TCHAR* format = it.GetAttribute(TEXT("format"));
				for (int i = 0; i < _countof(_list); i++)
				{
					if (!unicode::strcmp(format, _list[i].format))
					{
						m_RenderTarget.depthformat = _list[i].code;
						break;
					}
				}
			}

		}
	}

	return true;
}

void RenderPass::LoadModes(const TCHAR* mode)
{
#ifdef WIN32
#define	ENUM_MASK(SM) {##SM, L#SM}
#elif defined(__APPLE__) || defined(__ANDROID__)
#define	ENUM_MASK(SM) {SM, L## #SM}
#endif

	static struct
	{
		int mask;
		const TCHAR* format;
	} _list[] = 
	{
		ENUM_MASK(_CLEARCOLOR),
		ENUM_MASK(_CLEARDEPTH),
		ENUM_MASK(_CLEARSTENCIL),
		ENUM_MASK(_DISCARDCOLORS),
		ENUM_MASK(_DISCARDDEPTHSTENCIL),
		ENUM_MASK(_FIXEDSIZE),
	};
	for (int32 i = 0; i < _countof(_list); i++)
	{
		if (unicode::strstr(mode, _list[i].format))
			m_Features |= _list[i].mask;
	}
}

IRenderTarget* RenderPass::GetRenderTarget()
{
	if (m_RenderTarget.width > 0 && m_RenderTarget.height > 0)
	{
		int width = 0;
		int height = 0;

		if (m_Features&_FIXEDSIZE)
		{
			width = (int)m_RenderTarget.width;
			height = (int)m_RenderTarget.height;
		}
		else
		{
			width = (int)(GetRenderDriver()->GetWidth() * m_RenderTarget.width);
			height = (int)(GetRenderDriver()->GetHeight() * m_RenderTarget.height);
		}

		const TCHAR* name[4];
		for (int i = 0; i < 4; i++)
		{
			if (m_RenderTarget.name[i].size() > 0)
				name[i] = m_RenderTarget.name[i].c_str();
		}
		IRenderTarget* rt = m_Parent->CreateRenderTarget(width, height, name, m_RenderTarget.pixelformat);

		if (m_RenderTarget.depthbuffer.size() > 0)
		{
			const Ptr<IDepthStencilSurface>& depthbuffer = m_Parent->CreateDepthBuffer(m_RenderTarget.depthbuffer, width, height, m_RenderTarget.depthformat);
			rt->SetDepthBuffer(depthbuffer);
		}
		_ASSERT(rt != NULL);
		return rt;
	}
	return NULL;
}

IRenderTexture2D* RenderPass::GetRenderTexture(const TCHAR* name)
{
	return m_Parent->GetRenderTexture(name);
}

RenderPipeline::RenderCallback* RenderPass::GetRenderCallback()
{
	return m_Parent->GetRenderCallback();
}

float RenderPass::GetWidth() const
{
	float width = 0.0f;

	if (m_Features&_FIXEDSIZE)
	{
		width = m_RenderTarget.width;
	}
	else
	{
		if (m_RenderTarget.width > 0.0f)
			width = (GetRenderDriver()->GetWidth() * m_RenderTarget.width);
		else
			width = (float)GetRenderDriver()->GetWidth();
	}

	return width;
}

float RenderPass::GetHeight() const
{
	float height = 0.0f;

	if (m_Features&_FIXEDSIZE)
	{
		height = m_RenderTarget.height;
	}
	else
	{
		if (m_RenderTarget.height > 0.0f)
			height = (GetRenderDriver()->GetHeight() * m_RenderTarget.height);
		else
			height = (float)GetRenderDriver()->GetHeight();
	}
	return height;
}

bool RenderPass::IsRequired(const TCHAR* renderTexture) const
{
	std::map <String, String>::const_iterator it = m_RenderTextures.find(renderTexture);
	if (it == m_RenderTextures.end())
		return false;

	return true;
}

//
//
//
__ImplementRenderPass(SceneRenderPass);

SceneRenderPass::SceneRenderPass()
{
}

SceneRenderPass::~SceneRenderPass()
{
	Destroy();
}

bool SceneRenderPass::Load(CMiniXML::iterator& node, RenderPipeline* parent)
{
	if (RenderPass::Load(node, parent) == false)
		return false;

	return true;
}

void SceneRenderPass::Destroy()
{

}

void SceneRenderPass::Update()
{

}

void SceneRenderPass::Render()
{
	IRenderDriver* device = GetRenderDriver();

	IRenderTarget* rt = RenderPass::GetRenderTarget();
	//device->SetViewport(&m_Viewport);

	device->BeginScene(rt);

	uint32 clearflags = 0;
	if (m_Features&_CLEARCOLOR)
		clearflags |= CLEAR_COLOR;
	if (m_Features&_CLEARDEPTH)
		clearflags |= CLEAR_DEPTH;
	if (m_Features&_CLEARSTENCIL)
		clearflags |= CLEAR_STENCIL;

	device->Clear(m_ClearColor, m_ClearDepth, m_ClearStencil, clearflags);

	device->EndScene();
}


//
//
//
__ImplementRenderPass(PostRenderPass);

PostRenderPass::PostRenderPass()
{
	m_ScreenQuad = NULL;
}

PostRenderPass::~PostRenderPass()
{
	Destroy();
}

bool PostRenderPass::Load(CMiniXML::iterator& node, RenderPipeline* parent)
{
	if (RenderPass::Load(node, parent) == false)
		return false;

	// Screen Quad
	float width = GetWidth();
	float height = GetHeight();

	CPrimitive* prim = ScreenQuadCache::Create(0, 0, (int)width, (int)height, 0.0f, 0.0f, 1.0f, 1.0f);
	
	m_ScreenQuad = new CMesh(prim);

	// Shader
	CMiniXML::iterator shaderNode = node.FindChild(TEXT("Shader"));
	if (shaderNode != NULL)
	{
		ShaderFeature::MASK shaderDefine = 0;

		const TCHAR* shaderDef = shaderNode.GetAttribute(TEXT("define"));
		if (shaderDef)
		{
			shaderDefine = ShaderFeature::GetShaderMask(shaderDef);
		}

		SHADERCODE shaderCode;
		for (CMiniXML::iterator it = shaderNode.FindChild(TEXT("shader")); it != NULL; it = it.Find(TEXT("shader")))
		{
			const TCHAR* type = it.GetAttribute(TEXT("type"));
			if (!unicode::strcmp(type, TEXT("vs")))
			{
				shaderCode.Insert(ShaderType::VERTEXSHADER, it.GetValue());
			}
			else if (!unicode::strcmp(type, TEXT("ps")))
			{
				shaderCode.Insert(ShaderType::FRAGMENTSHADER, it.GetValue());
			}
		}

		m_Shader = ShaderCore::Build(shaderCode, shaderDefine);

		m_Material = new CMaterial();

		// shader variables
		CMiniXML::iterator shaderValueNode = shaderNode.FindChild(TEXT("parameter"));
		if (shaderValueNode != NULL)
		{
			for (CMiniXML::iterator it = shaderValueNode.GetChild(); it != NULL; it = it.Next())
			{
				const TCHAR* sementic = it.GetName();
				const TCHAR* type = it.GetAttribute(TEXT("type"));
				const TCHAR* value = it.GetValue();

				if (!unicode::strcmp(type, TEXT("rendertexture")))
				{
					m_RenderTextures.insert(std::make_pair(sementic, value));
				}
				else if (!unicode::strcmp(type, TEXT("texture")))
				{
					ITexture2D* texture = CreateFileTexture();
					if (texture->Load(value) == true)
					{
						m_Material->SetParameter(sementic, texture);
					}
				}
				else if (!unicode::strcmp(type, TEXT("float")))
				{
					m_Material->SetParameter(sementic, unicode::atof(value));
				}
				else if (!unicode::strcmp(type, TEXT("int")))
				{
					m_Material->SetParameter(sementic, unicode::atoi(value));
				}
				else if (!unicode::strcmp(type, TEXT("vec4")))
				{
					Vector4 v;
					unicode::atov(value, &v);
					m_Material->SetParameter(sementic, v);
				}
				else
				{
					PlatformUtil::DebugOutFormat(TEXT("%s is Undefined\n"), type);
					_ASSERT(0);
				}
			}
		}
	}

	m_Material->AddPass(m_Shader);

	m_ScreenQuad->SetMaterial(m_Material);
	//m_ScreenQuad->SetShader(m_Shader);

	return true;
}

void PostRenderPass::Destroy()
{
	m_RenderTextures.clear();

	if (m_ScreenQuad)
	{
		delete m_ScreenQuad;
		m_ScreenQuad = NULL;
	}
}

void PostRenderPass::ApplyValue()
{
	if (m_Shader == NULL)
		return;

	// Render Textures
	{
		std::map <String, String>::const_iterator it, itend;

		itend = m_RenderTextures.end();
		for (it = m_RenderTextures.begin(); it != itend; it++)
		{
			IRenderTexture2D* texture = RenderPass::GetRenderTexture(it->second);
			//m_Material->SetValue(it->first, texture);
			int pid = ShaderParameter::Register(it->first);
			ShaderParameter::SetCommonValue(pid, texture);
		}
	}
}

void PostRenderPass::Update()
{
	//
}

void PostRenderPass::Render()
{
	IRenderDriver* device = GetRenderDriver();

	IRenderTarget* rt = RenderPass::GetRenderTarget();

	if (m_Features&_DISCARDCOLORS || m_Features&_DISCARDDEPTHSTENCIL)
	{
		bool discardDepth = false, discardStencil = false;
		uint32 discardColors = 0;

		if (m_Features&_DISCARDCOLORS)
		{
			discardColors = 1;
		}
		if (m_Features&_DISCARDDEPTHSTENCIL)
		{
			discardDepth = true;
			discardStencil = true;
		}
		device->DiscardRenderTarget(discardDepth, discardStencil, discardColors);
	}

	//device->SetViewport(&m_Viewport);
	device->BeginScene(rt);
	{
		uint32 clearflags = 0;
		if (m_Features&_CLEARCOLOR)
			clearflags |= CLEAR_COLOR;
		if (m_Features&_CLEARDEPTH)
			clearflags |= CLEAR_DEPTH;
		if (m_Features&_CLEARSTENCIL)
			clearflags |= CLEAR_STENCIL;

		device->Clear(m_ClearColor, m_ClearDepth, m_ClearStencil, clearflags);

		ApplyValue();

		m_ScreenQuad->Render();
	}
	device->EndScene();
}

//
//
//
__ImplementRenderPass(CallbackPass);

CallbackPass::CallbackPass()
{
}

CallbackPass::~CallbackPass()
{
	Destroy();
}

void CallbackPass::Destroy()
{
}

void CallbackPass::Update()
{
}

void CallbackPass::Render()
{
	RenderPipeline::RenderCallback* callback = RenderPass::GetRenderCallback();

	if (callback != NULL)
	{
		callback->Callback(m_Name.c_str());
	}
}
