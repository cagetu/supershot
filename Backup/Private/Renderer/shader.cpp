#include "renderer.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, IShader, IRenderResource);

IShader::IShader()
{
	static int autoIncrement = 0;
	m_UID = UID_SHADER | ((autoIncrement++) & 0xFFFFF);
}

IShader::~IShader()
{
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, IShaderResource, IObject);

IShaderResource::IShaderResource()
	: m_ShaderType(ShaderType::_INVALID)
{
}
IShaderResource::~IShaderResource()
{
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, IShaderProgram, IObject);

IShaderProgram::IShaderProgram(ShaderType::MASK shaderTypes)
	: m_ShaderTypeBits(shaderTypes)
{
}
IShaderProgram::~IShaderProgram()
{
}

void IShaderProgram::UpdateInternal(int32 index, int32 pid)
{
	switch (pid)
	{
	case ShaderParameter::WORLDVIEWPROJECTION:
	{
		const Variant& worldTM = ShaderParameter::GetCommonValue(ShaderParameter::WORLD);
		const Variant& viewTM = ShaderParameter::GetCommonValue(ShaderParameter::VIEW);
		const Variant& projTM = ShaderParameter::GetCommonValue(ShaderParameter::PROJECTION);
		SetValue(index, (const Mat4&)worldTM * (const Mat4&)viewTM * (const Mat4&)projTM);
		break;
	}
	case ShaderParameter::WORLDVIEW:
	{
		const Variant& worldTM = ShaderParameter::GetCommonValue(ShaderParameter::WORLD);
		const Variant& viewTM = ShaderParameter::GetCommonValue(ShaderParameter::VIEW);
		SetValue(index, (const Mat4&)worldTM * (const Mat4&)viewTM);
		break;
	}
	case ShaderParameter::VIEWPROJECTION:
	{
		const Variant& viewTM = ShaderParameter::GetCommonValue(ShaderParameter::VIEW);
		const Variant& projTM = ShaderParameter::GetCommonValue(ShaderParameter::PROJECTION);
		SetValue(index, (const Mat4&)viewTM * (const Mat4&)projTM);
		break;
	}
	case ShaderParameter::SCREENPROJSCALE:
	{
		const Vector4& screenSize = (const Vector4&)ShaderParameter::GetCommonValue(ShaderParameter::SCREENSIZE);
		Vector4 v(2.0f * screenSize.z, -2.0f * screenSize.w, 1.0f, 1.0f);

		Vector4 p(screenSize.x * v.x - 1.0f, screenSize.y * v.y + 1.0f, 0.0f, 1.0f);

		SetValue(index, v);
		break;
	}
	case ShaderParameter::SCREENPROJBIAS:
	{
		const Variant& screenSize = ShaderParameter::GetCommonValue(ShaderParameter::SCREENSIZE);
		const Vector4& v = (const Vector4&)screenSize;

		SetValue(index, Vector4(-1.0f, 1.0f, 0.0f, 0.0f));
		break;
	}

	/*
	case ShaderParameter::QUICKGAUSSIANWEIGHTS:
	{
	SetValue(index, Vector3(5.0f / 16.0f, 6.0f / 16.0f, 5.0f / 16.0f));
	break;
	}
	case ShaderParameter::QUICKGAUSSIANOFFSETS:
	{
	const Variant& v = ShaderParameter::GetCommonValue(ShaderParameter::DIFFUSEMAP);
	if (v.empty() == false)
	{
	Texture* texture = (Texture*)v;
	if (texture)
	{
	int width = texture->GetWidth();
	int height = texture->GetHeight();

	const float offset = 1.2f;

	float texel[2] = { 1.0f / (float)width, 1.0f / (float)height };
	SetValue(index, Vector4(-offset*texel[0], offset*texel[0], -offset*texel[1], offset*texel[1]));
	}
	}
	break;
	}
	*/
	}

}