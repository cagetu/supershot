#include "renderer.h"

CPrimitive::CPrimitive()
	: m_VertexBuffer(NULL)
	, m_IndexBuffer(NULL)
	, m_VertexFormat(0)
	, m_VertexOffset(0)
	, m_VertexCount(0)
	, m_PrimitiveType(PT_TRIANGLELIST)
	, m_PrimitieveCount(0)
	, m_StartVertexIndex(0)
	, m_BaseVertexIndex(0)
{
	static int autoIncrement = 0;
	m_UID = UID_PRIMITIVE | ((autoIncrement++) & 0xFFFFF);
}

CPrimitive::~CPrimitive()
{
}

void CPrimitive::SetLocalBound(const AABB& bound)
{
	m_LocalAABB = bound;
}

void CPrimitive::SetVertexBuffer(const Ptr<VertexBuffer>& vb, int numVerts, int offset)
{
	m_VertexBuffer = vb;
	m_VertexFormat = vb->GetFVF();
	m_VertexOffset = offset;
	m_VertexCount = numVerts;
}

void CPrimitive::SetIndexBuffer(const Ptr<IndexBuffer>& ib, int numFaces, int startIndex, int baseIndex)
{
	m_IndexBuffer = ib;
	m_PrimitieveCount = numFaces;
	m_StartVertexIndex = startIndex;
	m_BaseVertexIndex = baseIndex;
}

void CPrimitive::SetPrimitiveType(PRIMITIVETYPE type)
{
	m_PrimitiveType = type;
}

uint32 CPrimitive::GetFVF() const
{
	return m_VertexBuffer->GetFVF();
}

void CPrimitive::Render(IShader* shader, const CMaterial* material, uint32 renderstate)
{
	IRenderDriver* renderDevice = GetRenderDriver();

	renderDevice->SetVertexBuffer(m_VertexBuffer);
	renderDevice->SetIndexBuffer(m_IndexBuffer);

	renderDevice->SetShader(shader, m_VertexBuffer, material);
	renderDevice->SetRenderState(renderstate);

	if (m_IndexBuffer == NULL)
		renderDevice->DrawPrimitive(m_PrimitiveType, 0, m_PrimitiveType == PT_LINELIST ? m_VertexCount / 2 : m_VertexCount / 3);
	else
		renderDevice->DrawIndexedPrimitive(m_PrimitiveType, m_BaseVertexIndex, m_StartVertexIndex, m_IndexBuffer->GetCount(), m_IndexBuffer);
}