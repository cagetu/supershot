#include "renderer.h"

__ImplementRtti(VisualShock, IndexBuffer, IRenderResource);

IndexBuffer::IndexBuffer()
	: m_Length(0), m_Offset(0)
	, m_Flags(0), m_BufferUsage(0)
	, m_ValidCount(0)
	, m_IdxBuf(NULL)
	, m_Stride(0)
{
}

IndexBuffer::~IndexBuffer()
{
}

void IndexBuffer::Destroy()
{
	if (m_IdxBuf != NULL)
	{
		delete[] m_IdxBuf;
		m_IdxBuf = NULL;
	}
}

void IndexBuffer::SetStride(int32 stride)
{
	m_Stride = stride;
	if (m_Stride == 2)	m_Flags |= _INDEX16;
	if (m_Stride == 4)	m_Flags |= _INDEX32;
}

/////

bool IndexBufferCache::Register(IndexBuffer* resource)
{
	m_Resources.push_back(resource);
	return true;
}

bool IndexBufferCache::Unregister(IndexBuffer* resource)
{
	auto it = std::find(m_Resources.begin(), m_Resources.end(), resource);
	if (it != m_Resources.end())
	{
		m_Resources.erase(it);
		return true;
	}
	return false;
}

void IndexBufferCache::Clear()
{
	m_Resources.clear();
}