#include "renderer.h"
#include <spirv_cross.hpp>

__ImplementRtti(VisualShock, VulkanShaderResource, IShaderResource);

///////////////////////////////////////////////////////////////////

VulkanShaderResource::VulkanShaderResource(VulkanDevice* device)
	: m_Device(device)
	, m_ShaderModule(VK_NULL_HANDLE)
{
}
VulkanShaderResource::~VulkanShaderResource()
{
	Destroy();
}

bool VulkanShaderResource::Create(ICompiledShaderCode* code, ShaderType::TYPE type)
{
	VulkanCompiledShaderCode* spirvShader = (VulkanCompiledShaderCode*)code;

	// Create Shader Module
	VkShaderModuleCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = 0;
	createInfo.pCode = spirvShader->code.data();
	createInfo.codeSize = spirvShader->code.size() * sizeof(uint32_t);
	VK_RESULT(vkCreateShaderModule(m_Device->GetHandle(), &createInfo, nullptr, &m_ShaderModule));

	// Table 정보..
	// TODO: std::move 에 대해서 공부하기
	m_NumDescriptors = 0;
	m_NumImages = 0;
	{
		spirv_cross::Compiler compiler(std::move(spirvShader->code));
		spirv_cross::ShaderResources resources = compiler.get_shader_resources();

		const std::vector<std::string>& entryPoints = compiler.get_entry_points();
		_ASSERT(entryPoints.size() == 1);
		m_EntryPoint = entryPoints[0];

		m_Inputs.resize(resources.stage_inputs.size());
		for (uint32 i = 0; i < resources.stage_inputs.size(); i++)
		{
			spirv_cross::Resource& res = resources.stage_inputs[i];

			m_Inputs[i].name = res.name;
			m_Inputs[i].location = compiler.get_decoration(res.id, spv::DecorationLocation);
		}
		m_Outputs.resize(resources.stage_outputs.size());
		for (uint32 i = 0; i < resources.stage_outputs.size(); i++)
		{
			spirv_cross::Resource& res = resources.stage_outputs[i];

			m_Outputs[i].name = res.name;
			m_Outputs[i].location = compiler.get_decoration(res.id, spv::DecorationLocation);
		}

		// Uniform Buffer
		m_UniformBuffers.resize(resources.uniform_buffers.size());
		for (uint32 i = 0; i < resources.uniform_buffers.size(); i++, m_NumDescriptors++)
		{
			spirv_cross::Resource& res = resources.uniform_buffers[i];

			Uniform& UniformBuffer = m_UniformBuffers[i];
			UniformBuffer.name = res.name;
			UniformBuffer.binding = compiler.get_decoration(res.id, spv::DecorationBinding);
			UniformBuffer.descriptorSet = compiler.get_decoration(res.id, spv::DecorationDescriptorSet);
			UniformBuffer.size = 0;

			const spirv_cross::SPIRType& structType = compiler.get_type(res.base_type_id);
			_ASSERT(structType.basetype == spirv_cross::SPIRType::Struct);

			uint32 memberCount = structType.member_types.size();
			UniformBuffer.members.resize(memberCount);

			uint32 offset = 0;
			for (uint32 index = 0; index < memberCount; index++)
			{
				const spirv_cross::SPIRType& memberType = compiler.get_type(structType.member_types[index]);

				Descriptor::MemberType& MemberDesc = UniformBuffer.members[index];
				MemberDesc.name = compiler.get_member_name(res.base_type_id, index);
				MemberDesc.offset = compiler.get_member_decoration(res.base_type_id, index, spv::DecorationOffset);
				MemberDesc.size = compiler.get_declared_struct_member_size(structType, index);

				if (offset < MemberDesc.offset)
				{
					PlatformUtil::DebugOutFormat("[Warning] Please Check Memory Alignment: %s.%s (offset: %d < %d) \n", res.name.c_str(), MemberDesc.name.c_str(), offset, MemberDesc.offset);
				}

				offset += MemberDesc.size;

				if (memberType.basetype == spirv_cross::SPIRType::Float)
				{
					MemberDesc.vecsize = memberType.vecsize;
					MemberDesc.columns = memberType.columns;
					MemberDesc.arraysize = (memberType.array.size() > 0) ? memberType.array.at(0) : 0;

					if (memberType.vecsize == 1)		// float
					{
						MemberDesc.basetype = Descriptor::MemberType::Float;
					}
					else if (memberType.vecsize == 2)	// vec2
					{
						MemberDesc.basetype = Descriptor::MemberType::Vector2;
					}
					else if (memberType.vecsize == 3)	// vec3, mat3, mat3x4
					{
						MemberDesc.basetype = Descriptor::MemberType::Vector3;

						if (memberType.columns > 1)
						{
							_ASSERT(memberType.columns == 3);
							MemberDesc.basetype = Descriptor::MemberType::Matrix3;
						}

					}
					else if (memberType.vecsize == 4)	// vec4, mat4, mat4x3
					{
						MemberDesc.basetype = Descriptor::MemberType::Vector3;

						if (memberType.columns > 1)
						{
							_ASSERT(memberType.columns == 4);
							MemberDesc.basetype = Descriptor::MemberType::Matrix4;
						}
					}
				}
			}

			UniformBuffer.size = offset;
		}

		m_Images.resize(resources.separate_images.size());
		for (uint32 i = 0; i < resources.separate_images.size(); i++, m_NumDescriptors++, m_NumImages++)
		{
			spirv_cross::Resource& res = resources.separate_images[i];

			m_Images[i].name = res.name;
			m_Images[i].binding = compiler.get_decoration(res.id, spv::DecorationBinding);
			//m_Images[i].offset = compiler.get_decoration(res.id, spv::DecorationOffset);
			m_Images[i].descriptorSet = compiler.get_decoration(res.id, spv::DecorationDescriptorSet);
		}

		m_Samplers.resize(resources.separate_samplers.size());
		for (uint32 i = 0; i < resources.separate_samplers.size(); i++, m_NumDescriptors++, m_NumImages++)
		{
			spirv_cross::Resource& res = resources.separate_samplers[i];

			m_Samplers[i].name = res.name;
			m_Samplers[i].binding = compiler.get_decoration(res.id, spv::DecorationBinding);
			//m_Samplers[i].offset = compiler.get_decoration(res.id, spv::DecorationOffset);
			m_Samplers[i].descriptorSet = compiler.get_decoration(res.id, spv::DecorationDescriptorSet);
		}

		m_SampledImages.resize(resources.sampled_images.size());
		for (uint32 i = 0; i < resources.sampled_images.size(); i++, m_NumDescriptors++, m_NumImages++)
		{
			spirv_cross::Resource& res = resources.sampled_images[i];

			m_SampledImages[i].name = res.name;
			m_SampledImages[i].binding = compiler.get_decoration(res.id, spv::DecorationBinding);
			//m_SampledImages[i].offset = compiler.get_decoration(res.id, spv::DecorationOffset);
			m_SampledImages[i].descriptorSet = compiler.get_decoration(res.id, spv::DecorationDescriptorSet);
		}

		m_PushConstants.resize(resources.push_constant_buffers.size());
		for (uint32 i = 0; i < resources.push_constant_buffers.size(); i++)
		{
			spirv_cross::Resource& res = resources.push_constant_buffers[i];
			std::vector<spirv_cross::BufferRange>& bufferRanges = compiler.get_active_buffer_ranges(res.id);

			m_PushConstants[i].bufferRanges.resize(bufferRanges.size());
			for (uint32 index = 0; index < bufferRanges.size(); index++)
			{
				PushConstant::Range& range = m_PushConstants[i].bufferRanges[index];
				range.index = bufferRanges[index].index;
				range.offset = bufferRanges[index].offset;
				range.range = bufferRanges[index].range;
			}

			m_PushConstants[i].name = res.name;
			//m_PushConstants[i].binding = compiler.get_decoration(res.id, spv::DecorationBinding);
			//m_PushConstants[i].offset = compiler.get_decoration(res.id, spv::DecorationOffset);
			//m_PushConstants[i].set = compiler.get_decoration(res.id, spv::DecorationDescriptorSet);
			//m_PushConstants[i].input_attachment = compiler.get_decoration(res.id, spv::DecorationInputAttachmentIndex);
		}

		m_SubpassInputs.resize(resources.subpass_inputs.size());
		for (uint32 i = 0; i < resources.subpass_inputs.size(); i++)
		{
			spirv_cross::Resource& res = resources.subpass_inputs[i];

			m_Samplers[i].name = res.name;
			m_SubpassInputs[i].binding = compiler.get_decoration(res.id, spv::DecorationBinding);
			//m_SubpassInputs[i].offset = compiler.get_decoration(res.id, spv::DecorationOffset);
			m_SubpassInputs[i].descriptorSet = compiler.get_decoration(res.id, spv::DecorationDescriptorSet);
			m_SubpassInputs[i].input_attachment = compiler.get_decoration(res.id, spv::DecorationInputAttachmentIndex);
		}
	}

	m_ShaderType = type;
	return true;
}

void VulkanShaderResource::Destroy()
{
	// Destroy를 호출하면 즉시 삭제된다.
	// Module을 사용하는 Pipeline이 있다면, Pipeline이 소멸되기 전에는 삭제되어서는 안된다.
	if (m_ShaderModule != VK_NULL_HANDLE)
	{
		vkDestroyShaderModule(m_Device->GetHandle(), m_ShaderModule, nullptr);
	}
	m_ShaderModule = VK_NULL_HANDLE;
}

///////////////////////////////////////////////////////////////////
IShaderResource* CreateShaderResource(ICompiledShaderCode* code, ShaderType::TYPE type)
{
	VulkanDriver* driver = (VulkanDriver*)GetRenderDriver();
	_ASSERT(driver);

	VulkanShaderResource* shaderCode = new VulkanShaderResource(driver->GetDevice());
	if (shaderCode->Create(code, type) == true)
		return shaderCode;

	delete shaderCode;
	return NULL;
}
