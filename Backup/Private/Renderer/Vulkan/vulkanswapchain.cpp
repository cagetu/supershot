#include "renderer.h"

VulkanSwapChain::VulkanSwapChain(VulkanDevice* device)
	: m_Device(device)
	, m_SwapChain(VK_NULL_HANDLE)
	, m_Surface(VK_NULL_HANDLE)
	, m_CurrentAcquiredImageIndex(UINT_MAX)
	, m_CurrentSemaphoreIndex(-1)
{
}
VulkanSwapChain::~VulkanSwapChain()
{
	_ASSERT(m_SwapChain == VK_NULL_HANDLE);
}

VkFormat ConvertFormat(uint32 format)
{
	static std::map<uint32, VkFormat> GTypeFormat =
	{
		std::make_pair(RT_B8G8R8A8, VK_FORMAT_B8G8R8A8_UNORM),
	};

	auto it = GTypeFormat.find(format);
	if (it != GTypeFormat.end())
	{
		return it->second;
	}

	return VK_FORMAT_UNDEFINED;
}

VkSurfaceFormatKHR GetSurfaceFormat(std::vector<VkSurfaceFormatKHR>& formats, VkFormat DesireFormat)
{
	static std::map<VkFormat, const char*> GSurfaceFormats =
	{
		std::make_pair(VK_FORMAT_UNDEFINED, "VK_FORMAT_UNDEFINED"),
		std::make_pair(VK_FORMAT_R8G8B8A8_UNORM, "VK_FORMAT_R8G8B8A8_UNORM"),
		std::make_pair(VK_FORMAT_R8G8B8A8_SRGB, "VK_FORMAT_R8G8B8A8_SRGB"),
		std::make_pair(VK_FORMAT_B8G8R8A8_UNORM, "VK_FORMAT_B8G8R8A8_UNORM"),
		std::make_pair(VK_FORMAT_B8G8R8A8_SRGB, "VK_FORMAT_B8G8R8A8_SRGB"),
	};

	for (uint32 index = 0; index < formats.size(); index++)
	{
		auto it = GSurfaceFormats.find(formats[index].format);
		if (it != GSurfaceFormats.end())
		{
			PlatformUtil::DebugOutFormat("%d: %s\n", index, it->second);
		}
	}

	if (formats.size() == 1 && formats[0].format == VK_FORMAT_UNDEFINED)
	{
		return{ VK_FORMAT_R8G8B8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for (VkSurfaceFormatKHR& surfaceFormat : formats)
	{
		if (surfaceFormat.format == DesireFormat)
			return surfaceFormat;
	}

	return formats[0];
}

VkPresentModeKHR GetSurfacePresentMode(std::vector<VkPresentModeKHR>& modes, VkPresentModeKHR DesireMode)
{
	for (VkPresentModeKHR& mode : modes)
	{
		if (mode == DesireMode)
			return DesireMode;
	}

	PlatformUtil::DebugOutFormat("%d PresentMode is not Support!! Select %d Mode\n", DesireMode, modes[0]);
	return modes[0];
}

VkSurfaceTransformFlagBitsKHR GetSurfaceTransform(VkSurfaceCapabilitiesKHR& surfaceCaps)
{
	if (surfaceCaps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
	{
		return VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	}
	else
	{
		return surfaceCaps.currentTransform;
	}
}

bool VulkanSwapChain::Create(VkInstance instance, void* windowHandle, int32& width, int32& height, uint32 requestImageCount, uint32 imageFormat)
{
	//
	// 1. Surface 생성
	//
#if TARGET_OS_WINDOWS
	VkWin32SurfaceCreateInfoKHR surfaceCreateInfo;
	Memory::Memzero(surfaceCreateInfo);
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.flags = 0;
	surfaceCreateInfo.pNext = nullptr;
	surfaceCreateInfo.hinstance = GetModuleHandle(nullptr);
	surfaceCreateInfo.hwnd = (HWND)windowHandle;
	VK_RESULT_ASSERT(::vkCreateWin32SurfaceKHR(instance, &surfaceCreateInfo, nullptr, &m_Surface));
#elif TARGET_OS_ANDROID
	VkAndroidSurfaceCreateInfoKHR surfaceCreateInfo;
	Memory::Memzero(surfaceCreateInfo);
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.window = (ANativeWindow*)WindowHandle;

	VK_RESULT_ASSERT(vkCreateAndroidSurfaceKHR(Instance, &surfaceCreateInfo, nullptr, &m_Surface));
#endif

	// 1-1. Surface 지원 여부 체크 (GraphicsQueue의 FamilyIndex에 대한 Surface가 Present를 지원하는지 체크)
	VkBool32 bSupportsPresent = VK_FALSE;
	VK_RESULT(vkGetPhysicalDeviceSurfaceSupportKHR(m_Device->GetGpuHandle(), m_Device->GetGfxQueue()->GetFamilyIndex(), m_Surface, &bSupportsPresent));
	_ASSERT(bSupportsPresent == VK_TRUE);

	//
	// 2. surfaceFormat 설정 
	//
	VkSurfaceFormatKHR surfaceFormat;
	Memory::Memzero(surfaceFormat);
	{
		uint32 count;
		VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(m_Device->GetGpuHandle(), m_Surface, &count, nullptr));
		_ASSERT(count > 0);

		std::vector<VkSurfaceFormatKHR> surfaceFormats(count);
		VK_RESULT(vkGetPhysicalDeviceSurfaceFormatsKHR(m_Device->GetGpuHandle(), m_Surface, &count, &surfaceFormats[0]));
		_ASSERT(surfaceFormats[0].format != VK_FORMAT_UNDEFINED);

		surfaceFormat = GetSurfaceFormat(surfaceFormats, ConvertFormat(imageFormat));
	}

	//
	// 3. Present Mode 설정
	//
	VkPresentModeKHR PresentMode = VK_PRESENT_MODE_FIFO_KHR;
	{
		uint32_t count;
		VK_RESULT(::vkGetPhysicalDeviceSurfacePresentModesKHR(m_Device->GetGpuHandle(), m_Surface, &count, nullptr));
		_ASSERT(count > 0);

		std::vector<VkPresentModeKHR> present_modes(count);
		VK_RESULT(vkGetPhysicalDeviceSurfacePresentModesKHR(m_Device->GetGpuHandle(), m_Surface, &count, &present_modes[0]));

		PresentMode = GetSurfacePresentMode(present_modes, PresentMode);
	}

	//
	// 4. SwapChain Capability 체크
	//
	VkSurfaceCapabilitiesKHR surfaceCaps;
	VK_RESULT(::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_Device->GetGpuHandle(), m_Surface, &surfaceCaps));

	//
	// 5. SwapChain 생성
	//
	VkSwapchainCreateInfoKHR createInfo;
	Memory::Memzero(createInfo);
	{
		/*
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT
				- 이미지가 Transfer Command의 Source로 사용될 수 있다.
			VK_IMAGE_USAGE_TRANSFER_DST_BIT
				- 이미지가 Transfer Command의 Destination로 사용될 수 있다.
			VK_IMAGE_USAGE_SAMPLED_BIT
				- 이미지가 VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE 혹은 VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER로 VkDescriptorSet slot을 사용하기 위해 적합한 VkImageView 생성에 사용되고, 셰이더에 의해 샘플 된다.
			VK_IMAGE_USAGE_STORAGE_BIT
				- 이미지가 VK_DESCRIPTOR_TYPE_STORAGE_IMAGE VkDescriptorSet slot 타입을 사용하기에 VkImageView를 생성하는데 사용된다.
			VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT
				- 이미지가 Color 로서 사용하기 위해 VkImageView를 생성하거나 VkFrameBuffer에 attachment를 resolve할 수 있다.
			VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT
				- 이미지가 VkFrameBuffer에 Depth/Stencil attachment로서 사용하기에 적합한 VkImageView를 생성하는데 사용된다.
			VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT 
				- VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT 를 가지고 할당한 이미지에 메모리를 바운드 한다. 
				- (color, resolve, depth/stencil, input attachment)로 사용하기에 적합한 VkImageView 생성되는 이미지에 대해서 설정될 수 있다.
			VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT
				- 이미지가 VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT VkDescriptorSet slot 타입을 사용하기에 적합한 VkImageView를 생성하는데 사용된다. 
				- input attachment로 셰이더로부터 읽어진다.
				- framebuffer에서 input attachment로서 사용된다.
		*/
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.pNext = nullptr;
		createInfo.flags = 0;
		createInfo.surface = m_Surface;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;		//! 이미지를 생성할 때 이미지의 처리 목적을 지정해야 한다.
		createInfo.minImageCount = Math::clamp(requestImageCount, surfaceCaps.minImageCount, surfaceCaps.maxImageCount);
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageArrayLayers = 1;
		createInfo.imageExtent.width = (surfaceCaps.currentExtent.width == -1 ? width : surfaceCaps.currentExtent.width);
		createInfo.imageExtent.height = (surfaceCaps.currentExtent.height == -1 ? height : surfaceCaps.currentExtent.height);
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;	// 오직 하나의 Queue Family가 한번에 하나의 Image를 사용할 때, "exclusive" sharing mode를 사용한다.
		//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
		//createInfo.queueFamilyIndexCount = 0;
		//createInfo.pQueueFamilyIndices = nullptr;
		createInfo.preTransform = GetSurfaceTransform(surfaceCaps);
		createInfo.presentMode = PresentMode;
		createInfo.oldSwapchain = VK_NULL_HANDLE;
		createInfo.clipped = VK_TRUE;
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	}
	VK_RESULT(::vkCreateSwapchainKHR(m_Device->GetHandle(), &createInfo, nullptr, &m_SwapChain));

	//
	// 6. Swapchain Image 얻기
	//
	GetImages(m_Images);

	//
	// 7. Image Semaphore 생성
	//
	uint32 imageCount = GetImageCount();
	for (uint32 i = 0; i < imageCount; i++)
	{
		m_ImageAcquiredSemaphore.push_back(new VulkanSemaphore(m_Device));
	}

	width = createInfo.imageExtent.width;
	height = createInfo.imageExtent.height;

	return true;
}

void VulkanSwapChain::Destroy()
{
	// Clean Semaphore
	for (VulkanSemaphore* semaphore : m_ImageAcquiredSemaphore)
		delete semaphore;
	m_ImageAcquiredSemaphore.clear();

	m_CurrentAcquiredImageIndex = UINT_MAX;
	m_CurrentSemaphoreIndex = -1;

	// Clean SwapChain
	if (m_SwapChain)
	{
		::vkDestroySwapchainKHR(m_Device->GetHandle(), m_SwapChain, nullptr);
		m_SwapChain = VK_NULL_HANDLE;
	}
}

void VulkanSwapChain::GetImages(std::vector<VkImage>& outImages)
{
	uint32 count;
	VK_RESULT(::vkGetSwapchainImagesKHR(m_Device->GetHandle(), m_SwapChain, &count, nullptr));

	outImages.resize(count);
	VK_RESULT(::vkGetSwapchainImagesKHR(m_Device->GetHandle(), m_SwapChain, &count, &outImages[0]));
}

void VulkanSwapChain::AcquireNextImageIndex(uint32& acquiredImageIndex, VulkanSemaphore** acquiredSemaphore)
{
	/*	- 우리가 그릴려고 하는 다음 Swapchain Image의 Index를 얻는다.
		- "infinite" timeout으로 설정하면, Image가 준비될 때까지 Block될 것이다.
		-  Semaphore는 Image가 준비될 때 Signal을 얻어올 것이다.
	*/
	m_CurrentSemaphoreIndex = (m_CurrentSemaphoreIndex + 1) % m_ImageAcquiredSemaphore.size();

	VkResult result = ::vkAcquireNextImageKHR(m_Device->GetHandle(), 
											  m_SwapChain,
											  UINT64_MAX,														// TIMEOUT = 무한대!!!
											  m_ImageAcquiredSemaphore[m_CurrentSemaphoreIndex]->GetHandle(),	// Presention Engine이 이미지 사용을 끝냈을 때 Signal된다. Image를 획득할 수 있을 때까지 대기
											  VK_NULL_HANDLE,													// Fence
											  &m_CurrentAcquiredImageIndex);
	_ASSERT(result == VK_SUCCESS || result == VK_SUBOPTIMAL_KHR);
	if (result == VK_SUBOPTIMAL_KHR) { PlatformUtil::DebugOutFormat("[AcquireNextImageIndex] VK_SUBOPTIMAL_KHR"); }

	acquiredImageIndex = m_CurrentAcquiredImageIndex;
	*acquiredSemaphore = m_ImageAcquiredSemaphore[m_CurrentSemaphoreIndex];
}

void VulkanSwapChain::Present(const VulkanQueue* Queue, VulkanSemaphore* renderingDoneSemaphore)
{
	_ASSERT(m_CurrentAcquiredImageIndex != UINT_MAX);

	VkPresentInfoKHR info;	
	Memory::Memzero(info);
	info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	info.pImageIndices = &m_CurrentAcquiredImageIndex;	// Present하기를 원하는 Image들의 인덱스를 포함한 Array. Index는 pSwapChain의 Array에 대응되는 인덱스이다.
	info.swapchainCount = 1;
	info.pSwapchains = &m_SwapChain;					// Image들을 Present하기를 원하는 모든 Swapchain의 핸들 Array.
	// Present가 발생할 수 있을 때까지 기다린다.
	if (renderingDoneSemaphore)
	{
		VkSemaphore waitSemaphore = renderingDoneSemaphore->GetHandle();
		info.pWaitSemaphores = &waitSemaphore;			// Queue가 기다려야 하는 Semaphore 핸들 Array
		info.waitSemaphoreCount = 1;					// 이미지를 Present 하기 전에 우리가 원하는 대기하는 Queue Semaphore의 수
	}

	VkResult result = ::vkQueuePresentKHR(Queue->GetHandle(), &info);
	if (result == VK_SUBOPTIMAL_KHR) { PlatformUtil::DebugOutFormat("[AcquireNextImageIndex] VK_SUBOPTIMAL_KHR"); }
	_ASSERT(result == VK_SUCCESS);

	VK_RESULT(::vkQueueWaitIdle(Queue->GetHandle()));
}
