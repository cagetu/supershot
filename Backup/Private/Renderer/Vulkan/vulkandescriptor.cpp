#include "renderer.h"

/*---------------------------------------------------------------------------------------------------------------------------------
//
/---------------------------------------------------------------------------------------------------------------------------------*/
VulkanDescriptorSetsLayout::VulkanDescriptorSetsLayout(VulkanDevice* device)
	: m_Device(device)
{
	Memory::Memzero(m_NumDescriptorsPerType);
}
VulkanDescriptorSetsLayout::~VulkanDescriptorSetsLayout()
{
	// :::::TODO:::::
	//	Fence 처리를 해서, Pending 처리를 해야 한다....

	for (VkDescriptorSetLayout& Handle : m_Handles)
	{
		vkDestroyDescriptorSetLayout(m_Device->GetHandle(), Handle, nullptr);
	}
}

// DescriptorSet에 바인딩된 DescriptorSetBinding을 추가한다.
void VulkanDescriptorSetsLayout::AddDescriptor(int32 descriptorSetIndex, const VkDescriptorSetLayoutBinding& descriptor)
{
	m_NumDescriptorsPerType[descriptor.descriptorType]++;

	m_SetLayouts[descriptorSetIndex].Bindings.push_back(descriptor);
}

void VulkanDescriptorSetsLayout::Compile()
{
	const VkPhysicalDeviceLimits& Limits = m_Device->GetLimits();

	// Check for maxDescriptorSetSamplers
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_SAMPLER] 
		+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER]
		<	Limits.maxDescriptorSetSamplers);

	// Check for maxDescriptorSetUniformBuffers
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER]
		+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC]
		<	Limits.maxDescriptorSetUniformBuffers);

	// Check for maxDescriptorSetUniformBuffersDynamic
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC]
		<	Limits.maxDescriptorSetUniformBuffersDynamic);

	// Check for maxDescriptorSetStorageBuffers
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER]
		+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC]
		<	Limits.maxDescriptorSetStorageBuffers);

	// Check for maxDescriptorSetStorageBuffersDynamic
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC]
		<	Limits.maxDescriptorSetStorageBuffersDynamic);

	// Check for maxDescriptorSetSampledImages
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER]
		+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE]
		+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER]
		<	Limits.maxDescriptorSetSampledImages);

	// Check for maxDescriptorSetStorageImages
	_ASSERT(m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_IMAGE]
		+ m_NumDescriptorsPerType[VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER]
		<	Limits.maxDescriptorSetStorageImages);

	m_Handles.reserve(m_SetLayouts.size());

	for (auto it : m_SetLayouts)
	{
		VkDescriptorSetLayoutCreateInfo layoutInfo;
		Memory::Memzero(layoutInfo);
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.pNext = nullptr;
		layoutInfo.bindingCount = it.second.Bindings.size();
		layoutInfo.pBindings = it.second.Bindings.data();

		VK_RESULT(vkCreateDescriptorSetLayout(m_Device->GetHandle(), &layoutInfo, nullptr, &it.second.handle));

		m_Handles.push_back(it.second.handle);
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------
//	DescriptorSetLayoutBinding -> DescriptorSetLayout -> PipelineLayout 생성
/---------------------------------------------------------------------------------------------------------------------------------*/
VulkanPipelineLayout::VulkanPipelineLayout(VulkanDevice* device, VulkanDescriptorSetsLayout* descriptorSetsLayout)
{
	m_Device = device;

	const std::vector<VkDescriptorSetLayout>& layoutHandles = descriptorSetsLayout->GetHandles();
	m_NumDescriptorSets = layoutHandles.size();

	VkPipelineLayoutCreateInfo CreateInfo;
	Memory::Memzero(CreateInfo);
	CreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	CreateInfo.pNext = nullptr;
	CreateInfo.setLayoutCount = layoutHandles.size();
	CreateInfo.pSetLayouts = layoutHandles.data();
	// PushConstant는 이후에 처리하자!!!
	CreateInfo.pushConstantRangeCount = 0;
	CreateInfo.pPushConstantRanges = nullptr;

	VK_RESULT(vkCreatePipelineLayout(device->GetHandle(), &CreateInfo, nullptr, &m_Handle));
}
VulkanPipelineLayout::~VulkanPipelineLayout()
{
	if (m_Handle != VK_NULL_HANDLE)
	{
		vkDestroyPipelineLayout(m_Device->GetHandle(), m_Handle, nullptr);
	}
}


/*---------------------------------------------------------------------------------------------------------------------------------
//
/---------------------------------------------------------------------------------------------------------------------------------*/
VulkanDescriptorSets::VulkanDescriptorSets(VulkanDevice* device, VulkanDescriptorSetsLayout* layout)
	: m_Device(device)
	, m_Pool(nullptr)
	, m_Layout(layout)
{
	m_Sets.resize(layout->GetHandles().size());

	// DescriptorSet 생성
	m_Pool = m_Device->AllocateDescriptorSets(*layout, m_Sets.data());
	m_Pool->TrackAddUsage(*layout);
}

VulkanDescriptorSets::~VulkanDescriptorSets()
{
	_ASSERT(m_Pool);
	m_Pool->TrackRemoveUsage(*m_Layout);

	if (m_Sets.empty() == false)
	{
		VK_RESULT_EXPAND(vkFreeDescriptorSets(m_Device->GetHandle(), m_Pool->Handle(), m_Sets.size(), m_Sets.data()));
	}
}

/*---------------------------------------------------------------------------------------------------------------------------------
//
/---------------------------------------------------------------------------------------------------------------------------------*/
VulkanDescriptorPool::VulkanDescriptorPool(VulkanDevice* device)
	: m_Device(device)
	, m_DescriptorPool(VK_NULL_HANDLE)
	, m_MaxDescriptorSets(0)
	, m_NumAllocatedDescriptorSets(0)
	, m_PeakAllocatedDescriptorSets(0)
{
	// Increased from 8192 to prevent Protostar crashing on Mali
	m_MaxDescriptorSets = 16384;
	Memory::Memzero(m_MaxAllocatedTypes);
	Memory::Memzero(m_NumAllocatedTypes);
	Memory::Memzero(m_PeakAllocatedTypes);

	//#todo-rco: Get some initial values
	uint32 LimitMaxUniformBuffers = 2048;
	uint32 LimitMaxSamplers = 1024;
	uint32 LimitMaxCombinedImageSamplers = 4096;
	uint32 LimitMaxUniformTexelBuffers = 512;

	std::vector<VkDescriptorPoolSize> poolTypes;
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		type.descriptorCount = LimitMaxUniformBuffers;
		poolTypes.push_back(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
		type.descriptorCount = LimitMaxUniformBuffers;
		poolTypes.push_back(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_SAMPLER;
		type.descriptorCount = LimitMaxSamplers;
		poolTypes.push_back(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		type.descriptorCount = LimitMaxCombinedImageSamplers;
		poolTypes.push_back(type);
	}
	{
		VkDescriptorPoolSize type;
		type.type = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
		type.descriptorCount = LimitMaxUniformTexelBuffers;
		poolTypes.push_back(type);
	}
	for (const VkDescriptorPoolSize& poolType : poolTypes)
	{
		m_MaxAllocatedTypes[poolType.type] = poolType.descriptorCount;
	}

	VkDescriptorPoolCreateInfo PoolInfo;
	Memory::Memzero(PoolInfo);
	PoolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	PoolInfo.poolSizeCount = poolTypes.size();
	PoolInfo.pPoolSizes = poolTypes.data();
	PoolInfo.maxSets = m_MaxDescriptorSets;;

	VK_RESULT(vkCreateDescriptorPool(m_Device->GetHandle(), &PoolInfo, nullptr, &m_DescriptorPool));
}
VulkanDescriptorPool::~VulkanDescriptorPool()
{
	if (m_DescriptorPool != VK_NULL_HANDLE)
	{
		vkDestroyDescriptorPool(m_Device->GetHandle(), m_DescriptorPool, nullptr);
		m_DescriptorPool = VK_NULL_HANDLE;
	}
}

bool VulkanDescriptorPool::CanAllocate(const VulkanDescriptorSetsLayout& Layout) const
{
	for (uint32 TypeIndex = VK_DESCRIPTOR_TYPE_BEGIN_RANGE; TypeIndex < VK_DESCRIPTOR_TYPE_END_RANGE; ++TypeIndex)
	{
		if (m_NumAllocatedTypes[TypeIndex] + (int32)Layout.NumUsedType((VkDescriptorType)TypeIndex) > m_MaxAllocatedTypes[TypeIndex])
		{
			return false;
		}
	}

	return true;
}

void VulkanDescriptorPool::TrackAddUsage(const VulkanDescriptorSetsLayout& Layout)
{
	for (uint32 TypeIndex = VK_DESCRIPTOR_TYPE_BEGIN_RANGE; TypeIndex < VK_DESCRIPTOR_TYPE_END_RANGE; ++TypeIndex)
	{
		m_NumAllocatedTypes[TypeIndex] += (int32)Layout.NumUsedType((VkDescriptorType)TypeIndex);
		m_PeakAllocatedTypes[TypeIndex] = Math::Max(m_PeakAllocatedTypes[TypeIndex], m_NumAllocatedTypes[TypeIndex]);
	}

	m_NumAllocatedDescriptorSets += Layout.GetLayouts().size();
	m_PeakAllocatedDescriptorSets = Math::Max(m_PeakAllocatedDescriptorSets, m_NumAllocatedDescriptorSets);
}

void VulkanDescriptorPool::TrackRemoveUsage(const VulkanDescriptorSetsLayout& Layout)
{
	for (uint32 TypeIndex = VK_DESCRIPTOR_TYPE_BEGIN_RANGE; TypeIndex < VK_DESCRIPTOR_TYPE_END_RANGE; ++TypeIndex)
	{
		m_NumAllocatedTypes[TypeIndex] -= (int32)Layout.NumUsedType((VkDescriptorType)TypeIndex);
		_ASSERT(m_NumAllocatedTypes[TypeIndex] >= 0);
	}

	m_NumAllocatedDescriptorSets -= Layout.GetLayouts().size();
}