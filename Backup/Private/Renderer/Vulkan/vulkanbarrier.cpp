#include "renderer.h"


namespace Vulkan
{
	/*	ImageLayout을 전환한다.
	- 이미지 생성하는 동안 우리가 원하는 다른 타입의 수행을 위해서 다른 usage flags를 명시해야만 한다.
	- 각 타입의 이미지 처리는 다른 Image Layout에 연결된다.
	*/
	void TransitionImageLayout(VulkanCmdBuffer* CmdBuffer, VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, const VkImageSubresourceRange& subresourceRange)
	{
		VkCommandBuffer cmdBuffer = CmdBuffer->Handle();

		//_ASSERT(CmdBuffer->GetState() == VulkanCmdBuffer::_STATE::BEGIN);
		_ASSERT(CmdBuffer->Handle() != VK_NULL_HANDLE);

		// Barrier는 우리가 기대했던 결과를 얻는다는 것을 확인하는 특수한 순서에 발생하는 우리의 operation이 GPU에서 완료되었다는 것을 확인한다.
		// 하나의 Barrier는 큐에서 두 가지 수행으로 나뉜다: Barrier 이전과 Barrier 이후
		// 
		VkImageMemoryBarrier imageBarrier;
		Memory::Memzero(imageBarrier);
		imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageBarrier.pNext = nullptr;
		imageBarrier.oldLayout = oldLayout;
		imageBarrier.newLayout = newLayout;
		imageBarrier.image = image;
		imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageBarrier.subresourceRange = subresourceRange;

		/*
		VK_IMAGE_LAYOUT_UNDEFINED
		- device 접근을 지원하지 않는다.
		- 이 layout은 반드시 initialLayout이나 image transition에서 oldLayout에서 사용되어야만 한다.
		- 이 layout의 밖으로 transition 될 때, 메모리의 내용이 보존된다 것을 보장하지 않는다.

		VK_IMAGE_LAYOUT_GENERAL
		- 모든 타입의 device 접근을 허용한다.

		VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		- 오직 VkFramebuffer에 color 혹은 resolve attachment로 사용되어야만 한다.
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들 대해서만 유효하다.

		VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		- 오직 VkFramebuffer에 depth/stencil attachment로 사용되어야만 한다.
		- 이 layout은 오직 활성화된 VK_IAMGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
		- 오직 VkFramebuffer에 depth/stencil attachment를 read-only로 사용되어야만 한다. (and/or) 셰이더 내에 read-only image로 사용된다. (samgpled image로 읽을 수 있고, image/sampler 와 input attachment를 조합할 수 있다.)
		- 이 layout은 오직 활성화된 VK_IAMGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
		- 셰이더 내에 read-only image로 사용된다. (samgpled image로 읽을 수 있고, image/sampler 와 input attachment를 조합할 수 있다.)
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_SAMPLED_BIT나 VK_IMAGE_USAGE_INPUT_ATTACHMENT usage bit를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
		- transfer command의 source image로 사용되어야만 한다. (VK_PIPELINE_STAGE_TRANSFER_BIT 참고)
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_TRANSFER_SRC_BIT를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
		- transfer command의 destination image로 사용되어야만 한다.
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_TRANSFER_DST_BIT를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_PREINITIALIZED
		- device 접근을 지원하지 않는다.
		- 이 layout은 반드시 initialLayout이나 image transition에서 oldLayout에서 사용되어야만 한다.
		- layout 밖으로 transition 될 때, 메모리의 내용은 보존된다.
		- 이 layout은 host에 의해 쓰여진 내용의 이미지에 대해 초기 layout으로 사용된다는 의도이다. 그리고, 이런 이유로 최초 layout transtion 실행없이 데이터는 즉시 메모리에 작성할 수 있다.
		- 현재 VK_IMAGE_LAYOUT_PREINITIALIZED는 오직 VK_IMAGE_TILTING_LINEAR 이미지에 유용하다. 왜냐하면, 이것은 VK_IMAGE_TILING_OPTIMAL 이미지로 정의된 standard layout 이 아니다.

		VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		- 디스플레이를 위해서 Swapchain 이미지를 Present하는데 사용되어야만 한다.
		- Swapchain 이미지는 반드시 VkQueuePresentKHR이 호출되기 전에 이 layout으로 transtion되어야만 한다.
		- VkQueuePresentKHR이 호출된 후에 이 layout으로 부터 transition 되어져야만 한다. (transition away)


		VK_ACCESS_INDIRECT_COMMAND_READ_BIT
		-
		VK_ACCESS_INDEX_READ_BIT = 0x00000002,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT = 0x00000004,
		VK_ACCESS_UNIFORM_READ_BIT = 0x00000008,
		VK_ACCESS_INPUT_ATTACHMENT_READ_BIT = 0x00000010,
		VK_ACCESS_SHADER_READ_BIT = 0x00000020,
		VK_ACCESS_SHADER_WRITE_BIT = 0x00000040,
		VK_ACCESS_COLOR_ATTACHMENT_READ_BIT = 0x00000080,
		VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT = 0x00000100,
		VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT = 0x00000200,
		VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT = 0x00000400,
		VK_ACCESS_TRANSFER_READ_BIT
		- access는 transfer operation로 부터 읽기를 한다.

		VK_ACCESS_TRANSFER_WRITE_BIT
		- access는 transfer operation로 부터 쓰기를 한다.

		VK_ACCESS_HOST_READ_BIT = 0x00002000,
		VK_ACCESS_HOST_WRITE_BIT = 0x00004000,
		VK_ACCESS_MEMORY_READ_BIT
		- 메모리에 attach된 명시되지 않은 unit을 읽는다.
		- 이 unit은 vulkan device 외부 이거나 코어 vulkan pipeline의 일부가 아니다.
		- dstAccessMask에 포함되어 있을 때, srcStageMask에 파이프라인 스테이지들에 의해 수행된 srcAccessMask에 access type을 사용한 모든 쓰기는 메모리에 보여져야만 한다.

		VK_ACCESS_MEMORY_WRITE_BIT = 0x00010000,
		VK_ACCESS_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF

		*/
		switch (newLayout)
		{
		case VK_IMAGE_LAYOUT_GENERAL:
			if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
			{
				imageBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			}
			break;
		case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
			if (oldLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
			{
				imageBarrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			}
			imageBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			break;
		case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
			imageBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
			break;
		case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL:
			_ASSERT(0);
			break;
		case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
			if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
			{
				imageBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			}
			imageBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
			break;
		case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
			_ASSERT(0);
			break;
		case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
			imageBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			break;
		case VK_IMAGE_LAYOUT_PREINITIALIZED:
			_ASSERT(0);
			break;
		case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
			if (oldLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
			{
				imageBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			}
			imageBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
			break;
		default:
			_ASSERT(0);
			break;
		}

		VkImageMemoryBarrier BarrierList[] = { imageBarrier };

		/*	Pipeline Stage Flags
		: logical pipeline 이벤트들이 시그널 될 수 있는 곳이 어딘지 명시. 실행 의존성의 source와 destination을 명시

		VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT
		- Command들이 처음에 Queue에 의해 받아들여지는 파이프라인의 Stage

		VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT
		- Draw/DispatchIndirect 데이터 구조체가 소비되는 파이프라인 Stage

		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
		- 버텍스와 인덱스 버퍼가 소비되는 파이프라인 Stage

		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT
		- Vertex Shader Stage

		VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT
		- Tessellation Control Shader Stage

		VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT
		- Tessellation Evaulation Shader Stage

		VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT
		- Geometry Shader Stage

		VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT
		- Fragment Shader Stage

		VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
		- 미리(early) fragment tests(fragment shading 호출 전에 depth나 stencil test)를 수행하는 파이프라인 스테이지

		VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT
		- 늦은(late) fragment tests(fragment shading 호출 후 depth,stencil test)를 수행하는 파이프라인 스테이지

		VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
		- 최종 칼라값이 pipeline으로부터 출력되고 Blending 이후 파이프라인 스테이지
		- 이 stage는 subpass의 마지막에 발생하는 resolve 오퍼레이션이 포함된다.
		- 값들이 메모리에 Commit 되어지는 불필요한 지시를 하지 말아야 한다.

		VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT
		- Compute Shader의 실행

		VK_PIPELINE_STAGE_TRANSFER_BIT
		- Copy Command의 실행
		- 이것은 모든 transfer commands로 부터 수행 결과를 포함한다.
		- transfer command 의 세트는 vkCmdCopyBuffer, vkCmdCopyImage, vkCmdBlitImage, VkCmdCopyBufferToImage, vkCmdCopyImageToBuffer,
		vkCmdUpdateBuffer, vkCmdFillBuffer, vkCmdClearColorImage, vkCmdClearDepthStencilImage, vkCmdResolveImage, vkCmdCopyQueryPoolResults로 구성된다.

		VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
		- Command가 실행이 완료되는 파이프라인에서 Final Stage

		VK_PIPELINE_STAGE_HOST_BIT
		- 디바이스 메모리의 읽기/쓰기 호스트에 실행을 지시하는 pseudo-stage

		VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT
		- 모든 graphics 파이프라인 Stage의 실행

		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT
		- Queue에 지원되는 모든 Stage의 실행
		*/
		VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

		if (oldLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
		{
			srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		}
		else if (newLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
		{
			srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
		}

		/*	Pipeline Barrier
		- Command buffer내 초기(eariler) command의 세트와 command buffer내 이후(later) command의 세트 사이의 메모리 의존성과 실행 의존성을 추가한다.
		- dstStageMask에 오직 VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT와 실행 의존성은 다음 순서의 Commands를 지연시키지 않을 것이다.
		- VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT는 다음 access가 다른 queue에서나 presentation engine에 의해 완료되었을 memory barriers와 layout transition을 완수하는데 유용하다.
		- 이 경우에 같은 Queue에서 다음 순서의 command들은 대기할 필요가 없지만, barrier나 transition은 반드시 batch 시그널과 관련된 이전의 세마포어들이 완료되어야만 한다.
		- 만약 구현이 파이프라인의 어떤 명시된 스테이지에 이벤트 스테이트를 업데이트를 할 수 없다면, 대신에 논리적으로 마지막 Stage에 업데이트한다.
		*/
		::vkCmdPipelineBarrier(cmdBuffer,
			srcStageMask,	//  
			dstStageMask,
			0,				// VkDependencyFlags 
			0, nullptr,		// MemoryBarrier
			0, nullptr,		// BufferMemoryBarrier
			1, BarrierList);	// ImageMemoryBarrier
	}

	void TransitionImageLayout(VulkanCmdBuffer* CmdBuffer, VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout)
	{
		TransitionImageLayout(CmdBuffer, image, oldLayout, newLayout, DefaultSubresourceRange());
	}

	void TransitionImageLayout(VulkanCmdBuffer* CmdBuffer, VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, VkAccessFlags srcAccessMask, VkAccessFlags dstAccessMask,
		VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask, const VkImageSubresourceRange& subresourceRange)
	{
		VkCommandBuffer cmdBuffer = CmdBuffer->Handle();
		_ASSERT(cmdBuffer != VK_NULL_HANDLE);
		//_ASSERT(CmdBuffer->GetState() == VulkanCmdBuffer::_STATE::BEGIN);

		VkImageMemoryBarrier imageBarrier;
		Memory::Memzero(imageBarrier);
		imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		imageBarrier.pNext = nullptr;
		//
		imageBarrier.image = image;
		imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		imageBarrier.oldLayout = oldLayout;
		imageBarrier.newLayout = newLayout;
		//
		imageBarrier.srcAccessMask = srcAccessMask;
		imageBarrier.dstAccessMask = dstAccessMask;
		imageBarrier.subresourceRange = subresourceRange;

		VkImageMemoryBarrier BarrierList[] = { imageBarrier };
		::vkCmdPipelineBarrier(cmdBuffer, srcStageMask, dstStageMask, 0, 0, nullptr, 0, nullptr, 1, BarrierList);
	}
}