#include "renderer.h"

__ImplementRtti(VisualShock, VulkanRenderTarget, IRenderTarget);

VulkanRenderTarget::VulkanRenderTarget(VulkanDevice* device)
	: m_Device(device)
	, m_NeedUpdateDescription(false)
{
}

VulkanRenderTarget::~VulkanRenderTarget()
{
}

bool VulkanRenderTarget::Create(int width, int height, int32 multisample)
{
	m_Width = width;
	m_Height = height;
	m_Multisample = multisample;

	return true;
}

void VulkanRenderTarget::AddRenderTexture(const Ptr<VulkanRenderTexture2D>& surface, RTLoadAction loadAction, RTStoreAction storeAction)
{
	ColorSurface surfaceInfo;
	{
		surfaceInfo.surface = surface;
		surfaceInfo.loadAction = loadAction;
		surfaceInfo.storeAction = storeAction;
	}
	m_ColorSurfaces.push_back(surfaceInfo);

	m_NeedUpdateDescription = true;
}

IRenderTexture2D* VulkanRenderTarget::AddRenderTexture(int32 format, RTLoadAction loadAction, RTStoreAction storeAction)
{
	VulkanRenderTexture2D* surface = (VulkanRenderTexture2D*)CreateRenderTexture();
	if (surface->Create(m_Width, m_Height, format) == false)
	{
		delete surface;
		return NULL;
	}

	ColorSurface surfaceInfo;
	{
		surfaceInfo.surface = surface;
		surfaceInfo.loadAction = loadAction;
		surfaceInfo.storeAction = storeAction;
	}
	m_ColorSurfaces.push_back(surfaceInfo);

	m_NeedUpdateDescription = true;

	return surface;
}

IRenderTexture2D* VulkanRenderTarget::GetRenderTexture(int32 channel) const
{
	return m_ColorSurfaces.empty() == false && channel < (int32)m_ColorSurfaces.size() ? m_ColorSurfaces[channel].surface : NULL;
}

IRenderTexture2D* VulkanRenderTarget::GetRenderTexture(const TCHAR* id) const
{
	for (auto const it : m_ColorSurfaces)
	{
		if (!unicode::strcmp(it.surface->GetID().c_str(), id))
			return it.surface;
	}
	return NULL;
}

void VulkanRenderTarget::ClearRenderTextures()
{
	m_ColorSurfaces.clear();

	m_NeedUpdateDescription = true;
}

void VulkanRenderTarget::CreateDepthBuffer(int format, RTLoadAction loadAction, RTStoreAction storeAction, RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction)
{
	VulkanDepthStencilSurface* depthStencil = new VulkanDepthStencilSurface(m_Device);
	if (depthStencil->Create(m_Width, m_Height, format, m_Multisample) == false)
	{
		delete depthStencil;
		return;
	}

	m_DepthStencilSurface.surface.Null();

	m_DepthStencilSurface.surface = depthStencil;
	m_DepthStencilSurface.loadAction = loadAction;
	m_DepthStencilSurface.storeAction = storeAction;
	m_DepthStencilSurface.stencilLoadAction = stencilLoadAction;
	m_DepthStencilSurface.stencilStoreAction = stencilStoreAction;

	m_NeedUpdateDescription = true;
}

void VulkanRenderTarget::SetRenderTexture(const Ptr<VulkanRenderTexture2D>& surface, RTLoadAction loadAction, RTStoreAction storeAction)
{
	if (!m_ColorSurfaces.empty())
	{
		if (m_ColorSurfaces[0].surface == surface.Object() && m_ColorSurfaces[0].loadAction == loadAction && m_ColorSurfaces[0].storeAction == storeAction)
			return;
	}

	ClearRenderTextures();
	AddRenderTexture(surface, loadAction, storeAction);
}

void VulkanRenderTarget::SetDepthBuffer(const Ptr<IDepthStencilSurface>& surface, RTLoadAction loadAction, RTStoreAction storeAction, RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction)
{
	if (m_DepthStencilSurface.surface == surface.Object() && m_DepthStencilSurface.loadAction == loadAction && m_DepthStencilSurface.storeAction == storeAction &&
		m_DepthStencilSurface.stencilLoadAction == stencilLoadAction && m_DepthStencilSurface.stencilStoreAction == stencilStoreAction)
		return;

	_ASSERT(surface->IsExactOf(&VulkanDepthStencilSurface::RTTI) == true);

	m_DepthStencilSurface.surface.Null();
	
	m_DepthStencilSurface.surface = surface;
	m_DepthStencilSurface.loadAction = loadAction;
	m_DepthStencilSurface.storeAction = storeAction;
	m_DepthStencilSurface.stencilLoadAction = stencilLoadAction;
	m_DepthStencilSurface.stencilStoreAction = stencilStoreAction;

	m_NeedUpdateDescription = true;
}

const Ptr<IDepthStencilSurface>& VulkanRenderTarget::GetDepthBuffer() const
{
	return m_DepthStencilSurface.surface;
}

const Vulkan::RenderTargetDesc& VulkanRenderTarget::GetDescription() const
{
	UpdateRenderTargetDesc();

	return m_Description;
}

void VulkanRenderTarget::UpdateRenderTargetDesc() const
{
	if (m_NeedUpdateDescription)
	{
		m_Description.Clear();

		m_Description.width = m_Width;
		m_Description.height = m_Height;

		/*
			vkQueuePresentKHR, I get this and I do not know what is means:
			vkQueuePresentKHR(): Cannot read invalid swapchain image 7fe47c383f80, please fill the memory before using.
		*/
		for (auto it : m_ColorSurfaces)
		{
			VulkanRenderTexture2D* surface = it.surface;

			m_Description.AddColorAttachment(m_Device, surface->GetView().GetHandle(), surface->GetFormat(), m_Multisample, it.loadAction, it.storeAction, m_ClearColor);
		}

		if (m_DepthStencilSurface.surface.IsValid())
		{
			VulkanDepthStencilSurface* surface = m_DepthStencilSurface.surface.Cast<VulkanDepthStencilSurface>();
			ASSERT(surface != NULL, "VulkanDepthStencilSurface is NULL");

			m_Description.AddDepthStencilAttachment(m_Device, surface->GetView().GetHandle(), surface->GetFormat(), m_Multisample, 
													m_DepthStencilSurface.loadAction, m_DepthStencilSurface.storeAction, m_ClearDepth,
													m_DepthStencilSurface.stencilLoadAction, m_DepthStencilSurface.stencilStoreAction, m_ClearStencil);
		}

		m_NeedUpdateDescription = false;
	}
}

void VulkanRenderTarget::Begin()
{
	// BeginDraw
}

void VulkanRenderTarget::End()
{
	// EndDraw
}