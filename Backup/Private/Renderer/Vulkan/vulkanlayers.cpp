#include "renderer.h"

#if VULKAN_HAS_DEBUGGING_ENABLED

#if VULKAN_ENABLE_DRAW_MARKERS
#define RENDERDOC_LAYER_NAME	"VK_LAYER_RENDERDOC_Capture"
#endif

#define VULKAN_ENABLE_STANDARD_VALIDATION	1

// intance에 대해서 활성화 되기를 원하는 유효한 layer의 리스트
static const char* GRequiredLayersInstance[] =
{
	"VK_LAYER_LUNARG_swapchain",
};

// instance에 대해서 활성화 되기를 원하는 유효한 layer의 리스트
static const char* GValidationLayersInstance[] =
{
#if VULKAN_ENABLE_API_DUMP
	"VK_LAYER_LUNARG_api_dump",
#endif

#if VULKAN_ENABLE_STANDARD_VALIDATION
	"VK_LAYER_LUNARG_standard_validation",
#else
	"VK_LAYER_GOOGLE_threading",
	"VK_LAYER_LUNARG_parameter_validation",
	"VK_LAYER_LUNARG_object_tracker",
	"VK_LAYER_LUNARG_image",
	"VK_LAYER_LUNARG_core_validation",
	"VK_LAYER_LUNARG_swapchain",
	"VK_LAYER_GOOGLE_unique_objects",
#endif

	"VK_LAYER_LUNARG_device_limits",
	//"VK_LAYER_LUNARG_screenshot",
	//"VK_LAYER_NV_optimus",
	//"VK_LAYER_LUNARG_vktrace",		// Useful for future
};

// Instance Extensions to enable
static const char* GInstanceExtensions[] =
{
	VK_KHR_SURFACE_EXTENSION_NAME,
#if TARGET_OS_ANDROID
	VK_KHR_ANDROID_SURFACE_EXTENSION_NAME,
#else
	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#endif
#if !VULKAN_DISABLE_DEBUG_CALLBACK
	VK_EXT_DEBUG_REPORT_EXTENSION_NAME
#endif
};

struct LayerExtension
{
	VkLayerProperties LayerProps;
	std::vector<VkExtensionProperties> ExtensionProps;
};

// Instance 레벨 확장 지원여부
// LayerName이 null이면 모든 layer 대해서 
void GetInstanceLayerExtensions(const char* LayerName, LayerExtension& OutLayer)
{
	VkResult Result;
	do
	{
		// 3번째 인자를 null로 해서 total number 획득
		uint32 Count = 0;
		Result = ::vkEnumerateInstanceExtensionProperties(LayerName, &Count, nullptr);
		_ASSERT(Result >= VK_SUCCESS);

		// 3번째 인자를 저장할 포인트를 넘겨서 다시 호출
		if (Count > 0)
		{
			OutLayer.ExtensionProps.clear();
			OutLayer.ExtensionProps.resize(Count);
			Result = ::vkEnumerateInstanceExtensionProperties(LayerName, &Count, &OutLayer.ExtensionProps[0]);
			_ASSERT(Result >= VK_SUCCESS);
		}
	} while (Result == VK_INCOMPLETE);
}

void VulkanDriver::GetInstanceLayersAndExtensions(std::vector<const char*>& outInstanceExtensions, std::vector<const char*>& OutInstanceLayers)
{
	LayerExtension globalLayerExtensions;

	GetInstanceLayerExtensions(nullptr, globalLayerExtensions);

	VkResult Result;
	
	// Now per layer
	std::vector<LayerExtension> GlobalLayers;
	
	int32 TotalLayerCount = 0;
	VkLayerProperties GlobalLayerProperties[32];
	PlatformUtil::Memzero(GlobalLayerProperties, sizeof(GlobalLayerProperties));

	do
	{
		uint32 InstanceLayerCount = 0;
		Result = vkEnumerateInstanceLayerProperties(&InstanceLayerCount, nullptr);
		_ASSERT(Result >= VK_SUCCESS);
	
		if (InstanceLayerCount > 0)
		{
			Result = vkEnumerateInstanceLayerProperties(&InstanceLayerCount, &GlobalLayerProperties[TotalLayerCount]);
			_ASSERT(Result >= VK_SUCCESS);
		}
	
		TotalLayerCount += InstanceLayerCount;
	
	} while (Result == VK_INCOMPLETE);

	for (int32 i = 0; i < TotalLayerCount; i++)
	{
		LayerExtension layer;
		layer.LayerProps = GlobalLayerProperties[i];
		GetInstanceLayerExtensions(GlobalLayerProperties[i].layerName, layer);
		GlobalLayers.push_back(layer);
	}
	
#if VULKAN_HAS_DEBUGGING_ENABLED
	// Verify that all required instance layers are available
	for (uint32 LayerIndex = 0; LayerIndex < _countof(GRequiredLayersInstance); ++LayerIndex)
	{
		bool bValidationFound = false;
		const char* CurrValidationLayer = GRequiredLayersInstance[LayerIndex];
		for (uint32 Index = 0; Index < GlobalLayers.size(); ++Index)
		{
			if (!::strcmp(GlobalLayers[Index].LayerProps.layerName, CurrValidationLayer))
			{
				bValidationFound = true;
				OutInstanceLayers.push_back(CurrValidationLayer);
				break;
			}
		}

		if (!bValidationFound)
		{
			PlatformUtil::DebugOutFormat("Unable to find Vulkan Instance Required Layer: %s\n", CurrValidationLayer);
		}
	}

	// Verify that all requested debugging device-layers are available
	for (uint32 LayerIndex = 0; LayerIndex < _countof(GValidationLayersInstance); ++LayerIndex)
	{
		bool bValidationFound = false;
		const char* CurrValidationLayer = GValidationLayersInstance[LayerIndex];
		for (uint32 Index = 0; Index < GlobalLayers.size(); ++Index)
		{
			if (!::strcmp(GlobalLayers[Index].LayerProps.layerName, CurrValidationLayer))
			{
				bValidationFound = true;
				OutInstanceLayers.push_back(CurrValidationLayer);
				break;
			}
		}

		if (!bValidationFound)
		{
			PlatformUtil::DebugOutFormat("Unable to find Vulkan Instance validation Layer: %s\n", CurrValidationLayer);
		}
	}
#endif	// VULKAN_HAS_DEBUGGING_ENABLED

	for (ushort i = 0; i < globalLayerExtensions.ExtensionProps.size(); i++)
	{
		for (int32 j = 0; j < _countof(GInstanceExtensions); j++)
		{
			if (!::strcmp(globalLayerExtensions.ExtensionProps[i].extensionName, GInstanceExtensions[j]))
			{
				outInstanceExtensions.push_back(GInstanceExtensions[j]);
				break;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////

	if (outInstanceExtensions.size() > 0)
	{
		PlatformUtil::DebugOut("Using Instance Extenstions.\n");
		for (const char* Extension : outInstanceExtensions)
		{
			PlatformUtil::DebugOutFormat("* %s \n", Extension);
		}
	}

	if (OutInstanceLayers.size() > 0)
	{
		PlatformUtil::DebugOut("Using Instance layers. \n");
		for (const char* Layer : OutInstanceLayers)
		{
			PlatformUtil::DebugOutFormat("* %s \n", Layer);
		}
	}
}

//*********************************************************************************************************
//
//*********************************************************************************************************

// List of validation layers which we want to activate for the device
static const char* GRequiredLayersDevice[] =
{
	"VK_LAYER_LUNARG_swapchain",
};

// List of validation layers which we want to activate for the device
static const char* GValidationLayersDevice[] =
{
#if VULKAN_ENABLE_API_DUMP
	"VK_LAYER_LUNARG_api_dump",
#endif

#if VULKAN_ENABLE_STANDARD_VALIDATION
	"VK_LAYER_LUNARG_standard_validation",
#else
	"VK_LAYER_GOOGLE_threading",
	"VK_LAYER_LUNARG_parameter_validation",
	"VK_LAYER_LUNARG_object_tracker",
	"VK_LAYER_LUNARG_image",
	"VK_LAYER_LUNARG_core_validation",
	"VK_LAYER_LUNARG_swapchain",
	"VK_LAYER_GOOGLE_unique_objects",
#endif

	"VK_LAYER_LUNARG_device_limits",
	//"VK_LAYER_LUNARG_screenshot",
	//"VK_LAYER_NV_optimus",
	//"VK_LAYER_LUNARG_vktrace",		// Useful for future
};
#endif // VULKAN_HAS_DEBUGGING_ENABLED


// Device Extensions to enable
static const char* GDeviceExtensions[] =
{
	//	VK_KHR_SURFACE_EXTENSION_NAME,			// Not supported, even if it's reported as a valid extension... (SDK/driver bug?)
#if TARGET_OS_ANDROID
	VK_KHR_SURFACE_EXTENSION_NAME,
	VK_KHR_ANDROID_SURFACE_EXTENSION_NAME,
#else
	//	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,	// Not supported, even if it's reported as a valid extension... (SDK/driver bug?)
#endif
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};

// Device 레벨 확장 지원여부
// LayerName이 null이면 모든 layer 대해서 
static inline void GetDeviceLayerExtensions(VkPhysicalDevice Device, const char* LayerName, LayerExtension& OutLayer)
{
	VkResult Result;
	do
	{
		// 3번째 인자를 null로 해서 total number 획득
		uint32 Count = 0;
		Result = vkEnumerateDeviceExtensionProperties(Device, LayerName, &Count, nullptr);
		_ASSERT(Result >= VK_SUCCESS);

		// 3번째 인자를 저장할 포인트를 넘겨서 다시 호출
		if (Count > 0)
		{
			OutLayer.ExtensionProps.clear();
			OutLayer.ExtensionProps.resize(Count);
			Result = vkEnumerateDeviceExtensionProperties(Device, LayerName, &Count, &OutLayer.ExtensionProps[0]);
			_ASSERT(Result >= VK_SUCCESS);
		}
	} while (Result == VK_INCOMPLETE);
}

void VulkanDevice::GetDeviceLayerAndExtensions(std::vector<const char*>& deviceExtensions, std::vector<const char*>& deviceLayers, bool& outDebugMarkers)
{
	// Setup device layer properties
	uint32 Count = 0;
	::vkEnumerateDeviceLayerProperties(m_Gpu, &Count, nullptr);

	std::vector<VkLayerProperties> LayerProperties(Count);
	::vkEnumerateDeviceLayerProperties(m_Gpu, &Count, &LayerProperties[0]);

#if VULKAN_HAS_DEBUGGING_ENABLED

	bool bRenderDocFound = false;
#if VULKAN_ENABLE_DRAW_MARKERS
	bool bDebugExtMarkerFound = false;
	for (uint32 Index = 0; Index < LayerProperties.size(); ++Index)
	{
		if (!::strcmp(LayerProperties[Index].layerName, RENDERDOC_LAYER_NAME))
		{
			bRenderDocFound = true;
			break;
		}
		else if (!::strcmp(LayerProperties[Index].layerName, VK_EXT_DEBUG_MARKER_EXTENSION_NAME))
		{
			bDebugExtMarkerFound = true;
			break;
		}
	}
#endif


	// Verify that all required device layers are available
	for (uint32 LayerIndex = 0; LayerIndex < _countof(GRequiredLayersDevice); ++LayerIndex)
	{
		bool bValidationFound = false;
		const char* CurrValidationLayer = GRequiredLayersDevice[LayerIndex];
		for (uint32 Index = 0; Index < LayerProperties.size(); ++Index)
		{
			if (!::strcmp(LayerProperties[Index].layerName, CurrValidationLayer))
			{
				bValidationFound = true;
				deviceLayers.push_back(CurrValidationLayer);
				break;
			}
		}

		if (!bValidationFound)
		{
			PlatformUtil::DebugOutFormat("Unable to find Vulkan device Layer: %s\n", CurrValidationLayer);
		}
	}

	// Verify that all requested debugging device-layers are available. Skip validation layers under RenderDoc
	if (!bRenderDocFound)
	{
		for (uint32 LayerIndex = 0; LayerIndex < _countof(GValidationLayersDevice); ++LayerIndex)
		{
			bool bValidationFound = false;
			const char* CurrValidationLayer = GValidationLayersDevice[LayerIndex];
			for (uint32 Index = 0; Index < LayerProperties.size(); ++Index)
			{
				if (!::strcmp(LayerProperties[Index].layerName, CurrValidationLayer))
				{
					bValidationFound = true;
					deviceLayers.push_back(CurrValidationLayer);
					break;
				}
			}

			if (!bValidationFound)
			{
				PlatformUtil::DebugOutFormat("Unable to find Vulkan validation device Layer: %s\n", CurrValidationLayer);
			}
		}
	}
#endif

	LayerExtension Extensions;
	GetDeviceLayerExtensions(m_Gpu, nullptr, Extensions);

	for (uint32 Index = 0; Index < _countof(GDeviceExtensions); ++Index)
	{
		for (uint32 i = 0; i < Extensions.ExtensionProps.size(); i++)
		{
			if (!::strcmp(GDeviceExtensions[Index], Extensions.ExtensionProps[i].extensionName))
			{
				deviceExtensions.push_back(GDeviceExtensions[Index]);
				break;
			}
		}
	}

#if VULKAN_ENABLE_DRAW_MARKERS
	if (bRenderDocFound)
	{
		for (uint32 i = 0; i < Extensions.ExtensionProps.size(); i++)
		{
			if (!::strcmp(Extensions.ExtensionProps[i].extensionName, VK_EXT_DEBUG_MARKER_EXTENSION_NAME))
			{
				deviceExtensions.push_back(VK_EXT_DEBUG_MARKER_EXTENSION_NAME);
				outDebugMarkers = true;
				break;
			}
		}
	}
#endif

	//////////////////////////////////////////////////////////////////////

	if (deviceExtensions.size() > 0)
	{
		PlatformUtil::DebugOut("Using Device Extenstions.\n");
		for (const char* Extension : deviceExtensions)
		{
			PlatformUtil::DebugOutFormat("* %s \n", Extension);
		}
	}

	if (deviceLayers.size() > 0)
	{
		PlatformUtil::DebugOut("Using Device layers. \n");
		for (const char* Layer : deviceLayers)
		{
			PlatformUtil::DebugOutFormat("* %s \n", Layer);
		}
	}
}