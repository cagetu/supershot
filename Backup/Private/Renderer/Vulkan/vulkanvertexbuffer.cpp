#include "renderer.h"

__ImplementRtti(VisualShock, VulkanVertexBuffer, VertexBuffer);

VulkanVertexBuffer::VulkanVertexBuffer(VulkanDevice* device)
	: IVulkanBuffer(device)
	, m_StagingBuffer(nullptr)
{
}
VulkanVertexBuffer::~VulkanVertexBuffer()
{
}

bool VulkanVertexBuffer::Create(uint32 fvf, int32 vertexCount)
{
	int32 stride = VertexDescription::GetStride(fvf);
	int32 byteSize = stride * vertexCount;

	if (CreateBuffer(byteSize, BUF_Static) == false)
		return false;

	m_FVF = fvf;
	m_Stride = stride;
	m_Length = byteSize;
	return true;
}

bool VulkanVertexBuffer::Create(uint32 fvf, int32 vertexCount, void* vertexData)
{
	if (Create(fvf, vertexCount) == false)
		return false;

	void* ptr = Lock(m_Stride, vertexCount);
	{
		Memory::Memcopy(ptr, vertexData, m_Length);
	}
	Unlock();

	return true;
}

void VulkanVertexBuffer::Destroy()
{
	VertexBuffer::Destroy();
	IVulkanBuffer::DestroyBuffer();

	if (m_StagingBuffer)
	{
		m_Device->StagingBufferPool().Release(nullptr, m_StagingBuffer);
		m_StagingBuffer = nullptr;
	}
}

bool VulkanVertexBuffer::CreateBuffer(uint32 bufferSize, uint32 usage)
{
	IVulkanBuffer::CreateBuffer(bufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	m_Length = bufferSize;
	m_BufferUsage = usage;
	return true;
}

void* VulkanVertexBuffer::Lock(int32 stride, int32 numVert, int32 lockFlag)
{
	if (m_BufferUsage&BUF_Static || m_BufferUsage&BUF_Dynamic)
	{
		_ASSERT(stride * numVert <= m_Length);

		int32 bufferSize = stride * numVert;

		m_StagingBuffer = m_Device->StagingBufferPool().Allocate(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
		return m_StagingBuffer->Lock();
	}
	return NULL;
}

void VulkanVertexBuffer::Unlock()
{
	/*
		BeginCmdBuffer
		CopyBuffer
		EndCmdBuffer
		**** Fence ****
		Submit
		WaitFence

		FreeCmdBuffer
		DestroyBuffer(stagingBuffer)
		FreeMemory(stagingMemory);
	*/
	if (m_BufferUsage&BUF_Static || m_BufferUsage&BUF_Dynamic)
	{
		m_StagingBuffer->Unlock();

		/////

		VulkanCmdBuffer* CmdBuffer = m_Device->CommandBufferPool().GetUploadCommandBuffer();
		CmdBuffer->CopyBuffer(m_StagingBuffer->GetHandle(), 0, m_Buffer, m_Offset, m_Length);
		//m_Device->CommandBufferPool().SubmitUploadCmdBuffer(true);

		m_Device->StagingBufferPool().Release(CmdBuffer, m_StagingBuffer);
		m_StagingBuffer = nullptr;
	}
}

void VulkanVertexBuffer::SetStreamSource(int32 channel, int32 offset)
{

}

unsigned char* VulkanVertexBuffer::GetBytes() const
{
	return NULL;
}


