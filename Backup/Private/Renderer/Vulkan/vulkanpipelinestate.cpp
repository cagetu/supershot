#include "renderer.h"

void GfxPipelineContext::SetShader(VulkanShader* shader)
{
	Shader = shader;
	VertexInputState.layout = shader->GetVertexAttributes();
}

void GfxPipelineContext::Clear()
{
	Memory::Memzero(ViewportState);
	Memory::Memzero(ScissorState);
	Memory::Memzero(InputAssemblyState);
	Memory::Memzero(VertexInputState);
	Memory::Memzero(TessellationState);
	Memory::Memzero(RasterizationState);
	Memory::Memzero(MultiSampleState);
	Memory::Memzero(DepthBiasState);
	Memory::Memzero(DepthBoundTestState);
	Memory::Memzero(DepthTestState);
	Memory::Memzero(StencilTestState);
	Memory::Memzero(BlendConstants);
	Memory::Memzero(ColorBlendState);

	Shader = NULL;

	RenderPass = NULL;
	SubPassIndex = 0;

	DynamicState = DYNAMICSTATE::_VIEWPORT | DYNAMICSTATE::_SCISSOR;
}

GfxPipelineContext& GfxPipelineContext::operator = (const GfxPipelineContext& rhs)
{
	Shader = rhs.Shader;

	RenderPass = rhs.RenderPass;
	SubPassIndex = rhs.SubPassIndex;

	VertexInputState.layout = rhs.VertexInputState.layout;

	InputAssemblyState = rhs.InputAssemblyState;
	ViewportState = rhs.ViewportState;
	ScissorState = rhs.ScissorState;
	MultiSampleState = rhs.MultiSampleState;
	TessellationState = rhs.TessellationState;
	DepthBiasState = rhs.DepthBiasState;
	DepthBoundTestState = rhs.DepthBoundTestState;
	StencilTestState = rhs.StencilTestState;
	TessellationState = rhs.TessellationState;
	RasterizationState = rhs.RasterizationState;
	for (int32 i = 0; i < 4; i++)
		BlendConstants.blendConstants[i] = rhs.BlendConstants.blendConstants[i];

	for (int32 i = 0; i < _MAX_MRT; i++)
	{
		ColorBlendState[i] = rhs.ColorBlendState[i];
	}

	return *this;
}

bool GfxPipelineContext::operator == (const GfxPipelineContext& rhs) const
{
	// 셰이더에 대해서...
	if (!Shader->CompareTo(rhs.Shader))
		return false;

	if (this->RenderPass != rhs.RenderPass || this->SubPassIndex != rhs.SubPassIndex)
		return false;

	//if (this->ViewportState != rhs.ViewportState)
	//	return false;
	//if (ScissorState != rhs.ScissorState)
	//	return false;

	if (TessellationState.patchControlPoints != rhs.TessellationState.patchControlPoints)
		return false;

	if (this->VertexInputState.layout->CompareTo(rhs.VertexInputState.layout) == false)
		return false;
	if (this->InputAssemblyState.topology != rhs.InputAssemblyState.topology)
		return false;
	if (RasterizationState != rhs.RasterizationState)
		return false;
	if (MultiSampleState != rhs.MultiSampleState)
		return false;
	if (DepthBiasState != rhs.DepthBiasState)
		return false;
	if (DepthBoundTestState != rhs.DepthBoundTestState)
		return false;
	if (DepthTestState != rhs.DepthTestState)
		return false;
	if (StencilTestState != rhs.StencilTestState)
		return false;
	if (BlendConstants != rhs.BlendConstants)
		return false;

	for (int32 i = 0; i < _MAX_MRT; i++)
	{
		if (ColorBlendState[i] != rhs.ColorBlendState[i])
			return false;
	}

	return true;
}

VkPipelineViewportStateCreateInfo CreateViewportState(const PipelineState::ViewportDesc& viewportDesc, const PipelineState::ScissorDesc& scissorDesc)
{
	VkViewport vp;
	vp.x = viewportDesc.x;
	vp.y = viewportDesc.y;
	vp.width = viewportDesc.width;
	vp.height = viewportDesc.height;
	vp.minDepth = viewportDesc.minDepth;
	vp.maxDepth = viewportDesc.maxDepth;

	VkRect2D scissorRect;
	scissorRect.offset.x = scissorDesc.x;
	scissorRect.offset.y = scissorDesc.y;
	scissorRect.extent.width = scissorDesc.width;
	scissorRect.extent.height = scissorDesc.height;

	VkPipelineViewportStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	createInfo.viewportCount = 1;
	createInfo.scissorCount = 1;
	createInfo.pViewports = &vp;
	createInfo.pScissors = &scissorRect;
	return createInfo;
}


VkPipelineInputAssemblyStateCreateInfo CreateInputAssemblyState(const PipelineState::InputAssemblyDesc& description)
{
	VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
	switch (description.topology)
	{
	case PT_TRIANGLELIST: topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST; break;
	case PT_TRIANGLESTRIP: topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;	break;
	case PT_TRIANGLEFAN: topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN; break;
	case PT_LINELIST: topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST; break;
	case PT_LINESTRIP: topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP; break;
	case PT_POINTLIST: topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;	break;
	case PT_PATCHLIST: topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;	break;
	default: _ASSERT(0); break;
	};

	VkPipelineInputAssemblyStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	createInfo.topology = topology;
	createInfo.primitiveRestartEnable = VK_FALSE;
	return createInfo;
}

VkPipelineVertexInputStateCreateInfo CreateVertexInputState(const PipelineState::VertexInputDesc& description)
{
	Ptr<VulkanVertexDeclaration> vertexLayout = description.layout.DynamicCast<VulkanVertexDeclaration>();

	VkPipelineVertexInputStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	createInfo.vertexAttributeDescriptionCount = vertexLayout->NumAttributes();
	createInfo.pVertexAttributeDescriptions = vertexLayout->AttributeData();
	createInfo.vertexBindingDescriptionCount = vertexLayout->NumStreams();
	createInfo.pVertexBindingDescriptions = vertexLayout->StreamData();
	return createInfo;
}

VkPipelineTessellationStateCreateInfo CreateTessellationState(const PipelineState::TessellationDesc& description)
{
	VkPipelineTessellationStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
	createInfo.patchControlPoints = description.patchControlPoints;
	return createInfo;
}

VkPipelineMultisampleStateCreateInfo CreateMultisampleState(const PipelineState::MultiSampleDesc& desc)
{
	/*
	- RasterizationSample
		: Rasterization 에서 사용되는 픽셀 당 샘플의 수
	- SampleShadingEnable
		: 셰이딩이 Fragment 마다 대신 샘플 마다 발생하도록 한다.
	- MinSampleShading
		: 주어진 Fragment의 셰이딩 동안에 사용되어야 하는 unique 샘플 위치들의 최소 수를 결정한다.
	- pSampleMask
		: static coverage sample mask의 array
	- alphaToCoverageEnable
		: Fragment의 alpha 값이 Coverage 계산에 사용되어야 하는지를 결정한다.
	- alphaToOneEnable
		: Fragment의 Alpha 값이 1로 교체되어야(should be replaced with) 를 결정
	*/
	VkPipelineMultisampleStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	createInfo.sampleShadingEnable = desc.sampleShadingEnable;
	createInfo.minSampleShading = desc.minSampleShading;
	createInfo.rasterizationSamples = (VkSampleCountFlagBits)desc.rasterizationSamples;
	createInfo.pSampleMask = NULL; // &desc.sampleMask;
	createInfo.alphaToCoverageEnable = desc.alphaToCoverageEnable;
	createInfo.alphaToOneEnable = desc.alphaToOneEnable;
	return createInfo;
}

VkPipelineRasterizationStateCreateInfo CreateRasterizationState(const PipelineState::RasterizationDesc& rasterDesc, const PipelineState::DepthTestDesc& depthTestDesc, const PipelineState::DepthBiasDesc& depthBiasDesc)
{
	VkPipelineRasterizationStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;

	/*
	- CullMode
		: Viewer가 보이는 어떤 면을 제거할지를 결정한다. (FrontBit: 앞면을 제거, BackBit : 뒷면을 제거, Front_Back : 전체를 제거)
	- FrontFace
		: 삼각형의 감는 순서를 결정한다. (CW, CCW)
	- PolyMode
		: Fill, Line, Point 로 어떻게 채울 것인가.
	- DepthClamping
		: Projection의 Near/Far Plane 대신에 normal Near/Far Plane으로 Clipping 시킨다.
	- RasterizerDiscard
		: Rasterizer를 비활성화 한다. (Fragment가 생성되지 않는다.)
	- DepthBiasEnable : depth bias 활성화
		https://msdn.microsoft.com/ko-kr/library/windows/desktop/cc308048(v=vs.85).aspx
		https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCmdSetDepthBias.html
	- DepthBiasClamp
	- DepthBiasSlopeFactor
	- DepthBiasConstantFactor
		: Fragment 들의 DepthTest를 할 때, Depth Fighting을 방지하기 위한 Offset 값을 지정한다.
	*/

	// CullMode
	switch (rasterDesc.CullMode)
	{
	case CULLMODE::_CW:
		createInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
		createInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;				// CW
		break;
	case CULLMODE::_CCW:
		createInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
		createInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;		// CCW
		break;
	case CULLMODE::_NONE:
		createInfo.cullMode = VK_CULL_MODE_NONE;					// 면 제거 없음
		break;
	default:
		_ASSERT(0);
		break;
	}

	// FillMode
	switch (rasterDesc.FillMode)
	{
	case FILLMODE::_FILL:
		createInfo.polygonMode = VK_POLYGON_MODE_FILL;
		break;
	case FILLMODE::_LINE:
		createInfo.polygonMode = VK_POLYGON_MODE_LINE;
		break;
	default:
		_ASSERT(0);
		break;
	}

	// Enable / Disable
	createInfo.rasterizerDiscardEnable = rasterDesc.bSkipRasterizer;
	createInfo.lineWidth = 1.0f;
	createInfo.depthClampEnable = false;

	// Depth Bias
	createInfo.depthBiasEnable = depthBiasDesc.bDepthBiasEnable;
	createInfo.depthBiasConstantFactor = depthBiasDesc.DepthBiasConstantFactor;
	createInfo.depthBiasClamp = depthBiasDesc.DepthBiasClamp;
	createInfo.depthBiasSlopeFactor = depthBiasDesc.DepthBiasSlopeFactor;
	return createInfo;
}

VkStencilOp ToStencilFunc(STENCILFUNC::TYPE funcType)
{
	VkStencilOp result = VK_STENCIL_OP_MAX_ENUM;
	switch (funcType)
	{
	case STENCILFUNC::_KEEP: result = VK_STENCIL_OP_KEEP; break;
	case STENCILFUNC::_ZERO: result = VK_STENCIL_OP_ZERO; break;
	case STENCILFUNC::_REPLACE: result = VK_STENCIL_OP_REPLACE; break;
	case STENCILFUNC::_INCREMENT_AND_CLAMP: result = VK_STENCIL_OP_INCREMENT_AND_CLAMP; break;
	case STENCILFUNC::_DECREMENT_AND_CLAMP: result = VK_STENCIL_OP_DECREMENT_AND_CLAMP; break;
	case STENCILFUNC::_INVERT: result = VK_STENCIL_OP_INVERT; break;
	case STENCILFUNC::_INCREMENT_AND_WRAP: result = VK_STENCIL_OP_INCREMENT_AND_WRAP; break;
	case STENCILFUNC::_DECREMENT_AND_WRAP: result = VK_STENCIL_OP_DECREMENT_AND_WRAP; break;
	default: _ASSERT(0); break;
	}
	return result;
}

VkCompareOp ToCompareFunc(COMPAREFUNC::TYPE funcType)
{
	VkCompareOp result = VK_COMPARE_OP_MAX_ENUM;
	switch (funcType)
	{
	case COMPAREFUNC::_LESS:
		result = VK_COMPARE_OP_LESS;
		break;
	case COMPAREFUNC::_EQUAL:
		result = VK_COMPARE_OP_EQUAL;
		break;
	case COMPAREFUNC::_LESS_EQUAL:
		result = VK_COMPARE_OP_LESS_OR_EQUAL;
		break;
	case COMPAREFUNC::_GREATER:
		result = VK_COMPARE_OP_GREATER;
		break;
	case COMPAREFUNC::_NOT_EQUAL:
		result = VK_COMPARE_OP_NOT_EQUAL;
		break;
	case COMPAREFUNC::_GREATER_EQUAL:
		result = VK_COMPARE_OP_GREATER_OR_EQUAL;
		break;
	case COMPAREFUNC::_ALWAYS:
		result = VK_COMPARE_OP_ALWAYS;
		break;
	default:
		_ASSERT(0);
		break;
	}
	return result;
}

VkBlendOp ToBlendOp(BLENDFUNC::TYPE blendFunc)
{
	VkBlendOp result = VK_BLEND_OP_ADD;
	switch (blendFunc)
	{
	case BLENDFUNC::_ADD:				result = VK_BLEND_OP_ADD; break;
	case BLENDFUNC::_SUBTRACT:			result = VK_BLEND_OP_SUBTRACT; break;
	case BLENDFUNC::_REVERSE_SUBTRACT:	result = VK_BLEND_OP_REVERSE_SUBTRACT; break;
	case BLENDFUNC::_MIN:				result = VK_BLEND_OP_MIN; break;
	case BLENDFUNC::_MAX:				result = VK_BLEND_OP_MAX; break;
	}
	return result;
}

VkBlendFactor ToBlendFactor(BLENDFACTOR::TYPE blendFactor)
{
	VkBlendFactor result = VK_BLEND_FACTOR_ZERO;
	switch (blendFactor)
	{
	case BLENDFACTOR::_ZERO:						result = VK_BLEND_FACTOR_ZERO; break;
	case BLENDFACTOR::_ONE:							result = VK_BLEND_FACTOR_ONE; break;
	case BLENDFACTOR::_SRC_COLOR:					result = VK_BLEND_FACTOR_SRC_COLOR; break;
	case BLENDFACTOR::_ONE_MINUS_SRC_COLOR:			result = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR; break;
	case BLENDFACTOR::_DST_COLOR:					result = VK_BLEND_FACTOR_DST_COLOR; break;
	case BLENDFACTOR::_ONE_MINUS_DST_COLOR:			result = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR; break;
	case BLENDFACTOR::_SRC_ALPHA:					result = VK_BLEND_FACTOR_SRC_ALPHA; break;
	case BLENDFACTOR::_ONE_MINUS_SRC_ALPHA:			result = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA; break;
	case BLENDFACTOR::_DST_ALPHA:					result = VK_BLEND_FACTOR_DST_ALPHA; break;
	case BLENDFACTOR::_ONE_MINUS_DST_ALPHA:			result = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA; break;
	case BLENDFACTOR::_CONSTANT_COLOR:				result = VK_BLEND_FACTOR_CONSTANT_COLOR; break;
	case BLENDFACTOR::_ONE_MINUS_CONSTANT_COLOR:	result = VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR; break;
	case BLENDFACTOR::_CONSTANT_ALPHA:				result = VK_BLEND_FACTOR_CONSTANT_ALPHA; break;
	case BLENDFACTOR::_ONE_MINUS_CONSTANT_ALPHA:	result = VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA; break;
	case BLENDFACTOR::_SRC_ALPHA_SATURATE:			result = VK_BLEND_FACTOR_SRC_ALPHA_SATURATE; break;
	case BLENDFACTOR::_SRC1_COLOR:					result = VK_BLEND_FACTOR_SRC1_COLOR; break;
	case BLENDFACTOR::_ONE_MINUS_SRC1_COLOR:		result = VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR; break;
	case BLENDFACTOR::_SRC1_ALPHA:					result = VK_BLEND_FACTOR_SRC1_ALPHA; break;
	case BLENDFACTOR::_ONE_MINUS_SRC1_ALPHA:		result = VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA; break;
	}
	return result;
}

VkPipelineDepthStencilStateCreateInfo CreateDepthStencilState(const PipelineState::DepthTestDesc& depthTestDesc, const PipelineState::DepthBoundTestDesc& depthBoundDesc, const PipelineState::StencilTestDesc& stencilTestDesc)
{
	/*
	- DepthTestEnable
		: 깊이 테스트 사용 여부
	- DepthCompareOp
		: 깊이 테스트를 사용할 때, 깊이 비교하는데 사용되는 연산
		ALWAYS: 항상 성공
		NAVER: 항상 실패
		LESS : 예전 화소 깊이 값보다 작으면 성공
		LESS_OR_EQUAL: 예전 화소 깊이 값보다 같거나 작으면 성공
		EQUAL: 예전 깊이 값이랑 같으면 성공
		NOT_EQUAL: 예전 깊이 값과 다르면 성공
		GREATER: 예전 깊이 값보다 크면 성공
		GREATER_OR_EQUAL: 예전 깊이 값보다 크거나 같이면 성공
	- DetphWriteEnable
		: 깊이 테스트를 통과한 결과를 깊이 버퍼에 기록할 지를 결정
	- DepthBoundsTestEnable
		: 깊이 바운드 테스트 사용 여부
		(현재 fragment에 대해 깊이 버퍼에 저장된 값이 지정된 범위 값과 비교된다. 만약 깊이 버퍼에 값이 지정한 범위 안에 떨어지면(fall) Test는 통과하고, 반대라면 실패한다.)
		(깊이 테스트와 독립적이기 때문에, Fragment Shading이나 Depth Interpolation 이전에 빠르게 평가할 수 있다)

	*/
	VkPipelineDepthStencilStateCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

	// Depth Test
#if 0
	createInfo.depthTestEnable = (renderState&ZDISABLE) ? false : true;
	createInfo.depthWriteEnable = (renderState&ZWRITEDISABLE) ? false : true;

	switch (renderState&ZFUNC_MASK)
	{
	case ZFUNC_LESS:
		createInfo.depthCompareOp = VK_COMPARE_OP_LESS;
		break;
	case ZFUNC_LESSEQUAL:
		createInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		break;
	case ZFUNC_EQUAL:
		createInfo.depthCompareOp = VK_COMPARE_OP_EQUAL;
		break;
	case ZFUNC_GREATER:
		createInfo.depthCompareOp = VK_COMPARE_OP_GREATER;
		break;
	case ZFUNC_ALWAYS:
		createInfo.depthCompareOp = VK_COMPARE_OP_ALWAYS;
		break;
	}
#endif

	createInfo.depthCompareOp = ToCompareFunc(depthTestDesc.DepthCompareOp);

	// Enable / Disable
	createInfo.depthTestEnable = depthTestDesc.bDepthTestEnable;
	createInfo.depthWriteEnable = depthTestDesc.bDepthWriteEnable;

	// Depth Bound Test
	createInfo.depthBoundsTestEnable = depthBoundDesc.bDepthBoundTestEnable;
	createInfo.minDepthBounds = depthBoundDesc.minDepthBound;
	createInfo.maxDepthBounds = depthBoundDesc.maxDepthBound;

	// StencilTest
	createInfo.stencilTestEnable = stencilTestDesc.bStencilTestEnable;
	createInfo.front.failOp = ToStencilFunc(stencilTestDesc.front.failOp);
	createInfo.front.passOp = ToStencilFunc(stencilTestDesc.front.passOp);
	createInfo.front.depthFailOp = ToStencilFunc(stencilTestDesc.front.depthFailOp);
	createInfo.front.compareOp = ToCompareFunc(stencilTestDesc.front.compareOp);
	createInfo.front.compareMask = stencilTestDesc.front.compareMask;
	createInfo.front.writeMask = stencilTestDesc.front.writeMask;
	createInfo.front.reference = stencilTestDesc.front.reference;
	createInfo.back.failOp = ToStencilFunc(stencilTestDesc.back.failOp);
	createInfo.back.passOp = ToStencilFunc(stencilTestDesc.back.passOp);
	createInfo.back.depthFailOp = ToStencilFunc(stencilTestDesc.back.depthFailOp);
	createInfo.back.compareOp = ToCompareFunc(stencilTestDesc.back.compareOp);
	createInfo.back.compareMask = stencilTestDesc.back.compareMask;
	createInfo.back.writeMask = stencilTestDesc.back.writeMask;
	createInfo.back.reference = stencilTestDesc.back.reference;
	return createInfo;
}

void GetColorBlendStates(int32 attachmentCount, const PipelineState::ColorBlendDesc* colorBlendStates, std::vector<VkPipelineColorBlendAttachmentState>& attachmentBlendStates)
{
	// Color 블랜딩 상태 설정
	attachmentBlendStates.reserve(attachmentCount);
	for (int32 i = 0; i < attachmentCount; i++)
	{
		VkPipelineColorBlendAttachmentState state;
		{
			state.blendEnable = colorBlendStates[i].bBlendEnable;
			state.colorBlendOp = ToBlendOp(colorBlendStates[i].colorBlendOp);
			state.srcColorBlendFactor = ToBlendFactor(colorBlendStates[i].srcColorBlendFactor);
			state.dstColorBlendFactor = ToBlendFactor(colorBlendStates[i].dstColorBlendFactor);
			state.alphaBlendOp = ToBlendOp(colorBlendStates[i].alphaBlendOp);
			state.srcAlphaBlendFactor = ToBlendFactor(colorBlendStates[i].srcAlphaBlendFactor);
			state.dstAlphaBlendFactor = ToBlendFactor(colorBlendStates[i].dstAlphaBlendFactor);
			state.colorWriteMask = colorBlendStates[i].colorWriteMask;
		}
		attachmentBlendStates.push_back(state);
	}
}

void GetShaderStages(VulkanShader* shader, std::vector<VkPipelineShaderStageCreateInfo>& shaderStages)
{
	shaderStages.reserve(ShaderType::NUM);
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		VulkanShaderProgram* shaderProgram = shader->GetShaderProgram(index);
		if (shaderProgram)
		{
			VkPipelineShaderStageCreateInfo shaderStage;
			Memory::Memzero(shaderStage);
			shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
			shaderStage.stage = Vulkan::ShaderTypeToStage(shaderProgram->GetShaderResource()->GetType());
			shaderStage.module = shaderProgram->GetShaderResource()->Handle();
			shaderStage.pName = shaderProgram->GetShaderResource()->GetEntryName();
			shaderStages.push_back(shaderStage);
		}
	}
}

void GetDynamicState(DYNAMICSTATE::MASK dymamicState, std::vector<VkDynamicState>& states)
{
	states.reserve(VK_DYNAMIC_STATE_RANGE_SIZE);
	if (dymamicState&DYNAMICSTATE::_VIEWPORT)			states.push_back(VK_DYNAMIC_STATE_VIEWPORT);
	if (dymamicState&DYNAMICSTATE::_SCISSOR)			states.push_back(VK_DYNAMIC_STATE_SCISSOR);
	if (dymamicState&DYNAMICSTATE::_LINE_WIDTH)			states.push_back(VK_DYNAMIC_STATE_LINE_WIDTH);
	if (dymamicState&DYNAMICSTATE::_DEPTH_BIAS)			states.push_back(VK_DYNAMIC_STATE_DEPTH_BIAS);
	if (dymamicState&DYNAMICSTATE::_BLEND_CONSTANTS)	states.push_back(VK_DYNAMIC_STATE_BLEND_CONSTANTS);
	if (dymamicState&DYNAMICSTATE::_DEPTH_BOUNDS)		states.push_back(VK_DYNAMIC_STATE_DEPTH_BOUNDS);
	if (dymamicState&DYNAMICSTATE::_STENCIL_COMPARE_MASK)	states.push_back(VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK);
	if (dymamicState&DYNAMICSTATE::_STENCIL_WRITE_MASK)	states.push_back(VK_DYNAMIC_STATE_STENCIL_WRITE_MASK);
	if (dymamicState&DYNAMICSTATE::_STENCIL_REFERENCE)		states.push_back(VK_DYNAMIC_STATE_STENCIL_REFERENCE);
}