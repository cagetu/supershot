#include "renderer.h"

__ImplementRtti(VisualShock, VulkanUniformBuffer, UniformBuffer);

VulkanUniformBuffer::VulkanUniformBuffer(VulkanDevice* device)
	: IVulkanBuffer(device)
{
}
VulkanUniformBuffer::~VulkanUniformBuffer()
{
}

bool VulkanUniformBuffer::Create(void* uniformData, int32 size)
{
	if (CreateBuffer(size) == false)
		return false;

	void* ptr = Lock(size, 0);
	{
		Memory::Memcopy(ptr, uniformData, m_Length);
	}
	Unlock();
	return true;
}

void VulkanUniformBuffer::Destroy()
{
	UniformBuffer::Destroy();
	IVulkanBuffer::DestroyBuffer();
}

bool VulkanUniformBuffer::CreateBuffer(uint32 bufferSize)
{
	IVulkanBuffer::CreateBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

	m_Length = bufferSize;
	return true;
}

void* VulkanUniformBuffer::Lock(uint32 bufferSize, uint32 offset)
{
#ifdef _MANAGED_BUFFER
	return m_BufferAllocation->GetMappedPointer();
#else
	return m_MemoryAllocation->Map(bufferSize, offset);
#endif
}

void VulkanUniformBuffer::Unlock()
{
#ifdef _MANAGED_BUFFER
#else
	m_MemoryAllocation->Unmap();
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*																																*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VulkanUniformBufferUploader::VulkanUniformBufferUploader(VulkanDevice* device, VkDeviceSize totalSize)
	: m_Device(device)
{
	if (m_Device->MemoryManager().HasUnifiedMemory())
	{
		m_CPUBuffer = new Vulkan::RingBuffer(device, totalSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		m_GPUBuffer = m_CPUBuffer;
	}
	else
	{
		m_CPUBuffer = new Vulkan::RingBuffer(device, totalSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		m_GPUBuffer = new Vulkan::RingBuffer(device, totalSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	}
}
VulkanUniformBufferUploader::~VulkanUniformBufferUploader()
{
	if (m_Device && m_Device->MemoryManager().HasUnifiedMemory())
	{
		delete m_GPUBuffer;
		m_GPUBuffer = nullptr;
	}
	delete m_CPUBuffer;
	m_CPUBuffer = nullptr;
}

char* VulkanUniformBufferUploader::GetCPUMapPointer()
{
	return (char*)m_CPUBuffer->GetMappedPointr();
}

uint64 VulkanUniformBufferUploader::AllocateMemory(uint64 Size, uint32 Alignment)
{
	return m_CPUBuffer->AllocateMemory(Size, Alignment);
}

VkBuffer VulkanUniformBufferUploader::GetCPUBufferHandle() const
{
	return m_CPUBuffer->GetHandle();
}

uint32 VulkanUniformBufferUploader::GetCPUBufferOffset() const
{
	return m_CPUBuffer->GetBufferOffset();
}

uint64 VulkanUniformBufferUploader::GetCPUBufferSize() const
{
	return m_CPUBuffer->GetTotalSize();
}
