#include "renderer.h"
#include <shaderc/shaderc.hpp>

std::map <String, VulkanCompiledShaderCode*> VulkanShaderCompiler::m_ShaderCache;

bool VulkanShaderCompiler::Initialize()
{
	// shadercache 내부에 있는 파일을 모두 읽는다.

	return IShaderCompiler::Initialize();
}

void VulkanShaderCompiler::Shutdown()
{
	for (auto it : m_ShaderCache)
		delete it.second;
	m_ShaderCache.clear();

	IShaderCompiler::Shutdown();
}

ICompiledShaderCode* VulkanShaderCompiler::Find(const TCHAR* id)
{
	auto it = m_ShaderCache.find(id);
	if (it != m_ShaderCache.end())
		return it->second;
	return NULL;
}

ICompiledShaderCode* VulkanShaderCompiler::Compile(const TCHAR* id, const std::string& source, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option)
{
	std::string macro;
	{
		static std::string macrolist[128];
		ShaderFeature::GetMacro(shaderMask, macrolist, &macro);
	}
	std::string shaderCode = macro + source;

	char name[256];
	string::conv_s(name, sizeof(name), id);

	shaderc::Compiler compiler;
	shaderc::CompileOptions options;
#ifdef _DEBUG
	options.SetGenerateDebugInfo();
	options.SetSuppressWarnings();
	options.SetWarningsAsErrors();
#endif
	//options.SetOptimizationLevel(shaderc_optimization_level_size);

	shaderc_shader_kind kind;
	if (type == ShaderType::VERTEXSHADER)			kind = shaderc_shader_kind::shaderc_glsl_vertex_shader;
	else if (type == ShaderType::FRAGMENTSHADER)	kind = shaderc_shader_kind::shaderc_glsl_fragment_shader;
	else if (type == ShaderType::COMPUTESHADER)		kind = shaderc_shader_kind::shaderc_glsl_compute_shader;
	else if (type == ShaderType::GEOMETRYSHADER)	kind = shaderc_shader_kind::shaderc_glsl_geometry_shader;
	else _ASSERT(0);

	shaderc::SpvCompilationResult module = compiler.CompileGlslToSpv(shaderCode.c_str(), shaderCode.size(), kind, name, "main", options);
	if (module.GetCompilationStatus() != shaderc_compilation_status_success)
	{
		const std::string& error = module.GetErrorMessage();
		PlatformUtil::DebugOutFormat("%s\n", error.c_str());
		PlatformUtil::Dialog(error.c_str(), NULL, 0);
		return false;
	}

	// GLSL 셰이더 파일을 가지고 와서, "glslanvalidator.exe -v -o"를 통해서 셰이더를 컴파일 한다.
	VulkanCompiledShaderCode* compiled = new VulkanCompiledShaderCode();
	compiled->code.assign(module.cbegin(), module.cend());

	m_ShaderCache.insert(std::make_pair(id, compiled));

	return compiled;
}

//

ICompiledShaderCode* CompileShader(const TCHAR* id, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option)
{
	return VulkanShaderCompiler::Compile(id, shaderCode, type, shaderMask, option);
}