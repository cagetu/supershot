#include "renderer.h"

//https://matthewwellings.com/blog/the-new-vulkan-coordinate-system/
//http://anki3d.org/vulkan-coordinate-system/
//https://github.com/LunarG/VulkanSamples/commit/0dd36179880238014512c0637b0ba9f41febe803

/*	Mirror Matrix
	: DirectX -> OpenGL -> Vulkan으로 변환
	1. OpenGL : 기본적은 DirectX의 Projection Matrix가 Transpose되고, z축이 부호가 바뀐 것!!!
	2. Vulkan : OpenGL을 Y축을 변환하고, 0.5, 0.5 변환
*/
static Mat4 inv = {	1, 0, 0, 0,
					0, -1, 0, 0,
					0, 0, -0.5f, 0,
					0, 0, 0.5f, 1 };

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 공간을 계산 후, Mirror Transform으로 z[0, 1] 공간으로 변환
*/
//--------------------------------------------------------------
Mat4 VulkanTransform::Projection(float nearwidth, float nearheight, float nearclip, float farclip)
{
	// z : [-1, 1]
	float	q = 1.0f / (farclip - nearclip);
	Mat4	m;
	m._11 = 2.0f*nearclip / nearwidth;	m._12 = 0.0f;						m._13 = 0.0f;					m._14 = 0.0f;
	m._21 = 0.0f;						m._22 = 2.0f*nearclip / nearheight;	m._23 = 0.0f;					m._24 = 0.0f;
	m._31 = 0.0f;						m._32 = 0.0f;						m._33 = -(farclip + nearclip) * q;	m._34 = -(farclip*nearclip*2.0f) * q;
	m._41 = 0.0f;						m._42 = 0.0f;						m._43 = -1.0f;					m._44 = 0.0f;

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 공간을 계산 후, Mirror Transform으로 z[0, 1] 공간으로 변환
*/
//--------------------------------------------------------------
Mat4 VulkanTransform::ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio)
{
	// z : [-1, 1]
	float	f = 1.0f / (float)tan(fov * 0.5f);
	float	q = 1.0f / (farclip - nearclip);

	Mat4	m;
	m._11 = f*aspectratio; 	m._12 = 0.0f;	m._13 = 0.0f;						m._14 = 0.0f;
	m._21 = 0.0f;			m._22 = f;		m._23 = 0.0f;						m._24 = 0.0f;
	m._31 = 0.0f;			m._32 = 0.0f;	m._33 = -(farclip + nearclip) * q;	m._34 = -(farclip*nearclip*2.0f) * q;
	m._41 = 0.0f;			m._42 = 0.0f;	m._43 = -1.0f;						m._44 = 0.0f;

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 공간을 계산 후, Mirror Transform으로 z[0, 1] 공간으로 변환
*/
//--------------------------------------------------------------
Mat4 VulkanTransform::ProjectionOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	float	m00 = 2 * zn / (r - l),
			m02 = (r + l) / (r - l),
			m11 = 2 * zn / (t - b),
			m12 = (t + b) / (t - b),
			m22 = -(zf + zn) / (zf - zn),
			m23 = -2 * zf * zn / (zf - zn),
			m32 = -1;

	Mat4 m = {	m00, 0.0, m02, 0.0,
				0.0, m11, m12, 0.0,
				0.0, 0.0, m22, m23,
				0.0, 0.0, m32, 0.0 };

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 공간을 계산 후, Mirror Transform으로 z[0, 1] 공간으로 변환
	http://www.terathon.com/gdc07_lengyel.pdf
*/
//--------------------------------------------------------------
Mat4 VulkanTransform::InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon)
{
	float	wtan = (float)tan(fov * 0.5f);
	float	f = 1.0f / wtan;

	// [-1, 1-e] 로 변환
	Mat4	m;
	m._11 = f / aspectratio;	m._12 = 0.0f;	m._13 = 0.0f;				m._14 = 0.0f;
	m._21 = 0.0f;				m._22 = f;		m._23 = 0.0f;				m._24 = 0.0f;
	m._31 = 0.0f;				m._32 = 0.0f;	m._33 = (epsilon - 1.0f);	m._34 = (epsilon - 2.0f) * nearclip;
	m._41 = 0.0f;				m._42 = 0.0f;	m._43 = -1.0f;				m._44 = 0.0f;

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 공간을 계산 후, Mirror Transform으로 z[0, 1] 공간으로 변환
*/
//--------------------------------------------------------------
Mat4 VulkanTransform::Ortho(float w, float h, float nearclip, float farclip)
{
	// z : [-1, 1]
	float q = 1.0f / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f / w;	m._12 = 0.0f;	m._13 = 0.0f;	m._14 = 0.0f;
	m._21 = 0.0f;	m._22 = 2.0f / h;	m._23 = 0.0f;	m._24 = 0.0f;
	m._31 = 0.0f;	m._32 = 0.0f;	m._33 = -2.0f*q;	m._34 = -(farclip + nearclip) * q;
	m._41 = 0.0f;	m._42 = 0.0f;	m._43 = 0.0f;		m._44 = 1.0f;

	m = inv * m.Transpose();
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[-1, 1] 공간을 계산 후, Mirror Transform으로 z[0, 1] 공간으로 변환
*/
//--------------------------------------------------------------
Mat4 VulkanTransform::OrthoOffCenter(float l, float r, float b, float t, float nearclip, float farclip)
{
	// z : [-1, 1]
	float q = 1.0f / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f / (r - l);	m._12 = 0.0f;		m._13 = 0.0f;	m._14 = -(r + l) / (r - l);
	m._21 = 0.0f;		m._22 = 2.0f / (t - b);	m._23 = 0.0f;	m._24 = -(t + b) / (t - b);
	m._31 = 0.0f;		m._32 = 0.0f;		m._33 = -2.0f*q; m._34 = -(farclip + nearclip) * q;
	m._41 = 0.0f;		m._42 = 0.0f;		m._43 = 0.0f;	m._44 = 1.0f;

	m = inv * m.Transpose();
	return m;
}

