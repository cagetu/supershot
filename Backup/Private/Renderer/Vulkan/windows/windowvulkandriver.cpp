#include "renderer.h"

//////////////////////////////////////////////////////////////////////////////

static HMODULE GVulkanDLLModule = nullptr;
static bool LoadVulkanLibrary()
{
	GVulkanDLLModule = ::LoadLibraryW(TEXT("vulkan-1.dll"));
	return GVulkanDLLModule != nullptr;
}

static bool LoadVulkanEntryFunctions()
{
#define LoadProcAddress GetProcAddress

#define VK_EXPORTED_FUNCTION( fun )                                                   \
    if( !(fun = (PFN_##fun)LoadProcAddress( GVulkanDLLModule, #fun )) ) {                \
      std::cout << "Could not load exported function: " << #fun << "!" << std::endl;  \
      return false;                                                                   \
	    }


}

static bool LoadVulkanInstanceFunctions(VkInstance inInstance)
{
	return true;
}

static void FreeVulkanLibrary()
{
	if (GVulkanDLLModule != nullptr)
	{
		::FreeLibrary(GVulkanDLLModule);
		GVulkanDLLModule = nullptr;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

WindowVulkanDriver::WindowVulkanDriver()
{
}

WindowVulkanDriver::~WindowVulkanDriver()
{
}

bool WindowVulkanDriver::Create(const IDisplayContext* displayHandle)
{
	if (LoadVulkanLibrary() == false)
		return false;

	if (VulkanDriver::Create(displayHandle) == false)
		return false;

	GetDevice()->WaitForIdle();

	return true;
}

void WindowVulkanDriver::Destroy()
{
	VulkanDriver::Destroy();

	FreeVulkanLibrary();
}

//////////////////////////////////////////////////////////////////////////////////////////////


static WindowVulkanDriver* g_RenderDevice = nullptr;

IRenderDriver* CreateRenderDriver()
{
	if (g_RenderDevice == nullptr)
	{
		g_RenderDevice = new WindowVulkanDriver;
	}
	return g_RenderDevice;
}

void DestroyRenderDriver()
{
	if (g_RenderDevice != nullptr)
	{
		g_RenderDevice->Destroy();
		delete g_RenderDevice;
		g_RenderDevice = nullptr;
	}
}

IRenderDriver* GetRenderDriver()
{
	return g_RenderDevice;
}
