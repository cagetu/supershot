#include "renderer.h"

VulkanCmdBufferPool::VulkanCmdBufferPool()
	: m_Device(nullptr)
	, m_UploadCmdBuffer(nullptr)
	, m_ActiveCmdBuffer(nullptr)
	, m_CreateFlags(0)
{
}
VulkanCmdBufferPool::~VulkanCmdBufferPool()
{
}

void VulkanCmdBufferPool::Init(VulkanDevice* device, PoolCreateFlags::Type createFlags)
{
	m_Device = device;

	/*
		* VK_COMMAND_POOL_CREATE_TRANSIENT_BIT
			- Pool로 부터 가져온 command buffer가 짧게 사용되고 사용 이후 Pool로 반환될 때
			- Pool내에 메모리 할당 행위를 컨트롤하는데 사용될 수 있다.
		* VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
			- vkResetCommandBuffer가 호출될 때, 명시적으로 Reset된다.
			- vkBeginCommandBuffer가 호출될 때, 암묵적으로 Reset된다.
		- Reset이 설정되지 않았을 때, 이것이 설정되지 않았을 때에는 vkResetCommandPool이 호출될 때에만 리셋될 수 있다.
	*/
	m_CreateFlags = createFlags;
	VkCommandPoolCreateFlags poolCreateFlags = 0;
	if (createFlags&PoolCreateFlags::_TRANSIENT_BIT)			
		poolCreateFlags |= VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
	if (createFlags&PoolCreateFlags::_RESET_COMMAND_BUFFER_BIT)	
		poolCreateFlags |= VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	VkCommandPoolCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = poolCreateFlags;  // VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;	// or VK_COMMAND_POOL_CREATE_TRANSIENT_BIT
	createInfo.queueFamilyIndex = device->GetGfxQueue()->GetFamilyIndex();

	VK_RESULT(::vkCreateCommandPool(device->GetHandle(), &createInfo, nullptr, &m_CommandPool));

#ifdef _SYNC_CMDBUFFER
	m_ActiveCmdBuffer = CreateCmdBuffer();
	m_ActiveCmdBuffer->Begin();
	SubmitActiveCmdBuffer(false);
	PrepareForNewActiveCommandBuffer();
#endif
}

void VulkanCmdBufferPool::Destroy()
{
	for (auto it = m_CommandBuffers.begin(); it != m_CommandBuffers.end(); it++)
		delete (*it);
	m_CommandBuffers.clear();

	if (m_CommandPool != VK_NULL_HANDLE)
	{
		::vkDestroyCommandPool(m_Device->GetHandle(), m_CommandPool, nullptr);
		m_CommandPool = VK_NULL_HANDLE;
	}
}

void VulkanCmdBufferPool::Reset()
{
	if (m_CommandPool != VK_NULL_HANDLE)
	{
		VK_RESULT(vkResetCommandPool(m_Device->GetHandle(), m_CommandPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT));
	}
}

VulkanCmdBuffer* VulkanCmdBufferPool::CreateCmdBuffer()
{
	VulkanCmdBuffer* instance = new VulkanCmdBuffer(m_Device, this);
	m_CommandBuffers.push_back(instance);

	return instance;
}

void VulkanCmdBufferPool::DestroyCmdBuffer(VulkanCmdBuffer* instance)
{
	auto it = std::find(m_CommandBuffers.begin(), m_CommandBuffers.end(), instance);
	if (it != m_CommandBuffers.end())
	{
		m_CommandBuffers.erase(it);
		delete instance;
	}
}

VulkanCmdBuffer* VulkanCmdBufferPool::GetUploadCommandBuffer()
{
	if (!m_UploadCmdBuffer)
	{
		for (uint32 i = 0; i < m_CommandBuffers.size(); i++)
		{
			m_CommandBuffers[i]->RefreshFenceStatus();
			if (m_CommandBuffers[i]->State() == VulkanCmdBuffer::_STATE::READYTOBEGIN)
			{
				m_UploadCmdBuffer = m_CommandBuffers[i];
				m_UploadCmdBuffer->Begin();
				return m_UploadCmdBuffer;
			}
		}

		m_UploadCmdBuffer = CreateCmdBuffer();
		m_UploadCmdBuffer->Begin();
	}

	return m_UploadCmdBuffer;
}

VulkanCmdBuffer* VulkanCmdBufferPool::GetActiveCommandBuffer()
{
	if (m_UploadCmdBuffer)
	{
		SubmitUploadCmdBuffer(false);
	}
	return m_ActiveCmdBuffer;
}

void VulkanCmdBufferPool::PrepareForNewActiveCommandBuffer()
{
	_ASSERT(!m_UploadCmdBuffer);

	for (uint32 i = 0; i < m_CommandBuffers.size(); i++)
	{
		m_CommandBuffers[i]->RefreshFenceStatus();
		if (m_CommandBuffers[i]->State() == VulkanCmdBuffer::_STATE::READYTOBEGIN)
		{
			m_ActiveCmdBuffer = m_CommandBuffers[i];
			m_ActiveCmdBuffer->Begin();
			return;
		}
		else
		{
			_ASSERT(m_CommandBuffers[i]->State() == VulkanCmdBuffer::_STATE::SUBMITTED);
		}
	}

	// All cmd buffers are being executed still
	m_ActiveCmdBuffer = CreateCmdBuffer();
	m_ActiveCmdBuffer->Begin();
}


void VulkanCmdBufferPool::SubmitUploadCmdBuffer(bool bWaitForFence)
{
	_ASSERT(m_UploadCmdBuffer);
	_ASSERT(m_UploadCmdBuffer->State() == VulkanCmdBuffer::_STATE::BEGIN);

	m_UploadCmdBuffer->End();
	m_Device->GetGfxQueue()->Submit(m_UploadCmdBuffer);

	if (bWaitForFence)
	{
		if (m_UploadCmdBuffer->IsSubmitted())
		{
			WaitForCmdBuffer(m_UploadCmdBuffer, 1.0f);
		}
	}

	m_UploadCmdBuffer = nullptr;
}

void VulkanCmdBufferPool::SubmitActiveCmdBuffer(bool bWaitForFence)
{
	_ASSERT(!m_UploadCmdBuffer);
	_ASSERT(m_ActiveCmdBuffer);
	_ASSERT(m_ActiveCmdBuffer->State() == VulkanCmdBuffer::_STATE::BEGIN);

	m_ActiveCmdBuffer->End();
	m_Device->GetGfxQueue()->Submit(m_ActiveCmdBuffer);
	
	if (bWaitForFence)
	{
		if (m_ActiveCmdBuffer->IsSubmitted())
		{
			WaitForCmdBuffer(m_ActiveCmdBuffer);
		}
		//m_Device->WaitForIdle();
		//RefreshFenceStatus();
	}

	m_ActiveCmdBuffer = nullptr;
}

void VulkanCmdBufferPool::WaitForCmdBuffer(VulkanCmdBuffer* cmdBuffer, float timeOut)
{
	_ASSERT(cmdBuffer->IsSubmitted());

	bool res = m_Device->FencePool().WaitForFence(cmdBuffer->m_Fence, (uint64)(timeOut * 1e9));
	_ASSERT(res == true);

	cmdBuffer->RefreshFenceStatus();
}

void VulkanCmdBufferPool::RefreshFenceStatus()
{
	for (uint32 i = 0; i < m_CommandBuffers.size(); i++)
	{
		VulkanCmdBuffer* CmdBuffer = m_CommandBuffers[i];
		CmdBuffer->RefreshFenceStatus();
	}
}

void VulkanCmdBufferPool::Initialize(uint32 ImageCount)
{
	for (uint32 i = 0; i < ImageCount; i++)
	{
		CreateCmdBuffer();
	}
}

