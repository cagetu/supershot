#include "renderer.h"

namespace Vulkan
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DeviceMemoryPool::DeviceMemoryPool()
	: m_Device(nullptr)
	, m_NumAllocations(0)
	, m_PeakNumAllocations(0)
	, m_bHasUnifiedMemory(false)
{
}
DeviceMemoryPool::~DeviceMemoryPool()
{
}

void DeviceMemoryPool::Init(VulkanDevice* device)
{
	m_Device = device;
	m_NumAllocations = 0;

	vkGetPhysicalDeviceMemoryProperties(device->GetGpuHandle(), &m_MemoryProperties);

	m_HeapInfos.resize(m_MemoryProperties.memoryHeapCount);

	const uint32 MaxAllocations = m_Device->GetLimits().maxMemoryAllocationCount;
	PlatformUtil::DebugOutFormat(TEXT("%d Device Memory Heaps; Max memory allocations %d\n"), m_MemoryProperties.memoryHeapCount, MaxAllocations);

	for (uint32 Index = 0; Index < m_MemoryProperties.memoryHeapCount; ++Index)
	{
		bool bIsGPUHeap = ((m_MemoryProperties.memoryHeaps[Index].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) == VK_MEMORY_HEAP_DEVICE_LOCAL_BIT);
		PlatformUtil::DebugOutFormat(TEXT("%d: Flags 0x%x Size %llu (%.2f MB) %s\n"), 
										Index,
										m_MemoryProperties.memoryHeaps[Index].flags,
										m_MemoryProperties.memoryHeaps[Index].size,
										(float)((double)m_MemoryProperties.memoryHeaps[Index].size / 1024.0 / 1024.0),
										bIsGPUHeap ? TEXT("GPU") : TEXT(""));
		m_HeapInfos[Index].TotalSize = m_MemoryProperties.memoryHeaps[Index].size;
	}
	m_bHasUnifiedMemory = (m_MemoryProperties.memoryHeapCount == 1);
	PlatformUtil::DebugOutFormat(TEXT("%d Device Memory Types\n"), m_MemoryProperties.memoryTypeCount);

	for (uint32 Index = 0; Index < m_MemoryProperties.memoryTypeCount; ++Index)
	{
		auto GetFlagsString = [](VkMemoryPropertyFlags Flags)
		{
			unicode::string String;
			if ((Flags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
			{
				String += TEXT(" Local");
			}
			if ((Flags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
			{
				String += TEXT(" HostVisible");
			}
			if ((Flags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
			{
				String += TEXT(" HostCoherent");
			}
			if ((Flags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
			{
				String += TEXT(" HostCached");
			}
			if ((Flags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) == VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT)
			{
				String += TEXT(" Lazy");
			}
			return String;
		};
		PlatformUtil::DebugOutFormat(TEXT("%d: Flags 0x%x Heap %d %s\n"),
			Index,
			m_MemoryProperties.memoryTypes[Index].propertyFlags,
			m_MemoryProperties.memoryTypes[Index].heapIndex,
			GetFlagsString(m_MemoryProperties.memoryTypes[Index].propertyFlags).c_str());
	}
}

void DeviceMemoryPool::Destroy()
{
	for (FHeapInfo& info : m_HeapInfos)
	{
		for (auto it = info.Allocations.begin(); it != info.Allocations.end(); it++)
		{
			DeviceMemoryAllocation* allocation = (*it);
			Free(allocation);
		}
	}
	m_NumAllocations = 0;
	m_PeakNumAllocations = 0;
}

DeviceMemoryAllocation* DeviceMemoryPool::Allocate(VkDeviceSize allocationSize, uint32 memoryTypeIndex)
{
	//FScopeLock Lock(&GAllocationLock);

	_ASSERT(allocationSize > 0);
	_ASSERT(memoryTypeIndex < m_MemoryProperties.memoryTypeCount);

	VkDeviceMemory result;
	{
		VkMemoryAllocateInfo memoryAllocInfo;
		Memory::Memzero(memoryAllocInfo);
		memoryAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		memoryAllocInfo.pNext = nullptr;
		memoryAllocInfo.allocationSize = allocationSize;
		memoryAllocInfo.memoryTypeIndex = memoryTypeIndex;
		VK_RESULT(vkAllocateMemory(m_Device->GetHandle(), &memoryAllocInfo, nullptr, &result));
	}

	DeviceMemoryAllocation* memoryAllocation = new DeviceMemoryAllocation();
	memoryAllocation->m_DeviceHandle = m_Device->GetHandle();
	memoryAllocation->m_MemoryHandle = result;
	memoryAllocation->m_MemorySize = allocationSize;
	memoryAllocation->m_MemoryTypeIndex = memoryTypeIndex;
	memoryAllocation->m_bCanBeMapped = (m_MemoryProperties.memoryTypes[memoryTypeIndex].propertyFlags&VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
	memoryAllocation->m_bCoherent = (m_MemoryProperties.memoryTypes[memoryTypeIndex].propertyFlags&VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	memoryAllocation->m_bCached = (m_MemoryProperties.memoryTypes[memoryTypeIndex].propertyFlags&VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT;

	++m_NumAllocations;
	m_PeakNumAllocations = Math::Max<int32>(m_NumAllocations, m_PeakNumAllocations);

	if (m_NumAllocations == m_Device->GetLimits().maxMemoryAllocationCount)
	{
		////////////////////////////
		//_ASSERT(0);
		PlatformUtil::DebugOutFormat("m_NumAllocations >= m_Device->GetLimits().maxMemoryAllocationCount, %d, %d", m_NumAllocations, m_Device->GetLimits().maxMemoryAllocationCount);
	}

	uint32 HeapIndex = m_MemoryProperties.memoryTypes[memoryTypeIndex].heapIndex;

	m_HeapInfos[HeapIndex].Allocations.push_back(memoryAllocation);
	m_HeapInfos[HeapIndex].UsedSize += allocationSize;
	m_HeapInfos[HeapIndex].PeakSize = Math::Max(m_HeapInfos[HeapIndex].PeakSize, m_HeapInfos[HeapIndex].UsedSize);

	return memoryAllocation;
}


void DeviceMemoryPool::Free(DeviceMemoryAllocation* allocation)
{
	//FScopeLock Lock(&GAllocationLock);

	_ASSERT(allocation);
	_ASSERT(allocation->Handle() != VK_NULL_HANDLE);
	::vkFreeMemory(m_Device->GetHandle(), allocation->Handle(), nullptr);

	--m_NumAllocations;

	uint32 HeapIndex = m_MemoryProperties.memoryTypes[allocation->Type()].heapIndex;

	std::vector<DeviceMemoryAllocation*>& heapAllocations = m_HeapInfos[HeapIndex].Allocations;
	auto it = std::find(heapAllocations.begin(), heapAllocations.end(), allocation);
	if (it != heapAllocations.end())
	{
		m_HeapInfos[HeapIndex].Allocations.erase(it);
		m_HeapInfos[HeapIndex].UsedSize -= allocation->Size();
		allocation->m_bFreedBySystem = true;
		delete allocation;
		allocation = nullptr;
	}
}

DeviceMemoryAllocation* DeviceMemoryPool::Allocate(VkDeviceSize allocationSize, uint32 memoryTypeBits, VkMemoryPropertyFlags Properties)
{
	uint32 memoryTypeIndex = 0;
	VK_RESULT(GetMemoryType(memoryTypeBits, Properties, &memoryTypeIndex));
	return Allocate(allocationSize, memoryTypeIndex);
}

VkResult DeviceMemoryPool::GetMemoryType(uint32 memoryTypeBits, VkMemoryPropertyFlags Properties, uint32* outTypeIndex)
{
	// Search memtypes to find first index with those properties
	for (uint32 i = 0; i < m_MemoryProperties.memoryTypeCount && memoryTypeBits; i++)
	{
		if ((memoryTypeBits & 1) == 1)
		{
			// Type is available, does it match user properties?
			if ((m_MemoryProperties.memoryTypes[i].propertyFlags & Properties) == Properties)
			{
				*outTypeIndex = i;
				return VK_SUCCESS;
			}
		}
		memoryTypeBits >>= 1;
	}

	// No memory types matched, return failure
	return VK_ERROR_FEATURE_NOT_PRESENT;
}

VkResult DeviceMemoryPool::GetMemoryTypeExcluding(uint32 TypeBits, VkMemoryPropertyFlags Properties, uint32 ExcludeTypeIndex, uint32* OutTypeIndex)
{
	// Search memtypes to find first index with those properties
	for (uint32 i = 0; i < m_MemoryProperties.memoryTypeCount && TypeBits; i++)
	{
		if ((TypeBits & 1) == 1)
		{
			// Type is available, does it match user properties?
			if ((m_MemoryProperties.memoryTypes[i].propertyFlags & Properties) == Properties && ExcludeTypeIndex != i)
			{
				*OutTypeIndex = i;
				return VK_SUCCESS;
			}
		}
		TypeBits >>= 1;
	}

	// No memory types matched, return failure
	return VK_ERROR_FEATURE_NOT_PRESENT;
}

/*=========================================================================================
	DeviceMemoryAllocation
==========================================================================================*/

DeviceMemoryAllocation::DeviceMemoryAllocation()
	: m_DeviceHandle(VK_NULL_HANDLE)
	, m_MemoryHandle(VK_NULL_HANDLE)
	, m_MemorySize(0)
	, m_MemoryTypeIndex(0)
	, m_Offset(0)
	, m_bCanBeMapped(false)
	, m_bCoherent(false)
	, m_bCached(false)
	, m_MappedData(nullptr)
	, m_bFreedBySystem(false)
{
}

void* DeviceMemoryAllocation::Map(VkDeviceSize size, VkDeviceSize offset)
{
	_ASSERT(CanBeMapped());
	_ASSERT(!IsMapped());
	_ASSERT(size + offset <= m_MemorySize);

	VK_RESULT(vkMapMemory(m_DeviceHandle, m_MemoryHandle, m_Offset, size, 0, &m_MappedData));
	return m_MappedData;
}

void DeviceMemoryAllocation::Unmap()
{
	_ASSERT(IsMapped());

	vkUnmapMemory(m_DeviceHandle, m_MemoryHandle);
	m_MappedData = nullptr;
}

void DeviceMemoryAllocation::FlushMapped()
{
	_ASSERT(!IsCoherent());
	_ASSERT(IsMapped());

	VkMappedMemoryRange mappedMemoryRange;
	Memory::Memzero(mappedMemoryRange);
	mappedMemoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	mappedMemoryRange.memory = m_MemoryHandle;
	mappedMemoryRange.size = m_MemorySize;
	//mappedMemoryRange.offset = 0;
	vkFlushMappedMemoryRanges(m_DeviceHandle, 1, &mappedMemoryRange);
}

void DeviceMemoryAllocation::InvalidateMapped()
{
	_ASSERT(!IsCoherent());
	_ASSERT(IsMapped());

	VkMappedMemoryRange mappedMemoryRange;
	Memory::Memzero(mappedMemoryRange);
	mappedMemoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	mappedMemoryRange.memory = m_MemoryHandle;
	mappedMemoryRange.size = m_MemorySize;
	//mappedMemoryRange.offset = 0;
	vkInvalidateMappedMemoryRanges(m_DeviceHandle, 1, &mappedMemoryRange);

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // end of namespace 