#include "renderer.h"

VulkanContext::VulkanContext(VulkanDevice* inDevice)
	: m_Device(inDevice)
{
}

void VulkanContext::SetVertexBuffer(VertexBuffer* vertexbuffer)
{

}

void VulkanContext::SetIndexBuffer(IndexBuffer* indexbuffer)
{

}

void VulkanContext::SetRenderTarget(IRenderTarget* renderTarget)
{

}

void VulkanContext::SetShader(IShader* shader, const VertexBuffer* vb, const CMaterial* material)
{
}

void VulkanContext::SetRenderState(uint32 rs)
{

}

void VulkanContext::DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint32 StartVertex, uint32 PrimitiveCount)
{
}

void VulkanContext::DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint32 BaseVertex, uint32 StartIndex, uint32 PrimitiveCount, IndexBuffer* IdxBuffer)
{
}

