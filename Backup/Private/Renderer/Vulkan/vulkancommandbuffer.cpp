#include "renderer.h"

VulkanCmdBuffer::VulkanCmdBuffer(VulkanDevice* device, VulkanCmdBufferPool* commandPool, bool bSecondary)
	: m_Device(device)
	, m_CommandPool(commandPool)
	, m_Handle(VK_NULL_HANDLE)
	, m_State(_STATE::READYTOBEGIN)
	, m_bSecondary(bSecondary)
{
	VkCommandBufferAllocateInfo allocateInfo;
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.pNext = nullptr;
	// Drawing List와 유사한 전통적인 Command Buffer. Queue에 Submit된다. Secondary는 Primary CommandBuffer에 참조될수만 있다. (Submit 불가능)
	allocateInfo.level = bSecondary ? VK_COMMAND_BUFFER_LEVEL_SECONDARY : VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocateInfo.commandBufferCount = 1;							// 생성할 CommandBuffer 수
	allocateInfo.commandPool = commandPool->GetHandle();			// CommandPool 핸들

	VK_RESULT(::vkAllocateCommandBuffers(device->GetHandle(), &allocateInfo, &m_Handle));

	m_Fence = m_Device->FencePool().AllocateFence(false);
	m_FenceSignaledCounter = 0;
}

VulkanCmdBuffer::~VulkanCmdBuffer()
{
	if (m_Handle = VK_NULL_HANDLE)
		return;

	if (m_State == _STATE::SUBMITTED)
	{
		uint32 timeout = 60 * 1000 * 1000LL;
		m_Device->FencePool().WaitAndReleaseFence(m_Fence, timeout);
	}
	else
	{
		m_Device->FencePool().ReleaseFence(m_Fence);
	}

	::vkFreeCommandBuffers(m_Device->GetHandle(), m_CommandPool->GetHandle(), 1, &m_Handle);
	m_Handle = VK_NULL_HANDLE;
}

void VulkanCmdBuffer::ExecuteCommands(int32 numSecondaryCmdBuffer, VulkanCmdBuffer** secondaryCmdBuffers)
{
	_ASSERT(IsSecondary() == false);

	std::vector<VkCommandBuffer> buffers;
	buffers.reserve(numSecondaryCmdBuffer);
	for (int32 i = 0; i < numSecondaryCmdBuffer; i++)
	{
		buffers.push_back(secondaryCmdBuffers[i]->Handle());
	}
	vkCmdExecuteCommands(m_Handle, buffers.size(), buffers.data());
}

void VulkanCmdBuffer::Reset()
{
	// FreeCommandBuffer -> AllocateCommandBuffer 하는 것보다 훨씬 효율적이다.

	if (m_Handle != VK_NULL_HANDLE)
	{
		_ASSERT(m_CommandPool->CanBeReset());
		VK_RESULT(vkResetCommandBuffer(m_Handle, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT));

		m_State = _STATE::READYTOBEGIN;
	}
}

void VulkanCmdBuffer::Begin(UsageFlags::Type usageFlags)
{
	ASSERT(m_State == _STATE::READYTOBEGIN, "m_State != _STATE::READYTOBEGIN");

	/*
		VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT : Secondary Command Buffer에 사용한다.
		VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT : 한번 Recording 된 Primary Command Buffer 를 resubmit 가능하다.
		VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT : 한번만 SUBMIT 된 후, CommandBuffer가 Reset 된다.
	*/
	m_UsageFlags = usageFlags;
	VkCommandBufferUsageFlags bufferUsageFlags = 0;
	if (usageFlags&UsageFlags::_RENDER_PASS_CONTINUE)
		bufferUsageFlags |= VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
	if (usageFlags&UsageFlags::_SIMULTANEOUS_USE_BIT)
		bufferUsageFlags |= VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	if (usageFlags&UsageFlags::_ONETIME_SUBMIT)
	{
		_ASSERT(m_CommandPool->CanBeReset());
		bufferUsageFlags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	}

	VkCommandBufferBeginInfo info;
	Memory::Memzero(info);
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.flags = bufferUsageFlags;
	info.pNext = nullptr;
	info.pInheritanceInfo = nullptr;
	VK_RESULT(::vkBeginCommandBuffer(m_Handle, &info));

	m_State = _STATE::BEGIN;
}

void VulkanCmdBuffer::End()
{
	_ASSERT(m_State == _STATE::BEGIN);

	VK_RESULT(::vkEndCommandBuffer(m_Handle));
	
	m_State = _STATE::END;
}

void VulkanCmdBuffer::Submit(VulkanQueue* queue)
{
	_ASSERT(m_State == _STATE::END);

	m_State = _STATE::SUBMITTED;

	m_CommandPool->RefreshFenceStatus();
}

void VulkanCmdBuffer::BeginRenderPass(VulkanFrameBuffer* framebuffer)
{
	_ASSERT(m_State == _STATE::BEGIN);

	const Vulkan::RenderTargetDesc& RTDesc = framebuffer->GetRenderTargetDesc();
	const Vulkan::RenderTargetLayout& RTLayout = RTDesc.layout;

	VkRenderPassBeginInfo info;
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	info.pNext = nullptr;
	info.framebuffer = framebuffer->GetHandle();
	info.renderPass = framebuffer->GetRenderPass()->GetHandle();
	info.renderArea.offset.x = 0;
	info.renderArea.offset.y = 0;
	info.renderArea.extent.width = RTDesc.width;
	info.renderArea.extent.height = RTDesc.height;
	info.clearValueCount = RTLayout.GetNumAttachments();
	info.pClearValues = RTDesc.clearValues;

	vkCmdBeginRenderPass(m_Handle, &info, VK_SUBPASS_CONTENTS_INLINE/*VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS*/);

	m_State = _STATE::RENDERPASS;
}

void VulkanCmdBuffer::EndRenderPass()
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdEndRenderPass(m_Handle);

	m_State = _STATE::BEGIN;
}

void VulkanCmdBuffer::ClearColor(VkImage imageHandle, VkImageLayout imageLayout, const VkClearColorValue& clearValue)
{
	vkCmdClearColorImage(m_Handle, imageHandle, imageLayout, &clearValue, 1, &Vulkan::DefaultSubresourceRange());
}

void VulkanCmdBuffer::ClearDepthStencil(VkImage imageHandle, VkImageLayout imageLayout, const VkClearDepthStencilValue& clearValue)
{
	vkCmdClearDepthStencilImage(m_Handle, imageHandle, imageLayout, &clearValue, 1, &Vulkan::DefaultSubresourceRange());
}

void VulkanCmdBuffer::ClearAttachments(uint32 attachmentCount, const VkClearAttachment* pAttachments, uint32 rectCount,	const VkClearRect* pRects)
{
	vkCmdClearAttachments(m_Handle, attachmentCount, pAttachments, rectCount, pRects);
}

void VulkanCmdBuffer::ClearResolve(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageResolve* pRegions)
{
	vkCmdResolveImage(m_Handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void VulkanCmdBuffer::SetViewport(float width, float height, float posX, float posY, float minDepth, float maxDepth)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	VkViewport vp;
	vp.x = posX;
	vp.y = posY;
	vp.width = width;
	vp.height = height;
	vp.minDepth = minDepth;
	vp.maxDepth = maxDepth;

	vkCmdSetViewport(m_Handle, 0, 1, &vp);
}

void VulkanCmdBuffer::SetScissor(uint32 width, uint32 height, int32 posX, int32 posY)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	VkRect2D rect;
	rect.offset.x = posX;
	rect.offset.y = posY;
	rect.extent.width = width;
	rect.extent.height = height;

	vkCmdSetScissor(m_Handle, 0, 1, &rect);
}

void VulkanCmdBuffer::SetLineWidth(float lineWidth)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetLineWidth(m_Handle, lineWidth);
}

void VulkanCmdBuffer::SetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetDepthBias(m_Handle, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
}

void VulkanCmdBuffer::SetDepthBounds(float minDepthBounds, float maxDepthBounds)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetDepthBounds(m_Handle, minDepthBounds, maxDepthBounds);
}

void VulkanCmdBuffer::SetBlendConstants(float blendConstants[4])
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetBlendConstants(m_Handle, blendConstants);
}

void VulkanCmdBuffer::SetStencilCompareMask(uint32 faceMask, uint32 compareMask)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetStencilCompareMask(m_Handle, faceMask, compareMask);
}

void VulkanCmdBuffer::SetStencilWriteMask(uint32 faceMask, uint32 writeMask)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetStencilWriteMask(m_Handle, faceMask, writeMask);
}

void VulkanCmdBuffer::SetStencilReference(uint32 faceMask, uint32 reference)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdSetStencilReference(m_Handle, faceMask, reference);
}

void VulkanCmdBuffer::BindDescriptorSets(VulkanDescriptorSets* descriptor, VulkanPipelineLayout* pipelineLayout)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	const std::vector<VkDescriptorSet>& sets = descriptor->GetHandles();
	vkCmdBindDescriptorSets(m_Handle, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout->Handle(), 0, sets.size(), sets.data(), 0, nullptr);
}

void VulkanCmdBuffer::BindDescriptorSets(VulkanShader* shader)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	VulkanDescriptorSets* descriptor = shader->GetDescriptorSets();
	VulkanPipelineLayout* pipelineLayout = shader->GetPipelineLayout();

	BindDescriptorSets(descriptor, pipelineLayout);
}

void VulkanCmdBuffer::BindPipeline(VulkanPipeline* pipeline)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	/*	- 파이프라인을 Bind할 때에는 GRAPHIC / COMPUTE를 Command Buffer State에 맞게 바인드 해줘야 한다.
		- 현재 해당 체크 코드가 없는 상태이고, Graphic Pipeline만 사용한다고 가정하자!!!
	*/
	vkCmdBindPipeline(m_Handle, pipeline->GetType(), pipeline->GetHandle());
}

void VulkanCmdBuffer::BindVertexBuffer(VulkanVertexBuffer* vertexBuffer, uint64* Offsets)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	const VkDeviceSize offset[1] = { 0 };
	VkBuffer buffer = vertexBuffer->GetHandle();
	vkCmdBindVertexBuffers(m_Handle, 0, 1, &buffer, offset);
}

void VulkanCmdBuffer::BindIndexBuffer(VulkanIndexBuffer* indexBuffer, uint StartIndex)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdBindIndexBuffer(m_Handle, indexBuffer->GetHandle(), StartIndex, indexBuffer->IsIndex16() ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32);
}

void VulkanCmdBuffer::Draw(uint StartVertex, uint VertexCount, uint32 StartInstance, uint32 InstanceCount)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdDraw(m_Handle, VertexCount, StartInstance, StartVertex, InstanceCount);
}

void VulkanCmdBuffer::DrawIndexed(int32 BaseVertex, uint StartIndex, uint IndexCount, uint32 StartInstance, uint32 InstanceCount)
{
	_ASSERT(m_State == _STATE::RENDERPASS);

	vkCmdDrawIndexed(m_Handle, IndexCount, InstanceCount, StartIndex, BaseVertex, StartInstance);
}

void VulkanCmdBuffer::CopyBuffer(VkBuffer srcBuffer, uint32 srcOffset, VkBuffer dstBuffer, uint32 dstOffset, uint32 bufferSize)
{
	/* 주의사항)
	1. 디바이스에 의해 CopyBuffer 커멘드가 실행되기 전에 source region 안에 데이터가 있어야 한다.
	2. 디바이스에서 커멘드가 실행된 이후까지 source region 안에 데이터가 있어야 한다.
	3. 디바이스에서 커멘드가 실행된 이후까지 destination 데이터를 읽을 수 없어야 한다.
	*/
	VkBufferCopy copyRegion;
	Memory::Memzero(copyRegion);
	copyRegion.srcOffset = srcOffset;	// From: SourceBuffer + Offset
	copyRegion.dstOffset = dstOffset;	// To: DestBuffer + Offset
	copyRegion.size = bufferSize;

	vkCmdCopyBuffer(m_Handle, srcBuffer, dstBuffer, 1, &copyRegion);
}

void VulkanCmdBuffer::CopyImage(VkImage srcImage, VkImageLayout srcImageLayout,	VkImage dstImage, VkImageLayout dstImageLayout,	uint32 regionCount,	const VkImageCopy* pRegions)
{
	vkCmdCopyImage(m_Handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void VulkanCmdBuffer::CopyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout,	uint32 regionCount,	const VkBufferImageCopy* pRegions)
{
	vkCmdCopyBufferToImage(m_Handle, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
}

void VulkanCmdBuffer::CopyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32 regionCount, const VkBufferImageCopy* pRegions)
{
	vkCmdCopyImageToBuffer(m_Handle, srcImage, srcImageLayout, dstBuffer, regionCount, pRegions);
}

void VulkanCmdBuffer::BlitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageBlit* pRegions, VkFilter filter)
{
	vkCmdBlitImage(m_Handle, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter);
}

void VulkanCmdBuffer::UpdateBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData)
{
	vkCmdUpdateBuffer(m_Handle, dstBuffer, dstOffset, dataSize, pData);
}

void VulkanCmdBuffer::FillBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize size, uint32 data)
{
	vkCmdFillBuffer(m_Handle, dstBuffer, dstOffset, size, data);
}

void VulkanCmdBuffer::TransferBuffer(VkBuffer bufferHandle, uint32 bufferSize, VkAccessFlags srcAccessMask, VkAccessFlags dstAccessMask)
{
	_ASSERT(m_Handle != VK_NULL_HANDLE);

	VkBufferMemoryBarrier bufferBarrier;
	Memory::Memzero(bufferBarrier);
	bufferBarrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
	bufferBarrier.pNext = nullptr;
	bufferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	bufferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	bufferBarrier.buffer = bufferHandle;
	bufferBarrier.size = bufferSize;
	bufferBarrier.srcAccessMask = srcAccessMask;
	bufferBarrier.dstAccessMask = dstAccessMask;
}

/*	ImageLayout을 전환한다.
	- 이미지 생성하는 동안 우리가 원하는 다른 타입의 수행을 위해서 다른 usage flags를 명시해야만 한다.
	- 각 타입의 이미지 처리는 다른 Image Layout에 연결된다.
*/
void VulkanCmdBuffer::TransitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout, const VkImageSubresourceRange& subresourceRange)
{
	//_ASSERT(CmdBuffer->GetState() == VulkanCmdBuffer::_STATE::BEGIN);
	_ASSERT(m_Handle != VK_NULL_HANDLE);

	// Barrier는 우리가 기대했던 결과를 얻는다는 것을 확인하는 특수한 순서에 발생하는 우리의 operation이 GPU에서 완료되었다는 것을 확인한다.
	// 하나의 Barrier는 큐에서 두 가지 수행으로 나뉜다: Barrier 이전과 Barrier 이후
	VkImageMemoryBarrier imageBarrier;
	Memory::Memzero(imageBarrier);
	imageBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageBarrier.pNext = nullptr;
	imageBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageBarrier.oldLayout = oldLayout;
	imageBarrier.newLayout = newLayout;
	imageBarrier.image = image;
	imageBarrier.subresourceRange = subresourceRange;

	/*
		VK_IMAGE_LAYOUT_UNDEFINED
		- device 접근을 지원하지 않는다.
		- 이 layout은 반드시 initialLayout이나 image transition에서 oldLayout에서 사용되어야만 한다.
		- 이 layout의 밖으로 transition 될 때, 메모리의 내용이 보존된다 것을 보장하지 않는다.

		VK_IMAGE_LAYOUT_GENERAL
		- 모든 타입의 device 접근을 허용한다.

		VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
		- 오직 VkFramebuffer에 color 혹은 resolve attachment로 사용되어야만 한다.
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들 대해서만 유효하다.

		VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
		- 오직 VkFramebuffer에 depth/stencil attachment로 사용되어야만 한다.
		- 이 layout은 오직 활성화된 VK_IAMGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
		- 오직 VkFramebuffer에 depth/stencil attachment를 read-only로 사용되어야만 한다. (and/or) 셰이더 내에 read-only image로 사용된다. (samgpled image로 읽을 수 있고, image/sampler 와 input attachment를 조합할 수 있다.)
		- 이 layout은 오직 활성화된 VK_IAMGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT usage bit를 가지고 생성된 이미지들의 이미지 subresource들에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
		- 셰이더 내에 read-only image로 사용된다. (samgpled image로 읽을 수 있고, image/sampler 와 input attachment를 조합할 수 있다.)
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_SAMPLED_BIT나 VK_IMAGE_USAGE_INPUT_ATTACHMENT usage bit를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
		- transfer command의 source image로 사용되어야만 한다. (VK_PIPELINE_STAGE_TRANSFER_BIT 참고)
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_TRANSFER_SRC_BIT를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
		- transfer command의 destination image로 사용되어야만 한다.
		- 이 layout은 오직 활성화된 VK_IMAGE_USAGE_TRANSFER_DST_BIT를 가지고 생성된 이미지들의 이미지 subresource에 대해서만 유효하다.

		VK_IMAGE_LAYOUT_PREINITIALIZED
		- device 접근을 지원하지 않는다.
		- 이 layout은 반드시 initialLayout이나 image transition에서 oldLayout에서 사용되어야만 한다.
		- layout 밖으로 transition 될 때, 메모리의 내용은 보존된다.
		- 이 layout은 host에 의해 쓰여진 내용의 이미지에 대해 초기 layout으로 사용된다는 의도이다. 그리고, 이런 이유로 최초 layout transtion 실행없이 데이터는 즉시 메모리에 작성할 수 있다.
		- 현재 VK_IMAGE_LAYOUT_PREINITIALIZED는 오직 VK_IMAGE_TILTING_LINEAR 이미지에 유용하다. 왜냐하면, 이것은 VK_IMAGE_TILING_OPTIMAL 이미지로 정의된 standard layout 이 아니다.

		VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
		- 디스플레이를 위해서 Swapchain 이미지를 Present하는데 사용되어야만 한다.
		- Swapchain 이미지는 반드시 VkQueuePresentKHR이 호출되기 전에 이 layout으로 transtion되어야만 한다.
		- VkQueuePresentKHR이 호출된 후에 이 layout으로 부터 transition 되어져야만 한다. (transition away)

		VK_ACCESS_INDIRECT_COMMAND_READ_BIT
		-
		VK_ACCESS_INDEX_READ_BIT = 0x00000002,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT = 0x00000004,
		VK_ACCESS_UNIFORM_READ_BIT = 0x00000008,
		VK_ACCESS_INPUT_ATTACHMENT_READ_BIT = 0x00000010,
		VK_ACCESS_SHADER_READ_BIT = 0x00000020,
		VK_ACCESS_SHADER_WRITE_BIT = 0x00000040,
		VK_ACCESS_COLOR_ATTACHMENT_READ_BIT = 0x00000080,
		VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT = 0x00000100,
		VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT = 0x00000200,
		VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT = 0x00000400,
		VK_ACCESS_TRANSFER_READ_BIT
		- access는 transfer operation로 부터 읽기를 한다.

		VK_ACCESS_TRANSFER_WRITE_BIT
		- access는 transfer operation로 부터 쓰기를 한다.

		VK_ACCESS_HOST_READ_BIT = 0x00002000,
		VK_ACCESS_HOST_WRITE_BIT = 0x00004000,
		VK_ACCESS_MEMORY_READ_BIT
		- 메모리에 attach된 명시되지 않은 unit을 읽는다.
		- 이 unit은 vulkan device 외부 이거나 코어 vulkan pipeline의 일부가 아니다.
		- dstAccessMask에 포함되어 있을 때, srcStageMask에 파이프라인 스테이지들에 의해 수행된 srcAccessMask에 access type을 사용한 모든 쓰기는 메모리에 보여져야만 한다.

		VK_ACCESS_MEMORY_WRITE_BIT = 0x00010000,
		VK_ACCESS_FLAG_BITS_MAX_ENUM = 0x7FFFFFFF
	*/
	switch (newLayout)
	{
	case VK_IMAGE_LAYOUT_GENERAL:
		if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		break;
	case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
		if (oldLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		}
		imageBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		break;
	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
		imageBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
		break;
	case VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL:
		_ASSERT(0);
		break;
	case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
		if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		imageBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
		break;
	case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
		_ASSERT(0);
		break;
	case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
		imageBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		break;
	case VK_IMAGE_LAYOUT_PREINITIALIZED:
		_ASSERT(0);
		break;
	case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
		if (oldLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
		{
			imageBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
		imageBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
		break;
	default:
		_ASSERT(0);
		break;
	}

	VkImageMemoryBarrier BarrierList[] = { imageBarrier };

	/*	Pipeline Stage Flags
		: logical pipeline 이벤트들이 시그널 될 수 있는 곳이 어딘지 명시. 실행 의존성의 source와 destination을 명시

		VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT
		- Command들이 처음에 Queue에 의해 받아들여지는 파이프라인의 Stage

		VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT
		- Draw/DispatchIndirect 데이터 구조체가 소비되는 파이프라인 Stage

		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
		- 버텍스와 인덱스 버퍼가 소비되는 파이프라인 Stage

		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT
		- Vertex Shader Stage

		VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT
		- Tessellation Control Shader Stage

		VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT
		- Tessellation Evaulation Shader Stage

		VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT
		- Geometry Shader Stage

		VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT
		- Fragment Shader Stage

		VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
		- 미리(early) fragment tests(fragment shading 호출 전에 depth나 stencil test)를 수행하는 파이프라인 스테이지

		VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT
		- 늦은(late) fragment tests(fragment shading 호출 후 depth,stencil test)를 수행하는 파이프라인 스테이지

		VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
		- 최종 칼라값이 pipeline으로부터 출력되고 Blending 이후 파이프라인 스테이지
		- 이 stage는 subpass의 마지막에 발생하는 resolve 오퍼레이션이 포함된다.
		- 값들이 메모리에 Commit 되어지는 불필요한 지시를 하지 말아야 한다.

		VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT
		- Compute Shader의 실행

		VK_PIPELINE_STAGE_TRANSFER_BIT
		- Copy Command의 실행
		- 이것은 모든 transfer commands로 부터 수행 결과를 포함한다.
		- transfer command 의 세트는 vkCmdCopyBuffer, vkCmdCopyImage, vkCmdBlitImage, VkCmdCopyBufferToImage, vkCmdCopyImageToBuffer,
		vkCmdUpdateBuffer, vkCmdFillBuffer, vkCmdClearColorImage, vkCmdClearDepthStencilImage, vkCmdResolveImage, vkCmdCopyQueryPoolResults로 구성된다.

		VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
		- Command가 실행이 완료되는 파이프라인에서 Final Stage

		VK_PIPELINE_STAGE_HOST_BIT
		- 디바이스 메모리의 읽기/쓰기 호스트에 실행을 지시하는 pseudo-stage

		VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT
		- 모든 graphics 파이프라인 Stage의 실행

		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT
		- Queue에 지원되는 모든 Stage의 실행
	*/
	VkPipelineStageFlags srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	VkPipelineStageFlags dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;

	if (oldLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
	{
		srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	}
	else if (newLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
	{
		srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	}

	/*	Pipeline Barrier
		- Command buffer내 초기(eariler) command의 세트와 command buffer내 이후(later) command의 세트 사이의 메모리 의존성과 실행 의존성을 추가한다.
		- dstStageMask에 오직 VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT와 실행 의존성은 다음 순서의 Commands를 지연시키지 않을 것이다.
		- VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT는 다음 access가 다른 queue에서나 presentation engine에 의해 완료되었을 memory barriers와 layout transition을 완수하는데 유용하다.
		- 이 경우에 같은 Queue에서 다음 순서의 command들은 대기할 필요가 없지만, barrier나 transition은 반드시 batch 시그널과 관련된 이전의 세마포어들이 완료되어야만 한다.
		- 만약 구현이 파이프라인의 어떤 명시된 스테이지에 이벤트 스테이트를 업데이트를 할 수 없다면, 대신에 논리적으로 마지막 Stage에 업데이트한다.
	*/
	::vkCmdPipelineBarrier(m_Handle,
							srcStageMask,	//  
							dstStageMask,	// 
							0,				// VkDependencyFlags 
							0, nullptr,		// MemoryBarrier
							0, nullptr,		// BufferMemoryBarrier
							1, BarrierList);// ImageMemoryBarrier
}

void VulkanCmdBuffer::TransitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout)
{
	TransitionImageLayout(image, oldLayout, newLayout, Vulkan::DefaultSubresourceRange());
}

void VulkanCmdBuffer::RefreshFenceStatus()
{
	if (m_State == _STATE::SUBMITTED)
	{
		if (m_Device->FencePool().IsSignaled(m_Fence))
		{
			Reset();

			m_Device->FencePool().ResetFence(m_Fence);

			m_FenceSignaledCounter++;
		}
	}
	else
	{
		_ASSERT(m_Fence->IsSignaled() == false);
	}
}
