#include "renderer.h"

VulkanRenderState::VulkanRenderState(VulkanDriver* driver)
	: m_Driver(driver)
{
}

void VulkanRenderState::Reset()
{
	RenderState::Reset();
}

void VulkanRenderState::Update(uint32 rs, int32 alphaRef)
{
	if (rs&CULL_FLIP)
	{
		if ((rs&CULL_MASK) != CULL_NONE)
		{
			// XOR : 다르면 1, 같으면 0
			rs ^= CULL_CW;
		}
		rs &= ~CULL_FLIP;
	}
	ASSERT(!(rs&CULL_FLIP), "renderState&CULL_FLIP");

	// 서로 다른 비트만 체크
	unsigned long rs_xor = m_RenderState ^ rs;
	if (rs_xor == 0)
		return;

	if (rs_xor&CULL_MASK)
	{
		switch (rs&CULL_MASK)
		{
		case CULL_CCW:
			m_Driver->SetCullMode(CULLMODE::_CCW);
			break;
		case CULL_CW:
			m_Driver->SetCullMode(CULLMODE::_CW);
			break;
		case CULL_NONE:
			m_Driver->SetCullMode(CULLMODE::_NONE);
			break;
		}
	}

	if (rs_xor&WIREFRAMEENABLE)
	{
		if (rs&WIREFRAMEENABLE)
			m_Driver->SetFillMode(FILLMODE::_LINE);
		else
			m_Driver->SetFillMode(FILLMODE::_FILL);
	}

	if (rs_xor&ZDISABLE)
	{
		bool bDepthTestEnable = !(rs&ZDISABLE);
		m_Driver->SetDepthTestEnable(bDepthTestEnable);
	}

	if (rs_xor&ZWRITEDISABLE)
	{
		bool bDepthWriteEnable = !(rs&ZWRITEDISABLE);
		m_Driver->SetDepthWriteEnable(bDepthWriteEnable);
	}

	if (rs_xor&ZFUNC_MASK)
	{
		switch (rs&ZFUNC_MASK)
		{
		case ZFUNC_LESSEQUAL:
			m_Driver->SetDepthFunc(COMPAREFUNC::_LESS_EQUAL);
			break;
		case ZFUNC_EQUAL:
			m_Driver->SetDepthFunc(COMPAREFUNC::_EQUAL);
			break;
		case ZFUNC_GREATER:
			m_Driver->SetDepthFunc(COMPAREFUNC::_GREATER);
			break;
		case ZFUNC_LESS:
			m_Driver->SetDepthFunc(COMPAREFUNC::_LESS);
			break;
		case ZFUNC_GREATEREQUAL:
			m_Driver->SetDepthFunc(COMPAREFUNC::_GREATER_EQUAL);
			break;
		case ZFUNC_ALWAYS:
			m_Driver->SetDepthFunc(COMPAREFUNC::_ALWAYS);
			break;
		}
	}

	if (rs_xor&COLORWRITEDISABLE)
	{
		if (!(rs&COLORWRITEDISABLE))
			m_Driver->SetBlendWriteMask(PipelineState::ColorBlendDesc::COLOR_RGBA);
		else
			m_Driver->SetBlendWriteMask(PipelineState::ColorBlendDesc::COLOR_NONE);
	}

	if (rs_xor&ALPHABLEND_MASK)
	{
		switch (rs&ALPHABLEND_MASK)
		{
		case ALPHABLEND_COPY:
			m_Driver->SetBlendEnable(false);
			break;
		case ALPHABLEND_TRANSLUCENT:
			m_Driver->SetBlendEnable(true);
			if (rs_xor&ALPHABLEND_PREMULPLIED)	
				m_Driver->SetBlendAlphaFunc(BLENDFACTOR::_ONE, BLENDFACTOR::_ONE_MINUS_SRC_ALPHA); // SrcColor * 1.0 + DestColor * (1-SrcColor.a)
			else 
				m_Driver->SetBlendAlphaFunc(BLENDFACTOR::_SRC_ALPHA, BLENDFACTOR::_ONE_MINUS_SRC_ALPHA); // SrcColor * SrcColor.a + DestColor * (1-SrcColor.a)
			break;
		case ALPHABLEND_ADD:
			m_Driver->SetBlendEnable(true);
			m_Driver->SetBlendAlphaFunc(BLENDFACTOR::_ONE, BLENDFACTOR::_ONE); // SrcColor  * 1.0f + DestColor * 1.0f
			break;
		case ALPHABLEND_MULADD:
			m_Driver->SetBlendEnable(true);
			m_Driver->SetBlendAlphaFunc(BLENDFACTOR::_SRC_ALPHA, BLENDFACTOR::_ONE); // SrcColor.rgb * SrcColor.a + Dest
			break;
		case ALPHABLEND_MULTIPLY:
			m_Driver->SetBlendEnable(true);
			m_Driver->SetBlendAlphaFunc(BLENDFACTOR::_ZERO, BLENDFACTOR::_SRC_COLOR); // SrcColor * 0.0f + DestColor * SrcColor
			break;
		case ALPHABLEND_INVMULTIPLY:
			m_Driver->SetBlendEnable(true);
			m_Driver->SetBlendAlphaFunc(BLENDFACTOR::_ZERO, BLENDFACTOR::_ONE_MINUS_SRC_COLOR); // SrcColor * 0.0f + DestColor * (1.0f - SrcColor)
			break;
		}
	}

	if (rs_xor&ALPHATESTENABLE)
	{
		m_Driver->SetBlendEnable(false);
	}

	m_RenderState = rs;
}
