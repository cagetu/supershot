#include "renderer.h"

namespace Vulkan
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	RenderTargetLayout::RenderTargetLayout()
	{
		Memory::Memzero(attachmentDesc);
		Memory::Memzero(colorAttachments);
		Memory::Memzero(resolveAttachments);
		Memory::Memzero(depthStencilAttachment);

		numAttachments = 0;
		numColorAttachments = 0;
		hasResolveAttachments = false;
		hasDepthStencilAttachment = false;
	}

	void RenderTargetLayout::Clear()
	{
		Memory::Memzero(attachmentDesc);
		Memory::Memzero(colorAttachments);
		Memory::Memzero(resolveAttachments);
		Memory::Memzero(depthStencilAttachment);

		numAttachments = 0;
		numColorAttachments = 0;
		hasResolveAttachments = false;
		hasDepthStencilAttachment = false;
	}

	uint32 RenderTargetLayout::AddColorAttachment(VulkanDevice* device, int32 format, int32 multiSamples, RTLoadAction loadAction, RTStoreAction storeAction)
	{
		const uint32 colorIndex = numColorAttachments;
		const uint32 attachmentIndex = numAttachments;

		// AttachmentReference
		{
			/// attachmentDesc 배열 인덱스
			colorAttachments[colorIndex].attachment = attachmentIndex;
			/// attachment가 subpass에서 사용할 layout. subpass 전에서 제공된 layout으로 자동으로 전환된다.
			colorAttachments[colorIndex].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		}
		// AttachmentDescription
		{
			VkAttachmentDescription& desc = attachmentDesc[attachmentIndex];
			desc.flags = 0;
			/// attachment Format
			desc.format = device->GetFormat(format);
			/// 이미지의 sample 수 (멀티샘플링 지원 여부 체크)
			desc.samples = device->GetSampleCount(multiSamples);
			/// 렌더패스가 시작할 때 이미지의 내용을 어떻게 처리할 것인지 결정 (Clear, Preserve, don't care).
			desc.loadOp = LoadAction(loadAction);
			/// 렌더패스 이후에 이미지의 내용을 어떻게 할 것인지 드라이버에게 정보를 준다.
			desc.storeOp = StoreAction(storeAction);
			/// depth/stencil 이미지들의 stencil 파트에 대해서 LoadAction 설정. color attachment들에 대해서는 무시된다.
			desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			/// depth/stencil 이미지들의 stencil 파트에 대해서 StoreAction 설정. color attachment들에 대해서는 무시된다.
			desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			/// 렌더패스가 시작할 때, attachment가 가질 layout
			desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			/// 렌더패스가 끝났을 때 이미지를 자동으로 Layout으로 전환한다.
			desc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		}
		{
			if (multiSamples == 1)
			{
				resolveAttachments[colorIndex].attachment = VK_ATTACHMENT_UNUSED;
				resolveAttachments[colorIndex].layout = VK_IMAGE_LAYOUT_UNDEFINED;
			}
			else
			{
				_ASSERT(0);
			}
		}
		// Attachment
		numAttachments++;
		numColorAttachments++;
		return attachmentIndex;
	}

	uint32 RenderTargetLayout::AddDepthStencilAttachment(VulkanDevice* device, int32 format, int32 multiSamples, RTLoadAction depthLoadAction, RTStoreAction depthStoreAction, RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction)
	{
		const uint32 attachmentIndex = numAttachments;

		// AttachmentReference
		{
			depthStencilAttachment.attachment = attachmentIndex;
			depthStencilAttachment.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		}
		// AttachmentDescription
		{
			VkAttachmentDescription& desc = attachmentDesc[attachmentIndex];
			desc.format = device->GetFormat(format);
			desc.samples = device->GetSampleCount(multiSamples);
			desc.loadOp = LoadAction(depthLoadAction);
			desc.storeOp = StoreAction(depthStoreAction);
			desc.stencilLoadOp = LoadAction(stencilLoadAction);
			desc.stencilStoreOp = StoreAction(stencilStoreAction);
			/// depth/stencil 이미지들의 stencil 파트에 대해서 StoreAction 설정. color attachment들에 대해서는 무시된다.
			//desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			//desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			desc.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		}
		// Attachment
		numAttachments++;
		hasDepthStencilAttachment = true;
		return attachmentIndex;
	}

	bool RenderTargetLayout::Compare(const RenderTargetLayout& rhs) const
	{
		if (numAttachments != rhs.numAttachments ||
			numColorAttachments != rhs.numColorAttachments ||
			hasResolveAttachments != rhs.hasResolveAttachments ||
			hasDepthStencilAttachment != rhs.hasDepthStencilAttachment)
			return false;

		if (numColorAttachments == rhs.numColorAttachments)
		{
			if (memcmp(colorAttachments, rhs.colorAttachments, sizeof(VkAttachmentReference) * numColorAttachments) != 0)
				return false;
		}
		if (hasResolveAttachments == rhs.hasResolveAttachments)
		{
			if (memcmp(resolveAttachments, rhs.resolveAttachments, sizeof(VkAttachmentReference) * numColorAttachments) != 0)
				return false;
		}
		if (hasDepthStencilAttachment == rhs.hasDepthStencilAttachment)
		{
			if (memcmp(&depthStencilAttachment, &rhs.depthStencilAttachment, sizeof(VkAttachmentReference)) != 0)
				return false;
		}

		if (numAttachments != rhs.numAttachments)
		{
			if (memcmp(attachmentDesc, rhs.attachmentDesc, sizeof(VkAttachmentDescription) * numAttachments) != 0)
				return false;
		}
		return true;
	}

	bool RenderTargetLayout::operator==(const RenderTargetLayout& rhs)
	{
		return Compare(rhs);
	}

	bool RenderTargetLayout::operator!=(const RenderTargetLayout& rhs)
	{
		return (*this) == rhs ? false : true;
	}

	bool RenderTargetLayout::operator==(const RenderTargetLayout& rhs) const
	{
		return Compare(rhs);
	}

	bool RenderTargetLayout::operator!=(const RenderTargetLayout& rhs) const
	{
		return (*this) == rhs ? false : true;
	}

	////
	
	RenderTargetDesc::RenderTargetDesc()
	{
		Memory::Memzero(attachmentViews);
		Memory::Memzero(clearValues);
	}

	void RenderTargetDesc::Clear()
	{
		layout.Clear();

		Memory::Memzero(attachmentViews);
		Memory::Memzero(clearValues);
	}

	uint32 RenderTargetDesc::AddColorAttachment(VulkanDevice* device, VkImageView attachment, int32 format, int32 multiSamples, RTLoadAction loadAction, RTStoreAction storeAction, const RGBA& clearColor)
	{
		const uint32 attachmentIndex = layout.GetNumAttachments();

		attachmentViews[attachmentIndex] = attachment;

		clearValues[attachmentIndex].color.float32[0] = clearColor.r;
		clearValues[attachmentIndex].color.float32[1] = clearColor.g;
		clearValues[attachmentIndex].color.float32[2] = clearColor.b;
		clearValues[attachmentIndex].color.float32[3] = clearColor.a;

		layout.AddColorAttachment(device, format, multiSamples, loadAction, storeAction);
		return attachmentIndex;
	}

	uint32 RenderTargetDesc::AddDepthStencilAttachment(VulkanDevice* device, VkImageView attachment, int32 format, int32 multiSamples, 
													   RTLoadAction depthLoadAction, RTStoreAction depthStoreAction, float clearDepth,
													   RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction, ushort clearStencil)
	{
		const uint32 attachmentIndex = layout.GetNumAttachments();

		attachmentViews[attachmentIndex] = attachment;
		clearValues[attachmentIndex].depthStencil.depth = clearDepth;
		clearValues[attachmentIndex].depthStencil.stencil = clearStencil;

		layout.AddDepthStencilAttachment(device, format, multiSamples, depthLoadAction, depthStoreAction, stencilLoadAction, stencilStoreAction);
		return attachmentIndex;
	}

	bool RenderTargetDesc::operator==(const RenderTargetDesc& rhs)
	{
		if (layout != rhs.layout)
			return false;

		if (layout.GetNumAttachments() == rhs.layout.GetNumAttachments())
		{
			if (memcmp(attachmentViews, rhs.attachmentViews, sizeof(VkImageView) * layout.GetNumAttachments()) != 0)
				return false;
		}

		return true;
	}

	bool RenderTargetDesc::operator==(const RenderTargetDesc& rhs) const
	{
		if (layout != rhs.layout)
			return false;

		if (layout.GetNumAttachments() == rhs.layout.GetNumAttachments())
		{
			if (memcmp(attachmentViews, rhs.attachmentViews, sizeof(VkImageView) * layout.GetNumAttachments()) != 0)
				return false;
		}

		return true;
	}

	bool RenderTargetDesc::operator!=(const RenderTargetDesc& rhs)
	{
		return (*this) == rhs ? false : true;
	}

	bool RenderTargetDesc::operator!=(const RenderTargetDesc& rhs) const
	{
		return (*this) == rhs ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	TextureView::TextureView()
		: m_ImageView(VK_NULL_HANDLE)
		, m_MipmapCount(0)
		, m_ArrayCount(0)
	{
	}

	void TextureView::Create(VulkanDevice* device, VkImage image, VkImageViewType viewType, VkImageAspectFlags aspectMask, int32 format, uint32 mipMaps, uint32 arraySize)
	{
		_ASSERT(m_ImageView == VK_NULL_HANDLE);

		VkImageViewCreateInfo createInfo;
		Memory::Memzero(createInfo);
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.image = image;
		createInfo.viewType = viewType;
		createInfo.format = device->GetFormat(format);
		createInfo.components = device->GetFormatCommpoentMapping(format);
		////SubresourceRange
		createInfo.subresourceRange.aspectMask = aspectMask;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = mipMaps;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = arraySize * (viewType == VK_IMAGE_VIEW_TYPE_CUBE ? 6 : 1);

		VK_RESULT(vkCreateImageView(device->GetHandle(), &createInfo, nullptr, &m_ImageView));

		m_MipmapCount = mipMaps;
		m_ArrayCount = arraySize;
	}

	void TextureView::Destroy(VulkanDevice* device)
	{
		if (m_ImageView)
		{
			vkDestroyImageView(device->GetHandle(), m_ImageView, nullptr);
			m_ImageView = VK_NULL_HANDLE;
		}
	}

	//
	//
	//
	TextureSampler::TextureSampler()
		: m_Sampler(VK_NULL_HANDLE)
	{
	}

	// Create a texture sampler
	// In Vulkan textures are accessed by samplers
	// This separates all the sampling information from the texture data. This means you could have multiple sampler objects for the same texture with different settings
	// Note: Similar to the samplers available with OpenGL 3.3
	void TextureSampler::Create(VulkanDevice* device, VkFilter minFilter, VkFilter magFilter, VkSamplerAddressMode addressU, VkSamplerAddressMode addressV, VkSamplerAddressMode addressW,
		int32 maxLodLevel, int32 minLodLevel, float mipLodBias, VkSamplerMipmapMode mipmapMode,
		VkBorderColor borderColor, bool anisotropyEnable, bool compareEnable, VkCompareOp compareOp)
	{
		_ASSERT(m_Sampler == VK_NULL_HANDLE);

		VkSamplerCreateInfo createInfo;
		Memory::Memzero(createInfo);
		createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		createInfo.maxAnisotropy = 1.0f;
		createInfo.magFilter = magFilter;
		createInfo.minFilter = minFilter;
		createInfo.addressModeU = addressU;
		createInfo.addressModeV = addressV;
		createInfo.addressModeW = addressW;
		createInfo.mipmapMode = mipmapMode;
		createInfo.mipLodBias = mipLodBias;
		createInfo.minLod = (float)minLodLevel;
		// Set max level-of-detail to mip level count of the texture
		createInfo.maxLod = (float)maxLodLevel;

		// Enable anisotropic filtering
		// This feature is optional, so we must check if it's supported on the device
		if (device->GetFeatures().samplerAnisotropy)
		{
			// Use max. level of anisotropy for this example
			createInfo.maxAnisotropy = device->GetProperties().limits.maxSamplerAnisotropy;
			createInfo.anisotropyEnable = anisotropyEnable;
		}
		else
		{
			// The device does not support anisotropic filtering
			createInfo.maxAnisotropy = 1.0;
			createInfo.anisotropyEnable = VK_FALSE;
		}

		createInfo.compareEnable = compareEnable;
		createInfo.compareOp = compareOp;
		createInfo.borderColor = borderColor;

		VK_RESULT(vkCreateSampler(device->GetHandle(), &createInfo, nullptr, &m_Sampler));
	}

	void TextureSampler::Destroy(VulkanDevice* device)
	{
		if (m_Sampler != VK_NULL_HANDLE)
		{
			vkDestroySampler(device->GetHandle(), m_Sampler, nullptr);
			m_Sampler = VK_NULL_HANDLE;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	VkShaderStageFlagBits ShaderTypeToStage(ShaderType::TYPE type)
	{
		switch (type)
		{
		case ShaderType::VERTEXSHADER:		return VK_SHADER_STAGE_VERTEX_BIT;
		case ShaderType::FRAGMENTSHADER:	return VK_SHADER_STAGE_FRAGMENT_BIT;
		case ShaderType::COMPUTESHADER:		return VK_SHADER_STAGE_COMPUTE_BIT;
		case ShaderType::GEOMETRYSHADER:	return VK_SHADER_STAGE_GEOMETRY_BIT;
		default: _ASSERT(0); break;
		};

		return VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM;
	}

	VkWriteDescriptorSet WriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding, VkDescriptorBufferInfo* bufferInfo)
	{
		VkWriteDescriptorSet writeDescriptorSet;
		Memory::Memzero(writeDescriptorSet);
		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.descriptorType = type;
		writeDescriptorSet.dstSet = dstSet;
		writeDescriptorSet.dstBinding = binding;
		writeDescriptorSet.pBufferInfo = bufferInfo;
		return writeDescriptorSet;
	}

	VkWriteDescriptorSet WriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding, VkDescriptorImageInfo* imageInfo)
	{
		VkWriteDescriptorSet writeDescriptorSet;
		Memory::Memzero(writeDescriptorSet);
		writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writeDescriptorSet.descriptorCount = 1;
		writeDescriptorSet.descriptorType = type;
		writeDescriptorSet.dstSet = dstSet;
		writeDescriptorSet.dstBinding = binding;
		writeDescriptorSet.pImageInfo = imageInfo;
		return writeDescriptorSet;
	}

	const VkImageSubresourceRange& DefaultSubresourceRange()
	{
		static VkImageSubresourceRange SubresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
		return SubresourceRange;
	}

	///////////////////////////////////////////////////////////////////

	VkAttachmentLoadOp LoadAction(RTLoadAction loadAction)
	{
		VkAttachmentLoadOp loadOp = VK_ATTACHMENT_LOAD_OP_MAX_ENUM;

		switch (loadAction)
		{
		case RTLoadAction::ELoad:		loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;		break;
		case RTLoadAction::EClear:		loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;		break;
		case RTLoadAction::ENoAction:	loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;	break;
		default:																	break;
		}

		_ASSERT(loadOp != VK_ATTACHMENT_LOAD_OP_MAX_ENUM);
		return loadOp;
	}

	VkAttachmentStoreOp StoreAction(RTStoreAction storeAction)
	{
		VkAttachmentStoreOp storeOp = VK_ATTACHMENT_STORE_OP_MAX_ENUM;

		switch (storeAction)
		{
		case RTStoreAction::EStore:		storeOp = VK_ATTACHMENT_STORE_OP_STORE;		break;
		case RTStoreAction::ENoAction:	storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;	break;
		default:																	break;
		}

		_ASSERT(storeOp != VK_ATTACHMENT_STORE_OP_MAX_ENUM);
		return storeOp;
	}

	///////////////////////////////////////////////////////////////////

	static const VkImageTiling GVulkanViewTypeTilingMode[VK_IMAGE_VIEW_TYPE_RANGE_SIZE] =
	{
		VK_IMAGE_TILING_LINEAR,		// VK_IMAGE_VIEW_TYPE_1D
		VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_2D
		VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_3D
		VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_CUBE
		VK_IMAGE_TILING_LINEAR,		// VK_IMAGE_VIEW_TYPE_1D_ARRAY
		VK_IMAGE_TILING_OPTIMAL,	// VK_IMAGE_VIEW_TYPE_2D_ARRAY
		VK_IMAGE_TILING_LINEAR,		// VK_IMAGE_VIEW_TYPE_CUBE_ARRAY
	};

	VkImage CreateImage(VulkanDevice* device, VkImageViewType viewType, uint32 format, uint32 width, uint32 height, uint32 depth,
		bool isArray, uint32 arraySize, uint32 mipMaps, uint32 multiSamples, bool isTilingLinear, uint32 textureFlags,
		VkImageCreateInfo* OutCreateInfo, VkMemoryRequirements* OutMemoryRequirements)
	{
		const VkPhysicalDeviceProperties& deviceProperties = device->GetProperties();

		VkImageCreateInfo Temp_createInfo;
		VkImageCreateInfo& createInfo = OutCreateInfo ? *OutCreateInfo : Temp_createInfo;
		Memory::Memzero(createInfo);
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;

		switch (viewType)
		{
		case VK_IMAGE_VIEW_TYPE_1D:
		case VK_IMAGE_VIEW_TYPE_1D_ARRAY:
			createInfo.imageType = VK_IMAGE_TYPE_1D;
			createInfo.extent.width = width;
			createInfo.extent.height = height;
			createInfo.extent.depth = 1;
			_ASSERT(width <= deviceProperties.limits.maxImageDimension1D);
			break;

		case VK_IMAGE_VIEW_TYPE_2D:
		case VK_IMAGE_VIEW_TYPE_2D_ARRAY:
			createInfo.imageType = VK_IMAGE_TYPE_2D;
			createInfo.extent.width = width;
			createInfo.extent.height = height;
			createInfo.extent.depth = 1;
			_ASSERT(width <= deviceProperties.limits.maxImageDimension2D);
			_ASSERT(height <= deviceProperties.limits.maxImageDimension2D);
			break;

		case VK_IMAGE_VIEW_TYPE_3D:
			createInfo.imageType = VK_IMAGE_TYPE_3D;
			createInfo.extent.width = width;
			createInfo.extent.height = height;
			createInfo.extent.depth = depth;
			_ASSERT(width <= deviceProperties.limits.maxImageDimension3D);
			_ASSERT(height <= deviceProperties.limits.maxImageDimension3D);
			_ASSERT(depth <= deviceProperties.limits.maxImageDimension3D);
			break;

		case VK_IMAGE_VIEW_TYPE_CUBE:
		case VK_IMAGE_VIEW_TYPE_CUBE_ARRAY:
			createInfo.imageType = VK_IMAGE_TYPE_2D;
			createInfo.extent.width = width;
			createInfo.extent.height = height;
			createInfo.extent.depth = 1;
			_ASSERT(width <= deviceProperties.limits.maxImageDimensionCube);
			_ASSERT(height <= deviceProperties.limits.maxImageDimensionCube);
			_ASSERT(width == height);
			break;
		}

		uint32 layerCount = (viewType == VK_IMAGE_VIEW_TYPE_CUBE) ? 6 : 1;
		createInfo.arrayLayers = (isArray ? height : 1) * layerCount;
		createInfo.mipLevels = mipMaps;

		createInfo.format = device->GetFormat(format);
		_ASSERT(createInfo.format != VK_FORMAT_UNDEFINED);

		createInfo.tiling = isTilingLinear ? VK_IMAGE_TILING_LINEAR : GVulkanViewTypeTilingMode[viewType];

		createInfo.usage = 0;
		createInfo.usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		//createInfo.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		//createInfo.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;

		if (textureFlags & TEX_Present)
		{
			createInfo.usage |= VK_IMAGE_USAGE_STORAGE_BIT;
		}
		else if (textureFlags & TEX_RenderTarget)
		{
			createInfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		}
		else if (textureFlags & TEX_DepthStencilTarget)
		{
			createInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
			createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		}
		else if (textureFlags & TEX_ResolveRenderTarget)
		{
			createInfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		}

		createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;	// 오직 하나의 Queue Family가 한번에 하나의 Image를 사용할 때, "exclusive" sharing mode를 사용한다.
		//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;

		if (createInfo.tiling == VK_IMAGE_TILING_LINEAR)
		{
			multiSamples = 1;
			_ASSERT(0);
		}

		createInfo.samples = device->GetSampleCount(multiSamples);

		VkImage createdImage;
		VK_RESULT(vkCreateImage(device->GetHandle(), &createInfo, nullptr, &createdImage));

		VkMemoryRequirements TEMP_MemoryRequirements;
		VkMemoryRequirements& memoryRequirements = OutMemoryRequirements ? *OutMemoryRequirements : TEMP_MemoryRequirements;
		vkGetImageMemoryRequirements(device->GetHandle(), createdImage, &memoryRequirements);

		return createdImage;
	}

	VkBuffer CreateBuffer(VulkanDevice* device, uint32 bufferSize, uint32 usage, VkBufferCreateInfo* OutCreateInfo, VkMemoryRequirements* OutMemoryRequirements)
	{
		VkBufferCreateInfo Temp_createInfo;
		VkBufferCreateInfo& createInfo = OutCreateInfo ? *OutCreateInfo : Temp_createInfo;
		Memory::Memzero(createInfo);
		createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		createInfo.flags = 0;
		createInfo.pNext = nullptr;
		createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;	// 오직 하나의 Queue Family가 한번에 하나의 Image를 사용할 때, "exclusive" sharing mode를 사용한다.
		//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
		createInfo.size = bufferSize;
		//Usage
		createInfo.usage = usage;
		//createInfo.usage |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		//createInfo.usage |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

		VkBuffer createdBuffer;
		VK_RESULT(vkCreateBuffer(device->GetHandle(), &createInfo, nullptr, &createdBuffer));

		VkMemoryRequirements TEMP_MemoryRequirements;
		VkMemoryRequirements& memoryRequirements = OutMemoryRequirements ? *OutMemoryRequirements : TEMP_MemoryRequirements;
		vkGetBufferMemoryRequirements(device->GetHandle(), createdBuffer, &memoryRequirements);

		return createdBuffer;
	}
}


//
//
//
IShader* CreateShader()
{
	return GetRenderDriver()->CreateShader();
}

VertexBuffer* CreateVertexBuffer()
{
	return GetRenderDriver()->CreateVertexBuffer();
}

IndexBuffer* CreateIndexBuffer()
{
	return GetRenderDriver()->CreateIndexBuffer();
}

ITexture2D* CreateFileTexture()
{
	return NULL;
}

IRenderTexture2D* CreateRenderTexture()
{
	return NULL;
}

IRenderTarget* CreateRenderTarget()
{
	return NULL;
}

IRenderTexture2D* CreateRenderTargetSurface()
{
	return NULL;
}

IDepthStencilSurface* CreateDepthStencilSurface()
{
	return NULL;
}
