#include "renderer.h"

VulkanRenderPass::VulkanRenderPass(VulkanDevice* device, const Vulkan::RenderTargetDesc& RTDesc)
	: m_Device(device)
{
	/*	RenderPass에서 사용될 모든 Attachment 정보와 구성하고 있는 SubPass 정보를 모두 기술해야 한다.
		
		1. Attachment Description 목록을 작성한다.
		: 각 attachment가 렌더패스 내에서 어떻게 사용될 것인지 각 Attachment를 설명한다.

		2. Subpass에 설정할 모든 Attachment Reference 목록을 만들어야 한다.
		: AttachmentDescription 배열 인덱스로 어떤 Attachment를 사용할지 정하고, 사용할 Layout을 정한다.

		3. SubPass Description 목록을 만든다.
		: SubPass가 사용할 Attachment Reference 를 설정한다.
		: SubPass에 Multiple Attachment (Color, Depth/Stencil Attachment)를 설정할수도 있고, SubPass-ColorAttachment, SubPass-DepthAttachment 형태로 SubPass를 여러 개로 나눌수도 있다.

		4. RenderPass 생성
		: RenderPass가 사용할 모든 Attachment와 SubPass에 대해서 기술한 내용을 명시한다.
	*/
	const Vulkan::RenderTargetLayout& RTLayout = RTDesc.layout;

	// subpass 설명: attachment 정보 설정
	VkSubpassDescription subPassDesc;
	subPassDesc.flags = 0;
	subPassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subPassDesc.colorAttachmentCount = RTLayout.GetNumColorAttachments();
	subPassDesc.pColorAttachments = RTLayout.GetColorAttachments();
	subPassDesc.pDepthStencilAttachment = RTLayout.GetDepthStencilAttachment();
	subPassDesc.pResolveAttachments = RTLayout.GetResolveAttachments();
	subPassDesc.inputAttachmentCount = 0;
	subPassDesc.pInputAttachments = nullptr;
	subPassDesc.preserveAttachmentCount = 0;
	subPassDesc.pPreserveAttachments = nullptr;

	// rendepass 생성 구조체
	VkRenderPassCreateInfo createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	createInfo.flags = 0;
	createInfo.pNext = nullptr;
	// subpass 목록
	createInfo.subpassCount = 1;
	createInfo.pSubpasses = &subPassDesc;
	// 사용될 모든 attachment 목록
	createInfo.attachmentCount = RTLayout.GetNumAttachments();
	createInfo.pAttachments = RTLayout.GetAttachmentDesc();
	// dependency
	createInfo.dependencyCount = 0;
	createInfo.pDependencies = nullptr;

	/*

	// Setup subpass dependencies
	// These will add the implicit ttachment layout transitionss specified by the attachment descriptions
	// The actual usage layout is preserved through the layout specified in the attachment reference
	// Each subpass dependency will introduce a memory and execution dependency between the source and dest subpass described by
	// srcStageMask, dstStageMask, srcAccessMask, dstAccessMask (and dependencyFlags is set)
	// Note: VK_SUBPASS_EXTERNAL is a special constant that refers to all commands executed outside of the actual renderpass)
	std::array<VkSubpassDependency, 2> dependencies;

	// First dependency at the start of the renderpass
	// Does the transition from final to initial layout
	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;								// Producer of the dependency
	dependencies[0].dstSubpass = 0;													// Consumer is our single subpass that will wait for the execution depdendency
	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	// Second dependency at the end the renderpass
	// Does the transition from the initial to the final layout
	dependencies[1].srcSubpass = 0;													// Producer of the dependency is our single subpass
	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;								// Consumer are all commands outside of the renderpass
	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	*/
	VK_RESULT(vkCreateRenderPass(m_Device->GetHandle(), &createInfo, nullptr, &m_RenderPass));

	m_RenderTargetDesc = RTDesc;
}

VulkanRenderPass::~VulkanRenderPass()
{
	vkDestroyRenderPass(m_Device->GetHandle(), m_RenderPass, nullptr);
}

int32 VulkanRenderPass::GetColorAttachmentCount() const
{
	return GetRenderTargetDesc().layout.GetNumColorAttachments();
}
