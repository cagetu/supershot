#include "renderer.h"
//#include <lodepng.h>
#include <tga.h>
#include <png.h>

/////////////////////////////////////////////////////////////////////////////

IVulkanTexture::IVulkanTexture(VulkanDevice* device)
	: m_Device(device)
	, m_ImageHandle(VK_NULL_HANDLE)
{
}
IVulkanTexture::~IVulkanTexture()
{
}

/////////////////////////////////////////////////////////////////////////////
/*
 *
 */
__ImplementRtti(VisualShock, VulkanTexture2D, ITexture2D);

VulkanTexture2D::VulkanTexture2D(VulkanDevice* device)
	: IVulkanTexture(device)
{
}
VulkanTexture2D::~VulkanTexture2D()
{
}

bool VulkanTexture2D::Load(const TCHAR* fname)
{
	char filename[128];
	string::conv_s(filename, 128, fname);

	String fext = string::splitFileExt(fname);
	if (fext == TEXT(".png"))
	{
		FILE* fp = unicode::fopen(fname, L"wb");
		png_structp pPng = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if (!pPng)
		{
			unicode::fclose(fp);
			return false;
		}
		png_infop pPngInfo = png_create_info_struct(pPng);
		if (pPngInfo)
		{
			unicode::fclose(fp);
			return false;
		}

		if (setjmp(png_jmpbuf(pPng)))
		{
			unicode::fclose(fp);
			return false;
		}

		png_init_io(pPng, fp);
		png_read_info(pPng, pPngInfo);

		uint32 width = png_get_image_width(pPng, pPngInfo);
		uint32 height = png_get_image_height(pPng, pPngInfo);
		uint8 colorType = png_get_color_type(pPng, pPngInfo);
		uint8 depthBit = png_get_bit_depth(pPng, pPngInfo);

		if (depthBit == 16)
			png_set_strip_16(pPng);

		if (colorType == PNG_COLOR_MASK_PALETTE)
			png_set_palette_to_rgb(pPng);

		// PNG_COLOR_TYPE_GRAY is always 8 or 16 bit depth
		if (colorType == PNG_COLOR_TYPE_GRAY && depthBit < 8)
			png_set_expand_gray_1_2_4_to_8(pPng);

		if (png_get_valid(pPng, pPngInfo, PNG_INFO_tRNS))
			png_set_tRNS_to_alpha(pPng);

		// These color_type don't have an alpha channel then fill it with 0xff.
		if (colorType == PNG_COLOR_TYPE_RGB || colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_PALETTE)
			png_set_filler(pPng, 0xFF, PNG_FILLER_AFTER);

		if (colorType == PNG_COLOR_TYPE_GRAY ||	colorType == PNG_COLOR_TYPE_GRAY_ALPHA)
			png_set_gray_to_rgb(pPng);

		png_read_update_info(pPng, pPngInfo);

		png_bytep* row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
		for (int y = 0; y < height; y++) {
			row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(pPng, pPngInfo));
		}

		png_read_image(pPng, row_pointers);

		unicode::fclose(fp);

		//std::vector<unsigned char> image;
		//unsigned width, height;
		//unsigned error = lodepng::decode(image, width, height, filename, LCT_RGBA);
		//if (error)
		//{
		//	const char* log = lodepng_error_text(error);

		//	return false;
		//}

		m_TextureView.Create(m_Device, m_ImageHandle, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_ASPECT_COLOR_BIT, RT_R8G8B8A8);
		m_TextureSampler.Create(m_Device, VK_FILTER_LINEAR, VK_FILTER_LINEAR, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE, 0, 0);
	}
	else if (fext == TEXT(".tga"))
	{
		TGA* tga = TGAOpen(filename, "r");
		if (!tga || tga->last != TGA_OK)
		{
			return false;
		}

		//tga->hdr

		TGAData data;

		/* the TGA_IMAGE_ID tells the TGAReadImage() to read the image
		id, the TGA_IMAGE_DATA tells it to read the whole image data
		and any color map data if existing. NOTE: the image header is
		read always no matter what options were specified.
		At last we pass over the TGA_RGB flag so the returned data is
		in RGB format and not BGR */
		data.flags = TGA_IMAGE_DATA | TGA_IMAGE_ID | TGA_RGB;
		if (TGAReadImage(tga, &data) != TGA_OK) {
			/* error handling goes here */
			return false;
		}

		TGAClose(tga);
	}
	else if (fext == TEXT(".pvr"))
	{
		return false;
	}

	return false;
}

void VulkanTexture2D::SetTexture()
{
}

void* VulkanTexture2D::Lock(int32 length)
{
	m_StagingBuffer = m_Device->StagingBufferPool().Allocate(length, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
	return m_StagingBuffer->Lock();
}

void VulkanTexture2D::Unlock()
{
	m_StagingBuffer->Unlock();

	VulkanCmdBuffer* UpdateCmdBuffer = m_Device->CommandBufferPool().GetUploadCommandBuffer();

	//// ImageLayout 처리 필요!!!!

	//// CopyBufferToImage

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.layerCount = 1;
	region.imageExtent.width = m_Width;
	region.imageExtent.height = m_Height;
	region.imageExtent.depth = 1;

	UpdateCmdBuffer->CopyBufferToImage(m_StagingBuffer->GetHandle(), m_TextureView.GetHandle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

	//// ImageLayout 처리 필요!!!!

	m_Device->StagingBufferPool().Release(UpdateCmdBuffer, m_StagingBuffer);
	m_StagingBuffer = nullptr;
}

/////////////////////////////////////////////////////////////////////////////
/*
*
*/
__ImplementRtti(VisualShock, VulkanRenderTexture2D, IRenderTexture2D);

VulkanRenderTexture2D::VulkanRenderTexture2D(VulkanDevice* device)
	: IVulkanTexture(device)
{
}
VulkanRenderTexture2D::~VulkanRenderTexture2D()
{
}

bool VulkanRenderTexture2D::Create(int width, int height, int format)
{
	m_Width = width;
	m_Height = height;
	m_PixelFormat = format;

	m_TextureView.Create(m_Device, m_ImageHandle, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_ASPECT_COLOR_BIT, (RT_TYPE)format);
	return true;
}

void VulkanRenderTexture2D::Destroy()
{
	m_TextureView.Destroy(m_Device);
}

void VulkanRenderTexture2D::SetTexture()
{
}

void VulkanRenderTexture2D::SetSurface(int channel)
{
	// Surface Binding!!!!
}

/////////////////////////////////////////////////////////////////////////////
/*
*
*/
__ImplementRtti(VisualShock, VulkanWritableTexture, IWritableTexture);

VulkanWritableTexture::VulkanWritableTexture(VulkanDevice* device)
	: IVulkanTexture(device)
{
}
VulkanWritableTexture::~VulkanWritableTexture()
{
}

bool VulkanWritableTexture::Create(int width, int height, int format)
{
	m_Width = width;
	m_Height = height;
	m_PixelFormat = format;

	return false;
}

void* VulkanWritableTexture::Lock(int *pitch, int lv, int flags)
{
	return NULL;
}

void VulkanWritableTexture::Unlock(bool force, int* rect)
{

}

