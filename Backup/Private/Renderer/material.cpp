#include "renderer.h"

CMaterial::CMaterial()
{
	static int autoIncrement = 0;
	m_UID = UID_MATERIAL | ((autoIncrement++) & 0xFFFFF);

#ifndef PASS_SHADER
	m_ShaderMask = 0;
#endif
	m_RenderState = 0;
}

CMaterial::~CMaterial()
{
	for (auto it : m_Passes)
		delete it;
	m_Passes.clear();
}

#ifndef PASS_SHADER
void CMaterial::SetShaderCode(const SHADERCODE& shaderCode)
{
	m_ShaderCode = shaderCode;
}

void CMaterial::SetShaderMask(uint32 shaderMask)
{
	m_ShaderMask = shaderMask;
}
#endif

void CMaterial::SetRenderState(uint32 renderstate)
{
	m_RenderState = renderstate;
}

void CMaterial::SetParameter(const TCHAR* sementic, const Variant& value)
{
	int32 paramId = FindPID(sementic);
	if (paramId < 0)
	{
		ShaderParameter::PID pid = ShaderParameter::Register(sementic);
		m_Types.insert(std::make_pair(sementic, pid));

		InsertParameter(pid, value);
		return;
	}

	auto it = m_Values.find(paramId);
	_ASSERT(it != m_Values.end());

	it->second = value;
}

bool CMaterial::GetParameter(int paramId, Variant& output) const
{
	auto it = m_Values.find(paramId);
	if (it == m_Values.end())
		return false;

	output = it->second;
	return true;
}

bool CMaterial::GetParameter(const TCHAR* sementic, Variant& output) const
{
	int pid = FindPID(sementic);
	if (pid < 0)
		return false;

	return GetParameter(pid, output);
}

void CMaterial::InsertParameter(int pid, const Variant& value)
{
	m_Values.insert(std::make_pair(pid, value));
}

void CMaterial::InsertParameter(const TCHAR* sementic, const Variant& value)
{
	ShaderParameter::PID pid = ShaderParameter::Register(sementic);
	m_Types.insert(std::make_pair(sementic, pid));
	m_Values.insert(std::make_pair(pid, value));
}

int32 CMaterial::FindPID(const TCHAR* sementic) const
{
	auto it = m_Types.find(sementic);
	if (it == m_Types.end())
		return -1;

	return it->second;
}

CMaterial::_PASS* CMaterial::AddPass(const Ptr<IShader>& shader, uint32 renderstate)
{
	_PASS * pass = new _PASS;
	pass->shader = shader;
	pass->renderstate = renderstate;

	m_Passes.push_back(pass);
	return pass;
}

CMaterial::_PASS* CMaterial::GetPass(uint passIndex) const
{
	_ASSERT(passIndex < m_Passes.size());
	return m_Passes[passIndex];
}

void CMaterial::SetRenderState(uint passIndex, uint32 renderstate)
{
	_PASS * pass = GetPass(passIndex);
	_ASSERT(pass);

	pass->SetRenderState(renderstate);
}

#ifdef PASS_VALUES
void CMaterial::SetPassParameter(uint passIndex, const TCHAR* sementic, const Variant& value)
{
	_PASS * pass = GetPass(passIndex);

	pass->parameters[sementic] = value;
}
#endif

CMaterial::_PASS* CMaterial::BeginPass(uint passIndex)
{
	_PASS * pass = GetPass(passIndex);

#ifdef PASS_VALUES
	for (auto it : pass->values)
	{
		SetParameter(it.first, it.second);
	}
#endif

	return pass;
}

void CMaterial::EndPass()
{
}

