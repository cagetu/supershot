#include "renderer.h"

std::map <String, ShaderCode*> ShaderCode::m_ShaderCodes;

bool ShaderCode::Initialize()
{
	m_ShaderCodes.clear();
	return true;
}

void ShaderCode::Shutdown()
{
	auto itend = m_ShaderCodes.end();
	for (auto it = m_ShaderCodes.begin(); it != itend; it++)
		delete it->second;
	m_ShaderCodes.clear();
}

ShaderCode* ShaderCode::Load(const TCHAR* filename, ShaderType::TYPE shaderType)
{
	auto it = m_ShaderCodes.find(filename);
	if (it != m_ShaderCodes.end())
	{
		_ASSERT(it->second->GetShaderType() == shaderType);
		return it->second;
	}

	CStdIO* fileIO = CStdIO::Open(filename);
	if (fileIO == NULL)
		return NULL;

	uint codeSize = fileIO->GetSize();
	char* codeBytes = new char[codeSize];
	fileIO->Read(codeBytes, codeSize);

	ShaderCode* code = new ShaderCode();

	code->m_ID = filename;
	code->m_ShaderCode = std::string(codeBytes, codeSize);
	code->m_ShaderType = shaderType;

	delete fileIO;
	delete[] codeBytes;

	m_ShaderCodes.insert(std::make_pair(filename, code));
	return code;
}

ShaderCode* ShaderCode::Load(const TCHAR* id, ShaderType::TYPE shaderType, const char* codeBytes, int codeSize)
{
	auto it = m_ShaderCodes.find(id);
	if (it != m_ShaderCodes.end())
	{
		_ASSERT(it->second->GetShaderType() == shaderType);
		return it->second;
	}

	ShaderCode* code = new ShaderCode();

	code->m_ID = id;
	code->m_ShaderCode = std::string(codeBytes, codeSize);
	code->m_ShaderType = shaderType;

	m_ShaderCodes.insert(std::make_pair(id, code));
	return code;
}

ShaderCode* ShaderCode::Find(const TCHAR* id)
{
	auto it = m_ShaderCodes.find(id);
	if (it != m_ShaderCodes.end())
		return it->second;

	return NULL;
}
