#include "renderer.h"

__ImplementRtti(VisualShock, VertexBuffer, IRenderResource);

VertexBuffer::VertexBuffer()
	: m_Length(0), m_Stride(0), m_Offset(0)
	, m_Flags(0), m_BufferUsage(0)
	, m_FVF(0), m_ValidCount(0)
	, m_VtxBuf(NULL)
{
}

VertexBuffer::~VertexBuffer()
{
}

void VertexBuffer::Destroy()
{
	if (m_VtxBuf != NULL)
	{
		delete[] m_VtxBuf;
		m_VtxBuf = NULL;
	}
}

int32 VertexBuffer::SetFVF(uint32 fvf)
{
	int32 stride = VertexDescription::GetStride(fvf);

	m_Stride = stride;
	m_FVF = fvf;

	return stride;
}

//////////////////////////////////////////


inline void _memcpy(unsigned char* dest, int32 pitch, const void* srcptr, int32 size, int32 count)
{
	char *src = (char*)srcptr;
	int32 i = 0;

	for (; i + 4<count; i += 4, dest += 4 * pitch, src += 4 * size)
	{
		memcpy(dest + 0 * pitch, src + 0 * size, size);
		memcpy(dest + 1 * pitch, src + 1 * size, size);
		memcpy(dest + 2 * pitch, src + 2 * size, size);
		memcpy(dest + 3 * pitch, src + 3 * size, size);
	}
	for (; i<count; i++, dest += pitch, src += size)
		memcpy(dest, src, size);
}

void VertexBuffer::Fill(const Vector3 * v, const Vector3 * norm, const uint32 *diffuse,
						const float(*uv)[2], const float(*uv2)[2], const Vector4 * tan,
						const unsigned char(*skinindex)[4], const unsigned char(*skinweight)[4],
						const float* shcoeff, const uint32* vtxao)
{
	unsigned char* ptr = (unsigned char*)Lock();
	VertexBuffer::FillData(ptr, m_Stride, m_FVF, 0, m_Length / m_Stride, v, norm, diffuse, uv, uv2, tan, skinindex, skinweight, shcoeff, vtxao);
	Unlock();
}

void VertexBuffer::FillData(OUT void* dest, int32 stride, uint32 fvf, int32 offset, int32 vtxnum,
							const Vector3 * v, const Vector3 * norm, const uint32 *diffuse,
							const float(*uv)[2], const float(*uv2)[2],
							const Vector4 * tan,
							const unsigned char(*skinindex)[4], const unsigned char(*skinweight)[4],
							const float* shcoeff, const uint32* vtxao)
{
	static uint32 _fvf = 0;
	static int32 off[8];

	if (_fvf != fvf)
	{
		_fvf = fvf;

		VertexDescription::GetOffsets(fvf, off);
	}

	unsigned char* ptr = (unsigned char*)dest + offset * stride;

	for (int32 i = 0; i<vtxnum;)
	{
		int32 blockcount = vtxnum - i < 16 ? vtxnum - i : 16;

		if (fvf&FVF_XYZ)
			_memcpy(ptr, stride, &v[i], sizeof(float)* 3, blockcount);

		if (fvf&FVF_XYZRHW)
		{
			_memcpy(ptr, stride, &v[i], sizeof(float)* 3, blockcount);
			for (int32 j = 0; j<blockcount; j++)
				*(float*)(ptr + stride*j + 3 * sizeof(float)) = 1.0f;
		}

		if (fvf&FVF_NORMAL)
		{
			if (fvf&FVF_NORMAL_PACKED)
			{
				unsigned char n[16][4];
				for (int32 j = 0; j<blockcount; j++)
					VertexCompress::NormalToUBYTE4N(n[j], norm[i + j]);
				_memcpy(ptr + off[1], stride, n, sizeof(unsigned char)* 4, blockcount);
			}
			else
			{
				_memcpy(ptr + off[1], stride, &norm[i], sizeof(float)* 3, blockcount);
			}
		}

		if (fvf&FVF_SKIN_INDEX)
		{
			_memcpy(ptr + off[2], stride, &skinindex[i][0], sizeof(unsigned char)* 4, blockcount);
		}
		if (fvf&FVF_DIFFUSE)
		{
			_memcpy(ptr + off[2], stride, &diffuse[i], sizeof(uint32), blockcount);
		}

		if (fvf&FVF_SKIN_WEIGHT)
		{
			_memcpy(ptr + off[3] + sizeof(unsigned char) * 4, stride, &skinweight[i][0], sizeof(unsigned char) * 4, blockcount);
		}

		if (fvf&FVF_TEX0)
		{
			if (fvf&FVF_TEX0_PACKED)
			{
				short t[16][2];
				for (int32 j = 0; j<blockcount; j++) {
					VertexCompress::TexcoordToShort2(t[j], uv[i + j]);
				}
				_memcpy(ptr + off[4], stride, t, sizeof(short)* 2, blockcount);
			}
			else
			{
				_memcpy(ptr + off[4], stride, &uv[i], sizeof(float)* 2, blockcount);
			}
		}

		if (fvf&FVF_TEX1)
		{
			if (fvf&FVF_TEX1_PACKED)
			{
				short t[16][2];
				for (int32 j = 0; j<blockcount; j++) {
					VertexCompress::TexcoordToShort2(t[j], uv2[i + j]);
				}
				_memcpy(ptr + off[5], stride, t, sizeof(short)* 2, blockcount);
			}
			else
			{
				_memcpy(ptr + off[5], stride, &uv2[i], sizeof(float)* 2, blockcount);
			}
		}

		if (fvf&FVF_TANSPC)
		{
			if (fvf&FVF_TANSPC_PACKED)
			{
				unsigned char t[16][4];
				for (int32 j = 0; j<blockcount; j++) {
					VertexCompress::TangentToUBYTE4N(t[j], tan[i + j]);
				}
				_memcpy(ptr + off[6], stride, t, sizeof(unsigned char)* 4, blockcount);
			}
			else
			{
				_memcpy(ptr + off[6], stride, &tan[i], sizeof(float)* 4, blockcount);
			}
		}

		if (fvf&FVF_VERTEXAO)
		{
			_memcpy(ptr + off[7], stride, &vtxao[i], sizeof(uint32), blockcount);
		}

		if (fvf&FVF_SHCOEFF)
		{
#if 1
			float shcoeff_opt[16][9];
			for (int32 j = 0; j<blockcount; j++)
			{
				shcoeff_opt[j][0] = shcoeff[(i + j) * 9 + 3];
				shcoeff_opt[j][1] = shcoeff[(i + j) * 9 + 1];
				shcoeff_opt[j][2] = shcoeff[(i + j) * 9 + 2];

				shcoeff_opt[j][3] = shcoeff[(i + j) * 9 + 4];
				shcoeff_opt[j][4] = shcoeff[(i + j) * 9 + 5];
				shcoeff_opt[j][5] = shcoeff[(i + j) * 9 + 7];

				shcoeff_opt[j][6] = shcoeff[(i + j) * 9 + 8];
				shcoeff_opt[j][7] = shcoeff[(i + j) * 9 + 6];
				shcoeff_opt[j][8] = shcoeff[(i + j) * 9 + 0] - shcoeff_opt[j][7];
			}

			_memcpy(ptr + off[7], stride, shcoeff_opt, sizeof(float)* 3 * 3, blockcount);
#else
			_memcpy(ptr + off[7], stride, &shcoeff[i * 9], sizeof(float)* 3 * 3, blockcount);
#endif
		}

		ptr += blockcount*stride;
		i += blockcount;
	}
}

