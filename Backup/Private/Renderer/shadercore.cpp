#include "renderer.h"

std::vector <ShaderCore::SHADERKEY> ShaderCore::m_ShaderList;
std::map <String, IShaderResource*> ShaderCore::m_IShaderResources;

void ShaderCore::Initialize()
{
	ShaderParameter::Initialize();
	ShaderFeature::Initialize();
	ShaderCompiler::Initialize();
}

void ShaderCore::Shutdown()
{
	{
		auto itend = m_ShaderList.end();
		for (auto it = m_ShaderList.begin(); it != itend; it++)
			(*it).instance->DecRef();
		m_ShaderList.clear();
	}
	{
		auto itend = m_IShaderResources.end();
		for (auto it = m_IShaderResources.begin(); it != itend; it++)
			delete it->second;
		m_IShaderResources.clear();
	}

	ShaderCompiler::Shutdown();
	ShaderParameter::Shutdown();
	ShaderFeature::Shutdown();
}

/*	단위 셰이더를 빌드한다.
	- 셰이더 id (파일 이름) + 마스크 이름을 Key로 관리한다.
	- 생성된 최적화된 셰이더 코드가 있다면 사용해서 빌드를 한다.
	- 없다면, 새롭게 빌드를 진행한다.
		- 셰이더 코드를 읽어온다. (셰이더 코드는 리소스로 관리된다.)
		- 최적화 코드를 생성한다.
		- 생성한 최적화 코드는 캐쉬로 저장한다.
*/
IShaderResource* ShaderCore::Build(const TCHAR* filename, ShaderType::TYPE shaderType, ShaderFeature::MASK shaderMask)
{
	String maskName = ShaderFeature::GetMaskName(shaderMask);
	
	String shaderId(filename);
	shaderId += TEXT("@");
	shaderId += maskName;

	// 1. 빌드된 셰이더를 찾는다.
	auto it = m_IShaderResources.find(shaderId);
	if (it != m_IShaderResources.end())
		return it->second;

	// 2. 셰이더 컴파일
	ICompiledShaderCode* compiled = ShaderCompiler::Find(shaderId);
	if (!compiled)
	{
		ShaderCode* shaderCode = ShaderCode::Load(filename, shaderType);
		_ASSERT(shaderCode != NULL);

		compiled = CompileShader(shaderId, shaderCode->GetShaderCode(), shaderCode->GetShaderType(), shaderMask);
		_ASSERT(compiled);
	}

	// 3. 셰이더 컴파일
	IShaderResource* IShaderResource = CreateShaderResource(compiled, shaderType);
	m_IShaderResources.insert(std::make_pair(shaderId, IShaderResource));

	return IShaderResource;
}

Ptr<IShader> ShaderCore::Build(const SHADERCODE& shaderCodeID, ShaderFeature::MASK shaderMask)
{
	IShader* instance = NULL;
	
	instance = Find(shaderCodeID, shaderMask);
	if (instance != NULL)
		return instance;

	instance = CreateShader();
	if (instance->Create(shaderCodeID, shaderMask) == false)
		return NULL;

	SHADERKEY shaderkey;
	shaderkey.code = shaderCodeID;
	shaderkey.mask = shaderMask;
	shaderkey.instance = instance;
	shaderkey.instance->IncRef();

	m_ShaderList.push_back(shaderkey);

	return instance;
}

Ptr<IShader> ShaderCore::Find(const SHADERCODE& shaderCodeID, ShaderFeature::MASK shaderMask)
{
	std::vector <SHADERKEY>::const_iterator it, itend;
	itend = m_ShaderList.end();

	for (it = m_ShaderList.begin(); it != itend; it++)
	{
		if ((*it).code == shaderCodeID && (*it).mask == shaderMask)
			return (*it).instance;
	}
	return NULL;
}

void ShaderCore::Release(IShader* shader)
{
}

void ShaderCore::Begin(IShader* shader, const CMaterial* material, const VertexBuffer* vb)
{
	// 여기서 그냥 관리하는 것이 어떨까???
	shader->Begin(material, vb);
}

void ShaderCore::End(IShader* shader)
{
	shader->End();
}