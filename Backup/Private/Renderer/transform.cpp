#include "renderer.h"


Mat4 Transform::RotationX(float rad)
{
	return DeviceTransform::RotationX(rad);
}

Mat4 Transform::RotationY(float rad)
{
	return DeviceTransform::RotationY(rad);
}

Mat4 Transform::RotationZ(float rad)
{
	return DeviceTransform::RotationZ(rad);
}

Mat4 Transform::Rotation(const Quat4& q)
{
	return DeviceTransform::Rotation(q);
}

Mat4 Transform::Multiply(const Mat4 & a, const Mat4 & b)
{
	return DeviceTransform::Multiply(a, b);
}

Vector4 Transform::Multiply(const Mat4 & m, const Vector4 & p)
{
	return DeviceTransform::Transform(m, p);
}

Mat4 Transform::Projection(float nearwidth, float nearheight, float nearclip, float farclip)
{
	return DeviceTransform::Projection(nearwidth, nearheight, nearclip, farclip);
}

Mat4 Transform::ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio)
{
	return DeviceTransform::ProjectionWithFov(fov, nearclip, farclip, aspectratio);
}

Mat4 Transform::ProjectionOffCenter(float left, float right, float bottom, float top, float zn, float zf)
{
	return DeviceTransform::ProjectionOffCenter(left, right, bottom, top, zn, zf);
}

Mat4 Transform::InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon)
{
	return DeviceTransform::InfiniteProjectionWithFov(fov, nearclip, aspectratio, epsilon);
}

Mat4 Transform::OrthoOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	return DeviceTransform::OrthoOffCenter(l, r, b, t, zn, zf);
}

Mat4 Transform::Ortho(float w, float h, float zn, float zf)
{
	return DeviceTransform::Ortho(w, h, zn, zf);
}

Mat4 Transform::LookAt(const Vector3 & pivot, const Vector3 & target, const Vector3 & upvec)
{
	return DeviceTransform::LookAt(pivot, target, upvec);
}

Mat4 Transform::LookAt(const Vector3 & pivot, const Vector3 & target)
{
	return DeviceTransform::LookAt(pivot, target);
}

Mat4 Transform::LookAtZAxis(const Vector3& pivot, const Vector3& zaxis)
{
	return DeviceTransform::LookAtZAxis(pivot, zaxis);
}

Mat4 Transform::Scaling(const Vector3& s)
{
	return DeviceTransform::Scaling(s);
}

Mat4 Transform::Scaling(float s)
{
	return DeviceTransform::Scaling(s);
}

Mat4 Transform::Translate(const Vector3 & p)
{
	return DeviceTransform::Translate(p);
}

Mat4 Transform::Translate(float x, float y, float z)
{
	return DeviceTransform::Translate(x, y, z);
}

void Transform::Decompose(const Mat4& input, Vector3& translate, Quat4& rotate, Vector3& scale)
{
	return DeviceTransform::Decompose(input, translate, rotate, scale);
}