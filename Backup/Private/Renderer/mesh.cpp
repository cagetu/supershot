#include "renderer.h"

__ImplementRootRtti(CMesh);

//
CMesh::CMesh(const Ptr<CPrimitive>& primitive)
	: m_DirtyWorldBound(false)
	, m_WorldTM(Mat4::IDENTITY)
	, m_Flags(0)
{
	m_Primitive = primitive;
}

CMesh::~CMesh()
{
}

void CMesh::SetWorldTM(const Mat4& tm)
{
	m_WorldTM = tm;
	m_DirtyWorldBound = true;
}

#ifndef PASS_SHADER
void CMesh::SetShader(IShader* shader)
{
	m_Shader = shader;
}
#endif

void CMesh::SetMaterial(CMaterial* material)
{
	m_Material = material;
}

const AABB& CMesh::GetLocalBound() const
{
	return m_Primitive->GetLocalBound();
}

const AABB& CMesh::GetWorldBound() const
{
	if (m_DirtyWorldBound = true)
	{
		const AABB& local = GetLocalBound();

		m_WorldAABB = local.Transform(m_WorldTM);
		m_DirtyWorldBound = false;
	}
	return m_WorldAABB;
}

void CMesh::Update(float dt)
{
}

void CMesh::Render()
{
	_SHADERPARAMETER(ShaderParameter::WORLD, m_WorldTM);

	CMaterial * material = m_Material;

#ifndef PASS_SHADER
	Shader * shader = m_Shader;

	uint32 renderstate = m_Material->GetRenderState();

	m_Primitive->Render(shader, material, renderstate);
#else
	uint32 renderstate = m_Material->GetRenderState();

	const uint passCount = m_Material->NumPasses();

	for (uint index = 0; index < passCount; index++)
	{
		CMaterial::_PASS* pass = material->BeginPass(index);
		if (pass != NULL)
		{
			uint32 rs = renderstate | pass->GetRenderState();

			m_Primitive->Render(pass->GetShader(), material, rs);
		}
		material->EndPass();
	}

#endif
}

//
//
//
__ImplementRtti(VisualShock, CModelMesh, CMesh);

CModelMesh::CModelMesh(const Ptr<CPrimitive>& primitive)
	: CMesh(primitive)
	, m_BoneTable(NULL)
	, m_BoneCount(0)
	, m_BoneID(-1)
{
}

CModelMesh::~CModelMesh()
{
	m_BoneController.Null();
}

void CModelMesh::Update(float dt)
{
}

void CModelMesh::Render()
{
	if (m_BoneController != NULL)
	{
		if (m_BoneTable != NULL)
		{
			if (_MAX_BONE < m_BoneCount)
			{
				PlatformUtil::DebugOutFormat(TEXT("[CModelMesh::Render] _MAX_BONE <= m_BoneCount(%d)\n"), m_BoneCount);
				return;
			}
			//_SHADERPARAMETER(ShaderParameter::WORLD, m_WorldTM);

			static Mat4 tmarray[_MAX_BONE];
			for (int i = 0; i < m_BoneCount; i++)
			{
				m_BoneController->GetTM(m_BoneTable[i], tmarray[i]);
			}
			_SHADERPARAMETER(ShaderParameter::TMARRAY, Variant(tmarray, m_BoneCount));

			/*
			if ((m_Flags&_SKINNED_CPU) && m_Primitive->IsEnableSkinSW())
			{
				unsigned long mask = GetShaderMask();
				mask &= ~CShader::_SKINNING;

				m_Primitive->RenderSkinnedSW(CShader::MixShaderMask(shadermask, mask), rs, m_Material, tmarray);
			}
			else
			*/
			{
				CMesh::Render();
			}
		}
		else
		{
			_ASSERT(m_BoneID != -1);

			Mat4 tm;
			m_BoneController->GetTM(m_BoneID, tm);
			//_SHADERPARAMETER(ShaderParameter::WORLD, m_WorldTM);

			//m_Primitive->Render(CShader::MixShaderMask(shadermask, GetShaderMask()), rs, m_Material);

			CMesh::Render();
		}
	}
	else
	{
		//_SHADERPARAMETER(ShaderParameter::WORLD, m_WorldTM);

		CMesh::Render();

		//device.SetWorldTM(m_WorldTM);
		//device.SetLastWorldTM(m_LastWorldTM);
		//m_Primitive->Render(CShader::MixShaderMask(shadermask, GetShaderMask()), rs, m_Material);
	}
}


void CModelMesh::SetBoneController(const Ptr<CBoneController>& controller, int * boneTable, int boneNum)
{
	_ASSERT(boneNum < 128);

	m_BoneController = controller;
	m_BoneTable = boneTable;
	m_BoneCount = boneNum;

	if (m_BoneController != NULL && m_BoneTable != NULL)
	{
		m_Flags |= _SKINNED;
	}
}

void CModelMesh::SetBoneController(const Ptr<CBoneController>& controller, int boneindex)
{
	m_BoneController = controller;
	m_BoneID = boneindex;
}

//#define OBJECTMOTIONBLUR
int CModelMesh::GetTransforms(Mat4* tmlist, bool includelastTM)
{
	int count = 0;
	if (m_BoneController != NULL)
	{
		if (m_BoneTable != NULL)
		{
			for (int i = 0; i<m_BoneCount; i++)
				m_BoneController->GetTM(m_BoneTable[i], tmlist[i]);
			count += m_BoneCount;

#ifdef OBJECTMOTIONBLUR
			if (includelastTM)
			{
				memcpy(&tmlist[count], m_LastBoneTMArray, sizeof(Mat4)*m_BoneCount);
				count += m_BoneCount;
			}
#endif
		}
		else
		{
			_ASSERT(m_BoneID != -1);
			m_BoneController->GetTM(m_BoneID, tmlist[count++]);

#ifdef OBJECTMOTIONBLUR
			if (includelastTM)
				tmlist[count++] = m_LastWorldTM;
#endif
		}
	}
	else
	{
		tmlist[count++] = GetWorldTM();

#ifdef OBJECTMOTIONBLUR
		if (includelastTM)
			tmlist[count++] = m_LastWorldTM;
#endif
	}
	return count;
}

bool CModelMesh::CheckTransforms() const
{
	return (m_BoneCount<59);
}
