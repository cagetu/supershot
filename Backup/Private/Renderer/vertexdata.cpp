#include "rendercore.h"

//
//
//
VertexData::VertexData()
	: m_Stride(0), m_Count(0), m_FVF(0), m_Flags(0)
{
}

VertexData::~VertexData()
{
	Clear();
}

void VertexData::Bind(const void *p, int32 usage, int32 stride, int32 count)
{
	StreamData* element = new StreamData(p, usage, stride, count);
	m_Streams.insert(std::make_pair(usage, element));

	m_Count = count;

	m_Flags |= _NEED_UPDATE;
}

void VertexData::Clear()
{
	auto iend = m_Streams.end();
	for (auto it = m_Streams.begin(); it != iend; it++)
		delete (it->second);
	m_Streams.clear();

	m_Stride = 0;
	m_Count = 0;
	m_FVF = 0;
}

int32 VertexData::GetStride() const
{
	Update();

	return m_Stride;
}

int32 VertexData::GetFVF() const
{
	Update();

	return m_FVF;
}

void VertexData::Update() const
{
	if (m_Flags&_NEED_UPDATE)
	{
		m_Stride = 0;
		m_FVF = 0;

		auto iend = m_Streams.end();
		for (auto it = m_Streams.begin(); it != iend; it++)
		{
			m_FVF |= (it->second)->GetUsage();
			m_Stride += (it->second)->GetStride();
		}

		m_Flags &= ~_NEED_UPDATE;
	}
}


inline void _memcpy(unsigned char* dest, int32 pitch, const void* srcptr, int32 size, int32 count)
{
	char *src = (char*)srcptr;
	int32 i = 0;

	for (; i + 4<count; i += 4, dest += 4 * pitch, src += 4 * size)
	{
		memcpy(dest + 0 * pitch, src + 0 * size, size);
		memcpy(dest + 1 * pitch, src + 1 * size, size);
		memcpy(dest + 2 * pitch, src + 2 * size, size);
		memcpy(dest + 3 * pitch, src + 3 * size, size);
	}
	for (; i<count; i++, dest += pitch, src += size)
		memcpy(dest, src, size);
}

void VertexData::FillBuffer(void* dest)
{
	static uint32 _fvf = 0;
	static int32 off[8];

	uint32 fvf = GetFVF();

	if (_fvf != fvf)
	{
		_fvf = fvf;

		VertexDescription::GetOffsets(fvf, off);
	}

	const int32 vtxnum = m_Count;
	const int32 stride = m_Stride;
	const int32 offset = 0;

	unsigned char* ptr = (unsigned char*)dest + offset * stride;

	for (int32 i = 0; i<vtxnum;)
	{
		int32 blockcount = vtxnum - i < 16 ? vtxnum - i : 16;

		for (auto it : m_Streams)
		{
			const StreamData* element = it.second;

			if (fvf&FVF_XYZ)
			{
				const Vector3* v = (const Vector3*)element->GetData();
				_memcpy(ptr, stride, &v[i], sizeof(float) * 3, blockcount);
			}
			else if (fvf&FVF_XYZRHW)
			{
				const Vector3* v = (const Vector3*)element->GetData();
				_memcpy(ptr, stride, &v[i], sizeof(float) * 3, blockcount);
				for (int32 j = 0; j<blockcount; j++)
					*(float*)(ptr + stride*j + 3 * sizeof(float)) = 1.0f;
			}
			else if (fvf&FVF_NORMAL)
			{
				const Vector3* norm = (const Vector3*)element->GetData();

				if (fvf&FVF_NORMAL_PACKED)
				{
					unsigned char n[16][4];
					for (int32 j = 0; j<blockcount; j++)
						VertexCompress::NormalToUBYTE4N(n[j], norm[i + j]);
					_memcpy(ptr + off[1], stride, n, sizeof(unsigned char) * 4, blockcount);
				}
				else
				{
					_memcpy(ptr + off[1], stride, &norm[i], sizeof(float) * 3, blockcount);
				}
			}
			else if (fvf&FVF_SKIN_INDEX)
			{
				const unsigned char** skinindex = (const unsigned char**)element->GetData();

				_memcpy(ptr + off[2], stride, &skinindex[i][0], sizeof(unsigned char) * 4, blockcount);
			}
			else if (fvf&FVF_SKIN_INDEX)
			{
				const unsigned char** skinweight = (const unsigned char**)element->GetData();

				_memcpy(ptr + off[2] + sizeof(unsigned char) * 4, stride, &skinweight[i][0], sizeof(unsigned char) * 4, blockcount);
			}
			else if (fvf&FVF_DIFFUSE)
			{
				const uint32* diffuse = (const uint32*)element->GetData();

				_memcpy(ptr + off[2], stride, &diffuse[i], sizeof(uint32), blockcount);
			}
			else if (fvf&FVF_TEX0)
			{
				const float** uv = (const float**)element->GetData();

				if (fvf&FVF_TEX0_PACKED)
				{
					short t[16][2];
					for (int32 j = 0; j<blockcount; j++) {
						VertexCompress::TexcoordToShort2(t[j], uv[i + j]);
					}
					_memcpy(ptr + off[3], stride, t, sizeof(short) * 2, blockcount);
				}
				else
				{
					_memcpy(ptr + off[3], stride, &uv[i], sizeof(float) * 2, blockcount);
				}
			}
			else if (fvf&FVF_TEX1)
			{
				const float** uv2 = (const float**)element->GetData();

				if (fvf&FVF_TEX1_PACKED)
				{
					short t[16][2];
					for (int32 j = 0; j<blockcount; j++) {
						VertexCompress::TexcoordToShort2(t[j], uv2[i + j]);
					}
					_memcpy(ptr + off[4], stride, t, sizeof(short) * 2, blockcount);
				}
				else
				{
					_memcpy(ptr + off[4], stride, &uv2[i], sizeof(float) * 2, blockcount);
				}
			}
			else if (fvf&FVF_TANSPC)
			{
				const Vector4* tan = (const Vector4*)element->GetData();

				if (fvf&FVF_TANSPC_PACKED)
				{
					unsigned char t[16][4];
					for (int32 j = 0; j<blockcount; j++) {
						VertexCompress::TangentToUBYTE4N(t[j], tan[i + j]);
					}
					_memcpy(ptr + off[5], stride, t, sizeof(unsigned char) * 4, blockcount);
				}
				else
				{
					_memcpy(ptr + off[5], stride, &tan[i], sizeof(float) * 4, blockcount);
				}
			}
			else if (fvf&FVF_VERTEXAO)
			{
				const uint32* vtxao = (const uint32*)element->GetData();

				_memcpy(ptr + off[7], stride, &vtxao[i], sizeof(uint32), blockcount);
			}
			else if (fvf&FVF_SHCOEFF)
			{
				const float* shcoeff = (const float*)element->GetData();
#if 1
				float shcoeff_opt[16][9];
				for (int32 j = 0; j<blockcount; j++)
				{
					shcoeff_opt[j][0] = shcoeff[(i + j) * 9 + 3];
					shcoeff_opt[j][1] = shcoeff[(i + j) * 9 + 1];
					shcoeff_opt[j][2] = shcoeff[(i + j) * 9 + 2];

					shcoeff_opt[j][3] = shcoeff[(i + j) * 9 + 4];
					shcoeff_opt[j][4] = shcoeff[(i + j) * 9 + 5];
					shcoeff_opt[j][5] = shcoeff[(i + j) * 9 + 7];

					shcoeff_opt[j][6] = shcoeff[(i + j) * 9 + 8];
					shcoeff_opt[j][7] = shcoeff[(i + j) * 9 + 6];
					shcoeff_opt[j][8] = shcoeff[(i + j) * 9 + 0] - shcoeff_opt[j][7];
				}

				_memcpy(ptr + off[6], stride, shcoeff_opt, sizeof(float) * 3 * 3, blockcount);
#else
				_memcpy(ptr + off[6], stride, &shcoeff[i * 9], sizeof(float) * 3 * 3, blockcount);
#endif
			}
		}

		ptr += blockcount*stride;
		i += blockcount;
	}
}