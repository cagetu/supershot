#include "renderer.h"

__ImplementRtti(VisualShock, IRenderTarget, IRenderResource);

IRenderTarget::IRenderTarget()
	: m_Width(0)
	, m_Height(0)
	, m_MipmapCount(0)
	, m_MipmapLevel(0)
	, m_Multisample(0)
	, m_ClearFlags(0)
	, m_ClearColor(RGBA::WHITE)
	, m_ClearDepth(1.0f)
	, m_ClearStencil(0)
{
}

IRenderTarget::~IRenderTarget()
{
}
