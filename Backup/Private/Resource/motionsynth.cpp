#include "scene.h"
#include <algorithm>

CMotionSynth* CMotionSynth::Create(CMiniXML::const_iterator node, CModel* model)
{
	CMotionSynth* c = new CMotionSynth;
	if (c->Init(node, model) == false)
	{
		delete c;
		return NULL;
	}
	return c;
}

void CMotionSynth::Release(CMotionSynth* ms)
{
	delete ms;
}

CMotionSynth::CMotionSynth()
{
	m_BoneController = NULL;
	m_AnimationListRef = &m_AnimationList;
	m_DefaultCtrlrName = TEXT("default");
}

CMotionSynth* CMotionSynth::Duplicate(CModel* model)
{
	CMotionSynth* m = new CMotionSynth;

	m->m_BoneController = model->GetBoneController();
	m->m_AnimationListRef = m_AnimationListRef;

	for(unsigned int i=0; i<m_MotionList.size(); i++)
	{
		CMotionController* ctl = m_MotionList[i]->Duplicate(model, m);
		m->m_MotionList.push_back(ctl);

		for(auto it : m_MotionTable)
		{
			if (it.second == m_MotionList[i])
				m->m_MotionTable[it.first] = ctl;
		}
	}

	m->SetEnable(NULL, true);
	return m;
}

CMotionSynth::~CMotionSynth()
{
	for(auto it : m_AnimationList)
		CMotionController::Release(it.second);

	for(auto it : m_MotionTable)
		CMotionController::Release(it.second);
}

void CMotionSynth::PreUpdate(float dt, CEventListener* listener)
{
	for (auto it : m_ActiveMotions)
		it->PreUpdate(dt, this, listener);
}

void CMotionSynth::Update(float dt)
{
	for (auto it : m_ActiveMotions)
		it->Update(dt, this);
}

void CMotionSynth::SetEnable(const TCHAR* tag, bool enable)
{
	std::set <String>::iterator it;

	if (tag == NULL && enable == false)
	{
		if (m_Enable.empty())
			return ;

		m_Enable.clear();
	}
	else if (tag != NULL)
	{
		for(int i=0; tag[i] != '\0';)
		{
			int l;

			for(l=0; tag[i+l] != ',' && tag[i+l] != '\0'; l++) ;

			String mask(&tag[i], l);

			if (enable == true)
				m_Enable.insert(mask);
			else
			{
				it = m_Enable.find(mask);
				if (it != m_Enable.end())
					m_Enable.erase(it);
			}

			i += tag[i+l] ? l+1 : l;
		}
	}

	String masks;

	for(it=m_Enable.begin(); it!=m_Enable.end(); it++)
	{
		if (!masks.empty())
			masks += TEXT(",");
		masks += (*it);
	}

	SetEnable(masks);
}

void CMotionSynth::SetEnable(const TCHAR* masks)
{
	std::vector <CMotionController*> prevlist = m_ActiveMotions;

	m_ActiveMotions.clear();

	for(unsigned int i=0; i<m_MotionList.size(); i++)
	{
		bool activate = std::find(prevlist.begin(), prevlist.end(), m_MotionList[i]) != prevlist.end() ? true : false;

		if (m_MotionList[i]->SetEnable(masks) == true)
		{
			if (activate == false)
			{
				m_MotionList[i]->SetActivate(true, m_BoneController);
			}
			m_ActiveMotions.push_back(m_MotionList[i]);
		}
		else if (activate == true)
		{
			m_MotionList[i]->SetActivate(false, NULL);
		}
	}
}

void CMotionSynth::Reset()
{
	std::vector <CMotionController*> prevlist = m_ActiveMotions;

	m_ActiveMotions.clear();

	for(auto it : m_MotionList)
	{
		bool activate = std::find(prevlist.begin(), prevlist.end(), it) != prevlist.end() ? true : false;
		if (activate == true)
			it->SetActivate(false, NULL);
	}
}

bool CMotionSynth::Init(CMiniXML::const_iterator node, CModel* model)
{
	CMiniXML::const_iterator it;

	// Animation List
	CMiniXML::const_iterator ani = node.FindChild(TEXT("animation"));
	if (ani != NULL)
	{
		for (it = ani.FindChild(TEXT("animation")); it != NULL; it = it.Find(TEXT("animation")))
			m_AnimationList[it[TEXT("id")]] = CMotionController::Create(it, model, NULL);
	}

	// MotionController List
	CMiniXML::const_iterator m = node.FindChild(TEXT("motion"));
	if (m != NULL)
	{
		for (it = m.FindChild(TEXT("motion")); it != NULL; it = it.Find(TEXT("motion")))
		{
			CMotionController* ctl = CMotionController::Create(it, model, this);

			if (ctl != NULL)
			{
				m_MotionTable[it[TEXT("id")]] = ctl;
				m_MotionList.push_back(ctl);
			}
		}
	}

	if (m_MotionList.empty())
	{
		if (m_AnimationList.empty())
			return false;
#if 0
		<motion id="default" mask="_ANIMBLEND,_BASEANIM,_MOTIONEVENT">
			<BLENDTIME>0.100000</BLENDTIME>
			<CONDITION/>
			<TEST_ANIMATION>stand_run</TEST_ANIMATION>
		</motion>
#endif
		CMiniXML xml;
		CMiniXML::iterator motion = xml.GetRoot().Insert(TEXT("motion"));
		motion.SetAttribute(TEXT("mask"), TEXT("_BASEANIM,_MOTIONEVENT"));
		
		CMotionController* ctl = CMotionController::Create(motion, model, this);
		m_MotionTable[TEXT("default")] = ctl;
		m_MotionList.push_back(ctl);
	}

	m_BoneController = model->GetBoneController();

	SetEnable(NULL, true);
	return true;
}

#ifdef __APPLE__
#import <QuartzCore/QuartzCore.h>
#endif

Vector3 CMotionSynth::FindTarget(const TCHAR* id)
{
	std::map <String, Vector3>::iterator it = m_VecValue.find(id);
	if (it != m_VecValue.end())
		return (*it).second;

	return Vector3::ZERO;
}

CMotionController* CMotionSynth::FindAnimation(const TCHAR* anim) const
{
	auto it = (*m_AnimationListRef).find(anim);
	return it != (*m_AnimationListRef).end() ? (*it).second : NULL;
}

void CMotionSynth::SetAnimation(const TCHAR* id, const TCHAR* motion, bool force, bool notransition)
{
	auto it = m_MotionTable.find(id == NULL ? m_DefaultCtrlrName : String(id));
	if (it != m_MotionTable.end())
		(*it).second->SetValue(FindAnimation(motion), force, notransition);
}

void CMotionSynth::SetValue(const TCHAR* id, const TCHAR* val)
{
	auto it = m_MotionTable.find(id == NULL ? m_DefaultCtrlrName : String(id));
	if (it != m_MotionTable.end())
		(*it).second->SetValue(val);
}

void CMotionSynth::SetValue(const TCHAR* id, float val)
{
	auto it = m_MotionTable.find(id == NULL ? m_DefaultCtrlrName : String(id));
	if (it != m_MotionTable.end())
		(*it).second->SetValue(val);
}

void CMotionSynth::SetValue(const TCHAR* id, const Vector3& pos)
{
	auto it = m_MotionTable.find(id == NULL ? m_DefaultCtrlrName : String(id));
	if (it != m_MotionTable.end())
		(*it).second->SetValue(pos);

	m_VecValue[id] = pos;
}

CBoneController* CMotionSynth::GetBoneController(const TCHAR* /*id*/)
{
	return m_BoneController;
}

float CMotionSynth::GetLength(const TCHAR* motion)
{
	auto it = (*m_AnimationListRef).find(motion);
	if (it != (*m_AnimationListRef).end())
		return (*it).second->GetLength();
	return 0.0f;
}

bool CMotionSynth::IsFinished(const TCHAR* id)
{
	auto it = m_MotionTable.find(id == NULL ? m_DefaultCtrlrName : String(id));
	if (it != m_MotionTable.end())
		return (*it).second->IsFinished();
	return false;
}

Vector3 CMotionSynth::GetMoveDelta()
{
	Vector3 v(0.0f, 0.0f, 0.f);

	for (auto it : m_ActiveMotions)
		v += it->GetMoveDelta();

	return v;
}

void CMotionSynth::Impulse(const Vector3 &pivot, const Vector3 &force)
{
	for (auto it : m_ActiveMotions)
		it->Impulse(pivot, force);
}
