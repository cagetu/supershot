#include "resourcecore.h"

//#define _TEST_SKIN

CGeometry::CGeometry(const TCHAR* objlist)
{
	m_Flags = _BONELIST | _FLIPV | _REVERSE_WIND;
	//m_Flags |= _CALC_TANGENT| _USE_SKIN | _USE_VERTEXCOLOR;
	m_Flags |= _STOREMESH;

	if (objlist != NULL)
	{
		m_Flags |= _OBJ_MERGE;
		m_ObjList = objlist;
	}

	m_ExtraUV = NULL;
	m_ExtraUVCount = 0;
	m_ExtraDepthIdx = 0;
	m_PackSize[0] = m_PackSize[1] = 0;

	m_SplitBoneCount = 40;
}

CGeometry::~CGeometry()
{
	if (m_ExtraUV != NULL)
		delete[] m_ExtraUV;
}

bool CGeometry::Load(const TCHAR * fname, uint32 options)
{
	CFileIO * fp = CStdIO::Open(fname);
	if (fp == NULL)
		return false;

	m_Flags |= options;

	int tag = 0;

	while (fp->Read(&tag, 1) > 0)
	{
		if (tag == 0xff)
		{
			uint32 guid;
			fp->Read(&guid, 4);

			if (m_GUID != 0 && m_GUID != guid)
			{
				m_Flags &= _EXTRA_UV;
				// miss match
			}
			m_GUID = guid;
		}
		else if (tag == 0x01)
		{
			unsigned short uvcount;

			fp->Read(&m_PackSize[0], 4 * 2);
			fp->Read(&uvcount, 2);

			m_ExtraUVCount = uvcount;
			m_ExtraUV = new float[uvcount][2];

			fp->Read(m_ExtraUV, sizeof(float) * 2 * uvcount);

			m_Flags |= _EXTRA_UV;
		}
		else if (tag == 0x03)
		{
			_ASSERT(m_Flags&_EXTRA_UV);

			CGeometryReader f(fp);

			_EXTRACHANNEL extra;
			extra.name = f.ReadString();
			fp->Read(&extra.facenum, 4);
			extra.face = new unsigned short[extra.facenum * 3];
			fp->Read(extra.face, sizeof(unsigned short)*extra.facenum * 3);
			m_Extra.push_back(extra);
		}
		else if (tag == 0x05)
		{
			m_Flags |= _EXTRA_TRANSLUCENT;

			CGeometryReader f(fp);

			String name = f.ReadString();
			int vtxcount = f.ReadUShort();
			float* shcoeff = new float[9 * vtxcount];
			fp->Read(shcoeff, 9 * vtxcount*sizeof(float));

			_EXTRA_SURFACEDEPTH ex;
			ex.name = name;
			ex.vtxnum = vtxcount;
			ex.shcoeff = shcoeff;
			m_ExtraDepth.push_back(ex);
		}
		else if (tag == 0x07)
		{
			m_Flags |= _EXTRA_VERTEXAO;

			CGeometryReader f(fp);

			String name = f.ReadString();
			int vtxcount = f.ReadUShort();
			uint32* ao = new uint32[vtxcount];
			fp->Read(ao, vtxcount*sizeof(uint32));

			_EXTRA_AO ex;
			ex.name = name;
			ex.vtxnum = vtxcount;
			ex.ao = ao;
			m_AOList.push_back(ex);
		}
		else if (tag == 0x02 || tag == 0x12)
		{
			LoadGeomRaw(fp, (tag & 0x10) ? true : false);
		}
		else if (tag == 0x08)
		{
			LoadGeomBin(fp);
		}
		else if (tag == 0x04 || tag == 0x09)
		{
			LoadBone(fp, tag);
		}
		else
		{
			delete (fp);
			return false;
		}
	}

	MergeObject();

	unsigned int boneCount = m_BoneList.size();
	if (boneCount > 0)
	{
		m_BoneNameTable = new String[boneCount];
		m_BoneParentTable = new int[boneCount];
		m_BoneInvTMTable = new Mat4[boneCount];

		for (unsigned int i = 0; i< boneCount; i++)
		{
			m_BoneNameTable[i] = m_BoneList[i].name;
			m_BoneParentTable[i] = m_BoneList[i].parent;
			m_BoneInvTMTable[i] = m_BoneList[i].invWorldTM;
			m_BoneTable[m_BoneList[i].name] = i;
		}
	}

	delete (fp);
	return true;
}

template <class _Tx>
class CTable
{
public:
	int Insert(_Tx t)
	{
		int hash = *(int*)&t;
		for (std::multimap <int, int>::iterator it = table.lower_bound(hash); it != table.upper_bound(hash); it++)
		{
			if (!memcmp(&list[(*it).second], &t, sizeof(t)))
				return (*it).second;
		}
		table.insert(std::make_pair(hash, (int)list.size()));
		list.push_back(t);
		return list.size() - 1;
	}
	std::vector <_Tx> list;
private:
	std::multimap <int, int> table;
};

template <class _Tx> void Swap(_Tx& a, _Tx&b) { _Tx t = b; b = a; a = t; }

void CGeometry::LoadGeomRaw(CFileIO *fp, bool skin)
{
	CGeometryReader f(fp);

	String name = f.ReadString();

	int matcount = f.ReadChar();
	String hint;
	int i;

	for (i = 0; i<matcount; i++)
		hint = hint + f.ReadString();

	int numvert = f.ReadUShort();
	Vector3 * v = NULL;

	CPolygonSoup polysoup;

	if (skin == false)
	{
		v = new Vector3[numvert];

		for (i = 0; i<numvert; i++)
			v[i] = f.ReadVector();
		polysoup.SetPosition(v, numvert);
	}
	else if (m_Flags&_USE_SKIN)
	{
		v = new Vector3[numvert];
		int(*skinV)[4] = NULL;
		float(*skinW)[4] = NULL;

		skinV = new int[numvert][4];
		skinW = new float[numvert][4];

		for (i = 0; i<numvert; i++)
		{
			v[i] = f.ReadVector();

			int n = f.ReadChar(), j;

			for (j = 0; j<4; j++)
			{
				skinV[i][j] = -1;
				skinW[i][j] = 0.0f;
			}

			for (j = 0; j<n; j++)
			{
				String bonename = f.ReadString();
				float w = f.ReadFloat();

				int idx = FindBone(bonename);
				if (idx == -1)
					continue;

				if (j >= 4)
				{
					int slot = 0;
					for (slot = 0; slot<j && slot<4; slot++)
					{
						if (skinV[i][slot] == idx || skinV[i][slot] == -1)
						{
							skinW[i][slot] += w;
							break;
						}
					}

					if (slot == 4)
					{
						for (int k = 0; k<4; k++)
						{
							if ((slot == 4 && skinW[i][k] < w) || (slot != 4 && skinW[i][k] < skinW[i][slot]))
								slot = k;
						}
						if (slot != 4)
						{
							skinV[i][slot] = idx;
							skinW[i][slot] = w;
						}
					}
				}
				else
				{
					skinV[i][j] = idx;
					skinW[i][j] = w;
				}
			}

			float w = 0.0f;

			for (j = 0; j<n && j<4; j++)
				w += skinW[i][j];

			if (w > 0)
			{
				for (j = 0; j<n && j < 4; j++)
				{
					skinW[i][j] *= 1.0f / w;
				}
			}
		}

		polysoup.SetPosition(v, skinV, skinW, numvert);
		delete[] skinV;
		delete[] skinW;
	}
	else
	{
		v = new Vector3[numvert];
		for (i = 0; i<numvert; i++)
		{
			v[i] = f.ReadVector();

			int n = f.ReadChar(), j;
			for (j = 0; j<n; j++)
			{
				f.ReadString();
				f.ReadFloat();
			}
		}
		polysoup.SetPosition(v, numvert);
	}

	int numvertcolor = f.ReadUShort();
	if (numvertcolor > 0)
	{
		if (!(m_Flags&_VERTEXONLY))
		{
			unsigned char(*vertcolor)[3] = NULL;
			vertcolor = new unsigned char[numvertcolor][3];
			f.Read(vertcolor, numvertcolor * 3);
			polysoup.SetColor(vertcolor, numvertcolor);
			delete[] vertcolor;
		}
		else
			f.Seek(numvertcolor * 3, CFileIO::_CUR);
	}

	int numuv[2] = { 0, 0 };
	for (i = 0; i<2; i++)
	{
		if (!(m_Flags&_VERTEXONLY))
		{
			numuv[i] = f.ReadUShort();
			if (numuv[i] > 0)
			{
				float(*uv)[2] = new float[numuv[i]][2];
				f.Read(uv, sizeof(float) * 2 * numuv[i]);

				if (m_Flags&_FLIPV)
				{
					for (int j = 0; j<numuv[i]; j++)
						uv[j][1] = 1.0f - uv[j][1];
				}
				polysoup.SetTexcoord(i, uv, numuv[i]);
				delete[] uv;
			}
		}
		else
		{
			numuv[i] = f.ReadUShort();
			if (numuv[i] > 0)
				f.Seek(sizeof(float) * 2 * numuv[i], CFileIO::_CUR);
		}
	}

	int numface = f.ReadUShort();
	unsigned short(*face)[1 + 3 + 3 + 3 + 3 + 3] = new unsigned short[numface][1 + 3 + 3 + 3 + 3 + 3];
	//	Vector3 (*facenom)[3] = new Vector3 [numface][3];
	CTable <Vector3> normal;

	for (i = 0; i<numface; i++)
	{
		memset(face[i], 0, sizeof(unsigned short)*(1 + 3 + 3 + 3 + 3 + 3));

		face[i][0] = f.ReadChar();

		face[i][1] = f.ReadUShort();
		face[i][2] = f.ReadUShort();
		face[i][3] = f.ReadUShort();

		_ASSERT(face[i][1] < numvert);
		_ASSERT(face[i][2] < numvert);
		_ASSERT(face[i][3] < numvert);

		Vector3 n1 = f.ReadVector();
		Vector3 n2 = f.ReadVector();
		Vector3 n3 = f.ReadVector();

		if (!(m_Flags&_VERTEXONLY))
		{
			face[i][13] = normal.Insert(n1);
			face[i][14] = normal.Insert(n2);
			face[i][15] = normal.Insert(n3);

			if (numvertcolor > 0)
			{
				face[i][4] = f.ReadUShort();
				face[i][5] = f.ReadUShort();
				face[i][6] = f.ReadUShort();
			}
			if (numuv[0] > 0)
			{
				face[i][7] = f.ReadUShort();
				face[i][8] = f.ReadUShort();
				face[i][9] = f.ReadUShort();
			}
			if (numuv[1] > 0)
			{
				face[i][10] = f.ReadUShort();
				face[i][11] = f.ReadUShort();
				face[i][12] = f.ReadUShort();
			}

#if 1
			Vector3 facenorm = Vector3::CrossProduct(Vector3::Normalize(v[face[i][3]] - v[face[i][1]]), Vector3::Normalize(v[face[i][2]] - v[face[i][1]]));
			if (Vector3::DotProduct(facenorm, n1) < 0 && Vector3::DotProduct(facenorm, n2) < 0 && Vector3::DotProduct(facenorm, n3) < 0)
			{	// needs resetxform
				Swap(face[i][2], face[i][3]);
				Swap(face[i][5], face[i][6]);
				Swap(face[i][8], face[i][9]);
				Swap(face[i][11], face[i][12]);
				Swap(face[i][14], face[i][15]);
			}
#endif
		}
		else
		{
			if (numvertcolor > 0)
				f.Seek(sizeof(unsigned short) * 3, CFileIO::_CUR);
			if (numuv[0] > 0)
				f.Seek(sizeof(unsigned short) * 3, CFileIO::_CUR);
			if (numuv[1] > 0)
				f.Seek(sizeof(unsigned short) * 3, CFileIO::_CUR);
		}
	}

	if (!(m_Flags&_VERTEXONLY) && !normal.list.empty())
		polysoup.SetNormal(&normal.list[0], (int)normal.list.size());

	if (m_Flags&_EXTRA_UV)
	{
		polysoup.SetTexcoord(1, m_ExtraUV, m_ExtraUVCount);

		std::vector <_EXTRACHANNEL>::iterator it;
		for (it = m_Extra.begin(); it != m_Extra.end(); it++)
		{
			if (!string::strcmp(name, (*it).name) && (*it).facenum == numface)
			{
				for (i = 0; i<numface; i++)
				{
					face[i][10] = (*it).face[i * 3 + 0];
					face[i][11] = (*it).face[i * 3 + 1];
					face[i][12] = (*it).face[i * 3 + 2];
				}
				break;
			}
		}
	}

	m_MatHint = hint;

	if (numface > 0)
		Insert(polysoup, name, matcount, face, numface);

	delete[] v;
	delete[] face;
}

void CGeometry::LoadGeomBin(CFileIO* fp)
{
	CGeometryReader f(fp);

	Vector3* vtx;
	Vector3* norm;
	uint32* diffuse;
	float(*uv1)[2];
	float(*uv2)[2];
	Vector4 * tan;
	unsigned char(*skinindex)[4];
	unsigned char(*skinweight)[4];
	float* shcoeff;
	uint32* vtxao;
	unsigned short * face;
	int * bonetable;

	_DESC desc;

	desc.name = f.ReadString();
	desc.diffuseMap = f.ReadString();
	desc.specularMap = f.ReadString();
	desc.normalMap = f.ReadString();

	desc.bonename = desc.name;

	unsigned short flags = f.ReadUShort();

	desc.vtxnum = f.ReadULong();

	desc.vtx = vtx = (flags & 0x0001) ? new Vector3[desc.vtxnum] : NULL;
	desc.norm = norm = (flags & 0x0002) ? new Vector3[desc.vtxnum] : NULL;
	desc.diffuse = diffuse = (flags & 0x0004) ? new uint32[desc.vtxnum] : NULL;
	desc.uv1 = uv1 = (flags & 0x0008) ? new float[desc.vtxnum][2] : NULL;
	desc.uv2 = uv2 = (flags & 0x0010) ? new float[desc.vtxnum][2] : NULL;
	desc.tan = tan = (flags & 0x0020) ? new Vector4[desc.vtxnum] : NULL;
	desc.shcoeff = shcoeff = (flags & 0x0040) ? new float[9 * desc.vtxnum] : NULL;
	desc.vtxao = vtxao = (flags & 0x0080) ? new uint32[desc.vtxnum] : NULL;
	desc.skinindex = skinindex = (flags & 0x0100) ? new unsigned char[desc.vtxnum][4] : NULL;
	desc.skinweight = skinweight = (flags & 0x0100) ? new unsigned char[desc.vtxnum][4] : NULL;

	if (vtx)
		f.Read(vtx, sizeof(Vector3)*desc.vtxnum);

	if (norm)
		f.Read(norm, sizeof(Vector3)*desc.vtxnum);

	if (diffuse)
		f.Read(diffuse, sizeof(uint32)*desc.vtxnum);

	if (uv1)
		f.Read(uv1, sizeof(float) * 2 * desc.vtxnum);

	if (uv2)
		f.Read(uv2, sizeof(float) * 2 * desc.vtxnum);

	if (tan)
		f.Read(tan, sizeof(Vector4)*desc.vtxnum);

	if (shcoeff)
		f.Read(shcoeff, sizeof(float) * 9 * desc.vtxnum);

	if (vtxao)
		f.Read(vtxao, sizeof(uint32)*desc.vtxnum);

	if (skinindex)
	{
		f.Read(skinindex, sizeof(unsigned char) * 4 * desc.vtxnum);
		f.Read(skinweight, sizeof(unsigned char) * 4 * desc.vtxnum);
	}

	if (!(m_Flags&_CALC_TANGENT))
	{
		delete[] desc.tan;
		desc.tan = tan = NULL;
	}

	if (!(m_Flags&_USE_SKIN))
	{
		delete[] desc.skinindex;
		delete[] desc.skinweight;
		desc.skinindex = skinindex = NULL;
		desc.skinweight = skinweight = NULL;
	}
	if (!(m_Flags&_USE_VERTEXCOLOR))
	{
		delete[] desc.diffuse;
		desc.diffuse = diffuse = NULL;
	}

	desc.facen = f.ReadULong();
	desc.face = face = new unsigned short[desc.facen * 3];
	f.Read(face, sizeof(unsigned short)*desc.facen * 3);

	desc.bonecount = f.ReadULong();
	desc.bonetable = bonetable = desc.bonecount > 0 ? new int[desc.bonecount] : NULL;

	if (bonetable)
		f.Read(bonetable, sizeof(int)*desc.bonecount);

	if (m_Flags&_OBJ_MERGE)
	{
		m_MergeObjects.insert(std::make_pair(desc.name, desc));
	}
	else
	{
		// skinned mesh이기는 하지만, bonecount가 1이면, skeleton animation으로 전환한다.
		if (flags & 0x0100 && desc.bonecount == 1)
		{
			delete[] desc.skinindex;
			delete[] desc.skinweight;
			desc.skinindex = skinindex = NULL;
			desc.skinweight = skinweight = NULL;
		}

		const bool SplitMesh = false;
		const int SplitMaxBoneCount = _MAX_BONE;

		if (SplitMesh == true)
		{
			// 스킨 메쉬일경우 메쉬 분할...
			if (flags & 0x0100 && desc.bonecount > SplitMaxBoneCount)
			{
				int maxbone = desc.bonecount / 2 < SplitMaxBoneCount ? desc.bonecount / 2 : SplitMaxBoneCount;

				Split(desc, maxbone);

				PlatformUtil::DebugOutFormat(TEXT("[CModel::LoadMaterial] '%s is splitted (Bone Count %d/%d) in %s\n"), 
											 desc.name.c_str(), desc.bonecount, maxbone /*, fname*/);
			}
			else
			{
				Insert(desc);
			}
		}
		else
		{
			Insert(desc);
		}

		if (vtx) delete[] vtx;
		if (norm) delete[] norm;
		if (diffuse) delete[] diffuse;
		if (uv1) delete[] uv1;
		if (uv2) delete[] uv2;
		if (tan) delete[] tan;
		if (shcoeff) delete[] shcoeff;
		if (vtxao) delete[] vtxao;
		if (skinindex) delete[] skinindex;
		if (skinweight) delete[] skinweight;
		if (face) delete[] face;
		if (bonetable) delete[] bonetable;
	}
}

void CGeometry::LoadBone(CFileIO* fp, int tag)
{
	CGeometryReader f(fp);

	Mat4 worldTM = Mat4::IDENTITY;
	Mat4 localTM = Mat4::IDENTITY;

	String name = f.ReadString();
	String parent = f.ReadString();

	if (m_Flags&_BONELIST)
	{
		if (tag == 0x04)
		{
			worldTM = f.ReadMatrix();
			localTM = f.ReadMatrix();
		}
		else
		{
			f.Read(&worldTM._11, sizeof(Mat4));
			f.Read(&localTM._11, sizeof(Mat4));
		}

		_BONE b;

		if (parent.empty())
			b.parent = -1;
		else
		{
			std::vector <_BONE>::iterator it = std::find(m_BoneList.begin(), m_BoneList.end(), parent);
			_ASSERT(it != m_BoneList.end());

			b.parent = it - m_BoneList.begin();
		}

		b.name = name;
		b.worldTM = worldTM;
		b.invWorldTM = worldTM.Inverse();
		b.localTM = localTM;
		m_BoneList.push_back(b);
	}
	else
	{
		fp->Seek(tag == 0x04 ? sizeof(float) * 3 * 4 * 2 : sizeof(float) * 4 * 4 * 2, CFileIO::_CUR);
	}
}

void CGeometry::MergeObject()
{
	if ((m_Flags&_OBJ_MERGE) && m_MergeObjects.empty() == false)
	{
		const TCHAR* objname = m_ObjList;

		uint32 meshtype = 0;
		std::vector <_DESC*> meshlist;

		for (int i = 0; objname[i] != '\0';)
		{
			if (objname[i] != ',' && objname[i] != ';')
			{
				int l, seg = -1;
				for (l = 0; objname[i + l] != '\0' && objname[i + l] != ',' && objname[i + l] != ';'; l++)
				{
					if (objname[i + l] == '#')
						seg = l;
				}
				String obj(&objname[i], seg == -1 ? l : seg);
				int idx = seg != -1 ? string::atoi(&objname[i + seg + 1]) : 0;
				i += l;

				std::multimap <String, _DESC>::iterator it;
				for (it = m_MergeObjects.lower_bound(obj); it != m_MergeObjects.upper_bound(obj); it++, idx--)
				{
					if (idx == 0)
					{
						meshlist.push_back(&(*it).second);

						if ((*it).second.skinindex != NULL)
						{
							meshtype |= _SKINNED;
							meshtype |= ((*it).second.bonecount == 1) ? _SKELETAL : 0;
						}
					}
				}

				if (objname[i] == '\0' || objname[i] == ';')
				{
					_ASSERT(!meshlist.empty());

					unsigned int base = 0;
					for (unsigned int idx = 0; idx < meshlist.size();)
					{
						if (base != idx && Merge(*meshlist[base], *meshlist[idx], meshtype) == false)
						{
							Insert(*meshlist[base]);
							base = idx;
						}
						else
						{
							idx++;
						}
					}

					Insert(*meshlist[base]);

					meshlist.clear();
					meshtype = 0;
				}
			}
			else
			{
				i++;
			}
		}

		for (auto it : m_MergeObjects)
		{
			_DESC& desc = it.second;

			delete[] desc.vtx;
			delete[] desc.norm;
			delete[] desc.diffuse;
			delete[] desc.uv1;
			delete[] desc.uv2;
			delete[] desc.tan;
			delete[] desc.shcoeff;
			delete[] desc.vtxao;
			delete[] desc.skinindex;
			delete[] desc.skinweight;
			delete[] desc.face;
			delete[] desc.bonetable;
		}
	}
}

bool CGeometry::Merge(_DESC& dest, const _DESC& child, uint32 type)
{
	if (dest.shcoeff != NULL || child.shcoeff != NULL || dest.vtxao != NULL || child.vtxao != NULL)
		return false;

	if (dest.vtxnum + child.vtxnum >= 65536)
		return false;

	if ((dest.uv2 == NULL && child.uv2 != NULL) ||
		(dest.uv2 != NULL && child.uv2 == NULL))
		return false;

	int vtxnum = dest.vtxnum + child.vtxnum;
	int facen = dest.facen + child.facen;

	int i;

	std::vector <int> bonetable;

	unsigned char(*skinindex)[4] = NULL;
	unsigned char(*skinweight)[4] = NULL;

	if (type&_SKINNED)
	{
		skinindex = new unsigned char[vtxnum][4];
		skinweight = new unsigned char[vtxnum][4];

		if (dest.skinindex == NULL)
		{
			// bonecount == 1인 skeletal 메쉬의 경우와 static mesh를 구분해야 한다.
			int boneidx = (dest.bonecount == 1) ? dest.bonetable[0] : FindBone(dest.bonename);
			bonetable.push_back(boneidx);

			memset(skinindex, bonetable.size() - 1, sizeof(unsigned char) * 4 * dest.vtxnum);
			for (i = 0; i<dest.vtxnum; i++)
			{
				skinweight[i][0] = 255;
				skinweight[i][1] = skinweight[i][2] = skinweight[i][3] = 0;
			}
		}
		else
		{
			// skin mesh와 skin mesh를 합칠 때에는 좀 더 테스트 요망! (여기로 들어올 때...)
			for (i = 0; i<dest.bonecount; i++)
				bonetable.push_back(dest.bonetable[i]);

			memcpy(skinindex, dest.skinindex, sizeof(unsigned char) * 4 * dest.vtxnum);
			memcpy(skinweight, dest.skinweight, sizeof(unsigned char) * 4 * dest.vtxnum);
		}

		if (child.skinindex == NULL)
		{
			// bonecount == 1인 skeletal 메쉬의 경우와 static mesh를 구분해야 한다.
			int boneidx = (child.bonecount == 1) ? child.bonetable[0] : FindBone(child.bonename);

			std::vector <int>::iterator it = std::find(bonetable.begin(), bonetable.end(), boneidx);
			if (it == bonetable.end())
			{
				bonetable.push_back(boneidx);
				boneidx = bonetable.size() - 1;
			}
			else
			{
				boneidx = it - bonetable.begin();
			}

			memset(skinindex[dest.vtxnum], boneidx, sizeof(unsigned char) * 4 * child.vtxnum);

			for (i = dest.vtxnum; i<child.vtxnum + dest.vtxnum; i++)
			{
				skinweight[i][0] = 255;
				skinweight[i][1] = skinweight[i][2] = skinweight[i][3] = 0;
			}
		}
		else
		{
			std::vector <int> table;
			for (i = 0; i<child.bonecount; i++)
			{
				std::vector <int>::iterator it = std::find(bonetable.begin(), bonetable.end(), child.bonetable[i]);
				if (it == bonetable.end())
				{
					bonetable.push_back(child.bonetable[i]);
					table.push_back(bonetable.size() - 1);

					if (bonetable.size() >= 256)
					{
						delete[] skinindex;
						delete[] skinweight;
						return false;
					}
				}
				else
				{
					table.push_back(it - bonetable.begin());
				}
			}

			for (i = 0; i<child.vtxnum; i++)
			{
				skinindex[i + dest.vtxnum][0] = table[child.skinindex[i][0]];
				skinindex[i + dest.vtxnum][1] = table[child.skinindex[i][1]];
				skinindex[i + dest.vtxnum][2] = table[child.skinindex[i][2]];
				skinindex[i + dest.vtxnum][3] = table[child.skinindex[i][3]];
			}
			memcpy(skinweight[dest.vtxnum], child.skinweight, sizeof(unsigned char) * 4 * child.vtxnum);
		}
	}

	Vector3 * vtx = new Vector3[vtxnum];
	Vector3 * norm = new Vector3[vtxnum];
	uint32 *diffuse = child.diffuse ? new uint32[vtxnum] : NULL;
	float(*uv1)[2] = child.uv1 ? new float[vtxnum][2] : NULL;
	float(*uv2)[2] = child.uv2 ? new float[vtxnum][2] : NULL;
	Vector4 * tan = child.tan ? new Vector4[vtxnum] : NULL;

	memcpy(vtx, dest.vtx, sizeof(Vector3)*dest.vtxnum);
	memcpy(&vtx[dest.vtxnum], child.vtx, sizeof(Vector3)*child.vtxnum);

	memcpy(norm, dest.norm, sizeof(Vector3)*dest.vtxnum);
	memcpy(&norm[dest.vtxnum], child.norm, sizeof(Vector3)*child.vtxnum);

	if (diffuse)
	{
		if (dest.diffuse == NULL)
			memset(diffuse, 0xff, sizeof(uint32)*dest.vtxnum);
		else
			memcpy(diffuse, dest.diffuse, sizeof(uint32)*dest.vtxnum);
		memcpy(&diffuse[dest.vtxnum], child.diffuse, sizeof(uint32)*child.vtxnum);
	}

	if (uv1)
	{
		memcpy(uv1, dest.uv1, sizeof(float) * 2 * dest.vtxnum);
		memcpy(&uv1[dest.vtxnum], child.uv1, sizeof(float) * 2 * child.vtxnum);
	}
	if (uv2)
	{
		memcpy(uv2, dest.uv2, sizeof(float) * 2 * dest.vtxnum);
		memcpy(&uv2[dest.vtxnum], child.uv2, sizeof(float) * 2 * child.vtxnum);
	}
	if (tan)
	{
		memcpy(tan, dest.tan, sizeof(Vector4)*dest.vtxnum);
		memcpy(&tan[dest.vtxnum], child.tan, sizeof(Vector4)*child.vtxnum);
	}

	unsigned short * face = new unsigned short[facen * 3];
	memcpy(face, dest.face, sizeof(unsigned short) * 3 * dest.facen);
	for (i = 0; i<child.facen * 3; i++)
		face[dest.facen * 3 + i] = child.face[i] + dest.vtxnum;

	delete[] dest.vtx;
	delete[] dest.norm;
	delete[] dest.diffuse;
	delete[] dest.uv1;
	delete[] dest.uv2;
	delete[] dest.tan;
	delete[] dest.skinindex;
	delete[] dest.skinweight;
	delete[] dest.face;
	delete[] dest.bonetable;

	dest.vtx = vtx;
	dest.norm = norm;
	dest.diffuse = diffuse;
	dest.uv1 = uv1;
	dest.uv2 = uv2;
	dest.tan = tan;
	dest.face = face;
	dest.facen = facen;
	dest.vtxnum = vtxnum;

	if (type&_SKINNED)
	{
		_ASSERT(bonetable.size() > 0);

		if (bonetable.size() == 1 && bonetable[0] == -1)
		{
			_ASSERT(dest.bonecount == 0 && dest.bonetable == 0);
			dest.skinindex = NULL;
			dest.skinweight = NULL;

			delete[] skinindex;
			delete[] skinweight;
		}
		else
		{
			dest.skinindex = skinindex;
			dest.skinweight = skinweight;

			dest.bonecount = bonetable.size();
			int * ptable = new int[dest.bonecount];
			memcpy(&ptable[0], &bonetable[0], sizeof(int)*dest.bonecount);
			dest.bonetable = ptable;
		}
	}
	else
	{
		_ASSERT(dest.bonecount == 0 && dest.bonetable == 0);
		dest.skinindex = NULL;
		dest.skinweight = NULL;
		dest.bonecount = 0;
		dest.bonetable = 0;
	}

	return true;
}

static void* Duplicate(const void* ptr, int size)
{
	if (ptr == NULL)
		return NULL;
	void *p = (void*)(new char[size]);
	memcpy(p, ptr, size);
	return p;
}


void GenerateTangentSpace(Vector4* tan, Vector3* vtx, int vtxnum, Vector3* norm, const float(*uv)[2], unsigned short* faceidx, int facenum)
{
	// http://www.terathon.com/code/tangent.html
	Vector3 * tan1 = new Vector3[vtxnum];
	Vector3 * tan2 = new Vector3[vtxnum];
	int i;

	memset(tan1, 0, vtxnum * sizeof(Vector3));
	memset(tan2, 0, vtxnum * sizeof(Vector3));

	for (i = 0; i < facenum; i++)
	{
		// vertexIdx
		int i1 = faceidx[i * 3 + 0];
		int i2 = faceidx[i * 3 + 1];
		int i3 = faceidx[i * 3 + 2];

		/// position
		const Vector3& localV1 = vtx[i1];
		const Vector3& localV2 = vtx[i2];
		const Vector3& localV3 = vtx[i3];

		// compute tangents
		float x1 = localV2.x - localV1.x;
		float x2 = localV3.x - localV1.x;
		float y1 = localV2.y - localV1.y;
		float y2 = localV3.y - localV1.y;
		float z1 = localV2.z - localV1.z;
		float z2 = localV3.z - localV1.z;

		float v1u = uv[i1][0];
		float v1v = uv[i1][1];
		float v2u = uv[i2][0];
		float v2v = uv[i2][1];
		float v3u = uv[i3][0];
		float v3v = uv[i3][1];

		float s1 = v2u - v1u;
		float s2 = v3u - v1u;
		float t1 = v2v - v1v;
		float t2 = v3v - v1v;

		float l = (s1 * t2 - s2 * t1);

		// catch singularity
#if 1
		if (l == 0.0f)
			l = 0.0001f;
#else
		if (fabs(l) <= 1E-6f)
			continue;
#endif

		float r = 1.0f / l;
		Vector3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
		Vector3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	for (i = 0; i<vtxnum; i++)
	{
		const Vector3& nor = norm[i];
		const Vector3& t = tan1[i];

		if (t == Vector3::ZERO)
		{
			tan[i] = Vector4(1, 0, 0, 0);
		}
		else
		{
			// Gram-Schmidt orthogonalize
			Vector3 invTangent = t - nor * Vector3::DotProduct(nor, t);
			invTangent.Normalize();

			// Calculate handedness
			float h = Vector3::DotProduct(Vector3::CrossProduct(nor, t), tan2[i]) < 0.0f ? -1.0f : 1.0f;

			tan[i] = Vector4(invTangent.x, invTangent.y, invTangent.z, h);
		}
	}

	delete[] tan2;
	delete[] tan1;
}

void CGeometry::Insert(CPolygonSoup& polysoup, const TCHAR* name, int matcount, unsigned short(*face)[1 + 3 + 3 + 3 + 3 + 3], int numface)
{
	const TCHAR* p = m_MatHint.c_str();
	int i;

	for (int n = 0; n<matcount; n++, p = p == NULL ? NULL : p + 1)
	{
		String diffuseMap;
		String specularMap;
		String normalMap;

		if (p != NULL)
			p = string::strstr(p, TEXT("material:[")); // material:[diffuse=%;spec=%;norm=%]

		if (p != NULL)
		{
			const TCHAR* pend = string::strchr(p, ']');
			const TCHAR* tex;
			int l;
			_ASSERT(pend != NULL);

			tex = string::strstr(p, TEXT("diffuse="));
			if (tex != NULL && tex < pend)
			{
				for (l = 0; tex[8 + l] != ';' && tex[8 + l] != ']'; l++);
				diffuseMap = String(&tex[8], l);
			}

			tex = string::strstr(p, TEXT("spec="));
			if (tex != NULL && tex < pend)
			{
				for (l = 0; tex[5 + l] != ';' && tex[5 + l] != ']'; l++);
				specularMap = String(&tex[5], l);
			}

			tex = string::strstr(p, TEXT("norm="));
			if (tex != NULL && tex < pend)
			{
				for (l = 0; tex[5 + l] != ';' && tex[5 + l] != ']'; l++);
				normalMap = String(&tex[5], l);
			}
		}

		if ((m_Flags&_NOMATERIAL) && n != 0)
			continue;

		int offset = 0;

		while (offset < numface)
		{
			std::vector <unsigned short> faceidx;

			polysoup.ResetVertex();

			for (i = offset; i<numface; i++)
			{
				if (polysoup.CountVertex() > 65535 - 3)
					break;

				if (!(m_Flags&_NOMATERIAL) && face[i][0] != n)
					continue;

				for (int j = 0; j<3; j++)
				{
					int off = (m_Flags&_REVERSE_WIND) ? (2 - j) : j;
					unsigned short idx;

					if (!(m_Flags&_VERTEXONLY))
						idx = polysoup.FindVertex(face[i][1 + off], face[i][13 + off], face[i][4 + off], face[i][7 + off], face[i][10 + off]);
					else
						idx = polysoup.FindVertex(face[i][1 + off]);

					faceidx.push_back(idx);
				}
			}
			offset += i;

			if (faceidx.size() == 0)
				continue;

			Vector3 * vtx = new Vector3[polysoup.CountVertex()];
			Vector3 * norm = NULL;
			uint32 *diffuse = NULL;
			unsigned char(*skinv)[4] = NULL;
			unsigned char(*skinw)[4] = NULL;
			float(*uv1)[2] = NULL;
			float(*uv2)[2] = NULL;
			Vector4 * tan = NULL;

			std::vector<int> bonetable;

			polysoup.GetVertexPosition(vtx);

			if (!(m_Flags&_VERTEXONLY))
			{
				norm = new Vector3[polysoup.CountVertex()];
				polysoup.GetVertexNormal(norm);

				if (polysoup.GetFlags()&CPolygonSoup::_COLOR)
				{
					diffuse = new uint32[polysoup.CountVertex()];
					polysoup.GetColor(diffuse);
				}

				if (polysoup.GetFlags()&CPolygonSoup::_TEXCOORD1)
				{
					uv1 = new float[polysoup.CountVertex()][2];
					polysoup.GetTexcoord(0, uv1);
				}

				if (polysoup.GetFlags()&CPolygonSoup::_TEXCOORD2)
				{
					uv2 = new float[polysoup.CountVertex()][2];
					polysoup.GetTexcoord(1, uv2);
				}

				if ((polysoup.GetFlags()&CPolygonSoup::_TEXCOORD1) && (m_Flags&_CALC_TANGENT))
				{
					tan = new Vector4[polysoup.CountVertex()];

					GenerateTangentSpace(tan, vtx, polysoup.CountVertex(), norm, uv1, &faceidx[0], (int)faceidx.size() / 3);
				}

				if ((m_Flags&_USE_SKIN) && (polysoup.GetFlags()&CPolygonSoup::_SKIN))
				{
					skinv = new unsigned char[polysoup.CountVertex()][4];
					skinw = new unsigned char[polysoup.CountVertex()][4];

					int(*SkinV)[4] = new int[polysoup.CountVertex()][4];
					float(*SkinW)[4] = new float[polysoup.CountVertex()][4];

					polysoup.GetSkin(SkinV, SkinW);

					for (i = 0; i<polysoup.CountVertex(); i++)
					{
						int j, cnt = 0;
						int totalw = 0;
						for (j = 0; j<4; j++)
						{
							unsigned char w = (unsigned char)(SkinW[i][j] * 255);
							int boneid = SkinV[i][j];
							if (boneid != -1 && w > 0)
							{
								std::vector <int>::iterator it = std::find(bonetable.begin(), bonetable.end(), boneid);
								int boneidx;

								if (it == bonetable.end())
								{
									boneidx = bonetable.size();
									bonetable.push_back(boneid);
								}
								else
								{
									boneidx = it - bonetable.begin();
								}

								skinv[i][cnt] = boneidx;
								skinw[i][cnt] = w;
								cnt++;

								totalw += w;
							}
						}
						for (; cnt<4; cnt++)
						{
							skinv[i][cnt] = 0;
							skinw[i][cnt] = 0;
						}
						skinw[i][0] += 255 - totalw;
					}

					delete[] SkinV;
					delete[] SkinW;
				}
			}

			_DESC desc;

			//		memset(&desc, 0, sizeof(desc));

			desc.name = name;
			desc.bonename = name;
			desc.vtx = vtx;
			desc.norm = norm;
			desc.diffuse = diffuse;
			desc.uv1 = uv1;
			desc.uv2 = uv2;
			desc.vtxnum = polysoup.CountVertex();
			desc.tan = tan;
#ifndef _TEST_SKIN
			desc.skinindex = skinv;
			desc.skinweight = skinw;
#else
			desc.skinindex = desc.skinweight = NULL;
#endif
			desc.face = &faceidx[0];
			desc.facen = (int)faceidx.size() / 3;
			desc.bonetable = !bonetable.empty() ? &bonetable[0] : NULL;
			desc.bonecount = bonetable.size();
			desc.diffuseMap = diffuseMap;
			desc.specularMap = specularMap;
			desc.normalMap = normalMap;
#if 1
			desc.shcoeff = NULL;
			desc.vtxao = NULL;

			// skinned mesh이기는 하지만, bonecount가 1이면, skeleton animation으로 전환한다.
			if (skinv != NULL && desc.bonecount == 1)
			{
				delete[] desc.skinindex;
				delete[] desc.skinweight;
				desc.skinindex = NULL;
				desc.skinweight = NULL;
				skinv = NULL;
				skinw = NULL;
			}

			if (m_Flags&_EXTRA_TRANSLUCENT)
			{
				_ASSERT(m_ExtraDepthIdx < (int)m_ExtraDepth.size());
				_ASSERT(m_ExtraDepth[m_ExtraDepthIdx].vtxnum == desc.vtxnum);
				_ASSERT(!string::strcmp(m_ExtraDepth[m_ExtraDepthIdx].name, desc.name));
				desc.shcoeff = m_ExtraDepth[m_ExtraDepthIdx].shcoeff;
				m_ExtraDepthIdx++;
			}

			if (m_Flags&_EXTRA_VERTEXAO)
			{
				_ASSERT(m_ExtraDepthIdx < (int)m_AOList.size());
				_ASSERT(m_AOList[m_ExtraDepthIdx].vtxnum == desc.vtxnum);
				_ASSERT(!string::strcmp(m_AOList[m_ExtraDepthIdx].name, desc.name));
				desc.vtxao = m_AOList[m_ExtraDepthIdx].ao;
				m_ExtraDepthIdx++;
			}
#endif

#if 1
			// 스킨 메쉬일경우 메쉬 분할...
			if (m_Flags&_USE_SKIN && (polysoup.GetFlags()&CPolygonSoup::_SKIN) && m_Flags&_OBJ_SPLIT &&
				desc.bonecount > m_SplitBoneCount)
			{
				_ASSERT(m_SplitBoneCount > 0);
				Split(desc, m_SplitBoneCount);
			}
			else
			{
				Insert(desc);
			}
#else
			Insert(desc);
#endif

			delete[] tan;

			delete[] uv2;
			delete[] uv1;
			delete[] diffuse;
			delete[] norm;
			delete[] vtx;

			if (skinv != NULL) delete[] skinv;
			if (skinw != NULL) delete[] skinw;
		}
	}
}

void CGeometry::Insert(const _DESC& desc)
{
	bool compress = false;
	CPrimitive * geom = new CPrimitive;

	uint32 fvf = 0;

	if (desc.vtx) fvf |= FVF_XYZ;
	if (desc.norm) fvf |= FVF_NORMAL;
	if (desc.diffuse) fvf |= FVF_DIFFUSE;
	if (desc.uv1) fvf |= FVF_TEX0;
	if (desc.uv2) fvf |= FVF_TEX1;
	if (desc.tan) fvf |= FVF_TANSPC;
	if (desc.skinindex) fvf |= FVF_SKIN_INDEX;
	if (desc.skinweight) fvf |= FVF_SKIN_WEIGHT;

	VertexBuffer* vb = CreateVertexBuffer();
	if (vb->Create(fvf, desc.vtxnum) == true)
	{
		vb->Fill(desc.vtx, desc.norm, desc.diffuse, desc.uv1, desc.uv2, desc.tan, desc.skinindex, desc.skinweight, NULL, NULL);
		geom->SetVertexBuffer(vb, desc.vtxnum, 0);
	}

	IndexBuffer* ib = CreateIndexBuffer();
	if (ib->Create(desc.facen * 3, (void*)desc.face) == true)
	{
		geom->SetIndexBuffer(ib, desc.facen);
	}

#if 0
	PlatformUtil::DebugOutFormat(TEXT("\nvertex\n"));
	for (int i = 0; i<desc.vtxnum; i++)
		PlatformUtil::DebugOutFormat(TEXT("\t%d: uv: %f %f tan: %f %f %f\n"), i, desc.uv1[i][0], desc.uv1[i][1], desc.tan[i].x, desc.tan[i].y, desc.tan[i].z, desc.tan[i].w);
#endif

	// 실제 데이터
	_GEOM m;

	m.primitive = geom;
	m.name = desc.name;
	m.bonename = desc.bonename;
	m.bonecount = desc.bonecount;
	m.bonetable = NULL;

	m.boneindex = FindBone(desc.bonename);

	if (desc.bonecount > 0)
	{
		m.bonetable = new int[desc.bonecount];
		memcpy(m.bonetable, &desc.bonetable[0], sizeof(int)*desc.bonecount);
	}

	if (m_Flags&_STOREMESH)
	{
		m.vtx = (Vector3*)Duplicate(desc.vtx, sizeof(Vector3)*desc.vtxnum);
		m.norm = (Vector3*)Duplicate(desc.norm, sizeof(Vector3)*desc.vtxnum);
		m.diffuse = (uint32*)Duplicate(desc.diffuse, sizeof(uint32)*desc.vtxnum);
		m.uv1 = (float(*)[2]) Duplicate(desc.uv1, sizeof(float) * 2 * desc.vtxnum);
		m.uv2 = (float(*)[2]) Duplicate(desc.uv2, sizeof(float) * 2 * desc.vtxnum);
		m.tan = (Vector4*)Duplicate(desc.tan, sizeof(Vector4)*desc.vtxnum);
		m.face = (unsigned short*)Duplicate(desc.face, sizeof(unsigned short)*desc.facen * 3);
	}

	m.vtxnum = desc.vtxnum;
	m.facen = desc.facen;

	m.diffuseMap = desc.diffuseMap;
	m.specularMap = desc.specularMap;
	m.normalMap = desc.normalMap;

	m_GeomList.push_back(m);
}

/*	1. 삼각형에 attach된 본의 리스트를 구한다. (sorting이 중요)
2. 같은 본 인덱스를 가진 triangle 들을 하나의 triangle group으로 묶는다. (파티션)
3. 파티션들을 붙이면서 하나의 오브젝트를 완성하자!
*/
struct _FACEGROUP
{
	std::vector <int> facelist;
	std::vector <int> bonelist;

	void Init(const unsigned short* faceidx, unsigned int numbone, int* boneidx)
	{
		unsigned int i;
		for (i = 0; i<3; i++)
			facelist.push_back(faceidx[i]);

		for (i = 0; i<numbone; i++)
			bonelist.push_back(boneidx[i]);
		std::sort(bonelist.begin(), bonelist.end());
	}

	bool Compare(unsigned int numbone, int* boneidx)
	{
		unsigned int i;

		for (i = 0; i<numbone; i++)
		{
			if (std::find(bonelist.begin(), bonelist.end(), boneidx[i]) == bonelist.end())
				return false;
		}
		return true;
	}

	void Insert(const unsigned short* faceidx)
	{
		for (unsigned i = 0; i<3; i++)
			facelist.push_back(faceidx[i]);
	}

	static bool Sort(_FACEGROUP* p0, _FACEGROUP* p1) { return p0->bonelist[1] < p1->bonelist[1]; }
};

struct _OBJECT
{
	std::vector <int> bonelist;
	std::vector <int> facelist;

	void Init(_FACEGROUP* p)
	{
		bonelist.insert(bonelist.end(), p->bonelist.begin(), p->bonelist.end());
		facelist.insert(facelist.end(), p->facelist.begin(), p->facelist.end());
	}

	bool Insert(_FACEGROUP* p, unsigned int MaxBoneCount)
	{
		std::vector<int> list;

		unsigned int i;
		for (i = 0; i<p->bonelist.size(); i++)
		{
			int boneidx = p->bonelist[i];

			// 현재 bonelist에 없는 본에 대한 새로운 bonelist를 만든다.
			if (std::find(bonelist.begin(), bonelist.end(), boneidx) == bonelist.end())
			{
				list.push_back(boneidx);
			}
		}

		if (list.size() + bonelist.size() > MaxBoneCount)
			return false;

		bonelist.insert(bonelist.end(), list.begin(), list.end());
		facelist.insert(facelist.end(), p->facelist.begin(), p->facelist.end());
		return true;
	}
};

void CGeometry::Split(const _DESC& desc, int MAXBONE)
{
	std::vector <_FACEGROUP*> grouplist;

	unsigned int i, j;

	for (i = 0; i<(unsigned int)desc.facen; i++)
	{
		/// 1. face 단위로 bone list

		std::vector <int> bonelist;

		for (j = 0; j<3; j++)
		{
			const int vertid = desc.face[i * 3 + j];

			// 현재 버텍스의 attach bone index가 bone list에 등록되어 있는지 체크
			for (unsigned int k = 0; k<4; k++)
			{
				// 원본 bonetable의 index를 가지고 본 id를 사용
				const int boneidx = desc.bonetable[desc.skinindex[vertid][k]];

				std::vector<int>::iterator it = std::find(bonelist.begin(), bonelist.end(), boneidx);
				if (it == bonelist.end())
					bonelist.push_back(boneidx);
			}
		}
		std::sort(bonelist.begin(), bonelist.end());

		/// 2. facegroup을 만든다.

		bool find = false;
		for (j = 0; j<grouplist.size(); j++)
		{
			if (grouplist[j]->Compare(bonelist.size(), &bonelist[0]) == true)
			{
				grouplist[j]->Insert(&desc.face[i * 3]);
				find = true;
				break;
			}
		}
		if (find == false)
		{
			_FACEGROUP* group = new _FACEGROUP;
			group->Init(&desc.face[i * 3], bonelist.size(), &bonelist[0]);
			grouplist.push_back(group);
		}
	}

	/// 3. facegroup을 묶어서 하나의 오브젝트로 만든다.

	std::vector <_OBJECT*> objectlist;
	{
		_OBJECT* obj = new _OBJECT;
		obj->Init(grouplist[0]);
		objectlist.push_back(obj);

		for (i = 1; i<grouplist.size(); i++)
		{
			if (obj->Insert(grouplist[i], MAXBONE) == false)
			{
				obj = new _OBJECT;
				obj->Init(grouplist[i]);
				objectlist.push_back(obj);
			}
		}
	}

	Vector3* pvtx;
	Vector3* pnorm;
	uint32* pdiffuse;
	float(*puv1)[2];
	float(*puv2)[2];
	Vector4 * ptan;
	unsigned char(*pskinindex)[4];
	unsigned char(*pskinweight)[4];
	float* pshcoeff;
	uint32 * pvtxao;
	unsigned short * pface;
	int * pbonetable;

	std::vector<int>::iterator it;

	for (i = 0; i<objectlist.size(); i++)
	{
		_OBJECT* obj = objectlist[i];

		_DESC subdesc;

		// geometry name은 split 된 거은 ##을 붙이자
		subdesc.name = (i == 0) ? desc.name : desc.name + string::format(TEXT("@%d"), i);
		subdesc.bonename = desc.name;

		subdesc.diffuseMap = desc.diffuseMap;
		subdesc.normalMap = desc.normalMap;
		subdesc.specularMap = desc.specularMap;

		subdesc.vtxnum = obj->facelist.size();
		subdesc.vtx = pvtx = (desc.vtx != NULL) ? new Vector3[subdesc.vtxnum] : NULL;
		subdesc.norm = pnorm = (desc.norm != NULL) ? new Vector3[subdesc.vtxnum] : NULL;
		subdesc.diffuse = pdiffuse = (desc.diffuse != NULL) ? new uint32[subdesc.vtxnum] : NULL;
		subdesc.uv1 = puv1 = (desc.uv1 != NULL) ? new float[subdesc.vtxnum][2] : NULL;
		subdesc.uv2 = puv2 = (desc.uv2 != NULL) ? new float[subdesc.vtxnum][2] : NULL;
		subdesc.tan = ptan = (desc.tan != NULL) ? new Vector4[subdesc.vtxnum] : NULL;
		subdesc.shcoeff = pshcoeff = (desc.shcoeff != NULL) ? new float[9 * subdesc.vtxnum] : NULL;
		subdesc.vtxao = pvtxao = (desc.vtxao != NULL) ? new uint32[subdesc.vtxnum] : NULL;
		subdesc.skinindex = pskinindex = (desc.skinindex != NULL) ? new unsigned char[subdesc.vtxnum][4] : NULL;
		subdesc.skinweight = pskinweight = (desc.skinweight != NULL) ? new unsigned char[subdesc.vtxnum][4] : NULL;

		subdesc.facen = subdesc.vtxnum / 3;
		subdesc.face = pface = new unsigned short[subdesc.vtxnum];

		subdesc.bonecount = obj->bonelist.size();
		subdesc.bonetable = pbonetable = new int[subdesc.bonecount];
		memcpy(pbonetable, &obj->bonelist[0], sizeof(int)*subdesc.bonecount);

		for (j = 0; j<obj->facelist.size(); j++)
		{
			pface[j] = j;

			int idx = obj->facelist[j];

			if (desc.vtx)		memcpy(&pvtx[j].x, &desc.vtx[idx].x, sizeof(Vector3));
			if (desc.norm)		memcpy(&pnorm[j].x, &desc.norm[idx].x, sizeof(Vector3));
			if (desc.diffuse)	memcpy(&pdiffuse[j], &desc.diffuse[idx], sizeof(uint32));
			if (desc.uv1)		memcpy(puv1[j], desc.uv1[idx], sizeof(float) * 2);
			if (desc.uv2)		memcpy(puv2[j], desc.uv2[idx], sizeof(float) * 2);
			if (desc.tan)		memcpy(&ptan[j].x, &desc.tan[idx].x, sizeof(Vector3));
			if (desc.shcoeff)	memcpy(&pshcoeff[j * 9], &desc.shcoeff[idx * 9], sizeof(float) * 9);
			if (desc.vtxao)		memcpy(&pvtxao[j], &desc.vtxao[idx], sizeof(uint32));
			if (desc.skinindex)
			{
				for (int k = 0; k<4; k++)
				{
					const int boneidx = desc.bonetable[desc.skinindex[idx][k]];

					it = std::find(obj->bonelist.begin(), obj->bonelist.end(), boneidx);
					_ASSERT(it != obj->bonelist.end());

					pskinindex[j][k] = it - obj->bonelist.begin();
					pskinweight[j][k] = desc.skinweight[idx][k];
				}
			}
		}

		Insert(subdesc);

		if (pvtx) delete[] pvtx;
		if (pnorm) delete[] pnorm;
		if (pdiffuse) delete[] pdiffuse;
		if (puv1) delete[] puv1;
		if (puv2) delete[] puv2;
		if (ptan) delete[] ptan;
		if (pshcoeff) delete[] pshcoeff;
		if (pvtxao) delete[] pvtxao;
		if (pskinindex) delete[] pskinindex;
		if (pskinweight) delete[] pskinweight;
		if (pface) delete[] pface;
		if (pbonetable) delete[] pbonetable;
	}

	for (i = 0; i<(int)objectlist.size(); i++)
		delete objectlist[i];
	objectlist.clear();

	for (i = 0; i<(int)grouplist.size(); i++)
		delete grouplist[i];
	grouplist.clear();

	m_Flags |= _SPLITMESH;
}
