#include "resourcecore.h"

//
ModelData* ModelLoader::LoadModel(const TCHAR* filename)
{
	return NULL;
}

///
void ModelData::_MESH::Construct()
{
	uint32 fvf = 0;

	if (vertices) fvf |= FVF_XYZ;
	if (normals) fvf |= FVF_NORMAL;
	if (colors) fvf |= FVF_DIFFUSE;
	if (uv1) fvf |= FVF_TEX0;
	if (uv2) fvf |= FVF_TEX1;
	if (tangents) fvf |= FVF_TANSPC;
	if (skinindex) fvf |= FVF_SKIN_INDEX;
	if (skinweight) fvf |= FVF_SKIN_WEIGHT;

	vertexBuffer = CreateVertexBuffer();
	if (vertexBuffer->Create(fvf, vertexCount) == true)
	{
		vertexBuffer->Fill(vertices, normals, colors, uv1, uv2,
			tangents, skinindex, skinweight, NULL, NULL);
	}
	else
	{
		_ASSERT(1);
	}

	indexBuffer = CreateIndexBuffer();
	indexBuffer->Create(faceCount * 3, faces);
}

void ModelData::_MESH::CalcTangent()
{
	if (tangents)
	{
		delete[] tangents;
	}

	tangents = new Vector4[vertexCount];

	// http://www.terathon.com/code/tangent.html
	Vector3 * tan1 = new Vector3[vertexCount];
	Vector3 * tan2 = new Vector3[vertexCount];

	memset(tan1, 0, vertexCount * sizeof(Vector3));
	memset(tan2, 0, vertexCount * sizeof(Vector3));

	uint32 i;
	for (i = 0; i < faceCount; i++)
	{
		// vertexIdx
		int i1 = faces[i * 3 + 0];
		int i2 = faces[i * 3 + 1];
		int i3 = faces[i * 3 + 2];

		/// position
		const Vector3& localV1 = vertices[i1];
		const Vector3& localV2 = vertices[i2];
		const Vector3& localV3 = vertices[i3];

		// compute tangents
		float x1 = localV2.x - localV1.x;
		float x2 = localV3.x - localV1.x;
		float y1 = localV2.y - localV1.y;
		float y2 = localV3.y - localV1.y;
		float z1 = localV2.z - localV1.z;
		float z2 = localV3.z - localV1.z;

		float v1u = uv1[i1][0];
		float v1v = uv1[i1][1];
		float v2u = uv1[i2][0];
		float v2v = uv1[i2][1];
		float v3u = uv1[i3][0];
		float v3v = uv1[i3][1];

		float s1 = v2u - v1u;
		float s2 = v3u - v1u;
		float t1 = v2v - v1v;
		float t2 = v3v - v1v;

		float l = (s1 * t2 - s2 * t1);

		// catch singularity
#if 1
		if (l == 0.0f)
			l = 0.0001f;
#else
		if (fabs(l) <= 1E-6f)
			continue;
#endif

		float r = 1.0f / l;
		Vector3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
		Vector3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	for (i = 0; i<vertexCount; i++)
	{
		const Vector3& nor = normals[i];
		const Vector3& t = tan1[i];

		if (t == Vector3::ZERO)
		{
			tangents[i] = Vector4(1, 0, 0, 0);
		}
		else
		{
			// Gram-Schmidt orthogonalize
			Vector3 invTangent = t - nor * Vector3::DotProduct(nor, t);
			invTangent.Normalize();

			// Calculate handedness
			float h = Vector3::DotProduct(Vector3::CrossProduct(nor, t), tan2[i]) < 0.0f ? -1.0f : 1.0f;

			tangents[i] = Vector4(invTangent.x, invTangent.y, invTangent.z, h);
		}
	}

	delete[] tan2;
	delete[] tan1;
}
