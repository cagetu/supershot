#include "ikcontroller.h"
#include "bonecontroller.h"

CIKController::CIKController()
{
	m_Controller = NULL;
}

CIKController::~CIKController()
{
}

bool CIKController::SetBone(const wchar* rootbone, const wchar* endeffect, const Vec3 & vec)
{
	int root = m_Controller->Find(rootbone);
	int endeffector = m_Controller->Find(endeffect);

	if (root == -1 || endeffector == -1)
		return false;

	m_EndEffecter = endeffector;
	m_EndEffecterLocalPos = vec;

	return InsertBoneRecur(vec.x == 0 && vec.y == 0 && vec.z == 0 ? m_Controller->GetParent(endeffector) : endeffector, root);
}

bool CIKController::InsertBoneRecur(int boneIdx, int chainroot)
{
	if (boneIdx == -1)
		return false;

	if (boneIdx != chainroot)
		if (InsertBoneRecur(m_Controller->GetParent(boneIdx), chainroot) == false)
			return false;

	_JOINT jt;
	jt.pivot = Vec3::ZERO;
	jt.boneIndex = boneIdx;
	m_BoneList.push_back(jt);
	return true;
}

void CIKController::Apply(const Vec3& target, float blendweight, CBoneController* ctl)
{
	unsigned int i;

	if (m_BoneList.empty() || blendweight == 0.0f)
		return ;

	if (ctl == NULL)
		ctl = m_Controller;

	Vec3 endeffecter = ctl->GetWorld(m_EndEffecter).Transform(m_EndEffecterLocalPos);
	Mat4 tm = ctl->GetWorld(m_BoneList[0].boneIndex);

	for(i=0; i<m_BoneList.size(); i++)
	{
		if (i != 0)
			tm = ctl->GetLocal(m_BoneList[i].boneIndex) * tm;

		Mat4 invTM = tm.Inverse();
		Vec3 v1 = invTM.Transform(endeffecter);
		Vec3 v2 = invTM.Transform(target);
		Quat4 q = Quat4::MakeFromTo(Vec3::Normalize(v1), Vec3::Normalize(v2));
		Quat4 qq;

		ctl->GetRotation(m_BoneList[i].boneIndex, qq);

		if (blendweight < 1.0f)
		{
			Quat4 blendq = Quat4::Slerp(qq, Quat4::Multiply(q, qq), blendweight);
			ctl->SetLocalRotate(m_BoneList[i].boneIndex, blendq);
		}	else
		{
			ctl->SetLocalRotate(m_BoneList[i].boneIndex, Quat4::Multiply(q, qq));
		}

		tm = Mat4::Rotation(q) * tm;
		endeffecter = tm.Transform(v1);
	}
}