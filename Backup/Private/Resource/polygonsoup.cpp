#include "resourcecore.h"

CPolygonSoup::CPolygonSoup()
{
	m_Flags = 0;
}

void CPolygonSoup::SetPosition(Vector3 *v, int vtxnum)
{
	m_Flags |=_POSITION;

	for(int i=0; i<vtxnum; i++)
		vtx.push_back(v[i]);
}

void CPolygonSoup::SetPosition(Vector3 *v, int (* skinV)[4], float (*skinW)[4], int vtxnum)
{
	m_Flags |=_POSITION | _SKIN;

	for(int i=0; i<vtxnum; i++)
	{
		vtx.push_back(v[i]);

		SKIN skin;
		memcpy(skin.SkinV, skinV[i], sizeof(int)*4);
		memcpy(skin.SkinW, skinW[i], sizeof(float)*4);
		skinvtx.push_back(skin);
	}
}

void CPolygonSoup::SetNormal(Vector3* n, int num)
{
	m_Flags |= _NORMAL;

	for(int i=0; i<num; i++)
		normal.push_back(n[i]);
}

void CPolygonSoup::SetColor(unsigned char (*vertcolor)[3], int colornum)
{
	m_Flags |= _COLOR;

	for(int i=0; i<colornum; i++)
		color.push_back(0xFF000000 | (vertcolor[i][0]) | (vertcolor[i][1]<<8) | (vertcolor[i][2]<<16));
}

void CPolygonSoup::SetTexcoord(int ch, float (*uv)[2], int num)
{
	_ASSERT(ch == 0 || ch == 1);
	m_Flags |= ch == 0 ? _TEXCOORD1 : _TEXCOORD2;

	for(int i=0; i<num; i++)
	{
		TECOORD c;
		c.uv[0] = uv[i][0];
		c.uv[1] = uv[i][1];
		texcoord[ch].push_back(c);
	}
}

void CPolygonSoup::Insert(const Vector3 *v, int vtxnum, const unsigned short* face, int facen)
{
	unsigned int off = vtx.size();
	int i;

	m_Flags |= _POSITIONONLY | _MERGED;

	for(i=0; i<vtxnum; i++)
		vtx.push_back(v[i]);

	for (i=0; i<facen*3; i++)
		tri.push_back(face[i] + off);
}

void CPolygonSoup::ResetVertex()
{
	vertex.clear();
	vertextable.clear();
}

int CPolygonSoup::FindVertex(unsigned short vtxidx, unsigned short normal, unsigned short vtxcoloridx, unsigned short uv1idx, unsigned short uv2idx)
{
	_ASSERT(vtxidx < vtx.size());
	VERTEX v;
	v.vtxidx = vtxidx;
	v.vtxcoloridx = vtxcoloridx;
	v.uv1idx = uv1idx;
	v.uv2idx = uv2idx;
	v.normal = normal;
	int hash = vtxidx + vtxcoloridx + uv1idx + uv2idx + normal;
	for(std::multimap <int, int>::iterator it=vertextable.lower_bound(hash); it!=vertextable.upper_bound(hash); it++)
	{
		if (!memcmp(&v, &vertex[(*it).second], sizeof(VERTEX)))
			return (*it).second;
	}
	vertextable.insert(std::make_pair(hash, (int)vertex.size()));
	vertex.push_back(v);
	return vertex.size() - 1;
}

void CPolygonSoup::GetVertexPosition(Vector3* v)
{
	_ASSERT(m_Flags&_POSITION);
	for(unsigned int i=0; i<vertex.size(); i++)
		memcpy(&v[i], &vtx[vertex[i].vtxidx], sizeof(Vector3));
}

void CPolygonSoup::GetVertexNormal(Vector3* v)
{
	_ASSERT(m_Flags&_NORMAL);
	for(unsigned int i=0; i<vertex.size(); i++)
		memcpy(&v[i], &normal[vertex[i].normal], sizeof(Vector3));
}

void CPolygonSoup::GetColor(uint32* c)
{
	_ASSERT(m_Flags&_COLOR);
	for(unsigned int i=0; i<vertex.size(); i++)
		memcpy(&c[i], &color[vertex[i].vtxcoloridx], sizeof(uint32));
}

void CPolygonSoup::GetTexcoord(int ch, float (*uv)[2])
{
	_ASSERT(m_Flags&(_TEXCOORD1|_TEXCOORD2));
	for(unsigned int i=0; i<vertex.size(); i++)
		memcpy(&uv[i], &texcoord[ch][ch == 0 ? vertex[i].uv1idx : vertex[i].uv2idx], sizeof(float)*2);
}

void CPolygonSoup::GetSkin(int (*skinV)[4], float (*skinW)[4])
{
	_ASSERT(m_Flags&_SKIN);
	for(unsigned int i=0; i<vertex.size(); i++)
	{
		memcpy(&skinV[i], &skinvtx[vertex[i].vtxidx].SkinV, sizeof(int)*4);
		memcpy(&skinW[i], &skinvtx[vertex[i].vtxidx].SkinW, sizeof(float)*4);
	}
}
