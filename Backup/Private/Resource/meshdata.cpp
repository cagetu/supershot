#include "resourcecore.h"


CMeshData::CMeshData()
{
	m_GUID = 0;

	m_BoneNameTable = NULL;
	m_BoneParentTable = NULL;
	m_BoneInvTMTable = NULL;

	m_Flags = 0;
}

CMeshData::~CMeshData()
{
	for (auto it : m_GeomList)
	{
		delete[] it.vtx;
		delete[] it.norm;
		delete[] it.diffuse;
		delete[] it.uv1;
		delete[] it.uv2;
		delete[] it.tan;
		delete[] it.face;
		delete[] it.bonetable;

		delete it.primitive;
	}

	if (m_BoneNameTable != NULL)
		delete[] m_BoneNameTable;
	if (m_BoneParentTable != NULL)
		delete[] m_BoneParentTable;
	if (m_BoneInvTMTable != NULL)
		delete[] m_BoneInvTMTable;
}

template <class _Tx> class CIndexManager
{
public:
	unsigned short insert(const _Tx & tx, int key)
	{
		std::multimap <int, int>::const_iterator it;

		for (it = table.lower_bound(key); it != table.upper_bound(key); it++)
		{
			if (list[(*it).second] == tx)
				return (*it).second;
		}

		table.insert(std::make_pair(key, (int)list.size()));
		list.push_back(tx);

		return (unsigned short)list.size() - 1;
	}
	unsigned short count() const { return (unsigned short)list.size(); }
	const _Tx & operator [] (int idx) { return list[idx]; }

private:
	std::vector <_Tx> list;
	std::multimap <int, int> table;
};

unsigned int CMeshData::GetGeomCount() const
{
	return m_GeomList.size();
}

const CMeshData::_GEOM * CMeshData::FindGeom(const TCHAR* name, int index) const
{
	std::vector <_GEOM>::const_iterator it;
	for (it = m_GeomList.begin(); it != m_GeomList.end(); it++)
	{
		if (name == NULL || !string::strcmp(name, (*it).name))
		{
			if (index-- == 0)
				return &(*it);
		}
	}
	return NULL;
}

const CMeshData::_GEOM * CMeshData::FindGeom(int index) const
{
	return &m_GeomList[index];
}

int CMeshData::FindBone(const TCHAR * bonename) const
{
	if (m_BoneTable.empty())
	{
		std::vector <_BONE>::const_iterator it = std::find<std::vector <_BONE>::const_iterator>(m_BoneList.begin(), m_BoneList.end(), bonename);
		return it != m_BoneList.end() ? it - m_BoneList.begin() : -1;
	}
	else
	{
		std::map <String, int>::const_iterator it = m_BoneTable.find(bonename);
		return it != m_BoneTable.end() ? (*it).second : -1;
	}
}

int CMeshData::FindSplit(const TCHAR* name, String* list)
{
	if (!(m_Flags&_SPLITMESH))
		return 0;

	if (name == NULL || list == NULL)
		return 0;

	int count = 0;

	std::vector <_GEOM>::const_iterator it;
	for (it = m_GeomList.begin(); it != m_GeomList.end(); it++)
	{
		if (!string::strcmp(name, (*it).bonename) && (*it).name != (*it).bonename)
		{
			list[count++] = (*it).name;
		}
	}
	return count;
}

Ptr<CBoneController> CMeshData::CreateBoneController()
{
	if (m_BoneParentTable != NULL)
	{
		Ptr<CBoneController> controller = new CBoneController(*this);
		for (unsigned int i = 0; i<m_BoneList.size(); i++)
			controller->SetLocalTM(i, m_BoneList[i].localTM);
		return controller;
	}
	return NULL;
}

const Mat4&	CMeshData::GetBoneWorldTM(int h) const
{
	return m_BoneList[h].worldTM;
}
