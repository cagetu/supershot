#include "resourcecore.h"

CBoneController::CBoneController(const CMeshData& geom) 
	: m_Geom(geom)
{
	m_ParentTable = geom.GetBoneParentTable();
	m_WorldTM = Mat4::IDENTITY;

	m_BoneCount = geom.GetBoneCount();
	m_LocalTM = new Mat4 [m_BoneCount*2];
	m_BoneTM = &m_LocalTM[m_BoneCount];
	m_DeltaTM = Mat4::IDENTITY;

	Reset();
}

CBoneController::CBoneController(const CBoneController& ctl) 
	: m_Geom(ctl.m_Geom)
{
	m_ParentTable = ctl.m_ParentTable;
	m_WorldTM = Mat4::IDENTITY;

	m_BoneCount = ctl.m_BoneCount;
	m_LocalTM = new Mat4 [m_BoneCount*2];
	m_BoneTM = &m_LocalTM[m_BoneCount];
	m_DeltaTM = Mat4::IDENTITY;

	Reset();
}

CBoneController::~CBoneController()
{
	delete [] m_LocalTM;
}

void CBoneController::Reset()
{
	for(int i=0; i<m_BoneCount; i++)
	{
		m_BoneTM[i] = m_Geom.GetBoneWorldTM(i);
		m_LocalTM[i] = m_ParentTable[i] == -1 ? m_BoneTM[i] : m_BoneTM[i] * m_Geom.GetBoneInvTM(m_ParentTable[i]) ;
	}
}

void CBoneController::Normalize()
{
}

void CBoneController::Update()
{
	for(int i=0; i<m_BoneCount; i++)
		GetWorldTM(i);
}

void CBoneController::SetWorldTM(int h, const Mat4 & tm)
{
	SetLocalTM(h, tm * GetWorldTM(GetParent(h)).Inverse());
}

void CBoneController::SetLocalTM(int h, const Mat4 & tm)
{ 
	m_LocalTM[h] = tm;
}

void CBoneController::SetLocalTM(int h, const Quat4& q)
{ 
	m_LocalTM[h] = Mat4::Make(q, m_LocalTM[h].Position());
}

void CBoneController::SetLocalTM(int h, const Quat4& q, const Vector3 &p, float blendweight)
{
	m_LocalTM[h] = Mat4::Make(Quat4::Slerp(m_LocalTM[h].GetRotation(), q, blendweight), Vector3::Lerp(m_LocalTM[h].Position(), p, blendweight));
}

void CBoneController::SetDeltaTM(const Mat4& tm)
{ 
	m_DeltaTM = tm;
}

void CBoneController::GetLocalRotation(int h, OUT Quat4& q) const
{ 
	q = m_LocalTM[h].GetRotation();
}

Quat4 CBoneController::GetLocalRotation(int h) const
{ 
	return m_LocalTM[h].GetRotation();
}

const Vector3& CBoneController::GetLocalPosition(int h) const
{ 
	return m_LocalTM[h].Position();
}

int CBoneController::GetParent(int h) const
{
	return m_ParentTable[h];
}

const Mat4& CBoneController::GetWorldTM(int h) const
{
	int parent = GetParent(h);
	m_BoneTM[h] = parent == -1 ? GetLocalTM(h) * m_DeltaTM * m_WorldTM : GetLocalTM(h) * GetWorldTM(parent);
	return m_BoneTM[h];
}

void CBoneController::GetTM(int bonehandle, Mat4& tm) const
{
	tm = m_Geom.GetBoneInvTM(bonehandle) * m_BoneTM[bonehandle];
}

const Mat4& CBoneController::GetLocalTM(int h) const
{
	return m_LocalTM[h];
}

const TCHAR* CBoneController::GetBoneName(int h) const
{
	return m_Geom.GetBoneName(h);
}

int CBoneController::GetBoneCount() const
{
	return m_BoneCount; 
}

BONEHANDLE CBoneController::FindBone(const TCHAR * boneName) const
{
	return m_Geom.FindBone(boneName);
}

void CBoneController::Apply(const CBoneController& bonectl, float w, int *list, int listnum)
{
	_ASSERT(&m_Geom == &bonectl.m_Geom);
	if (list == NULL)
	{
		if (w >= 1.0f)
		{
			for(int i=0; i<m_BoneCount; i++)
				SetLocalTM(i, bonectl.GetLocalTM(i));
		}
		else if (w > 0)
		{
			for(int i=0; i<m_BoneCount; i++)
				SetLocalTM(i, bonectl.GetLocalRotation(i), bonectl.GetLocalPosition(i), w);
		}
	}
	else
	{
		if (w >= 1.0f)
		{
			for(int i=0; i<listnum; i++)
				SetLocalTM(list[i], bonectl.GetLocalTM(list[i]));
		}
		else if (w > 0)
		{
			for(int i=0; i<listnum; i++)
				SetLocalTM(list[i], bonectl.GetLocalRotation(list[i]), bonectl.GetLocalPosition(list[i]), w);
		}
	}
}

void CBoneController::ApplyDelta(CBoneController& postctl, CBoneController& pivot)
{
	_ASSERT(&m_Geom == &postctl.m_Geom);
	_ASSERT(&m_Geom == &pivot.m_Geom);

	for(int i=0; i<m_BoneCount; i++)
	{
//		Mat4 delta = postctl.GetWorld(i) * pivot.GetWorld(i).Inverse();
		Mat4 delta = postctl.GetLocalTM(i) * pivot.GetLocalTM(i).Inverse();
		SetLocalTM(i, delta * GetLocalTM(i));
	}
}
