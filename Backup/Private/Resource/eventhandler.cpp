﻿#include "resourcecore.h"

CEventHandler::CEventHandler()
{
}

bool CEventHandler::Open(CMiniXML::const_iterator node)
{
	if (node != NULL)
	{
		for (auto it = node.FindChild(TEXT("event")); it != NULL; it = it.Find(TEXT("event")))
			InsertEvent(it);
		std::sort(m_List.begin(), m_List.end());
	}
	return !m_List.empty() ? true : false;
}

void CEventHandler::InsertEvent(CMiniXML::const_iterator node)
{
	static const struct
	{
		const TCHAR* name;
		unsigned long flags;
	} _list[] =
	{
		TEXT("_SOUND"), _SOUND,
		TEXT("_EVENT"), _EVENT,
		TEXT("_PARTICLE"), _PARTICLE,
	} ;

	const TCHAR *mask = node[TEXT("mask")];
	unsigned long flags = 0;
	int i;

	for(i=0; i<_countof(_list); i++)
	{
		const TCHAR* p = string::strstr(mask, _list[i].name);
		if (p != NULL)
		{
			TCHAR endch = p[string::strlen(_list[i].name)];
			if (endch == '\0' || endch == ',' || endch == ';')
				flags |= _list[i].flags;
		}
	}

	EVENT evt;

	evt.time = string::atof(*node.FindChild(TEXT("TICK")));
	evt.param.flags = flags;

	if (flags&_SOUND)
		evt.param.eventparam = *node.FindChild(TEXT("SOUNDID"));

	if (flags&_EVENT)
		evt.param.eventparam = *node.FindChild(TEXT("EVENTID"));

	if (flags&_PARTICLE)
		evt.param.eventparam = *node.FindChild(TEXT("PARTICLEID"));

//	if (flags&_BONEBIND)
//		evt.param.bonebind = model->Find(*node.FindChild(TEXT("BONE")));

	m_List.push_back(evt); // event 추가!
}

void CEventHandler::Update(float pretime, float curtime, CEventListener* listener) const
{
	for (auto it : m_List)
	{
		if( it.time >= pretime && it.time < curtime )
			listener->OnExec(it.param);
	}
}