#include "scene.h"

void ViewFrustum::SetProjection(const Mat4 & camtm, float fovrad, float neardistance, float fardistance, float ratio, bool flip)
{
	static const int _face[6][4] =
	{
		0, 2, 3, 1, // -z
		1, 3, 7, 5, // +x
		3, 2, 6, 7, // +y
		2, 0, 4, 6, // -x
		0, 1, 5, 4, // -y
		7, 6, 4, 5, // +z
	} ;

	int i;
	float s = Math::tan(fovrad*0.5f);

	/*
		0: left, bottom, near
		1: right, bottom, near
		2: left, top, near
		3: right, top, near
		4: left, bottom, far
		5: right, bottom, far
		6: left, top, far
		7: right, top, far
	*/
	for(i=0; i<8; i++)
	{
		float d = (i&4) ? fardistance : neardistance;
		m_Corners[i] = Vector3((i&1) ? d * s / ratio : - d * s / ratio, (i&2) ? d * s : - d * s, d) * camtm;
	}

	for(i=0; i<6; i++)
		m_Plane[i] = Plane(m_Corners[_face[i][0]], m_Corners[_face[i][(flip?2:1)]], m_Corners[_face[i][(flip?1:2)]]);
}

void ViewFrustum::SetOrthogonal(const Mat4 & camtm, float width, float neardistance, float fardistance, float ratio, bool flip)
{
	static const int _face[6][4] =
	{
		2, 0, 4, 6, // -x
		1, 3, 7, 5, // +x
		0, 1, 5, 4, // -y
		3, 2, 6, 7, // +y
		0, 2, 3, 1, // -z
		7, 6, 4, 5, // +z
	} ;

	int i;

	for(i=0; i<8; i++)
	{
		float d = (i&4) ? fardistance : neardistance;
		m_Corners[i] = Vector3((i&1) ? width*0.5f : -width*0.5f, (i&2) ? width*0.5f*ratio : -width*0.5f*ratio, d) * camtm;
	}

	for(i=0; i<6; i++)
		m_Plane[i] = Plane(m_Corners[_face[i][0]], m_Corners[_face[i][(flip?2:1)]], m_Corners[_face[i][(flip?1:2)]]);
}

bool ViewFrustum::CullTest(const AABB & aabb) const
{
	for(int i=0; i<6; i++)
	{
		const Plane & p = m_Plane[i];
		Vector3 nearp(p.a > 0 ? aabb.minx : aabb.maxx, p.b > 0 ? aabb.miny : aabb.maxy, p.c > 0 ? aabb.minz : aabb.maxz);
		if (p.Distance(nearp) > 0)
			return false;
	}

	return true;
}

bool ViewFrustum::CullTest(const AABB & aabb, const Mat4 & tm) const
{
	Mat4 invtm = tm.Inverse();

	for(int i=0; i<6; i++)
	{
		Plane p = m_Plane[i].Transform(invtm);
		Vector3 nearp(p.a > 0 ? aabb.minx : aabb.maxx, p.b > 0 ? aabb.miny : aabb.maxy, p.c > 0 ? aabb.minz : aabb.maxz);
		if (p.Distance(nearp) > 0)
			return false;
	}

	return true;
}

bool ViewFrustum::CullTest(const Sphere & sphere) const
{
	for(int i=0; i<6; i++)
	{
		const Plane & p = m_Plane[i];
		if (p.Distance(sphere.Position()) > sphere.radius)
			return false;
	}

	return true;
}

bool ViewFrustum::CullTest(const AABB & aabb, bool& inbound) const
{
	inbound = true;

	for(int i=0; i<6; i++)
	{
		const Plane & p = m_Plane[i];
		Vector3 nearp(p.a > 0 ? aabb.minx : aabb.maxx, p.b > 0 ? aabb.miny : aabb.maxy, p.c > 0 ? aabb.minz : aabb.maxz);
		if (p.Distance(nearp) > 0)
			return false;

		if (inbound == true)
		{
			Vector3 farp(p.a < 0 ? aabb.minx : aabb.maxx, p.b < 0 ? aabb.miny : aabb.maxy, p.c < 0 ? aabb.minz : aabb.maxz);
			if (p.Distance(farp) > 0)
				inbound = false;
		}
	}

	return true;
}

int ViewFrustum::ClipPolygonRecur(const Vector3* v, int vn, Vector3* result, int seq) const
{
	if (seq >= 6)
	{
		PlatformUtil::Memcopy(result, v, sizeof(Vector3)*vn);
		return vn;
	}

	Vector3 vv[16];
	int n=0;
	float prevt = 0.0f;
	for(int i=0; i<=vn; i++)
	{
		float t = m_Plane[seq].Distance(v[i%vn]);
		if (i > 0)
		{
			if (prevt*t < 0.0f)
				vv[n++] = v[i-1] + (v[i%vn] - v[i-1]) * (prevt / (prevt - t));
			if (t <= 0)
				vv[n++] = v[i%vn];
		}
		prevt = t;
	}
	return n >= 3 ? ClipPolygonRecur(vv, n, result, seq+1) : 0;
}