#include "scene.h"

__ImplementRtti(VisualShock, SceneView, SceneEntity);

//
//
//
std::map<String, SceneView*> SceneView::m_CameraCache;

void SceneView::Initialize()
{
}

void SceneView::Shutdown()
{
	for (auto it = m_CameraCache.begin(); it != m_CameraCache.end(); it++)
		it->second->DecRef();
	m_CameraCache.clear();
}

Ptr<SceneView> SceneView::Create(const TCHAR* name)
{
	auto it = m_CameraCache.find(name);
	if (it != m_CameraCache.end())
	{
		return it->second;
	}

	SceneView* camera = new SceneView(name);
	camera->IncRef();
	m_CameraCache.insert(std::make_pair(name, camera));

	return camera;
}

void SceneView::Release(const Ptr<SceneView>& camera)
{
	int ref = camera->DecRef();

	auto it = m_CameraCache.find(camera->m_Name);
	if (it != m_CameraCache.end())
	{
		if (ref == 0)
			m_CameraCache.erase(it);
	}
}

///
///
///
SceneView::SceneView(const TCHAR* name)
	: m_Name(name)
	, m_CameraFOV(0.0f)
	, m_CameraNearClip(0.0f)
	, m_CameraFarClip(0.0f)
	, m_CameraAspectRatio(0.0f)
	, m_CameraOtrhoWidth(0.0f)
	, m_ProjectionMode(_PERSPECTIVE)
	, m_Flags(0)
{
}

SceneView::~SceneView()
{
}

bool SceneView::BeginScene()
{
	UpdateCamera();

	if (m_Flags&_NEED_UPDATE)
	{
		if (m_ProjectionMode == _PERSPECTIVE)
		{
			m_Frustum.SetProjection(m_CameraTM, m_CameraFOV, m_CameraNearClip, m_CameraFarClip, m_CameraAspectRatio, (m_Flags&_FLIP) ? true : false);
		}
		else if (m_ProjectionMode == _ORTHGONAL)
		{
			m_Frustum.SetOrthogonal(m_CameraTM, m_CameraOtrhoWidth, m_CameraNearClip, m_CameraFarClip, m_CameraAspectRatio, (m_Flags&_FLIP) ? true : false);
		}

		UpdateShader();

		m_Flags &= ~_NEED_UPDATE;
	}
	return true;
}

void SceneView::EndScene()
{
}

void SceneView::UpdateShader()
{
	_SHADERPARAMETER(ShaderParameter::CAMERA, m_CameraTM);
	_SHADERPARAMETER(ShaderParameter::VIEW, m_ViewTM);
	_SHADERPARAMETER(ShaderParameter::PROJECTION, m_ProjectionTM);
	_SHADERPARAMETER(ShaderParameter::CAMERADISTANCE, Vector4(m_CameraNearClip, m_CameraFarClip, m_CameraFarClip - m_CameraNearClip, 1.0f));
	_SHADERPARAMETER(ShaderParameter::CAMERAFOV, Vector4(m_CameraFOV, 1.0f / Math::tan(m_CameraFOV*0.5f), m_CameraAspectRatio, 1.0f));
	_SHADERPARAMETER(ShaderParameter::CAMERAPOSITION, m_CameraTM.Position());
	_SHADERPARAMETER(ShaderParameter::CAMERADIRECTION, m_CameraTM.GetRow2());

	// Frustum Corner Vertices
	{
		Vector3 corners[4];
		const Vector3* vertices = m_Frustum.GetVertices();
		for (int i = 0; i < 4; i++)
			corners[i] = vertices[4 + i] * m_ViewTM;

		_SHADERPARAMETER(ShaderParameter::FRUSTUMCORNERS, Variant(corners, 4));
	}
}

void SceneView::SetCamera(const Vector3& position, const Vector3& lookAt)
{
	m_CameraPosition = position;
	m_CameraLookAt = lookAt;

	m_Flags |= _NEED_UPDATE | _NEED_UPDATE_CAMERA;
}

void SceneView::SetPosition(const Vector3& position)
{
	m_CameraPosition = position;

	m_Flags |= _NEED_UPDATE | _NEED_UPDATE_CAMERA;
}

void SceneView::SetLookAt(const Vector3& lookAt)
{
	m_CameraLookAt = lookAt;

	m_Flags |= _NEED_UPDATE | _NEED_UPDATE_CAMERA;
}

void SceneView::UpdateCamera()
{
	if (m_Flags&_NEED_UPDATE_CAMERA)
	{
		m_CameraDir = Vector3::Normalize(m_CameraLookAt - m_CameraPosition);
		m_CameraTM = Transform::LookAt(m_CameraPosition, m_CameraLookAt);
		m_ViewTM = m_CameraTM.Inverse();

		m_Flags &= ~_NEED_UPDATE_CAMERA;
		m_Flags |= _NEED_UPDATE;
	}
}

const Vector3& SceneView::GetDirection()
{
	UpdateCamera();

	return m_CameraDir;
}

void SceneView::SetProjection(float fovrad, float neardistance, float fardistance, float aspectRatio, bool flip)
{
	m_CameraFOV = fovrad;
	m_CameraNearClip = neardistance;
	m_CameraFarClip = fardistance;
	m_CameraAspectRatio = aspectRatio;

	m_ProjectionTM = Transform::ProjectionWithFov(fovrad, neardistance, fardistance, aspectRatio);

	m_ProjectionMode = _PERSPECTIVE;
	m_Flags |= _NEED_UPDATE;
}

void SceneView::SetOrthogonal(float widthsize, float neardistance, float fardistance, float aspectRatio, bool flip)
{
	m_CameraOtrhoWidth = widthsize;
	m_CameraNearClip = neardistance;
	m_CameraFarClip = fardistance;
	m_CameraAspectRatio = aspectRatio;

	m_ProjectionTM = Transform::OrthoOffCenter(-widthsize*0.5f, widthsize*0.5f, -widthsize*0.5f*aspectRatio, widthsize*0.5f*aspectRatio, neardistance, fardistance);

	m_ProjectionMode = _ORTHGONAL;
	m_Flags |= _NEED_UPDATE;
}

void SceneView::SetViewport(float x, float y, float width, float height, float minZ, float maxZ)
{
	m_Viewport.x = x;
	m_Viewport.y = y;
	m_Viewport.width = width;
	m_Viewport.height = height;
	m_Viewport.minZ = minZ;
	m_Viewport.maxZ = maxZ;
}

bool SceneView::CullTest(const AABB& aabb)
{
	return m_Frustum.CullTest(aabb);
}

bool SceneView::CullTest(const Sphere& sphere)
{
	return m_Frustum.CullTest(sphere);
}
