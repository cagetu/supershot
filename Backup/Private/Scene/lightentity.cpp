#include "scene.h"

__ImplementRtti(VisualShock, LightEntity, SceneEntity);

LightEntity::LightEntity()
{

}
LightEntity::~LightEntity()
{
}

////
////
__ImplementRtti(VisualShock, DirectionLight, LightEntity);

DirectionLight::DirectionLight()
{

}
DirectionLight::~DirectionLight()
{

}

__ImplementRtti(VisualShock, PointLight, LightEntity);

PointLight::PointLight()
{

}
PointLight::~PointLight()
{

}

__ImplementRtti(VisualShock, SpotLight, LightEntity);

SpotLight::SpotLight()
{

}
SpotLight::~SpotLight()
{

}

__ImplementRtti(VisualShock, AmbientLight, LightEntity);

AmbientLight::AmbientLight()
{

}
AmbientLight::~AmbientLight()
{

}

__ImplementRtti(VisualShock, EnvironmentLight, LightEntity);

EnvironmentLight::EnvironmentLight()
{

}

EnvironmentLight::~EnvironmentLight()
{

}

__ImplementRtti(VisualShock, RimLight, LightEntity);

RimLight::RimLight()
{
	
}

RimLight::~RimLight()
{

}
