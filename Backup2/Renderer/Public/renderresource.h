#pragma once

class IRenderResource : public IObject
{
	__DeclareRtti;
public :
	IRenderResource();
	virtual ~IRenderResource();
};

///

class IShader;
class CMaterial;
class VertexBuffer;
class IndexBuffer;
class ITexture;
class ITexture2D;
class IRenderTexture2D;
class IRenderTarget;
class IRenderTargetSurface;
class IDepthStencilSurface;

///
IShader* CreateShader();
VertexBuffer* CreateVertexBuffer();
IndexBuffer* CreateIndexBuffer();

ITexture2D* CreateFileTexture();
IRenderTexture2D* CreateRenderTexture();

IRenderTarget* CreateRenderTarget();

IRenderTexture2D* CreateRenderTargetSurface();
IDepthStencilSurface* CreateDepthStencilSurface();