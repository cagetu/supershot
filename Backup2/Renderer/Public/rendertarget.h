#pragma once

class IRenderTarget : public IRenderResource
{
	__DeclareRtti;
public:
	IRenderTarget();
	virtual ~IRenderTarget();

	virtual bool Create(int32 width, int32 height, int32 multisample = 1) ABSTRACT;

	virtual void Begin() ABSTRACT;
	virtual void End() ABSTRACT;

public:
	// RenderSurface
	virtual IRenderTexture2D* AddRenderTexture(int32 format, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore) ABSTRACT;
	virtual IRenderTexture2D* GetRenderTexture(int32 channel) const ABSTRACT;
	virtual IRenderTexture2D* GetRenderTexture(const TCHAR* id) const ABSTRACT;
	virtual void ClearRenderTextures() ABSTRACT;

	// DepthSurface
	virtual void CreateDepthBuffer(int32 format, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore, RTLoadAction stencilLoadAction = RTLoadAction::ENoAction, RTStoreAction stencilStoreAction = RTStoreAction::ENoAction) ABSTRACT;
	virtual void SetDepthBuffer(const Ptr<IDepthStencilSurface>& surface, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::EStore, RTLoadAction stencilLoadAction = RTLoadAction::ENoAction, RTStoreAction stencilStoreAction = RTStoreAction::ENoAction) ABSTRACT;
	virtual const Ptr<IDepthStencilSurface>& GetDepthBuffer() const ABSTRACT;

public:
	int32 GetWidth() const { return m_Width; }
	int32 GetHeight() const { return m_Height; }

	void SetClearMode(int32 flags) { m_ClearFlags = flags; }
	void SetClearColor(COLOR color) { m_ClearColor = color; }
	void SetClearDepth(float depth) { m_ClearDepth = depth; }
	void SetClearStencil(ushort stencil) { m_ClearStencil = stencil; }

protected:
	int32 m_Width;
	int32 m_Height;

	/// Mipmap
	int32 m_MipmapLevel;
	int32 m_MipmapCount;

	/// MSAA
	int32 m_Multisample;

	///
	uint32 m_ClearFlags;
	COLOR m_ClearColor;
	float m_ClearDepth;
	ushort m_ClearStencil;
};