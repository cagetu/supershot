#pragma once

class RenderTargetPool;
class RenderPass;

class RenderPipeline
{
public :
	RenderPipeline();
	~RenderPipeline();

	static void Initialize();
	static void Shutdown();

	bool Create(const TCHAR* filename);
	void Destroy();

	void Update();
	void Render();

public:
	class RenderCallback
	{
	public:
		virtual void Callback(const TCHAR* passName) ABSTRACT;
	};
	
	void SetCallback(RenderCallback* callback);

	struct _RENDERTARGETINFO
	{
		int width;
		int height;
		std::vector <String> texturename;
		std::vector <uint32> textureformat;
		int depthType;
		int mipmapLevel;
		int multiSamples;
	};
private :
	friend class RenderPass;

	IRenderTarget* CreateRenderTarget(int width, int height, const TCHAR* name[4], uint32 format[4], int mipmapLevel = 0, int multisamples = 0);
	IRenderTexture2D* GetRenderTexture(const TCHAR* name) const;
	RenderCallback* GetRenderCallback() const;
	
	Ptr<IDepthStencilSurface> CreateDepthBuffer(const TCHAR* name, int width, int height, int format, int multisamples = 0);
	Ptr<IDepthStencilSurface> GetDepthBuffer(const TCHAR* name) const;

	void Reset();

private:
	std::vector <RenderPass*> m_RenderPasses;

	std::map <String, IRenderTexture2D*> m_RenderTextures;
	std::map <String, IDepthStencilSurface*> m_DepthBuffers;

	RenderCallback* m_RenderCallback;
};

/*
	- ���� �н� ����
	- ���� Ÿ�� ����

*/