#pragma once

struct D3D11Transform : public CommonTransform
{
	Mat4 Multiply(const Mat4 & a, const Mat4 & b);
	Mat4 OrthoOffCenter(float l, float r, float b, float t, float zn, float zf);
	Vector4 Transform(const Mat4 & m, const Vector4 & p);

	void Decompose(const Mat4& input, Vector3& translate, Quat4& rotate, Vector3& scale);
};

typedef D3D11Transform	DeviceTransform;

