#pragma once

/*	Viewport
	- 무엇인가를 렌더링하기 위한 화면을 의미한다.
*/
class IViewport : public IObject
{
	__DeclareRtti;
public:
	struct Extent
	{
		float x;
		float y;
		float width;
		float height;
		float minZ;
		float maxZ;

		Extent() : x(0.0f), y(0.0f), width(1.0f), height(1.0f), minZ(0.0f), maxZ(1.0f) {}
		Extent(float _x, float _y, float _width, float _height, float _minZ = 0.0f, float _maxZ = 1.0f) : x(_x), y(_y), width(_width), height(_height), minZ(_minZ), maxZ(_maxZ) {}
	};
	typedef Extent RelativeExtent;
	typedef Extent AbsoluteExtent;

	enum 
	{
		_SWAPCHAIN		= 1 << 0,
		_PRESENT		= 1 << 1,
		_MAINVIEWPORT	= 1 << 2,
		_FULLSCREEN		= 1 << 3,
		_DEPTHBUFFER	= 1 << 4,
	};

public:
	IViewport();
	virtual ~IViewport();

	virtual bool Create(void* windowHandle, int32 width, int32 height, int32 format, uint32 featureFlag, const RelativeExtent& extent = Extent()) = 0;
	virtual void Destroy() = 0;

	virtual void Begin() = 0;
	virtual void End() = 0;

	virtual void Present() = 0;

	virtual void Resize(int32 width, int32 height, int32 format, bool fullscreen = false) = 0;

public:
	int32 GetWidth() const { return m_Width; }
	int32 GetHeight() const { return m_Height; }
	int32 GetFormat() const { return m_PixelFormat; }

	const RelativeExtent& GetRelativeExtent() const { return m_RelativeExtent; }

protected:
	void* m_WindowHandle;

	RelativeExtent m_RelativeExtent;
	int32 m_Width;
	int32 m_Height;
	int32 m_PixelFormat;

	unsigned long m_Flags;
};
