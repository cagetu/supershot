#pragma once

class GLESIndexBuffer : public IndexBuffer
{
public:
	GLESIndexBuffer();
	virtual ~GLESIndexBuffer();

	bool Create(int32 indexCount, int32 stride = sizeof(unsigned short)) OVERRIDE;
	bool Create(int32 indexCount, void* indexData, int32 stride = sizeof(unsigned short)) OVERRIDE;
	void Destroy() OVERRIDE;

	unsigned char* GetBytes() const OVERRIDE;

	void* Lock(int32 length = 0, int32 lockFlag = 0) OVERRIDE;
	void  Unlock(ushort baseVertIdx = -1) OVERRIDE;

	int SetIndices() OVERRIDE;

	bool IsIBO() const { return m_IsIBO; }
private:
	bool CreateBuffer(int32 byteSize, uint32 usage);

	GLuint m_Buffer;
	bool m_IsIBO;
};
