#pragma once

class GLESVertexDeclaration : public IVertexDeclaration
{
	__DeclareRtti;
public :
	GLESVertexDeclaration();
	virtual ~GLESVertexDeclaration();

	bool Create(VertexDescription* inVertexDesc) OVERRIDE;

private:
	VertexDescription* m_VertexDesc;
};
