#pragma once

#ifndef _OPENGL_ES2
#define _OPENGL_ES2	1
#endif
#ifndef _OPENGL_ES3
#define _OPENGL_ES3	0
#endif

class GLESShader;
class GLESRenderTarget;
class GLESVertexBuffer;
class GLESIndexBuffer;
class GLESRenderState;
class GLESViewport;

#include "openglesdriver.h"
#include "openglesext.h"
#include "openglestransform.h"

#if defined(TARGET_OS_WINDOWS)
	#include "Windows/windowopengles.h"
#elif defined(TARGET_OS_ANDROID)
	#include "Android/androidopengles.h"
#elif defined(TARGET_OS_IPHONE)
	#include "iOS/iosopengles.h"
#endif

#include "openglesviewport.h"
#include "openglesvertexformat.h"
#include "openglesvertexbuffer.h"
#include "openglesindexbuffer.h"
//#include "openGLESVertexDescription.h"

#include "openglesrenderstate.h"

#include "openglestexture.h"
#include "openglessurface.h"
#include "openglesrendertarget.h"

#include "openglesshader.h"
#include "openglesshadercompiler.h"
