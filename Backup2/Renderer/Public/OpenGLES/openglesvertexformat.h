#pragma once

struct GLESVertexDescription : public IVertexDescription
{
	// Vertex Declaration 에서 하는 일을 여기서 한다...
	uint32 fvf;
	uint32 size;
	int32 locations[16];

	VertexDesc() { Clear(); }
	void Clear() { fvf = 0; size = 0; Memory::Memzero(locations); }

	static void GetDesc(GLuint program, VertexDesc* output);
};

void BindShaderVertexDesc(const GLESVertexDescription& vertexDesc, uint32 vertexFormat, uint vertexStride, unsigned char* vertexElements = NULL);
void UnbindShaderVertexDesc(const GLESVertexDescription& vertexDesc);

typedef GLESVertexDescription VertexDescription;