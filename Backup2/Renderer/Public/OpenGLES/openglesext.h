#ifndef _opengles_extension_
#define _opengles_extension_

struct COpenGLESCaps
{
	static void Setup() {};

	static bool IsGLExtensionSupported(const char* extension);

	static FORCEINLINE int GetMaxVertexAttributes() { return MaxVertexAttribs; }

	static bool IsSupportMapBuffer() { return bSupportMapBuffer; }
	static bool IsSupportDepthTexture() { return bSupportDepthTexture; }
	static bool IsSupportOcclusionQueries() { return bSupportOcclusionQueries; }

	static bool IsSupportDisjointTimeQueries() { return bSupportDisjointTimeQueries; }
	static bool IsTimerQueryCanBeDisjoint() { return bTimerQueryCanBeDisjoint; }
	static bool IsSupportRGBA8() { return bSupportRGBA8; }
	static bool IsSupportRGBA8888() { return bSupportBGRA8888; }
	static bool IsSupportBGRA8888RenderTarget() { return bSupportBGRA8888RenderTarget; }

	static bool IsSupportVertexHalfFloat() { return bSupportVertexHalfFloat; }
	static bool IsSupportTextureFloat() { return bSupportTextureFloat; }
	static bool IsSupportTextureHalfFloat() { return bSupportTextureHalfFloat; }
	static bool IsSupportRGB() { return bSupportSGRB; }

	static bool IsSupportMultisampledRenderToTexture() { return bSupportMultisampledRenderToTexture; }

	static bool IsSupportColorBufferFloat() { return bSupportColorBufferFloat; }
	static bool IsSupportColorBufferHalfFloat() { return bSupportColorBufferHalfFloat; }

	static bool IsSupportShaderFramebufferFetch() { return bSupportShaderFramebufferFetch; }
	static bool IsSupportShaderDepthStencilFetch() { return bSupportShaderDepthStencilFetch; }

	static bool IsSupportDXT() { return bSupportDXT; }
	static bool IsSupportPVRTC() { return bSupportPVRTC; }
	static bool IsSupportATITC() { return bSupportATITC; }
	static bool IsSupportETC1() { return bSupportETC1; }
	static bool IsSupportVertexArrayObjects() { return bSupportVertexArrayObjects; }
	static bool IsSupportDiscardFrameBuffer() { return bSupportDiscardFrameBuffer; }
	static bool IsSupportNVFrameBufferBlit() { return bSupportNVFrameBufferBlit; }
	static bool IsSupportPackedDepthStencil() { return bSupportPackedDepthStencil; }
	static bool IsSupportShaderTextureLod() { return bSupportShaderTextureLod; }
	static bool IsSupportTextureStorageEXT() { return bSupportTextureStorageEXT; }
	static bool IsSupportCopyTextureLevels() { return bSupportCopyTextureLevels; }
	static bool IsSupportTextureNPOT() { return bSupportTextureNPOT; }
	static bool IsSupportRGB10A2() { return bSupportRGB10A2; }
	static bool IsSupportStandardDerivativesExtension() { return bSupportStandardDerivativesExtension; }

	static int GetShaderLowPrecision() { return ShaderLowPrecision; }
	static int GetShaderMediumPrecision() { return ShaderMediumPrecision; }
	static int GetShaderHighPrecision() { return ShaderHighPrecision; }

protected:
	static void CheckDriverCaps();

	static int MaxVertexAttribs;
	static int MaxTextureImageUnits;
	static int MaxCombinedTextureImageUnits;
	static int MaxVertexTextureImageUnits;
	static int MaxGeometryTextureImageUnits;
	static int MaxHullTextureImageUnits;
	static int MaxDomainTextureImageUnits;
	static int MaxVertexUniformComponents;
	static int MaxPixelUniformComponents;
	static int MaxGeometryUniformComponents;
	static int MaxHullUniformComponents;
	static int MaxDomainUniformComponents;
	static int MaxVaryingVectors;

	static bool bSupportMapBuffer;
	static bool bSupportDepthTexture;
	static bool bSupportOcclusionQueries;
	static bool bSupportDisjointTimeQueries;
	static bool bTimerQueryCanBeDisjoint;
	static bool bSupportRGBA8;
	static bool bSupportBGRA8888;
	static bool bSupportBGRA8888RenderTarget;

	static bool bSupportVertexHalfFloat;
	static bool bSupportTextureFloat;
	static bool bSupportTextureHalfFloat;
	static bool bSupportSGRB;

	static bool bSupportMultisampledRenderToTexture;

	static bool bSupportColorBufferFloat;
	static bool bSupportColorBufferHalfFloat;

	static bool bSupportShaderFramebufferFetch;
	static bool bSupportShaderDepthStencilFetch;

	static bool bSupportDXT;
	static bool bSupportPVRTC;
	static bool bSupportATITC;
	static bool bSupportETC1;
	static bool bSupportVertexArrayObjects;
	static bool bSupportDiscardFrameBuffer;
	static bool bSupportNVFrameBufferBlit;
	static bool bSupportPackedDepthStencil;
	static bool bSupportShaderTextureLod;
	static bool bSupportTextureStorageEXT;
	static bool bSupportCopyTextureLevels;
	static bool bSupportTextureNPOT;
	static bool bSupportRGB10A2;
	static bool bSupportStandardDerivativesExtension;

	static int ShaderLowPrecision;
	static int ShaderMediumPrecision;
	static int ShaderHighPrecision;
};

#endif 