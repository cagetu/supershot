#pragma once

class GLESViewport : public IViewport
{
	__DeclareRtti;
public:
	GLESViewport();
	virtual ~GLESViewport();

	virtual bool Create(void* windowHandle, int32 width, int32 height, int32 format, uint32 featureFlag, const RelativeExtent& extent = Extent()) override;
	virtual void Destroy() override;

	virtual void Begin() override;
	virtual void End() override;

	virtual void Present() override;

	virtual void Resize(int32 width, int32 height, int32 format, bool fullscreen = false) override;
};