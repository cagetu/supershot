#pragma once

#define _MAX_BONE		40

class GLESShaderProgram;

/*
*/
class GLESShader : public IShader
{
	__DeclareRtti;
public:
	GLESShader();
	virtual ~GLESShader();

	bool Create(const SHADERCODE& shaderCode, ShaderFeature::MASK mask) OVERRIDE;
	void Destroy() OVERRIDE;

	void Begin(const CMaterial* material, const VertexBuffer* vb) OVERRIDE;
	void End() OVERRIDE;

	void SetValue(uint index, const Variant& value) OVERRIDE;

	void UpdateValues(const CMaterial* material) OVERRIDE;
	void ClearValues();

	void UpdateVertexAttributes(const VertexBuffer* vertexBuffer);
	void ClearVertexAttributes();

protected:
	friend class OpenGLESDriver;

	GLuint GetProgram() const { return m_Program; }

private:
	bool Initialize();

	GLuint m_Program;

	VertexDescription m_VertexDesc;

	GLESShaderProgram* m_ShaderParam;

	VertexBuffer* m_VertexBuffer;
	CMaterial* m_Material;
};

////////////////////////////////////////////////////////////////////////////////////

class GLESShaderProgram : public IShaderProgram
{
	__DeclareRtti;
public:
	GLESShaderProgram(uint32 shaderTypes);
	virtual ~GLESShaderProgram();

	bool Initialize(GLuint program);

	virtual void SetValue(uint32 index, const Variant& value) OVERRIDE;
	virtual void UpdateValues(const CMaterial* material) OVERRIDE;
	virtual void ClearValues() OVERRIDE;

protected:
	friend class GLESShader;

	void SetUniformValue(GLuint location, GLuint offset, const Variant& value);

	struct Uniform
	{
		int uid;			// uniform uid

		GLint location;		// uniform location
		GLint offset;		// uniform offset
		Variant value;		// uniform value;
	};
	Uniform* m_Uniforms;
	uint32 m_UniformCount;
};

////////////////////////////////////////////////////////////////////////////////////

class GLESShaderResource : public IShaderResource
{
	__DeclareRtti;
public:
	GLESShaderResource();
	virtual ~GLESShaderResource();

	bool Create(ICompiledShaderCode* code, ShaderType::TYPE type) OVERRIDE;
	void Destroy() OVERRIDE;

	GLuint GetShaderHandle() const { return m_ShaderHandle; }

private:
	GLuint m_ShaderHandle;
};

////

/*	
	- 컴파일된 셰이더 한 단위
	- 컴파일 조건
		- 같은 파일
		- 같은 셰이더 Define
*/
