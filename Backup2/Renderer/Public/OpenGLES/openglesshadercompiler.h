#pragma once

struct glslopt_shader;
struct glslopt_ctx;

struct GLESCompiledShaderCode : public ICompiledShaderCode
{
	std::string code;
};

class GLESShaderCompiler : public IShaderCompiler
{
public:
	enum Option
	{
		kGlslOptionSkipPreprocessor = (1 << 0), // Skip preprocessing shader source. Saves some time if you know you don't need it.
		kGlslOptionNotFullShader = (1 << 1), // Passed shader is not the full shader source. This makes some optimizations weaker.
	};

public :
	static bool Initialize();
	static void Shutdown();

	static ICompiledShaderCode* Find(const TCHAR* id);

	static ICompiledShaderCode* Compile(const TCHAR* id, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option = 0);

private:
	static bool _Compile(std::string& output, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option = 0);

	static std::map <String, GLESCompiledShaderCode*> m_ShaderCache;

	static glslopt_ctx* m_Target;
};

typedef GLESShaderCompiler ShaderCompiler;
