#pragma once

class GLESRenderTarget : public IRenderTarget
{
	__DeclareRtti;
public :
	GLESRenderTarget();
	virtual ~GLESRenderTarget();

	bool Create(int width, int height) OVERRIDE;

	// RenderSurface
	IRenderTexture2D* AddRenderTexture(int format, RTLoadAction loadAction = RTLoadAction::EClear, RTStoreAction storeAction = RTStoreAction::ENoAction) OVERRIDE;
	IRenderTexture2D* GetRenderTexture(int32 channel) const OVERRIDE;
	IRenderTexture2D* GetRenderTexture(const TCHAR* id) const OVERRIDE;
	virtual void ClearRenderTextures() OVERRIDE;

	// DepthSurface
	void CreateDepthBuffer(int format, int multisample) OVERRIDE;
	void SetDepthBuffer(const Ptr<IDepthStencilSurface>& surface) OVERRIDE;

private:
	void SetSurfaces();

	GLuint GetFrameBuffer() const { return m_FrameBuffer; }

	void Begin() OVERRIDE;
	void End() OVERRIDE;

	void Bind();
	void Discard();

private:
	friend class OpenGLESDriver;

	bool m_DirtyFBO;
	GLuint m_FrameBuffer;

	std::vector<IRenderTexture2D*> m_Channels;
	Ptr<GLESDepthStencilSurface> m_DepthBuffer;
};