#pragma once

class GLESTexture
{
public :
	GLESTexture();
	virtual ~GLESTexture();

	struct _PIXELFORMAT
	{
		GLuint internalformat;
		GLuint format;
		GLuint type;
	};

protected :
	_PIXELFORMAT ConvertPixelFormat(uint32 format);

	GLuint m_Texture;
	GLuint m_TextureTarget;
};

///////////

class GLESTexture2D : public GLESTexture, public ITexture2D
{
	__DeclareRtti;
public:
	GLESTexture2D();
	virtual ~GLESTexture2D();

	bool Load(const TCHAR* fname) OVERRIDE;

	void SetTexture() OVERRIDE;
};

///////////

class GLESWritableTexture : public GLESTexture, public IWritableTexture
{
	__DeclareRtti;
public:
	GLESWritableTexture();
	virtual ~GLESWritableTexture();

	bool Create(int width, int height, int format) OVERRIDE;

	void* Lock(int *pitch, int lv = 0, int flags = 0) OVERRIDE;
	void Unlock(bool force = true, int* rect = NULL) OVERRIDE;

protected:
	unsigned char* m_Buffer;
	unsigned int m_BufferSize;
	int m_LockRect[4];
};

///////////

class GLESRenderTexture : public GLESTexture, public IRenderTexture2D
{
	__DeclareRtti;
public:
	GLESRenderTexture();
	virtual ~GLESRenderTexture();

	bool Create(int width, int height, int format) OVERRIDE;

	void SetTexture() OVERRIDE;

	void SetSurface(int channel) OVERRIDE;
};