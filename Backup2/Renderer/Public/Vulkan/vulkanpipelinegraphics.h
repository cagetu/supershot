#pragma once

/* 그래픽 파이프라인 객체
*/
class VulkanGfxPipeline : public VulkanPipeline
{
	__DeclareRtti;
public:
	VulkanGfxPipeline(VulkanDevice* device, int32 pipelineIndex, VkGraphicsPipelineCreateInfo& pipelineCreateInfo, VkPipelineCache pipelineCache = VK_NULL_HANDLE, VulkanGfxPipeline* basePipeline = nullptr);
	virtual ~VulkanGfxPipeline();

	virtual VkPipelineBindPoint GetType() const OVERRIDE{ return VK_PIPELINE_BIND_POINT_GRAPHICS; }

private:
	VulkanGfxPipeline* m_BasePipeline;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class VulkanGfxPipelineManager : public VulkanPipelineManager
{
public:
	VulkanGfxPipeline* FindOrCreate(const GfxPipelineContext& pipelineContext, VulkanGfxPipeline* basePipeline = nullptr);
	void Remove(VulkanGfxPipeline* pipline);

public:
	virtual VkPipelineBindPoint GetType() const OVERRIDE{ return VK_PIPELINE_BIND_POINT_GRAPHICS; }

	virtual void Init(VulkanDevice* device) OVERRIDE;
	virtual void Destroy();

private:
	friend class VulkanDevice;

	VulkanGfxPipelineManager();
	virtual ~VulkanGfxPipelineManager();

	struct PipelineInstance
	{
		GfxPipelineContext context;
		VulkanGfxPipeline* pipline;
	};
	std::vector<PipelineInstance> m_PipelineInstances;
};