#pragma once

class VulkanViewport : public IViewport
{
	__DeclareRtti;
public:
	enum { _MAX_BUFFERS = 3 };

public:
	VulkanViewport(VulkanDriver* renderDriver);
	virtual ~VulkanViewport();

	bool Create(void* windowHandle, int32 width, int32 height, int32 format, uint32 featureFlag, const RelativeExtent& extent = Extent()) override;
	void Destroy() override;

	void Begin() override;
	void End() override;

	void Present() override;

	void Resize(int32 width, int32 height, int32 format, bool fullscreen = false) override;

public:
	VulkanSwapChain* GetSwapChain() const;

	const VkImage& GetCurrentImage() const;
	const Vulkan::TextureView& GetCurrentImageView() const;
	uint32 GetAcquiredImageIndex() const;
	const Ptr<VulkanBackBuffer>& GetBackBuffer() const;

private:
	bool CreateSwapChain(void* windowHandle, int32& width, int32& height, int32 format);
	void AcquireNextImage();

	VulkanDriver* m_RenderDriver;

	VulkanSemaphore* m_RenderingDoneSemaphore[_MAX_BUFFERS];
	VulkanSemaphore* m_AcquiredImageSemaphore;
	uint32 m_AcquiredImageIndex;
	VulkanSwapChain* m_SwapChain;

	// BackBuffer
	Ptr<VulkanBackBuffer> m_BackBuffers[_MAX_BUFFERS];

	VulkanCmdBuffer* m_CommandBuffer;

	// Swapchain Image 한 개에 하나의 framebuffer를 만드는데 이건 나중에 다시 체크해봐야 할 듯.
	//VulkanFramebuffer* m_Framebuffers[_MAX_BUFFERS];
};