#pragma once

/* Compute 파이프라인 객체
*/
class VulkanComputePipeline : public VulkanPipeline
{
	__DeclareRtti;
public:
	VulkanComputePipeline(VulkanDevice* device, int32 pipelineIndex, VkComputePipelineCreateInfo& pipelineCreateInfo, VkPipelineCache pipelineCache = VK_NULL_HANDLE);
	virtual ~VulkanComputePipeline();

	virtual VkPipelineBindPoint GetType() const OVERRIDE { return VK_PIPELINE_BIND_POINT_COMPUTE; }
};

class VulkanComputePipelineManager : public VulkanPipelineManager
{
public:

public:
	virtual VkPipelineBindPoint GetType() const OVERRIDE{ return VK_PIPELINE_BIND_POINT_COMPUTE; }

	virtual void Init(VulkanDevice* device) OVERRIDE;
	virtual void Destroy();

private:
	VulkanComputePipelineManager();
	virtual ~VulkanComputePipelineManager();

	friend class VulkanDevice;
};