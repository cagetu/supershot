#pragma once

#if TARGET_OS_ANDROID
#define VK_NO_PROTOTYPES
#include <vulkan.h>
#else
#include <vulkan/vulkan.h>
#endif

/*
	동적 링크용 Vulkan 함수 정의.... 
*/
#if defined(VK_NO_PROTOTYPES)

#define VK_EXPORTED_FUNCTION( fun ) extern PFN_##fun fun;
#define VK_GLOBAL_LEVEL_FUNCTION( fun) extern PFN_##fun fun;
#define VK_INSTANCE_LEVEL_FUNCTION( fun ) extern PFN_##fun fun;
#define VK_DEVICE_LEVEL_FUNCTION( fun ) extern PFN_##fun fun;

// 1. EntryPoint 함수들
#define ENUM_VK_ENTRYPOINTS_BASE(EnumMacro) \
	EnumMacro(PFN_vkCreateInstance, vkCreateInstance) \
	EnumMacro(PFN_vkGetDeviceProcAddr, vkGetDeviceProcAddr) \
	EnumMacro(PFN_vkEnumerateInstanceExtensionProperties, vkEnumerateInstanceExtensionProperties) \
	EnumMacro(PFN_vkEnumerateInstanceLayerProperties, vkEnumerateInstanceLayerProperties)

// 함수 리스트
#define ENUM_VK_FUNCTION(func) extern PFN_##func func;

/// Instance
ENUM_VK_FUNCTION(vkGetInstanceProcAddr)
ENUM_VK_FUNCTION(vkCreateInstance)
ENUM_VK_FUNCTION(vkDestroyInstance)
ENUM_VK_FUNCTION(vkEnumerateInstanceLayerProperties)
ENUM_VK_FUNCTION(vkEnumerateInstanceExtensionProperties)
/// Device
ENUM_VK_FUNCTION(vkGetDeviceProcAddr)
ENUM_VK_FUNCTION(vkCreateDevice)
ENUM_VK_FUNCTION(vkDestroyDevice)
ENUM_VK_FUNCTION(vkEnumeratePhysicalDevices)
ENUM_VK_FUNCTION(vkEnumerateDeviceLayerProperties)
ENUM_VK_FUNCTION(vkEnumerateDeviceExtensionProperties)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceProperties)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceFeatures)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceQueueFamilyProperties)
#if defined(USE_SWAPCHAIN_EXTENSIONS)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceSurfaceSupportKHR)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceSurfaceFormatsKHR)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceSurfacePresentModesKHR)
ENUM_VK_FUNCTION(vkGetPhysicalDeviceSurfaceCapabilitiesKHR)
ENUM_VK_FUNCTION(vkDestroySurfaceKHR)
#if TARGET_OS_ANDROID
ENUM_VK_FUNCTION(vkCreateAndroidSurfaceKHR)
#elif TARGET_OS_WINDOWS
ENUM_VK_FUNCTION(vkCreateWin32SurfaceKHR)
#endif
#endif // USE_SWAPCHAIN_EXTENSIONS

/// Queue
ENUM_VK_FUNCTION(vkGetDeviceQueue)

#endif // VK_NO_PROTOTYPES
