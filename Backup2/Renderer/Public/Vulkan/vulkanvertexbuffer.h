#pragma once

class VulkanVertexBuffer : public VertexBuffer, public IVulkanBuffer
{
	__DeclareRtti;
public:
	VulkanVertexBuffer(VulkanDevice* device);
	virtual ~VulkanVertexBuffer();

	bool Create(uint32 fvf, int32 vertexCount) OVERRIDE;
	bool Create(uint32 fvf, int32 vertexCount, void* vertexData) OVERRIDE;
	void Destroy() OVERRIDE;

	void* Lock(int32 stride = 0, int32 numVert = 0, int32 lockFlag = 0) OVERRIDE;
	void Unlock() OVERRIDE;

	void SetStreamSource(int32 channel = 0, int32 offset = 0) OVERRIDE;

	unsigned char* GetBytes() const OVERRIDE;

private:
	bool CreateBuffer(uint32 bufferSize, uint32 usage);

	Vulkan::StagingBuffer* m_StagingBuffer;
};