#pragma once

namespace Vulkan { class RingBuffer; }

class VulkanUniformBuffer : public UniformBuffer, public IVulkanBuffer
{
	__DeclareRtti;
public:
	VulkanUniformBuffer(VulkanDevice* device);
	virtual ~VulkanUniformBuffer();

	bool Create(void* uniformData, int32 size) OVERRIDE;
	void Destroy() OVERRIDE;

	void* Lock(uint32 bufferSize, uint32 offset);
	void  Unlock();

private:
	bool CreateBuffer(uint32 bufferSize);
};

class VulkanUniformBufferUploader
{
public:
	VulkanUniformBufferUploader(VulkanDevice* device, uint64 totalSize = VULKAN_UB_RING_BUFFER_SIZE);
	~VulkanUniformBufferUploader();

	uint64 AllocateMemory(uint64 Size, uint32 Alignment);

	char* GetCPUMapPointer();
	VkBuffer GetCPUBufferHandle() const;
	uint32 GetCPUBufferOffset() const;
	uint64 GetCPUBufferSize() const;

private:
	VulkanDevice* m_Device;

	Vulkan::RingBuffer* m_CPUBuffer;
	Vulkan::RingBuffer* m_GPUBuffer;
};
