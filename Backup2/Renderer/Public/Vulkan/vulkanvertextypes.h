#pragma once

// vertex buffer에서 값을 채우는 순서와 동일해야 함.
static const struct FVFInfo {
	const char* attrib;
	uint32 mask;
	uint32 size;
	VkFormat format;
	uint32 len;
	uint32 streamID;
	bool instanced;
	bool packed;
}	GFVFPreset[] =
{
	// Position
	"POSITIONT", FVF_XYZRHW, 4, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 4, 0, false, false,
	"POSITION", FVF_XYZ, 3, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 3, 0, false, false,
	"POSITION", FVF_XYZW, 4, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 4, 0, false, false,
	// Normal
	"NORMAL", FVF_NORMAL, 3, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 3, 0, false, false,
	"NORMAL", FVF_NORMAL_PACKED, 3, VK_FORMAT_R8G8B8A8_UNORM, sizeof(unsigned char) * 4, 0, false, true,
	// Skinned
	"BONEINDICES", FVF_SKIN_INDEX, 4, VK_FORMAT_R8G8B8A8_SNORM, sizeof(uint32), 0, false, false,
	"BONEWEIGHTS", FVF_SKIN_WEIGHT, 4, VK_FORMAT_R8G8B8A8_UNORM, sizeof(uint32), 0, false, false,
	// Color
	"DIFFUSE", FVF_DIFFUSE, 4, VK_FORMAT_R8G8B8A8_UNORM, sizeof(uint32), 0, false, false,
	// Texcoord
	"TEXCOORD0", FVF_TEX0, 2, VK_FORMAT_R32G32_SFLOAT, sizeof(float) * 2, 0, false, false,
	"TEXCOORD0", FVF_TEX0_PACKED, 2, VK_FORMAT_R16G16_SINT, sizeof(short) * 2, 0, false, true,
	"TEXCOORD1", FVF_TEX1, 2, VK_FORMAT_R32G32_SFLOAT, sizeof(float) * 2, 0, false, false,
	"TEXCOORD1", FVF_TEX1_PACKED, 2, VK_FORMAT_R16G16_SINT, sizeof(short) * 2, 0, false, true,
	// Tangent
	"TANGENT", FVF_TANSPC, 4, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 4, 0, false, false,
	// 
	"VERTEXA0", FVF_VERTEXAO, 4, VK_FORMAT_R8G8B8A8_UNORM, sizeof(uint32), 0, false, false,
	// SHCoeff
	"SHCOEFF", FVF_SHCOEFF, 3, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 3, 0, false, false,
	"SHCOEFF", FVF_SHCOEFF, 3, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 3, 0, false, false,
	"SHCOEFF", FVF_SHCOEFF, 3, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 3, 0, false, false,
	// Instance TM
	"FVF_INST_TM", FVF_INST_TM, 4, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 4, 1, true, false,
	"FVF_INST_TM", FVF_INST_TM, 4, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 4, 1, true, false,
	"FVF_INST_TM", FVF_INST_TM, 4, VK_FORMAT_R32G32B32A32_SFLOAT, sizeof(float) * 4, 1, true, false,
	"FVF_INST_TM_INDEX", FVF_INST_TM_INDEX, 1, VK_FORMAT_R32_SFLOAT, sizeof(float), 1, true, false,
};

