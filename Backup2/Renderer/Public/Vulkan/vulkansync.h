#pragma once

class VulkanFencePool;

/*	Queue 내부 또는 Queue 간의 Command 동기화를 처리
*/
class VulkanSemaphore
{
public:
	VulkanSemaphore(VulkanDevice* device);
	~VulkanSemaphore();

	inline VkSemaphore GetHandle() const { return m_Semaphore; }

private:
	VulkanDevice* m_Device;
	VkSemaphore m_Semaphore;
};

/*	CPU Side에서 GPU Ready를 기다림
	- 일반적으로 GPU 동작과 상관없이 프로그램 레벨에서 Queue 명령어의 완료를 체크하는데 사용
*/
class VulkanFence
{
public:
	VulkanFence(VulkanDevice* device, VulkanFencePool* owner, bool bCreateSignaled);
	~VulkanFence();

	inline const VkFence GetHandle() const { return m_Fence; }

	inline bool IsSignaled() const { return (m_State == _STATE::Signaled) ? true : false; }

private:
	friend class VulkanFencePool;

	VulkanDevice* m_Device;
	VulkanFencePool* m_Pool;

	VkFence m_Fence;

	enum class _STATE
	{
		// Initial state
		NotReady,

		// After GPU processed it
		Signaled,
	};
	_STATE m_State;
};

class VulkanFencePool
{
public:
	VulkanFencePool();
	~VulkanFencePool();

	void Init(VulkanDevice* device);
	void Destroy();

	VulkanFence* AllocateFence(bool bCreateSignaled = false);
	void ReleaseFence(VulkanFence*& fence);

	bool CheckFenceState(VulkanFence* fence);
	void ResetFence(VulkanFence* fence);

	bool WaitForFence(VulkanFence* fence, uint64 timeout);
	void WaitAndReleaseFence(VulkanFence*& fence, uint64 timeout);

	bool IsSignaled(VulkanFence* fence);

private:
	VulkanDevice* m_Device;
	std::deque<VulkanFence*> m_FreeFences;
	std::deque<VulkanFence*> m_UsedFences;
};