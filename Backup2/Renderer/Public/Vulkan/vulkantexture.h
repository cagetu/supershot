#pragma once

class IVulkanTexture
{
public:
	IVulkanTexture(VulkanDevice* device);
	virtual ~IVulkanTexture();

	const VkImage& GetImage() const { return m_ImageHandle; }
	const Vulkan::TextureView& GetView() const { return m_TextureView; }
	const Vulkan::TextureSampler& GetSampler() const { return m_TextureSampler; }

protected:
	VulkanDevice* m_Device;

	VkImage m_ImageHandle;

	Vulkan::TextureView m_TextureView;
	Vulkan::TextureSampler m_TextureSampler;
};

/////////////////////////////////////////////////////////////////////////////

class VulkanTexture2D : public IVulkanTexture, public ITexture2D
{
	__DeclareRtti;
public:
	VulkanTexture2D(VulkanDevice* device);
	virtual ~VulkanTexture2D();

	bool Load(const TCHAR* fname) OVERRIDE;

	void SetTexture() OVERRIDE;

	void* Lock(int32 length);
	void  Unlock();

private:
	Vulkan::StagingBuffer* m_StagingBuffer;
};

/////////////////////////////////////////////////////////////////////////////

class VulkanRenderTexture2D : public IVulkanTexture, public IRenderTexture2D
{
	__DeclareRtti;
public:
	VulkanRenderTexture2D(VulkanDevice* device);
	virtual ~VulkanRenderTexture2D();

	bool Create(int width, int height, int format) OVERRIDE;
	void Destroy() OVERRIDE;

	void SetTexture() OVERRIDE;
	void SetSurface(int channel) OVERRIDE;
};

/////////////////////////////////////////////////////////////////////////////

class VulkanWritableTexture : public IVulkanTexture, public IWritableTexture
{
	__DeclareRtti;
public:
	VulkanWritableTexture(VulkanDevice* device);
	virtual ~VulkanWritableTexture();

	bool Create(int width, int height, int format) OVERRIDE;

	void* Lock(int *pitch, int lv = 0, int flags = 0) OVERRIDE;
	void Unlock(bool force = true, int* rect = NULL) OVERRIDE;

protected:
	unsigned char* m_Buffer;
	unsigned int m_BufferSize;
	int m_LockRect[4];
};