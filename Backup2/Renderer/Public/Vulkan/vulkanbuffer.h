#pragma once

#define _MANAGED_BUFFER

class IVulkanBuffer
{
public:
	IVulkanBuffer(VulkanDevice* device);
	virtual ~IVulkanBuffer();

	inline VkBuffer GetHandle() const { return m_Buffer; }

protected:
	bool CreateBuffer(uint32 bufferSize, uint32 bufferUsage, uint32 memoryType);
	void DestroyBuffer();

	VulkanDevice* m_Device;

#ifdef _MANAGED_BUFFER
	Vulkan::BufferSubAllocation* m_BufferAllocation;
#else
	Vulkan::DeviceMemoryAllocation* m_MemoryAllocation;
#endif

	VkBuffer m_Buffer;

	//VkDescriptorBufferInfo m_BufferInfo;
};
