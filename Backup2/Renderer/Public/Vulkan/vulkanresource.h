#pragma once

namespace Vulkan
{
	class RenderTargetLayout
	{
	public:
		RenderTargetLayout();
		
		void Clear();

		uint32 AddColorAttachment(VulkanDevice* device, int32 format, int32 multiSamples, RTLoadAction loadAction, RTStoreAction storeAction);
		uint32 AddDepthStencilAttachment(VulkanDevice* device, int32 format, int32 multiSamples, RTLoadAction depthLoadAction, RTStoreAction depthStoreAction, RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction);

		inline int32 GetNumAttachments() const { return numAttachments; }
		inline int32 GetNumColorAttachments() const { return numColorAttachments; }
		inline bool HasResolveAttachments() const { return hasResolveAttachments; }
		inline bool HasDepthStencilAttachment() const { return hasDepthStencilAttachment; }

		inline const VkAttachmentReference* GetColorAttachments() const { return numColorAttachments > 0 ? colorAttachments : nullptr; }
		inline const VkAttachmentReference* GetResolveAttachments() const { return hasResolveAttachments ? resolveAttachments : nullptr; }
		inline const VkAttachmentReference* GetDepthStencilAttachment() const { return hasDepthStencilAttachment ? &depthStencilAttachment : nullptr; }

		inline const VkAttachmentDescription* GetAttachmentDesc() const { return attachmentDesc; }

		bool operator==(const RenderTargetLayout& rhs);
		bool operator==(const RenderTargetLayout& rhs) const;
		bool operator!=(const RenderTargetLayout& rhs);
		bool operator!=(const RenderTargetLayout& rhs) const;

	private:
		bool Compare(const RenderTargetLayout& rhs) const;

		VkAttachmentReference colorAttachments[_MAX_MRT];
		VkAttachmentReference resolveAttachments[_MAX_MRT];
		VkAttachmentReference depthStencilAttachment;

		int32 numColorAttachments;
		bool hasResolveAttachments;
		bool hasDepthStencilAttachment;

		uint32 numAttachments;
		VkAttachmentDescription attachmentDesc[_MAX_MRT * 2 + 1];
	};

	struct RenderTargetDesc
	{
		int32 width;
		int32 height;

		RenderTargetLayout layout;

		// All Attachments
		VkImageView attachmentViews[_MAX_MRT * 2 + 1];

		// Attachment ClearColor
		VkClearValue clearValues[_MAX_MRT + 1];

	public:
		RenderTargetDesc();

		void Clear();

		uint32 AddColorAttachment(VulkanDevice* device, VkImageView attachment, int32 format, int32 multiSamples, RTLoadAction loadAction, RTStoreAction storeAction, const RGBA& clearColor = RGBA::WHITE);
		uint32 AddDepthStencilAttachment(VulkanDevice* device, VkImageView attachment, int32 format, int32 multiSamples, 
			RTLoadAction depthLoadAction, RTStoreAction depthStoreAction, float clearDepth,
			RTLoadAction stencilLoadAction, RTStoreAction stencilStoreAction, ushort clearStencil = 0);

		bool operator==(const RenderTargetDesc& rhs);
		bool operator==(const RenderTargetDesc& rhs) const;
		bool operator!=(const RenderTargetDesc& rhs);
		bool operator!=(const RenderTargetDesc& rhs) const;
	};

	// View.....
	class TextureView
	{
	public:
		TextureView();

		void Create(VulkanDevice* device, VkImage image, VkImageViewType viewType = VK_IMAGE_VIEW_TYPE_2D, VkImageAspectFlags aspectMask = VK_IMAGE_ASPECT_COLOR_BIT, int32 format = RT_R8G8B8A8, uint32 mipMaps = 1, uint32 arraySize = 1);
		void Destroy(VulkanDevice* device);

		const VkImageView& GetHandle() const { return m_ImageView; }
		uint32 GetMipMaps() const { return m_MipmapCount; }
		uint32 GetArrayCount() const { return m_ArrayCount; }

		bool IsValid() const { return (m_ImageView != VK_NULL_HANDLE); }
		bool IsArray() const { return m_ArrayCount > 1; }

	private:
		VkImageView m_ImageView;

		uint32 m_MipmapCount;
		uint32 m_ArrayCount;
	};

	class TextureSampler
	{
	public:
		TextureSampler();

		void Create(VulkanDevice* device, VkFilter minFilter, VkFilter magFilter, VkSamplerAddressMode addressU, VkSamplerAddressMode addressV, VkSamplerAddressMode addressW, 
			int32 maxLodLevel, int32 minLodLevel, float mipLodBias = 0.0f, VkSamplerMipmapMode mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
			VkBorderColor borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE, bool anisotropyEnable = false, bool compareEnable = false, VkCompareOp compareOp = VK_COMPARE_OP_NEVER);
		void Destroy(VulkanDevice* device);

	private:
		VkSampler m_Sampler;
	};

	extern VkShaderStageFlagBits ShaderTypeToStage(ShaderType::TYPE type);
	extern VkWriteDescriptorSet WriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding, VkDescriptorBufferInfo* bufferInfo);
	extern VkWriteDescriptorSet WriteDescriptorSet(VkDescriptorSet dstSet, VkDescriptorType type, uint32_t binding, VkDescriptorImageInfo* imageInfo);

	extern const VkImageSubresourceRange& DefaultSubresourceRange();

	extern VkAttachmentLoadOp LoadAction(RTLoadAction loadAction);
	extern VkAttachmentStoreOp StoreAction(RTStoreAction storeAction);

	extern VkImage CreateImage(VulkanDevice* device, VkImageViewType viewType, uint32 format, uint32 width, uint32 height, uint32 depth, 
							   bool isArray, uint32 arraySize, uint32 mipMaps, uint32 multiSamples, bool isTilingLinear, uint32 textureFlags,
							   VkImageCreateInfo* OutCreateInfo, VkMemoryRequirements* OutMemoryRequirements);

	extern VkBuffer CreateBuffer(VulkanDevice* device, uint32 bufferSize, uint32 usage, VkBufferCreateInfo* OutCreateInfo, VkMemoryRequirements* OutMemoryRequirements);
}

/*
*/
struct VulKanRenderTargetView
{
	RTLoadAction loadAction;
	RTStoreAction storeAction;

	uint32 mipIndex;
	uint32 arraySliceIndex;


};