#pragma once

class VulkanSemaphore;

class VulkanSwapChain
{
public:
	VulkanSwapChain(VulkanDevice* device);
	~VulkanSwapChain();

	bool Create(VkInstance instance, void* windowHandle, int32& width, int32& height, uint32 requestImageCount, uint32 imageFormat = RT_B8G8R8A8);
	void Destroy();

	void Present(const VulkanQueue* Queue, VulkanSemaphore* RenderingDoneSignalSemaphore);

public:
	inline VkSwapchainKHR GetHandle() const { return m_SwapChain; }

	const VkImage& GetImage(uint32 index) const { return m_Images[index]; }
	uint32 GetImageCount() const { return m_Images.size(); }

private:
	friend class VulkanViewport;

	void AcquireNextImageIndex(uint32& acquiredImageIndex, VulkanSemaphore** acquiredSemaphore);
	void GetImages(std::vector<VkImage>& outImages);

private:
	VulkanDevice* m_Device;

	VkSwapchainKHR m_SwapChain;
	VkSurfaceKHR m_Surface;

	std::vector<VulkanSemaphore*> m_ImageAcquiredSemaphore;
	int32 m_CurrentSemaphoreIndex;
	uint32 m_CurrentAcquiredImageIndex;

	std::vector<VkImage> m_Images;
};

/*	SwapChain
	1. surface 생성
	2. surface format
	3. present mode 설정
	4. swapchain 생성

	- present
	- image
		- swap chain은 여러개의 image로 구성되어 있다.
		- image 하나 마다 하나의 Queue로 바인딩 된다.
*/