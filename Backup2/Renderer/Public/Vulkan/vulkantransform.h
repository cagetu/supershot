#pragma once

struct VulkanTransform : public CommonTransform
{
	static Mat4 Projection(float nearwidth, float nearheight, float nearclip, float farclip);
	static Mat4 ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio);
	static Mat4 ProjectionOffCenter(float l, float r, float b, float t, float nearclip, float farclip);
	static Mat4 InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon = 2.4f*10e-7f);
	static Mat4 Ortho(float w, float h, float nearclip, float farclip);
	static Mat4 OrthoOffCenter(float l, float r, float b, float t, float nearclip, float farclip);
};

typedef VulkanTransform	DeviceTransform;