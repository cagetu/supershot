#ifndef __VULKAN_VERTEXDECLARATION_H__
#define __VULKAN_VERTEXDECLARATION_H__

class VulkanVertexDeclaration : public IVertexDeclaration
{
	__DeclareRtti;
public:
	VulkanVertexDeclaration();
	virtual ~VulkanVertexDeclaration();

	bool Create(VertexDescription* inVertexDesc);
	bool Create(VertexDescription* inVertexDesc, int32 streamID, uint32 streamElements);

	void Clear();

	uint32 NumStreams() const { return m_BindingDescriptions.size(); }
	uint32 NumAttributes() const { return m_AttributeDescriptions.size(); }
	const VkVertexInputBindingDescription* StreamData() const { return m_BindingDescriptions.data(); }
	const VkVertexInputAttributeDescription* AttributeData() const { return m_AttributeDescriptions.data(); }

	bool operator == (const VulkanVertexDeclaration& rhs);
	bool CompareTo(IVertexDeclaration* other) override;

private:
	std::vector<VkVertexInputBindingDescription> m_BindingDescriptions;
	std::vector<VkVertexInputAttributeDescription> m_AttributeDescriptions;
};

#endif // __VULKAN_VERTEXDECLARATION_H__