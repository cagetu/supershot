#pragma once

struct GfxPipelineContext
{
	// Shader
	VulkanShader* Shader = NULL;

	// RenderPass
	VulkanRenderPass* RenderPass = NULL;
	int32 SubPassIndex = 0;

	// PipelineState
	PipelineState::ViewportDesc ViewportState;
	PipelineState::ScissorDesc ScissorState;
	PipelineState::InputAssemblyDesc InputAssemblyState;
	PipelineState::VertexInputDesc VertexInputState;
	PipelineState::TessellationDesc TessellationState;
	PipelineState::RasterizationDesc RasterizationState;
	PipelineState::MultiSampleDesc MultiSampleState;
	PipelineState::DepthBiasDesc DepthBiasState;
	PipelineState::DepthBoundTestDesc DepthBoundTestState;
	PipelineState::DepthTestDesc DepthTestState;
	PipelineState::StencilTestDesc StencilTestState;
	PipelineState::BlendConstant BlendConstants;
	PipelineState::ColorBlendDesc ColorBlendState[_MAX_MRT];
	DYNAMICSTATE::MASK DynamicState = DYNAMICSTATE::_VIEWPORT | DYNAMICSTATE::_SCISSOR;

	void Clear();

	void SetShader(VulkanShader* shader);

	GfxPipelineContext& operator = (const GfxPipelineContext& rhs);
	bool operator == (const GfxPipelineContext& rhs) const;
};

extern VkPipelineViewportStateCreateInfo CreateViewportState(const PipelineState::ViewportDesc& viewportDesc, const PipelineState::ScissorDesc& scissorDesc);
extern VkPipelineInputAssemblyStateCreateInfo CreateInputAssemblyState(const PipelineState::InputAssemblyDesc& description);
extern VkPipelineVertexInputStateCreateInfo CreateVertexInputState(const PipelineState::VertexInputDesc& description);
extern VkPipelineTessellationStateCreateInfo CreateTessellationState(const PipelineState::TessellationDesc& description);
extern VkPipelineRasterizationStateCreateInfo CreateRasterizationState(const PipelineState::RasterizationDesc& rasterDesc, const PipelineState::DepthTestDesc& depthTestDesc, const PipelineState::DepthBiasDesc& depthBiasDesc);
extern VkPipelineDepthStencilStateCreateInfo CreateDepthStencilState(const PipelineState::DepthTestDesc& depthTestDesc, const PipelineState::DepthBoundTestDesc& depthBoundDesc, const PipelineState::StencilTestDesc& stencilTestDesc);
extern VkPipelineMultisampleStateCreateInfo CreateMultisampleState(const PipelineState::MultiSampleDesc& desc);
extern void GetColorBlendStates(int32 attachmentCount, const PipelineState::ColorBlendDesc* colorBlendStates, std::vector<VkPipelineColorBlendAttachmentState>& attachmentBlendStates);
extern void GetShaderStages(VulkanShader* shader, std::vector<VkPipelineShaderStageCreateInfo>& stages);
extern void GetDynamicState(DYNAMICSTATE::MASK dymamicState, std::vector<VkDynamicState>& states);
