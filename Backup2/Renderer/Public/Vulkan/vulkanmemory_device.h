#pragma once

namespace Vulkan
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class DeviceMemoryAllocation;

enum
{
	NUM_FRAMES_TO_WAIT_BEFORE_RELEASING_TO_OS = 20,

	GPU_ONLY_HEAP_PAGE_SIZE = 256 * 1024 * 1024,		// VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
	STAGING_HEAP_PAGE_SIZE = 64 * 1024 * 1024,
};

/*	[디바이스 메모리 관리자]
	UE4(4.15)의 DeviceMemoryPool를 포팅했음
	*/
class DeviceMemoryPool
{
public:
	DeviceMemoryAllocation* Allocate(VkDeviceSize allocationSize, uint32 memoryTypeIndex);
	DeviceMemoryAllocation* Allocate(VkDeviceSize allocationSize, uint32 memoryTypeBits, VkMemoryPropertyFlags Properties);
	void Free(DeviceMemoryAllocation* allocation);

	VkResult GetMemoryType(uint32 memoryTypeBits, VkMemoryPropertyFlags Properties, uint32* outTypeIndex);
	VkResult GetMemoryTypeExcluding(uint32 TypeBits, VkMemoryPropertyFlags Properties, uint32 ExcludeTypeIndex, uint32* OutTypeIndex);

	inline const VkPhysicalDeviceMemoryProperties& GetMemoryProperies() const { return m_MemoryProperties; }
	inline uint32 NumMemoryTypes() const { return m_MemoryProperties.memoryTypeCount; }
	inline bool HasUnifiedMemory() const { return m_bHasUnifiedMemory; }

private:
	DeviceMemoryPool();
	~DeviceMemoryPool();

	void Init(VulkanDevice* device);
	void Destroy();

	friend class VulkanDevice;

private:
	struct FHeapInfo
	{
		VkDeviceSize TotalSize;
		VkDeviceSize UsedSize;
		VkDeviceSize PeakSize;
		std::vector<DeviceMemoryAllocation*> Allocations;

		FHeapInfo() :
			TotalSize(0),
			UsedSize(0),
			PeakSize(0)
		{
		}
	};

	std::vector<FHeapInfo> m_HeapInfos;

	VulkanDevice* m_Device;
	VkPhysicalDeviceMemoryProperties m_MemoryProperties;
	bool m_bHasUnifiedMemory;

	uint32 m_NumAllocations;
	uint32 m_PeakNumAllocations;
};

/*	DeviceMemory Allcation
*/
class DeviceMemoryAllocation
{
public:
	void* Map(VkDeviceSize size, VkDeviceSize offset);
	void Unmap();

	void FlushMapped();
	void InvalidateMapped();

	inline VkDeviceMemory Handle() const { return m_MemoryHandle; }
	inline VkDeviceSize Size() const { return m_MemorySize; }
	inline uint32 Offset() const { return m_Offset; }
	inline uint32 Type() const { return m_MemoryTypeIndex; }

	inline bool IsMapped() const { return m_MappedData == nullptr ? false : true; }
	inline bool IsCoherent() const { return m_bCoherent; }
	inline bool IsCached() const { return m_bCached; }
	inline bool CanBeMapped() const { return m_bCanBeMapped; }

	inline void* GetMappedData() { return m_MappedData; }

private:
	DeviceMemoryAllocation();

	VkDevice m_DeviceHandle;
	VkDeviceMemory m_MemoryHandle;
	VkDeviceSize m_MemorySize;
	uint32 m_MemoryTypeIndex;
	uint32 m_Offset;
	bool m_bCanBeMapped;
	bool m_bCoherent;
	bool m_bCached;
	bool m_bFreedBySystem;

	void* m_MappedData;

	friend class DeviceMemoryPool;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // end of namespace
