#pragma once

/*	Framebuffe 객체
	- RenderPass
	- 사용할 모든 Attachment (ImageView) 목록
*/
class VulkanFrameBuffer
{
public:
	VulkanFrameBuffer(VulkanDevice* device, VulkanRenderPass* renderPass, const Vulkan::RenderTargetDesc& RTDesc);
	~VulkanFrameBuffer();

	inline VkFramebuffer GetHandle() const { return m_Framebuffer; }

	inline VulkanRenderPass* GetRenderPass() const { return m_RenderPass; }
	inline const Vulkan::RenderTargetDesc& GetRenderTargetDesc() const { return m_RenderTargetDesc; }

private:
	VulkanDevice* m_Device;
	VulkanRenderPass* m_RenderPass;

	VkFramebuffer m_Framebuffer;

	Vulkan::RenderTargetDesc m_RenderTargetDesc;
};
