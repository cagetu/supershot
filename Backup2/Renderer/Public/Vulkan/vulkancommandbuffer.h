#pragma once

class VulkanCmdBufferPool;
class VulkanDescriptorSets;
class VulkanPipelineLayout;
class VulkanPipeline;

/* CommandBuffer
	- CommandPool에서 생성된다.
	- Primary Command Buffer
		: Secondary Commmand Buffer의 Owner가 되고, Secondary Command Buffer를 실행할 수 있다.
		: Queue에 직접 Submit할 수 있다.
	- Secondary Command Buffer
		: Primary Command Buffer를 통해서 실행된다.
		: Queue에 직접적으로 Submit할 수 없다.
*/
class VulkanCmdBuffer
{
public:
	struct UsageFlags
	{
		typedef int8 Type;
		enum
		{
			_ONETIME_SUBMIT = 1<<0,
			_RENDER_PASS_CONTINUE = 1<<1,
			_SIMULTANEOUS_USE_BIT = 1<<2,
		};
	};

	const static int32 BEGIN_ONETIME_SUBMIT = 0;

private:
	VulkanCmdBuffer(VulkanDevice* device, VulkanCmdBufferPool* commandPool, bool bSecondary = false);
	~VulkanCmdBuffer();

	friend class VulkanCmdBufferPool;

public:
	void Begin(UsageFlags::Type usageFlags = UsageFlags::_ONETIME_SUBMIT);
	void End();

	void Submit(VulkanQueue* queue);
	void Reset();

	void BeginRenderPass(VulkanFrameBuffer* framebuffer);
	void EndRenderPass();

	bool IsSubmitted() const { return (m_State == _STATE::SUBMITTED) ? true : false; }
	bool IsEnded() const { return (m_State == _STATE::END) ? true : false; }

public:
	void ExecuteCommands(int32 numSecondaryCmdBuffer, VulkanCmdBuffer** secondaryCmdBuffers);

public:
	void BindDescriptorSets(VulkanDescriptorSets* descriptor, VulkanPipelineLayout* pipelineLayout);
	void BindDescriptorSets(VulkanShader* shader);
	void BindPipeline(VulkanPipeline* pipeline);
	void BindVertexBuffer(VulkanVertexBuffer* vertexBuffer, uint64* Offsets);
	void BindIndexBuffer(VulkanIndexBuffer* indexBuffer, uint StartIndex);

	void Draw(uint StartVertex, uint VertexCount, uint32 StartInstance = 0, uint32 InstanceCount = 1);
	void DrawIndexed(int32 BaseVertex, uint StartIndex, uint IndexCount, uint32 StartInstance = 0, uint32 InstanceCount = 1);

public:
	void ClearColor(VkImage imageHandle, VkImageLayout imageLayout, const VkClearColorValue& clearValue);
	void ClearDepthStencil(VkImage imageHandle, VkImageLayout imageLayout, const VkClearDepthStencilValue& clearValue);

	void ClearAttachments(uint32 attachmentCount, const VkClearAttachment* pAttachments, uint32 rectCount, const VkClearRect* pRects);
	void ClearResolve(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageResolve* pRegions);

public:
	void CopyBuffer(VkBuffer srcBuffer, uint32 srcOffset, VkBuffer dstBuffer, uint32 dstOffset, uint32 bufferSize);
	void CopyImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageCopy* pRegions);
	void CopyBufferToImage(VkBuffer srcBuffer, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkBufferImageCopy* pRegions);
	void CopyImageToBuffer(VkImage srcImage, VkImageLayout srcImageLayout, VkBuffer dstBuffer, uint32 regionCount, const VkBufferImageCopy* pRegions);
	void BlitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, uint32 regionCount, const VkImageBlit* pRegions,	VkFilter filter);
	void UpdateBuffer(VkBuffer dstBuffer, VkDeviceSize dstOffset, VkDeviceSize dataSize, const void* pData);
	void FillBuffer(VkBuffer dstBuffer,	VkDeviceSize dstOffset,	VkDeviceSize size, uint32 data);

	//~Barriers
public:
	void TransferBuffer(VkBuffer bufferHandle, uint32 bufferSize, VkAccessFlags srcAccessMask, VkAccessFlags dstAccessMask);

	void TransitionImageLayout(VkImage imageHandle, VkImageLayout oldLayout, VkImageLayout newLayout);
	void TransitionImageLayout(VkImage imageHandle, VkImageLayout oldLayout, VkImageLayout newLayout, const VkImageSubresourceRange& subresourceRange);
	//~Barriers

	//~Begin Sync
public:
	void RefreshFenceStatus();

	VulkanFence* GetFence() const { return m_Fence; }
	uint64 GetFenceSignaledCounter() const { return m_FenceSignaledCounter; }
	//~End Sync

public:
	//~Begin DynamicState Commands
	void SetViewport(float width, float height, float posX = 0.0f, float posY = 0.0f, float minDepth = 0.0f, float maxDepth = 1.0f);
	void SetScissor(uint32 width, uint32 height, int32 posX = 0, int32 posY = 0);
	void SetLineWidth(float lineWidth);
	void SetDepthBias(float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);
	void SetDepthBounds(float minDepthBounds, float maxDepthBounds);
	void SetBlendConstants(float blendConstants[4]);
	void SetStencilCompareMask(uint32 faceMask, uint32 compareMask);
	void SetStencilWriteMask(uint32 faceMask, uint32 writeMask);
	void SetStencilReference(uint32 faceMask, uint32 reference);
	//~End DynamicState Commands

public:
	enum class _STATE
	{
		READYTOBEGIN,
		BEGIN,
			RENDERPASS,
		END,
		SUBMITTED,
	};

	inline const VkCommandBuffer Handle() const { return m_Handle; }
	inline _STATE State() const { return m_State; }

	inline bool IsSecondary() const { return m_bSecondary; }


private:
	VulkanDevice* m_Device;
	VkCommandBuffer m_Handle;

	VulkanCmdBufferPool* m_CommandPool;

	bool m_bSecondary;
	_STATE m_State;
	UsageFlags::Type m_UsageFlags;

	VulkanFence* m_Fence;
	volatile uint64 m_FenceSignaledCounter;
};

