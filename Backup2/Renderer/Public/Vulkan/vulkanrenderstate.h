#pragma once

class VulkanRenderState : public RenderState
{
public:
	VulkanRenderState(VulkanDriver* driver);

	void Update(uint32 renderState, int32 alphaRef = 128) OVERRIDE;
	void Reset() OVERRIDE;

private:
	VulkanDriver* m_Driver;
};

typedef VulkanRenderState CRenderState;
