#pragma once

#include "vulkanmemory_device.h"
#include "vulkanmemory_resource.h"

namespace Vulkan
{
///////////////////////////////////////////////////////////////////////////////////////
class StagingBuffer;

/* [StagingBufferPool]
*/
class StagingBufferPool
{
public:
	// VK_BUFFER_USAGE_TRANSFER_SRC_BIT : 버퍼 복사 시 BUFFER 전송 소스로 사용
	StagingBuffer* Allocate(uint32 bufferSize, VkBufferUsageFlags InUsageFlags = VK_BUFFER_USAGE_TRANSFER_SRC_BIT, bool bCPURead = false);
	void Release(VulkanCmdBuffer* CmdBuffer, StagingBuffer* buffer);

	void ProcessPendingFree(bool bImmediately = false);

private:
	StagingBufferPool();
	~StagingBufferPool();

	void Init(VulkanDevice* device);
	void Destroy();

private:
	friend class VulkanDevice;
	VulkanDevice* m_Device;

	struct PendingItem
	{
		VulkanCmdBuffer* CmdBuffer;
		uint64 FenceCounter;
		StagingBuffer* Resource;
	};

	std::vector<StagingBuffer*> m_UsedStagingBuffers;
	std::vector<PendingItem> m_PendingFreeStagingBuffers;
	std::vector<PendingItem> m_FreeStagingBuffers;
};

class StagingBuffer : public IRenderResource
{
public:
	StagingBuffer();

	VkBuffer GetHandle() const;

	void* Lock();
	void Unlock();

	void* GetMappedPointer();

	uint32 GetAllocationOffset() const;
	uint32 GetDeviceMemoryAllocationSize() const;
	VkDeviceMemory GetDeviceMemoryHandle() const;

protected:
	friend class StagingBufferPool;
	StagingBuffer(VulkanDevice* device, uint32 bufferSize, VkBufferUsageFlags InUsageFlags, bool bCPURead);
	virtual ~StagingBuffer();

	Vulkan::ResourceAllocation* m_ResourceAllocation;
	VkBuffer m_Buffer;
	bool m_bCPURead;

	void Destroy(VulkanDevice* Device);
};


class RingBuffer
{
public:
	RingBuffer(VulkanDevice* device, uint64 totalSize, VkFlags usage, VkMemoryPropertyFlags memoryPropertyFlags);
	~RingBuffer();

	uint64 AllocateMemory(uint64 size, uint32 alignment);

	inline uint64 GetTotalSize() const { return m_TotalSize; }
	inline uint32 GetBufferOffset() const { return m_BufferOffset; }
	inline VkBuffer GetHandle() const { return m_BufferSubAllocation->GetHandle(); }
	inline void* GetMappedPointr() const { return m_BufferSubAllocation->GetMappedPointer(); }

private:
	VulkanDevice* m_Device;
	uint64 m_TotalSize;
	uint32 m_BufferOffset;
	uint32 m_Alignment;

	BufferSubAllocation* m_BufferSubAllocation;
};

///////////////////////////////////////////////////////////////////////////////////////
} // end of namespace