
// MACROS
#define __DeclareRenderPass(classname)	\
public: \
	static RenderPass*		CreateClass(); \
	static bool				RegisterFactoryFunc(); \
private:

#define __ImplementRenderPass(classname) \
	RenderPass* classname::CreateClass() \
				{ \
		classname* result = new classname(); \
		return (RenderPass*)result; \
				} \
	bool classname::RegisterFactoryFunc() \
				{ \
			RenderPass::RegisterObject(#classname, classname::CreateClass); \
			return true; \
				} \
	static const bool factoryRegistered_##classname = classname::RegisterFactoryFunc();