#pragma once

struct Transform
{
	static Vector4 Multiply(const Mat4 & m, const Vector4 & p);
	static Mat4 Multiply(const Mat4 & a, const Mat4 & b);

	static Mat4 LookAt(const Vector3 & pivot, const Vector3 & target, const Vector3 & upvec);
	static Mat4 LookAt(const Vector3 & pivot, const Vector3 & target);
	static Mat4 LookAtZAxis(const Vector3 & pivot, const Vector3 & zaxis);
	static Mat4 RotationX(float rad);
	static Mat4 RotationY(float rad);
	static Mat4 RotationZ(float rad);
	static Mat4 Rotation(const Quat4& q);
	static Mat4 Scaling(const Vector3& s);
	static Mat4 Scaling(float s);
	static Mat4 Translate(const Vector3 & p);
	static Mat4 Translate(float x, float y, float z);

	static Mat4 Projection(float nearwidth, float nearheight, float nearclip, float farclip);
	static Mat4 ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio);
	static Mat4 ProjectionOffCenter(float l, float r, float b, float t, float nearclip, float farclip);
	static Mat4 InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon = 2.4f*10e-7f);
	static Mat4 Ortho(float w, float h, float nearclip, float farclip);
	static Mat4 OrthoOffCenter(float l, float r, float b, float t, float nearclip, float farclip);

	static void Decompose(const Mat4& input, Vector3& translate, Quat4& rotate, Vector3& scale);
};