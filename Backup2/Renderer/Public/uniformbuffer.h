#pragma once

class UniformBuffer : public IRenderResource
{
	__DeclareRtti;
public:
	UniformBuffer();
	virtual ~UniformBuffer();

	virtual bool Create(void* uniformData, int32 size) ABSTRACT;
	virtual void Destroy();

protected:
	int32 m_Offset;
	int32 m_Length;
};