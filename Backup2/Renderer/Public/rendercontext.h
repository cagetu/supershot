#pragma once

enum CLEARTYPE
{
	CLEAR_COLOR		= 0x00000001,
	CLEAR_DEPTH		= 0x00000002,
	CLEAR_STENCIL	= 0x00000004,
	LOAD_COLOR		= 0x00000010,
	LOAD_DEPTH		= 0x00000020,
	LOAD_STENCIL	= 0x00000040,
	NONE_COLOR		= 0x00000100,
	NONE_DEPTH		= 0x00000200,
	NONE_STENCIL	= 0x00000400,
};

enum PRIMITIVETYPE
{
	PT_TRIANGLELIST = 0,
	PT_TRIANGLESTRIP,
	PT_TRIANGLEFAN,
	PT_LINELIST,
	PT_LINESTRIP,
	PT_POINTLIST,
	PT_PATCHLIST,
};


enum RT_TYPE
{
	// 부호 없는 포멧
	RT_R5G6B5 = 0,
	RT_R8G8B8,
	RT_R8G8B8X8,
	RT_R5G5B5A1,
	RT_R4G4B4A4,
	RT_R8G8B8A8,
	RT_B8G8R8A8,
	// 부호 있는 포멧
	RT_U8V8W8Q8,		// 32 비트의 범프 맵 포맷으로, 채널 마다 8 비트를 사용한다.
	RT_U16V16W16Q16,	// 64 비트의 범프 맵 포맷으로, 성분 마다 16 비트를 사용한다
	// 버퍼포멧
	RT_D32,
	RT_D24S8,
	RT_D24X8,
	RT_D16,
	RT_D15S1,
	RT_DEPTH_BYTE,
	RT_DEPTH_INT,
	RT_DEPTH_SHORT,
	RT_ALPHA,
	RT_LUMINANCE,
	RT_LUMINANCEALPHA,
	// 부동 소수점 포멧
	RT_R16F,
	RT_G16R16F,
	RT_A16B16G16R16F,
	RT_R32F,
	RT_G32R32F,
	RT_A32B32G32R32F,
	RT_MAX,
};

/**
*	Resource usage flags - for vertex and index buffers.
*/
enum BufferUsageFlags
{
	// Mutually exclusive write-frequency flags
	BUF_Static = 0x0001, // The buffer will be written to once.
	BUF_Dynamic = 0x0002, // The buffer will be written to occasionally.
	BUF_Volatile = 0x0004, // The buffer will be written to frequently.

	// Mutually exclusive bind flags.
	BUF_UnorderedAccess = 0x0008, // Allows an unordered access view to be created for the buffer.

	/** Create a byte address buffer, which is basically a structured buffer with a uint32 type. */
	BUF_ByteAddressBuffer = 0x0020,
	/** Create a structured buffer with an atomic UAV counter. */
	BUF_UAVCounter = 0x0040,
	/** Create a buffer that can be bound as a stream output target. */
	BUF_StreamOutput = 0x0080,
	/** Create a buffer which contains the arguments used by DispatchIndirect or DrawIndirect. */
	BUF_DrawIndirect = 0x0100,
	/**
	* Create a buffer that can be bound as a shader resource.
	* This is only needed for buffer types which wouldn't ordinarily be used as a shader resource, like a vertex buffer.
	*/
	BUF_ShaderResource = 0x0200,

	/**
	* Request that this buffer is directly CPU accessible
	* (@todo josh: this is probably temporary and will go away in a few months)
	*/
	BUF_KeepCPUAccessible = 0x0400,

	/**
	* Provide information that this buffer will contain only one vertex, which should be delivered to every primitive drawn.
	* This is necessary for OpenGL implementations, which need to handle this case very differently (and can't handle GL_HALF_FLOAT in such vertices at all).
	*/
	BUF_ZeroStride = 0x0800,

	/** Buffer should go in fast vram (hint only) */
	BUF_FastVRAM = 0x1000,

	// Helper bit-masks
	BUF_AnyDynamic = (BUF_Dynamic | BUF_Volatile),
};

enum TEXTURECREATE_TYPE
{
	TEX_RenderTarget = 1 << 0,
	TEX_DepthStencilTarget = 1 << 1,
	TEX_ResolveRenderTarget = 1 << 2,
	TEX_ResolveDepthStencilTarget = 1 << 3,
	TEX_ShaderResource = 1 << 4,
	TEX_SRGB = 1 << 5,
	TEX_DYNAMIC = 1 << 6,
	TEX_Present = 1 << 7,
	TEX_FastVRAM = 1 << 8,
};

/**
* Action to take when a rendertarget is set.
*/
enum class RTLoadAction
{
	ENoAction,
	ELoad,
	EClear,
};

/**
* Action to take when a rendertarget is unset or at the end of a pass.
*/
enum class RTStoreAction
{
	ENoAction,
	EStore,
	EMultisampleResolve,
};

// 정렬 계층... 낮을수록 뒤에 그려짐...
enum SORTLAYER
{
	ORDER_SKY = -50,
	ORDER_BACKGROUND = -40,
	ORDER_WATER = -30,
	ORDER_TERRAIN = -20,
	ORDER_MODEL = 0,
	ORDER_FOREGROUND = 10,
};

enum UID_TYPE
{
	UID_MATERIAL = 0x100000,
	UID_SHADER = 0x200000,
	UID_PRIMITIVE = 0x500000,
};

#define _MAX_MRT	4

class IRenderContext
{
public:
	virtual void SetVertexBuffer(VertexBuffer* vertexbuffer) ABSTRACT;
	virtual void SetIndexBuffer(IndexBuffer* indexbuffer) ABSTRACT;

	virtual void SetRenderTarget(IRenderTarget* renderTarget) ABSTRACT;

	virtual void SetShader(IShader* shader, const VertexBuffer* vb, const CMaterial* material) ABSTRACT;

	virtual void SetRenderState(uint32 rs) ABSTRACT;

	virtual void DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint32 StartVertex, uint32 PrimitiveCount) ABSTRACT;
	virtual void DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint32 BaseVertex, uint32 StartIndex, uint32 PrimitiveCount, IndexBuffer* IdxBuffer) ABSTRACT;
};
