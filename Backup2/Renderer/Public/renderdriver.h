#pragma once

#include "rendercontext.h"

class IRenderDriver
{
public:
	virtual bool Create(const IDisplayContext* displayHandle) ABSTRACT;
	virtual void Destroy() ABSTRACT;

	virtual void Present() ABSTRACT;

	virtual void BeginScene(IRenderTarget* rt = NULL, uint32 rs_or = 0, uint32 rs_and = 0, int32 clearFlags = -1, COLOR clearColor = 0xffffffff, float clearDepth = 1.0f, ushort clearStencil = 0) ABSTRACT;
	virtual void EndScene() ABSTRACT;

	virtual void Clear(COLOR clearColor, float clearDepth = 1.0f, ushort clearStencil = 0, uint32 clearFlags = CLEAR_COLOR | CLEAR_DEPTH) ABSTRACT;

	virtual void DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint StartVertex, uint VertexCount, uint32 StartInstance = 0, uint32 InstanceCount = 1, uint32 NumControlPoints = 0) ABSTRACT;
	virtual void DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint BaseVertex, uint StartIndex, uint IndexCount, IndexBuffer* IdxBuffer, uint32 StartInstance = 1, uint32 InstanceCount = 1, uint32 NumControlPoints = 0) ABSTRACT;

public:
	virtual void SetVertexBuffer(VertexBuffer* vertexbuffer) ABSTRACT;
	virtual void SetIndexBuffer(IndexBuffer* indexbuffer) ABSTRACT;

	virtual void SetRenderTarget(IRenderTarget* renderTarget) ABSTRACT;
	virtual void DiscardRenderTarget(bool depth, bool stencil, uint32 colors = 0) ABSTRACT;

	virtual void SetShader(IShader* shader, const VertexBuffer* vb, const CMaterial* material) ABSTRACT;
	
	virtual void SetRenderState(uint32 rs) ABSTRACT;

public:
	virtual IViewport* CreateViewport() ABSTRACT;
	virtual void DestroyViewport(IViewport* viewport) ABSTRACT;

	virtual void SetViewport(IViewport* viewport) ABSTRACT;

	virtual IShader* CreateShader() ABSTRACT;
	virtual void DestroyShader(IShader* shader) ABSTRACT;

	virtual VertexBuffer* CreateVertexBuffer() ABSTRACT;
	virtual void DestroyVertexBuffer(VertexBuffer* vertexBuffer) ABSTRACT;

	virtual IndexBuffer* CreateIndexBuffer() ABSTRACT;
	virtual void DestroyIndexBuffer(IndexBuffer* indexBuffer) ABSTRACT;

public:
	int GetWidth() const { return m_Width; }
	int GetHeight() const { return m_Height; }

protected:
	int m_Width;
	int m_Height;
};

IRenderDriver* CreateRenderDriver();
void DestroyRenderDriver();
IRenderDriver* GetRenderDriver();
