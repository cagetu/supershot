#pragma once

struct ICompiledShaderCode
{
};

class IShaderCompiler
{
public :
	static bool Initialize();
	static void Shutdown();

	static ICompiledShaderCode* Find(const TCHAR* id) { return NULL; }

	static ICompiledShaderCode* Compile(const TCHAR* id, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option = 0) { return false; }
};

////// GLOBAL FUNCTION //////

ICompiledShaderCode* CompileShader(const TCHAR* id, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option = 0);