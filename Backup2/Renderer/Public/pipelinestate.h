#ifndef _PIPELINESTATE_DEFINES_H_
#define _PIPELINESTATE_DEFINES_H_

namespace PipelineState
{
	struct InputAssemblyDesc
	{
		PRIMITIVETYPE topology = PT_TRIANGLELIST;
	};

	struct VertexInputDesc
	{
		Ptr<IVertexDeclaration> layout;
	};

	struct TessellationDesc
	{
		uint32 patchControlPoints = 0;
	};

	struct ViewportDesc
	{
		float x = 0.0f;
		float y = 0.0f;
		float width = 0.0f;
		float height = 0.0f;
		float minDepth = 0.0f;
		float maxDepth = 1.0f;

		bool operator == (const ViewportDesc& rhs) const;
		bool operator != (const ViewportDesc& rhs) const;
	};

	struct ScissorDesc
	{
		int32 x = 0;
		int32 y = 0;
		int32 width = 0;
		int32 height = 0;

		bool operator == (const ScissorDesc& rhs) const;
		bool operator != (const ScissorDesc& rhs) const;
	};

	struct DepthBiasDesc
	{
		float DepthBiasConstantFactor = 0.0f;
		float DepthBiasSlopeFactor = 0.0f;
		float DepthBiasClamp = 0.0f;
		bool bDepthBiasEnable = false;

		bool operator == (const DepthBiasDesc& rhs) const;
		bool operator != (const DepthBiasDesc& rhs) const;
	};

	struct DepthBoundTestDesc
	{
		float minDepthBound = 0.0f;
		float maxDepthBound = 0.0f;
		bool bDepthBoundTestEnable = false;

		bool operator == (const DepthBoundTestDesc& rhs) const;
		bool operator != (const DepthBoundTestDesc& rhs) const;
	};

	struct StencilTestDesc
	{
		struct StencilOp
		{
			STENCILFUNC::TYPE failOp = STENCILFUNC::_KEEP;
			STENCILFUNC::TYPE passOp = STENCILFUNC::_KEEP;
			STENCILFUNC::TYPE depthFailOp = STENCILFUNC::_KEEP;
			COMPAREFUNC::TYPE compareOp = COMPAREFUNC::_ALWAYS;
			uint32 compareMask = 0;
			uint32 writeMask = 0;
			uint32 reference = 0;

			bool operator == (const StencilOp& rhs) const { return (failOp == rhs.failOp) && (passOp == rhs.passOp) && (depthFailOp == rhs.depthFailOp) && (compareOp == rhs.compareOp) && (compareMask == rhs.compareMask) && (writeMask == rhs.writeMask) && (reference == rhs.reference); }
		};
		StencilOp front;
		StencilOp back;
		bool bStencilTestEnable = false;

		bool operator == (const StencilTestDesc& rhs) const;
		bool operator != (const StencilTestDesc& rhs) const;
	};

	struct BlendConstant
	{
		float blendConstants[4];

		bool operator == (const BlendConstant& rhs) const;
		bool operator != (const BlendConstant& rhs) const;

		BlendConstant() { Memory::Memzero(blendConstants); }
	};

	struct MultiSampleDesc
	{
		uint32 sampleCount = 1;
		uint32 sampleMask = 0xffffffff;
		uint32 rasterizationSamples = 1;
		// Alpha Coverage
		bool alphaToCoverageEnable = false;
		bool alphaToOneEnable = false;
		// SampleShading
		bool sampleShadingEnable = false;
		float minSampleShading = 1.0f;

		bool operator == (const MultiSampleDesc& rhs) const;
		bool operator != (const MultiSampleDesc& rhs) const;
	};

	//*****************************************************************************
	// RenderState로 처리가 가능...
	// DepthBias, Stencil, DepthBoundTest는 별도로 처리하도록 한다.

	struct RasterizationDesc
	{
		CULLMODE::TYPE CullMode = CULLMODE::_CCW;
		FILLMODE::TYPE FillMode = FILLMODE::_FILL;
		bool bSkipRasterizer = false;

		bool operator == (const RasterizationDesc& rhs) const;
		bool operator != (const RasterizationDesc& rhs) const;
	};

	struct DepthTestDesc
	{
		COMPAREFUNC::TYPE DepthCompareOp = COMPAREFUNC::_LESS_EQUAL;
		bool bDepthTestEnable = true;
		bool bDepthWriteEnable = true;

		bool operator == (const DepthTestDesc& rhs) const;
		bool operator != (const DepthTestDesc& rhs) const;
	};

	struct ColorBlendDesc
	{
		enum COLORBIT
		{
			COLOR_R = 0x00000001,
			COLOR_G = 0x00000002,
			COLOR_B = 0x00000004,
			COLOR_A = 0x00000008,
			COLOR_RGBA = COLOR_R | COLOR_G | COLOR_B | COLOR_A,
			COLOR_NONE = 0,
		};

		BLENDFUNC::TYPE colorBlendOp = BLENDFUNC::_ADD;
		BLENDFACTOR::TYPE srcColorBlendFactor = BLENDFACTOR::_ONE;
		BLENDFACTOR::TYPE dstColorBlendFactor = BLENDFACTOR::_ZERO;
		BLENDFUNC::TYPE alphaBlendOp = BLENDFUNC::_ADD;
		BLENDFACTOR::TYPE srcAlphaBlendFactor = BLENDFACTOR::_ONE;
		BLENDFACTOR::TYPE dstAlphaBlendFactor = BLENDFACTOR::_ZERO;
		uint32 colorWriteMask = COLOR_RGBA;
		bool bBlendEnable = false;

		bool operator == (const ColorBlendDesc& rhs) const;
		bool operator != (const ColorBlendDesc& rhs) const;
	};

	//*****************************************************************************
}

#endif // _PIPELINESTATE_DEFINES_H_