#pragma once

class ICoordinate
{
public:
	virtual Mat4 Projection(float nearwidth, float nearheight, float nearclip, float farclip) = 0;
	virtual Mat4 ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio) = 0;
	virtual Mat4 ProjectionOffCenter(float l, float r, float b, float t, float nearclip, float farclip) = 0;
	virtual Mat4 InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon = 2.4f*10e-7f) = 0;
	virtual Mat4 Ortho(float w, float h, float nearclip, float farclip) = 0;
	virtual Mat4 OrthoOffCenter(float left, float right, float bottom, float top, float nearclip, float farclip) = 0;
};
