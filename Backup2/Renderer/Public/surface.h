#pragma once

//
//
//
class ISurface : public IRenderResource
{
	__DeclareRtti;
public :
	ISurface();
	virtual ~ISurface();

	inline int32 GetWidth() const { return m_Width; }
	inline int32 GetHeight() const { return m_Height; }
	inline int32 GetDepth() const { return m_Depth; }
	inline int32 GetFormat() const { return m_PixelFormat; }

	inline int32 GetMultisamples() const { return m_MultiSamples; }
	inline int32 GetMipmaps() const { return m_MipmapCount; }

	inline RTLoadAction GetLoadAction() const { return m_LoadAction; }
	inline RTStoreAction GetStoreAction() const { return m_StoreAction; }

	int32 GetByteSize() const;

protected :
	int32 m_Width;
	int32 m_Height;
	int32 m_Depth;

	int32 m_PixelFormat;

	int32 m_MultiSamples;
	int32 m_MipmapCount;
	int32 m_MipmapLevel;

	// RT용 텍스쳐를 Pooling해서 사용하려면, 여기에 있는 것이 맞지 않을 것 같기는 한데, 일단 가지고 있어보자!!! (RenderTarget이 가지고 있어야 하는 것 아닐까?)
	RTLoadAction m_LoadAction;
	RTStoreAction m_StoreAction;
};

//
//
//
class IDepthStencilSurface : public ISurface
{
	__DeclareRtti;
public :
	IDepthStencilSurface();
	virtual ~IDepthStencilSurface();

	virtual bool Create(uint32 width, uint32 height, uint32 format, uint32 samples = 1) ABSTRACT;
	virtual void Destroy() ABSTRACT;

	virtual void SetSurface() ABSTRACT;

	RTLoadAction GetStencilLoadAction() const { return m_StencilLoadAction; }
	RTStoreAction GetStencilStoreAction() const { return m_StencilStoreAction; }

protected:
	RTLoadAction m_StencilLoadAction;
	RTStoreAction m_StencilStoreAction;
};