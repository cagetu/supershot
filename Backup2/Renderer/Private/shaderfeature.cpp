#include "renderer.h"

std::vector <String> ShaderFeature::m_BitToString;
std::vector <std::string> ShaderFeature::m_Masks;

std::map <String, int>	ShaderFeature::m_StringToBit;

int ShaderFeature::m_AllocBit = 0;

void ShaderFeature::Initialize()
{
	static struct
	{
		int bit;
		const TCHAR* define;
	} _list[] =
	{
		_SKINNING, TEXT("SKINNING"),
		_INSTANCING, TEXT("_INSTANCING"),
		_VERTEXCOMPRESS, TEXT("_VERTEXCOMPRESS"),
		_USE_NORMAL, TEXT("_USE_NORMAL"),
		_USE_VERTEXCOLOR, TEXT("_USE_VERTEXCOLOR"),
		_USE_ALPHA, TEXT("_USE_ALPHA"),
	};
	for (int i = 0; i < _countof(_list); i++)
	{
		RegisterBit(_list[i].define);
	}
}

void ShaderFeature::Shutdown()
{
	Reset();
}

void ShaderFeature::Reset()
{
	m_StringToBit.clear();
	m_BitToString.clear();
	m_Masks.clear();

	m_AllocBit = 0;
}

ShaderFeature::MASK ShaderFeature::GetShaderMask(const TCHAR* shadermask)
{
	if (shadermask == NULL)
		return 0;

	MASK mask = 0;

	int i, len;
	for (i = 0; shadermask[i] != '\0';)
	{
		if (shadermask[i] == ',')
		{
			i++;
			continue;
		}

		for (len = 0; shadermask[i + len] != '\0' && shadermask[i + len] != ','; len++);
		
		String def(&shadermask[i], len);
		i += len;

		int bitIndex = RegisterBit(def);
		mask |= (1 << bitIndex);
	}

	return mask;
}

int ShaderFeature::RegisterBit(const TCHAR* def)
{
	std::map <String, int>::iterator it = m_StringToBit.find(def);
	if (it != m_StringToBit.end())
	{
		return (*it).second;
	}

	int bitIndex = m_AllocBit++;
	if (bitIndex >= _MAXBIT)
	{
		PlatformUtil::DebugOut("ShaderFeature Bit is Full 32\n");
		return 0;
	}
	m_StringToBit.insert(std::make_pair(def, bitIndex));
	m_BitToString.push_back(def);

	char txt8[128] = { 0, };
	string::conv_s(txt8, _countof(txt8), def);
	m_Masks.push_back(txt8);

	return bitIndex;
}

int ShaderFeature::FindBit(const TCHAR* def)
{
	std::map <String, int>::const_iterator it = m_StringToBit.find(def);
	if (it != m_StringToBit.end())
		return (*it).second;

	return -1;
}

String ShaderFeature::GetMaskName(ShaderFeature::MASK mask)
{
	const int totalID = m_AllocBit;

	String output;
	for (int i = 0; i < totalID; i++)
	{
		if (0 != (mask & (1 << i)))
		{
			if (output.empty() == false)
				output += TEXT(",");

			output += m_BitToString.at(i);
		}
	}
	return output;
}

int ShaderFeature::GetMacro(uint32 mask, std::string* macro, std::string* macroCode)
{
	int n = 0;

	for (int i = 0; i < m_AllocBit; i++)
	{
		if (0 != (mask & (1 << i)))
		{
			macro[n] = m_Masks.at(i);
			*macroCode += std::string("#define ") + macro[n] + std::string("\n");
			n++;
		}
	}
	macro[n].clear();
	return n;
}
