#include "foundation.h"
#include "d3dtransform.h"

//--------------------------------------------------------------
/**	@desc : z[0, 1] 구간으로 매핑되는 정규 시야 영역으로 변환
	2*zn/w  0       0              0
	0       2*zn/h  0              0
	0       0       zf/(zf-zn)     1
	0       0       zn*zf/(zn-zf)  0
*/
//--------------------------------------------------------------
Mat4 D3DCoordinate::Projection(float nearwidth, float nearheight, float nearclip, float farclip)
{
	float q = farclip / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f*nearclip / nearwidth;	m._12 = 0.0f;						m._13 = 0.0f;			m._14 = 0.0f;
	m._21 = 0.0f;						m._22 = 2.0f*nearclip / nearheight;	m._23 = 0.0f;			m._24 = 0.0f;
	m._31 = 0.0f;						m._32 = 0.0f;						m._33 = q;				m._34 = 1.0f;
	m._41 = 0.0f;						m._42 = 0.0f;						m._43 = -nearclip * q;	m._44 = 0.0f;
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[0, 1] 구간으로 매핑되는 정규 시야 영역으로 변환
	xScale     0          0               0
	0        yScale       0               0
	0          0       zf/(zf-zn)         1
	0          0       -zn*zf/(zf-zn)     0
	where:
	yScale = cot(fovY/2)
	xScale = aspect ratio / yScale
*/
//--------------------------------------------------------------
Mat4 D3DCoordinate::ProjectionWithFov(float fov, float nearclip, float farclip, float aspectratio)
{
	// z : [0, 1]
	float q = farclip / (farclip - nearclip);
	float wtan = 1.0f / (float)Math::Tan(fov * 0.5f);
	// aspectratio = height / weight (vs weight/height)

	Mat4 m;
	m._11 = wtan * aspectratio;	m._12 = 0.0f;	m._13 = 0.0f;			m._14 = 0.0f;
	m._21 = 0.0f;				m._22 = wtan;	m._23 = 0.0f;			m._24 = 0.0f;
	m._31 = 0.0f;				m._32 = 0.0f;	m._33 = q;				m._34 = 1.0f;
	m._41 = 0.0f;				m._42 = 0.0f;	m._43 = -nearclip * q;	m._44 = 0.0f;
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[0, 1] 구간으로 매핑되는 정규 시야 영역으로 변환
*/
//--------------------------------------------------------------
Mat4 D3DCoordinate::ProjectionOffCenter(float l, float r, float b, float t, float zn, float zf)
{
	float q = zf / (zf - zn);

	Mat4 m =
	{
		2.0f*zn / (r - l),	0.0f,				0.0f,		0.0f,
		0.0f,				2.0f*zn / (t - b),	0.0f,		0.0f,
		(l + r) / (l - r),	(t + b) / (b - t),	q,			1.0f,
		0.0f,				0.0f,				-zn * q,	0.0f
	};
	return m;
}

//--------------------------------------------------------------
/**	@desc : z[0, 1-e] 구간으로 매핑되는 정규 시야 영역으로 변환
	http://www.terathon.com/gdc07_lengyel.pdf
	xScale     0          0               0
	0        yScale       0               0
	0          0       zf/(zf-zn)         1
	0          0       -zn*zf/(zf-zn)     0
*/
//--------------------------------------------------------------
Mat4 D3DCoordinate::InfiniteProjectionWithFov(float fov, float nearclip, float aspectratio, float epsilon)
{
	float wtan = 1.0f / (float)tan(fov * 0.5f);
	Mat4 m;

	// [0, 1-e]로 변환해야 한다.
#if 1
	m._11 = wtan * aspectratio;	m._12 = 0.0f;	m._13 = 0.0f;							m._14 = 0.0f;
	m._21 = 0.0f;				m._22 = wtan;	m._23 = 0.0f;							m._24 = 0.0f;
	m._31 = 0.0f;				m._32 = 0.0f;	m._33 = (1.0f - epsilon);				m._34 = 1.0f;
	m._41 = 0.0f;				m._42 = 0.0f;	m._43 = (epsilon - 1.0f) * nearclip;	m._44 = 0.0f;
#else
	// NDC 공간이 [0, 1], viewport 공간에서의 z가 [0, 1]이므로, 특별히 할 필요없이, 무한 평면으로 사상!
	m._11 = wtan * aspectratio;	m._12 = 0.0f;	m._13 = 0.0f;				m._14 = 0.0f;
	m._21 = 0.0f;				m._22 = wtan;	m._23 = 0.0f;				m._24 = 0.0f;
	m._31 = 0.0f;				m._32 = 0.0f;	m._33 = (1.0f - epsilon);	m._34 = 1.0f;
	m._41 = 0.0f;				m._42 = 0.0f;	m._43 = -nearclip;			m._44 = 0.0f;
#endif 
	return m;
}


//--------------------------------------------------------------
/*
	2/w  0    0           0
	0    2/h  0           0
	0    0    1/(zf-zn)   0
	0    0    zn/(zn-zf)  1
*/
//--------------------------------------------------------------
Mat4 D3DCoordinate::Ortho(float width, float height, float nearclip, float farclip)
{
	// z[0, 1]
	float q = 1.0f / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f / width;	m._12 = 0;				m._13 = 0;				m._14 = 0;
	m._21 = 0;				m._22 = 2.0f / height;	m._23 = 0;				m._24 = 0;
	m._31 = 0;				m._32 = 0;				m._33 = q;				m._34 = 0;
	m._41 = 0;				m._42 = 0;				m._43 = -nearclip * q;	m._44 = 1.0f;
	return m;
}

//--------------------------------------------------------------
/*
	2/(r-l)      0            0           0
	0            2/(t-b)      0           0
	0            0            1/(zf-zn)   0
	(l+r)/(l-r)  (t+b)/(b-t)  zn/(zn-zf)  l
where:
	l = -w/2,
	r = w/2,
	b = -h/2, and
	t = h/2.
*/
//--------------------------------------------------------------
Mat4 D3DCoordinate::OrthoOffCenter(float l, float r, float b, float t, float nearclip, float farclip)
{
	// z[0, 1]
	float q = 1.0f / (farclip - nearclip);

	Mat4 m;
	m._11 = 2.0f / (r - l);		m._12 = 0;					m._13 = 0;				m._14 = 0;
	m._21 = 0;					m._22 = 2.0f / (t - b);		m._23 = 0;				m._24 = 0;
	m._31 = 0;					m._32 = 0;					m._33 = q;				m._34 = 0;
	m._41 = (l + r) / (l - r);	m._42 = (t + b) / (b - t);	m._43 = -nearclip * q;	m._44 = 1.0f;
	return m;
}

