#include "renderer.h"

__ImplementRtti(VisualShock, VulkanIndexBuffer, IndexBuffer);

VulkanIndexBuffer::VulkanIndexBuffer(VulkanDevice* device)
	: IVulkanBuffer(device)
	, m_StagingBuffer(nullptr)
{
}

VulkanIndexBuffer::~VulkanIndexBuffer()
{
}

bool VulkanIndexBuffer::Create(int32 indexCount, int32 stride)
{
	uint32 bufferSize = stride * indexCount;

	if (CreateBuffer(bufferSize, BUF_Static) == false)
		return false;

	m_Length = bufferSize;
	SetStride(stride);
	return true;
}

bool VulkanIndexBuffer::Create(int32 indexCount, void* indexData, int32 stride)
{
	if (Create(indexCount, stride) == false)
		return false;

	void* ptr = Lock();
	{
		Memory::Memcopy(ptr, indexData, m_Length);
	}
	Unlock();

	return true;
}

void VulkanIndexBuffer::Destroy()
{
	IndexBuffer::Destroy();
	IVulkanBuffer::DestroyBuffer();

	if (m_StagingBuffer)
	{
		m_Device->StagingBufferPool().Release(nullptr, m_StagingBuffer);
		m_StagingBuffer = nullptr;
	}
}

bool VulkanIndexBuffer::CreateBuffer(uint32 bufferSize, uint32 usage)
{
	IVulkanBuffer::CreateBuffer(bufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	m_Length = bufferSize;
	m_BufferUsage = usage;
	return true;
}

unsigned char* VulkanIndexBuffer::GetBytes() const
{
	return NULL;
}

void* VulkanIndexBuffer::Lock(int32 length, int32 lockFlag)
{
	if (m_BufferUsage&BUF_Static || m_BufferUsage&BUF_Dynamic)
	{
		_ASSERT(length < m_Length);

		m_StagingBuffer = m_Device->StagingBufferPool().Allocate(length, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
		return m_StagingBuffer->Lock();
	}
	return NULL;
}

void VulkanIndexBuffer::Unlock(ushort baseVertIdx)
{
	/*
		BeginCmdBuffer
			CopyBuffer
		EndCmdBuffer
		**** Fence ****
		Submit
		WaitFence

		FreeCmdBuffer
		DestroyBuffer(stagingBuffer)
		FreeMemory(stagingMemory);
	*/
	if (m_BufferUsage&BUF_Static || m_BufferUsage&BUF_Dynamic)
	{
		m_StagingBuffer->Unlock();

		/////

		VulkanCmdBuffer* CmdBuffer = m_Device->CommandBufferPool().GetUploadCommandBuffer();
		CmdBuffer->CopyBuffer(m_StagingBuffer->GetHandle(), 0, m_Buffer, m_Offset, m_Length);
		//m_Device->CommandBufferPool().SubmitUploadCmdBuffer(true);

		m_Device->StagingBufferPool().Release(CmdBuffer, m_StagingBuffer);
		m_StagingBuffer = nullptr;
	}
}

int32 VulkanIndexBuffer::SetIndices()
{
	return 0;
}