#include "renderer.h"

__ImplementRtti(VisualShock, VulkanPipeline, IRenderResource);

VulkanPipeline::VulkanPipeline(VulkanDevice* device, int32 pipelineIndex)
	: m_Device(device)
	, m_Handle(VK_NULL_HANDLE)
	, m_Index(pipelineIndex)
{
}
VulkanPipeline::~VulkanPipeline()
{
	if (m_Handle != VK_NULL_HANDLE)
	{
		vkDestroyPipeline(m_Device->GetHandle(), m_Handle, nullptr);
		m_Handle = VK_NULL_HANDLE;
	}
}

///////////////////////////////////////////////////////////////////

VulkanPipelineManager::VulkanPipelineManager()
	: m_Device(nullptr)
{
}

VulkanPipelineManager::~VulkanPipelineManager()
{
}

void VulkanPipelineManager::Init(VulkanDevice* device)
{
	m_Device = device;
}

void VulkanPipelineManager::Destroy()
{
}

bool VulkanPipelineManager::Load(const unicode::string& filename)
{
	return false;
}

bool VulkanPipelineManager::Save(const unicode::string& filename)
{
	return false;
}

///////////////////////////////////////////////////////////////////////////////

void VulkanPipelineManager::PipelineCache::Create(VulkanDevice* device, void* initialData, uint32 initialDataSize)
{
	_ASSERT(pipelineCache == VK_NULL_HANDLE);

	VkPipelineCacheCreateInfo CreateInfo;
	Memory::Memzero(CreateInfo);
	CreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	CreateInfo.initialDataSize = initialDataSize;
	CreateInfo.pInitialData = initialData;
	VK_RESULT(vkCreatePipelineCache(device->GetHandle(), &CreateInfo, nullptr, &pipelineCache));
}

void VulkanPipelineManager::PipelineCache::Destroy(VulkanDevice* device)
{
	if (pipelineCache != VK_NULL_HANDLE)
	{
		vkDestroyPipelineCache(device->GetHandle(), pipelineCache, nullptr);
	}
	pipelineCache = VK_NULL_HANDLE;
}

void VulkanPipelineManager::PipelineCache::Merge(VulkanDevice* device)
{

}

void* VulkanPipelineManager::PipelineCache::GetData(VulkanDevice* device)
{
	//void* data = nullptr;
	//uint32 size = 0;
	//VkResult result = vkGetPipelineCacheData(device->GetHandle(), pipelineCache, &size, data);
	return NULL;
}

bool VulkanPipelineManager::PipelineCache::SaveToFile(const char* filename)
{
	return false;
}


