#include "renderer.h"
#include "vulkanvertextypes.h"

__ImplementRtti(VisualShock, VulkanVertexDeclaration, IVertexDeclaration);

VulkanVertexDeclaration::VulkanVertexDeclaration()
{
}
VulkanVertexDeclaration::~VulkanVertexDeclaration()
{
}

void VulkanVertexDeclaration::Clear()
{
	m_BindingDescriptions.clear();
	m_AttributeDescriptions.clear();
}

bool VulkanVertexDeclaration::Create(VertexDescription* inVertexDesc)
{
	Clear();

	uint32 offset = 0;
	uint32 location = 0;

	for (uint32 streamID = 0; streamID < 4; streamID++)
	{
		const VertexStreamDesc& streamDesc = inVertexDesc->streams[streamID];

		if (streamDesc.stride > 0)
		{
			VkVertexInputBindingDescription bindDesc;
			bindDesc.binding = streamID;
			bindDesc.stride = streamDesc.stride;
			bindDesc.inputRate = streamDesc.instanced ? VK_VERTEX_INPUT_RATE_INSTANCE : VK_VERTEX_INPUT_RATE_VERTEX;

			m_BindingDescriptions.push_back(bindDesc);

			for (int32 index = 0; index < _countof(GFVFPreset); index++)
			{
				const FVFInfo& Preset = GFVFPreset[index];

				if (streamDesc.Contains(Preset.mask))
				{
					VkVertexInputAttributeDescription attributeDesc;
					attributeDesc.location = inVertexDesc->locations[location++];
					attributeDesc.binding = streamID;
					attributeDesc.format = Preset.format;
					attributeDesc.offset = offset;

					m_AttributeDescriptions.push_back(attributeDesc);

					offset += Preset.len;
					if (offset == inVertexDesc->vertexSize)
						break;
				}
			}
		}
	}
	return true;
}

bool VulkanVertexDeclaration::Create(VertexDescription* inVertexDesc, int32 streamID, uint32 streamElements)
{
	uint32 offset = 0;
	uint32 location = 0;

	const VertexStreamDesc& streamDesc = inVertexDesc->streams[streamID];

	if (streamDesc.stride > 0)
	{
		VkVertexInputBindingDescription bindDesc;
		bindDesc.binding = streamID;
		bindDesc.stride = streamDesc.stride;
		bindDesc.inputRate = streamDesc.instanced ? VK_VERTEX_INPUT_RATE_INSTANCE : VK_VERTEX_INPUT_RATE_VERTEX;

		m_BindingDescriptions.push_back(bindDesc);

		for (int32 index = 0; index < _countof(GFVFPreset); index++)
		{
			const FVFInfo& Preset = GFVFPreset[index];

			if (streamElements&Preset.mask)
			{
				if (streamDesc.Contains(Preset.mask))
				{
					VkVertexInputAttributeDescription attributeDesc;
					attributeDesc.location = location++;
					attributeDesc.binding = streamID;
					attributeDesc.format = Preset.format;
					attributeDesc.offset = offset;

					m_AttributeDescriptions.push_back(attributeDesc);
				}
				offset += Preset.len;
				if (offset == streamDesc.stride)
					break;
			}
		}
	}

	return true;
}

bool VulkanVertexDeclaration::CompareTo(IVertexDeclaration* other)
{
	VulkanVertexDeclaration* decl = DynamicCast<VulkanVertexDeclaration>(other);
	if (decl == NULL)
		return false;

	return (*this == *decl) ? true : false;
}

bool VulkanVertexDeclaration::operator == (const VulkanVertexDeclaration& rhs)
{
	if (rhs.m_BindingDescriptions.size() != m_BindingDescriptions.size())
		return false;

	for (uint32 i = 0; i < m_BindingDescriptions.size(); i++)
	{
		if (m_BindingDescriptions[i].binding != rhs.m_BindingDescriptions[i].binding ||
			m_BindingDescriptions[i].stride != rhs.m_BindingDescriptions[i].stride ||
			m_BindingDescriptions[i].inputRate != rhs.m_BindingDescriptions[i].inputRate)
			return false;
	}

	if (rhs.m_AttributeDescriptions.size() != m_AttributeDescriptions.size())
		return false;

	for (uint32 i = 0; i < m_AttributeDescriptions.size(); i++)
	{
		if (m_AttributeDescriptions[i].binding != rhs.m_AttributeDescriptions[i].binding ||
			m_AttributeDescriptions[i].format != rhs.m_AttributeDescriptions[i].format ||
			m_AttributeDescriptions[i].location != rhs.m_AttributeDescriptions[i].location ||
			m_AttributeDescriptions[i].offset != rhs.m_AttributeDescriptions[i].offset)
			return false;
	}

	return true;
}
