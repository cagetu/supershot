#include "renderer.h"

VulkanQueue::VulkanQueue(VulkanDevice* device, uint32 familyIndex, uint32 queueIndex)
	: m_Device(device)
	, m_FamilyIndex(familyIndex)
	, m_QueueIndex(queueIndex)
{
	::vkGetDeviceQueue(device->GetHandle(), familyIndex, queueIndex, &m_Queue);
	_ASSERT(m_Queue != VK_NULL_HANDLE);
}
VulkanQueue::~VulkanQueue()
{	
}

void VulkanQueue::Submit(VulkanCmdBuffer* commandBuffer, VulkanSemaphore* signalSemaphore, VulkanSemaphore* waitSemaphore, VkPipelineStageFlags waitStageMask)
{
	_ASSERT(commandBuffer->IsEnded());
	_ASSERT(m_Queue != VK_NULL_HANDLE);

	VulkanFence* fence = commandBuffer->GetFence();
	_ASSERT(fence->IsSignaled() == false);

	// Queue가 Image를 참조하는 Command를 수행하기 전에 Semaphore를 통해서, Image가 사용가능한 상태가 될 때까지 대기해야 한다.

	VkSubmitInfo submitInfo;
	Memory::Memzero(submitInfo);
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	// 실행을 위해 Submit할 CommandBuffer. Submit 하면 Command를 실행 (ex, acquired swapchain image를 Color Attachment로 바인드 하는 Command Buffer를 Submit 한다.)
	if (commandBuffer)
	{
		VkCommandBuffer commandBufferHandle = commandBuffer->Handle();
		submitInfo.pCommandBuffers = &commandBufferHandle;
		submitInfo.commandBufferCount = 1;
	}
	// 실행을 시작하기 전에 이용가능할 때까지 기다린다. 
	if (waitSemaphore)
	{
		VkSemaphore semaphore = waitSemaphore->GetHandle();
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &semaphore;				// 이 Batch에 대한 Command Buffer가 Execution을 시작하기 전에 대기하는 Semaphore의 Array
		submitInfo.pWaitDstStageMask = &waitStageMask;			// Semaphore Wait가 발생할 각 대응하는 파이프라인 스테이지의 Array
	}
	// CommandBuffer의 실행이 완료되었을 때, Signal된다.
	if (signalSemaphore)
	{
		VkSemaphore semaphore = signalSemaphore->GetHandle();
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &semaphore;
	}

	VK_RESULT(::vkQueueSubmit(m_Queue, 1, &submitInfo, fence->GetHandle()));

	if (commandBuffer)
	{
		commandBuffer->Submit(this);
	}
}

/*	Queue가 Submmit된 모든 작업을 완료하기를 기다린다.
	
	- Device->WaitForIdle 은 디바이스의 모든 Queue에 대해서 WaitForIdle 처리

	- 하지만 성능 상의 이유로 사용을 추천하지는 않는다. Queue에 모든 작업들을 완전히 Flush하며 매우 무거운 연산들이다.
	  따라서, 어플리케이션을 종료할 때나, 쓰레드 관리, 메모리 관리와 같은 어플리케이션 서브시스템을 다시 초기화할 때, Pause 등에 적합하다.

	- 동기화가 필요할 때에는 Fence를 이용하자!
*/
void VulkanQueue::WaitForIdle()
{
	if (m_Queue == VK_NULL_HANDLE)
		return;
	
	VK_RESULT(vkQueueWaitIdle(m_Queue));
}

//void VulkanQueue::Flush(VulkanCmdBuffer* commandBuffer, VulkanFence* fence)
//{
//	commandBuffer->End();
//
//	Submit(commandBuffer);
//
//	if (fence)
//	{
//		VkFence fences[] = { fence->GetHandle() };
//		VK_RESULT(vkWaitForFences(m_Device->GetHandle(), 1, &fences[0], VK_TRUE, 100000000000));
//	}
//
//	// 큐 삭제 요청
//}