#include "renderer.h"

///////////////////////////////////////////////////////////////////

VulkanFrameBuffer::VulkanFrameBuffer(VulkanDevice* device, VulkanRenderPass* renderPass, const Vulkan::RenderTargetDesc& RTDesc)
	: m_Device(device)
{
	/*	framebuffer는 어떤 이미지들이 이 attachment들로 사용될 것인지를 명시한다.

		* framebuffer는 render pass가 작동하는 특정 image들을 기술한다.
			: render pass에서 사용되는 모든 텍스쳐들을 기술한다.
			: (color, depth/stencil) 처럼 우리가 렌더링 하는 이미지들 뿐만 아니라, (input attachment) 처럼 source data로 사용되는 이미지들
		* render pass와 frame buffer를 분리함으로서, 다른 framebuffer에서 render pass를 사용할수도 있고, 다른 renderpass에서 framebuffer를 사용할수도 있다.
			: 유사한 타입과 용도의 이미지에 비슷한 형태로 수행되는 것을 의미한다.

		1. framebuffer와 renderpass attachment에 사용되는 각 이미지에 대한 ImageView를 만들어야 한다!
		2. Vulkan에서는 이미지를 직접 접근되지 않는다. 그래서 Image View가 사용된다. ImageView는 Image를 감싸고 추가적인 정보를 제공한다.
	*/
	//m_Attachments = attachments;

	VkFramebufferCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	createInfo.pNext = nullptr;
	// 사용할 렌더패스
	createInfo.renderPass = renderPass->GetHandle();
	// 렌더패스 attachment에 사용될 imageview 목록
	createInfo.attachmentCount = RTDesc.layout.GetNumAttachments();
	createInfo.pAttachments = RTDesc.attachmentViews;
	// Size 설정
	createInfo.width = RTDesc.width;
	createInfo.height = RTDesc.height;
	createInfo.layers = 1;		// ???
	VK_RESULT(vkCreateFramebuffer(m_Device->GetHandle(), &createInfo, nullptr, &m_Framebuffer));

	m_RenderPass = renderPass;
	m_RenderTargetDesc = RTDesc;
}

VulkanFrameBuffer::~VulkanFrameBuffer()
{
	vkDestroyFramebuffer(m_Device->GetHandle(), m_Framebuffer, nullptr);
}

