#include "renderer.h"

namespace Vulkan
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

StagingBufferPool::StagingBufferPool()
	: m_Device(nullptr)
{
}
StagingBufferPool::~StagingBufferPool()
{
	_ASSERT(m_UsedStagingBuffers.empty());
	_ASSERT(m_PendingFreeStagingBuffers.empty());
	_ASSERT(m_FreeStagingBuffers.empty());
}

void StagingBufferPool::Init(VulkanDevice* device)
{
	m_Device = device;
}

void StagingBufferPool::Destroy()
{
	ProcessPendingFree(true);

	_ASSERT(m_UsedStagingBuffers.empty());
	_ASSERT(m_PendingFreeStagingBuffers.empty());
	_ASSERT(m_FreeStagingBuffers.empty());
}

StagingBuffer* StagingBufferPool::Allocate(uint32 bufferSize, VkBufferUsageFlags InUsageFlags, bool bCPURead)
{
	//return new StagingBuffer(m_Device, bufferSize);

	//#todo-rco: Better locking!
	{
		//FScopeLock Lock(&GAllocationLock);

		for (auto it = m_FreeStagingBuffers.begin(); it != m_FreeStagingBuffers.end(); it++)
		{
			PendingItem& Item = (*it);

			StagingBuffer* FreeBuffer = (StagingBuffer*)Item.Resource;
			if (FreeBuffer->m_ResourceAllocation->GetSize() == bufferSize && FreeBuffer->m_bCPURead == bCPURead)
			{
				m_FreeStagingBuffers.erase(it);
				m_UsedStagingBuffers.push_back(FreeBuffer);
				return FreeBuffer;
			}
		}
	}

	StagingBuffer* NewStagingBuffer = new StagingBuffer(m_Device, bufferSize, InUsageFlags, bCPURead);

	{
		//FScopeLock Lock(&GAllocationLock);
		m_UsedStagingBuffers.push_back(NewStagingBuffer);
	}
	return NewStagingBuffer;
}

void StagingBufferPool::Release(VulkanCmdBuffer* CmdBuffer, StagingBuffer* buffer)
{
	//FScopeLock Lock(&GAllocationLock);
	auto it = std::find(m_UsedStagingBuffers.begin(), m_UsedStagingBuffers.end(), buffer);
	m_UsedStagingBuffers.erase(it);

	PendingItem Entry;
	Entry.CmdBuffer = CmdBuffer;
	Entry.FenceCounter = CmdBuffer ? CmdBuffer->GetFenceSignaledCounter() : 0;
	Entry.Resource = buffer;
	buffer = nullptr;
}

void StagingBufferPool::ProcessPendingFree(bool bImmediately)
{
	//FScopeLock Lock(&GAllocationLock);
	uint32 NumOriginalFreeBuffers = m_FreeStagingBuffers.size();

	for (auto it = m_PendingFreeStagingBuffers.begin(); it != m_PendingFreeStagingBuffers.end();)
	{
		PendingItem& Entry = (*it);

		if (bImmediately || !Entry.CmdBuffer || Entry.FenceCounter < Entry.CmdBuffer->GetFenceSignaledCounter())
		{
			PendingItem NewEntry = Entry;
			NewEntry.FenceCounter = Entry.CmdBuffer ? Entry.CmdBuffer->GetFenceSignaledCounter() : 0;

			m_PendingFreeStagingBuffers.erase(it++);
			m_FreeStagingBuffers.push_back(NewEntry);
		}
		else
			it++;
	}

	for (auto it = m_FreeStagingBuffers.begin(); it != m_FreeStagingBuffers.end();)
	{
		PendingItem& Entry = (*it);

		if (bImmediately || !Entry.CmdBuffer || Entry.FenceCounter + NUM_FRAMES_TO_WAIT_BEFORE_RELEASING_TO_OS < Entry.CmdBuffer->GetFenceSignaledCounter())
		{
			Entry.Resource->Destroy(m_Device);
			delete Entry.Resource;
			m_FreeStagingBuffers.erase(it++);
		}
		else
			it++;
	}
}


//----------------------------------------------------------------------
/*		Vulkan Staging Buffer		*/
//----------------------------------------------------------------------
StagingBuffer::StagingBuffer()
	: m_ResourceAllocation(nullptr)
	, m_Buffer(VK_NULL_HANDLE)
	, m_bCPURead(false)
{
}
StagingBuffer::StagingBuffer(VulkanDevice* device, uint32 bufferSize, VkBufferUsageFlags InUsageFlags, bool bCPURead)
{
	VkBufferCreateInfo StagingBufferCreateInfo;
	Memory::Memzero(StagingBufferCreateInfo);
	StagingBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	StagingBufferCreateInfo.size = bufferSize;
	StagingBufferCreateInfo.usage = InUsageFlags;
	VK_RESULT(::vkCreateBuffer(device->GetHandle(), &StagingBufferCreateInfo, nullptr, &m_Buffer));

	VkMemoryRequirements MemReqs;
	::vkGetBufferMemoryRequirements(device->GetHandle(), m_Buffer, &MemReqs);

	// Set minimum alignment to 16 bytes, as some buffers are used with CPU SIMD instructions
	MemReqs.alignment = Math::Max((VkDeviceSize)16, MemReqs.alignment);

	m_bCPURead = bCPURead;

	// VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT : CPU에서 접근 가능
	m_ResourceAllocation = device->ResourceHeapPool().AllocateBufferMemory(MemReqs, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | (bCPURead ? VK_MEMORY_PROPERTY_HOST_CACHED_BIT : VK_MEMORY_PROPERTY_HOST_COHERENT_BIT), __FILE__, __LINE__);
	m_ResourceAllocation->BindBuffer(device, m_Buffer);
}
StagingBuffer::~StagingBuffer()
{
	_ASSERT(!m_ResourceAllocation);
}

void StagingBuffer::Destroy(VulkanDevice* Device)
{
	_ASSERT(m_ResourceAllocation);

	::vkDestroyBuffer(Device->GetHandle(), m_Buffer, nullptr);
	m_Buffer = VK_NULL_HANDLE;
	m_ResourceAllocation = nullptr;
}


VkBuffer StagingBuffer::GetHandle() const
{
	return m_Buffer;
}

void* StagingBuffer::Lock()
{
	return GetMappedPointer();
}

void StagingBuffer::Unlock()
{
}

void* StagingBuffer::GetMappedPointer()
{
	return m_ResourceAllocation->GetMappedPointer();
}

uint32 StagingBuffer::GetAllocationOffset() const
{
	return m_ResourceAllocation->GetOffset();
}

uint32 StagingBuffer::GetDeviceMemoryAllocationSize() const
{
	return m_ResourceAllocation->GetAllocationSize();
}

VkDeviceMemory StagingBuffer::GetDeviceMemoryHandle() const
{
	return m_ResourceAllocation->GetHandle();
}

//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
RingBuffer::RingBuffer(VulkanDevice* device, uint64 totalSize, VkFlags usage, VkMemoryPropertyFlags memoryPropertyFlags)
: m_Device(device)
, m_TotalSize(totalSize)
{
	m_BufferSubAllocation = m_Device->ResourceHeapPool().AllocateBuffer(totalSize, usage, memoryPropertyFlags, __FILE__, __LINE__);
	m_BufferOffset = m_BufferSubAllocation->GetOffset();
	m_Alignment = m_BufferSubAllocation->GetBufferAllocation()->GetAlignment();
}
RingBuffer::~RingBuffer()
{
	delete m_BufferSubAllocation;
}

uint64 RingBuffer::AllocateMemory(uint64 size, uint32 alignment)
{
	m_Alignment = Math::Max<uint32>(alignment, m_Alignment);
	uint64 allocateOffset = Memory::Align(m_BufferOffset, m_Alignment);

	if ((allocateOffset + size) >= m_TotalSize)
	{
		allocateOffset = 0;
	}

	m_BufferOffset = allocateOffset + size;

	return allocateOffset;
}

} // end of namespace