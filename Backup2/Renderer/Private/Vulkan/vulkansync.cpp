#include "renderer.h"


/*	세마포어
	- 세마포어는 Queue 동기화를 위해서 사용된다.
	- 하나의 Queue는 어떤 처리가 끝났을 때 Semaphore 하나를 Signal한다.
	- 다른 Queue는 Semaphore가 Signal될 때까지 Semaphore에서 대기한다.
	- 그리고 나서, Queue는 Command Buffer들을 통해서 Submit된 처리를 수행할 것이다.
*/

VulkanSemaphore::VulkanSemaphore(VulkanDevice* device)
	: m_Device(device)
	, m_Semaphore(VK_NULL_HANDLE)
{
	VkSemaphoreCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkResult result = ::vkCreateSemaphore(device->GetHandle(), &createInfo, nullptr, &m_Semaphore);
	_ASSERT(result == VK_SUCCESS);
}
VulkanSemaphore::~VulkanSemaphore()
{
	::vkDestroySemaphore(m_Device->GetHandle(), m_Semaphore, nullptr);
	m_Semaphore = VK_NULL_HANDLE;
}

///////////////////////////////////////////////////////////////////

VulkanFence::VulkanFence(VulkanDevice* device, VulkanFencePool* owner, bool bCreateSignaled)
	: m_Device(device)
	, m_Pool(owner)
	, m_Fence(VK_NULL_HANDLE)
	, m_State(bCreateSignaled ? _STATE::Signaled : _STATE::NotReady)
{
	VkFenceCreateInfo createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.flags = bCreateSignaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;
	VK_RESULT(::vkCreateFence(m_Device->GetHandle(), &createInfo, nullptr, &m_Fence));
}
VulkanFence::~VulkanFence()
{
	::vkDestroyFence(m_Device->GetHandle(), m_Fence, nullptr);
	m_Fence = VK_NULL_HANDLE;
}

///////////////////////////////////////////////////////////////////

VulkanFencePool::VulkanFencePool()
	: m_Device(nullptr)
{
}
VulkanFencePool::~VulkanFencePool()
{
}

void VulkanFencePool::Init(VulkanDevice* device)
{
	m_Device = device;
}

void VulkanFencePool::Destroy()
{
	/// Need Threading Lock

	ASSERT(m_UsedFences.empty(), "UsedFences must be empty!!!");

	for (auto it = m_FreeFences.begin(); it != m_FreeFences.end(); it++)
		delete (*it);
	m_FreeFences.clear();
}

VulkanFence* VulkanFencePool::AllocateFence(bool bCreateSignaled)
{
	if (!m_FreeFences.empty())
	{
		VulkanFence* fence = m_FreeFences[0];
		m_FreeFences.pop_front();
		m_UsedFences.push_back(fence);

		if (bCreateSignaled)
		{
			fence->m_State = VulkanFence::_STATE::Signaled;
		}
		return fence;
	}

	VulkanFence* fence = new VulkanFence(m_Device, this, bCreateSignaled);
	m_UsedFences.push_back(fence);

	return fence;
}

void VulkanFencePool::ReleaseFence(VulkanFence*& fence)
{
	if (fence)
	{
		ResetFence(fence);

		m_UsedFences.erase(std::find(m_UsedFences.begin(), m_UsedFences.end(), fence));
		m_FreeFences.push_back(fence);
		fence = nullptr;
	}
}

void VulkanFencePool::ResetFence(VulkanFence* fence)
{
	if (fence->m_State != VulkanFence::_STATE::NotReady)
	{
		VkFence fenceHandle = fence->GetHandle();
		VK_RESULT(vkResetFences(m_Device->GetHandle(), 1, &fenceHandle));

		fence->m_State = VulkanFence::_STATE::NotReady;
	}
}

bool VulkanFencePool::CheckFenceState(VulkanFence* fence)
{
	VkResult result = vkGetFenceStatus(m_Device->GetHandle(), fence->GetHandle());
	if (result == VK_SUCCESS)
	{
		fence->m_State = VulkanFence::_STATE::Signaled;
		return true;
	}
	else if (result == VK_NOT_READY)
	{
		return false;
	}

	VK_RESULT(result);
	return false;
}

bool VulkanFencePool::WaitForFence(VulkanFence* fence, uint64 timeout)
{
	VkFence fenceHandle = fence->GetHandle();
	VkResult result = vkWaitForFences(m_Device->GetHandle(), 1, &fenceHandle, true, timeout);
	if (result == VK_SUCCESS)
	{
		fence->m_State = VulkanFence::_STATE::Signaled;
		return true;
	}
	else if (result == VK_TIMEOUT)
	{
		return false;
	}

	VK_RESULT(result);
	return false;
}

void VulkanFencePool::WaitAndReleaseFence(VulkanFence*& fence, uint64 timeout)
{
	if (fence->IsSignaled() == false)
	{
		WaitForFence(fence, timeout);
	}

	ResetFence(fence);
	m_UsedFences.erase(std::find(m_UsedFences.begin(), m_UsedFences.end(), fence));
	m_FreeFences.push_back(fence);
	fence = nullptr;
}

bool VulkanFencePool::IsSignaled(VulkanFence* fence)
{
	if (fence->IsSignaled())
	{
		return true;
	}

	return CheckFenceState(fence);
}