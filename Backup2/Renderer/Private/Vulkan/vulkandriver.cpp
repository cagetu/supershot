#include "renderer.h"

VulkanDriver::VulkanDriver()
	: m_Instance(nullptr)
	, m_Device(nullptr)
	, m_Viewport(nullptr)
{
	m_BackBuffer = nullptr;

	m_Pipeline = nullptr;
	m_VertexBuffer = nullptr;
	m_IndexBuffer = nullptr;
	m_Shader = nullptr;
	m_RenderState = nullptr;
	PlatformUtil::Memset(m_RenderStateFilter, 0, sizeof(m_RenderStateFilter));
}

VulkanDriver::~VulkanDriver()
{
	if (m_RenderState != NULL)
	{
		delete m_RenderState;
		m_RenderState = NULL;
	}

	if (m_Viewport)
	{
		m_Viewport->Destroy();
		delete m_Viewport;
	}
	m_Viewport = nullptr;

	DestroyInstance();
}

bool VulkanDriver::Create(const IDisplayContext* displayHandle)
{
	if (CreateInstance() == false)
		return false;

	if (CreateDevice() == false)
		return false;

	m_RenderState = new VulkanRenderState(this);
	m_RenderState->Reset();
	return true;
}

void VulkanDriver::Destroy()
{
	DestroyDevice();
	DestroyInstance();
}

bool VulkanDriver::CreateInstance()
{
	std::vector<const char*> instanceLayers;
	std::vector<const char*> instanceExtensions;
	GetInstanceLayersAndExtensions(instanceExtensions, instanceLayers);

	// 어플리케이션 정보 설정
	VkApplicationInfo app;
	{
		PlatformUtil::Memzero(&app, sizeof(app));
		app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		app.pApplicationName = "VisualShock";
		app.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
		app.engineVersion = VK_MAKE_VERSION(0, 0, 1);
		app.apiVersion = VULKAN_API_VERSION;
		app.pEngineName = "VisualShock";
		app.pNext = NULL;
	}

	// 벌칸 인스턴스 생성 정보
	VkInstanceCreateInfo inst;
	{
		PlatformUtil::Memzero(&inst, sizeof(inst));
		inst.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		inst.pNext = nullptr;
		inst.pApplicationInfo = &app;
		inst.enabledExtensionCount = instanceExtensions.size();
		inst.ppEnabledExtensionNames = inst.enabledExtensionCount > 0 ? &instanceExtensions[0] : nullptr;
		inst.flags = 0;

#if VULKAN_HAS_DEBUGGING_ENABLED
		inst.enabledLayerCount = instanceLayers.size();
		inst.ppEnabledLayerNames = inst.enabledLayerCount > 0 ? &instanceLayers[0] : nullptr;
#endif
	}

	// 인스턴스 생성
	VkResult result = ::vkCreateInstance(&inst, nullptr, &m_Instance);
	if (result != VK_SUCCESS)
	{
		if (result == VK_ERROR_LAYER_NOT_PRESENT)
			PlatformUtil::DebugOutFormat("vkCreateInstance is failed!! [VK_ERROR_LAYER_NOT_PRESENT] Specified layer is not found.\n");
		else if (result == VK_ERROR_EXTENSION_NOT_PRESENT)
			PlatformUtil::DebugOutFormat("vkCreateInstance is failed!! [VK_ERROR_EXTENSION_NOT_PRESENT]  Specified extensions is not found.\n", result);
		return false;
	}

	return true;
}

void VulkanDriver::DestroyInstance()
{
	if (m_Instance)
	{
		::vkDestroyInstance(m_Instance, nullptr);
		m_Instance = VK_NULL_HANDLE;
	}
}

bool VulkanDriver::CreateDevice()
{
	/*
	- GPU 리스트 검색
	- GPU 프로퍼티
	- Main GPU 선택
	*/

	//
	// 1. 장착된 모든 그래픽 카드 핸들 확보
	//
	uint32 gpuCount = 0;
	::vkEnumeratePhysicalDevices(m_Instance, &gpuCount, nullptr);

	std::vector<VkPhysicalDevice> physicalDevices(gpuCount);
	::vkEnumeratePhysicalDevices(m_Instance, &gpuCount, &physicalDevices[0]);

	//
	// 2. 그래픽 카드 수집
	//
	std::vector<VulkanDevice*> candidateGPU;

	for (uint32 i = 0; i < gpuCount; i++)
	{
		VulkanDevice* device = new VulkanDevice(physicalDevices[i]);

		bool isSupportGPU = device->IsSupportGPU(VK_QUEUE_GRAPHICS_BIT);

		if (isSupportGPU)
		{
			candidateGPU.push_back(device);
		}

		m_Devices.push_back(device);
	}

	//
	// 3. 그래픽 카드 선택 및 생성
	//
	for (uint32 index = 0; index < candidateGPU.size(); index++)
	{
		VulkanDevice* device = candidateGPU[index];

		bool isDiscrete = device->IsDiscreteGPU();
		bool isLastGPU = (index == gpuCount - 1) ? true : false;

		if (isDiscrete || isLastGPU)
		{
			if (device->Create() == true)
				m_Device = device;
		}
	}

	return m_Device != nullptr;
}

void VulkanDriver::DestroyDevice()
{
	m_Device->PrepareForDestroy();

	// 선택된 디바이스 삭제
	m_Device->Destroy();

	// Device 목록 삭제
	for (VulkanDevice* device : m_Devices)
		delete device;
	m_Devices.clear();
}

void VulkanDriver::Clear(COLOR clearColor, float clearDepth, ushort clearStencil, uint32 clearFlags)
{
	// 벌칸에서는 렌터타겟에 종속적이다. 렌더타겟에 설정하도록 한다.
	//m_ClearFlags = clearFlags;
	//m_ClearColor = clearColor;
	//m_ClearDepth = clearDepth;
	//m_ClearStencil = clearStencil;
}

/*
	BeginScene(RT);
	ClearColor(xxxx);
	...
	내부적으로 값을 저장하고 있다가 지연처리하면 인터페이스를 맞출 수 있지 않을까???
	EndScene();
*/

void VulkanDriver::BeginScene(IRenderTarget* rt, uint32 rs_or, uint32 rs_and, int32 clearFlags, COLOR clearColor, float clearDepth, ushort clearStencil)
{
	SetRenderTarget(rt);

	// 렌더 스테이트
	m_RenderStateFilter[0] = rs_or;
	m_RenderStateFilter[1] = ~rs_and;
	SetRenderState(0);

	int32 width = 0;
	int32 height = 0;

	if (rt)
	{
		// RenderTarget을 사용...
	}
	else
	{
		// Swapchain 이미지를 이용해서 기본 Backbuffer 사용...
		m_Viewport->Begin();

		width = m_Viewport->GetWidth();
		height = m_Viewport->GetHeight();

		if (m_BackBuffer == nullptr || 
			(m_BackBuffer != nullptr && (m_BackBuffer->GetWidth() != width || m_BackBuffer->GetHeight() != height)))
		{
			bool result = false;

			m_BackBuffer = new VulkanRenderTarget(GetDevice());
			result = m_BackBuffer->Create(width, height);
			_ASSERT(result);

			VulkanDepthStencilSurface* pDepthStencilBuffer = new VulkanDepthStencilSurface(GetDevice());
			result = pDepthStencilBuffer->Create(width, height, RT_D24S8, 1);
			_ASSERT(result);

			m_BackBuffer->SetDepthBuffer(pDepthStencilBuffer, (clearFlags&CLEAR_DEPTH) ? RTLoadAction::EClear : RTLoadAction::ENoAction, RTStoreAction::EStore, (clearFlags&CLEAR_STENCIL) ? RTLoadAction::EClear : RTLoadAction::ENoAction, RTStoreAction::EStore);
		}

		m_BackBuffer->SetRenderTexture(m_Viewport->GetBackBuffer().DynamicCast<VulkanRenderTexture2D>(), (clearFlags&CLEAR_COLOR) ? RTLoadAction::EClear : RTLoadAction::ENoAction, RTStoreAction::EStore);
		m_BackBuffer->SetClearColor(clearColor);
		m_BackBuffer->SetClearDepth(clearDepth);
		m_BackBuffer->SetClearStencil(clearStencil);

		const Vulkan::RenderTargetDesc& RTDesc = m_BackBuffer->GetDescription();

		VulkanRenderPass* renderPass = FindOrAddRenderPass(RTDesc);
		VulkanFrameBuffer* frameBuffer = FindOrAddFrameBuffer(renderPass, RTDesc);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		SetRenderPass(renderPass);
		SetViewportRect(0.0f, 0.0f, width, height);
		SetScissorRect(0.0f, 0.0f, width, height);

#ifdef _SYNC_CMDBUFFER
		VulkanCmdBuffer* activeCmdBuffer = m_Device->CommandBufferPool().GetActiveCommandBuffer();
		_ASSERT(activeCmdBuffer);

		activeCmdBuffer->BeginRenderPass(frameBuffer);
		activeCmdBuffer->SetViewport(width, height);
		activeCmdBuffer->SetScissor(width, height);
#else
		int32 currentImageIndex = m_Viewport->GetAcquiredImageIndex();
		m_ActiveCmdBuffer = m_Device->CommandBufferPool().GetCmdBuffer(currentImageIndex);

		m_ActiveCmdBuffer->Begin(false);
		m_ActiveCmdBuffer->BeginRenderPass(frameBuffer);

		m_ActiveCmdBuffer->SetViewport(width, height);
		m_ActiveCmdBuffer->SetScissor(width, height);
#endif
	}

	//if (clearFlags > 0)
	//{
	//	Clear(clearColor, clearDepth, clearStencil, clearFlags);
	//}
}

void VulkanDriver::EndScene()
{
#ifdef _SYNC_CMDBUFFER
	VulkanCmdBuffer* CmdBuffer = m_Device->CommandBufferPool().GetActiveCommandBuffer();

	CmdBuffer->EndRenderPass();
	//CmdBuffer->End();
#else
	int32 currentImageIndex = m_Viewport->GetAcquiredImageIndex();
	VulkanCmdBuffer* CmdBuffer = m_Device->CommandBufferPool().GetCmdBuffer(currentImageIndex);

	CmdBuffer->EndRenderPass();
	//CmdBuffer->End();
#endif
	m_Viewport->End();
}

void VulkanDriver::Present()
{
	m_Viewport->Present();
}

void VulkanDriver::DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint StartVertex, uint VertexCount, uint32 StartInstance, uint32 InstanceCount, uint32 NumControlPoints)
{
	SetPrimitiveTopology(PrimitiveType);

	VulkanGfxPipeline* pipelineInstance = m_Device->GfxPipelineManager()->FindOrCreate(m_GfxPipelineContext);

#ifdef _SYNC_CMDBUFFER
	VulkanCmdBuffer* activeCmdBuffer = m_Device->CommandBufferPool().GetActiveCommandBuffer();
	ASSERT(activeCmdBuffer, "activeCmdBuffer is NULL");

	activeCmdBuffer->BindPipeline(pipelineInstance);

	activeCmdBuffer->BindDescriptorSets(m_GfxPipelineContext.Shader);
	activeCmdBuffer->BindVertexBuffer(m_VertexBuffer, nullptr);

	activeCmdBuffer->Draw(StartVertex, VertexCount, StartInstance, InstanceCount);
#else
	m_ActiveCmdBuffer->BindPipeline(pipelineInstance);

	m_ActiveCmdBuffer->BindDescriptorSets(m_GfxPipelineContext.Shader);
	m_ActiveCmdBuffer->BindVertexBuffer(m_VertexBuffer, nullptr);

	m_ActiveCmdBuffer->Draw(StartVertex, VertexCount, StartInstance, InstanceCount);
#endif
}

void VulkanDriver::DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint BaseVertex, uint StartIndex, uint IndexCount, IndexBuffer* IdxBuffer, uint32 StartInstance, uint32 InstanceCount, uint32 NumControlPoints)
{
	SetPrimitiveTopology(PrimitiveType);

	VulkanGfxPipeline* pipelineInstance = m_Device->GfxPipelineManager()->FindOrCreate(m_GfxPipelineContext);

#ifdef _SYNC_CMDBUFFER
	VulkanCmdBuffer* activeCmdBuffer = m_Device->CommandBufferPool().GetActiveCommandBuffer();
	ASSERT(activeCmdBuffer, "activeCmdBuffer is NULL");

	activeCmdBuffer->BindPipeline(pipelineInstance);

	activeCmdBuffer->BindDescriptorSets(m_GfxPipelineContext.Shader);
	activeCmdBuffer->BindVertexBuffer(m_VertexBuffer, nullptr);
	activeCmdBuffer->BindIndexBuffer(m_IndexBuffer, StartIndex);

	activeCmdBuffer->DrawIndexed(BaseVertex, StartIndex, IndexCount, StartInstance, InstanceCount);
#else
	m_ActiveCmdBuffer->BindPipeline(pipelineInstance);

	m_ActiveCmdBuffer->BindDescriptorSets(m_GfxPipelineContext.Shader);
	m_ActiveCmdBuffer->BindVertexBuffer(m_VertexBuffer, nullptr);
	m_ActiveCmdBuffer->BindIndexBuffer(m_IndexBuffer, StartIndex);

	m_ActiveCmdBuffer->DrawIndexed(BaseVertex, StartIndex, IndexCount, StartInstance, InstanceCount);
#endif
}

void VulkanDriver::SetVertexBuffer(VertexBuffer* vertexBuffer)
{
	m_VertexBuffer = vertexBuffer ? DynamicCast<VulkanVertexBuffer>(vertexBuffer) : nullptr;
}

void VulkanDriver::SetIndexBuffer(IndexBuffer* indexBuffer)
{
	m_IndexBuffer = indexBuffer ? DynamicCast<VulkanIndexBuffer>(indexBuffer) : nullptr;
}

void VulkanDriver::SetRenderTarget(IRenderTarget* renderTarget)
{
	m_RenderTarget = renderTarget ? DynamicCast<VulkanRenderTarget>(renderTarget) : nullptr;
}

void VulkanDriver::DiscardRenderTarget(bool depth, bool stencil, uint32 colors)
{
	// Blank.. Blank.. Blank...
}

VulkanRenderPass* VulkanDriver::FindOrAddRenderPass(const Vulkan::RenderTargetDesc& RTDesc)
{
	// RenderTargetDescription이 변경되면 RenderPass가 변경되어야 한다.

	for (VulkanRenderPass* renderPass : m_RenderPasses)
	{
		if (renderPass->GetRenderTargetDesc() == RTDesc)
		{
			return renderPass;
		}
	}

	VulkanRenderPass* newRenderPass = new VulkanRenderPass(m_Device, RTDesc);
	m_RenderPasses.push_back(newRenderPass);

	return newRenderPass;
}

VulkanFrameBuffer* VulkanDriver::FindOrAddFrameBuffer(VulkanRenderPass* renderPass, const Vulkan::RenderTargetDesc& RTDesc)
{
	// 1. RTDesc가 이전 프레임과 다르다면, 새롭게 FrameBuffer를 생성해야 한다.
	// 2. RenderPass가 이전 프레임과 다르다면 새롭게 FrameBuffer를 생성해야 한다.

	for (VulkanFrameBuffer* frameBuffer : m_FrameBuffers)
	{
		if (frameBuffer->GetRenderTargetDesc() == RTDesc)
		{
			return frameBuffer;
		}
	}

	VulkanFrameBuffer* newFrameBuffer = new VulkanFrameBuffer(m_Device, renderPass, RTDesc);
	m_FrameBuffers.push_back(newFrameBuffer);

	return newFrameBuffer;
}

IViewport* VulkanDriver::CreateViewport()
{
	return new VulkanViewport(this);
}

void VulkanDriver::DestroyViewport(IViewport* viewport)
{
	viewport->Destroy();
	delete viewport;
}

void VulkanDriver::SetViewport(IViewport* viewport)
{
	m_Viewport = (VulkanViewport*)viewport;

	m_Width = m_Viewport->GetWidth();
	m_Height = m_Viewport->GetHeight();
}

VertexBuffer* VulkanDriver::CreateVertexBuffer()
{
	return new VulkanVertexBuffer(m_Device);
}

void VulkanDriver::DestroyVertexBuffer(VertexBuffer* vertexBuffer)
{
	delete vertexBuffer;
}

IndexBuffer* VulkanDriver::CreateIndexBuffer()
{
	return new VulkanIndexBuffer(m_Device);
}

void VulkanDriver::DestroyIndexBuffer(IndexBuffer* indexBuffer)
{
	delete indexBuffer;
}

IShader* VulkanDriver::CreateShader()
{
	return new VulkanShader(m_Device);
}

void VulkanDriver::DestroyShader(IShader* shader)
{
	delete shader;
}

void VulkanDriver::SetShader(IShader* shader, const VertexBuffer* vertexBuffer, const CMaterial* material)
{
	if (m_Shader)
	{
		m_Shader->ClearVertexAttributes();
		m_Shader->ClearValues();
	}

	VulkanShader* newShader = DynamicCast<VulkanShader>(shader);
	if (newShader)
	{
		newShader->UpdateVertexAttributes(vertexBuffer);
		newShader->UpdateValues(material);
	}

	m_Shader = newShader;
	//SetVertexDeclaration(m_Shader->GetVertexInput());

	m_GfxPipelineContext.SetShader(newShader);
}

void VulkanDriver::SetRenderState(uint32 rs)
{
	m_RenderState->Update(rs);
}

void VulkanDriver::SetRenderPass(VulkanRenderPass* renderPass, int32 SubpassIndex)
{
	m_GfxPipelineContext.RenderPass = renderPass;
	m_GfxPipelineContext.SubPassIndex = SubpassIndex;
}

void VulkanDriver::SetViewportRect(int32 x, int32 y, float width, float height, float minDepth, float maxDepth)
{
	m_GfxPipelineContext.ViewportState.x = x;
	m_GfxPipelineContext.ViewportState.y = y;
	m_GfxPipelineContext.ViewportState.width = width;
	m_GfxPipelineContext.ViewportState.height = height;
	m_GfxPipelineContext.ViewportState.minDepth = minDepth;
	m_GfxPipelineContext.ViewportState.maxDepth = maxDepth;
}

void VulkanDriver::SetScissorRect(int32 x, int32 y, int32 width, int32 height)
{
	m_GfxPipelineContext.ScissorState.x = x;
	m_GfxPipelineContext.ScissorState.y = y;
	m_GfxPipelineContext.ScissorState.width = width;
	m_GfxPipelineContext.ScissorState.height = height;
}

void VulkanDriver::SetPrimitiveTopology(PRIMITIVETYPE topology)
{
	m_GfxPipelineContext.InputAssemblyState.topology = topology;
}

void VulkanDriver::SetVertexDeclaration(VulkanVertexDeclaration* vertexDeclaration)
{
	m_GfxPipelineContext.VertexInputState.layout = vertexDeclaration;
}

void VulkanDriver::SetPatchControlPoints(uint32 controlPoints)
{
	m_GfxPipelineContext.TessellationState.patchControlPoints = controlPoints;
}

void VulkanDriver::SetRasterizerEnable(bool bEnable)
{
	m_GfxPipelineContext.RasterizationState.bSkipRasterizer = bEnable ? false : true;
}

void VulkanDriver::SetFillMode(FILLMODE::TYPE type)
{
	m_GfxPipelineContext.RasterizationState.FillMode = type;
}

void VulkanDriver::SetCullMode(CULLMODE::TYPE type)
{
	m_GfxPipelineContext.RasterizationState.CullMode = type;
}

void VulkanDriver::SetDepthTestEnable(bool bEnable)
{
	m_GfxPipelineContext.DepthTestState.bDepthTestEnable = bEnable;
}

void VulkanDriver::SetDepthWriteEnable(bool bEnable)
{
	m_GfxPipelineContext.DepthTestState.bDepthWriteEnable = bEnable;
}

void VulkanDriver::SetDepthFunc(COMPAREFUNC::TYPE type)
{
	m_GfxPipelineContext.DepthTestState.DepthCompareOp = type;
}

void VulkanDriver::SetDepthBoundTest(bool bEnable)
{
	m_GfxPipelineContext.DepthBoundTestState.bDepthBoundTestEnable = bEnable;
}

void VulkanDriver::SetDepthBound(float minDepthBound, float maxDepthBound)
{
	m_GfxPipelineContext.DepthBoundTestState.minDepthBound = minDepthBound;
	m_GfxPipelineContext.DepthBoundTestState.maxDepthBound = maxDepthBound;
}

void VulkanDriver::SetStencilTestEnable(bool bEnable)
{
	m_GfxPipelineContext.StencilTestState.bStencilTestEnable = bEnable;
}

void VulkanDriver::SetStencilFunc(COMPAREFUNC::TYPE compareFunc, int32 compareMask, int32 reference, bool bFrontFace)
{
	PipelineState::StencilTestDesc::StencilOp* stencilDesc = bFrontFace ? &m_GfxPipelineContext.StencilTestState.front : &m_GfxPipelineContext.StencilTestState.back;
	stencilDesc->compareOp = compareFunc;
	stencilDesc->compareMask = compareMask;
	stencilDesc->reference = reference;
}

void VulkanDriver::SetStencilMode(STENCILFUNC::TYPE failOp, STENCILFUNC::TYPE passOp, STENCILFUNC::TYPE depthFailOp, bool bFrontFace)
{
	PipelineState::StencilTestDesc::StencilOp* stencilDesc = bFrontFace ? &m_GfxPipelineContext.StencilTestState.front : &m_GfxPipelineContext.StencilTestState.back;
	stencilDesc->failOp = failOp;
	stencilDesc->passOp = passOp;
	stencilDesc->depthFailOp = depthFailOp;
}

void VulkanDriver::SetStencilWriteMask(int32 writeMask, bool bFrontFace)
{
	PipelineState::StencilTestDesc::StencilOp* stencilDesc = bFrontFace ? &m_GfxPipelineContext.StencilTestState.front : &m_GfxPipelineContext.StencilTestState.back;
	stencilDesc->writeMask = writeMask;
}

void VulkanDriver::SetBlendEnable(bool bEnable, int32 attacnmentIndex)
{
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].bBlendEnable = bEnable;
}

void VulkanDriver::SetBlendColorFunc(BLENDFACTOR::TYPE srcFactor, BLENDFACTOR::TYPE destFactor, int32 attacnmentIndex, BLENDFUNC::TYPE mode)
{
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].colorBlendOp = mode;
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].srcColorBlendFactor = srcFactor;
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].dstColorBlendFactor = destFactor;
}

void VulkanDriver::SetBlendAlphaFunc(BLENDFACTOR::TYPE srcFactor, BLENDFACTOR::TYPE destFactor, int32 attacnmentIndex, BLENDFUNC::TYPE mode)
{
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].alphaBlendOp = mode;
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].srcAlphaBlendFactor = srcFactor;
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].dstAlphaBlendFactor = destFactor;
}

void VulkanDriver::SetBlendWriteMask(int32 writeMask, int32 attacnmentIndex)
{
	m_GfxPipelineContext.ColorBlendState[attacnmentIndex].colorWriteMask = writeMask;
}

void VulkanDriver::ClearStates()
{
	m_GfxPipelineContext.Clear();
}