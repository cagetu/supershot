#include "renderer.h"

__ImplementRtti(VisualShock, VulkanShader, IShader);

/////////////////////////////////////////////////////////////////////////////////////

VulkanShader::VulkanShader(VulkanDevice* device)
	: m_Device(device)
	, m_VertexInput(nullptr)
	, m_DescriptorSets(nullptr)
	, m_DescriptorSetsLayout(nullptr)
	, m_PipelineLayout(nullptr)
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		m_ShaderPrograms[index] = NULL;
	}
}
VulkanShader::~VulkanShader()
{
}

bool VulkanShader::Create(const SHADERCODE& shaderCode, ShaderFeature::MASK mask)
{
	// 1. DescriptorSet 생성
	m_DescriptorSetsLayout = new VulkanDescriptorSetsLayout(m_Device);

	// 2. 모든 셰이더 생성 및 초기화
	for (int32 type = 0; type < ShaderType::NUM; type++)
	{
		if (shaderCode.IsValid(type))
			continue;

		// 타입 별 셰이더를 생성한다.
		VulkanShaderProgram* shaderProgram = VulkanShaderProgram::NewObject(m_Device, this, shaderCode.GetType(type));
		_ASSERT(shaderProgram != nullptr);

		if (!shaderProgram->Create(shaderCode.GetID(type), mask))
		{
			delete shaderProgram;
			continue;
		}

		m_ShaderPrograms[type] = shaderProgram;
	}

	// 3. DescriptorSetLayout 생성
	m_DescriptorSetsLayout->Compile();
	// 4. PipelineLayout 생성
	m_PipelineLayout = new VulkanPipelineLayout(m_Device, m_DescriptorSetsLayout);
	// 5. DescriptorSet 생성
	// UE4에서는 같은 CommandBuffer라면, DescriptorSet들을 Pooling해서 사용하고 싶어한다. (FVulkanShaderState::RequestDescriptorSets 참고)
	m_DescriptorSets = new VulkanDescriptorSets(m_Device, m_DescriptorSetsLayout);

	// 6. DescriptorSets Binding
	UpdateDescriptorSets();
	return true;
}

void VulkanShader::Destroy()
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		if (m_ShaderPrograms[index])
		{
			delete m_ShaderPrograms[index];
			m_ShaderPrograms[index] = NULL;
		}
	}

	if (m_DescriptorSetsLayout)
		delete m_DescriptorSetsLayout;
	m_DescriptorSetsLayout = nullptr;

	if (m_PipelineLayout)
		delete m_PipelineLayout;
	m_PipelineLayout = nullptr;

	if (m_DescriptorSets)
		delete m_DescriptorSets;
	m_DescriptorSets = nullptr;
}

bool VulkanShader::CompareTo(VulkanShader* Other)
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		VulkanShaderProgram* srcShaderProgram = GetShaderProgram(index);
		VulkanShaderProgram* dstShaderProgram = Other->GetShaderProgram(index);
		if (srcShaderProgram && dstShaderProgram)
		{
			if (srcShaderProgram->GetShaderResource()->Handle() != dstShaderProgram->GetShaderResource()->Handle())
				return false;
		}
	}
	return true;
}

void VulkanShader::Begin(const CMaterial* material, const VertexBuffer* vb)
{
	GetRenderDriver()->SetShader(this, vb, material);
}

void VulkanShader::End()
{
}

void VulkanShader::SetValue(uint32 index, const Variant& value)
{
	_ASSERT(0);
}

void VulkanShader::UpdateValues(const CMaterial* material)
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		if (m_ShaderPrograms[index])
		{
			m_ShaderPrograms[index]->UpdateValues(material);
		}
	}
}

void VulkanShader::ClearValues()
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		if (m_ShaderPrograms[index])
		{
			m_ShaderPrograms[index]->ClearValues();
		}
	}
}

bool VulkanShader::CreateVertexDeclaration(VertexDescription* vertexDesc)
{
	_ASSERT(m_VertexInput == nullptr);

	m_VertexInput = new VulkanVertexDeclaration();
	if (m_VertexInput->Create(vertexDesc) == false)
	{
		m_VertexInput = nullptr;
		return false;
	}
	return true;
}

void VulkanShader::DestroyVertexDeclaration()
{
	m_VertexInput = nullptr;
}

void VulkanShader::UpdateVertexAttributes(const VertexBuffer* vertexBuffer)
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		if (m_ShaderPrograms[index])
		{
			m_ShaderPrograms[index]->UpdateVertexAttributes(vertexBuffer);
		}
	}
}

void VulkanShader::ClearVertexAttributes()
{
	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		if (m_ShaderPrograms[index])
		{
			m_ShaderPrograms[index]->ClearVertexAttributes();
		}
	}
}

void VulkanShader::AddDescriptor(int32 descriptorSetIndex, const VkDescriptorSetLayoutBinding& descriptorBinding)
{
	m_DescriptorSetsLayout->AddDescriptor(descriptorSetIndex, descriptorBinding);
}

void VulkanShader::WriteTextureDescriptorSet(uint32 binding, int32 descriptorSetIndex, VkDescriptorImageInfo* imageInfo)
{
	VkWriteDescriptorSet writeDescriptorSet;
	Memory::Memzero(writeDescriptorSet);
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;	// VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC ?? 이건 어디에 쓸까???
	writeDescriptorSet.dstBinding = binding;
	writeDescriptorSet.dstSet = m_DescriptorSets->GetHandle(descriptorSetIndex);
	writeDescriptorSet.pImageInfo = imageInfo;

	m_DescriptorWrites.push_back(writeDescriptorSet);
}

void VulkanShader::WriteRenderTargetDescriptorSet(uint32 binding, int32 descriptorSetIndex, VkDescriptorImageInfo* imageInfo)
{
	VkWriteDescriptorSet writeDescriptorSet;
	Memory::Memzero(writeDescriptorSet);
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;	// VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC ?? 이건 어디에 쓸까???
	writeDescriptorSet.dstBinding = binding;
	writeDescriptorSet.dstSet = m_DescriptorSets->GetHandle(descriptorSetIndex);
	writeDescriptorSet.pImageInfo = imageInfo;

	m_DescriptorWrites.push_back(writeDescriptorSet);
}

void VulkanShader::WriteUniformBufferDescriptorSet(uint32 binding, int32 descriptorSetIndex, VkDescriptorBufferInfo* bufferInfo)
{
	VkWriteDescriptorSet writeDescriptorSet;
	Memory::Memzero(writeDescriptorSet);
	writeDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeDescriptorSet.descriptorCount = 1;
	writeDescriptorSet.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;	// VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC ?? 이건 어디에 쓸까???
	writeDescriptorSet.dstBinding = binding;
	writeDescriptorSet.dstSet = m_DescriptorSets->GetHandle(descriptorSetIndex);
	writeDescriptorSet.pBufferInfo = bufferInfo;

	m_DescriptorWrites.push_back(writeDescriptorSet);
}

void VulkanShader::UpdateDescriptorSets()
{
	m_DescriptorWrites.clear();

	for (int32 index = 0; index < ShaderType::NUM; index++)
	{
		if (m_ShaderPrograms[index])
		{
			m_ShaderPrograms[index]->UpdateDescriptorSets();
		}
	}

	vkUpdateDescriptorSets(m_Device->GetHandle(), m_DescriptorWrites.size(), m_DescriptorWrites.data(), 0, NULL);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, VulkanShaderProgram, IShaderProgram);

VulkanShaderProgram::VulkanShaderProgram(VulkanDevice* Device, VulkanShader* InOwner, ShaderType::MASK shaderTypes)
	: IShaderProgram(shaderTypes)
	, m_Device(Device)
	, m_Owner(InOwner)
	, m_UniformBuffers(nullptr)
	, m_NumUniformBuffers(0)
	, m_UniformParams(nullptr)
	, m_NumUniformParams(0)
{
}
VulkanShaderProgram::~VulkanShaderProgram()
{
}

ShaderType::TYPE VulkanShaderProgram::GetShaderType() const
{
	if (m_ShaderTypeBits&ShaderType::VERTEX_BIT)	return ShaderType::VERTEXSHADER;
	if (m_ShaderTypeBits&ShaderType::FRAGMENT_BIT)	return ShaderType::FRAGMENTSHADER;
	if (m_ShaderTypeBits&ShaderType::COMPUTE_BIT)	return ShaderType::COMPUTESHADER;
	if (m_ShaderTypeBits&ShaderType::GEOMETRY_BIT)	return ShaderType::GEOMETRYSHADER;

	return ShaderType::_INVALID;
}

bool VulkanShaderProgram::Create(const unicode::string& filename, ShaderFeature::MASK mask)
{
	VulkanShaderResource* shaderResource = DynamicCast<VulkanShaderResource>(ShaderCore::Build(filename.c_str(), GetShaderType(), mask));
	if (shaderResource == NULL)
	{
		return false;
	}

	m_ShaderResource = shaderResource;

	InitDescriptors(m_Device);
	InitShaderParams(m_Device);

	// Command Buffer에 셰이더 정보를 추가할 때에는 이 DescriptorSets으로 해준다.
	// 이건 UniformBuffer를 업데이트할 때, Device에 요청하는 방식으로 처리한다. (feat.UE4)
	// 같은 DescriptorSetLayout이라면, pipeline에서 DescriptorSet을 상호 교환할 수 있다.

	// UE4에서는 같은 CommandBuffer라면, DescriptorSet들을 Pooling해서 사용하고 싶어한다. (FVulkanShaderState::RequestDescriptorSets 참고)
	//m_DescriptorSets = new VulkanDescriptorSets(m_Device, m_DescriptorSetsLayout);

	// 실제 값을 연동할 UniformBuffer / Samplers 등을 만든다.
	// uniform buffers

	/*	[업데이트]
	1. 생성한 UniformBuffer의 정보를 이용해서 VkDescriptorBufferInfo 만들기
	2. VkWriteDescriptorSet 구조체에 pBufferInfo에 연결
	3. vkUpdateDescriptorSets 으로 VkWriteDescriptorSet를 넣어서 업데이트
	*/
	//m_DescriptorBufferInfos.resize(m_ShaderResource->NumUniformBuffers());
	//m_DescriptorImageInfos.resize(m_ShaderResource->NumImages());

	// Shader에 작성되어 있는 attribute와 uniform 정보를 얻어올 수 있는 방법이 없을까???
	//m_Device->AttachShader(m_ShaderResource);
	return true;
}

void VulkanShaderProgram::Destroy()
{
	if (m_UniformBuffers)
		delete[] m_UniformBuffers;
	m_UniformBuffers = nullptr;
	m_NumUniformBuffers = 0;

	if (m_UniformParams)
		delete[] m_UniformParams;
	m_UniformParams = nullptr;
	m_NumUniformParams = 0;
}

void VulkanShaderProgram::InitDescriptors(VulkanDevice* InDevice)
{
	VkDescriptorSetLayoutBinding descriptorBinding;
	Memory::Memzero(descriptorBinding);
	descriptorBinding.descriptorCount = 1;
	descriptorBinding.stageFlags = Vulkan::ShaderTypeToStage(m_ShaderResource->GetType());

	// UniformBuffer
	const std::vector<VulkanShaderResource::Uniform>& uniformBuffers = m_ShaderResource->GetUniformBuffers();
	for (auto& descriptor : uniformBuffers)
	{
		descriptorBinding.binding = descriptor.binding;
		descriptorBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		m_Owner->AddDescriptor(descriptor.descriptorSet, descriptorBinding);
	}

	// ImageSampler (Texture)
	const std::vector<VulkanShaderResource::Uniform>& images = m_ShaderResource->GetImages();
	for (auto& descriptor : images)
	{
		descriptorBinding.binding = descriptor.binding;
		descriptorBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		m_Owner->AddDescriptor(descriptor.descriptorSet, descriptorBinding);
	}

	// Sampler
	const std::vector<VulkanShaderResource::Uniform>& samplers = m_ShaderResource->GetSamplers();
	for (auto& descriptor : samplers)
	{
		descriptorBinding.binding = descriptor.binding;
		descriptorBinding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
		m_Owner->AddDescriptor(descriptor.descriptorSet, descriptorBinding);
	}

	// SampledImage
	const std::vector<VulkanShaderResource::Uniform>& sampledImages = m_ShaderResource->GetSampledImages();
	for (auto& descriptor : sampledImages)
	{
		descriptorBinding.binding = descriptor.binding;
		descriptorBinding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
		m_Owner->AddDescriptor(descriptor.descriptorSet, descriptorBinding);
	}
}

void VulkanShaderProgram::InitShaderParams(VulkanDevice* InDevice)
{
	const VkDeviceSize UBSizeAlignment = InDevice->GetLimits().minUniformBufferOffsetAlignment;
	const std::vector<VulkanShaderResource::Uniform>& uniformBufferInfos = m_ShaderResource->GetUniformBuffers();

	if (!uniformBufferInfos.empty())
	{
		m_NumUniformBuffers = uniformBufferInfos.size();
		m_NumUniformParams = 0;
		for (auto& uniformInfo : uniformBufferInfos) m_NumUniformParams += uniformInfo.members.size();

		m_UniformBuffers = new UniformBuffer[m_NumUniformBuffers];
		m_UniformParams = new UniformBuffer::Element[m_NumUniformParams];

		uint32 bufferIndex = 0;
		uint32 elementIndex = 0;
		for (uint32 bufferIndex = 0; bufferIndex < uniformBufferInfos.size(); bufferIndex++)
		{
			const VulkanShaderResource::Uniform& uniformInfo = uniformBufferInfos[bufferIndex];

			// Allocation
			m_UniformBuffers[bufferIndex].bufferInfo.offset = InDevice->GetUniformBufferUploader()->AllocateMemory(uniformInfo.size, UBSizeAlignment);
			m_UniformBuffers[bufferIndex].bufferInfo.buffer = InDevice->GetUniformBufferUploader()->GetCPUBufferHandle();
			m_UniformBuffers[bufferIndex].bufferInfo.range = uniformInfo.size;

			// UniformBuffer
			m_UniformBuffers[bufferIndex].binding = uniformInfo.binding;
			m_UniformBuffers[bufferIndex].descriptorSetIndex = uniformInfo.descriptorSet;

			for (auto& memberinfo : uniformInfo.members)
			{
				unicode::string sementic = string::convert(memberinfo.name.c_str());

				m_UniformParams[elementIndex].parentId = bufferIndex;
				m_UniformParams[elementIndex].uid = ShaderParameter::Register(sementic);
				m_UniformParams[elementIndex].offset = memberinfo.offset;
				m_UniformParams[elementIndex].value = Variant();

				elementIndex++;
			}
		}
	}
}

void VulkanShaderProgram::UpdateDescriptorSets()
{
	for (uint32 index = 0; index < m_NumUniformBuffers; index++)
	{
		UniformBuffer& buffer = m_UniformBuffers[index];
		m_Owner->WriteUniformBufferDescriptorSet(buffer.binding, buffer.descriptorSetIndex, &buffer.bufferInfo);
	}
}

void VulkanShaderProgram::SetValue(uint index, const Variant& value)
{
	UniformBuffer::Element& element = m_UniformParams[index];

	// 중복 체크
	if (element.value == value)
		return;

	element.value = value;

	UniformBuffer& uniformBuffer = m_UniformBuffers[element.parentId];
	{
		void* mappedPtr = m_Device->GetUniformBufferUploader()->GetCPUMapPointer() + uniformBuffer.bufferInfo.offset + element.offset;
		SetValueInternal(mappedPtr, value);
		uniformBuffer.bDirty = true;
	}
}

void VulkanShaderProgram::UpdateValues(const CMaterial* InMaterial)
{
	const int32 uniformCount = m_NumUniformParams;
	UniformBuffer::Element* uniformData = m_UniformParams;

	Variant value;

	for (int32 index = 0; index < uniformCount; index++)
	{
		ShaderParameter::PID pid = uniformData[index].uid;

		if (pid < ShaderParameter::_MAX_RESERVED_PARAM)		// 엔진 내부 예약된 변수
		{
			if (InMaterial && InMaterial->GetParameter(pid, value) == true)
			{
				SetValue(index, value);
			}
			else
			{
				SetValue(index, ShaderParameter::GetCommonValue(pid));
			}
		}
		else if (pid >= ShaderParameter::_MAX_PARAMCOUNT)	// 머터리얼 변수
		{
			if (InMaterial && InMaterial->GetParameter(pid, value) == true)
				SetValue(index, value);
		}
		else if (pid < ShaderParameter::_MAX_PARAMCOUNT)	// 엔진 내부 계산되는 변수
		{
			UpdateInternal(index, pid);;
		}
	}
}

void VulkanShaderProgram::ClearValues()
{
	for (uint32 index = 0; index < m_NumUniformParams; index++)
	{
		//if (m_Uniforms[index].value.type() == Variant::_Texture)
		m_UniformParams[index].value.clear();
	}
}

void VulkanShaderProgram::SetValueInternal(void* mappedPtr, const Variant& value)
{
	switch (value.type())
	{
	case Variant::_Void:
		break;
	case Variant::_Int:
	{
		int32 v = value;
		memcpy(mappedPtr, &v, value.size());
		break;
	}
	case Variant::_Float:
	{
		float v = value;
		memcpy(mappedPtr, &v, value.size());
		break;
	}
	case Variant::_Bool:
		_ASSERT(0);
		break;
	case Variant::_Vector2:
	{
		float* v = (float*)&((const Vector2&)value).x;
		memcpy(mappedPtr, v, value.size());
		break;
	}
	case Variant::_Vector3:
	{
		float* v = (float*)&((const Vector3&)value).x;
		memcpy(mappedPtr, v, value.size());
		break;
	}
	case Variant::_Vector4:
	{
		float* v = (float*)&((const Vector4&)value).x;
		memcpy(mappedPtr, v, value.size());
		break;
	}
	case Variant::_Matrix3:
	{
		float* v = (float*)&((const Mat3&)value)._11;
		memcpy(mappedPtr, v, value.size());
		break;
	}
	case Variant::_Matrix4:
	{
		float* v = (float*)&((const Mat4&)value)._11;
		memcpy(mappedPtr, v, value.size());
		break;
	}
	case Variant::_Texture:
	{
		break;
	}
	case Variant::_Float_Array:
	{
		memcpy(mappedPtr, value.GetFloatArray(), value.size());
		break;
	}
	case Variant::_Vector2_Array:
	{
		memcpy(mappedPtr, &value.GetVector2Array()[0].x, value.size());
		break;
	}
	case Variant::_Vector3_Array:
	{
		memcpy(mappedPtr, &value.GetVector3Array()[0].x, value.size());
		break;
	}
	case Variant::_Vector4_Array:
	{
		memcpy(mappedPtr, &value.GetVector4Array()[0].x, value.size());
		break;
	}
	case Variant::_Matrix_Array:
	{
		memcpy(mappedPtr, &value.GetMatrixArray()[0]._11, value.size());
		break;
	}
	default:
		_ASSERT(0);
		break;
	}
}

VulkanShaderProgram* VulkanShaderProgram::NewObject(VulkanDevice* InDevice, VulkanShader* InOwner, ShaderType::TYPE type)
{
	if (type == ShaderType::VERTEXSHADER)
	{
		return new VulkanVertexShaderProgram(InDevice, InOwner);
	}
	else if (type == ShaderType::FRAGMENTSHADER)
	{
		return new VulkanFragmentShaderProgram(InDevice, InOwner);
	}
	else if (type == ShaderType::COMPUTESHADER)
	{
		_ASSERT(0);
	}
	else if (type == ShaderType::GEOMETRYSHADER)
	{
		_ASSERT(0);
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, VulkanVertexShaderProgram, VulkanShaderProgram);

VulkanVertexShaderProgram::VulkanVertexShaderProgram(VulkanDevice* InDevice, VulkanShader* InOwner)
	: VulkanShaderProgram(InDevice, InOwner, ShaderType::VERTEX_BIT)
{
}
VulkanVertexShaderProgram::~VulkanVertexShaderProgram()
{
	Destroy();
}

bool VulkanVertexShaderProgram::Create(const unicode::string& filename, ShaderFeature::MASK mask)
{
	if (!VulkanShaderProgram::Create(filename, mask))
		return false;

	//	1. 버텍스 버퍼를 생성한다.
	//	2. Shader Compile할 때, 버텍스 버퍼에 맞는 버텍스 입력이 되도록 #ifdef ~ #endif를 잘 설정해서 맞춘다.
	//	3. 만약에 버텍스 버퍼 설정과 셰이더 버텍스 입력 포멧이 다를 경우에는 문제가 발생한다.
	VertexDescription::GetDesc(&m_VertexLayout, m_ShaderResource);

	if (m_Owner->CreateVertexDeclaration(&m_VertexLayout) == false)
		return false;

	return true;
}

void VulkanVertexShaderProgram::Destroy()
{
	VulkanShaderProgram::Destroy();

	m_Owner->DestroyVertexDeclaration();
}

void VulkanVertexShaderProgram::UpdateVertexAttributes(const VertexBuffer* vertexBuffer)
{
	// Empty... Empty.... Empty...
}

void VulkanVertexShaderProgram::ClearVertexAttributes()
{
	// Empty... Empty.... Empty...
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__ImplementRtti(VisualShock, VulkanFragmentShaderProgram, VulkanShaderProgram);

VulkanFragmentShaderProgram::VulkanFragmentShaderProgram(VulkanDevice* InDevice, VulkanShader* InOwner)
	: VulkanShaderProgram(InDevice, InOwner, ShaderType::FRAGMENT_BIT)
{
}
VulkanFragmentShaderProgram::~VulkanFragmentShaderProgram()
{
	Destroy();
}
