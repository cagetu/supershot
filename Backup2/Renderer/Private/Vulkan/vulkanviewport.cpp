#include "renderer.h"

bool DelayAcquireBackBuffer()
{
	return false;
}

__ImplementRtti(VisualShock, VulkanViewport, IViewport);

VulkanViewport::VulkanViewport(VulkanDriver* renderDriver)
	: m_RenderDriver(renderDriver)
	, m_SwapChain(nullptr)
	, m_AcquiredImageIndex(UINT_MAX)
	, m_AcquiredImageSemaphore(nullptr)
	, m_CommandBuffer(nullptr)
{
	for (int32 i = 0; i < _MAX_BUFFERS; i++)
		m_RenderingDoneSemaphore[i] = nullptr;

	//Memory::Memzero(m_BackBufferImages);
	//Memory::Memzero(m_BackBuffers);
}
VulkanViewport::~VulkanViewport()
{
}

bool VulkanViewport::Create(void* windowHandle, int32 width, int32 height, int32 format, uint32 featureFlag, const RelativeExtent& extent)
{
	int32 screenWidth = int32(width * extent.width);
	int32 screenHeight = int32(height * extent.height);

	//m_AbsoluteExtent = Extent(extent.x * width, extent.x * height, extent.width * width, extent.height * height, extent.minZ, extent.maxZ);


	// SwapChain 생성
	if (featureFlag&_SWAPCHAIN || featureFlag&_MAINVIEWPORT)
	{
		if (CreateSwapChain(windowHandle, screenWidth, screenHeight, format) == false)
			return false;
	}

	// Semaphore
	for (int32 i = 0; i < _MAX_BUFFERS; i++)
	{
		m_RenderingDoneSemaphore[i] = new VulkanSemaphore(m_RenderDriver->GetDevice());
	}

	// Window Handle
	m_WindowHandle = windowHandle;

	// Extents
	m_RelativeExtent = extent;

	// Info
	m_Width = screenWidth;
	m_Height = screenHeight;
	m_PixelFormat = format;

	// Features
	m_Flags = featureFlag;

	return true;
}

void VulkanViewport::Destroy()
{
	// Semaphore
	for (int32 i = 0; i < _MAX_BUFFERS; i++)
	{
		delete m_RenderingDoneSemaphore[i];
		m_RenderingDoneSemaphore[i] = nullptr;
	}

	// TextureView
	for (int32 i = 0; i < _MAX_BUFFERS; i++)
	{
		m_BackBuffers[i] = NULL;
	}

	// Swapchain
	if (m_SwapChain)
	{
		m_SwapChain->Destroy();

		delete m_SwapChain;
		m_SwapChain = nullptr;
	}
}

void VulkanViewport::Resize(int32 width, int32 height, int32 format, bool fullscreen)
{
	int32 screenWidth = int32(width * GetRelativeExtent().width);
	int32 screenHeight = int32(height * GetRelativeExtent().height);

	//m_AbsoluteExtent = Extent(m_RelativeExtent.x * width, m_RelativeExtent.x * height, m_RelativeExtent.width * width, m_RelativeExtent.height * height, m_RelativeExtent.minZ, m_RelativeExtent.maxZ);

	// TODO: RECREATE SWAPCHAIN....
	m_Width = screenWidth;
	m_Height = screenHeight;
	m_PixelFormat = format;
	_ASSERT(0);
}

bool VulkanViewport::CreateSwapChain(void* windowHandle, int32& width, int32& height, int32 format)
{
	VulkanDevice* CurrentDevice = m_RenderDriver->GetDevice();

	static const uint32 _NEED_BUFFERS = _MAX_BUFFERS;

	m_SwapChain = new VulkanSwapChain(m_RenderDriver->GetDevice());
	if (m_SwapChain->Create(m_RenderDriver->GetInstance(), windowHandle, width, height, _NEED_BUFFERS, format) == false)
		return false;

	VulkanCmdBuffer* CmdBuffer = nullptr;
#ifdef _SYNC_CMDBUFFER
	CmdBuffer = CurrentDevice->CommandBufferPool().GetUploadCommandBuffer();
#else
	CurrentDevice->CommandBufferPool().Initialize(_MAX_BUFFERS);
#endif

	uint32 imageCount = m_SwapChain->GetImageCount();
	for (uint32 index = 0; index < imageCount; index++)
	{
		VkImage imageHandle = m_SwapChain->GetImage(index);

		// Swapchain 이미지에 대응되는 ImageView를 만든다.
		m_BackBuffers[index] = new VulkanBackBuffer(m_RenderDriver->GetDevice(), imageHandle);
		bool result = m_BackBuffers[index]->Create(width, height, format);
		_ASSERT(result);

		// Clear the swapchain to avoid a validation warning, and transition to ColorAttachment
		{
			VkClearColorValue Color;
			Memory::Memzero(Color);
			Color.float32[0] = Color.float32[1] = Color.float32[2] = Color.float32[3] = 1.0f;

#ifdef _SYNC_CMDBUFFER
			//CmdBuffer->TransitionImageLayout(imageHandle, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);
			//CmdBuffer->ClearColor(imageHandle, VK_IMAGE_LAYOUT_GENERAL, Color);
			//CmdBuffer->TransitionImageLayout(imageHandle, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
			CmdBuffer->TransitionImageLayout(imageHandle, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
			CmdBuffer->ClearColor(imageHandle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, Color);
			CmdBuffer->TransitionImageLayout(imageHandle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
#else
			CmdBuffer = CurrentDevice->CommandBufferPool().GetCmdBuffer(index);
			CmdBuffer->Begin(false);
			{
				CmdBuffer->TransitionImageLayout(imageHandle, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);
				CmdBuffer->ClearColor(imageHandle, VK_IMAGE_LAYOUT_GENERAL, Color);
				CmdBuffer->TransitionImageLayout(imageHandle, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
			}
			CmdBuffer->End();
#endif
		}
	}

	return true;
}

VulkanSwapChain* VulkanViewport::GetSwapChain() const
{
	return m_SwapChain;
}

const VkImage& VulkanViewport::GetCurrentImage() const
{
	return m_BackBuffers[m_AcquiredImageIndex]->GetImage();
}

const Vulkan::TextureView& VulkanViewport::GetCurrentImageView() const
{
	return m_BackBuffers[m_AcquiredImageIndex]->GetView();
}

uint32 VulkanViewport::GetAcquiredImageIndex() const 
{ 
	return m_AcquiredImageIndex;
}

const Ptr<VulkanBackBuffer>& VulkanViewport::GetBackBuffer() const
{
	return m_BackBuffers[m_AcquiredImageIndex];
}

void VulkanViewport::AcquireNextImage()
{
	VulkanDevice* CurrentDevice = m_RenderDriver->GetDevice();

	m_SwapChain->AcquireNextImageIndex(m_AcquiredImageIndex, &m_AcquiredImageSemaphore);
	_ASSERT(m_AcquiredImageIndex != UINT_MAX);

	// 현재 이미지를 BackBuffer로 설정
#ifdef _SYNC_CMDBUFFER
	VulkanCmdBufferPool& CmdManager = CurrentDevice->CommandBufferPool();
	VulkanCmdBuffer* CmdBuffer = CmdManager.GetActiveCommandBuffer();
	VulkanQueue* Queue = CurrentDevice->GetGfxQueue();

	// SwapChain Image를 렌더링용 ImageLayout으로 전환
	CmdBuffer->TransitionImageLayout(m_SwapChain->GetImage(m_AcquiredImageIndex), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
	CmdBuffer->End();

	// Submit here so we can add a dependency with the acquired semaphore
	CurrentDevice->GetGfxQueue()->Submit(CmdBuffer, nullptr, m_AcquiredImageSemaphore, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);

	CmdManager.PrepareForNewActiveCommandBuffer();
#else
	VulkanCmdBuffer* CmdBuffer = CurrentDevice->CommandBufferPool().GetCmdBuffer(m_AcquiredImageIndex);

	// Submit here so we can add a dependency with the acquired semaphore
	//VulkanQueue* Queue = m_RenderDriver->GetDevice()->GetGfxQueue();
	//Queue->Submit(CmdBuffer, nullptr, m_AcquiredImageSemaphore, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);
#endif
}

void VulkanViewport::Begin()
{
	/*
		SwapChain에서 BackBuffer를 얻는다.
		얻은 BackBuffer를 FrameBuffer로 설정한다.

		ex) dx9
		BackBuffer = SwapChain->GetBackBuffer();

		Driver->SetRenderTarget(BackBuffer);
		Driver->SetFrameBuffer(BackBuffer)

		COLORBUFFER, DEPTHBUFFER, STENCILBUFFER 사용을 기본으로 함.
	*/

	/*
		1. AcquireNextImage
		2. Record Commands
		3. Submit CommandBuffer To Queue
		4. Present
	*/

	// IMAGE 얻어오기
	AcquireNextImage();
}

void VulkanViewport::End()
{
}

void VulkanViewport::Present()
{
	if (m_Flags&_PRESENT)
	{
		_ASSERT(m_SwapChain);
		_ASSERT(m_AcquiredImageIndex != UINT_MAX);

		VulkanDevice* CurrentDevice = m_RenderDriver->GetDevice();
		VulkanQueue* Queue = CurrentDevice->GetGfxQueue();

#ifdef _SYNC_CMDBUFFER
		VulkanCmdBufferPool& CmdManager = CurrentDevice->CommandBufferPool();
		VulkanCmdBuffer* CmdBuffer = CmdManager.GetActiveCommandBuffer();

		// SwapChain Image를 렌더링용 ImageLayout으로 전환
		CmdBuffer->TransitionImageLayout(m_SwapChain->GetImage(m_AcquiredImageIndex), VK_IMAGE_LAYOUT_UNDEFINED/*VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL*/, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
		CmdBuffer->End();

		// 1. CommandBuffer를 Queue에 Submit. CommandBuffer의 수행이 완료되면, m_RenderingDoneSemaphore에 시그널!
		VulkanSemaphore* SubmitSemaphore = DelayAcquireBackBuffer() ? m_AcquiredImageSemaphore : nullptr;
		VkPipelineStageFlags SubmitFlag = DelayAcquireBackBuffer() ? VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT : 0;
		Queue->Submit(CmdBuffer, m_RenderingDoneSemaphore[m_AcquiredImageIndex], SubmitSemaphore, SubmitFlag);

		// 2. SwapChain Present. CommandBuffer의 수행이 완료될 때까지 대기한다. (m_RenderingDoneSemaphore)
		m_SwapChain->Present(Queue, m_RenderingDoneSemaphore[m_AcquiredImageIndex]);

		CmdManager.PrepareForNewActiveCommandBuffer();
#else
		VulkanCmdBuffer* CmdBuffer = CurrentDevice->CommandBufferPool().GetCmdBuffer(m_AcquiredImageIndex);

		// 1. CommandBuffer를 Queue에 Submit. CommandBuffer의 수행이 완료되면, m_RenderingDoneSemaphore에 시그널!
		Queue->Submit(CmdBuffer, m_RenderingDoneSemaphore[m_AcquiredImageIndex], m_AcquiredImageSemaphore, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);

		// 2. SwapChain Present. CommandBuffer의 수행이 완료될 때까지 대기한다. (m_RenderingDoneSemaphore)
		m_SwapChain->Present(Queue, m_RenderingDoneSemaphore[m_AcquiredImageIndex]);
#endif
	}

}