#include "renderer.h"

__ImplementRtti(VisualShock, VulkanGfxPipeline, VulkanPipeline);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VulkanGfxPipeline::VulkanGfxPipeline(VulkanDevice* device, int32 pipelineIndex, VkGraphicsPipelineCreateInfo& pipelineCreateInfo, VkPipelineCache pipelineCache, VulkanGfxPipeline* basePipeline)
	: VulkanPipeline(device, pipelineIndex)
	, m_BasePipeline(basePipeline)
{
	VK_RESULT(vkCreateGraphicsPipelines(m_Device->GetHandle(), pipelineCache, 1, &pipelineCreateInfo, nullptr, &m_Handle));
}
VulkanGfxPipeline::~VulkanGfxPipeline()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VulkanGfxPipelineManager::VulkanGfxPipelineManager()
{
}

VulkanGfxPipelineManager::~VulkanGfxPipelineManager()
{
}

void VulkanGfxPipelineManager::Init(VulkanDevice* device)
{
	m_Device = device;

	m_PipelineCache.Create(device);
}

void VulkanGfxPipelineManager::Destroy()
{
	m_PipelineCache.Destroy(m_Device);
}

VulkanGfxPipeline* VulkanGfxPipelineManager::FindOrCreate(const GfxPipelineContext& pipelineContext, VulkanGfxPipeline* basePipeline)
{
	for (uint32 i = 0; i < m_PipelineInstances.size(); i++)
	{
		if (m_PipelineInstances[i].context == pipelineContext)
		{
			return m_PipelineInstances[i].pipline;
		}
	}

	// Create New Pipeline
	VkPipelineViewportStateCreateInfo viewportState = CreateViewportState(pipelineContext.ViewportState, pipelineContext.ScissorState);
	VkPipelineInputAssemblyStateCreateInfo IAState = CreateInputAssemblyState(pipelineContext.InputAssemblyState);
	VkPipelineVertexInputStateCreateInfo vertexInputState = CreateVertexInputState(pipelineContext.VertexInputState);
	VkPipelineTessellationStateCreateInfo tesselationState = CreateTessellationState(pipelineContext.TessellationState);
	VkPipelineRasterizationStateCreateInfo rasterizationState = CreateRasterizationState(pipelineContext.RasterizationState, pipelineContext.DepthTestState, pipelineContext.DepthBiasState);
	VkPipelineDepthStencilStateCreateInfo depthStencilState = CreateDepthStencilState(pipelineContext.DepthTestState, pipelineContext.DepthBoundTestState, pipelineContext.StencilTestState);
	VkPipelineMultisampleStateCreateInfo multisampleState = CreateMultisampleState(pipelineContext.MultiSampleState);
	multisampleState.rasterizationSamples = pipelineContext.RenderPass->GetRenderTargetDesc().layout.GetAttachmentDesc()->samples;

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	GetShaderStages(pipelineContext.Shader, shaderStages);

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	VkPipelineColorBlendStateCreateInfo colorBlendState;
	Memory::Memzero(colorBlendState);
	colorBlendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	// ColorBlendStates
	std::vector<VkPipelineColorBlendAttachmentState> attachmentBlendStates;
	GetColorBlendStates(pipelineContext.RenderPass->GetColorAttachmentCount(), pipelineContext.ColorBlendState, attachmentBlendStates);
	colorBlendState.attachmentCount = attachmentBlendStates.size();
	colorBlendState.pAttachments = attachmentBlendStates.data();
	// 논리 연산 (사용하지 않는다. 추후 필요하면 사용하는 걸로... 2017.08.18)
	//-LogicOperation : Fragment Shader의 Output과 Color Attachment의 내용 사이에 AND나 XOR 같은 Logical Operation이 적용되도록 허용한다.
	colorBlendState.logicOpEnable = false;
	colorBlendState.logicOp = VK_LOGIC_OP_CLEAR;
	// BlendConstants (DynamicState에서 지정할 경우, vkCmdSetBlendConstants에서 추가 가능)
	colorBlendState.blendConstants[0] = 1.0f;
	colorBlendState.blendConstants[1] = 1.0f;
	colorBlendState.blendConstants[2] = 1.0f;
	colorBlendState.blendConstants[3] = 1.0f;

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	VkPipelineDynamicStateCreateInfo dynamicState;
	Memory::Memzero(dynamicState);
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	//
	std::vector<VkDynamicState> states;
	GetDynamicState(pipelineContext.DynamicState, states);
	dynamicState.dynamicStateCount = states.size();
	dynamicState.pDynamicStates = states.size() > 0 ? states.data() : nullptr;

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	VkGraphicsPipelineCreateInfo pipelineCreateInfo;
	Memory::Memzero(pipelineCreateInfo);
	pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	// PipelineLayout
	pipelineCreateInfo.layout = pipelineContext.Shader->GetPipelineLayout()->Handle();
	// RenderPass Info
	pipelineCreateInfo.renderPass = pipelineContext.RenderPass->GetHandle();
	pipelineCreateInfo.subpass = pipelineContext.SubPassIndex;
	// Parant(?) Pipeline Info
	pipelineCreateInfo.basePipelineHandle = basePipeline ? basePipeline->GetHandle() : VK_NULL_HANDLE;
	pipelineCreateInfo.basePipelineIndex = basePipeline ? basePipeline->GetIndex() : -1;
	//
	pipelineCreateInfo.stageCount = shaderStages.size();
	pipelineCreateInfo.pStages = shaderStages.data();
	pipelineCreateInfo.pVertexInputState = &vertexInputState;
	pipelineCreateInfo.pInputAssemblyState = &IAState;
	pipelineCreateInfo.pRasterizationState = &rasterizationState;
	pipelineCreateInfo.pMultisampleState = &multisampleState;
	pipelineCreateInfo.pDepthStencilState = &depthStencilState;
	pipelineCreateInfo.pColorBlendState = &colorBlendState;
	pipelineCreateInfo.pViewportState = &viewportState;
	//pipelineCreateInfo.pTessellationState = &tesselationState;	// 사용하지 않을 때에는 NULL로 남겨둬야 하는것인가???
	pipelineCreateInfo.pDynamicState = &dynamicState;

	static int32 GTicket = 0;
	VulkanGfxPipeline* NewPipeline = new VulkanGfxPipeline(m_Device, GTicket++, pipelineCreateInfo, m_PipelineCache.pipelineCache, basePipeline);

	m_PipelineCache.GetData(m_Device);

	PipelineInstance NewInstance;
	NewInstance.context = pipelineContext;
	NewInstance.pipline = NewPipeline;
	m_PipelineInstances.push_back(NewInstance);

	return NewPipeline;
}

void VulkanGfxPipelineManager::Remove(VulkanGfxPipeline* pipline)
{

}