#include "renderer.h"

uint32 GFrameNumberRenderThread = 1;

namespace Vulkan
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ResourceHeapPool::ResourceHeapPool()
	: m_Device(nullptr)
{
}
ResourceHeapPool::~ResourceHeapPool()
{
	Destroy();
}

void ResourceHeapPool::Init(VulkanDevice* device)
{
	m_Device = device;

	DeviceMemoryPool& deviceMemoryManager = m_Device->MemoryManager();
	const VkPhysicalDeviceMemoryProperties& memoryProperties = deviceMemoryManager.GetMemoryProperies();
	const uint32 TypeBits = (1 << memoryProperties.memoryTypeCount) - 1;

	m_ResourceTypeHeaps.resize(memoryProperties.memoryTypeCount);

	std::vector<uint64> remainingHeapSizes;
	std::vector<uint64> numTypesPerHeap;
	for (uint32 i = 0; i < memoryProperties.memoryHeapCount; i++)
	{
		remainingHeapSizes.push_back(memoryProperties.memoryHeaps[i].size);
		numTypesPerHeap.push_back(0);
	}

	for (uint32 i = 0; i < memoryProperties.memoryTypeCount; i++)
	{
		numTypesPerHeap[memoryProperties.memoryTypes[i].heapIndex] += 1;
	}

	auto GetMemoryTypesFromProperties = [memoryProperties](uint32 InTypeBits, VkMemoryPropertyFlags Properties, std::vector<uint32>& OutTypeIndices)
	{
		std::vector<uint32> TypeIndices;
		for (uint32 i = 0; i < memoryProperties.memoryTypeCount && InTypeBits; i++)
		{
			if ((InTypeBits & 1) == 1)
			{
				if ((memoryProperties.memoryTypes[i].propertyFlags& Properties) == Properties)
				{
					TypeIndices.push_back(i);
				}
			}
			InTypeBits >>= 1;
		}

		for (int32 Index = TypeIndices.size() - 1; Index >= 1; --Index)
		{
			if (memoryProperties.memoryTypes[Index].propertyFlags != memoryProperties.memoryTypes[0].propertyFlags)
				continue;

			OutTypeIndices.push_back(TypeIndices[Index]);
		}
		return OutTypeIndices.size() > 0;
	};

	// main 메모리 힙 세팅
	uint32 numGPUAllocations = 0;
	{
		// Some drivers return the same memory types multiple times, so weed those out
		std::vector<uint32> TypeIndices;
		GetMemoryTypesFromProperties(TypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, TypeIndices);
		_ASSERT(TypeIndices.size() > 0);

		uint32 HeapIndex = memoryProperties.memoryTypes[TypeIndices[0]].heapIndex;
		uint64 HeapSize = memoryProperties.memoryHeaps[HeapIndex].size / TypeIndices.size();

		for (uint32 i = 0; i < TypeIndices.size(); i++)
		{
			const uint32 TypeIndex = TypeIndices[i];

			_ASSERT(memoryProperties.memoryTypes[TypeIndex].heapIndex == HeapIndex);

			m_ResourceTypeHeaps[TypeIndex] = new ResourceHeap(this, TypeIndex, GPU_ONLY_HEAP_PAGE_SIZE);
			remainingHeapSizes[memoryProperties.memoryTypes[TypeIndex].heapIndex] -= HeapSize;
			// Last one...
			m_GPUHeap = m_ResourceTypeHeaps[TypeIndex];

			m_ResourceTypeHeaps[TypeIndex]->m_bIsHostCachedSupported = ((memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) == VK_MEMORY_PROPERTY_HOST_CACHED_BIT);
			m_ResourceTypeHeaps[TypeIndex]->m_bIsLazilyAllocatedSupported = ((memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT) == VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT);
		}
		numGPUAllocations = uint32(HeapSize / GPU_ONLY_HEAP_PAGE_SIZE);
	}

	// Upload heap
	uint32 NumUploadAllocations = 0;
	{
		uint32 TypeIndex = 0;
		VK_RESULT(deviceMemoryManager.GetMemoryType(TypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &TypeIndex));

		uint64 HeapSize = memoryProperties.memoryHeaps[memoryProperties.memoryTypes[TypeIndex].heapIndex].size;

		m_UploadToGPUHeap = new ResourceHeap(this, TypeIndex, STAGING_HEAP_PAGE_SIZE);
		m_ResourceTypeHeaps[TypeIndex] = m_UploadToGPUHeap;
		remainingHeapSizes[memoryProperties.memoryTypes[TypeIndex].heapIndex] -= HeapSize;

		NumUploadAllocations = uint32(HeapSize / STAGING_HEAP_PAGE_SIZE);
	}

	// Download heap
	uint32 NumDownloadAllocations = 0;
	{
		uint32 TypeIndex = 0;
		{
			uint32 HostVisCachedIndex = 0;
			VkResult HostCachedResult = deviceMemoryManager.GetMemoryType(TypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT, &HostVisCachedIndex);

			uint32 HostVisIndex = 0;
			VkResult HostResult = deviceMemoryManager.GetMemoryType(TypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, &HostVisIndex);

			if (HostCachedResult == VK_SUCCESS)
			{
				TypeIndex = HostVisCachedIndex;
			}
			else if (HostResult == VK_SUCCESS)
			{
				TypeIndex = HostVisIndex;
			}
			else
			{
				// Redundant as it would have asserted above...
				//UE_LOG(LogVulkanRHI, Fatal, TEXT("No Memory Type found supporting VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT!"));
				//PlatformUtil::DebugOutFormat()
				_ASSERT(0);
			}
		}

		uint64 HeapSize = memoryProperties.memoryHeaps[memoryProperties.memoryTypes[TypeIndex].heapIndex].size;

		m_DownloadToCPUHeap = new ResourceHeap(this, TypeIndex, STAGING_HEAP_PAGE_SIZE);
		m_ResourceTypeHeaps[TypeIndex] = m_DownloadToCPUHeap;
		remainingHeapSizes[memoryProperties.memoryTypes[TypeIndex].heapIndex] -= HeapSize;

		NumDownloadAllocations = uint32(HeapSize / STAGING_HEAP_PAGE_SIZE);
	}

	uint32 NumMemoryAllocations = (uint64)m_Device->GetLimits().maxMemoryAllocationCount;
	if (numGPUAllocations + NumDownloadAllocations + NumUploadAllocations > NumMemoryAllocations)
	{
		_ASSERT(0);
		//UE_LOG(LogVulkanRHI, Warning, TEXT("Too many allocations (%d) per heap size (G:%d U:%d D:%d), might run into slow path in the driver"),
		//	NumGPUAllocations + NumDownloadAllocations + NumUploadAllocations, NumGPUAllocations, NumUploadAllocations, NumDownloadAllocations);
	}

}

void ResourceHeapPool::Destroy()
{
	DestroyResourceAllocations();

	for (uint32 Index = 0; Index < m_ResourceTypeHeaps.size(); ++Index)
	{
		delete m_ResourceTypeHeaps[Index];
		//m_ResourceTypeHeaps[Index] = nullptr;
	}
	m_ResourceTypeHeaps.clear();
}

void ResourceHeapPool::ReleaseFreePages()
{
	ResourceHeap* Heap = m_ResourceTypeHeaps[1 % m_ResourceTypeHeaps.size()];
	if (Heap)
	{
		Heap->ReleaseFreePages(false);
	}

	ReleaseFreedResources(false);
}

void ResourceHeapPool::DestroyResourceAllocations()
{
	ReleaseFreedResources(true);

	for (int32 Index = m_UsedBufferAllocations.size() - 1; Index >= 0; --Index)
	{
		BufferAllocation* BufferAllocation = m_UsedBufferAllocations[Index];
		if (BufferAllocation->JoinFreeBlocks())
		{
			BufferAllocation->Destroy(GetOwner());
			GetOwner()->MemoryManager().Free(BufferAllocation->m_MemoryAllocation);
			delete BufferAllocation;
		}
		else
		{
			_ASSERT(0);
		}
	}
	m_UsedBufferAllocations.clear();

	for (uint32 Index = 0; Index < m_FreeBufferAllocations.size(); ++Index)
	{
		BufferAllocation* BufferAllocation = m_FreeBufferAllocations[Index];
		BufferAllocation->Destroy(GetOwner());
		GetOwner()->MemoryManager().Free(BufferAllocation->m_MemoryAllocation);
		delete BufferAllocation;
	}
	m_FreeBufferAllocations.clear();
}

void ResourceHeapPool::ReleaseFreedResources(bool bForce)
{
	BufferAllocation* BufferAllocationToRelease = nullptr;

	{
		//FScopeLock ScopeLock(&CS);
		for (auto it = m_FreeBufferAllocations.begin(); it != m_FreeBufferAllocations.end(); it++)
		{
			BufferAllocation* BufferAllocation = (*it);
			if (bForce || BufferAllocation->m_FrameFreed + NUM_FRAMES_TO_WAIT_BEFORE_RELEASING_TO_OS < GFrameNumberRenderThread)
			{
				BufferAllocationToRelease = BufferAllocation;
				m_FreeBufferAllocations.erase(it);
				break;
			}
		}
	}

	if (BufferAllocationToRelease)
	{
		BufferAllocationToRelease->Destroy(GetOwner());
		GetOwner()->MemoryManager().Free(BufferAllocationToRelease->m_MemoryAllocation);
		//UsedMemory -= BufferAllocationToRelease->MaxSize;
		delete BufferAllocationToRelease;
	}
}

// Returns a sub-allocation, as there can be space inside a previously allocated VkBuffer to be reused; to release a sub allocation, just delete the pointer
BufferSubAllocation* ResourceHeapPool::AllocateBuffer(uint32 Size, VkBufferUsageFlags BufferUsageFlags, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line)
{
	const VkPhysicalDeviceLimits& Limits = m_Device->GetLimits();
	uint32 Alignment = ((BufferUsageFlags & VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT) != 0) ? (uint32)Limits.minUniformBufferOffsetAlignment : 1;
	Alignment = Math::Max(Alignment, ((BufferUsageFlags & (VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT)) != 0) ? (uint32)Limits.minTexelBufferOffsetAlignment : 1u);
	Alignment = Math::Max(Alignment, ((BufferUsageFlags & VK_BUFFER_USAGE_STORAGE_BUFFER_BIT) != 0) ? (uint32)Limits.minStorageBufferOffsetAlignment : 1u);

	//FScopeLock ScopeLock(&CS);

	// 사용 중인 BufferAllocation 중에서 할당 가능한가?
	for (uint32 Index = 0; Index < m_UsedBufferAllocations.size(); ++Index)
	{
		BufferAllocation* bufferAllocation = m_UsedBufferAllocations[Index];
		if ((bufferAllocation->m_BufferUsageFlags & BufferUsageFlags) == BufferUsageFlags &&
			(bufferAllocation->m_MemoryPropertyFlags & MemoryPropertyFlags) == MemoryPropertyFlags)
		{
			BufferSubAllocation* Suballocation = (BufferSubAllocation*)bufferAllocation->TryAllocateNoLocking(Size, Alignment, File, Line);
			if (Suballocation)
			{
				return Suballocation;
			}
		}
	}

	// 해제 된 FreeAllocation 목록 중에서 할당 시도
	for (auto it = m_FreeBufferAllocations.begin(); it != m_FreeBufferAllocations.end(); it++)
	{
		BufferAllocation* bufferAllocation = (*it);
		if ((bufferAllocation->m_BufferUsageFlags & BufferUsageFlags) == BufferUsageFlags &&
			(bufferAllocation->m_MemoryPropertyFlags & MemoryPropertyFlags) == MemoryPropertyFlags)
		{
			BufferSubAllocation* Suballocation = (BufferSubAllocation*)bufferAllocation->TryAllocateNoLocking(Size, Alignment, File, Line);
			if (Suballocation)
			{
				m_FreeBufferAllocations.erase(it);
				m_UsedBufferAllocations.push_back(bufferAllocation);
				return Suballocation;
			}
		}
	}

	// 새로운 할당 생성
	uint32 BufferSize = Math::Max(Size, (uint32)BufferAllocationSize);

	VkBuffer Buffer;
	VkBufferCreateInfo BufferCreateInfo;
	Memory::Memzero(BufferCreateInfo);
	BufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	BufferCreateInfo.size = BufferSize;
	BufferCreateInfo.usage = BufferUsageFlags;
	VK_RESULT(::vkCreateBuffer(m_Device->GetHandle(), &BufferCreateInfo, nullptr, &Buffer));

	VkMemoryRequirements MemReqs;
	::vkGetBufferMemoryRequirements(m_Device->GetHandle(), Buffer, &MemReqs);
	Alignment = Math::Max((uint32)MemReqs.alignment, Alignment);

	uint32 MemoryTypeIndex;
	VK_RESULT(m_Device->MemoryManager().GetMemoryType(MemReqs.memoryTypeBits, MemoryPropertyFlags, &MemoryTypeIndex));

	DeviceMemoryAllocation* deviceMemoryAllocation = m_Device->MemoryManager().Allocate(BufferSize, MemoryTypeIndex/*, File, Line*/);
	VK_RESULT(::vkBindBufferMemory(m_Device->GetHandle(), Buffer, deviceMemoryAllocation->Handle(), 0));
	if (deviceMemoryAllocation->CanBeMapped())
	{
		deviceMemoryAllocation->Map(BufferSize, 0);
	}

	BufferAllocation* bufferAllocation = new BufferAllocation(this, deviceMemoryAllocation, MemoryTypeIndex, MemoryPropertyFlags, (uint32)MemReqs.alignment, Buffer, BufferUsageFlags);
	m_UsedBufferAllocations.push_back(bufferAllocation);

	return (BufferSubAllocation*)bufferAllocation->TryAllocateNoLocking(Size, Alignment, File, Line);
}

// Release a whole allocation; this is only called from within a BufferAllocation
void ResourceHeapPool::ReleaseBuffer(BufferAllocation* BufferAllocation)
{
	//FScopeLock ScopeLock(&CS);
	_ASSERT(BufferAllocation->JoinFreeBlocks());
	auto it = std::find(m_UsedBufferAllocations.begin(), m_UsedBufferAllocations.end(), BufferAllocation);
	m_UsedBufferAllocations.erase(it);
	BufferAllocation->m_FrameFreed = GFrameNumberRenderThread;
	m_FreeBufferAllocations.push_back(BufferAllocation);
}

ResourceAllocation* ResourceHeapPool::AllocateBufferMemory(const VkMemoryRequirements& MemoryReqs, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line)
{
	uint32 TypeIndex = 0;
	VK_RESULT(m_Device->MemoryManager().GetMemoryType(MemoryReqs.memoryTypeBits, MemoryPropertyFlags, &TypeIndex));

	bool bMapped = (MemoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

	if (!m_ResourceTypeHeaps[TypeIndex])
	{
		// Try another heap type
		uint32 OriginalTypeIndex = TypeIndex;
		if (m_Device->MemoryManager().GetMemoryTypeExcluding(MemoryReqs.memoryTypeBits, MemoryPropertyFlags, TypeIndex, &TypeIndex) != VK_SUCCESS)
		{
			//UE_LOG(LogVulkanRHI, Fatal, TEXT("Unable to find alternate type for index %d, MemSize %d, MemPropTypeBits %u, MemPropertyFlags %u, %s(%d)"), OriginalTypeIndex, (uint32)MemoryReqs.size, (uint32)MemoryReqs.memoryTypeBits, (uint32)MemoryPropertyFlags, ANSI_TO_TCHAR(File), Line);
			_ASSERT(0);
		}

		if (!m_ResourceTypeHeaps[TypeIndex])
		{
#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
			DumpMemory();
#endif
			//UE_LOG(LogVulkanRHI, Fatal, TEXT("Missing memory type index %d (originally requested %d), MemSize %d, MemPropTypeBits %u, MemPropertyFlags %u, %s(%d)"), TypeIndex, OriginalTypeIndex, (uint32)MemoryReqs.size, (uint32)MemoryReqs.memoryTypeBits, (uint32)MemoryPropertyFlags, ANSI_TO_TCHAR(File), Line);
			_ASSERT(0);
		}
	}

	if (!m_ResourceTypeHeaps[TypeIndex]->IsHostCachedSupported())
	{
		//remove host cached bit if device does not support it
		//it should only affect perf
		MemoryPropertyFlags = MemoryPropertyFlags & ~VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
	}
	if (!m_ResourceTypeHeaps[TypeIndex]->IsLazilyAllocatedSupported())
	{
		//remove lazily bit if device does not support it
		//it should only affect perf
		MemoryPropertyFlags = MemoryPropertyFlags & ~VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
	}

	ResourceAllocation* Allocation = m_ResourceTypeHeaps[TypeIndex]->AllocateResource((uint32)MemoryReqs.size, MemoryReqs.alignment, false, bMapped, File, Line);
	if (!Allocation)
	{
		// Try another memory type if the allocation failed
		VK_RESULT(m_Device->MemoryManager().GetMemoryTypeExcluding(MemoryReqs.memoryTypeBits, MemoryPropertyFlags, TypeIndex, &TypeIndex));
		bMapped = (MemoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
		if (!m_ResourceTypeHeaps[TypeIndex])
		{
			//UE_LOG(LogVulkanRHI, Fatal, TEXT("Missing memory type index %d, MemSize %d, MemPropTypeBits %u, MemPropertyFlags %u, %s(%d)"), TypeIndex, (uint32)MemoryReqs.size, (uint32)MemoryReqs.memoryTypeBits, (uint32)MemoryPropertyFlags, ANSI_TO_TCHAR(File), Line);
			_ASSERT(0);
		}
		Allocation = m_ResourceTypeHeaps[TypeIndex]->AllocateResource(MemoryReqs.size, MemoryReqs.alignment, false, bMapped, File, Line);
	}
	return Allocation;
}

ResourceAllocation* ResourceHeapPool::AllocateImageMemory(const VkMemoryRequirements& MemoryReqs, VkMemoryPropertyFlags MemoryPropertyFlags, const char* File, uint32 Line)
{
	uint32 TypeIndex = 0;
	VK_RESULT(m_Device->MemoryManager().GetMemoryType((uint32)MemoryReqs.memoryTypeBits, MemoryPropertyFlags, &TypeIndex));

	if (!m_ResourceTypeHeaps[TypeIndex])
	{
		//UE_LOG(LogVulkanRHI, Fatal, TEXT("Missing memory type index %d, MemSize %d, MemPropTypeBits %u, MemPropertyFlags %u, %s(%d)"), TypeIndex, (uint32)MemoryReqs.size, (uint32)MemoryReqs.memoryTypeBits, (uint32)MemoryPropertyFlags, ANSI_TO_TCHAR(File), Line);
		_ASSERT(0);
	}

	bool bMapped = (MemoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
	ResourceAllocation* Allocation = m_ResourceTypeHeaps[TypeIndex]->AllocateResource((uint32)MemoryReqs.size, MemoryReqs.alignment, true, bMapped, File, Line);
	if (!Allocation)
	{
		VK_RESULT(m_Device->MemoryManager().GetMemoryTypeExcluding(MemoryReqs.memoryTypeBits, MemoryPropertyFlags, TypeIndex, &TypeIndex));
		bMapped = (MemoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) == VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
		Allocation = m_ResourceTypeHeaps[TypeIndex]->AllocateResource((uint32)MemoryReqs.size, MemoryReqs.alignment, true, bMapped, File, Line);
	}
	return Allocation;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ResourceAllocation::ResourceAllocation(ResourceHeapPage* InOwner, DeviceMemoryAllocation* InDeviceMemoryAllocation,
	uint32 InRequestedSize, uint32 InAlignedOffset,
	uint32 InAllocationSize, uint32 InAllocationOffset, const char* InFile, uint32 InLine)
	: m_Owner(InOwner)
	, m_DeviceMemoryAllocation(InDeviceMemoryAllocation)
	, m_RequestedSize(InRequestedSize)
	, m_AlignedOffset(InAlignedOffset)
	, m_AllocationSize(InAllocationSize)
	, m_AllocationOffset(InAllocationOffset)
	, File(InFile)
	, Line(InLine)
{
}
ResourceAllocation::~ResourceAllocation()
{
	m_Owner->ReleaseAllocation(this);
}

void ResourceAllocation::BindBuffer(VulkanDevice* Device, VkBuffer Buffer)
{
	VK_RESULT(vkBindBufferMemory(Device->GetHandle(), Buffer, m_DeviceMemoryAllocation->Handle(), GetOffset()));
}

void ResourceAllocation::BindImage(VulkanDevice* Device, VkImage Image)
{
	VK_RESULT(vkBindImageMemory(Device->GetHandle(), Image, m_DeviceMemoryAllocation->Handle(), GetOffset()));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ResourceHeap::ResourceHeap(ResourceHeapPool* owner, uint32 memoryTypeIndex, uint32 pageSize)
	: m_Owner(owner)
	, m_MemoryTypeIndex(memoryTypeIndex)
	, m_DefaultPageSize(pageSize)
	, m_PeakPageSize(0)
	, m_UsedMemorySize(0)
	, m_PageIDCounter(0)
{
}
ResourceHeap::~ResourceHeap()
{
	ReleaseFreePages(true);

	auto DeletePages = [&](std::vector<ResourceHeapPage*>& UsedPages)
	{
		for (int32 Index = UsedPages.size() - 1; Index >= 0; --Index)
		{
			ResourceHeapPage* Page = UsedPages[Index];
			if (Page->JoinFreeBlocks())
			{
				m_Owner->GetOwner()->MemoryManager().Free(Page->m_DeviceMemoryAllocation);
				delete Page;
			}
			else
			{
				//#if UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT
				//Owner->GetParent()->GetMemoryManager().DumpMemory();
				//Owner->GetParent()->GetResourceHeapManager().DumpMemory();
				//GLog->Flush();
				//#endif
				//UE_LOG(LogVulkanRHI, Error, TEXT("Memory leak!"));
				PlatformUtil::DebugOut(TEXT("Memory Leak"));
			}
		}
		UsedPages.clear();
	};
	DeletePages(m_UsedBufferPages);
	DeletePages(m_UsedImagePages);

	for (uint32 Index = 0; Index < m_FreePages.size(); ++Index)
	{
		ResourceHeapPage* Page = m_FreePages[Index];
		m_Owner->GetOwner()->MemoryManager().Free(Page->m_DeviceMemoryAllocation);
		delete Page;
	}
	m_FreePages.clear();
}

void ResourceHeap::FreePage(ResourceHeapPage* page)
{
	//FScopeLock ScopeLock(&CriticalSection);
	_ASSERT(page->JoinFreeBlocks());
	int32 Index = -1;

	auto it = std::find(m_UsedBufferPages.begin(), m_UsedBufferPages.end(), page);
	if (m_UsedBufferPages.end() != it)
	{
		m_UsedBufferPages.erase(it);
	}
	else
	{
		it = std::find(m_UsedImagePages.begin(), m_UsedImagePages.end(), page);
		m_UsedImagePages.erase(it);
	}

	page->m_FrameFreed = GFrameNumberRenderThread;
	m_FreePages.push_back(page);
}

void ResourceHeap::ReleaseFreePages(bool bForce)
{
	ResourceHeapPage* PageToRelease = nullptr;

	{
		//FScopeLock ScopeLock(&CriticalSection);
		for (std::vector<ResourceHeapPage*>::iterator it = m_FreePages.begin(); it != m_FreePages.end(); it++)
		{
			ResourceHeapPage* Page = (*it);
			if (bForce || Page->m_FrameFreed + NUM_FRAMES_TO_WAIT_BEFORE_RELEASING_TO_OS < GFrameNumberRenderThread)
			{
				PageToRelease = Page;
				m_FreePages.erase(it);
				break;
			}
		}
	}

	if (PageToRelease)
	{
		m_Owner->GetOwner()->MemoryManager().Free(PageToRelease->m_DeviceMemoryAllocation);
		m_UsedMemorySize -= PageToRelease->m_MaxSize;
		delete PageToRelease;
	}
}

ResourceAllocation* ResourceHeap::AllocateResource(uint32 Size, uint32 Alignment, bool bIsImage, bool bMapAllocation, const char* File, uint32 Line)
{
	std::vector<ResourceHeapPage*>& usedPages = bIsImage ? m_UsedImagePages : m_UsedBufferPages;

	if (Size < m_DefaultPageSize)
	{
		for (uint32 index = 0; index < usedPages.size(); index++)
		{
			ResourceHeapPage* page = usedPages[index];
			if (page->m_DeviceMemoryAllocation->IsMapped() == bMapAllocation)
			{
				ResourceAllocation* allocation = page->TryAllocate(Size, Alignment, File, Line);
				if (allocation)
				{
					return allocation;
				}
			}
		}
	}

	for (std::vector<ResourceHeapPage*>::iterator it = m_FreePages.begin(); it != m_FreePages.end(); it++)
	{
		ResourceHeapPage* page = (*it);
		if (page->m_DeviceMemoryAllocation->IsMapped() == bMapAllocation)
		{
			ResourceAllocation* allocation = page->TryAllocate(Size, Alignment, File, Line);
			if (allocation)
			{
				m_FreePages.erase(it);
				usedPages.push_back(page);
				return allocation;
			}
		}
	}

	uint32 allocationSize = Math::Max<uint32>(Size, m_DefaultPageSize);

	DeviceMemoryAllocation* newDeviceMemoryBlock = m_Owner->GetOwner()->MemoryManager().Allocate(allocationSize, m_MemoryTypeIndex);
	ResourceHeapPage* newPage = new ResourceHeapPage(this, newDeviceMemoryBlock, ++m_PageIDCounter);
	usedPages.push_back(newPage);

	m_UsedMemorySize += allocationSize;

	m_PeakPageSize = Math::Max<uint32>(allocationSize, m_PeakPageSize);

	if (bMapAllocation)
	{
		newDeviceMemoryBlock->Map(allocationSize, 0);
	}

	return newPage->Allocate(Size, Alignment, File, Line);
}

ResourceHeapPage::ResourceHeapPage(ResourceHeap* owner, DeviceMemoryAllocation* deviceMemoryAllocation, uint32 ID)
	: m_Owner(owner)
	, m_DeviceMemoryAllocation(deviceMemoryAllocation)
	, m_ID(ID)
	, m_MaxSize(0)
	, m_UsedSize(0)
	, m_PeakNumAllocation(0)
	, m_FrameFreed(0)
{
	m_MaxSize = (uint32)deviceMemoryAllocation->Size();

	AllocRange Range;
	Range.Offset = 0;
	Range.Size = m_MaxSize;
	m_FreeList.push_back(Range);
}
ResourceHeapPage::~ResourceHeapPage()
{
	_ASSERT(!m_DeviceMemoryAllocation);
}

ResourceAllocation* ResourceHeapPage::TryAllocate(uint32 size, uint32 alignment, const char* File, uint32 Line)
{
	for (std::vector<AllocRange>::iterator it = m_FreeList.begin(); it != m_FreeList.end(); it++)
	{
		AllocRange& entry = (*it);

		uint32 allocatedOffset = entry.Offset;
		uint32 alignedOffset = Memory::Align(entry.Offset, alignment);
		uint32 alignmentAdjust = alignedOffset - entry.Offset;
		uint32 allocatedSize = alignmentAdjust + size;

		if (allocatedSize <= entry.Size)
		{
			if (allocatedSize < entry.Size)
			{
				// 할당한 목록에서 할당
				entry.Offset += allocatedSize;
				entry.Size += allocatedSize;
			}
			else
			{
				// entry는 free 목록에서 제거
				m_FreeList.erase(it);
			}

			m_UsedSize += allocatedSize;

			ResourceAllocation* NewAllocation = new ResourceAllocation(this, m_DeviceMemoryAllocation, size, alignedOffset, allocatedSize, allocatedOffset, File, Line);
			m_Allocations.push_back(NewAllocation);

			m_PeakNumAllocation = Math::Max<int32>(m_PeakNumAllocation, m_Allocations.size());
			return NewAllocation;
		}
	}

	return NULL;
}

ResourceAllocation* ResourceHeapPage::Allocate(uint32 size, uint32 alignment, const char* File, uint32 Line)
{
	ResourceAllocation* allocation = TryAllocate(size, alignment, File, Line);
	_ASSERT(allocation);

	return allocation;
}

void ResourceHeapPage::ReleaseAllocation(ResourceAllocation* allocation)
{
	std::vector<ResourceAllocation*>::iterator it = std::find(m_Allocations.begin(), m_Allocations.end(), allocation);
	m_Allocations.erase(it);

	m_UsedSize -= allocation->GetSize();

	if (JoinFreeBlocks())
	{
		m_Owner->FreePage(this);
	}
}

void AllocRange::JoinConsecutiveRanges(std::vector<AllocRange>& Ranges)
{
	if (Ranges.size() > 1)
	{
		std::sort(Ranges.begin(), Ranges.end());

		std::vector<AllocRange> NewList;
		for (int32 Index = Ranges.size() - 1; Index > 0; --Index)
		{
			AllocRange& Current = Ranges[Index];
			AllocRange& Prev = Ranges[Index - 1];
			if (Prev.Offset + Prev.Size == Current.Offset)
			{
				Prev.Size += Current.Size;
			}
			else
			{
				NewList.push_back(Current);
			}
		}

		Ranges.clear();
		Ranges.assign(NewList.begin(), NewList.end());
	}
}

bool ResourceHeapPage::JoinFreeBlocks()
{
	//FScopeLock ScopeLock(&GAllocationLock);
	AllocRange::JoinConsecutiveRanges(m_FreeList);

	if (m_FreeList.size() == 1)
	{
		if (m_Allocations.size() == 0)
		{
			_ASSERT(m_UsedSize == 0);
			_ASSERT(m_FreeList[0].Offset == 0 && m_FreeList[0].Size == m_MaxSize);
			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*																																				*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ResourceSubAllocation::ResourceSubAllocation(uint32 InRequestedSize, uint32 InAlignedOffset, uint32 InAllocationSize, uint32 InAllocationOffset)
	: m_RequestedSize(InRequestedSize)
	, m_AlignedOffset(InAlignedOffset)
	, m_AllocationSize(InAllocationSize)
	, m_AllocationOffset(InAllocationOffset)
{
}

SubResourceAllocator::SubResourceAllocator(ResourceHeapPool* owner, DeviceMemoryAllocation* deviceMemroyAllocation,
	uint32 InMemoryTypeIndex, VkMemoryPropertyFlags InMemoryPropertyFlags,
	uint32 InAlignment)
	: m_Owner(owner)
	, m_MemoryAllocation(deviceMemroyAllocation)
	, m_MemoryTypeIndex(InMemoryTypeIndex)
	, m_MemoryPropertyFlags(InMemoryPropertyFlags)
	, m_Alignment(InAlignment)
	, m_FrameFreed(0)
	, m_UsedSize(0)
{
	m_MaxSize = m_MemoryAllocation->Size();
	AllocRange range;
	range.Offset = 0;
	range.Size = m_MaxSize;
	m_FreeList.push_back(range);
}
SubResourceAllocator::~SubResourceAllocator()
{
	// Empty... Empty...
}

bool SubResourceAllocator::JoinFreeBlocks()
{
	//FScopeLock ScopeLock(&CS);
	AllocRange::JoinConsecutiveRanges(m_FreeList);

	if (m_FreeList.size() == 1)
	{
		if (m_Suballocations.size() == 0)
		{
			_ASSERT(m_UsedSize == 0);
			//checkf(FreeList[0].Offset == 0 && FreeList[0].Size == MaxSize, TEXT("Resource Suballocation leak, should have %d free, only have %d; missing %d bytes"), MaxSize, FreeList[0].Size, MaxSize - FreeList[0].Size);
			_ASSERT(m_FreeList[0].Offset == 0 && m_FreeList[0].Size == m_MaxSize);
			return true;
		}
	}
	return false;
}

ResourceSubAllocation* SubResourceAllocator::TryAllocateNoLocking(uint32 InSize, uint32 InAlignment, const char* File, uint32 Line)
{
	InAlignment = Math::Max<uint32>(InAlignment, m_Alignment);
	for (auto it = m_FreeList.begin(); it != m_FreeList.end(); it++)
	{
		AllocRange& entry = (*it);

		uint32 AllocatedOffset = entry.Offset;
		uint32 AlignedOffset = Memory::Align(entry.Offset, InAlignment);
		uint32 AlignmentAdjustment = AlignedOffset - entry.Offset;
		uint32 AllocatedSize = AlignmentAdjustment + InSize;
		if (AllocatedSize <= entry.Size)
		{
			if (AllocatedSize < entry.Size)
			{
				// Modify current free entry in-place
				entry.Size -= AllocatedSize;
				entry.Offset += AllocatedSize;
			}
			else
			{
				// Remove this free entry
				m_FreeList.erase(it);
			}

			m_UsedSize += AllocatedSize;

			ResourceSubAllocation* NewSuballocation = CreateSubAllocation(InSize, AlignedOffset, AllocatedSize, AllocatedOffset);// , File, Line);
			m_Suballocations.push_back(NewSuballocation);

			//PeakNumAllocations = Math::Max(PeakNumAllocations, ResourceAllocations.Num());
			return NewSuballocation;
		}
	}

	return nullptr;
}

BufferSubAllocation::BufferSubAllocation(BufferAllocation* InOwner, VkBuffer InHandle,
	uint32 InRequestedSize, uint32 InAlignedOffset,
	uint32 InAllocationSize, uint32 InAllocationOffset)
	: ResourceSubAllocation(InRequestedSize, InAlignedOffset, InAllocationSize, InAllocationOffset)
	, m_Owner(InOwner)
	, m_Handle(InHandle)
{
}

BufferSubAllocation::~BufferSubAllocation()
{
	m_Owner->Release(this);
}

// Returns the pointer to the mapped data for this SubAllocation, not the full buffer!
void* BufferSubAllocation::GetMappedPointer()
{
	return m_Owner->GetMappedPointer();
}

//
//
//
BufferAllocation::BufferAllocation(ResourceHeapPool* InOwner, DeviceMemoryAllocation* InDeviceMemoryAllocation,
	uint32 InMemoryTypeIndex, VkMemoryPropertyFlags InMemoryPropertyFlags,
	uint32 InAlignment,
	VkBuffer InBuffer, VkBufferUsageFlags InBufferUsageFlags)
	: SubResourceAllocator(InOwner, InDeviceMemoryAllocation, InMemoryTypeIndex, InMemoryPropertyFlags, InAlignment)
	, m_BufferUsageFlags(InBufferUsageFlags)
	, m_Buffer(InBuffer)
{
}

BufferAllocation::~BufferAllocation()
{
	_ASSERT(m_Buffer == VK_NULL_HANDLE);
}

ResourceSubAllocation* BufferAllocation::CreateSubAllocation(uint32 Size, uint32 AlignedOffset, uint32 AllocatedSize, uint32 AllocatedOffset)
{
	return new BufferSubAllocation(this, m_Buffer, Size, AlignedOffset, AllocatedSize, AllocatedOffset);// , File, Line);
}

void BufferAllocation::Destroy(VulkanDevice* Device)
{
	// Does not need to go in the deferred deletion queue
	vkDestroyBuffer(Device->GetHandle(), m_Buffer, nullptr);
	m_Buffer = VK_NULL_HANDLE;
}

void BufferAllocation::Release(BufferSubAllocation* Suballocation)
{
	{
		//FScopeLock ScopeLock(&CS);
		//m_Suballocations.RemoveSingleSwap(Suballocation, false);
		auto it = std::find(m_Suballocations.begin(), m_Suballocations.end(), Suballocation);
		m_Suballocations.erase(it);

		AllocRange NewFree;
		NewFree.Offset = Suballocation->m_AllocationOffset;
		NewFree.Size = Suballocation->m_AllocationSize;

		m_FreeList.push_back(NewFree);
	}

	m_UsedSize -= Suballocation->m_AllocationSize;
	_ASSERT(m_UsedSize >= 0);

	if (JoinFreeBlocks())
	{
		m_Owner->ReleaseBuffer(this);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // end of namespace "Vulkan"