#include "renderer.h"

/////////////////////////////////////////////////////////////////////////////
//
//
//
__ImplementRtti(VisualShock, VulkanColorSurface, VulkanRenderTexture2D);

VulkanColorSurface::VulkanColorSurface(VulkanDevice* device)
	: VulkanRenderTexture2D(device)
{
}
VulkanColorSurface::~VulkanColorSurface()
{
}

/////////////////////////////////////////////////////////////////////////////
//
//
//
__ImplementRtti(VisualShock, VulkanDepthStencilSurface, IDepthStencilSurface);

VulkanDepthStencilSurface::VulkanDepthStencilSurface(VulkanDevice* device)
	: IVulkanTexture(device)
	, m_MemoryAllocation(nullptr)
{
}
VulkanDepthStencilSurface::~VulkanDepthStencilSurface()
{
}

bool VulkanDepthStencilSurface::Create(uint32 width, uint32 height, uint32 format, uint32 samples)
{
	m_Width = width;
	m_Height = height;
	m_PixelFormat = format;
	m_MultiSamples = samples;

	// 1. CreateImage
	VkImageCreateInfo createInfo;
	{
		Memory::Memzero(createInfo);
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		createInfo.flags = 0;
		createInfo.pNext = nullptr;
		createInfo.imageType = VK_IMAGE_TYPE_2D;
		createInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT /*| VK_IMAGE_USAGE_TRANSFER_SRC_BIT*/;
		createInfo.mipLevels = 1;
		createInfo.arrayLayers = 1;
		createInfo.extent = { width, height, 1 };
		createInfo.samples = (VkSampleCountFlagBits)samples;
		createInfo.mipLevels = 1;
		createInfo.format = m_Device->GetFormat(format);
		createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		//createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	}
	VK_RESULT(vkCreateImage(m_Device->GetHandle(), &createInfo, nullptr, &m_ImageHandle));

	// 2. Create Memory / Bind Memory
	VkMemoryRequirements memoryRequirements;
	vkGetImageMemoryRequirements(m_Device->GetHandle(), m_ImageHandle, &memoryRequirements);

	Vulkan::DeviceMemoryPool& deviceMemoryManager = m_Device->MemoryManager();

	m_MemoryAllocation = deviceMemoryManager.Allocate(memoryRequirements.size, memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	VK_RESULT(vkBindImageMemory(m_Device->GetHandle(), m_ImageHandle, m_MemoryAllocation->Handle(), m_MemoryAllocation->Offset()));

	// 3. Create Image View
	m_TextureView.Create(m_Device, m_ImageHandle, VK_IMAGE_VIEW_TYPE_2D, format, VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT);
	return true;
}

void VulkanDepthStencilSurface::Destroy()
{
	if (m_ImageHandle)
	{
		m_TextureView.Destroy(m_Device);
		vkDestroyImage(m_Device->GetHandle(), m_ImageHandle, nullptr);

		m_Device->MemoryManager().Free(m_MemoryAllocation);
		m_MemoryAllocation = nullptr;
	}
}

void VulkanDepthStencilSurface::SetSurface()
{
	// EMPTY!!!! EMPTY!!!! EMPTY!!!! EMPTY!!!! EMPTY!!!! EMPTY!!!! EMPTY!!!!
}

/////////////////////////////////////////////////////////////////////////////
//
//
//
__ImplementRtti(VisualShock, VulkanBackBuffer, VulkanRenderTexture2D);

VulkanBackBuffer::VulkanBackBuffer(VulkanDevice* device, VkImage imageHandle)
	: VulkanRenderTexture2D(device)
{
	m_ImageHandle = imageHandle;
}
VulkanBackBuffer::~VulkanBackBuffer()
{
}
