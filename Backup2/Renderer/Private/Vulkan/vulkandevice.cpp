#include "renderer.h"

VulkanDevice::VulkanDevice(VkPhysicalDevice GPU)
	: m_Gpu(GPU)
	, m_Device(VK_NULL_HANDLE)
	, m_CmdDebugMarkerBegin(nullptr)
	, m_CmdDebugMarkerEnd(nullptr)
	, m_QueueFlags(0)
	, m_UniformBufferUploader(nullptr)
	, m_GfxPipelineManager(nullptr)
{
	// 물리적 장치에 대한 정보들을 체크한다.

	::vkGetPhysicalDeviceProperties(m_Gpu, &m_GpuProps);
	::vkGetPhysicalDeviceFeatures(m_Gpu, &m_GpuFeatures);

	// CommandBuffer는 Queue를 통해서 실행을 위해 하드웨어에 전달된다.
	// 하지만, 버퍼는 Graphics Command, Compute Command와 같이 다른 타입의 Operation을 포함할 것이다. 이게 Queue가 타른 타입으로 나뉘어져있는 이유이다.
	// 각 Queue Family는 다른 타입의 Operation을 지원한다.
	// PhysicalDevice가 우리가 수행하기를 원하는 Operation의 타입을 지원하는지 체크해야만 한다.

	// QueueFamilyProps
	uint32 queueCount;
	::vkGetPhysicalDeviceQueueFamilyProperties(m_Gpu, &queueCount, nullptr);

	m_QueueFamilyProps.resize(queueCount);
	::vkGetPhysicalDeviceQueueFamilyProperties(m_Gpu, &queueCount, &m_QueueFamilyProps[0]);

	// Device Type
	std::string deviceType;
	switch (m_GpuProps.deviceType)
	{
	case VK_PHYSICAL_DEVICE_TYPE_OTHER:				deviceType = "Other";			break;
	case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:	deviceType = "Intergrated GPU";	break;
	case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:		deviceType = "Discrete GPU";	break;
	case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:		deviceType = "Virtual GPU";		break;
	case VK_PHYSICAL_DEVICE_TYPE_CPU:				deviceType = "CPU";				break;
	default:										deviceType = "Unknown";			break;
	}

	PlatformUtil::DebugOutFormat("* DeviceID: %d, DeviceName: %s, DeviceType: %s \n", m_GpuProps.deviceID, m_GpuProps.deviceName, deviceType.c_str());
	PlatformUtil::DebugOutFormat("* API 0x%x Driver 0x%x VendorId 0x%x\n", m_GpuProps.apiVersion, m_GpuProps.driverVersion, m_GpuProps.vendorID);

	for (uint32 familyIndex = 0; familyIndex < m_QueueFamilyProps.size(); familyIndex++)
	{
		const VkQueueFamilyProperties& prop = m_QueueFamilyProps[familyIndex];

		std::string desc;
		if ((prop.queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
		{
			m_QueueFlags |= VK_QUEUE_GRAPHICS_BIT;
			desc += (" Gfx");
		}
		if ((prop.queueFlags & VK_QUEUE_COMPUTE_BIT) == VK_QUEUE_COMPUTE_BIT)
		{
			m_QueueFlags |= VK_QUEUE_COMPUTE_BIT;
			desc += (desc.empty() ? std::string("") : std::string(" |")) + std::string(" Compute");
		}
		if ((prop.queueFlags & VK_QUEUE_TRANSFER_BIT) == VK_QUEUE_TRANSFER_BIT)
		{
			m_QueueFlags |= VK_QUEUE_TRANSFER_BIT;
			desc += (desc.empty() ? std::string("") : std::string(" |")) + std::string(" Transfer");
		}
		if ((prop.queueFlags & VK_QUEUE_SPARSE_BINDING_BIT) == VK_QUEUE_SPARSE_BINDING_BIT)
		{
			m_QueueFlags |= VK_QUEUE_SPARSE_BINDING_BIT;
			desc += (desc.empty() ? std::string("") : std::string(" |")) + std::string(" Sparse");
		}

		PlatformUtil::DebugOutFormat("* QueueFamilyIndex: %d, queueCount: %d : %s \n", familyIndex, prop.queueCount, desc.c_str());
	}
}
VulkanDevice::~VulkanDevice()
{
}

bool VulkanDevice::IsSupportGPU(VkQueueFlagBits operationType) const
{
	return (m_QueueFlags&operationType) ? true : false;
}

bool VulkanDevice::IsDiscreteGPU() const
{
	return m_GpuProps.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

bool VulkanDevice::Create()
{
	uint32 renderQueueFamilyIndex = UINT32_MAX;
	uint32 totalQueueCount = 0;

	////
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos(m_QueueFamilyProps.size());
	for (uint32 familyIndex = 0; familyIndex < m_QueueFamilyProps.size(); familyIndex++)
	{
		const VkQueueFamilyProperties& prop = m_QueueFamilyProps[familyIndex];

		VkDeviceQueueCreateInfo& info = queueCreateInfos[familyIndex];
		info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		info.pNext = nullptr;
		info.flags = 0;
		info.queueFamilyIndex = familyIndex;
		info.queueCount = prop.queueCount;

		totalQueueCount += prop.queueCount;

		if ((prop.queueFlags & VK_QUEUE_GRAPHICS_BIT) == VK_QUEUE_GRAPHICS_BIT)
		{
			if (renderQueueFamilyIndex == UINT32_MAX)
				renderQueueFamilyIndex = familyIndex;
		}
	}

	std::vector<float> queueProperties(totalQueueCount);
	float* CurrentPriority = &queueProperties[0];
	for (uint32 index = 0; index < m_QueueFamilyProps.size(); ++index)
	{
		const VkQueueFamilyProperties& CurrProps = m_QueueFamilyProps[index];

		VkDeviceQueueCreateInfo& info = queueCreateInfos[index];
		info.pQueuePriorities = CurrentPriority;
		for (int32 QueueIndex = 0; QueueIndex < (int32)CurrProps.queueCount; ++QueueIndex)
		{
			*CurrentPriority++ = 1.0f;
		}
	}

	bool debugMarkers;
	std::vector<const char*> deviceExtensions;
	std::vector<const char*> deviceLayers;
	GetDeviceLayerAndExtensions(deviceExtensions, deviceLayers, debugMarkers);

	VkDeviceCreateInfo deviceInfo;
	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceInfo.pNext = nullptr;
	deviceInfo.flags = 0;
	deviceInfo.enabledExtensionCount = deviceExtensions.size();
	deviceInfo.ppEnabledExtensionNames = &deviceExtensions[0];
	deviceInfo.enabledLayerCount = deviceLayers.size();
	deviceInfo.ppEnabledLayerNames = deviceLayers.empty() ? nullptr : &deviceLayers[0];
	deviceInfo.queueCreateInfoCount = m_QueueFamilyProps.size();
	deviceInfo.pQueueCreateInfos = &queueCreateInfos[0];
	deviceInfo.pEnabledFeatures = &m_GpuFeatures;

	VkResult result = ::vkCreateDevice(m_Gpu, &deviceInfo, nullptr, &m_Device);
	if (result != VK_SUCCESS)
	{
		return false;
	}

	m_Queue = new VulkanQueue(this, renderQueueFamilyIndex, 0);

#if VULKAN_ENABLE_DRAW_MARKERS
	if (debugMarkers)
	{
		m_CmdDebugMarkerBegin = (PFN_vkCmdDebugMarkerBeginEXT)(void*)::vkGetDeviceProcAddr(m_Device, "vkCmdDebugMarkerBeginEXT");
		m_CmdDebugMarkerEnd = (PFN_vkCmdDebugMarkerEndEXT)(void*)::vkGetDeviceProcAddr(m_Device, "vkCmdDebugMarkerEndEXT");
	}
#endif

	SetupFormat();

	m_GfxPipelineManager = new VulkanGfxPipelineManager();
	m_GfxPipelineManager->Init(this);

	m_MemoryManager.Init(this);
	m_ResourceHeapManager.Init(this);
	m_StagingBufferManager.Init(this);

	m_FencePool.Init(this);

	m_UniformBufferUploader = new VulkanUniformBufferUploader(this, VULKAN_UB_RING_BUFFER_SIZE);

	m_DescriptorPools.push_back(new VulkanDescriptorPool(this));

	m_CmdBufferPool.Init(this, PoolCreateFlags::_RESET_COMMAND_BUFFER_BIT);
	return true;
}

void VulkanDevice::Destroy()
{
	for (auto pool : m_DescriptorPools)
		delete pool;
	m_DescriptorPools.clear();

	m_StagingBufferManager.Destroy();
	m_ResourceHeapManager.Destroy();
	m_MemoryManager.Destroy();
	m_CmdBufferPool.Destroy();
	m_FencePool.Destroy();

	if (m_GfxPipelineManager)
	{
		m_GfxPipelineManager->Destroy();
		delete m_GfxPipelineManager;
		m_GfxPipelineManager = nullptr;
	}

	if (m_UniformBufferUploader)
	{
		delete m_UniformBufferUploader;
		m_UniformBufferUploader = nullptr;
	}

	if (m_Queue)
	{
		delete m_Queue;
		m_Queue = nullptr;
	}

	if (m_Device != VK_NULL_HANDLE)
	{
		::vkDestroyDevice(m_Device, nullptr);
		m_Device = VK_NULL_HANDLE;
	}
}

void VulkanDevice::PrepareForDestroy()
{
	WaitForIdle();
}

void VulkanDevice::WaitForIdle()
{
	if (m_Device == VK_NULL_HANDLE)
		return;

	// 디바이스에 모든 Queue에 Submit된 모든 Command들이 실행이 완료될 때까지 기다린다.
	// Queue->WaitForIdle은 Queue 하나에 대해서 WaitForIdle을 처리한다.
	VK_RESULT(::vkDeviceWaitIdle(m_Device));
}

//////////////////////////////////////////////////////////////////////////////////////
struct _VULKANFORMAT
{
	VkFormat format;
	VkComponentMapping mapping;
	uint32 bpp;
	bool isSupported;
};
static _VULKANFORMAT GPixelFormat[RT_MAX];

static const VkComponentMapping ComponentIdentity = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };
static const VkComponentMapping ComponentR = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO };
static const VkComponentMapping ComponentGR = { VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_ZERO, VK_COMPONENT_SWIZZLE_ZERO };
static const VkComponentMapping ComponentRGBA = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };
static const VkComponentMapping ComponentBGRA = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G, VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };

void SetBpp(uint32 format, uint32 bpp)
{
	GPixelFormat[format].bpp = bpp;
}

void VulkanDevice::SetFormat(uint32 format, VkFormat vkFormat, const VkComponentMapping& vkComponent)
{
	GPixelFormat[format].format = vkFormat;
	GPixelFormat[format].mapping = vkComponent;
	GPixelFormat[format].isSupported = IsFormatSupported(vkFormat);
}

void VulkanDevice::SetupFormat()
{
	for (uint32 Index = 0; Index < VK_FORMAT_RANGE_SIZE; ++Index)
	{
		const VkFormat Format = (VkFormat)Index;
		Memory::Memzero(m_FormatProperties[Index]);
		vkGetPhysicalDeviceFormatProperties(m_Gpu, Format, &m_FormatProperties[Index]);
	}

	for (int i = 0; i < RT_MAX; i++)
	{
		GPixelFormat[i].format = VK_FORMAT_UNDEFINED;
		GPixelFormat[i].mapping = ComponentRGBA;
		GPixelFormat[i].bpp = 0;
	}

	SetFormat(RT_B8G8R8A8, VK_FORMAT_B8G8R8A8_UNORM, ComponentBGRA);
	SetFormat(RT_R8G8B8A8, VK_FORMAT_R8G8B8A8_UNORM, ComponentRGBA);
	SetFormat(RT_D24S8, VK_FORMAT_D24_UNORM_S8_UINT, ComponentIdentity);	// VK_FORMAT_D32_SFLOAT_S8_UINT
	SetFormat(RT_D16, VK_FORMAT_D16_UNORM, ComponentR);
	SetFormat(RT_R16F, VK_FORMAT_R16_SFLOAT, ComponentR);
	SetFormat(RT_G16R16F, VK_FORMAT_R16G16_SFLOAT, ComponentGR);
	SetFormat(RT_A16B16G16R16F, VK_FORMAT_R16G16B16A16_SFLOAT, ComponentRGBA);
	SetFormat(RT_R32F, VK_FORMAT_R32_SFLOAT, ComponentR);
	SetFormat(RT_G32R32F, VK_FORMAT_R32G32_SFLOAT, ComponentGR);
	SetFormat(RT_A32B32G32R32F, VK_FORMAT_R32G32B32A32_SFLOAT, ComponentRGBA);
}

VkFormat VulkanDevice::GetFormat(uint32 format) const
{
	return GPixelFormat[format].format;
}

const VkComponentMapping& VulkanDevice::GetFormatCommpoentMapping(uint32 format) const
{
	return GPixelFormat[format].mapping;
}

uint32 VulkanDevice::GetFormatBlockByteSize(uint32 format) const
{
	uint32 bpp = 0;
	switch (format)
	{
	case RT_R5G6B5:
	case RT_R5G5B5A1:
	case RT_R4G4B4A4:
		bpp = 16;
		break;
	case RT_R8G8B8:
		bpp = 24;
		break;
	case RT_R8G8B8A8:
	case RT_B8G8R8A8:
		bpp = 32;
		break;
	default:
		_ASSERT(0);
	};
	return bpp / 8;
}

bool VulkanDevice::IsFormatSupported(VkFormat Format) const
{
	auto ArePropertiesSupported = [](const VkFormatProperties& Prop) -> bool
	{
		return (Prop.bufferFeatures != 0) || (Prop.linearTilingFeatures != 0) || (Prop.optimalTilingFeatures != 0);
	};

	if (Format >= 0 && Format < VK_FORMAT_RANGE_SIZE)
	{
		const VkFormatProperties& Prop = m_FormatProperties[Format];
		return ArePropertiesSupported(Prop);
	}

	// Check for extension formats
	auto it = m_ExtensionFormatProperties.find(Format);
	if (it == m_ExtensionFormatProperties.end())
	{
		return ArePropertiesSupported(it->second);
	}

	// Add it for faster caching next time
	VkFormatProperties NewProperties;
	Memory::Memzero(NewProperties);
	vkGetPhysicalDeviceFormatProperties(m_Gpu, Format, &NewProperties);
	m_ExtensionFormatProperties.insert(std::make_pair(Format, NewProperties));

	return ArePropertiesSupported(NewProperties);
}

VkSampleCountFlagBits VulkanDevice::GetSampleCount(uint32 sampleCount) const
{
	VkSampleCountFlagBits sampleBit;

	switch (sampleCount)
	{
	case 1:		sampleBit = VK_SAMPLE_COUNT_1_BIT;	break;
	case 2:		sampleBit = VK_SAMPLE_COUNT_2_BIT;	break;
	case 4:		sampleBit = VK_SAMPLE_COUNT_4_BIT;	break;
	case 8:		sampleBit = VK_SAMPLE_COUNT_8_BIT;	break;
	case 16:	sampleBit = VK_SAMPLE_COUNT_16_BIT;	break;
	case 32:	sampleBit = VK_SAMPLE_COUNT_32_BIT;	break;
	case 64:	sampleBit = VK_SAMPLE_COUNT_64_BIT;	break;
	default:	_ASSERT(0);							break;
	}

	return sampleBit;
}

VulkanDescriptorPool* VulkanDevice::AllocateDescriptorSets(/*const VkDescriptorSetAllocateInfo& allocateInfo, */const VulkanDescriptorSetsLayout& layout, VkDescriptorSet* OutSets)
{
	// DescriptorSet 생성
#if 0
	VkDescriptorSetAllocateInfo DescriptorAllocateInfo = allocateInfo;
#else
	VkDescriptorSetAllocateInfo DescriptorAllocateInfo;
	Memory::Memzero(DescriptorAllocateInfo);
	DescriptorAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	DescriptorAllocateInfo.descriptorSetCount = layout.GetHandles().size();
	DescriptorAllocateInfo.pSetLayouts = layout.GetHandles().data();
#endif

	VulkanDescriptorPool* DescriptorPool = m_DescriptorPools.back();

	VkResult Result = VK_ERROR_OUT_OF_DEVICE_MEMORY;

	if (DescriptorPool->CanAllocate(layout))
	{
		DescriptorAllocateInfo.descriptorPool = DescriptorPool->Handle();
		Result = vkAllocateDescriptorSets(GetHandle(), &DescriptorAllocateInfo, OutSets);
	}

	if (Result < VK_SUCCESS)
	{
		if (DescriptorPool->IsEmpty())
		{
			VK_RESULT(Result);
		}
		else
		{
			DescriptorPool = new VulkanDescriptorPool(this);
			m_DescriptorPools.push_back(DescriptorPool);

			DescriptorAllocateInfo.descriptorPool = DescriptorPool->Handle();
			VK_RESULT_EXPAND(vkAllocateDescriptorSets(GetHandle(), &DescriptorAllocateInfo, OutSets));
		}
	}

	return DescriptorPool;
}

//////////////////////////////////////////////////////////////////

const char* VkErrorString(VkResult result)
{
	switch (result)
	{
#define STR(r) case VK_ ##r: return #r
		STR(NOT_READY);
		STR(TIMEOUT);
		STR(EVENT_SET);
		STR(EVENT_RESET);
		STR(INCOMPLETE);
		STR(ERROR_OUT_OF_HOST_MEMORY);
		STR(ERROR_OUT_OF_DEVICE_MEMORY);
		STR(ERROR_INITIALIZATION_FAILED);
		STR(ERROR_DEVICE_LOST);
		STR(ERROR_MEMORY_MAP_FAILED);
		STR(ERROR_LAYER_NOT_PRESENT);
		STR(ERROR_EXTENSION_NOT_PRESENT);
		STR(ERROR_FEATURE_NOT_PRESENT);
		STR(ERROR_INCOMPATIBLE_DRIVER);
		STR(ERROR_TOO_MANY_OBJECTS);
		STR(ERROR_FORMAT_NOT_SUPPORTED);
		STR(ERROR_SURFACE_LOST_KHR);
		STR(ERROR_NATIVE_WINDOW_IN_USE_KHR);
		STR(SUBOPTIMAL_KHR);
		STR(ERROR_OUT_OF_DATE_KHR);
		STR(ERROR_INCOMPATIBLE_DISPLAY_KHR);
		STR(ERROR_VALIDATION_FAILED_EXT);
		STR(ERROR_INVALID_SHADER_NV);
#undef STR
default:
	return "UNKNOWN_ERROR";
	}
}

void VulkanDevice::CheckResult(VkResult result, const char* func, const char* file, const int line, int level)
{
	PlatformUtil::DebugOut("**********************************************************\n");
	PlatformUtil::DebugOut("[ERROR] VkResult is Failed\n");
	PlatformUtil::DebugOutFormat("\t*ErrorCode: %s \n", VkErrorString(result), file, line);
	PlatformUtil::DebugOutFormat("\t*Func: %s \n", func);
	PlatformUtil::DebugOutFormat("\t*File: %s : %d\n", file, line);
	PlatformUtil::DebugOut("**********************************************************\n");

	if (level == 1)
	{
		_ASSERT(result == VK_SUCCESS);
	}
}
