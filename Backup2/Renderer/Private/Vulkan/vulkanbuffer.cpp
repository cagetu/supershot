#include "renderer.h"

IVulkanBuffer::IVulkanBuffer(VulkanDevice* device)
	: m_Device(device)
	, m_Buffer(VK_NULL_HANDLE)
#ifdef _MANAGED_BUFFER
	, m_BufferAllocation(nullptr)
#else
	, m_MemoryAllocation(nullptr)
#endif
{
}
IVulkanBuffer::~IVulkanBuffer()
{
	_ASSERT(m_Buffer == VK_NULL_HANDLE);
}

bool IVulkanBuffer::CreateBuffer(uint32 bufferSize, uint32 bufferUsage, uint32 memoryType)
{
	// 1. Create Buffer
#ifdef _MANAGED_BUFFER
	m_BufferAllocation = m_Device->ResourceHeapPool().AllocateBuffer(bufferSize, bufferUsage, memoryType, __FILE__, __LINE__);
	m_Buffer = m_BufferAllocation->GetHandle();
#else
	VkBufferCreateInfo createInfo;
	Memory::Memzero(createInfo);
	createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	createInfo.flags = 0;
	createInfo.pNext = nullptr;
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;	// 오직 하나의 Queue Family가 한번에 하나의 Image를 사용할 때, "exclusive" sharing mode를 사용한다.
	//VK_SHARING_MODE_CONCURRENT일 경우에만 사용
	createInfo.queueFamilyIndexCount = 0;
	createInfo.pQueueFamilyIndices = nullptr;
	createInfo.size = bufferSize;
	//Usage
	createInfo.usage = bufferUsage;

	VK_RESULT(vkCreateBuffer(m_Device->GetHandle(), &createInfo, nullptr, &m_Buffer));

	VkMemoryRequirements memoryRequirements;
	vkGetBufferMemoryRequirements(m_Device->GetHandle(), m_Buffer, &memoryRequirements);

	// 2. Create Memory / Bind Memory
	Vulkan::DeviceMemoryPool& deviceMemoryManager = m_Device->MemoryManager();
	m_MemoryAllocation = deviceMemoryManager.Allocate(memoryRequirements.size, memoryRequirements.memoryTypeBits, memoryType/*VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT*/);
	VK_RESULT(vkBindBufferMemory(m_Device->GetHandle(), m_Buffer, m_MemoryAllocation->Handle(), m_MemoryAllocation->Offset()));
#endif
	return true;
}

void IVulkanBuffer::DestroyBuffer()
{
#ifdef _MANAGED_BUFFER
	if (m_BufferAllocation)
	{
		delete m_BufferAllocation;
		m_BufferAllocation = nullptr;
	}
#else
	if (m_Buffer != VK_NULL_HANDLE)
	{
		vkDestroyBuffer(m_Device->GetHandle(), m_Buffer, nullptr);
		m_Buffer = VK_NULL_HANDLE;

		m_Device->MemoryManager().Free(m_MemoryAllocation);
		m_MemoryAllocation = nullptr;
	}
#endif
}