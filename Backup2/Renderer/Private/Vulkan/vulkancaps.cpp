#include "renderer.h"

void VulkanCaps::Setup(VkPhysicalDevice device)
{
	VkFormatProperties properties;
	vkGetPhysicalDeviceFormatProperties(device, VK_FORMAT_ASTC_4x4_UNORM_BLOCK, &properties);
	bSupportsASTC = (properties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT) != 0;
	PlatformUtil::DebugOutFormat("");
}