#include "renderer.h"

__ImplementRtti(VisualShock, VulkanComputePipeline, VulkanPipeline);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VulkanComputePipeline::VulkanComputePipeline(VulkanDevice* device, int32 pipelineIndex, VkComputePipelineCreateInfo& pipelineCreateInfo, VkPipelineCache pipelineCache)
	: VulkanPipeline(device, pipelineIndex)
{
	VK_RESULT(vkCreateComputePipelines(m_Device->GetHandle(), pipelineCache, 1, &pipelineCreateInfo, nullptr, &m_Handle));
}
VulkanComputePipeline::~VulkanComputePipeline()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VulkanComputePipelineManager::VulkanComputePipelineManager()
{
}

VulkanComputePipelineManager::~VulkanComputePipelineManager()
{
}

void VulkanComputePipelineManager::Init(VulkanDevice* device)
{
	VulkanPipelineManager::Init(device);
}

void VulkanComputePipelineManager::Destroy()
{
	VulkanPipelineManager::Destroy();
}
