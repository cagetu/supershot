#include "renderer.h"
#include "vulkanvertextypes.h"

void VulkanVertexDescription::Clear()
{
	Memory::Memzero(streams); 
	Memory::Memset(locations, -1);
	vertexSize = 0;
}

void VulkanVertexDescription::GetDesc(VulkanVertexDescription* Output, const VulkanShaderResource* vertexShader, bool packed)
{
	Output->Clear();

	const std::vector<VulkanShaderResource::Attribute>& attributes = vertexShader->GetInputs();

	int32 location = 0;
	for (auto attrib : attributes)
	{
		for (int32 index = 0; index < _countof(GFVFPreset); index++)
		{
			const FVFInfo& Preset = GFVFPreset[index];

			if (Preset.attrib == attrib.name && Preset.packed == packed)
			{
				// element
				Output->streams[Preset.streamID].elements |= Preset.mask;
				Output->streams[Preset.streamID].stride += Preset.len;
				Output->streams[Preset.streamID].instanced = Preset.instanced;
				// location
				Output->locations[location++] = attrib.location;
				// length
				Output->vertexSize += Preset.len;
#ifdef _DEBUG
				PlatformUtil::DebugOutFormat("[Shader Attributes] [%d] : %s (%d)\n", Preset.streamID, Preset.attrib, Preset.len);
#endif
				break;
			}
		}
	}
}