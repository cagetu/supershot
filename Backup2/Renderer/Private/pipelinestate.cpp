#include "renderer.h"

namespace PipelineState
{
	bool ViewportDesc::operator == (const ViewportDesc& rhs) const
	{
		return (x == rhs.x) && (y == rhs.y) && (width == rhs.width) && (height && rhs.height) && (minDepth == rhs.minDepth) && (maxDepth == rhs.maxDepth) ? true : false;
	}
	bool ViewportDesc::operator != (const ViewportDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////

	bool ScissorDesc::operator == (const ScissorDesc& rhs) const
	{
		return (x == rhs.x) && (y == rhs.y) && (width == rhs.width) && (height && rhs.height) ? true : false;
	}
	bool ScissorDesc::operator != (const ScissorDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DepthBiasDesc::operator == (const DepthBiasDesc& rhs) const
	{
		return (DepthBiasConstantFactor == rhs.DepthBiasConstantFactor) && (DepthBiasSlopeFactor == rhs.DepthBiasSlopeFactor) &&
				(DepthBiasClamp == rhs.DepthBiasClamp) && (bDepthBiasEnable == rhs.bDepthBiasEnable) ? true : false;
	}
	bool DepthBiasDesc::operator != (const DepthBiasDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DepthBoundTestDesc::operator == (const DepthBoundTestDesc& rhs) const
	{
		return (minDepthBound == rhs.minDepthBound) && (maxDepthBound == rhs.maxDepthBound) &&
			(bDepthBoundTestEnable == rhs.bDepthBoundTestEnable) ? true : false;
	}
	bool DepthBoundTestDesc::operator != (const DepthBoundTestDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool StencilTestDesc::operator == (const StencilTestDesc& rhs) const
	{
		return (bStencilTestEnable == rhs.bStencilTestEnable) && (front == rhs.front) && (back == rhs.back);
	}
	bool StencilTestDesc::operator != (const StencilTestDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool BlendConstant::operator == (const BlendConstant& rhs) const
	{
		for (int32 i = 0; i < 4; i++)
		{
			if (blendConstants[i] != rhs.blendConstants[i])
				return false;
		}
		return true;
	}
	bool BlendConstant::operator != (const BlendConstant& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool MultiSampleDesc::operator == (const MultiSampleDesc& rhs) const
	{
		if (sampleCount != rhs.sampleCount)
			return false;
		if (sampleMask != rhs.sampleMask)
			return false;
		if (rasterizationSamples != rhs.rasterizationSamples)
			return false;

		// Alpha Coverage
		if (alphaToCoverageEnable != rhs.alphaToCoverageEnable)
			return false;
		if (alphaToOneEnable != rhs.alphaToOneEnable)
			return false;

		// SampleShading
		if (sampleShadingEnable != rhs.sampleShadingEnable)
			return false;
		if (minSampleShading != rhs.minSampleShading)
			return false;

		return true;
	}
	bool MultiSampleDesc::operator != (const MultiSampleDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool RasterizationDesc::operator == (const RasterizationDesc& rhs) const
	{
		if (CullMode != rhs.CullMode)
			return false;

		if (FillMode != rhs.FillMode)
			return false;

		if (bSkipRasterizer != rhs.bSkipRasterizer)
			return false;

		return true;
	}
	bool RasterizationDesc::operator != (const RasterizationDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool DepthTestDesc::operator == (const DepthTestDesc& rhs) const
	{
		if (DepthCompareOp != rhs.DepthCompareOp)
			return false;

		if (bDepthTestEnable != rhs.bDepthTestEnable)
			return false;

		if (bDepthWriteEnable != rhs.bDepthWriteEnable)
			return false;

		return true;
	}
	bool DepthTestDesc::operator != (const DepthTestDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool ColorBlendDesc::operator == (const ColorBlendDesc& rhs) const
	{
		if (bBlendEnable != rhs.bBlendEnable)
			return false;

		// AlphaBlend
		if (alphaBlendOp != rhs.alphaBlendOp)
			return false;
		if (srcAlphaBlendFactor != rhs.srcAlphaBlendFactor)
			return false;
		if (dstAlphaBlendFactor != rhs.dstAlphaBlendFactor)
			return false;

		// ColorBlend
		if (colorWriteMask != rhs.colorWriteMask)
			return false;
		if (colorBlendOp != rhs.colorBlendOp)
			return false;
		if (srcColorBlendFactor != rhs.srcColorBlendFactor)
			return false;
		if (dstColorBlendFactor != rhs.dstColorBlendFactor)
			return false;

		return true;
	}
	bool ColorBlendDesc::operator != (const ColorBlendDesc& rhs) const
	{
		return (*this == rhs) ? false : true;
	}
}
