#include "renderer.h"

Variant Variant::null;

Variant & Variant::operator = ( const Variant& rhs )
{
	Delete();

	m_type = rhs.m_type;

	switch (m_type)
	{
		case _Void:
			break;
		case _Int:
			m_Value.Int = rhs.m_Value.Int;
			break;
		case _Float:
			m_Value.Float[0] = rhs.m_Value.Float[0];
			break;
		case _Bool:
			m_Value.Bool = rhs.m_Value.Bool;
			break;
		case _Vector2:
		case _Vector3:
		case _Vector4:
			m_Value.Float[0] = rhs.m_Value.Float[0];
			m_Value.Float[1] = rhs.m_Value.Float[1];
			m_Value.Float[2] = rhs.m_Value.Float[2];
			m_Value.Float[3] = rhs.m_Value.Float[3];
			break;
		case _Matrix3:
		case _Matrix4:
			m_Value.Matrix = rhs.m_Value.Matrix;
			break;
		case _Texture:
			m_Value.Texture = rhs.m_Value.Texture;
			break;
		case _Float_Array:
			Duplicate(rhs.m_Value.Array.Float, sizeof(float), rhs.m_Value.Array.Count);
			break;
		case _Vector2_Array:
			Duplicate(rhs.m_Value.Array.Vector2, sizeof(Vector2), rhs.m_Value.Array.Count);
			break;
		case _Vector3_Array:
			Duplicate(rhs.m_Value.Array.Vector3, sizeof(Vector3), rhs.m_Value.Array.Count);
			break;
		case _Vector4_Array:
			Duplicate(rhs.m_Value.Array.Vector4, sizeof(Vector4), rhs.m_Value.Array.Count);
			break;
		case _Matrix_Array:
			Duplicate(rhs.m_Value.Array.Matrix, sizeof(Mat4), rhs.m_Value.Array.Count);
			break;
		default:
			_ASSERT(0);
			break;
	}
	return *this;
}

bool Variant::operator == ( const Variant& rhs ) const
{
	if (rhs.m_type == m_type)
	{
		switch (rhs.m_type)
		{
		case _Void:
			return true;
		case _Int:
			return (m_Value.Int == rhs.m_Value.Int);
		case _Bool:
			return (m_Value.Bool == rhs.m_Value.Bool);
		case _Float:
			return (m_Value.Float[0] == rhs.m_Value.Float[0]);
		case _Vector2:
		case _Vector3:
		case _Vector4:
			return !PlatformUtil::Memcmp(m_Value.Float, rhs.m_Value.Float, sizeof(float) * 4);
		case _Texture:
			return (m_Value.Texture == rhs.m_Value.Texture);
		case _Matrix4:
		case _Matrix3:
			return !PlatformUtil::Memcmp(&m_Value.Matrix, &rhs.m_Value.Matrix, sizeof(Mat4));
		case _Float_Array:
			return count() == rhs.count() && !PlatformUtil::Memcmp(m_Value.Array.Float, rhs.m_Value.Array.Float, sizeof(float)*count()) ? true : false;
		case _Vector2_Array:
			return count() == rhs.count() && !PlatformUtil::Memcmp(m_Value.Array.Vector2, rhs.m_Value.Array.Vector2, sizeof(Vector2)*count()) ? true : false;
		case _Vector3_Array:
			return count() == rhs.count() && !PlatformUtil::Memcmp(m_Value.Array.Vector3, rhs.m_Value.Array.Vector3, sizeof(Vector3)*count()) ? true : false;
		case _Vector4_Array:
			return count() == rhs.count() && !PlatformUtil::Memcmp(m_Value.Array.Vector4, rhs.m_Value.Array.Vector4, sizeof(Vector4)*count()) ? true : false;
		case _Matrix_Array:
			return count() == rhs.count() && !PlatformUtil::Memcmp(m_Value.Array.Matrix, rhs.m_Value.Array.Matrix, sizeof(Mat4)*count()) ? true : false;
		default:
			_ASSERT(0);
			return false;
		}
	}
	return false;
}

void Variant::Delete()
{
	if (_Float_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
			delete[] (m_Value.Array.Float);
    }
	else if (_Vector2_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vector2) > minsize)
			delete[] (m_Value.Array.Vector2);
    }
	else if (_Vector3_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vector3) > minsize)
			delete[] (m_Value.Array.Vector3);
    }
	else if (_Vector4_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vector4) > minsize)
			delete[] (m_Value.Array.Vector4);
    }
	else if (_Matrix_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
			delete[] (m_Value.Array.Matrix);
    }
	m_type = _Void;
}

void Variant::Reset()
{
	if (_Float_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(float) > minsize)
		{
			for(unsigned int i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Float[i] = 0.0f;
		}
    }
	else if (_Vector2_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vector2) > minsize)
		{
			for(unsigned int i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector2[i].x = m_Value.Array.Vector2[i].y = 0.0f;
		}
    }
	else if (_Vector3_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vector3) > minsize)
		{
			for(unsigned int i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector3[i].x = m_Value.Array.Vector3[i].y = m_Value.Array.Vector3[i].z = 0.0f;
		}
    }
	else if (_Vector4_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Vector4) > minsize)
		{
			for(unsigned int i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Vector4[i].x = m_Value.Array.Vector4[i].y = m_Value.Array.Vector4[i].z = m_Value.Array.Vector4[i].w = 0.0f;
		}
    }
	else if (_Matrix_Array == m_type)
    {
		const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
		if (m_Value.Array.Count * sizeof(Mat4) > minsize)
		{
			for(unsigned int i=0; i<m_Value.Array.Count; i++)
				m_Value.Array.Matrix[i] = Mat4::IDENTITY;
		}
    }
}

void Variant::Duplicate(const void * ptr, int pitch, int num)
{
	const unsigned int minsize = sizeof(m_Value) - (int)((char*)&m_Value.Array._empty - (char*)&m_Value.Array);
	unsigned int size = pitch * num;

	m_Value.Array.Count = num;
	m_Value.Array.ptr = size <= minsize ? &m_Value.Array._empty : (void*)(new char [size]);
	memcpy(m_Value.Array.ptr, ptr, size);
}

unsigned int Variant::size() const
{
	switch (m_type)
	{
	case _Void:
		return 0;
	case _Int:
		return sizeof(int);
	case _Bool:
		return sizeof(bool);
	case _Float:
		return sizeof(float);
	case _Vector2:
		return sizeof(Vector2);
	case _Vector3:
		return sizeof(Vector3);
	case _Vector4:
		return sizeof(Vector4);
	case _Texture:
		return 0;
	case _Matrix4:
		return sizeof(Mat4);
	case _Matrix3:
		return sizeof(Mat3);
	case _Float_Array:
		return sizeof(float) * count();
	case _Vector2_Array:
		return sizeof(Vector2) * count();
	case _Vector3_Array:
		return sizeof(Vector3) * count();
	case _Vector4_Array:
		return sizeof(Vector4) * count();
	case _Matrix_Array:
		return sizeof(Mat4) * count();
	default:
		break;
	}

	_ASSERT(0);
	return 0;
}

//float _ttt23232[11];
//Variant _ttt2312(_ttt23232, 11);