#include "renderer.h"

__ImplementRtti(VisualShock, ITexture, ISurface);

ITexture::ITexture()
{
}

ITexture::~ITexture()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__ImplementRtti(VisualShock, ITexture2D, ITexture);

ITexture2D::ITexture2D()
{
	m_FilterMode[0] = m_FilterMode[1] = FILTERMODE::_LINEAR;
	m_AddressMode[0] = m_AddressMode[1] = m_AddressMode[2] = ADDRESSMODE::_CLAMP_TO_EDGE;
	m_MipLodBias = 0.0f;
}

void ITexture2D::SetFilterMode(FILTERMODE::TYPE minFilter, FILTERMODE::TYPE magFilter)
{
	m_FilterMode[0] = minFilter;
	m_FilterMode[1] = magFilter;
}

void ITexture2D::SetAddressMode(ADDRESSMODE::TYPE U, ADDRESSMODE::TYPE V, ADDRESSMODE::TYPE W)
{
	m_AddressMode[0] = U;
	m_AddressMode[1] = V;
	m_AddressMode[2] = W;
}

void ITexture2D::SetMipLodBias(float mipLodBias)
{
	m_MipLodBias = mipLodBias;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__ImplementRtti(VisualShock, IWritableTexture, ITexture);
__ImplementRtti(VisualShock, IRenderTexture2D, ITexture);
