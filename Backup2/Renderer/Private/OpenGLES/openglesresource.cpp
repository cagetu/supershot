#include "renderer.h"


IShader* CreateShader()
{
	return GetRenderDriver()->CreateShader();
}

VertexBuffer* CreateVertexBuffer()
{
	return new GLESVertexBuffer();
}

IndexBuffer* CreateIndexBuffer()
{
	return  new GLESIndexBuffer();
}

ITexture2D* CreateFileTexture()
{
	GLESTexture2D* texture = new GLESTexture2D();
	return (ITexture2D*)texture;
}

IRenderTexture2D* CreateRenderTexture()
{
	GLESRenderTexture* texture = new GLESRenderTexture();
	return (IRenderTexture2D*)texture;
}

IRenderTarget* CreateRenderTarget()
{
	GLESRenderTarget* rt = new GLESRenderTarget();
	return rt;
}

IRenderTexture2D* CreateRenderTargetSurface()
{
	GLESRenderTargetSurface* surface = new GLESRenderTargetSurface();
	return surface;
}

IDepthStencilSurface* CreateDepthStencilSurface()
{
	GLESDepthStencilSurface* surface = new GLESDepthStencilSurface();
	return surface;
}
