#include "renderer.h"

// vertex buffer에서 값을 채우는 순서와 동일해야 함.
static const struct {
	const char* attrib;
	uint32 mask;
	int32 size, type, len;
	bool normalize;
}	s_FVFPreSet[] =
{
	"POSITIONT\0", FVF_XYZRHW, 4, GL_FLOAT, sizeof(float) * 4, GL_FALSE,
	"POSITION\0", FVF_XYZ, 3, GL_FLOAT, sizeof(float) * 3, GL_FALSE,
	"NORMAL\0", FVF_NORMAL, 3, GL_FLOAT, sizeof(float) * 3, GL_FALSE,
	"BONEINDICES\0", FVF_SKIN_INDEX, 4, GL_UNSIGNED_BYTE, sizeof(uint32), GL_FALSE,
	"BONEWEIGHTS\0", FVF_SKIN_WEIGHT, 4, GL_UNSIGNED_BYTE, sizeof(uint32), GL_TRUE,
	"DIFFUSE\0", FVF_DIFFUSE, 4, GL_UNSIGNED_BYTE, sizeof(uint32), GL_TRUE,
	"TEXCOORD0\0", FVF_TEX0, 2, GL_FLOAT, sizeof(float) * 2, GL_FALSE,
	"TEXCOORD1\0", FVF_TEX1, 2, GL_FLOAT, sizeof(float) * 2, GL_FALSE,
	"TANGENT\0", FVF_TANSPC, 4, GL_FLOAT, sizeof(float) * 4, GL_FALSE,
	"VERTEXA0\0", FVF_VERTEXAO, 4, GL_UNSIGNED_BYTE, sizeof(uint32), GL_TRUE,
	"SHCOEFF\0", FVF_SHCOEFF, 3, GL_FLOAT, sizeof(float) * 3, GL_FALSE,
	"SHCOEFF\0", FVF_SHCOEFF, 3, GL_FLOAT, sizeof(float) * 3, GL_FALSE,
	"SHCOEFF\0", FVF_SHCOEFF, 3, GL_FLOAT, sizeof(float) * 3, GL_FALSE,
};

void GLESVertexDescription::GetDesc(GLuint32 program, GLESVertexDescription* output)
{
	for (int32 i = 0; i<_countof(s_FVFPreSet); i++)
	{
		output->locations[i] = glGetAttribLocation(program, s_FVFPreSet[i].attrib);
		if (output->locations[i] != -1)
		{
			output->fvf |= s_FVFPreSet[i].mask;
			output->size += s_FVFPreSet[i].len;

#ifdef _DEBUG
			PlatformUtil::DebugOutFormat("[Shader Attributes] : %s %d\n", s_FVFPreSet[i].attrib, i);
#endif
		}
	}
}

void BindShaderVertexDesc(const GLESVertexDescription& vertexDesc, uint32 vertexFormat, uint32 vertexStride, unsigned char* vertexElements)
{
	uint32 offset = 0;

	int32 total = _countof(s_FVFPreSet);
	for (int32 i = 0; i<total; i++)
	{
		if (vertexFormat&s_FVFPreSet[i].mask)
		{
			if (vertexDesc.fvf&s_FVFPreSet[i].mask)
			{
				_ASSERT(vertexDesc.locations[i] != -1);
				glVertexAttribPoint32er(vertexDesc.locations[i], s_FVFPreSet[i].size, s_FVFPreSet[i].type, s_FVFPreSet[i].normalize, vertexStride, (const void*)(vertexElements + offset));
				glEnableVertexAttribArray(vertexDesc.locations[i]);
			}
#if 0
			else
			{
				util::DebugOut("Invalid attribtes : %s\n", s_FVFPreSet[i].attrib);
			}
#endif
			offset += s_FVFPreSet[i].len;
			if (offset == vertexDesc.size)
				break;
		}
	}

}

void UnbindShaderVertexDesc(const GLESVertexDescription& vertexDesc)
{
	uint32 offset = 0;

	int32 total = _countof(s_FVFPreSet);
	for (int32 i = 0; i<total; i++)
	{
		if (vertexDesc.fvf&s_FVFPreSet[i].mask)
		{
			_ASSERT(vertexDesc.locations[i] != -1);
			glDisableVertexAttribArray(vertexDesc.locations[i]);
			offset += s_FVFPreSet[i].len;
		}
		if (offset == vertexDesc.size)
			break;
	}
}