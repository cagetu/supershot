#include "renderer.h"

GLESRenderState::GLESRenderState()
{
}

void GLESRenderState::Reset()
{
	RenderState::Reset();

	glCullFace(GL_FRONT);
	//glEnable(GL_CULL_FACE);
	//glDepthMask(GL_TRUE);
	//glEnable(GL_DEPTH_TEST);
	//glDisable(GL_BLEND);
}

void GLESRenderState::Update(uint32 rs, int alphaRef)
{
	rs &= ~INSTANCEDISABLE;

	if (rs&CULL_FLIP)
	{
		if ((rs&CULL_MASK) != CULL_NONE)
			rs ^= CULL_CW;
		rs &= ~CULL_FLIP;
	}
	_ASSERT(!(rs&CULL_FLIP));

	if (rs&ALPHABLEND_SMOOTH)
	{
		if (rs&ALPHABLEND_BLENDPASS)
			rs |= ZFUNC_LESS | ZWRITEDISABLE | ALPHABLEND_TRANSLUCENT;
		else
			rs |= ALPHABLEND_MASK;
	}

	unsigned long rs_xor = m_RenderState ^ rs;

	if (rs_xor == 0)
		return;

	if (rs_xor&CULL_MASK)
	{
		switch (rs&CULL_MASK)
		{
		case CULL_CCW:
			glFrontFace(GL_CCW);
			glEnable(GL_CULL_FACE);
			break;
		case CULL_CW:
			glFrontFace(GL_CW);
			glEnable(GL_CULL_FACE);
			break;
		case CULL_NONE:
			glDisable(GL_CULL_FACE);
			break;
		}
	}

	if (rs_xor&ZDISABLE)
	{
		if (!(rs&ZDISABLE))
			glEnable(GL_DEPTH_TEST);
		else
			glDisable(GL_DEPTH_TEST);
	}

	if (rs_xor&ZWRITEDISABLE)
	{
		if (!(rs&ZWRITEDISABLE))
			glDepthMask(GL_TRUE);
		else
			glDepthMask(GL_FALSE);
	}

	if (rs_xor&ZFUNC_MASK)
	{
		switch (rs&ZFUNC_MASK)
		{
		case ZFUNC_LESSEQUAL:
			glDepthFunc(GL_LEQUAL);
			break;
		case ZFUNC_EQUAL:
			glDepthFunc(GL_EQUAL);
			break;
		case ZFUNC_GREATER:
			glDepthFunc(GL_GREATER);
			break;
		case ZFUNC_LESS:
			glDepthFunc(GL_LESS);
			break;
		case _ZFUNC_GREATEREQUAL :
			glDepthFunc(GL_GEQUAL);
			break;
		case ZFUNC_ALWAYS:
			glDepthFunc(GL_ALWAYS);
			break;
		}
	}

	if (rs_xor&COLORWRITEDISABLE)
	{
		if (!(rs&COLORWRITEDISABLE))
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		else
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	}

	//glDepthRangef(0.0f, 1.0f);

	if (rs_xor&ALPHABLEND_MASK)
	{
		switch (rs&ALPHABLEND_MASK)
		{
		case ALPHABLEND_COPY:
			glDisable(GL_BLEND);
			break;
		case ALPHABLEND_TRANSLUCENT:
			glEnable(GL_BLEND);
			if (rs_xor&ALPHABLEND_PREMULPLIED)
				glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			else
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			break;
		case ALPHABLEND_ADD:
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE);
			break;
		case ALPHABLEND_ALPHAEDADD:
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			break;
		case ALPHABLEND_MULTIPLY:
			glEnable(GL_BLEND);
			glBlendFunc(GL_ZERO, GL_SRC_COLOR);
			break;
		case ALPHABLEND_INVMULTIPLY:
			glEnable(GL_BLEND);
			glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
			break;
		}
	}

	if (rs_xor&ALPHATESTENABLE)
	{
		glDisable(GL_BLEND);
	}

	m_RenderState = rs;
}