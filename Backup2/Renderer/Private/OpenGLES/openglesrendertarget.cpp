#include "renderer.h"

__ImplementRtti(VisualShock, GLESRenderTarget, IRenderTarget);

GLESRenderTarget::GLESRenderTarget()
	: m_FrameBuffer(0)
	, m_DirtyFBO(false)
{
}

GLESRenderTarget::~GLESRenderTarget()
{
	if (m_FrameBuffer != 0)
	{
		glDeleteFramebuffers(1, &m_FrameBuffer);
		m_FrameBuffer = 0;
	}

	m_DepthBuffer.Null();

	for (auto it : m_Channels)
		it->DecRef();
	m_Channels.clear();
}

bool GLESRenderTarget::Create(int width, int height)
{
	glGenFramebuffers(1, &m_FrameBuffer);

	m_Width = width;
	m_Height = height;
	return true;
}

IRenderTexture2D* GLESRenderTarget::AddRenderTexture(int format, RTLoadAction loadAction, RTStoreAction storeAction)
{
	IRenderTexture2D* rt = CreateRenderTexture();
	if (rt->Create(m_Width, m_Height, format) == false)
	{
		delete rt;
		return NULL;
	}
	rt->IncRef();

	m_Channels.push_back(rt);

	m_DirtyFBO = true;
	return rt;
}

IRenderTexture2D* GLESRenderTarget::GetRenderTexture(int32 channel) const
{
	return m_Channels.empty() == false && channel < (int32)m_Channels.size() ? m_Channels[channel] : NULL;
}

IRenderTexture2D* GLESRenderTarget::GetRenderTexture(const TCHAR* id) const
{
	for (auto const it : m_Channels)
	{
		if (!unicode::strcmp(it->GetID().c_str(), id))
			return it;
	}
	return NULL;
}

void GLESRenderTarget::ClearRenderTextures()
{
	for (auto it : m_Channels)
		it->DecRef();
	m_Channels.clear();
}

void GLESRenderTarget::Begin()
{
	// FrameBuffer를 체크하는 부분이 병목이 될 수 있기 때문에 최대한 Renderer에 맡기도록 변경하자!!!
	//GetRenderDriver()->SetRenderTarget(this);
	SetSurfaces();
}

void GLESRenderTarget::End()
{
	//Discard();

	//glFlush();
}

void GLESRenderTarget::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBuffer);
}

void GLESRenderTarget::Discard()
{
	if (glDiscardFramebufferEXT != NULL)
	{
		if (m_Multisample > 0 || m_DepthBuffer != NULL)
		{
			if (m_DepthBuffer != NULL && m_DepthBuffer->GetMultisamples() > 0)
			{
				const GLenum attachments[] = { GL_DEPTH_ATTACHMENT, GL_COLOR_ATTACHMENT0 };
				glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 2, attachments);
			}
			else
			{
				const GLenum attachments[] = { GL_COLOR_ATTACHMENT0 };
				glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, attachments);
			}
		}
		else
		{
			// Color Buffer를 Discard 하지 않는 이유가 뭘까??? [2013-04-23 : cagetu]
			if (m_DepthBuffer != NULL)
			{
				const GLenum attachments[] = { GL_DEPTH_ATTACHMENT, GL_STENCIL_ATTACHMENT };
				glDiscardFramebufferEXT(GL_FRAMEBUFFER, 2, attachments);
			}
		}
	}
}

void GLESRenderTarget::CreateDepthBuffer(int format, int multisamples)
{
	if (m_DepthBuffer != NULL)
		m_DepthBuffer.Null();

	GLESDepthStencilSurface* depthBuffer = new GLESDepthStencilSurface();
	if (depthBuffer->Create(m_Width, m_Height, format, multisamples) == false)
	{
		delete depthBuffer;
		return;
	}

	m_DepthBuffer = depthBuffer;
	m_DepthBuffer->IncRef();

	m_DirtyFBO = true;
	//@>
}

void GLESRenderTarget::SetDepthBuffer(const Ptr<IDepthStencilSurface>& surface)
{
	if (m_DepthBuffer == surface.Object())
		return;

	m_DepthBuffer.Null();

	_ASSERT(surface->IsExactOf(&GLESDepthStencilSurface::RTTI) == true);
	m_DepthBuffer = surface.Cast<GLESDepthStencilSurface>();

	m_DirtyFBO = true;
}

void GLESRenderTarget::SetSurfaces()
{
	if (m_DirtyFBO == false)
		return;

	// 프레임 버퍼 바인딩
	int ch = 0;
	std::vector <IRenderTexture2D*>::iterator it;
	for (it = m_Channels.begin(); it != m_Channels.end(); it++, ch++)
		(*it)->SetSurface(ch);

	// 깊이 버퍼 바인딩
	if (m_DepthBuffer != NULL)
		m_DepthBuffer->SetSurface();

	m_DirtyFBO = false;
}
