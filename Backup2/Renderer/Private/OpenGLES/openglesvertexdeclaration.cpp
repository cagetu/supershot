#include "renderer.h"

__ImplementRtti(VisualShock, GLESVertexDeclaration, IVertexDeclaration);

GLESVertexDeclaration::GLESVertexDeclaration()
{
	m_VertexDesc = nullptr;
}

GLESVertexDeclaration::~GLESVertexDeclaration()
{
}

bool GLESVertexDeclaration::Create(VertexDescription* inVertexDesc)
{
	m_VertexDesc = inVertexDesc;
}