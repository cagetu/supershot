#include "renderer.h"

OpenGLESDriver::OpenGLESDriver()
{
	m_FrameBuffer = 0;
	m_BackBuffer = 0;

	m_RenderTarget = NULL;
	m_Shader = NULL;
	m_Viewport = NULL;

	m_RenderState = NULL;
	PlatformUtil::Memset(m_RenderStateFilter, 0, sizeof(m_RenderStateFilter));

	m_AlphaRef = 0;
}

OpenGLESDriver::~OpenGLESDriver()
{
	if (m_RenderState != NULL)
	{
		delete m_RenderState;
		m_RenderState = NULL;
	}
}

void OpenGLESDriver::CheckDriverCaps()
{
	PlatformDriverCaps::Setup();

	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_BackBuffer);
}

void OpenGLESDriver::BeginScene(IRenderTarget* rt, uint32 rs_or, uint32 rs_and, int32 clearFlags, COLOR clearColor, float clearDepth, ushort clearStencil)
{
	// ���� Ÿ��
	SetRenderTarget(rt);

	// ���� ������Ʈ
	m_RenderStateFilter[0] = rs_or;
	m_RenderStateFilter[1] = ~rs_and;
	SetRenderState(0);

	float width = 0.0f;
	float height = 0.0f;

	if (rt)
	{
		rt->Begin();

		width = (float)rt->GetWidth();
		height = (float)rt->GetHeight();
	}
	else
	{
		width = (float)m_Width;
		height = (float)m_Height;
	}
	ShaderParameter::SetCommonValue(ShaderParameter::SCREENSIZE, Vector4(width, height, 1.0f / width, 1.0f / height));

	if (m_Viewport != NULL)
	{
		const IViewport::Extent& viewport = m_Viewport->GetRelativeExtent();

		float left = viewport.x * width;
		float top = viewport.y * height;
		float right = viewport.width * width;
		float bottom = viewport.height * height;

		glViewport((int)left, (int)top, (int)right, (int)bottom);
	}
	else
	{
		glViewport(0, 0, (int)width, (int)height);
	}

	if (clearFlags > 0)
	{
		Clear(clearColor, clearDepth, clearStencil, clearFlags);
	}
}

void OpenGLESDriver::EndScene()
{
	if (m_Shader)
	{
		m_Shader->ClearVertexAttributes();
		m_Shader->ClearValues();

		m_Shader = NULL;
	}
	glUseProgram(0);

	if (m_RenderTarget)
	{
		m_RenderTarget->End();
	}
	//else
	//{
	//	glBindFramebuffer(GL_FRAMEBUFFER, m_BackBuffer);
	//}
	m_Viewport = NULL;
}

void OpenGLESDriver::Clear(COLOR clearColor, float clearDepth, ushort clearStencil, uint32 clearFlags)
{
	if (clearFlags != 0)
	{
		uint32 clearMask = 0;
		if (clearFlags & CLEAR_COLOR)
		{
			RGBA color(clearColor);
			glClearColor(color.r, color.g, color.b, color.a);
			clearMask |= GL_COLOR_BUFFER_BIT;
		}
		if (clearFlags & CLEAR_DEPTH)
		{
			glClearDepthf(clearDepth);
			clearMask |= GL_DEPTH_BUFFER_BIT;
		}
		if (clearFlags & CLEAR_STENCIL)
		{
			glClearStencil(clearStencil);
			clearMask |= GL_STENCIL_BUFFER_BIT;
		}

		glClear(clearMask);
	}
}

GLenum GetPrimitiveType(PRIMITIVETYPE PrimitiveType)
{
	switch (PrimitiveType)
	{
	case PT_TRIANGLELIST:	return GL_TRIANGLES;
	case PT_TRIANGLESTRIP:	return GL_TRIANGLE_STRIP;
	case PT_TRIANGLEFAN:	return GL_TRIANGLE_FAN;
	case PT_LINELIST:		return GL_LINES;
	case PT_LINESTRIP:		return GL_LINE_STRIP;
	case PT_POINTLIST:		return GL_POINTS;
	default:				_ASSERT(0);
	};

	return 0;
}

void OpenGLESDriver::DrawPrimitive(PRIMITIVETYPE PrimitiveType, uint StartVertex, uint VertexCount, uint32 StartInstance, uint32 InstanceCount, uint32 NumControlPoints)
{
	glDrawArrays(GetPrimitiveType(PrimitiveType), StartVertex, VertexCount);
	TestGLError(TEXT("[COpenGLES::DrawPrimitive] glDrawArrays"));
}

void OpenGLESDriver::DrawIndexedPrimitive(PRIMITIVETYPE PrimitiveType, uint BaseVertex, uint StartIndex, uint IndexCount, IndexBuffer* IdxBuffer, uint32 StartInstance, uint32 InstanceCount, uint32 NumControlPoints)
{
	unsigned char* idx = IdxBuffer->GetBytes();

	glDrawElements(GetPrimitiveType(PrimitiveType), IndexCount, GL_UNSIGNED_SHORT, (const void*)(idx + StartIndex*sizeof(unsigned short)));
	TestGLError(TEXT("[COpenGLES::DrawIndexedPrimitive] glDrawElements"));
}

void OpenGLESDriver::DrawInstancePrimitive(PRIMITIVETYPE PrimitiveType, uint32 StartVertex, uint32 VertexCount, uint32 InstanceCount, uint32 StartInstance, uint32 NumControlPoints)
{
	_ASSERT(0);
}

void OpenGLESDriver::DrawIndexedInstancePrimitive(PRIMITIVETYPE PrimitiveType, uint32 StartIndex, uint32 BaseVertex, uint32 IndexCount, uint32 InstanceCount, uint32 StartInstance, uint32 NumControlPoints)
{
	_ASSERT(0);
}

void OpenGLESDriver::SetVertexBuffer(VertexBuffer* vertexbuffer)
{
	vertexbuffer->SetStreamSource();
}

void OpenGLESDriver::SetIndexBuffer(IndexBuffer* indexbuffer)
{
	if (indexbuffer != NULL)
	{
		indexbuffer->SetIndices();
	}
}

void OpenGLESDriver::SetRenderTarget(IRenderTarget* renderTarget)
{
	if (m_RenderTarget != renderTarget)
	{
		if (renderTarget != NULL)
		{
			GLESRenderTarget* pRenderTarget = (GLESRenderTarget*)renderTarget;

			if (pRenderTarget->GetFrameBuffer() != m_FrameBuffer)
			{
				if (m_RenderTarget != NULL)
					m_RenderTarget->Discard();

				glBindFramebuffer(GL_FRAMEBUFFER, pRenderTarget->GetFrameBuffer());

				m_FrameBuffer = pRenderTarget->GetFrameBuffer();
			}
			m_RenderTarget = pRenderTarget;
		}
		else
		{
			if (m_RenderTarget != NULL)
				m_RenderTarget->Discard();

			glBindFramebuffer(GL_FRAMEBUFFER, m_BackBuffer);

			m_FrameBuffer = 0;
			m_RenderTarget = NULL;
		}
	}
}

void OpenGLESDriver::DiscardRenderTarget(bool depth, bool stencil, uint32 colors)
{
	if (COpenGLESCaps::IsSupportDiscardFrameBuffer())
	{
		uint32 num = 0;
		GLenum attachments[10];
		if (depth)
		{
			attachments[num++] = GL_DEPTH_ATTACHMENT;
		}
		if (stencil)
		{
			attachments[num++] = GL_STENCIL_ATTACHMENT;
		}
		for (uint32 i = 0; i < colors; i++)
		{
			attachments[num++] = GL_COLOR_ATTACHMENT0 + i;
		}
		if (num > 0)
		{
			glDiscardFramebufferEXT(GL_FRAMEBUFFER, num, attachments);
		}
	}
}

void OpenGLESDriver::SetShader(IShader* shader, const VertexBuffer* vb, const CMaterial* material)
{
	//if (shader != m_Shader)
	{
		GLESShader* pShader = DynamicCast<GLESShader>(shader);
		if (m_Shader)
		{
			m_Shader->ClearVertexAttributes();
			m_Shader->ClearValues();
		}

		glUseProgram(pShader->GetProgram());
		TestGLError(TEXT("glUseProgram"));

		pShader->UpdateVertexAttributes(vb);
		pShader->UpdateValues(material);

		m_Shader = pShader;
	}
}

void OpenGLESDriver::SetRenderState(uint32 rs)
{
	m_RenderState->Update((rs&m_RenderStateFilter[1]) | m_RenderStateFilter[0]/*, m_AlphaRef*/);
}

IViewport* OpenGLESDriver::CreateViewport()
{
	return new GLESViewport();
}

void OpenGLESDriver::DestroyViewport(IViewport* viewport)
{
	delete viewport;
}

void OpenGLESDriver::SetViewport(IViewport* viewport)
{
	m_Viewport = viewport;
}

IShader* OpenGLESDriver::CreateShader()
{
	return new GLESShader();
}

void OpenGLESDriver::DestroyShader(IShader* shader)
{
	delete shader;
}

////


bool TestGLError(const TCHAR* function)
{
	GLenum lastError = glGetError();
	if (lastError != GL_NO_ERROR)
	{
		String text;

		switch (lastError)
		{
		case GL_INVALID_ENUM:		text = TEXT("GL_INVALID_ENUM");			break;
		case GL_INVALID_VALUE:		text = TEXT("GL_INVALID_VALUE");		break;
		case GL_INVALID_OPERATION:	text = TEXT("GL_INVALID_OPERATION");	break;
		case GL_OUT_OF_MEMORY:		text = TEXT("GL_OUT_OF_MEMORY");		break;
		};

		TCHAR temp[256];
		unicode::format(temp, TEXT("%s failed (%s).\n"), function, text.c_str());
		PlatformUtil::Dialog(temp, TEXT("[OpenGL Driver Error]"), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	return true;
}

