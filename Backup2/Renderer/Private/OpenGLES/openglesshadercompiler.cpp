#include "renderer.h"
#include <glsl_optimizer.h>

std::map <String, GLESCompiledShaderCode*> GLESShaderCompiler::m_ShaderCache;
glslopt_ctx* GLESShaderCompiler::m_Target = NULL;

bool GLESShaderCompiler::Initialize()
{
#if _OPENGL_ES3
	m_Target = glslopt_initialize(kGlslTargetOpenGLES30);
	if (m_Target == NULL)
		return false;
#endif
#if _OPENGL_ES2
	m_Target = glslopt_initialize(kGlslTargetOpenGLES20);
	if (m_Target == NULL)
		return false;
#endif

	// shadercache 내부에 있는 파일을 모두 읽는다.

	return IShaderCompiler::Initialize();
}

void GLESShaderCompiler::Shutdown()
{
	if (m_Target != NULL)
		glslopt_cleanup(m_Target);
	m_Target = NULL;

	for (auto it : m_ShaderCache)
		delete it.second;
	m_ShaderCache.clear();

	IShaderCompiler::Shutdown();
}

ICompiledShaderCode* GLESShaderCompiler::Find(const TCHAR* id)
{
	auto it = m_ShaderCache.find(id);
	if (it != m_ShaderCache.end())
		return it->second;

	return NULL;
}

ICompiledShaderCode* GLESShaderCompiler::Compile(const TCHAR* id, const std::string& source, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option)
{
	std::string macro;
	{
		static std::string macrolist[128];
		ShaderFeature::GetMacro(shaderMask, macrolist, &macro);
	}
	std::string shaderCode = macro + source;

	/*	체크는 위해서 한다고 가정
	if (IShaderCompiler::Find(code, id) == true)
	{
		output = code;
		return true;
	}
	*/

	std::string code;
	bool result = _Compile(code, shaderCode.c_str(), type, option);
	if (result == false)
	{
		return NULL;
	}

	/*
	char temp[128];
	string::conv_s(temp, 128, id);
	sprintf(temp, "%s", temp);

	FILE* fp = fopen(temp, "wt");
	fwrite(code.c_str(), 1, code.size(), fp);
	fclose(fp);
	*/

	//PlatformUtil::Dialog(code.c_str(), "test");
	GLESCompiledShaderCode* compiled = new GLESCompiledShaderCode();
	compiled->code = code;

	m_ShaderCache.insert(std::make_pair(id, compiled));
	return compiled;
}

bool GLESShaderCompiler::_Compile(std::string& output, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option)
{
	glslopt_shader_type shaderType = (glslopt_shader_type)type;
	glslopt_shader* shader = glslopt_optimize(m_Target, shaderType, shaderCode.c_str(), option);
	if (shader == NULL)
		return false;

	bool optimizeOk = glslopt_get_status(shader);
	if (optimizeOk == false)
	{
		char temp[1024];
		sprintf_s(temp, 1024, "\n  optimize error : %s\n", glslopt_get_log(shader));

		PlatformUtil::DebugOut(temp);
		PlatformUtil::Dialog(temp, "[ERROR]");

		glslopt_shader_delete(shader);
		return false;
	}

	output = glslopt_get_output(shader);

	// stat
	int ALU, TEX, FLOW;
	glslopt_shader_get_stats(shader, &ALU, &TEX, &FLOW);

	glslopt_shader_delete(shader);
	return true;
}


ICompiledShaderCode* CompileShader(const TCHAR* id, const std::string& shaderCode, ShaderType::TYPE type, ShaderFeature::MASK shaderMask, int option)
{
	return GLESShaderCompiler::Compile(id, shaderCode, type, shaderMask, option);
}