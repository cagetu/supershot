#ifndef _MOTION_CONTROLLER_
#define _MOTION_CONTROLLER_

//#define _ENABLE_IK
class CModel;
class CMotionSynth;
class CEventListener;
class CEventHandler;
class CAnimationController;

class CMotionController
{
public :
	static CMotionController* Create(CMiniXML::const_iterator node, CModel* model, CMotionSynth* synth);
	static void Release(CMotionController* motionctl);

	void Apply(CBoneController * ctl, float t, float blendweight=1.0f);

	void PreUpdate(float dt, CMotionSynth* synth, CEventListener* listener);
	void Update(float dt, CMotionSynth* synth);
	void UpdateEvent(float t1, float t2, CEventListener* listener);

	float GetLength() const;
	bool IsLoopAnimation() const { return m_Flags[_LOOP] ? true : false; }
	bool IsFinished() const;

	Vector3 GetDeltaVec(float prevt, float curt);
	const Vector3& GetMoveDelta() const { return m_MoveDelta; }

	Mat4 GetAnimTM(const TCHAR* bonename, float t);

	CMotionController* Duplicate(CModel* model, CMotionSynth* synth);

public:

public:
	bool SetEnable(const TCHAR* masks);

	void SetValue(CMotionController* motion, bool force, bool notransition = false);
	void SetValue(const Vector3& val);
	void SetValue(const TCHAR* val);
	void SetValue(float val);

public:
	void Impulse(const Vector3 &pivot, const Vector3 &force);

private :
	CMotionController();
	~CMotionController();

	void SetFlags(const CBitFlags <64>& flags) { m_Flags = flags; }
	bool Init(CMiniXML::const_iterator node, CModel* model, CMotionSynth* synth);

	float GetAnimeTime(float t);

	enum
	{
		_IK,
		_ANIMATION,
		_RANGE,
		_LOOP,
		_MOTIONBLENDING,
		_BONESEPARATE,
		_COPY_BASE,

		_DELTAMOTION,
		_DELTA_POSE,
		_DELTA_1D,
		_DELTA_2D,
		_FIXEDPOS,
		_ZEROPIVOT,
		_ROTATE,
		_ANIMSPEED,
		_DISABLE,
		_CONDITION,
		_BASEANIM,
		_BASEBLEND,
		_ABSOLUTE,
		_INCLUDEROOTBONE,
		_ANI,
		_STOPMOTION,
		_DELTAKEYANI,
		_ANIMBLEND,
		_PHYSXBLEND=_ANIMBLEND,
		_MOTIONEVENT,
		_PHYSICS,
		_UPDATEKINEMATIC,
		_CONNECTLOOP,
		_30FPS,
		_UPDOWN_BLEND,
		_BONESCALE,

		_HAS_EVENT,

		_INSTANCE,
	} ;
	CBitFlags <64> m_Flags;
	String	m_Condition;

private:
	Vector3 m_MoveDelta;

	float m_BlendWeight;
	float m_Time;

	BONEHANDLE m_Rootbone;
	CBoneController* m_BoneCtrlr;
	std::vector <BONEHANDLE> m_BoneSet;

private:
#ifdef _ENABLE_IK
	CIKController*	m_IKController;
	String m_IKTarget;
#endif

private :
	Vector3 ApplyAnimation(CBoneController * bonectl, float t, float blendweight);
	Vector3 GetPivot(int boneidx, float t);
	CAnimationController* GetAnimController() {  return m_AnimationController; }

	CAnimationController* m_AnimationController;
	CAnimationController* m_DeltaAnim;
	CBoneController* m_DeltaPivot[3];
	float m_DeltaTable[9];
	float m_DeltaBaseTime;
	int m_FixedBone, m_FixedRoot;
	String m_FixedBoneID;
	Vector3 m_FixedPos;
	Vector3 m_Param;
	Mat4 m_LocalTM;

	float m_AnimSpeed;
	float m_AnimRange[2];

	CMotionController* m_AnimCtl;
	CMotionController* m_AnimUpDn[2];
	CMotionSynth* m_MotionSynth;
	float m_LoopTransitionRange;

	int m_BoneScaleID;
	float m_BoneScale;

	CMotionController* m_BlendAnimCtl;
	float m_BlendTime[3];
	std::vector <int> m_ParentList; // for _ROTATE

	CEventHandler* m_Event;

	Vector3 m_FixedDelta;

private :
	CBoneController* m_BoneController;
	CBoneController* m_DefaultController;
	CBoneController* m_TempController[2];

	CMiniXML* m_XML;
	std::vector <int> m_Filter;
	std::vector <int> m_Anchor;

	int m_RefCount;
	CMotionController* m_RefCtrl;

protected :
	friend class CMotionSynth;
	void SetActivate(bool activation, CBoneController* bonectl);
} ;

#endif