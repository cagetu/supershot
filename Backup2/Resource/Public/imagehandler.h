#pragma once

namespace ERGBFormat
{
	/**
	* Enumerates the types of RGB formats this class can handle
	*/
	enum Type
	{
		Invalid = -1,
		RGBA = 0,
		BGRA = 1,
		Gray = 2,
	};
};

class IImageHandler
{
public:
	IImageHandler();
	virtual ~IImageHandler();

	virtual bool Load(const TCHAR* filename) = 0;
};

class PNGHandler : public IImageHandler
{
public:
	PNGHandler();
	virtual ~PNGHandler();

	virtual bool Load(const TCHAR* filename);

private:
	uint32 m_Width;
	uint32 m_Height;
	uint8 m_BitDepth;
	uint8 m_ColorType;
	uint8 m_Channel;
	ERGBFormat::Type m_Format;

	bool LoadHeader();
};

class TGAHandler : public IImageHandler
{
public:
	TGAHandler();
	virtual ~TGAHandler();

	virtual bool Load(const TCHAR* filename);
};