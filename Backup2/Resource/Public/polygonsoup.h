#ifndef _POLYGON_SOUP
#define _POLYGON_SOUP

class	CPolygonSoup
{
public :
	CPolygonSoup();

	void SetPosition(Vector3* v, int num);
	void SetPosition(Vector3* v, int (* skinV)[4], float (*skinW)[4], int num);
	void SetNormal(Vector3* n, int num);
	void SetColor(unsigned char (*vertcolor)[3], int num);
	void SetTexcoord(int ch, float (*uv)[2], int num);

	void ResetVertex();
	int FindVertex(unsigned short vtxidx, unsigned short normal=0, unsigned short vtxcoloridx=0, unsigned short uv1idx=0, unsigned short uv2idx=0);
	int CountVertex() { return (int)vertex.size(); }

	void GetVertexNormal(Vector3* v);
	void GetVertexPosition(Vector3* v);
	void GetColor(uint32* c);
	void GetTexcoord(int ch, float (*uv)[2]);
	void GetSkin(int (*skinV)[4], float (*skinW)[4]);

	void Insert(const Vector3 *v, int vtxnum, const unsigned short* face, int facen);

	std::vector <Vector3> vtx;
	std::vector <int> tri;

	enum
	{
		_POSITIONONLY		= 1<<0,
		_MERGED				= 1<<1,

		_POSITION			= 1<<2,
		_SKIN				= 1<<3,
		_NORMAL				= 1<<4,
		_COLOR				= 1<<5,
		_TEXCOORD1			= 1<<6,
		_TEXCOORD2			= 1<<7,
	} ;

	uint32 GetFlags() const { return m_Flags; }

protected :
	uint32 m_Flags;

	struct SKIN
	{
		int SkinV[4];
		float SkinW[4];
	} ;
	std::vector <SKIN> skinvtx;
	std::vector <uint32> color;

	struct TECOORD
	{
		float uv[2];
	} ;
	std::vector <TECOORD> texcoord[2];
	std::vector <Vector3> normal;

private :
	struct	VERTEX
	{
		unsigned short vtxidx, vtxcoloridx, uv1idx, uv2idx, normal;
	} ;

	std::vector <VERTEX> vertex;
	std::multimap <int, int> vertextable;
} ;

#endif