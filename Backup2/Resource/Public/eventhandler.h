﻿#ifndef _EVENT_HANDLER_
#define _EVENT_HANDLER_

class CEventListener
{
public :
	enum
	{
		_SOUND		= 1<<0,
		_EVENT		= 1<<1,
		_PARTICLE	= 1<<2,
	} ;

	struct PARAM
	{
		unsigned long flags;
		String	eventparam;
//		int bonebind;
	} ;

	virtual void OnExec(const PARAM& /*param*/) {}
} ;

class CEventHandler : public CEventListener
{
public :
	CEventHandler();

	bool Open(CMiniXML::const_iterator node);
	void Update(float pretime, float curtime, CEventListener* listener) const;

protected :
	struct EVENT
	{
		float time;
		PARAM param;
		bool operator < (const EVENT& k) const { return time < k.time ? true : false; }
	} ;

private :
	void InsertEvent(CMiniXML::const_iterator node);

	std::vector <EVENT> m_List;
};

#endif