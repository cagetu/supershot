#ifndef _ik_controller_
#define _iK_controller_

#include "unicode.h"
#include "vec.h"
#include <vector>

class	CBoneController;

class	CIKController
{
public :
	CIKController();
	~CIKController();

	void SetBoneController(CBoneController * ctl) { m_Controller = ctl; }
	bool SetBone(const wchar* rootbone, const wchar* endeffect, const Vec3 & vec);

	void Apply(const Vec3 & target, float blendweight=1.0f, CBoneController* ctl=NULL);
	
private :
	bool InsertBoneRecur(int boneHandle, int chainroot);

	CBoneController * m_Controller;

	struct _JOINT {
		Vec3 pivot;
		int boneIndex;

		// _JOINT CONSTRAIT ����
	} ;
	std::vector <_JOINT> m_BoneList;

	int		m_EndEffecter;
	Vec3	m_EndEffecterLocalPos;
} ;

#endif