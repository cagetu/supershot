#ifndef _ANIMATION_CONTROLLER
#define _ANIMATION_CONTROLLER

class	CBoneController;

class	CAnimationController
{
public :
	static CAnimationController* LoadAnimationController(const TCHAR * fname);
	static void Release(CAnimationController* animctl);

	static void Shutdown();

public :
	void Apply(CBoneController * ctl, float t, float blendweight=1.0f);

	Vector3 GetPivot(int boneidx, float t) { return GetTM(boneidx, t).Position(); }
	Mat4 GetTM(int boneidx, float t);
	float GetLength();

	BONEHANDLE FindBone(const TCHAR* bonename);

private :
	CAnimationController(const TCHAR* fname);
	~CAnimationController();

	bool Load(const TCHAR* fname);
	bool Load(CMiniXML::const_iterator node);

private :
	struct _BONE
	{
		String name;

		BONEHANDLE parent;

		Mat4 worldTM;
		Mat4 invWorldTM;
		Mat4 localTM;

		bool operator == (const TCHAR* bonename) { return !unicode::strcmp(bonename, name.c_str()); }
	} ;

	struct _KEYFRAME
	{
		float*	posTime;
		Vector3* posKey;
		int		posNum;

		float*	rotTime;
		Quat4*	rotKey;
		int		rotNum;

		float len;

		_KEYFRAME() : posNum(0), rotNum(0), len(0.0f) {}
	} ;

	_KEYFRAME* m_pKey;
	float m_AnimLength;

	std::vector <_BONE> m_BoneList;
	std::map <CBoneController*, int*> m_BoneTable;

	int Find(const TCHAR* bonename);
	int FindPos(float * timearay, int count, float t);

	int* CreateBoneTable(CBoneController * ctl);

private :
	void Loading();

	enum
	{
		_LOADING_COMPLETE	= 1<<0,
		_LOADING_FAIL		= 1<<1,
	} ;
	unsigned long m_Flags;
	
	void IncRefCount() { m_RefCount++; }
	void DecRefCount() { m_RefCount--; }

	volatile long m_RefCount;
	String m_Filename;
} ;

#endif