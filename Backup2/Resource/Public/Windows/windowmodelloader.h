#pragma once

struct WindowModelLoader : public ModelLoader
{
	static bool Initialize();
	static void Shutdown();

	static ModelData* LoadFBX(const TCHAR* filename);
};

typedef WindowModelLoader CModelLoader;