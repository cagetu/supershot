#pragma once

class CGeometry : public CMeshData
{
public:
	CGeometry(const TCHAR* objlist = NULL);
	virtual ~CGeometry();

	bool Load(const TCHAR * fname, uint32 options = _USE_VERTEXCOLOR | _USE_SKIN | _CALC_TANGENT);

protected:
	struct _DESC
	{
		String name;
		String bonename;

		int vtxnum;
		const Vector3 * vtx;
		const Vector3 * norm;
		const uint32 *diffuse;
		const float(*uv1)[2];
		const float(*uv2)[2];
		const Vector4 * tan;
		const float* shcoeff;
		const uint32* vtxao;
		const unsigned char(*skinindex)[4];
		const unsigned char(*skinweight)[4];

		int facen;
		const unsigned short * face;

		int bonecount;
		const int* bonetable;

		String diffuseMap;
		String specularMap;
		String normalMap;
	};

	void LoadGeomRaw(CFileIO *fp, bool skin);
	void LoadGeomBin(CFileIO* fp);
	void LoadBone(CFileIO* fp, int tag);

	virtual void Insert(CPolygonSoup& polygonsoup, const TCHAR* name, int matcount, unsigned short(*face)[1 + 3 + 3 + 3 + 3 + 3], int numface);
	virtual void Insert(const _DESC& desc);

	void Split(const _DESC& desc, int MAXBONE);

	void MergeObject();
	bool Merge(_DESC& dest, const _DESC& child, uint32 type);

protected:
	int m_SplitBoneCount;

	String m_ObjList;
	std::multimap <String, _DESC> m_MergeObjects;

	String m_MatHint;

	float(*m_ExtraUV)[2];
	int m_ExtraUVCount;
	int m_PackSize[2];

	struct _EXTRACHANNEL
	{
		String name;
		int facenum;
		unsigned short* face;
	};
	std::vector <_EXTRACHANNEL> m_Extra;

	struct _EXTRA_SURFACEDEPTH
	{
		String name;
		int vtxnum;
		float* shcoeff;
	};
	std::vector <_EXTRA_SURFACEDEPTH> m_ExtraDepth;
	int m_ExtraDepthIdx;

	struct _EXTRA_AO
	{
		String name;
		int vtxnum;
		uint32* ao;
	};
	std::vector <_EXTRA_AO> m_AOList;
	//	int m_ExtraDepthIdx;
};