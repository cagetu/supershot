#include "resourcecore.h"

CAnimationController* CAnimationController::LoadAnimationController(const TCHAR * fname)
{
	CAnimationController* anim = new CAnimationController(fname);
	return anim;
}

void CAnimationController::Release(CAnimationController* animctl)
{
	if (animctl != NULL)
		animctl->DecRefCount();
}

void CAnimationController::Shutdown()
{
	//
}

CAnimationController::CAnimationController(const TCHAR* fname)
{
	m_Flags = 0;
	m_pKey = NULL;
	m_AnimLength = 0.0f;
	m_RefCount = 0;
	m_Filename = fname;
}

CAnimationController::~CAnimationController()
{
	std::map <CBoneController*, int*>::iterator it;
	for(it=m_BoneTable.begin(); it!=m_BoneTable.end(); it++)
		delete [] (*it).second;

	for(unsigned int i=0; i<m_BoneList.size(); i++)
	{
		delete [] m_pKey[i].posTime;
		delete [] m_pKey[i].posKey;
		delete [] m_pKey[i].rotTime;
		delete [] m_pKey[i].rotKey;
	}

	delete [] m_pKey;
}

int CAnimationController::FindBone(const TCHAR* bonename)
{
	if ((m_Flags&(_LOADING_COMPLETE|_LOADING_FAIL)) == 0)
		Loading();

	return Find(bonename);
}

int CAnimationController::Find(const TCHAR* bonename)
{
	std::vector <_BONE>::iterator it = std::find(m_BoneList.begin(), m_BoneList.end(), bonename);
	return it != m_BoneList.end() ? it - m_BoneList.begin() : -1;
}

int CAnimationController::FindPos(float * timearray, int num, float t)
{
	int i1 = 0, i2 = num - 1;
	while (i1 + 1 < i2)
	{
		int c = (i1 + i2) >> 1;

		if (timearray[c] < t)
			i1 = c;
		else
			i2 = c;
	}
	_ASSERT(i1 < num - 1);
	return i1;
}

void CAnimationController::Loading()
{
	if ((m_Flags&(_LOADING_COMPLETE|_LOADING_FAIL)) == 0)
		m_Flags |= Load(m_Filename) ? _LOADING_COMPLETE : _LOADING_FAIL;
}

bool CAnimationController::Load(const TCHAR* fname)
{
	CStdIO* fp = CStdIO::Open(fname);
	if (fp == NULL)
		return false;

	float framerate = 30.0f;

	int tag = fp->GetChar();

	CGeometryReader f(fp);

	std::map<int, _KEYFRAME> key;

	while(tag != -1)
	{
		if (tag == 0x04)
		{
			String name = f.ReadString();
			String parent = f.ReadString();
			Mat4 worldTM = f.ReadMatrix();
			Mat4 localTM = f.ReadMatrix();

			_BONE b;

			if (parent.empty())
				b.parent = -1;
			else
			{
				b.parent = Find(parent);
			}

			b.name = name;
			b.worldTM = worldTM;
			b.invWorldTM = worldTM.Inverse();
			b.localTM = localTM;
			m_BoneList.push_back(b);
		}
		else if (tag == 0x05)
		{
			framerate = f.ReadChar();
		}
		else if (tag == 0x08)
		{
			String name = f.ReadString();
			int boneindex = Find(name);
			int i;

			_KEYFRAME &k = key[boneindex];
#if 1
			if (k.posNum != 0)
			{
				delete [] k.posTime;
				delete [] k.posKey;
			}
			if (k.rotNum != 0)
			{
				delete [] k.rotTime;
				delete [] k.rotKey;
			}
#endif
			int rotnum = f.ReadUShort();

			k.rotTime = new float [rotnum];
			k.rotKey = new Quat4 [rotnum];
			k.rotNum = rotnum;

			for(i=0; i<rotnum; i++)
			{
				k.rotTime[i] = f.ReadFloat() / framerate;
				k.rotKey[i] = f.ReadQuat();
			}

			k.len = k.rotTime[rotnum-1];

			int posnum = f.ReadUShort();

			k.posTime = new float [posnum];
			k.posKey = new Vector3 [posnum];
			k.posNum = posnum;

			for(i=0; i<posnum; i++)
			{
				k.posTime[i] = f.ReadFloat() / framerate;
				k.posKey[i] = f.ReadVector();
			}

			m_AnimLength = k.posTime[posnum - 1];
		}
		else
		{
			return false;
		}

		tag = fp->GetChar();
	}

	m_pKey = new _KEYFRAME [m_BoneList.size()];
	memset(m_pKey, 0, sizeof(_KEYFRAME) * m_BoneList.size());

	for(unsigned int i=0; i<m_BoneList.size(); i++)
	{
		if (key.find(i) != key.end())
			m_pKey[i] = key[i];
	}

	delete (fp);
	return true;
}

int* CAnimationController::CreateBoneTable(CBoneController * ctl)
{
	std::map <CBoneController*, int*>::iterator it = m_BoneTable.find(ctl);
	if (it != m_BoneTable.end())
		return (*it).second;

	unsigned int boneCount = m_BoneList.size();

	int* boneTable = new int[boneCount];

	for (unsigned i = 0; i<boneCount; i++)
		boneTable[i] = ctl->FindBone(m_BoneList[i].name);

	m_BoneTable.insert(std::map <CBoneController*, int*>::value_type(ctl, boneTable));
	return boneTable;
}

void CAnimationController::Apply(CBoneController * ctl, float t, float blendweight)
{
	if ((m_Flags&(_LOADING_COMPLETE|_LOADING_FAIL)) == 0)
		Loading();

	if (m_Flags&_LOADING_FAIL)
		return ;

	_ASSERT(ctl != NULL);

	int * bonetable = CreateBoneTable(ctl);

	unsigned boneCount = m_BoneList.size();

	for (unsigned i = 0; i<boneCount; i++)
	{
		int idx = bonetable[i];
		if (idx == -1)
			continue;

		const _KEYFRAME &key = m_pKey[i];

		if (key.posKey == 0 || key.posNum == 0)
			continue;

		int k1 = FindPos(key.rotTime, key.rotNum, t);
		float w1 = (t - key.rotTime[k1]) / (key.rotTime[k1+1] - key.rotTime[k1]);
		Quat4 q = Quat4::Slerp(key.rotKey[k1], key.rotKey[k1+1], w1 > 1 ? 1 : w1);

		int k2 = FindPos(key.posTime, key.posNum, t);
		float w2 = (t - key.posTime[k2]) / (key.posTime[k2+1] - key.posTime[k2]);
		Vector3 pos = Vector3::Lerp(key.posKey[k2], key.posKey[k2+1], w2 > 1 ? 1 : w2);

		if (blendweight == 1.0f)
		{
			ctl->SetLocalTM(idx, Mat4::Make(q, pos));
		}
		else
		{
			ctl->SetLocalTM(idx, q, pos, blendweight);
		}
	}
}

float CAnimationController::GetLength()
{
	if ((m_Flags&(_LOADING_COMPLETE | _LOADING_FAIL)) == 0)
		Loading();

	return m_AnimLength;
}

Mat4 CAnimationController::GetTM(int boneidx, float t)
{
	Mat4 tm = Mat4::IDENTITY;

	while(boneidx >= 0)
	{
		const _KEYFRAME &key = m_pKey[boneidx];

		if (key.posKey != 0 && key.posNum != 0)
		{
			int k1 = FindPos(key.rotTime, key.rotNum, t);
			float w1 = (t - key.rotTime[k1]) / (key.rotTime[k1+1] - key.rotTime[k1]);
			Quat4 q = Quat4::Slerp(key.rotKey[k1], key.rotKey[k1+1], w1 > 1 ? 1 : w1);

			int k2 = FindPos(key.posTime, key.posNum, t);
			float w2 = (t - key.posTime[k2]) / (key.posTime[k2+1] - key.posTime[k2]);
			Vector3 pos = Vector3::Lerp(key.posKey[k2], key.posKey[k2+1], w2 > 1 ? 1 : w2);

			tm = tm * Mat4::Make(q, pos);
		}

		boneidx = m_BoneList[boneidx].parent;
	}

	return tm;
}