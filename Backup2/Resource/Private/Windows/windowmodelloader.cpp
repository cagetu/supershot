#include "resourcecore.h"
#include <fbxsdk.h>

FbxManager* g_fbxSdkManager = NULL;

bool WindowModelLoader::Initialize()
{
	_ASSERT(g_fbxSdkManager == NULL);

	// 메모리 관리를 위한 SDK 관리자
	g_fbxSdkManager = FbxManager::Create();
	if (g_fbxSdkManager == NULL)
		return false;

	return true;
}

void WindowModelLoader::Shutdown()
{
	if (g_fbxSdkManager != NULL)
		g_fbxSdkManager->Destroy();
	g_fbxSdkManager = NULL;
}

void TraverseFBXNodes(FbxNode* node)
{
	const char* nodeName = node->GetName();

	// Transforms
	FbxDouble3 translation = node->LclTranslation.Get();
	FbxDouble3 scale = node->LclScaling.Get();
	FbxDouble3 rotation = node->LclRotation.Get();

	int numChildren = node->GetChildCount();

	FbxNode* childNode = NULL;

	for (int i = 0; i < numChildren; i++)
	{
		childNode = node->GetChild(i);

		// mesh
		FbxMesh* mesh = childNode->GetMesh();


		CPolygonSoup polysoup;

		/*	control point
			- 기본적으로 물리적인 버텍스!!
			- control point == vertex
			- control point index == vertex index
			- control point는 4개의 element로 구성되어 있는데, mesh는 xyz 좌표만 사용한다.
			- polygon은 polygon vertex의 group
		*/
		int numControlPoint = mesh->GetControlPointsCount();

		std::vector <Vector3> vertices;
		vertices.reserve(numControlPoint);

		for (int i = 0; i < numControlPoint; i++)
		{
			Vector3 p;

			FbxVector4 point = mesh->GetControlPointAt(i);
			p.x = (float)point.mData[0];
			p.y = (float)point.mData[1];
			p.z = (float)point.mData[2];

			vertices.push_back(p);
		}

		// 메쉬의 폴리곤 개수
		int numPolygons = mesh->GetPolygonCount();

		// 인덱스 리스트
		std::vector <int> indices;

		for (int i = 0; i < numPolygons; i++)
		{
			// 하나의 폴리곤 안에 버텍스 개수
			int numVerts = mesh->GetPolygonSize(i);
			for (int j = 0; j < numVerts; j++)
			{
				// control point의 index를 얻어온다.
				int vertexIndex = mesh->GetPolygonVertex(i, j);
				indices.push_back(vertexIndex);

				FbxVector4 point = mesh->GetControlPointAt(i);

				// 
				//int textureUVIndex = mesh->GetTextureUVIndex(i, j);

				FbxVector4 normal;
				mesh->GetPolygonVertexNormal(i, j, normal);
			}
		}
	}
}

ModelData* WindowModelLoader::LoadFBX(const TCHAR* filename)
{
	_ASSERT(g_fbxSdkManager != NULL);

	// 메모리 관리를 위한 SDK 관리자
	g_fbxSdkManager = FbxManager::Create();
	if (g_fbxSdkManager == NULL)
		return false;

	/*	Import Option
		- import file안에 저장된 모든 데이터 타입이나 특정 데이터 타입을 FBX Scene으로 임포트할 수 있다.
		- import option을 통해서, 카메라, 라이트, 애니메이션을 임포트할 것인지 아닌지를 선택할 수 있다.
		- Import Option은 IOSetting 객체로 설정한다.
		- Import Option의 기본값은 true이고, default라는 의미는 FBX SDK가 처리할 수 있는 모든 데이터 타입을 임포트한다는 의미이다.

	*/
	FbxIOSettings* pIOsettings = FbxIOSettings::Create(g_fbxSdkManager, IOSROOT);
	g_fbxSdkManager->SetIOSettings(pIOsettings);

	// Import options determine what kind of data is to be imported.
	// True is the default, but here we’ll set some to true explicitly, and others to false.
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_MATERIAL, true);
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_TEXTURE, true);
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_LINK, false);
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_SHAPE, false);
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_GOBO, false);
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_ANIMATION, true);
	//(*(g_fbxSdkManager->GetIOSettings())).SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, true);

	/*	Scene 생성
		- 하나의 FBX 파일은 하나의 scene만을 가진다.
		- Scene Scene Graph 노드를 저장하고 있다.
	*/
	FbxScene* pFbxScene = FbxScene::Create(g_fbxSdkManager, "");
	_ASSERT(pFbxScene != NULL);

	/*	FBX Scene으로 file을 임포트한다.
		1. import 객체 생성
		2. initialize 함수로 임포트할 파일 설정
	*/
	FbxImporter* pImporter = FbxImporter::Create(g_fbxSdkManager, "");
	{
		char fname[128];
		string::conv_s(fname, 128, filename);

		// 파일 설정
		bool result = pImporter->Initialize(fname, -1, g_fbxSdkManager->GetIOSettings());
		if (result = false)
		{
			DebugOutMsg("Call to KFbxImporter::Initialize() failed.");
			DebugOutMsgFormat("Error returned: %s", pImporter->GetStatus().GetErrorString());
		}

		// 버전 체크
		int fbxMajorVer, fbxMinorVer, fbxRevisionVer;
		FbxManager::GetFileFormatVersion(fbxMajorVer, fbxMinorVer, fbxRevisionVer);

		int majorVer, minorVer, revisionVer;
		pImporter->GetFileVersion(majorVer, minorVer, revisionVer);

		// Import from file to fbxScene
		result = pImporter->Import(pFbxScene);
		if (result == false)
			return NULL;

		pImporter->Destroy();
	}

	// Scene Graph 순회...
	FbxNode* pFbxRootNode = pFbxScene->GetRootNode();

	return NULL;
}