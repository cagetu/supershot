#include "resourcecore.h"

#include <tga.h>
#ifdef __USE_PNG
#include <png.h>
#include <pnginfo.h>
#endif

IImageHandler::IImageHandler()
{
}

IImageHandler::~IImageHandler()
{
}

PNGHandler::PNGHandler()
{
}
PNGHandler::~PNGHandler()
{
}

bool PNGHandler::Load(const TCHAR* filename)
{
#ifdef __USE_PNG
	FILE* fp = unicode::fopen(filename, L"wb");
	png_structp pPng = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!pPng)
	{
		unicode::fclose(fp);
		return false;
	}
	png_infop pPngInfo = png_create_info_struct(pPng);
	if (pPngInfo)
	{
		unicode::fclose(fp);
		return false;
	}

	if (setjmp(png_jmpbuf(pPng)))
	{
		unicode::fclose(fp);
		return false;
	}

	png_init_io(pPng, fp);
	png_read_info(pPng, pPngInfo);

	uint32 width = png_get_image_width(pPng, pPngInfo);
	uint32 height = png_get_image_height(pPng, pPngInfo);
	uint8 colorType = png_get_color_type(pPng, pPngInfo);
	uint8 depthBit = png_get_bit_depth(pPng, pPngInfo);

	if (depthBit == 16)
		png_set_strip_16(pPng);

	if (colorType == PNG_COLOR_MASK_PALETTE)
		png_set_palette_to_rgb(pPng);

	// PNG_COLOR_TYPE_GRAY is always 8 or 16 bit depth
	if (colorType == PNG_COLOR_TYPE_GRAY && depthBit < 8)
		png_set_expand_gray_1_2_4_to_8(pPng);

	if (png_get_valid(pPng, pPngInfo, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(pPng);

	// These color_type don't have an alpha channel then fill it with 0xff.
	if (colorType == PNG_COLOR_TYPE_RGB || colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_PALETTE)
		png_set_filler(pPng, 0xFF, PNG_FILLER_AFTER);

	if (colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(pPng);

	png_read_update_info(pPng, pPngInfo);

	png_bytep* row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
	for (int y = 0; y < height; y++) {
		row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(pPng, pPngInfo));
	}

	png_read_image(pPng, row_pointers);

	unicode::fclose(fp);
#else
#endif

	return false;
}

bool PNGHandler::LoadHeader()
{
#ifdef __USE_PNG
	png_structp pPng = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!pPng)
	{
		return false;
	}
	png_infop pPngInfo = png_create_info_struct(pPng);
	if (pPngInfo)
	{
		return false;
	}

	m_Width = pPngInfo->width;
	m_Height = pPngInfo->height;
	m_ColorType = pPngInfo->color_type;
	m_BitDepth = pPngInfo->bit_depth;
	m_Channel = pPngInfo->channels;
	m_Format = (pPngInfo->color_type & PNG_COLOR_MASK_COLOR) ? ERGBFormat::RGBA : ERGBFormat::Gray;
#endif

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TGAHandler::TGAHandler()
{
}

TGAHandler::~TGAHandler()
{
}

bool TGAHandler::Load(const TCHAR* filename)
{
	return false;
}