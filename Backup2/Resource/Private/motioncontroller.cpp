﻿#include "scene.h"

#define _NOPHYSICS

CMotionController* CMotionController::Create(CMiniXML::const_iterator node, CModel* model, CMotionSynth* synth)
{
	CMotionController * motionctl = new CMotionController;
	if (motionctl->Init(node, model, synth) == false)
	{
		delete motionctl;
		return NULL;
	}
	return motionctl;
}

void CMotionController::Release(CMotionController* motionctl)
{
	if (motionctl != NULL)
	{
		if (motionctl->m_RefCount <= 0)
		{
#if 1		// 원본에게 카운트를 내려달라고 요청
			if (motionctl->m_RefCtrl != NULL)
			{
				motionctl->m_RefCtrl->m_RefCount--;
			}
#endif
			delete motionctl;
		}
		else
		{
			motionctl->m_RefCount--;
		}
	}
}

CMotionController::CMotionController()
{
//	m_Flags = flags;
	m_BlendWeight = 1.0f;
	m_Time = 0.0f;
	m_BoneCtrlr = NULL;
	m_AnimSpeed = 1.0f;
	m_Param = Vector3::ZERO;
	m_LoopTransitionRange = 0.0f;
	m_MoveDelta = Vector3::ZERO;

	m_RefCount = 0;
	m_RefCtrl = NULL;

	m_AnimationController = NULL;
	m_DeltaAnim = NULL;
	m_DeltaPivot[0] = m_DeltaPivot[1] = m_DeltaPivot[2] = NULL;
#ifdef _ENABLE_IK
	m_IKController = NULL;
#endif
	m_AnimCtl = NULL;
	m_MotionSynth = NULL;

	m_FixedDelta = Vector3::ZERO;

	m_XML = NULL;
	m_TempController[0] = m_TempController[1] = NULL;
}

CMotionController* CMotionController::Duplicate(CModel* model, CMotionSynth* synth)
{
	CMotionController* m = new CMotionController;
	m->m_Flags = m_Flags;
	m->m_Flags.SetFlags(_INSTANCE, true);
	m->m_MotionSynth = synth;
	
	if (m_Flags[_ANIMATION])
	{
		m->m_AnimationController = m_AnimationController;
//		m_AnimationController->IncRef();
	}

	m->m_Condition = m_Condition;

	if (m_Flags[_ANIMSPEED])
		m->m_AnimSpeed = m_AnimSpeed;

	if (m_Flags[_BASEANIM] && m_Flags[_ANI])
	{
		m->m_AnimCtl = m_AnimCtl;

		if (m_Flags[_STOPMOTION])
			m->m_Time = m_Time;
	}

	if (m_Flags[_ANIMBLEND] || m_Flags[_PHYSXBLEND])
	{
		m->m_BlendAnimCtl = NULL;
		m->m_BlendTime[0] = m->m_BlendTime[1] = 0.0f;
		m->m_BlendTime[2] = m_BlendTime[2];
	}

	if (m_Flags[_UPDOWN_BLEND])
	{
		m->m_AnimUpDn[0] = m_AnimUpDn[0];
		m->m_AnimUpDn[1] = m_AnimUpDn[1];
	}

	if (m_Flags[_BONESCALE])
	{
		m->m_BoneScaleID = m_BoneScaleID;
		m->m_BoneScale = m_BoneScale;
	}

	_ASSERT(!m_Flags[_FIXEDPOS]);
	_ASSERT(!m_Flags[_ROTATE]);
	_ASSERT(!m_Flags[_RANGE]);

	if (m_Flags[_IK])
	{
		m_Flags.SetFlags(_IK, false);
//		m->m_IKTarget = m_IKTarget;
//		m->m_IKController = m_IKController->Duplicate(model);
	}

	if (m_Flags[_BONESEPARATE])
	{
		_ASSERT(m_Rootbone != -1);
		m->m_Rootbone = m_Rootbone;
		CBoneController* bonectl = model->GetBoneController();
		m->m_BoneCtrlr = new CBoneController(*bonectl);
		m->m_BoneSet = m_BoneSet;
	}

	if (m_Flags[_DELTAMOTION])
	{
		_ASSERT(m_DeltaAnim != NULL);

		CBoneController* bonectl = model->GetBoneController();
		m->m_DeltaAnim = m_DeltaAnim;

		m->m_DeltaPivot[0] = new CBoneController(*bonectl);
		m->m_DeltaPivot[1] = new CBoneController(*bonectl);
		m->m_DeltaPivot[2] = new CBoneController(*bonectl);
		m->m_DeltaBaseTime = m_DeltaBaseTime;
		m->m_DeltaAnim->Apply(m->m_DeltaPivot[0], m_DeltaBaseTime);

		if (m_Flags[_DELTA_POSE])
		{
			for(int i=0; i<1; i++)
				m->m_DeltaTable[i] = m_DeltaTable[i];
			m->m_DeltaAnim->Apply(m_DeltaPivot[1], m_DeltaTable[0]);
		}
		if (m_Flags[_DELTA_1D])
		{
			for(int i=0; i<3; i++)
				m->m_DeltaTable[i] = m_DeltaTable[i];
		}
		if (m_Flags[_DELTA_2D])
		{
			for(int i=0; i<3*3; i++)
				m->m_DeltaTable[i] = m_DeltaTable[i];
		}
	}

	if (m_Flags[_PHYSICS])
	{
		m->m_BoneCtrlr = new CBoneController(*model->GetBoneController());
		m->m_DefaultController = new CBoneController(*model->GetBoneController());

		m->m_XML = m_XML;

		if (m_Flags[_UPDATEKINEMATIC])
		{
			m->m_Filter = m_Filter;
			m->m_Anchor = m_Anchor;
		}
	}

	if (m_Flags[_MOTIONBLENDING])
	{
		m->m_BlendWeight = m_BlendWeight;
	}

	_ASSERT(!m_Flags[_CONNECTLOOP]);

	if (m_Flags[_HAS_EVENT])
	{
		m->m_Event = m_Event;
	}

	m_RefCount++;
	m->m_RefCtrl = this;

	return m;
}

CMotionController::~CMotionController()
{
	_ASSERT(m_RefCount == 0);

	if (m_BoneCtrlr != NULL)
		delete m_BoneCtrlr;

#ifdef _ENABLE_IK
	if (m_Flags[_IK] && m_IKController != NULL)
		delete m_IKController;
#endif

	if (m_Flags[_ANIMATION] && m_AnimationController != NULL)
	{
		_ASSERT(!m_Flags[_INSTANCE]);
		_ASSERT(m_AnimationController != NULL);
		CAnimationController::Release(m_AnimationController);
	}

	if (m_Flags[_HAS_EVENT])
		delete m_Event;

	if (m_DeltaAnim != NULL)
	{
		CAnimationController::Release(m_DeltaAnim);
		delete m_DeltaPivot[0];
		delete m_DeltaPivot[1];
		delete m_DeltaPivot[2];
	}

	if (m_XML && !m_Flags[_INSTANCE])
		delete m_XML;
}

bool CMotionController::Init(CMiniXML::const_iterator node, CModel* model, CMotionSynth* synth)
{
	m_MotionSynth = synth;

	if (!unicode::strcmp(node.GetName(), TEXT("animation")))
		m_Flags.SetFlags(_ANIMATION, true);

	static MASKPRESET _functions[] =
	{
		TEXT("_IK"), NULL, _IK,
		TEXT("_ANIMATION"), NULL, _ANIMATION,
		TEXT("_BASEANIM"), NULL, _BASEANIM,
		TEXT("_BASEBLEND"), NULL, _BASEBLEND,
		TEXT("_RANGE"), NULL, _RANGE,
		TEXT("_LOOP"), NULL, _LOOP,
		TEXT("_MOTIONBLENDING"), NULL, _MOTIONBLENDING,
		TEXT("_BONESEPARATE"), NULL, _BONESEPARATE,
		TEXT("_ABSOLUTE"), NULL, _ABSOLUTE,
		TEXT("_INCLUDEROOTBONE"), NULL, _INCLUDEROOTBONE,
		TEXT("_DELTAMOTION"), NULL, _DELTAMOTION,
		TEXT("DELTATYPE"), TEXT("포즈"), _DELTA_POSE,
		TEXT("DELTATYPE"), TEXT("1D"), _DELTA_1D,
		TEXT("DELTATYPE"), TEXT("2D"), _DELTA_2D,
		TEXT("_FIXEDPOS"), NULL, _FIXEDPOS,
		TEXT("_ZEROPIVOT"), NULL, _ZEROPIVOT,
		TEXT("_ROTATE"), NULL, _ROTATE,
		TEXT("_ANIMSPEED"), NULL, _ANIMSPEED,
		TEXT("_DISABLE"), NULL, _DISABLE,
//		TEXT("_ANIKEY"), NULL, _ANIKEY,
		TEXT("_ANI"), NULL, _ANI,
		TEXT("_ANITYPE"), TEXT("정지모션"), _STOPMOTION,
		TEXT("_ANITYPE"), TEXT("상하블랜딩"), _UPDOWN_BLEND,
		TEXT("_BONESCALE"), NULL, _BONESCALE,
		TEXT("_DELTAKEYANI"), NULL, _DELTAKEYANI,
		TEXT("_ANIMBLEND"), NULL, _ANIMBLEND,
		TEXT("_MOTIONEVENT"), NULL, _MOTIONEVENT,
		TEXT("_PHYSICS"), NULL, _PHYSICS,
		TEXT("_PHYSXBLEND"), NULL, _PHYSXBLEND,
		TEXT("_UPDATEKINEMATIC"), NULL, _UPDATEKINEMATIC,
		TEXT("_CONNECTLOOP"), NULL, _CONNECTLOOP,
		TEXT("_30FPS"), NULL, _30FPS,
	} ;

	const wchar* mask = node[TEXT("mask")];

	for(unsigned int i=0; i<_countof(_functions); i++)
	{
		if (MASKPRESET::IsSet(mask, _functions[i].mask, _functions[i].value))
			m_Flags.SetFlags(_functions[i].flag, true);
	}

	float fps = m_Flags[_30FPS] ? 30.0f : 1.0f;

	if (m_Flags[_DISABLE])
		return false;

	CParameter param(node);

	if (m_Flags[_ANIMATION])
	{
		const wchar* fname = param.GetString(TEXT("ANIMFILE"));
		m_AnimationController = CAnimationController::LoadAnimationController(fname);
		if (m_AnimationController == NULL)
		{
			m_Flags.SetFlags(_ANIMATION, false);
			m_Flags.SetFlags(_BONESEPARATE, false);
			m_Flags.SetFlags(_FIXEDPOS, false);
			m_Flags.SetFlags(_ZEROPIVOT, false);
		}
	}

	m_Condition = param.GetString(TEXT("CONDITION"));

	if (m_Flags[_ANIMSPEED])
		m_AnimSpeed = param.GetFloat(TEXT("ANIMSPEED"));

	if (m_Flags[_BASEANIM] && m_Flags[_ANI])
	{
		m_AnimCtl = synth ? synth->FindAnimation(param.GetString(TEXT("ANIID"))) : NULL;

		if (m_Flags[_STOPMOTION])
			m_Time = param.GetFloat(TEXT("ANITIME")) / fps;
	}

	if (m_Flags[_BONESCALE])
	{
		CBoneController* bonectl = model->GetBoneController();
		m_BoneScaleID = bonectl->FindBone(param.GetString(TEXT("BONESCALEID")));
		m_BoneScale = param.GetFloat(TEXT("BONESCALE"));
	}

	if (m_Flags[_UPDOWN_BLEND])
	{
		m_AnimUpDn[0] = synth ? synth->FindAnimation(param.GetString(TEXT("UPANIID"))) : NULL;
		m_AnimUpDn[1] = synth ? synth->FindAnimation(param.GetString(TEXT("DNANIID"))) : NULL;
	}

	if (m_Flags[_ANIMBLEND] || m_Flags[_PHYSXBLEND])
	{
		m_BlendAnimCtl = NULL;
		m_BlendTime[0] = m_BlendTime[1] = 0.0f;
		m_BlendTime[2] = param.GetFloat(m_Flags[_PHYSICS] ? TEXT("PHYBLENDTIME") : TEXT("BLENDTIME"), 0.1f);
	}

	if (m_Flags[_FIXEDPOS])
	{
		_ASSERT(m_Flags[_ANIMATION]);
		_ASSERT(m_AnimationController != NULL);

		CBoneController* bonectl = model->GetBoneController();
		m_FixedBoneID = param.GetString(TEXT("FIXEDBONE"));
		m_FixedBone = bonectl->FindBone(m_FixedBoneID);
		if (m_FixedBone == -1)
		{
			m_Flags.SetFlags(_FIXEDPOS, false);
		}
		else
		{
			int parent = m_FixedBone;
			for(; parent != -1; parent=bonectl->GetParent(parent))
				m_FixedRoot = parent;

			if (m_Flags[_ZEROPIVOT])
			{
				m_FixedPos = Vector3::ZERO;
			}
			else
			{
				CBoneController* temp = new CBoneController(*bonectl);
				m_AnimationController->Apply(temp, 0);
				m_FixedPos = temp->GetWorldTM(m_FixedBone).Position();
				delete temp;
			}
		}
	}

	if (m_Flags[_ROTATE])
	{
		if (m_Flags[_FIXEDPOS])
		{
			if (m_TempController[0] == NULL)
				m_TempController[0] = new CBoneController(*model->GetBoneController());

			m_LocalTM = Transform::RotationY(param.GetFloat(TEXT("ROTANGLE")) * _PI / 180.0f);
		}
		else
		{
			m_Flags.SetFlags(_ROTATE, false);
		}
	}

	if (m_Flags[_RANGE])
	{
		m_AnimRange[0] = param.GetFloat(TEXT("RANGE_MIN")) / fps;
		m_AnimRange[1] = param.GetFloat(TEXT("RANGE_MAX")) / fps;
	}

#ifdef _ENABLE_IK
	if (m_Flags[_IK])
	{
		m_IKTarget = param.GetString(TEXT("TARGETNAME"));

		m_IKController = new CIKController;
		m_IKController->SetBoneController(model->GetBoneController());
		if (m_IKController->SetBone(param.GetString(TEXT("CHAINROOT")), param.GetString(TEXT("ENDEFFECTER")), Vector3(0, 0, 0)) == false)
		{
			delete m_IKController;
			m_IKController = NULL;
			m_Flags.SetFlags(_IK, false);
		}
	}
#endif

	if (m_Flags[_BONESEPARATE])
	{
		CBoneController* bonectl = model->GetBoneController();
		m_Rootbone = bonectl->FindBone(param.GetString(TEXT("ANIMROOTBONE")));
		if (m_Rootbone != -1)
		{
			m_BoneCtrlr = new CBoneController(*bonectl);

			for(unsigned int i=0; bonectl->GetBoneName(i); i++)
			{
				if (m_Flags[_INCLUDEROOTBONE] && (int)i == m_Rootbone)
				{
					m_BoneSet.push_back(i);
				}
				else
				{
					for(int h=bonectl->GetParent(i); h!=-1; h=bonectl->GetParent(h))
					{
						if (h == m_Rootbone)
						{
							m_BoneSet.push_back(i);
							break;
						}
					}
				}
			}
		}
		else
		{
			m_Flags.SetFlags(_BONESEPARATE, false);
		}
	}

	if (m_Flags[_DELTAMOTION])
	{
		CBoneController* bonectl = model->GetBoneController();
		const wchar* fname = param.GetString(TEXT("DELTAFILE"));
		CMotionController* ctl = synth->FindAnimation(fname);
		m_DeltaAnim = ctl != NULL ? ctl->GetAnimController() : NULL;
		if (m_DeltaAnim != NULL)
		{
			m_DeltaPivot[0] = new CBoneController(*bonectl);
			m_DeltaPivot[1] = new CBoneController(*bonectl);
			m_DeltaPivot[2] = new CBoneController(*bonectl);
			m_DeltaBaseTime = param.GetFloat(TEXT("DELTABASETIME")) / fps;
			m_DeltaAnim->Apply(m_DeltaPivot[0], m_DeltaBaseTime);

			const wchar* _table1[] = { TEXT("DELTA0") };
			const wchar* _table3[] = { TEXT("DELTA1"), TEXT("DELTA2"), TEXT("DELTA3") };
			const wchar* _table9[] = { TEXT("DELTA01"), TEXT("DELTA02"), TEXT("DELTA03"), TEXT("DELTA11"), TEXT("DELTA12"), TEXT("DELTA13"), TEXT("DELTA21"), TEXT("DELTA22"), TEXT("DELTA23") };

			if (m_Flags[_DELTA_POSE])
			{
				for(int i=0; i<_countof(_table1); i++)
					m_DeltaTable[i] = param.GetFloat(_table1[i]) / fps;
				m_DeltaAnim->Apply(m_DeltaPivot[1], m_DeltaTable[0]);
			}
			if (m_Flags[_DELTA_1D])
			{
				for(int i=0; i<_countof(_table3); i++)
					m_DeltaTable[i] = param.GetFloat(_table3[i]) / fps;
			}
			if (m_Flags[_DELTA_2D])
			{
				for(int i=0; i<_countof(_table9); i++)
					m_DeltaTable[i] = param.GetFloat(_table9[i]) / fps;
			}
		}
		else
		{
			m_Flags.SetFlags(_DELTAMOTION, false);
		}
	}

	if (m_Flags[_PHYSICS])
	{
		m_BoneCtrlr = new CBoneController(*model->GetBoneController());
		m_DefaultController = new CBoneController(*model->GetBoneController());
	}

	if (m_Flags[_MOTIONBLENDING])
	{
		m_BlendWeight = param.GetFloat(TEXT("WEIGHT"));
	}

	if (m_Flags[_CONNECTLOOP])
	{
		if (m_TempController[1] == NULL)
			m_TempController[1] = new CBoneController(*model->GetBoneController());

		m_LoopTransitionRange = param.GetFloat(TEXT("CONNECTSEC")) / fps;
		_ASSERT(m_AnimationController != NULL);
		if (m_LoopTransitionRange > m_AnimationController->GetLength() / 2)
			m_LoopTransitionRange = m_AnimationController->GetLength() / 2;
	}

	CMiniXML::const_iterator evt=node.FindChild(TEXT("event"));
	if (evt != NULL)
	{
		m_Flags.SetFlags(_HAS_EVENT, true);
		m_Event = new CEventHandler;
		m_Event->Open(node);
	}

	return true;
}

static bool IsVisible(const wchar* tag, const wchar* masks) // from renderpass.h
{
	int i, l;
	bool condition = true;

	for(i=0; tag[i] ;)
	{
		if (tag[i] == ',')
			return condition || IsVisible(&tag[i+1], masks);
		if (tag[i] == '&')
			i++;

		bool invert = false;
		if (tag[i] == '!')
		{
			invert = true;
			i++;
		}
		for(l=0; tag[i+l]!='\0' && tag[i+l]!=',' && tag[i+l]!='&'; l++);
		unicode::string keyword(&tag[i], l);

		const wchar *p = unicode::strstr(masks, keyword);
		bool findmatch = false;
		while(findmatch == false && p != NULL)
		{
			findmatch = p != NULL && (p[l] == '\0' || p[l] == ',');
			p = unicode::strstr(p+1, keyword);
		}
		condition = condition && (invert ? !findmatch : findmatch);

		i += l;
	}
	return condition;
}

bool CMotionController::SetEnable(const wchar* masks)
{
	if (m_Condition.empty() || IsVisible(m_Condition, masks) == true)
		return true;
	return false;
}

bool CMotionController::IsFinished() const
{
	if (m_Flags[_BASEANIM])
	{
		if (m_AnimCtl != NULL)
			return m_AnimCtl->IsLoopAnimation() == false && m_Time >= m_AnimCtl->GetLength() - m_LoopTransitionRange ? true : false;
	}
	return false;
}

void CMotionController::PreUpdate(float dt, CMotionSynth* /*synth*/, CEventListener* listener)
{
	_ASSERT(!m_Flags[_DISABLE]);

	float prevtick = m_Time;

	if (!m_Flags[_STOPMOTION])
	{
		if (m_Flags[_MOTIONEVENT] && m_AnimCtl != NULL)
			m_AnimCtl->UpdateEvent(m_Time, m_Time+dt, listener);

		m_Time += dt;
	}

	if (m_Flags[_BASEANIM])
	{
		if (m_AnimCtl != NULL)
		{
			if (m_Flags[_ANIMBLEND])
				m_BlendTime[1] += dt;

			if (m_Flags[_ANIMBLEND] && m_BlendAnimCtl != NULL && m_BlendTime[1] < m_BlendTime[2])
			{
//				m_BlendAnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_BlendTime[0]);
//				m_AnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time, m_BlendTime[1]/m_BlendTime[2]);
				m_MoveDelta = m_AnimCtl->GetDeltaVec(prevtick, m_Time) * (m_BlendTime[1]/m_BlendTime[2]);
			}
			else
			{
//				m_AnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
				m_MoveDelta = m_AnimCtl->GetDeltaVec(prevtick, m_Time);				
			}

			//if (m_MoveDelta != Vector3::ZERO)
			//{
			//	int a = 1;
			//}
		}
	}
}

void CMotionController::Update(float dt, CMotionSynth* synth)
{
	_ASSERT(!m_Flags[_DISABLE]);

	CBoneController* bonectl = synth->GetBoneController(NULL);

	if (m_Flags[_BASEANIM])
	{
		if (m_AnimCtl != NULL)
		{
//			if (m_Flags[_ANIMBLEND])
//				m_BlendTime[1] += dt;

			if (m_Flags[_UPDOWN_BLEND])
			{
				float weight = m_Param.x;

				if (weight >= 1)
				{
					m_AnimUpDn[0]->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
				}
				else if (weight > 0)
				{
					m_AnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
					m_AnimUpDn[0]->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time, weight);
				}
				else if (weight <= -1)
				{
					m_AnimUpDn[1]->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
				}
				else if (weight < 0)
				{
					m_AnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
					m_AnimUpDn[1]->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time, -weight);
				}
				else
				{
					m_AnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
				}
			}
			else
			{
				m_AnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time);
			}

			if (m_Flags[_ANIMBLEND] && m_BlendAnimCtl != NULL && m_BlendTime[1] < m_BlendTime[2])
				m_BlendAnimCtl->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_BlendTime[0], 1-m_BlendTime[1]/m_BlendTime[2]);
		}
	}

	if (m_Flags[_PHYSICS])
	{
#ifndef _NOPHYSICS
		_ASSERT(m_PhysBind != NULL);

		float w = m_BlendWeight;

		if (m_Flags[_PHYSXBLEND])
		{
			m_BlendTime[1] += dt;
			if (m_BlendTime[1] < m_BlendTime[2])
				w *= m_BlendTime[1]/m_BlendTime[2];
		}

		m_BoneCtrlrler->SetTransform(bonectl->GetTransform());
		/*Mat4 tm = */m_PhysBind->Update(m_BoneCtrlrler);

		if (m_Flags[_UPDATEKINEMATIC])
		{
			unsigned int i;

			for(i=0; i<m_Anchor.size(); i++)
			{
				int bone = m_Anchor[i];
				int parent = bonectl->GetParent(bone);
				Mat4 localtm = m_BoneCtrlrler->GetWorldTM(bone) * Mat4::Inverse(bonectl->GetWorldTM(parent));
				m_BoneCtrlrler->SetLocalTM(bone, localtm.GetRotation());
			}

			for(i=0; i<m_Filter.size() && !(m_Filter[i]&0x8000); i++) ;

			bonectl->Apply(*m_BoneCtrlrler, w, &m_Filter[0], i);

			for(; i<m_Filter.size(); i++)
				m_PhysBind->SetTransform(m_Filter[i]&0x7FFF, bonectl->GetWorld(m_Filter[i]&0x7FFF));
		}
		else
		{
			bonectl->Apply(*m_BoneCtrlrler, w);
		}
#endif //!_NOPHYSICS
	}

#if 0
	if (m_Flags[_ANIKEY])
	{
		m_AnimationController->Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, 0);
	}
#endif
	if (m_Flags[_COPY_BASE] && m_BlendWeight < 1.0f)
	{
		_ASSERT(m_BoneCtrlr != NULL);
		m_BoneCtrlr->Apply(*bonectl, 1);
	}

	if (m_Flags[_ANIMATION])
	{
		Apply(m_BoneCtrlr ? m_BoneCtrlr : bonectl, m_Time, m_BlendWeight);
	}

#ifdef _ENABLE_IK
	if (m_Flags[_IK])
	{
		Vector3 pos = synth->FindTarget(m_IKTarget);
		for(int j=0; j<4; j++)
			m_IKController->Apply(pos, m_BlendWeight, m_BoneCtrlr ? m_BoneCtrlr : bonectl);
	}
#endif

	if (m_Flags[_DELTAMOTION])
	{
		if (m_Flags[_DELTA_POSE])
		{
			bonectl->ApplyDelta(*m_DeltaPivot[1], *m_DeltaPivot[0]);
		}
		if (m_Flags[_DELTA_1D])
		{
#if 0
			float weight = cos(timeGetTime() / 3000.0f * 3.14f) * 0.5f + 0.5f;
#else
			float weight = m_Param.x * 0.5f + 0.5f;
#endif
			if (weight > 1.0f)
				weight = 1.0f;
			else if (weight < 0.0f)
				weight = 0.0f;

			float t = weight * 2;
			int idx = (int) t;
			float w = t - idx;

			if (m_Flags[_DELTAKEYANI])
			{
				m_DeltaAnim->Apply(m_DeltaPivot[1], w == 0 ? m_DeltaTable[idx] : m_DeltaTable[idx] + (m_DeltaTable[idx+1]-m_DeltaTable[idx])*w);
			}
			else
			{
				m_DeltaAnim->Apply(m_DeltaPivot[1], m_DeltaTable[idx]);
				if (w > 0)
					m_DeltaAnim->Apply(m_DeltaPivot[1], m_DeltaTable[idx+1], w);
			}
			bonectl->ApplyDelta(*m_DeltaPivot[1], *m_DeltaPivot[0]);
		}
		if (m_Flags[_DELTA_2D])
		{
#if 0
			float weightu = cos(timeGetTime() / 3000.0f * 3.14f * 1.5f + 2) * 0.5f + 0.5f;
			float weightv = cos(timeGetTime() / 3000.0f * 3.14f * 0.3f) * 0.5f + 0.5f;
#else
			float weightu = m_Param.x * 0.5f + 0.5f;
			float weightv = m_Param.y * 0.5f + 0.5f;
#endif
			if (weightu > 1.0f)
				weightu = 1.0f;
			else if (weightu < 0.0f)
				weightu = 0.0f;

			if (weightv > 1.0f)
				weightv = 1.0f;
			else if (weightv < 0.0f)
				weightv = 0.0f;

			float t[2] = { weightu * 2, weightv * 2 };
			int idx[2] = { (int) t[0], (int) t[1] };
			float w[2] = { t[0] - idx[0], t[1] - idx[1] };

			int off = idx[0] + idx[1]*3;

			if (m_Flags[_DELTAKEYANI])
			{
				m_DeltaAnim->Apply(m_DeltaPivot[1], w[0] == 0 ? m_DeltaTable[off] : m_DeltaTable[off] + (m_DeltaTable[off+1]-m_DeltaTable[off])*w[0]);
			}
			else
			{
				m_DeltaAnim->Apply(m_DeltaPivot[1], m_DeltaTable[off]);
				if (w[0] > 0)
					m_DeltaAnim->Apply(m_DeltaPivot[1], m_DeltaTable[off+1], w[0]);
			}

			if (w[1] > 0)
			{
				if (m_Flags[_DELTAKEYANI])
				{
					m_DeltaAnim->Apply(m_DeltaPivot[2], m_DeltaTable[off+3] + (m_DeltaTable[off+4]-m_DeltaTable[off+3])*w[0]);
				}
				else
				{
					m_DeltaAnim->Apply(m_DeltaPivot[2], m_DeltaTable[off+3]);
					if (w[0] > 0)
						m_DeltaAnim->Apply(m_DeltaPivot[2], m_DeltaTable[off+3+1], w[0]);
				}
				m_DeltaPivot[1]->Apply(*m_DeltaPivot[2], w[1]);
			}

			bonectl->ApplyDelta(*m_DeltaPivot[1], *m_DeltaPivot[0]);
		}
	}

	if (m_Flags[_BONESEPARATE])
	{
		if (m_BoneSet.empty() == false)
		{
			if (m_Flags[_ABSOLUTE])
			{
				int bone = m_BoneSet[0];
				int parent = bonectl->GetParent(bone);

				Mat4 localtm = (m_BoneCtrlr->GetWorldTM(bone) * bonectl->GetTransform()) * Mat4::Inverse(bonectl->GetWorldTM(parent));
				bonectl->SetLocalTM(bone, localtm.GetRotation());

				if (m_BoneSet.size() > 1)
					bonectl->Apply(*m_BoneCtrlr, m_BlendWeight, &m_BoneSet[1], m_BoneSet.size()-1);
			}
			else
			{
				bonectl->Apply(*m_BoneCtrlr, m_BlendWeight, &m_BoneSet[0], m_BoneSet.size());
			}
		}
	}

	if (m_Flags[_BONESCALE])
	{
		bonectl->SetLocalTM(m_BoneScaleID, bonectl->GetWorldTM(m_BoneScaleID) * Transform::Scaling(m_BoneScale));
	}

}

void CMotionController::UpdateEvent(float t1, float t2, CEventListener* listener)
{
	float dt = t2 - t1;

	if (m_AnimationController == NULL || dt <= 0 || !m_Flags[_HAS_EVENT] || listener == NULL)
		return ;

	if (m_Flags[_ANIMSPEED])
	{
		t1 *= m_AnimSpeed;
		dt *= m_AnimSpeed;
	}

	float len = m_Flags[_RANGE] ? m_AnimRange[1] - m_AnimRange[0] : m_AnimationController->GetLength();

	if (len <= 0)
		return ;

	if (m_Flags[_LOOP])
		t1 = fmod(t1, len);
	else if (t1 >= len)
		return ;

	_ASSERT(t1 < len);

	if (t1 + dt <= len)
	{
		m_Event->Update(t1, t1+dt, listener);
	}
	else
	{
		m_Event->Update(t1, len, listener);

		if (m_Flags[_LOOP])
			m_Event->Update(0, fmod(t1+dt, len), listener);
	}
}

void CMotionController::Apply(CBoneController * bonectl, float t, float blendweight)
{
	if (m_AnimationController == NULL)
		return ;

	if (m_Flags[_ANIMSPEED])
		t *= m_AnimSpeed;

	if (m_Flags[_CONNECTLOOP])
	{
		float len = (m_Flags[_RANGE] ? m_AnimRange[1] - m_AnimRange[0] : m_AnimationController->GetLength()) - m_LoopTransitionRange;
		float pos = fmod(t, len);

		if (pos < m_LoopTransitionRange)
		{
			ApplyAnimation(m_TempController[1], pos, 1);
			ApplyAnimation(m_TempController[1], len + pos, 1 - (float)pos / m_LoopTransitionRange);
			bonectl->Apply(*m_TempController[1], blendweight);
		}
		else
		{
			ApplyAnimation(bonectl, pos, blendweight);
		}
		return ;
	}


	bonectl->SetDeltaTM(Mat4::IDENTITY);
	Vector3 delta = ApplyAnimation(bonectl, t, blendweight);
	bonectl->SetDeltaTM(Transform::Translate(delta));
}

Vector3 CMotionController::ApplyAnimation(CBoneController * bonectl, float t, float blendweight)
{
	if (m_Flags[_RANGE])
	{
		if (m_AnimRange[1] <= m_AnimRange[0])
			t = m_AnimRange[0];
		else if (t > m_AnimRange[1] - m_AnimRange[0])
			t = m_Flags[_LOOP] ? fmod(t, m_AnimRange[1] - m_AnimRange[0]) + m_AnimRange[0] : m_AnimRange[1];
		else
			t = m_AnimRange[0] + t;

		if (t > m_AnimationController->GetLength())
			t = m_AnimationController->GetLength();
	}
	else
	{
		if (t > m_AnimationController->GetLength())
			t = m_Flags[_LOOP] ? fmod(t, m_AnimationController->GetLength()) : m_AnimationController->GetLength();
	}

	if (m_Flags[_ROTATE])
	{
		m_AnimationController->Apply(m_TempController[0], t);
		const Mat4& tm = m_TempController[0]->GetWorldTM(m_FixedBone);
		int parent = m_TempController[0]->GetParent(m_FixedBone);
		const Mat4& parenttm = parent == -1 ? m_TempController[0]->GetTransform() : m_TempController[0]->GetWorldTM(parent);
		m_TempController[0]->SetLocalTM(m_FixedBone, tm * m_LocalTM * Mat4::Inverse(parenttm));

		bonectl->Apply(*m_TempController[0], blendweight);
	}
	else
	{
		m_AnimationController->Apply(bonectl, t, blendweight);
	}

	if (m_Flags[_FIXEDPOS])
	{
		const Mat4& tm = bonectl->GetWorldTM(m_FixedBone);
		Vector3 delta = m_FixedPos - tm.Position() * bonectl->GetTransform().Inverse();
#if 1
		m_FixedDelta = Vector3(delta.x, 0.0f, delta.z);
#else
		Mat4 localtm = bonectl->GetLocal(m_FixedRoot);
		localtm._41 += delta.x;
//		localtm._42 = delta.y;
		localtm._43 += delta.z;
		bonectl->SetLocal(m_FixedRoot, localtm);
#endif
	}

	return m_FixedDelta;
}

float CMotionController::GetAnimeTime(float t)
{
	if (m_Flags[_ANIMSPEED])
		t *= m_AnimSpeed;

	if (m_Flags[_RANGE])
	{
		if (m_AnimRange[1] <= m_AnimRange[0])
			t = m_AnimRange[0];
		else if (t > m_AnimRange[1] - m_AnimRange[0])
			t = m_Flags[_LOOP] ? fmod(t, m_AnimRange[1] - m_AnimRange[0]) + m_AnimRange[0] : m_AnimRange[1];
		else
			t = m_AnimRange[0] + t;

		if (t > m_AnimationController->GetLength())
			t = m_AnimationController->GetLength();
	}
	else
	{
		if (t > m_AnimationController->GetLength())
			t = m_Flags[_LOOP] ? fmod(t, m_AnimationController->GetLength()) : m_AnimationController->GetLength();
	}

	return t;
}

Vector3 CMotionController::GetPivot(int boneidx, float t)
{
	if (m_Flags[_CONNECTLOOP])
	{
		_ASSERT(0);
		return Vector3::ZERO;
	}

	return m_AnimationController->GetPivot(boneidx, GetAnimeTime(t));
}

Vector3 CMotionController::GetDeltaVec(float prevt, float curt)
{
	if (m_Flags[_FIXEDPOS] == false || prevt == curt)
		return Vector3::ZERO;

	_ASSERT(prevt <= curt);

	float len = m_AnimationController->GetLength();

	if (m_Flags[_LOOP] && curt > len)
	{
		prevt = fmod(prevt, len);
		curt = fmod(curt, len);

		return prevt < curt ? GetDeltaVec(prevt, curt) : GetDeltaVec(prevt, len) + GetDeltaVec(0, curt);
	}
	else
	{
		if (prevt >= len)
			return Vector3::ZERO;
		if (curt > len)
			curt = len;
		int boneid = m_AnimationController->FindBone(m_FixedBoneID);
		return GetPivot(boneid, curt) - GetPivot(boneid, prevt);
	}
}

Mat4 CMotionController::GetAnimTM(const wchar* bonename, float t)
{
	_ASSERT(m_AnimationController != NULL);
	
	int boneid = m_AnimationController->FindBone(bonename);
	if (boneid == -1)
		return Mat4::IDENTITY;
	return m_AnimationController->GetTM(boneid, GetAnimeTime(t));
}

float CMotionController::GetLength() const
{
	if (m_AnimationController != NULL)
		return ((m_Flags[_RANGE] ? m_AnimRange[1] - m_AnimRange[0] : m_AnimationController->GetLength()) - m_LoopTransitionRange) / m_AnimSpeed;
	return 0.0f;
}

void CMotionController::SetValue(CMotionController* motion, bool force, bool notransition)
{
	if (m_Flags[_BASEANIM] && (m_AnimCtl != motion || force == true))
	{
		if (m_Flags[_ANIMBLEND] && m_AnimCtl != NULL)
		{
			m_BlendAnimCtl = m_AnimCtl;
			m_BlendTime[0] = m_Time;
			m_BlendTime[1] = notransition ? m_BlendTime[2] : 0.0f;
		}

		m_AnimCtl = motion;
		m_Time = 0.0f;
	}
}

void CMotionController::SetValue(const Vector3& val)
{
	m_Param = val;
}

void CMotionController::SetValue(const wchar* /*val*/)
{
}

void CMotionController::SetValue(float val)
{
	m_Param.x = val;
}

#ifndef _NOPHYSICS
void CMotionController::SetPhysic(CPhysicSystem* physic, CMiniXML::const_iterator node)
{
	if (m_Flags[_PHYSICS])
	{
		CMiniXML::const_iterator root = node.FindChild(TEXT("physic"));

		if (root == NULL)
		{
			m_Flags.SetFlags(_PHYSICS, false);
			return ;
		}

		for(node=root.FindChild(TEXT("physic")); node!=NULL; node=node.Find(TEXT("physic")))
		{
			if (!unicode::strcmp(node[TEXT("id")], m_PhysicID))
				break;
		}
		
		if (node == NULL)
		{
			m_Flags.SetFlags(_PHYSICS, false);
			return ;
		}

		m_Physics = physic;
		m_XML = new CMiniXML;
		CMiniXML::iterator imm = m_XML->GetRoot().Insert(TEXT("physics"));
		CMiniXML::const_iterator item;

		item = node.FindChild(TEXT("actor");
		if (item != NULL)
			imm.InsertChild(item);

		if (m_Flags[_UPDATEKINEMATIC])
		{
			std::set <int> list;
			std::set <int> kinemlist;
			for(CMiniXML::const_iterator it=item.FindChild(TEXT("actor")); it!=NULL; it=it.Find(TEXT("actor")))
			{
				if (unicode::strstr(it[TEXT("mask"], TEXT("_BINDBONE")))
				{
					const wchar* bone = *it.FindChild(TEXT("BINDBONE"));
					int boneidx = m_DefaultController->Find(bone);
					bool kinematic = unicode::strstr(it[TEXT("mask")], TEXT("_KINEMATIC")) ? true : false;

					list.insert(boneidx);
					m_Filter.push_back(kinematic ? (0x8000|boneidx) : boneidx);
					if (kinematic)
						list.insert(0x8000|boneidx);
				}
			}

			const wchar* bone = m_DefaultController->GetBoneName(0);
			for(unsigned int i=0; bone != NULL;)
			{
				int h = i;

				if (list.find(0x8000 | m_DefaultController->GetParent(i)) != list.end())
					m_Anchor.push_back(i);

				while(h != -1)
				{
					if (list.find(h) != list.end())
					{
						if (list.find(i) == list.end())
							m_Filter.push_back(i);

						list.insert(i);
						break;
					}
					h = m_DefaultController->GetParent(h);
				}
				bone=m_DefaultController->GetBoneName(++i);
			}

			std::sort(m_Filter.begin(), m_Filter.end());
		}

		item = node.FindChild(TEXT("joint"));
		if (item != NULL)
			imm.InsertChild(item);
	}
}
#endif //!_NOPHYSICS

void CMotionController::SetActivate(bool activation, CBoneController* bonectl)
{
#ifndef _NOPHYSICS
	if (m_Flags[_PHYSICS])
	{
		if (m_XML == NULL)
		{
			m_Flags.SetFlags(_PHYSICS, false);
			return;
		}

		if (activation == true)
		{
			_ASSERT(m_PhysBind == NULL);
			_ASSERT(m_XML != NULL);
			_ASSERT(bonectl != NULL);
			m_PhysBind = new CPhysicBind;
			m_PhysBind->Init(m_XML->GetRoot(), m_DefaultController, m_Physics);
			m_PhysBind->SetTransform(bonectl);
//			m_BoneCtrlrler->SetTransform(bonectl->GetTransform());

			if (m_Flags[_PHYSXBLEND])
			{
				m_BlendTime[1] = 0.0f;
			}
		}
		else
		{
			_ASSERT(m_PhysBind != NULL);
			delete m_PhysBind;
			m_PhysBind = NULL;
		}
	}
#endif //!_NOPHYSICS
}

void CMotionController::Impulse(const Vector3 &pivot, const Vector3 &force)
{
#ifndef _NOPHYSICS
	if (m_Flags[_PHYSICS] && m_PhysBind != NULL)
		m_PhysBind->Impulse(pivot, force);
#endif 
}
