#ifndef _LIGHT_H_
#define _LIGHT_H_

class LightEntity : public SceneEntity
{
	__DeclareRtti;
public:
	LightEntity();
	virtual ~LightEntity();

	virtual void Update();

public:
	void SetName(const String& name);
	const String& GetName() const;

	void SetColor(const RGBA& Color);
	const RGBA& GetColor() const;
};

///

class DirectionLight : public LightEntity
{
	__DeclareRtti;
public:
	DirectionLight();
	virtual ~DirectionLight();

	void SetDirection(const Vector3& dir);
	const Vector3& GetDirection() const;

	void Update() OVERRIDE;
};

///
class PointLight : public LightEntity
{
	__DeclareRtti;
public:
	PointLight();
	virtual ~PointLight();

	void SetPosition(const Vector3& pos);
	const Vector3& GetPosition() const;

	void SetAttenuation(float radius);

	void Update() OVERRIDE;
};

///
class SpotLight : public LightEntity
{
	__DeclareRtti;
public:
	SpotLight();
	virtual ~SpotLight();

	void SetPosition(const Vector3& pos);
	const Vector3& GetPosition() const;

	void SetDirection(const Vector3& dir);
	const Vector3& GetDirection() const;

	void Update() OVERRIDE;
};

/// 

class AmbientLight : public LightEntity
{
	__DeclareRtti;
public:
	AmbientLight();
	virtual ~AmbientLight();

	void SetColor(const RGBA& color);
	const RGBA& GetColor() const;

	void Update() OVERRIDE;
};


///

class EnvironmentLight : public LightEntity
{
	__DeclareRtti;
public:
	EnvironmentLight();
	virtual ~EnvironmentLight();

	void Update() OVERRIDE;

};

///

class RimLight : public LightEntity
{
	__DeclareRtti;
public:
	RimLight();
	virtual ~RimLight();

	void SetColor(const RGBA& color);
	const RGBA& GetColor() const;

	void SetPower(float power);
	float GetPower() const;

	void SetWidth(float width);
	float GetWidth() const;

	void SetWeight(float weight);
	float GetWeight() const;

	void Update() OVERRIDE;
};

#endif