#include "scene.h"

__ImplementRtti(VisualShock, CModel, SceneEntity);

CModel::CModel()
{
	m_Flags = 0;
}

CModel::~CModel()
{

}

void CModel::SetWorldTM(const Mat4& tm)
{
	SceneEntity::SetWorldTM(tm);

	m_Flags |= _UPDATE_WORLD_BOUND;
}

void CModel::GetLocalBound(OUT AABB* output) const
{
	UpdateLocalBound();

	*output = m_LocalAABB;
}

void CModel::GetLocalBound(OUT Sphere* output) const
{
	UpdateLocalBound();

	Vector3 center = m_LocalAABB.Center();
	float radius = Vector3::Magnitude(m_LocalAABB.Max() - center);

	*output = Sphere(center, radius);
}

void CModel::GetWorldBound(OUT AABB* aabb) const
{
	if (m_Flags&_UPDATE_WORLD_BOUND)
	{
		UpdateLocalBound();

		m_WorldAABB = m_LocalAABB.Transform(m_WorldTM);

		m_Flags &= ~_UPDATE_WORLD_BOUND;
	}

	*aabb = m_WorldAABB;
}

void CModel::GetWorldBound(OUT Sphere* sphere) const
{
	AABB aabb;
	GetWorldBound(&aabb);

	Vector3 center = aabb.Center();
	float radius = Vector3::Magnitude(aabb.Max() - center);

	*sphere = Sphere(center, radius);
}

void CModel::UpdateLocalBound() const
{
	if (m_Flags&_UPDATE_LOCAL_BOUND)
	{
		m_LocalAABB.Reset();

		for (auto it : m_Meshes)
			m_LocalAABB.Expand(it->GetLocalBound());

		m_Flags &= ~_UPDATE_LOCAL_BOUND;
		m_Flags |= _UPDATE_WORLD_BOUND;
	}
}

CMesh* CModel::CreateMesh(CMeshData* geom, const wchar* obj, int idx)
{
	const CMeshData::_GEOM* g = geom->FindGeom(obj, idx);
	_ASSERT(g != NULL);

	return CreateMesh(g, geom);
}

CMesh* CModel::CreateMesh(CMeshData* geom, int index)
{
	const CMeshData::_GEOM* g = geom->FindGeom(index);
	_ASSERT(g != NULL);

	return CreateMesh(g, geom);
}

CMesh* CModel::CreateMesh(const CMeshData::_GEOM* geom, CMeshData* meshData)
{
	CModelMesh* mesh = new CModelMesh(geom->primitive);
	_ASSERT(mesh != NULL);

	Ptr<CBoneController> boneCtrlr = meshData->CreateBoneController();
	if (boneCtrlr != NULL)
	{
		m_BoneController = boneCtrlr;

		if (geom->bonecount == 1)
			mesh->SetBoneController(m_BoneController, geom->bonetable[0]);
		else if (geom->bonecount > 1)
			mesh->SetBoneController(m_BoneController, geom->bonetable, geom->bonecount);
		else
			mesh->SetBoneController(m_BoneController, geom->boneindex);
	}

	AddMesh(mesh);
	return mesh;
}

void CModel::AddMesh(CMesh* mesh)
{
	m_Meshes.push_back(mesh);
	mesh->IncRef();

	m_Flags |= _UPDATE_LOCAL_BOUND;
}

void CModel::RemoveMesh(CMesh* mesh)
{
	auto it = std::find(m_Meshes.begin(), m_Meshes.end(), mesh);
	if (it == m_Meshes.end())
		return;

	mesh->DecRef();
	m_Meshes.erase(it);

	m_Flags |= _UPDATE_LOCAL_BOUND;
}

void CModel::RemoveAllMeshes()
{
	for (auto it : m_Meshes)
		it->DecRef();

	m_Meshes.clear();
}

const Ptr<CBoneController>& CModel::GetBoneController() const
{
	return m_BoneController;
}

const Ptr<CMotionSynth>& CModel::GetMotionSynth() const
{
	return m_MotionSynth;
}

void CModel::Update(float dt)
{
	if (m_BoneController != NULL)
	{
		// model에서 처리하자..
		m_BoneController->SetTransform(m_WorldTM);
		m_BoneController->Update();
	}
	
	// TODO : 메쉬 스케일 적용
	{
		for (auto it : m_Meshes)
		{
			CMesh* mesh = it;

			if (mesh->IsVisible() == false)
				continue;

			mesh->SetWorldTM(GetWorldTM());
		}
	}
}

void CModel::Render()
{
	for (auto it : m_Meshes)
	{
		it->Render();
	}
}
