#include "scene.h"

__ImplementRootRtti(SceneEntity);

SceneEntity::SceneEntity()
{
}

SceneEntity::~SceneEntity()
{
}

void SceneEntity::SetWorldTM(const Mat4& tm)
{
	m_WorldTM = tm;
}

const Mat4& SceneEntity::GetWorldTM() const
{
	return m_WorldTM;
}

void SceneEntity::GetWorldBound(OUT AABB* aabb) const
{
	*aabb = m_WorldAABB;
}

void SceneEntity::GetWorldBound(OUT Sphere* sphere) const
{
	AABB aabb;
	GetWorldBound(&aabb);

	Vector3 center = aabb.Center();
	float radius = Vector3::Magnitude(aabb.Max() - center);

	*sphere = Sphere(center, radius);
}
